﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ComplaintsFakeAPI.Models
{
    [DataContract]
    public class Category
    {
        [DataMember(Name="id")]
        public int ID { get; set; }
        [DataMember(Name = "name")]
        public string Name { get; set; }
        [DataMember(Name = "imageUrl")]
        public string ImageUrl { get; set; }

        public Category(int id, string name, string imageUrl) 
        {
            ID = id;
            Name = name;
            ImageUrl = imageUrl;
        }

    }
}