﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ComplaintsFakeAPI.Models
{
    [DataContract]
    public class Department
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "color")]
        public string Color { get; set; }

        public Department(int id, string name, string color) 
        {
            Id = id;
            Name = name;
            Color = color;
        }
    }
}