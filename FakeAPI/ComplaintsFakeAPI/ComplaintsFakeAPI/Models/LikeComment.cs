﻿using System.Runtime.Serialization;

namespace ComplaintsFakeAPI.Models
{
    [DataContract]
    public class UserInteraction
    {
        [DataMember(Name = "userId")]
        public string UserId { get; set; }

        [DataMember(Name = "likes")]
        public int Likes { get; set; }

        [DataMember(Name = "comments")]
        public int Comments { get; set; }

        public UserInteraction(string userId, int likes, int comments) 
        {
            UserId = userId;
            Likes = likes;
            Comments = comments;
        }
    }
}