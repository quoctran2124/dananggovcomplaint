﻿using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace ComplaintsFakeAPI.Models
{
    public class CustomActionResult<T> : IHttpActionResult
    {
        private System.Net.HttpStatusCode statusCode;
        private T data;

        public CustomActionResult(System.Net.HttpStatusCode statusCode, T data)
        {
            this.statusCode = statusCode;
            this.data = data;
        }

        public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

    }
}