﻿using ComplaintsFakeAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ComplaintsFakeAPI.Controllers
{
    public class CategoryController : ApiController
    {
        [Route("api/category/getall")]
        [HttpGet]
        public List<Category> GetAllCategories() {
            List<Category> categories = new List<Category> {
                new Category(1, "Hạ tầng đô thị", "http://egov.danang.gov.vn/documents/10180/261989/0688_V1000_226198.png"),
                new Category(2, "An ninh trật tự", "http://egov.danang.gov.vn/documents/10180/261989/0688_V1000_226196.png"),
                new Category(3, "An toàn giao thông", "http://egov.danang.gov.vn/documents/10180/261989/0688_V104_44794.png"),
                new Category(4, "Môi trường", "http://egov.danang.gov.vn/documents/10180/261989/0688_V51_40296.png"),
                new Category(5, "Đạo đức công vụ", "http://egov.danang.gov.vn/documents/10180/261989/0688_V1000_226801.png"),
                new Category(6, "Doanh nghiệp", "http://egov.danang.gov.vn/documents/10180/261989/0688_V1000_226197.png"),
                new Category(7, "Lĩnh vực khác", "http://egov.danang.gov.vn/documents/10180/261989/0688_V46_40194.png")
            };

            return categories;
        }
    }
}
