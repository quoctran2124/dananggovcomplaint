﻿using ComplaintsFakeAPI.Models;
using System.Collections.Generic;
using System.Web.Http;

namespace ComplaintsFakeAPI.Controllers
{
    public class DepartmentController : ApiController
    {
        [Route("api/department/getall")]
        [HttpGet]
        public List<Department> GetAllDepartments() 
        {
            List<Department> departments = new List<Department> {
                new Department(1, "Trung tâm thông tin dịch vụ công", "42f495"),
                new Department(2, "UBND thành phố đà nẵng", "f4df42"),
                new Department(3, "BQL Dự án ĐTXD Hạ tầng và Phát triển Đô thị", "f44262"),
                new Department(4, "Công an Đà Nẵng", "6b42f4"),
                new Department(5, "Sở du lịch", "4245f4"),
                new Department(6, "Sở Giao thông vận tải", "9ef442"),
                new Department(7, "Sở Giáo dục và Đào tạo", "f49842"),
                new Department(8, "Sở Giáo dục và Đào tạo", "42f495"),
                new Department(9, "Sở Giáo dục và Đào tạo", "59f442"),
                new Department(10, "Sở Giáo dục và Đào tạo", "42f495"),
                new Department(11, "Sở Giáo dục và Đào tạo", "59f442"),
                new Department(12, "Sở Giáo dục và Đào tạo", "6b42f4"),
            };

            return departments;
        }
    }
}
