﻿using ComplaintsFakeAPI.Helper;
using ComplaintsFakeAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ComplaintsFakeAPI.Controllers
{
    public class UserInfoController : ApiController
    {
        [Route("api/user/interactionInfo")]
        [HttpGet]
        public HttpResponseMessage getLikeCommentInfo(string userId) 
        {
            HttpResponseMessage message;
            UserInteraction ui = (new DataHelper()).getUserInteraction(userId);

            if (ui != null)
            {
                message = Request.CreateResponse(HttpStatusCode.OK, ui);
            } else
            {
                message = Request.CreateResponse(HttpStatusCode.NotFound, "User not found");
            }

            return message;
        }
    }
}