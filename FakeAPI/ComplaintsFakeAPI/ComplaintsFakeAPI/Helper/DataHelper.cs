﻿using ComplaintsFakeAPI.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace ComplaintsFakeAPI.Helper
{
    public class DataHelper
    {
        private const String UserInteractionDataPath = @"D:\\complaintsFakeData\\UserInteractionData.txt";

        private void WriteToFileIfDoesNotExist(string[] content, string filePath)
        {
            string folderPath = Path.GetDirectoryName(filePath);

            if (!Directory.Exists(folderPath)) 
                Directory.CreateDirectory(folderPath);

            File.WriteAllLines(filePath, content);
        }

        private List<UserInteraction> ParseUserInteraction(string[] data)
        {
            List<UserInteraction> result = new List<UserInteraction>();

            string[] lineData;

            foreach (var row in data)
            {
                lineData = row.Split(',');

                if (lineData.Length < 3)
                {
                    continue;
                }

                int like = 0;
                Int32.TryParse(lineData[1], out like);

                int comment = 0;
                Int32.TryParse(lineData[1], out comment);

                result.Add(new UserInteraction(lineData[0], like, comment));
            }

            return result;
        }

        private UserInteraction FindUserInteraction(string userId, List<UserInteraction> data)
        {
            foreach (var u in data)
            {
                if (u.UserId.Equals(userId))
                    return u;
            }

            return null;
        }

        private List<UserInteraction> GetDefaultUserInteractionList()
        {
            List<UserInteraction> data = new List<UserInteraction>();

            data.Add(new UserInteraction("A", 5, 10));
            data.Add(new UserInteraction("B", 7, 2));
            data.Add(new UserInteraction("C", 0, 0));

            return data;
        }

        private string[] UserInteractionsToString(List<UserInteraction> uis)
        {
            List<string> data = new List<string>();

            foreach (var ui in uis)
            {
                data.Add(string.Format("{0}, {1}, {2}", ui.UserId, ui.Comments, ui.Likes));
            }

            return data.ToArray();
        }
        
        public UserInteraction getUserInteraction(string userId) 
        {
            List<UserInteraction> userInteractions;

            if (!File.Exists(UserInteractionDataPath))
            {
                userInteractions = GetDefaultUserInteractionList();
                WriteToFileIfDoesNotExist(UserInteractionsToString(userInteractions), UserInteractionDataPath);
            }
            else
            {
                string[] data = File.ReadAllLines(UserInteractionDataPath);
                userInteractions = ParseUserInteraction(data);
            }

            return FindUserInteraction(userId, userInteractions);
        }
    }
}