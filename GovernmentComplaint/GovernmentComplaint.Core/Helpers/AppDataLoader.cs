﻿using System;
using System.Xml.Linq;
using System.Xml.Serialization;
using GovernmentComplaint.Core.Models;
using MvvmCross.Platform.Platform;
using MvvmCross.Plugins.File;
using Newtonsoft.Json;
using System.IO;
using System.Collections.Generic;
using System.Linq;

namespace GovernmentComplaint.Core.Helpers
{
	public class AppDataLoader : IAppDataLoader
	{
		private const string SettingFile = "ComplaintApp.json";
		private const string DataFolder = "AppSettings";
		private const string SavedFeedbackFile = "Feedback.json";
		private const string SavedFeedbackFolder = "SavedFeedbacks";
		private const string SavedFeedbackImageFolder = "SavedFeedbacksImage";


		private static object _lockObject = new object();
		private static object _lockFeedbackFileObject = new object();
		private readonly string _filePath;

		private readonly string _feedbackFilePath;

		private readonly IMvxFileStore _fileStore;
		private readonly IMvxResourceLoader _resourceLoader;

		public AppData AppData
		{
			get
			{
				string content;
				AppData result = null;

				lock (_lockObject)
				{
					if (_fileStore.TryReadTextFile(_filePath, out content))
					{
						if (!string.IsNullOrWhiteSpace(content))
						{
							try
							{
								result = JsonConvert.DeserializeObject<AppData>(content);
							}
							catch { }
						}
					}
				}

				return result ?? new AppData();
			}
		}

		public AppDataLoader(IMvxFileStore fileStore, IMvxResourceLoader resourceLoader)
		{
			_resourceLoader = resourceLoader;
			_fileStore = fileStore;
			_filePath = _fileStore.PathCombine(DataFolder, SettingFile);
			_feedbackFilePath = _fileStore.PathCombine(SavedFeedbackFolder, SavedFeedbackFile);
			EnsureFoldersExist();
		}

		private void EnsureFoldersExist()
		{
			_fileStore.EnsureFolderExists(DataFolder);
			_fileStore.EnsureFolderExists(SavedFeedbackFolder);
			_fileStore.EnsureFolderExists(SavedFeedbackImageFolder);
		}

		public void Initialize()
		{
			if (!_fileStore.Exists(SettingFile))
			{
				return;
			}

			lock (_lockObject)
			{
				if (!_fileStore.TryReadBinaryFile(SettingFile, LoadFrom))
				{
					_resourceLoader.GetResourceStream(SettingFile, inputStream => LoadFrom(inputStream));
				}
			}
		}

		public void SaveData(AppData appData)
		{
			lock (_lockObject)
			{
				_fileStore.WriteFile(_filePath, JsonConvert.SerializeObject(appData));
			}
		}

		private bool LoadFrom(Stream inputStream)
		{
			var loadedData = XDocument.Load(inputStream);

			if (loadedData.Root == null)
			{
				return false;
			}

			using (var reader = loadedData.Root.CreateReader())
			{
				new XmlSerializer(typeof(AppData)).Deserialize(reader);
				return true;
			}
		}

		private string SaveImage(MemoryStream stream)
		{
			if (stream != null)
			{
				try
				{
					var imgPath = _fileStore.PathCombine(SavedFeedbackImageFolder, Guid.NewGuid() + ".img");
					_fileStore.WriteFile(imgPath, stream.ToArray());
					return imgPath;
				}
				catch { }
			}
			return null;
		}

		public MemoryStream LoadImage(string filePath)
		{
			byte[] content;
			_fileStore.TryReadBinaryFile(filePath, out content);
			if (content?.Length > 0)
			{
				return new MemoryStream(content);
			}
			return null;
		}

		private void WriteFeedbackToFile(List<NewComplaintContent> feedbacks)
		{
			lock (_lockFeedbackFileObject)
			{
				_fileStore.WriteFile(_feedbackFilePath, JsonConvert.SerializeObject(feedbacks));
			}
		}

		public List<NewComplaintContent> LoadFeedBacks()
		{
			string content;

			lock (_lockFeedbackFileObject)
			{
				if (_fileStore.TryReadTextFile(_feedbackFilePath, out content))
				{
					if (!string.IsNullOrWhiteSpace(content))
					{
						try
						{
							var fbs = JsonConvert.DeserializeObject<List<NewComplaintContent>>(content);
							foreach (var fb in fbs)
							{
								fb.RestoreImageStreams(this);
							}
							return fbs;
						}
						catch { }
					}
				}
			}
			return null;
		}

		public void SaveFeedback(NewComplaintContent feedback)
		{
			var feedbackList = LoadFeedBacks() ?? new List<NewComplaintContent>();

			// save images
			ProcessFeedbackImage(feedback);

			feedbackList.Add(feedback);

			WriteFeedbackToFile(feedbackList);
			var appdata = new AppData();
			appdata.NumberOfOfflineFeedback = feedbackList.Count;
			SaveData(appdata);
		}

		public void SaveOverride(List<NewComplaintContent> feedbacks)
		{
			foreach (NewComplaintContent feedback in feedbacks)
			{
				// save images
				if (feedback?.SavedImagePaths?.Count == 0)
				{
					ProcessFeedbackImage(feedback);
				}
			}

			WriteFeedbackToFile(feedbacks);
		}

		private void ProcessFeedbackImage(NewComplaintContent feedback)
		{
			if (feedback?.Images?.Count > 0)
			{
				feedback.SavedImagePaths = new List<string>();
				foreach (var memoryStream in feedback.Images)
				{
					feedback.SavedImagePaths.Add(SaveImage(memoryStream));
				}

				// clear memory
				feedback.Images.Clear();
			}
		}

		public void RemoveFeedback(List<NewComplaintContent> feedbacks, NewComplaintContent toRemoveFb)
		{
			if (toRemoveFb != null)
			{
				// delete images
				if (toRemoveFb.SavedImagePaths != null)
				{
					foreach (var path in toRemoveFb.SavedImagePaths)
					{
						lock (_lockFeedbackFileObject)
						{
							_fileStore.DeleteFile(path);
						}
					}
				}
				
				feedbacks.Remove(toRemoveFb);
			}
			
			var appdata = new AppData() { NumberOfOfflineFeedback = feedbacks.Count };

			SaveOverride(feedbacks);
			SaveData(appdata);
		}

		public void DeleteAllSavedFeedback()
		{
			try
			{
				lock (_lockFeedbackFileObject)
				{
					_fileStore.DeleteFolder(SavedFeedbackFolder, true);
					_fileStore.DeleteFolder(SavedFeedbackImageFolder, true);
					EnsureFoldersExist();
					var appdata = new AppData() { NumberOfOfflineFeedback = 0 };
					SaveData(appdata);
				}
			} catch { }
		}
	}
}
