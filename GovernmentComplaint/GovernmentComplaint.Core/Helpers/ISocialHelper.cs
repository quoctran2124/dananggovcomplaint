﻿namespace GovernmentComplaint.Core.Helpers
{
    public interface ISocialHelper
    {
        void LogoutFacebook();
        void LogoutGoogle();
    }
}