﻿namespace GovernmentComplaint.Core.Helpers
{
    public interface IBadgeHelper
    {
        void SetBadge(int badgeNumber);
        void ClearBadge();
    }
}