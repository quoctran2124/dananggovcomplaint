﻿using System;

namespace GovernmentComplaint.Core.Helpers
{
    /// <summary>
    /// Custom timer in PCL
    /// </summary>
    public interface ITimer
    {
        /// <summary>
        /// Init action & interval for timer
        /// </summary>
        void Init(Action action, int interval);

        /// <summary>
        /// Start timer
        /// </summary>
        void Start();

        /// <summary>
        /// Stop timer
        /// </summary>
        void Stop();
    }
}
