﻿
using System.Collections.Generic;
using System.IO;
using GovernmentComplaint.Core.Models;

namespace GovernmentComplaint.Core.Helpers
{
	public interface IAppDataLoader
	{
		AppData AppData { get; }
		void Initialize();
		void SaveData(AppData appData);
		MemoryStream LoadImage(string imagePath);
		List<NewComplaintContent> LoadFeedBacks();
		void SaveFeedback(NewComplaintContent feedback);
		void SaveOverride(List<NewComplaintContent> feedbacks);
		void RemoveFeedback(List<NewComplaintContent> feedbacks, NewComplaintContent toRemoveFb);
		void DeleteAllSavedFeedback();
	}
}
