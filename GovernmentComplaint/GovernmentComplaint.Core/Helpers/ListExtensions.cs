﻿using GovernmentComplaint.Core.Enums;
using GovernmentComplaint.Core.ViewModels.ItemViewModels;
using System.Collections.Generic;
using System.Linq;

namespace GovernmentComplaint.Core.Helpers
{
	public static class ListExtensions
	{
		public static void Sort(this List<SidebarItemViewModel> list, SidebarMenuType[] order)
		{
			var newList = new List<SidebarItemViewModel>(list.Count);
			foreach (var orderItem in order)
			{
			    newList.AddRange(list.Where(item => item.MenuType == orderItem));
			}
			list.Clear();
			list.AddRange(newList);
		}
	}
}
