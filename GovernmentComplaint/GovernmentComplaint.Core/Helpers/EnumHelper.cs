﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Reflection;

namespace GovernmentComplaint.Core.Helpers
{
    public static class EnumHelper
    {
        public static string GetDisplayValue(Enum value)
        {
            var fieldInfo = value.GetType().GetRuntimeField(value.ToString());

            var descriptionAttributes = fieldInfo.GetCustomAttributes(typeof(DisplayAttribute), false) as DisplayAttribute[];

            if (descriptionAttributes == null)
            {
                return string.Empty;
            };

            return (descriptionAttributes.Length > 0) ? descriptionAttributes[0].Description : value.ToString();
        }
    }
}
