﻿using System;
using System.Diagnostics;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using GovernmentComplaint.Core.Config;
using GovernmentComplaint.Core.Enums;
using GovernmentComplaint.Core.Models.Responses;
using MvvmCross.Platform;

namespace GovernmentComplaint.Core.Helpers
{
	public class WebApiHelper
	{
		private readonly INetworkHelper _networkHelper;

		public WebApiHelper()
		{
			_networkHelper = Mvx.Resolve<INetworkHelper>();
		}

		public async Task<BaseResponse> Get(string url)
		{
			return await SendRequest(RequestMethod.Get, url).ConfigureAwait(false);
		}

		public async Task<BaseResponse> Post(string url, HttpContent content)
		{
			return await SendRequest(RequestMethod.Post, url, content).ConfigureAwait(false);
		}

		public async Task<BaseResponse> Put(string url, HttpContent content)
		{
			return await SendRequest(RequestMethod.Put, url, content).ConfigureAwait(false);
		}

		public async Task<BaseResponse> Delete(string url)
		{
			return await SendRequest(RequestMethod.Delete, url).ConfigureAwait(false);
		}

		private async Task<BaseResponse> SendRequest(RequestMethod method, string url, HttpContent content = null)
		{
			var response = new BaseResponse
			{
				NetworkStatus = NetworkStatus.Success
			};

			if (!_networkHelper.IsConnected)
			{
				response.NetworkStatus = NetworkStatus.NoWifi;

				return response;
			}

			try
			{
				var client = CreateClient();

				HttpResponseMessage httpResponse = null;

				switch (method)
				{
					case RequestMethod.Get:
						httpResponse = await client.GetAsync(url).ConfigureAwait(false);
						break;
					case RequestMethod.Post:
						httpResponse = await client.PostAsync(url, content).ConfigureAwait(false);
						break;
					case RequestMethod.Put:
						httpResponse = await client.PutAsync(url, content).ConfigureAwait(false);
						break;
					case RequestMethod.Delete:
						httpResponse = await client.DeleteAsync(url).ConfigureAwait(false);
						break;
					default:
						throw new Exception("Request method " + method + " is not supported");
				}

				response.ConstructFromResponse(httpResponse);
			}
			catch (OperationCanceledException)
			{
				Debug.WriteLine("Time out when sending request: " + url);
				response.NetworkStatus = NetworkStatus.Timeout;
			}
			catch (Exception e)
			{
				Debug.WriteLine("Error: \"{0}\" with request: {1}", e.Message, url);
				response.NetworkStatus = NetworkStatus.Exception;
			}

			return response;
		}

		private HttpClient CreateClient()
		{
			var client = new HttpClient
			{
				BaseAddress = new Uri(ApiConfig.ApiUrl),
				Timeout = TimeSpan.FromSeconds(ConstVariable.ServiceTimeOut)
			};

			var byteArray =
				Encoding.UTF8.GetBytes(string.Format("{0}:{1}", ConstVariable.AuthorizationUsername,
					ConstVariable.AuthorizationPasword));
			client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic",
				Convert.ToBase64String(byteArray));

			return client;

		}
	}
}