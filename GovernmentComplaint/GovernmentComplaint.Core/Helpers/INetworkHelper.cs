﻿using System;

namespace GovernmentComplaint.Core.Helpers
{
	public interface INetworkHelper
	{
		bool IsConnected { get; }
		EventHandler NetworkConnected { get; set; }
	}
}