﻿namespace GovernmentComplaint.Core.Helpers
{
	public interface IResourceHelper
	{
		string IconPath(string icon);
	}

}
