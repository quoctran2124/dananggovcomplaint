﻿using System;
using System.Threading.Tasks;

namespace GovernmentComplaint.Core.Helpers
{
    public static class ExecutionHelper
    {
        public static async void ExecuteWithOffset(Action action, int offset)
        {
            await Task.Delay(offset);
            action();
        }
    }
}
