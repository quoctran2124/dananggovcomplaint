﻿namespace GovernmentComplaint.Core.Helpers
{
	public interface IVersionHelper
	{
		string GetVersion();
	}
}
