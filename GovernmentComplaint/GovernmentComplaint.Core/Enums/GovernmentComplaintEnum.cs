﻿using System.ComponentModel.DataAnnotations;

namespace GovernmentComplaint.Core.Enums
{
	public enum AuthenticationStatus
	{
		Unauthenticated,
		Authenticated
	}

	public enum WebServiceState
	{
		Available,
		NoInternetConnection
	}

	public enum NetworkStatus
	{
		Null,
		Success,
		NoWifi,
		Timeout,
		Exception
	}

	public enum SidebarMenuType
	{
		// Display name for icon name, change image name to make sense later
		[Display(Description = "email_sidebar")] NewComplaint,
		[Display(Description = "home")] Home,
		[Display(Description = "noti")] Notification,
		[Display(Description = "history")] History,
		[Display(Description = "about")] Intro,
		[Display(Description = "logout")] Logout,
	    [Display(Description = "dashboard")] Dashboard,
    }

	public enum ItemStatus
	{
		Active,
		Inactive
	}

	public enum LabelStatus
	{
		Active,
		Inactive,
		Special,
		Count
	}

	public enum BackgroundType
	{
		Normal,
		Error
	}

	public enum HeaderButtonType
	{
		None,
		Search,
		SearchActive,
		Back,
		Menu,
		MenuActive,
		Logo
	}

	public enum IndicatorStyle
	{
		None,
		Dots
	}

	public enum WsCommand
	{
		GetData,
		GetComplaints,
		GetComplaintsByKeyword,
		PostComplaint,
		GetDetailComplaint,
		GetNotifications,
		GetComplaintHistory,
		GetUserId,
		GetUserInfo,
		CountNotification,
		PostComment,
		LikeComment,
		VerifyResult,
		RateReply,
		SaveUser,
		GetDepartments,
		GetCategories,
		OutOfData,
		OutOfDataComplaints,
		OutOfHistory,
		OutOfNotification,
		NoWifi
	}

	public enum RequestMethod
	{
		Get,
		Post,
		Put,
		Delete
	}

	public enum PreviousPage
	{
		CreateNewComplaint,
		ComplaintDetail,
		HomeViewModel
	}

	public enum AccountType
	{
		GOOGLE,
		FACEBOOK
	}

    public enum TimeSelectionEnum
    {
        Week,
        Month,
        Year
    }
}