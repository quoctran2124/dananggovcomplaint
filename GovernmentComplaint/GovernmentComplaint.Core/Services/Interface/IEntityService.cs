﻿using System;
using GovernmentComplaint.Core.Models;

namespace GovernmentComplaint.Core.Services.Interface
{
	public interface IEntityService<T> where T : IEntity
	{
		T Create(T entity);
		void GetAll();
		T Get(Guid id);
		bool Update(T entity);
		bool Delete(T entity);
	}
}
