﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using GovernmentComplaint.Core.Config;
using GovernmentComplaint.Core.Enums;
using GovernmentComplaint.Core.Helpers;
using GovernmentComplaint.Core.Models;
using GovernmentComplaint.Core.Models.Requests;
using GovernmentComplaint.Core.ViewModels.Bases;
using MvvmCross.Platform;

namespace GovernmentComplaint.Core.Services
{
	public class ManagementService : IManagementService
	{
		private readonly WebApiHelper _apiHelper;
		private readonly IDataModel _dataModel;
		private readonly ITimer _timer;

		public ManagementService(IDataModel dataModel)
		{
			_dataModel = dataModel;
			_apiHelper = new WebApiHelper();
			// TODO: init after login successfully
			_timer = Mvx.Resolve<ITimer>();
			_timer.Init(() => CountNotifications(), ConstVariable.TimeToLoadNotification);
		}

		public void GetDepartments()
		{
			Task.Run(async () =>
			{
				var response = await _apiHelper.Get(ApiConfig.GetDepartment);
				if (response != null && response.NetworkStatus == NetworkStatus.Success)
					_dataModel.Departments = JsonHelper.Deserialize<List<Department>>(response.RawContent);
				_dataModel.RaiseDataReceived(WsCommand.GetDepartments, response.StatusCode, response.NetworkStatus);
			});
		}

		public void GetCategories()
		{
			Task.Run(async () =>
			{
				var response = await _apiHelper.Get(ApiConfig.GetCategories);
				if (response != null && response.NetworkStatus == NetworkStatus.Success)
					_dataModel.Categories = JsonHelper.Deserialize<List<Category>>(response.RawContent);
				_dataModel.RaiseDataReceived(WsCommand.GetCategories, response.StatusCode, response.NetworkStatus);
			});
		}

		public void GetUserInfo(string id, string type)
		{
			string parameters = $"?maTaiKhoan={id}&loaiTaiKhoan={type}";
			Task.Run(async () =>
			{
				var response = await _apiHelper.Get(ApiConfig.GetUserInfo + parameters);

				if (response == null)
				{
					return;
				}

				if (response.NetworkStatus == NetworkStatus.Success)
				{
					_dataModel.UserInfo = JsonHelper.Deserialize<Authentication>(response.RawContent);
					_dataModel.GetUserInfoNetworkStatus = response.NetworkStatus;
					_dataModel.RaiseDataReceived(WsCommand.GetUserInfo, response.StatusCode, response.NetworkStatus);
				}
				else
				{
					_dataModel.GetUserInfoNetworkStatus = response.NetworkStatus;
					_dataModel.RaiseDataReceived(WsCommand.OutOfData, response.StatusCode, response.NetworkStatus);
				}
			});
		}

		public bool VerifyLinkFb(string videoLink)
		{
			var response = _apiHelper.Get(videoLink).Result;
			if (response != null && response.NetworkStatus == NetworkStatus.Success)
			{
				_dataModel.FacebookLink = JsonHelper.Deserialize<FacebookLink>(response.RawContent);
				try
				{
					if (_dataModel.FacebookLink.ErrorFacebook.Code == 100)
						return true;
				}
				catch (Exception)
				{
					return true;
				}
			}

			return false;
		}

		public bool VerifyLinkYt(string videoLink)
		{
			var response = _apiHelper.Get(videoLink).Result;
			if (response != null && response.NetworkStatus == NetworkStatus.Success)
			{
				if (string.CompareOrdinal(response.RawContent, "Not Found") == 0 ||
				    string.CompareOrdinal(response.RawContent, "Unauthorized") == 0)
					return false;
			}
			else
			{
				return false;
			}
			return true;
		}


		public void GetHistoryComplaints(int page, int sortType)
		{
			if (_dataModel.UserInfo != null)
			{
				string parameters = $"?page={page}&userId={_dataModel.UserInfo.UserId}";
				if (sortType > 0)
				{
					parameters += $"&loai={sortType}";
				}
				Task.Run(async () =>
				{
					var response = await _apiHelper.Get(ApiConfig.GetComplaintHistory + parameters);
					if (response != null && response.NetworkStatus == NetworkStatus.Success)
					{
						var newComplaints = JsonHelper.Deserialize<List<ComplaintContent>>(response.RawContent);
						if (newComplaints == null)
						{
							_dataModel.RaiseDataReceived(WsCommand.OutOfHistory, response.StatusCode,
								response.NetworkStatus);
						}
						else
						{
							if (_dataModel.ComplaintHistory == null)
							{
								_dataModel.ComplaintHistory = newComplaints;
							}
							else
							{
								_dataModel.ComplaintHistory = _dataModel.ComplaintHistory.Union(newComplaints).ToList();
							}
							_dataModel.RaiseDataReceived(WsCommand.GetComplaintHistory, response.StatusCode,
								response.NetworkStatus);
						}
					}
					else
					{
						_dataModel.RaiseDataReceived(WsCommand.OutOfHistory, response.StatusCode, response.NetworkStatus);
					}
				});
			}
			else
			{
				_dataModel.RaiseDataReceived(WsCommand.OutOfHistory, HttpStatusCode.OK,
					NetworkStatus.Success);
			}
		}

		public void GetDetailComplaint(int id)
		{
			Task.Run(async () =>
			{
				var response = await _apiHelper.Get(ApiConfig.GetDetailComplaint + "?id=" + id);
				if (response != null && response.NetworkStatus == NetworkStatus.Success)
					_dataModel.ComplaintContent = JsonHelper.Deserialize<ComplaintContent>(response.RawContent);
				_dataModel.RaiseDataReceived(WsCommand.GetDetailComplaint, response.StatusCode, response.NetworkStatus);
			});
		}

		public void GetNotifications(int page)
		{
			if (_dataModel.UserInfo != null)
			{
				string parameters = $"?page={page}&userId={_dataModel.UserInfo.UserId}";
				Task.Run(async () =>
				{
					var response = await _apiHelper.Get(ApiConfig.GetNotification + parameters);
					if (response != null && response.NetworkStatus == NetworkStatus.Success)
					{
						var newNoti = JsonHelper.Deserialize<List<Notification>>(response.RawContent);
						if (newNoti.Count == 0)
						{
							_dataModel.RaiseDataReceived(WsCommand.OutOfNotification, response.StatusCode,
								response.NetworkStatus);
						}
						else
						{
							if (_dataModel.Notifications == null)
								_dataModel.Notifications = newNoti;
							else
								_dataModel.Notifications = _dataModel.Notifications.Union(newNoti).ToList();
							_dataModel.RaiseDataReceived(WsCommand.GetNotifications, response.StatusCode,
								response.NetworkStatus);
						}
					}
					else
					{
						_dataModel.RaiseDataReceived(WsCommand.OutOfNotification, HttpStatusCode.OK,
							response.NetworkStatus);
					}
				});
			}
			else
			{
				_dataModel.RaiseDataReceived(WsCommand.OutOfNotification, HttpStatusCode.OK,
					NetworkStatus.Success);
			}
		}

		public void CountNotifications()
		{
			if (_dataModel.UserInfo != null)
			{
				string parameters = $"?userId={_dataModel.UserInfo.UserId}";
				Task.Run(async () =>
				{
					var response = await _apiHelper.Get(ApiConfig.CountNotification + parameters);
					if (response != null && response.NetworkStatus == NetworkStatus.Success)
					{
						_dataModel.CountNotification = JsonHelper.Deserialize<int>(response.RawContent);
						_dataModel.RaiseDataReceived(WsCommand.CountNotification, response.StatusCode,
							response.NetworkStatus);
					}
				});
			}
		}

		public void ChangeNotificationStatus(int thongBaoId)
		{
			string parameters = $"?thongBaoId={thongBaoId}&trangThai=DA_XEM";
			Task.Run(async () =>
			{
				var response = await _apiHelper.Put(ApiConfig.PutNotificationsStatus + parameters, null);
				CountNotifications();
			});
		}

		public void StartTimer()
		{
			_timer.Start();
		}

		public void StopTimer()
		{
			_timer.Stop();
		}

		public void PostComplaint(string complaintContentText, string complaintLocationText, List<MemoryStream> images,
			string videoLink)
		{
			if (_dataModel.UserInfo != null)
			{
				Task.Factory.StartNew(async () =>
				{
					_dataModel.CanPostComplaint = false;
					var base64Images = images?.Select(i => Convert.ToBase64String(i.ToArray())).ToList() ?? new List<string>();
					var complaintRequest = new ComplaintRequest
					{
						UserId = _dataModel.UserInfo.UserId,
						Location = complaintLocationText,
						DateCreated = DateTime.Now.ToString("dd/MM/yyyy"),
						TimeCreated = DateTime.Now.ToString("HH:mm"),
						Content = complaintContentText,
						VideoPath = videoLink
					};
					switch (base64Images.Count)
					{
						case 1:
							complaintRequest.ImagePath = base64Images[0];
							complaintRequest.ImageName = "Complaint_App.jpeg";
							break;
						case 2:
							complaintRequest.ImagePath = base64Images[0];
							complaintRequest.ImageName = "Complaint_App.jpeg";
							complaintRequest.ImagePath1 = base64Images[1];
							complaintRequest.ImageName1 = "Complaint_App1.jpeg";
							break;
						case 3:
							complaintRequest.ImagePath = base64Images[0];
							complaintRequest.ImageName = "Complaint_App.jpeg";
							complaintRequest.ImagePath1 = base64Images[1];
							complaintRequest.ImageName1 = "Complaint_App1.jpeg";
							complaintRequest.ImagePath2 = base64Images[2];
							complaintRequest.ImageName2 = "Complaint_App2.jpeg";
							break;
						default:
							break;
					}

					var dataInString = JsonHelper.Serialize(complaintRequest);
					var content = new StringContent(dataInString, Encoding.UTF8, "application/json");

					var response = await _apiHelper.Post(ApiConfig.PostComplaint, content);
					if (response.Success && response.NetworkStatus == NetworkStatus.Success)
					{
						var complaintContent = JsonHelper.Deserialize<ComplaintContent>(response.RawContent);
						_dataModel.CanPostComplaint = !string.IsNullOrEmpty(complaintContent.Id.ToString());
					}
					_dataModel.RaiseDataReceived(WsCommand.PostComplaint, response.StatusCode,
						response.NetworkStatus);
				});
			}
		}

		public void RateReply(long replyId, double userRate)
		{
			var ratingData = new {xuLyId = replyId, userId = _dataModel.UserInfo.UserId, rate = userRate};
			var dataInString = JsonHelper.Serialize(ratingData);
			var content = new StringContent(dataInString, Encoding.UTF8, "application/json");
			Task.Run(async () =>
			{
				var response = await _apiHelper.Post(ApiConfig.Rate, content);
				if (response.Success && response.NetworkStatus == NetworkStatus.Success)
				{
					_dataModel.ComplaintContent.RatingFeedback = JsonHelper.Deserialize<double>(response.RawContent);
				}
					_dataModel.RaiseDataReceived(WsCommand.RateReply, response.StatusCode, response.NetworkStatus);
			});
		}

		public void SaveUser(string name, string email, string phoneNumber, string avatarUrl, string accountType,
			string accountId)
		{
			var newUser = new Authentication
			{
				FullName = name,
				Email = email,
				Phone = phoneNumber,
				AvatarURL = avatarUrl,
				AccountType = accountType,
				AccountId = accountId
			};
			var dataInString = JsonHelper.Serialize(newUser);
			var content = new StringContent(dataInString, Encoding.UTF8, "application/json");
			Task.Factory.StartNew(async () =>
			{
				var response = await _apiHelper.Post(ApiConfig.SaveUser, content);
				if (response.Success && response.NetworkStatus == NetworkStatus.Success)
				{
					_dataModel.UserInfo = JsonHelper.Deserialize<Authentication>(response.RawContent);
					_dataModel.UserInfo.FullName = name;
					_dataModel.UserInfo.Email = email;
					_dataModel.UserInfo.Phone = phoneNumber;
					_dataModel.UserInfo.AvatarURL = avatarUrl;
					_dataModel.UserInfo.AccountType = accountType;
					_dataModel.UserInfo.AccountId = accountId;
					_dataModel.RaiseDataReceived(WsCommand.SaveUser, response.StatusCode, response.NetworkStatus);
				}
			});
		}

		public void PostComment(int complaintDetailId, int userId, int? parentId, string commentContent,
			string interactionType)
		{
			var commentRequest = new CommentRequest
			{
				ComplaintDetailId = complaintDetailId,
				UserId = userId,
				ParentId = parentId,
				CommentContent = commentContent,
				InteractionType = interactionType
			};
			var dataInString = JsonHelper.Serialize(commentRequest);
			var content = new StringContent(dataInString, Encoding.UTF8, "application/json");
			Task.Factory.StartNew(async () =>
			{
				var response = await _apiHelper.Post(ApiConfig.PostReaction, content);
				//if (response.Success && response.NetworkStatus == NetworkStatus.Success)
				//{
				//	var deserialize = JsonHelper.Deserialize<ComplaintContent>(response.RawContent);
				//}
				_dataModel.RaiseDataReceived(WsCommand.PostComment, response.StatusCode,
							response.NetworkStatus);
			});
		}

		public async Task<bool> LikeComment(int complaintDetailId, int userId, int? parentId, string commentContent,
			string interactionType)
		{
			_dataModel.CanLikeComment = false;
			var commentRequest = new CommentRequest
			{
				ComplaintDetailId = complaintDetailId,
				UserId = userId,
				ParentId = parentId,
				CommentContent = commentContent,
				InteractionType = interactionType
			};

			var dataInString = JsonHelper.Serialize(commentRequest);
			var content = new StringContent(dataInString, Encoding.UTF8, "application/json");
			var response = await _apiHelper.Post(ApiConfig.PostReaction, content);
			if (response != null)
				if (response.Success && response.NetworkStatus == NetworkStatus.Success)
				{
					if (string.Equals(response.RawContent, "FailUpdate24h"))
					{
						return false;
					}
				}
				else
				{
					if (response.NetworkStatus == NetworkStatus.NoWifi)
					{
						MasterViewModel.Instance.IsNoWifiPopupShown = true;
						return false;
					}
					if (response.NetworkStatus == NetworkStatus.Timeout ||
					    response.NetworkStatus == NetworkStatus.Exception)
					{
						MasterViewModel.Instance.IsConnectionErrorPopupShown = true;
						return false;
					}
				}
			return true;
		}

		public void GetComplaints(int page, int catId, int sortType, string keyword, int departmentId)
		{
			string parameters = $"?page={page}";
			if (catId > 0)
			{
				parameters += $"&linhvuc={catId}";
			}
			if (sortType > 0)
			{
				parameters += $"&loai={sortType}";
			}

			if (!string.IsNullOrEmpty(keyword))
			{
				parameters += $"&keyword={ConvertToUnsign(keyword)}";
			}

			if (departmentId > 0)
			{
				parameters += $"&coquan={departmentId}";
			}

			Task.Run(async () =>
			{
				var response = await _apiHelper.Get(ApiConfig.GetComplaints + parameters);
				if (response != null && response.NetworkStatus == NetworkStatus.Success)
				{
					var newComplaints = JsonHelper.Deserialize<List<ComplaintContent>>(response.RawContent);
					if (newComplaints == null)
					{
						_dataModel.RaiseDataReceived(WsCommand.OutOfDataComplaints, response.StatusCode,
							response.NetworkStatus);
					}
					else
					{
						if (_dataModel.ComplaintContents == null ||
						    (page == 1 &&
						     (catId != 0 || catId != 0 || sortType != 0 || !string.IsNullOrEmpty(keyword) || departmentId != 0)))
						{
							_dataModel.ComplaintContents = newComplaints;
						}
						else
						{
							_dataModel.ComplaintContents = _dataModel.ComplaintContents.Union(newComplaints).ToList();
						}
						_dataModel.RaiseDataReceived(WsCommand.GetComplaints, response.StatusCode,
							response.NetworkStatus);
					}
				}
				else
				{
					_dataModel.RaiseDataReceived(WsCommand.OutOfDataComplaints, response.StatusCode,
						response.NetworkStatus);
				}
			});
		}

		public string ConvertToUnsign(string str)
		{
			string[] signs =
			{
				"aAeEoOuUiIdDyY",
				"áàạảãâấầậẩẫăắằặẳẵ",
				"ÁÀẠẢÃÂẤẦẬẨẪĂẮẰẶẲẴ",
				"éèẹẻẽêếềệểễ",
				"ÉÈẸẺẼÊẾỀỆỂỄ",
				"óòọỏõôốồộổỗơớờợởỡ",
				"ÓÒỌỎÕÔỐỒỘỔỖƠỚỜỢỞỠ",
				"úùụủũưứừựửữ",
				"ÚÙỤỦŨƯỨỪỰỬỮ",
				"íìịỉĩ",
				"ÍÌỊỈĨ",
				"đ",
				"Đ",
				"ýỳỵỷỹ",
				"ÝỲỴỶỸ"
			};
			for (var i = 1; i < signs.Length; i++)
			for (var j = 0; j < signs[i].Length; j++)
				str = str.Replace(signs[i][j], signs[0][i - 1]);
			return str;
		}

		private byte[] GetArray(Stream stream)
		{
			using (var memoryStream = new MemoryStream())
			{
				stream.CopyTo(memoryStream);
				return memoryStream.ToArray();
			}
		}
	}
}