﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using GovernmentComplaint.Core.Models;

namespace GovernmentComplaint.Core.Services
{
    public interface IManagementService
    {
        void GetCategories();
        void GetDepartments();
        void GetComplaints(int page,int catId,int sortType, string keyword, int departmentId);
        void StartTimer();
        void StopTimer();
        void PostComplaint(string complaintContentText, string complaintLocationText, List<MemoryStream> images, string videoLink);
        void GetDetailComplaint(int id);
        void GetNotifications(int page);
        void GetHistoryComplaints(int page, int sortType);
        void CountNotifications();
        void PostComment(int complaintDetailId, int userId, int? parentId, string commentContent, string interactionType);
        Task<bool> LikeComment(int complaintDetailId, int userId, int? parentId, string commentContent, string interactionType);
        void GetUserInfo(string id, string type);
        bool VerifyLinkFb(string videoLink);
        bool VerifyLinkYt(string videoLink);
        void SaveUser(string name, string email, string phoneNumber, string avatarUrl, string accountType, string accountId);
        void RateReply(long replyId, double rate);
        void ChangeNotificationStatus(int thongBaoId);
    }
}
