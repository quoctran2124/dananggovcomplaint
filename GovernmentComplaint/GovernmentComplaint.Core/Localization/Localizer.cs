﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Reflection;

namespace GovernmentComplaint.Core.Localization
{
    /// <summary>
    ///     The localization static class
    /// </summary>
    public static class Localizer
	{
		// The filenames now contain the same ISO 639-1 two letter code as the CurrentCulture and as the languages
		// codes used in our application, so this can be made smarter without repetition of the full paths
		private const string Assembly = "GovernmentComplaint.Core.Localization.";
		private const string LanguageFileNameFormat = "UserConfig_lang_{0}.properties";
		private const string DefaultLanguageFileName = "UserConfig_lang_en.properties";

		private static string _keyPrefix = ""; // if keyPrefix is not empty, add this prefix to each key that is looked up
		private static string _defaultLanguage = "";
		private static string _currentLanguage = "";
		private static Dictionary<string, string> _dictionary = new Dictionary<string, string>();

		public static string GetCurrentLanguage()
		{
			return _currentLanguage;
		}

		public static void ChangeLanguage(string isoCode)
		{
			LoadResources();
		}

		/// <summary>
		///     Setting language files will be loaded from disk or from embedded resource, then load resources
		/// </summary>	
		public static void Initialize(string defaultLanguage = "vn")
		{
			_defaultLanguage = defaultLanguage; 
            _keyPrefix = "";

            LoadResources();
		}

		/// <summary>
		///     Gets the text. First try with device specific prefix, then without prefix, final fallback is empty string.
		/// </summary>
		public static string GetText(string key)
		{
			// First try to lookup the key including prefix (if prefix configured)
			if (!string.IsNullOrEmpty(_keyPrefix))
			{
				var lookupKey = _keyPrefix + key;
				if (_dictionary.ContainsKey(lookupKey))
				{
					return _dictionary[lookupKey];
				}
			}
			return _dictionary.ContainsKey(key) ? _dictionary[key] : string.Empty;
		}

		#region Load Resources

		/// <summary>
		///     Reads the resource file.
		/// </summary>
		public static void LoadResources()
		{
            // Set current language
            _currentLanguage = _defaultLanguage;

            // Load all localization keys from corresponding .properties resource file
            LoadLocalizationKeys();
		}

		private static string DefaultLanguage
		{
			get
			{
				// if available, use the configured default language, otherwise use the current culture
				if (!string.IsNullOrEmpty(_defaultLanguage))
				{
					return _defaultLanguage;
				}
				return CultureInfo.CurrentCulture.TwoLetterISOLanguageName;
			}
		}

		private static void LoadLocalizationKeys()
		{
			_dictionary = new Dictionary<string, string>();

			var fileName = string.Format(LanguageFileNameFormat, _currentLanguage);
			var localizationResources = ReadResourceFile(fileName);

			using (var reader = new StringReader(localizationResources))
			{
				string line;
				while ((line = reader.ReadLine()) != null)
				{
					// # indicates comment line
					if (!(line.StartsWith("#") || string.IsNullOrWhiteSpace(line)))
					{
						// each key value pair: key = value
						var i = line.IndexOf('=');

						if (i >= 0)
						{
							AddKeyAndValue(line, i);
						}
						else
						{
							// In release mode, just ignore this line
							Debug.WriteLine("Line is not a comment or key value pair in file '{0}': '{1}'", fileName, line);
						}
					}
				}
			}
		}

		private static void AddKeyAndValue(string line, int i)
		{
			var key = line.Substring(0, i).Trim();

			if (!string.IsNullOrWhiteSpace(key) && !key.Contains(" "))
			{
				var value = line.Substring(i + 1).Trim().Replace("\\n", "\n");
				_dictionary.Add(key, value);
			}
			else
			{
				Debug.WriteLine("Key '{0}' is not a valid key", key);
			}
		}

		private static string ReadResourceFile(string fileName)
		{
			var assembly = typeof(Localizer).GetTypeInfo().Assembly;
			var filePath = Assembly + fileName;

            using (var stream = assembly.GetManifestResourceStream(filePath))
            {
                if (stream != null)
                {
                    using (var reader = new StreamReader(stream))
                    {
                        return reader.ReadToEnd();
                    }
                }
            }

            return ReadResourceFile(DefaultLanguageFileName);
        }

		#endregion
    }
}