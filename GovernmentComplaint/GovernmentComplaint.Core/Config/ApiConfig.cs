﻿	namespace GovernmentComplaint.Core.Config
{
	public class ApiConfig
	{
        //public const string ApiUrl = "https://qa.danang.gov.vn/GopY-WS/user/";
        public const string ApiUrl = "https://egov.danang.gov.vn/GopY-WS/user/";

		public const string GetCategories = "chude";
		public const string GetComplaints = "ykien";
		public const string PostComplaint = "saveykien";
		public const string GetDetailComplaint = "chitiet";
		public const string GetDepartment = "coquan";
		public const string GetNotification = "thongbao";
		public const string CountNotification = "thongbaochuadoc";
		public const string GetComplaintHistory = "lichsugopy";
		public const string PostReaction = "savetuongtac";
		public const string GetUserInfo = "userlogin";
		public const string Rate = "ratetrungbinh";
		public const string PutNotificationsStatus = "updatetttb";
		public const string SaveUser = "saveuser";
	}
}