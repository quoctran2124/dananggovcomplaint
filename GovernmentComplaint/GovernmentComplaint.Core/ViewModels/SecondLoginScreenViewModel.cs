﻿using System;
using System.Windows.Input;
using GovernmentComplaint.Core.Enums;
using GovernmentComplaint.Core.Helpers;
using GovernmentComplaint.Core.Localization;
using GovernmentComplaint.Core.Models;
using GovernmentComplaint.Core.Services;
using GovernmentComplaint.Core.ViewModels.Bases;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform;

namespace GovernmentComplaint.Core.ViewModels
{
	public class SecondLoginScreenViewModel : DetailViewModel
	{
		private string _previousPage;
		private readonly INetworkHelper _networkHelper;
		private readonly IAppDataLoader _applDataLoader;

		public SecondLoginScreenViewModel(IManagementService managementService, IDataModel dataModel,
			IResourceHelper resourceHelper, IAppDataLoader applDataLoader)
			: base(managementService, dataModel, resourceHelper)
		{
			_networkHelper = Mvx.Resolve<INetworkHelper>();
			_applDataLoader = applDataLoader;

			MasterViewModel.Instance.TitleBar = Localizer.GetText("LoginTitle");
			MasterViewModel.Instance.SecondLoginScreenViewModel = this;
			MasterViewModel.Instance.InitButtonOnDetailView();
			MasterViewModel.Instance.LeftButtonClicked += OnLeftButtonClicked;
			GoogleLoginCommand = new MvxCommand<UserInfo>(ParseUserInfo);
		}

		public void Init(string previousPage, int numOfOfflineFeedback = 0)
		{
			_previousPage = previousPage;

			SecondLoginText = numOfOfflineFeedback > 0 ? string.Format(_loginInstructionResendFeedback, numOfOfflineFeedback) : _loginInstruction;
			IsCancelResendingButtonShown = numOfOfflineFeedback > 0;
			MasterViewModel.Instance.IsLeftButtonShown = !IsCancelResendingButtonShown;
			MasterViewModel.Instance.IsRightButtonShown = !IsCancelResendingButtonShown;
		}

		private void ParseUserInfo(UserInfo user)
		{
			MasterViewModel.Instance.LoggedInUser = user;
			MasterViewModel.Instance.LoginSuccess(user);
		}

		private void OnLeftButtonClicked(object sender, EventArgs e)
		{
			OnBackCommand();
		}

		protected override void UnregisteredEvent()
		{
			base.UnregisteredEvent();
			MasterViewModel.Instance.LeftButtonClicked -= OnLeftButtonClicked;
		}

		protected override void OnBackCommand()
		{
			if (IsCancelResendingButtonShown)
			{
				return;
			}

			if (DataModel.PreviousPage.Equals(PreviousPage.CreateNewComplaint))
			{
				OnShowDetailView(typeof(CreateNewComplaintViewModel),
					delegate
					{
						ShowViewModel<CreateNewComplaintViewModel>(new
						{
							previousPage = _previousPage
						});
					});
			}
			else if (DataModel.PreviousPage.Equals(PreviousPage.ComplaintDetail))
			{
				OnShowDetailView(typeof(ComplaintDetailViewModel),
					delegate
					{
						ShowViewModel<ComplaintDetailViewModel>(new
						{
							id = DataModel.ComplaintContent.Id,
							pageType = _previousPage,
							searchText = DataModel.Keyword
						});
					});
			}
			else if (DataModel.PreviousPage.Equals(PreviousPage.HomeViewModel))
			{
				OnShowDetailView(typeof(HomeViewModel),
					delegate
					{
						ShowViewModel<HomeViewModel>();
					});
			}

		}

		private void OnCheckNetworkCommand()
		{
			if (!_networkHelper.IsConnected)
			{
				MasterViewModel.Instance.IsNoWifiPopupShown = true;
			}
		}

		private void OnCancelResendingCommand()
		{
			_applDataLoader.DeleteAllSavedFeedback();

			OnShowDetailView(typeof(HomeViewModel),
					delegate
					{
						ShowViewModel<HomeViewModel>();
					});
		}


		#region Properties

		private string _loginInstruction => Localizer.GetText("SecondLoginText");
		private string _loginInstructionResendFeedback => Localizer.GetText("SecondLoginTextResendFeedback");

		private string _secondLoginText;
		public string SecondLoginText
		{
			get { return _secondLoginText; }
			set { SetProperty(ref _secondLoginText, value); }
		}


		public string CancelResendingOfflineFeedbackText => Localizer.GetText("CancelSendSavedFeedback");

		public string Facebook => Localizer.GetText("Facebook");
		public string Google => Localizer.GetText("Google");
		public MvxCommand<UserInfo> GoogleLoginCommand { get; private set; }

		private ICommand _checkNetworkCommand;
		public ICommand CheckNetworkCommand => _checkNetworkCommand ??
											   (_checkNetworkCommand = new MvxCommand(OnCheckNetworkCommand));

		private ICommand _cancelResendingCommand;

		public ICommand CancelResendingCommand
			=> _cancelResendingCommand ?? (_cancelResendingCommand = new MvxCommand(OnCancelResendingCommand));

		private bool _isCancelResendingButtonShown;
		public bool IsCancelResendingButtonShown
		{
			get { return _isCancelResendingButtonShown; }
			set { SetProperty(ref _isCancelResendingButtonShown, value); }
		}

		#endregion
	}
}