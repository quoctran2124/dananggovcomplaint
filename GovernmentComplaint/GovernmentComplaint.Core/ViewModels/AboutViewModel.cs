﻿using System;
using GovernmentComplaint.Core.Helpers;
using GovernmentComplaint.Core.Localization;
using GovernmentComplaint.Core.Models;
using GovernmentComplaint.Core.Services;
using GovernmentComplaint.Core.ViewModels.Bases;
using MvvmCross.Platform;

namespace GovernmentComplaint.Core.ViewModels
{
	public class AboutViewModel : DetailViewModel
	{
		public AboutViewModel(IManagementService managementService, IDataModel dataModel, IResourceHelper resourceHelper)
			: base(managementService, dataModel, resourceHelper)
		{
			MasterViewModel.Instance.InitButtonOnProcessingDetailView();
			MasterViewModel.Instance.IsIndicatorShown = false;
			MasterViewModel.Instance.TitleBar = Localizer.GetText("AboutTitle");
			MasterViewModel.Instance.LeftButtonClicked += OnLeftButtonClicked;
			dataModel.PreviousPage = null;
		}

		protected override void UnregisteredEvent()
		{
			base.UnregisteredEvent();
			MasterViewModel.Instance.LeftButtonClicked -= OnLeftButtonClicked;
		}

		private void OnLeftButtonClicked(object sender, EventArgs e)
		{
			OnBackCommand();
		}

		protected override void OnBackCommand()
		{
			base.OnBackCommand();
			ClearSearchResult();
			MasterViewModel.Instance.ExecuteBeforeShowDetailView(typeof(HomeViewModel),
				delegate { ShowViewModel<HomeViewModel>(); });
		}

		#region UI Texts
		public string FirstIntro => Localizer.GetText("FirstIntro");
		public string SecondIntro => Localizer.GetText("SecondIntro");
		public string ThirdIntro => Localizer.GetText("ThirdIntro");
		public string FourthIntro => string.Format(Localizer.GetText("FourthIntro"), Mvx.Resolve<IVersionHelper>().GetVersion());
		public string FifthIntro => Localizer.GetText("FifthIntro");
		public string SixthIntro => Localizer.GetText("SixthIntro");
		public string SeventhIntro => Localizer.GetText("SeventhIntro");
		public string EighthIntro => Localizer.GetText("EighthIntro");
		#endregion
	}
}