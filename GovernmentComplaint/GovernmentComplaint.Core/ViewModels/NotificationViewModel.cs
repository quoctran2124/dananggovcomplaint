﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Windows.Input;
using GovernmentComplaint.Core.Enums;
using GovernmentComplaint.Core.Helpers;
using GovernmentComplaint.Core.Localization;
using GovernmentComplaint.Core.Models;
using GovernmentComplaint.Core.Services;
using GovernmentComplaint.Core.ViewModels.Bases;
using GovernmentComplaint.Core.ViewModels.ItemViewModels;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform;

namespace GovernmentComplaint.Core.ViewModels
{
	public class NotificationViewModel : DetailViewModel
	{
		private readonly INetworkHelper _networkHelper;

		public NotificationViewModel(IManagementService managementService, IDataModel dataModel,
			IResourceHelper resourceHelper)
			: base(managementService, dataModel, resourceHelper)
		{
			ManagementService.StartTimer();
			_networkHelper = Mvx.Resolve<INetworkHelper>();
			DataModel.DataReceived += OnDataReceived;
			MasterViewModel.Instance.TitleBar = Localizer.GetText("NotificationTitle");
			MasterViewModel.Instance.InitButtonOnProcessingDetailView();
			MasterViewModel.Instance.LeftButtonClicked += OnLeftButtonClicked;
			LoadMoreData = new MvxCommand(OnLoadMoreData);
			RefreshCommand = new MvxCommand(OnRefreshCommand);
			CheckNetworkCommand = new MvxCommand(OnCheckNetworkCommand);
			if (DataModel.Notifications == null)
			{
				IsNoDataTextShown = true;
			}
			dataModel.PreviousPage = null;
		}

		protected override void UnregisteredEvent()
		{
			base.UnregisteredEvent();
			MasterViewModel.Instance.LeftButtonClicked -= OnLeftButtonClicked;
		}

		public void Init(int lastComplaintItemId)
		{
			if (DataModel.Notifications == null || lastComplaintItemId == 0) return;
			var item = DataModel.Notifications.FirstOrDefault(m => m.ComplaintId == lastComplaintItemId);
			_indexOfItem = DataModel.Notifications.IndexOf(item);
			LastComplaintItemPosition = _indexOfItem;
		}

		protected override void HandleDataReceived(WsCommand command, HttpStatusCode code)
		{
			base.HandleDataReceived(command, code);
			if (code == HttpStatusCode.OK)
				switch (command)
				{
					case WsCommand.GetNotifications:
						MasterViewModel.Instance.IsIndicatorShown = false;
						NotificationItemViewModels =
							DataModel.Notifications.Select(
									m => new NotificationItemViewModel(m, ManagementService, DataModel, ResourceHelper))
								.ToList();
						if (DataModel.Notifications != null)
						{
							IsNoDataTextShown = false;
						}
						LoadingMore = false;
						break;
					case WsCommand.OutOfNotification:
						MasterViewModel.Instance.IsIndicatorShown = false;
						if (DataModel.Notifications != null)
						{
							IsNoDataTextShown = false;
						}
						break;
					case WsCommand.OutOfData:
						MasterViewModel.Instance.IsIndicatorShown = false;
						IsNoDataTextShown = DataModel.Notifications == null;
						break;
				}
		}

		#region Properties

		private readonly SemaphoreSlim _lock = new SemaphoreSlim(1);
		private List<NotificationItemViewModel> _notificationItemViewModels;
		private int _page = 1;
		public string NoData => Localizer.GetText("NoData");

		public List<NotificationItemViewModel> NotificationItemViewModels
		{
			get { return _notificationItemViewModels; }
			set
			{
				_notificationItemViewModels = value;
				RaisePropertyChanged(() => NotificationItemViewModels);
			}
		}

		private bool _isRefreshing;

		public bool IsRefreshing
		{
			get { return _isRefreshing; }
			set
			{
				_isRefreshing = value;
				RaisePropertyChanged(() => IsRefreshing);
			}
		}

		private bool _loadingMore;

		public bool LoadingMore
		{
			get { return _loadingMore; }
			set
			{
				_loadingMore = value;
				RaisePropertyChanged(() => LoadingMore);
			}
		}


		private bool _isNoDataTextShown;

		public bool IsNoDataTextShown
		{
			get { return _isNoDataTextShown; }
			set
			{
				_isNoDataTextShown = value;
				RaisePropertyChanged(() => IsNoDataTextShown);
			}
		}

		private int _lastComplaintItemPosition;

		public int LastComplaintItemPosition
		{
			get { return _lastComplaintItemPosition; }
			set
			{
				_lastComplaintItemPosition = value;
				RaisePropertyChanged(() => LastComplaintItemPosition);
			}
		}

		private bool _moveListToTop;

		public bool MoveListToTop
		{
			get { return _moveListToTop; }
			set
			{
				_moveListToTop = value;
				RaisePropertyChanged(() => MoveListToTop);
			}
		}

		private int _indexOfItem;
		public ICommand CheckNetworkCommand { get; private set; }
		public ICommand RefreshCommand { get; set; }
		public ICommand LoadMoreData { get; set; }

		#endregion

		#region Command Method

		private void OnLoadMoreData()
		{
			if (DataModel.Notifications == null) return;
			else
			{
				if (DataModel.Notifications.Count >= 15)
				{
					MasterViewModel.Instance.IsIndicatorShown = true;
					ManagementService.GetNotifications(++_page);
				}
				else
				{
					MasterViewModel.Instance.IsIndicatorShown = false;
				}
			}
		}

		private void OnCheckNetworkCommand()
		{
			LoadingMore = false;
			if (_networkHelper.IsConnected)
			{
				if (DataModel.Notifications == null)
				{
					MasterViewModel.Instance.IsIndicatorShown = true;
					ManagementService.GetNotifications(1);
				}
				else
				{
					_page = DataModel.Notifications.Count / 15;
					NotificationItemViewModels = DataModel.Notifications
						.Select(m => new NotificationItemViewModel(m, ManagementService, DataModel, ResourceHelper))
						.ToList();
					MasterViewModel.Instance.IsIndicatorShown = false;
				}
			}
			else
			{
				MasterViewModel.Instance.IsNoWifiPopupShown = true;
			}
		}

		protected override void OnClose()
		{
			base.OnClose();
			ManagementService.StopTimer();
		}

		private async void OnRefreshCommand()
		{
			if (_networkHelper.IsConnected)
			{
				await _lock.WaitAsync();
				try
				{
					MasterViewModel.Instance.IsIndicatorShown = true;
					IsRefreshing = true;
					DataModel.Notifications = null;
					ManagementService.GetNotifications(1);
					IsRefreshing = false;
				}
				finally
				{
					_lock.Release();
				}
			}
			else
			{
				MasterViewModel.Instance.IsNoWifiPopupShown = true;
				IsRefreshing = false;
			}
		}

		protected override void OnBackCommand()
		{
			base.OnBackCommand();
			ClearSearchResult();
			OnShowDetailView(typeof(HomeViewModel),
				delegate { ShowViewModel<HomeViewModel>(); });
		}

		private void OnLeftButtonClicked(object sender, EventArgs e)
		{
			OnBackCommand();
		}

		#endregion
	}
}