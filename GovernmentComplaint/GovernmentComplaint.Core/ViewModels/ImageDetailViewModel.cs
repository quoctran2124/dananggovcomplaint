﻿using System;
using GovernmentComplaint.Core.Helpers;
using GovernmentComplaint.Core.Models;
using GovernmentComplaint.Core.Services;
using GovernmentComplaint.Core.ViewModels.Bases;

namespace GovernmentComplaint.Core.ViewModels
{
	public class ImageDetailViewModel : DetailViewModel
	{
		private int _detailId;
		private string _previousPreviousPage;

		public ImageDetailViewModel(IManagementService managementService, IDataModel dataModel,
			IResourceHelper resourceHelper) : base(managementService, dataModel, resourceHelper)
		{
			MasterViewModel.Instance.LeftButtonClicked += OnSpecificBackClick;
		}

		public void Init(int id, string pageType, int scrollPosition)
		{
			_previousPreviousPage = pageType;
			_detailId = id;
			_scrollPosition = scrollPosition;
			ImagePaths = DataModel.ComplaintContent.PictureStrings;
			if (ImagePaths.Length > 2)
			{
				ImagesQuantity = 3;
				IsImage2Shown = true;
				IsImage3Shown = true;
				IsImage1Shown = true;
				ImagePath1 = ImagePaths[0];
				ImagePath2 = ImagePaths[1];
				ImagePath3 = ImagePaths[2];
				IsIndicator1Shown = true;
			}
			else if (ImagePaths.Length > 1)
			{
				ImagesQuantity = 2;
				IsImage2Shown = true;
				IsImage1Shown = true;
				ImagePath1 = ImagePaths[0];
				ImagePath2 = ImagePaths[1];
				IsIndicator1Shown = true;
			}
			else if (ImagePaths.Length > 0)
			{
				ImagesQuantity = 1;
				ImagePath1 = ImagePaths[0];
			}
		}
		
		private void OnSpecificBackClick(object sender, EventArgs e)
		{
			OnBackCommand();
		}

		protected override void OnBackCommand()
		{
			MasterViewModel.Instance.ExecuteBeforeShowDetailView(typeof(ComplaintDetailViewModel),
				delegate
				{
					ShowViewModel<ComplaintDetailViewModel>(new {id = _detailId, pageType = _previousPreviousPage});
				});
		}

		protected override void UnregisteredEvent()
		{
			base.UnregisteredEvent();
			MasterViewModel.Instance.LeftButtonClicked -= OnSpecificBackClick;
		}

		#region Properties

		private bool _isIndicator1Shown;

		public bool IsIndicator1Shown
		{
			get { return _isIndicator1Shown; }
			set
			{
				_isIndicator1Shown = value;
				RaisePropertyChanged(() => IsIndicator1Shown);
			}
		}

		private int _scrollPosition;

		public int ScrollPosition
		{
			get { return _scrollPosition; }
			set
			{
				_scrollPosition = value;
				RaisePropertyChanged(() => ScrollPosition);
			}
		}

		private string[] _imagePaths;

		public string[] ImagePaths
		{
			get { return _imagePaths; }
			set
			{
				_imagePaths = value;
				RaisePropertyChanged(() => ImagePaths);
			}
		}

		private string _imagePath1;

		public string ImagePath1
		{
			get { return _imagePath1; }
			set
			{
				_imagePath1 = value;
				RaisePropertyChanged(() => ImagePath1);
			}
		}

		private string _imagePath2;

		public string ImagePath2
		{
			get { return _imagePath2; }
			set
			{
				_imagePath2 = value;
				RaisePropertyChanged(() => ImagePath2);
			}
		}

		private string _imagePath3;

		public string ImagePath3
		{
			get { return _imagePath3; }
			set
			{
				_imagePath3 = value;
				RaisePropertyChanged(() => ImagePath3);
			}
		}

		private bool _isImage1Shown;

		public bool IsImage1Shown
		{
			get { return _isImage1Shown; }
			set
			{
				_isImage1Shown = value;
				RaisePropertyChanged(() => IsImage1Shown);
			}
		}

		private bool _isImage2Shown;

		public bool IsImage2Shown
		{
			get { return _isImage2Shown; }
			set
			{
				_isImage2Shown = value;
				RaisePropertyChanged(() => IsImage2Shown);
			}
		}

		private bool _isImage3Shown;

		public bool IsImage3Shown
		{
			get { return _isImage3Shown; }
			set
			{
				_isImage3Shown = value;
				RaisePropertyChanged(() => IsImage3Shown);
			}
		}

		private int _imagesQuantity;

		public int ImagesQuantity
		{
			get { return _imagesQuantity; }
			set
			{
				_imagesQuantity = value;
				RaisePropertyChanged(() => ImagesQuantity);
			}
		}

		#endregion
	}
}