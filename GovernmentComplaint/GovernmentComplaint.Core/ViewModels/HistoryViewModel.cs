﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Windows.Input;
using GovernmentComplaint.Core.Enums;
using GovernmentComplaint.Core.Helpers;
using GovernmentComplaint.Core.Localization;
using GovernmentComplaint.Core.Models;
using GovernmentComplaint.Core.Services;
using GovernmentComplaint.Core.ViewModels.Bases;
using GovernmentComplaint.Core.ViewModels.ItemViewModels;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform;

namespace GovernmentComplaint.Core.ViewModels
{
	public class HistoryViewModel : DetailViewModel
	{
		private readonly SemaphoreSlim _lock = new SemaphoreSlim(1);
		private int _page = 1;
		private readonly INetworkHelper _networkHelper;

		public HistoryViewModel(IManagementService managementService, IDataModel dataModel,
			IResourceHelper resourceHelper)
			: base(managementService, dataModel, resourceHelper)
		{
			_networkHelper = Mvx.Resolve<INetworkHelper>();
			MasterViewModel.Instance.HistoryViewModel = this;
			MasterViewModel.Instance.InitButtonOnProcessingDetailView();
			MasterViewModel.Instance.LeftButtonClicked += OnLeftButtonClicked;
			IsSortingShown = false;
			dataModel.DataReceived += OnDataReceived;
			InitCommands();
			MasterViewModel.Instance.TitleBar = Localizer.GetText("HistoryTitle");
			if (DataModel.ComplaintHistory == null)
			{
				IsNoDataTextShown = true;
			}
			else
			{
				InitDropdownItems();
			}
			dataModel.PreviousPage = null;
		}
		
		protected override void HandleDataReceived(WsCommand command, HttpStatusCode code)
		{
			base.HandleDataReceived(command, code);
			if (code == HttpStatusCode.OK)
			{
				InitDropdownItems();
				switch (command)
				{
					case WsCommand.GetComplaintHistory:
						MasterViewModel.Instance.IsIndicatorShown = false;
						FeedbackHistoryItemViewModels = DataModel.ComplaintHistory
							.Select(m => new FeedbackItemViewModel(m, ManagementService, DataModel, ResourceHelper))
							.ToList();
						if (DataModel.ComplaintHistory.Count > 0)
							IsNoDataTextShown = false;
						IsSortingShown = false;
						LoadingMore = false;
						break;
					case WsCommand.OutOfHistory:
						MasterViewModel.Instance.IsIndicatorShown = false;
						if (DataModel.ComplaintHistory == null || DataModel.ComplaintHistory.Count <= 0)
						{
							IsNoDataTextShown = true;
						}
						break;
					case WsCommand.OutOfData:
						MasterViewModel.Instance.IsIndicatorShown = false;
						break;
				}
			}
		}

		protected override void UnregisteredEvent()
		{
			base.UnregisteredEvent();
			MasterViewModel.Instance.LeftButtonClicked -= OnLeftButtonClicked;
		}

		public void Init(int lastComplaintItemId)
		{
			if (DataModel.ComplaintHistory == null || lastComplaintItemId == 0) return;
			var item = DataModel.ComplaintHistory.FirstOrDefault(m => m.Id == lastComplaintItemId);
			_indexOfItem = DataModel.ComplaintHistory.IndexOf(item);
			LastComplaintItemPosition = _indexOfItem;
		}

		private void InitDropdownItems()
		{
			var historySortingItemViewModels = new List<HistorySortingItemViewModel>();
			var sortingList = new List<SortingCondition>(new[]
			{
				new SortingCondition
				{
					SortId = 1,
					SortCondition = Localizer.GetText("Time"),
					SortImage = ResourceHelper.IconPath("calendar_sort")
				},
				new SortingCondition
				{
					SortId = 2,
					SortCondition = Localizer.GetText("Interest"),
					SortImage = ResourceHelper.IconPath("interest")
				},
				new SortingCondition
				{
					SortId = 3,
					SortCondition = Localizer.GetText("Comment"),
					SortImage = ResourceHelper.IconPath("comment_sort")
				}
			});
			foreach (var sortType in sortingList)
			{
				var sortingItemViewmodel = new HistorySortingItemViewModel(sortType, ManagementService, DataModel,
					ResourceHelper);
				historySortingItemViewModels.Add(sortingItemViewmodel);
			}
			HistorySortingItemViewModels = historySortingItemViewModels;
		}

		#region Commands

		public ICommand CheckNetworkCommand { get; private set; }

		public ICommand LoadMoreData { get; set; }

		public ICommand SortingCommand { get; private set; }

		public ICommand SortingOverlayCommand { get; private set; }

		public ICommand RefreshCommand { get; private set; }

		#endregion

		#region Properties

		private List<HistorySortingItemViewModel> _historySortingItemViewModels;

		public List<HistorySortingItemViewModel> HistorySortingItemViewModels
		{
			get { return _historySortingItemViewModels; }
			set
			{
				_historySortingItemViewModels = value;
				RaisePropertyChanged(() => HistorySortingItemViewModels);
			}
		}

		private List<FeedbackItemViewModel> _feedbackHistoryItemViewModels;

		public List<FeedbackItemViewModel> FeedbackHistoryItemViewModels
		{
			get { return _feedbackHistoryItemViewModels; }
			set
			{
				_feedbackHistoryItemViewModels = value;
				RaisePropertyChanged(() => FeedbackHistoryItemViewModels);
			}
		}

		private bool _loadingMore;

		public bool LoadingMore
		{
			get { return _loadingMore; }
			set
			{
				_loadingMore = value;
				RaisePropertyChanged(() => LoadingMore);
			}
		}

		private bool _isSortingShown;

		public bool IsSortingShown
		{
			get { return _isSortingShown; }
			set
			{
				_isSortingShown = value;
				SortingIconName = ResourceHelper.IconPath(value ? "sort_active" : "sort");
				RaisePropertyChanged(() => IsSortingShown);
			}
		}

		private string _sortingIconName;

		public string SortingIconName
		{
			get { return _sortingIconName; }
			set
			{
				_sortingIconName = value;
				RaisePropertyChanged(() => SortingIconName);
			}
		}

		private bool _isNoDataTextShown;

		public bool IsNoDataTextShown
		{
			get { return _isNoDataTextShown; }
			set
			{
				_isNoDataTextShown = value;
				RaisePropertyChanged(() => IsNoDataTextShown);
			}
		}

		private bool _isRefreshing;

		public bool IsRefreshing
		{
			get { return _isRefreshing; }
			set
			{
				_isRefreshing = value;
				RaisePropertyChanged(() => IsRefreshing);
			}
		}

		private bool _moveListToTop;

		public bool MoveListToTop
		{
			get { return _moveListToTop; }
			set
			{
				_moveListToTop = value;
				RaisePropertyChanged(() => MoveListToTop);
			}
		}

		private int _lastComplaintItemPosition;

		public int LastComplaintItemPosition
		{
			get { return _lastComplaintItemPosition; }
			set
			{
				_lastComplaintItemPosition = value;
				RaisePropertyChanged(() => LastComplaintItemPosition);
			}
		}

		private int _indexOfItem;

		public string Sorting => Localizer.GetText("Sorting");
		public string NoData => Localizer.GetText("NoData");

		#endregion

		#region Command Methods

		protected override void OnBackCommand()
		{
			base.OnBackCommand();
			ClearSearchResult();
			MasterViewModel.Instance.ExecuteBeforeShowDetailView(typeof(HomeViewModel),
				delegate { ShowViewModel<HomeViewModel>(); });
		}

		private async void OnRefreshCommand()
		{
			if (_networkHelper.IsConnected)
			{
				await _lock.WaitAsync();
				try
				{
					MasterViewModel.Instance.IsIndicatorShown = true;
					IsRefreshing = true;
					DataModel.ComplaintHistory = null;
					ManagementService.GetHistoryComplaints(1, DataModel.HistorySortFilter);
					IsRefreshing = false;
				}
				finally
				{
					_lock.Release();
				}
			}
			else
			{
				MasterViewModel.Instance.IsNoWifiPopupShown = true;
				IsRefreshing = false;
			}
		}

		protected virtual void InitCommands()
		{
			SortingCommand = new MvxCommand(OnSortingCommand);
			SortingOverlayCommand = new MvxCommand(OnSortingOverlayCommand);
			LoadMoreData = new MvxCommand(OnLoadMoreData);
			RefreshCommand = new MvxCommand(OnRefreshCommand);
			CheckNetworkCommand = new MvxCommand(OnCheckNetworkCommand);
		}

		private void OnLeftButtonClicked(object sender, EventArgs e)
		{
			OnBackCommand();
		}

		private void OnLoadMoreData()
		{
			if (DataModel.ComplaintHistory == null) return;
			if (DataModel.ComplaintHistory.Count >= 15)
			{
				MasterViewModel.Instance.IsIndicatorShown = true;
				ManagementService.GetHistoryComplaints(++_page, DataModel.HistorySortFilter);
			}
			else
			{
				MasterViewModel.Instance.IsIndicatorShown = false;
			}
		}

		private void OnCheckNetworkCommand()
		{
			LoadingMore = false;
			if (_networkHelper.IsConnected)
			{
				if (DataModel.ComplaintHistory == null)
				{
					MasterViewModel.Instance.IsIndicatorShown = true;
					ManagementService.GetHistoryComplaints(1, DataModel.HistorySortFilter);
				}
				else
				{
					_page = DataModel.ComplaintHistory.Count / 15;
					FeedbackHistoryItemViewModels = DataModel.ComplaintHistory
						.Select(m => new FeedbackItemViewModel(m, ManagementService, DataModel, ResourceHelper))
						.ToList();
					MasterViewModel.Instance.IsIndicatorShown = false;
				}
			}
			else
			{
				MasterViewModel.Instance.IsNoWifiPopupShown = true;
			}
		}

		private void OnSortingCommand()
		{
			IsSortingShown = !IsSortingShown;
		}

		private void OnSortingOverlayCommand()
		{
			IsSortingShown = false;
		}

		#endregion
	}
}