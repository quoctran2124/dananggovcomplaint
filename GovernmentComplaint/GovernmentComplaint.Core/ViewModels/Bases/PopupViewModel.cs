﻿using System;
using System.Windows.Input;
using GovernmentComplaint.Core.Helpers;
using GovernmentComplaint.Core.Localization;
using GovernmentComplaint.Core.Models;
using GovernmentComplaint.Core.Services;
using MvvmCross.Core.ViewModels;

namespace GovernmentComplaint.Core.ViewModels.Bases
{
	public abstract class PopupViewModel : BaseViewModel
	{
		public EventHandler SaveFeedBackClicked;

        protected PopupViewModel(IManagementService managementService, IDataModel dataModel,
			IResourceHelper resourceHelper)
			: base(managementService, dataModel, resourceHelper)
		{
			InitCommandMethods();
		}

		#region Method

		private void InitCommandMethods()
		{
			_backCommand = new MvxCommand(OnBackCommand);
		}

		protected virtual void OnBackCommand()
		{
		}

		#endregion

		#region Command

		private MvxCommand _backCommand;

		public ICommand BackCommand => _backCommand;

		private ICommand _saveFeedbackCommand;
		public ICommand SaveFeedbackCommand
			=> _saveFeedbackCommand ?? (_saveFeedbackCommand = new MvxCommand(OnSaveFeedbackCommand));

	    private ICommand _sendAnswerFeedback;
        public ICommand SendAnswerFeedback
            => _sendAnswerFeedback ?? (_sendAnswerFeedback = new MvxCommand<AnswerFeedbackContent>(OnSendAnswerFeedbackCommand));
        
        #endregion

        #region Properties

        private bool _isIndicatorShown;

		public bool IsIndicatorShown
		{
			get { return _isIndicatorShown; }
			set
			{
				_isIndicatorShown = value;
				RaisePropertyChanged(() => IsIndicatorShown);
			}
		}

		private string _toastText;

		public string ToastText
		{
			get { return _toastText; }
			set
			{
				_toastText = value;

				IsToastShown = !string.IsNullOrEmpty(value);
				RaisePropertyChanged(() => ToastText);
			}
		}

		private bool _isInfoRequestPopupShown;

		public bool IsInfoRequestPopupShown
		{
			get { return _isInfoRequestPopupShown; }
			set
			{
				_isInfoRequestPopupShown = value;
				RaisePropertyChanged(() => IsInfoRequestPopupShown);
			}
		}

		private bool _isVideoPopupShown;

		public bool IsVideoPopupShown
		{
			get { return _isVideoPopupShown; }
			set
			{
				_isVideoPopupShown = value;
				RaisePropertyChanged(() => IsVideoPopupShown);
			}
		}


		private bool _isCancelPopupShown;

		public bool IsCancelPopupShown
		{
			get { return _isCancelPopupShown; }
			set
			{
				_isCancelPopupShown = value;
				RaisePropertyChanged(() => IsCancelPopupShown);
			}
		}

		private bool _isCancelCommentPopupShown;

		public bool IsCancelCommentPopupShown
		{
			get { return _isCancelCommentPopupShown; }
			set
			{
				_isCancelCommentPopupShown = value;
				RaisePropertyChanged(() => IsCancelCommentPopupShown);
			}
		}

		private bool _isLogoutPopupShown;

		public bool IsLogoutPopupShown
		{
			get { return _isLogoutPopupShown; }
			set
			{
				_isLogoutPopupShown = value;
				RaisePropertyChanged(() => IsLogoutPopupShown);
			}
		}

		private bool _isDropdownPopupShown;

		public bool IsDropdownPopupShown
		{
			get { return _isDropdownPopupShown; }
			set
			{
				_isDropdownPopupShown = value;
				RaisePropertyChanged(() => IsDropdownPopupShown);
			}
		}

		private bool _isAttachedImagesPopupShown;

		public bool IsAttachedImagesPopupShown
		{
			get { return _isAttachedImagesPopupShown; }
			set
			{
				_isAttachedImagesPopupShown = value;
				RaisePropertyChanged(() => IsAttachedImagesPopupShown);
			}
		}

		private bool _isFail24HPopupShown;

		public bool IsFail24HPopupShown
		{
			get { return _isFail24HPopupShown; }
			set
			{
				_isFail24HPopupShown = value;
				RaisePropertyChanged(() => IsFail24HPopupShown);
			}
		}

	    private bool _isNoWifiPopupShown;

	    public bool IsNoWifiPopupShown
        {
	        get { return _isNoWifiPopupShown; }
	        set
	        {
	            _isNoWifiPopupShown = value;
	            RaisePropertyChanged(() => IsNoWifiPopupShown);
	        }
	    }


        
        private bool _isErrorImagesShown;

		public bool IsErrorImagesShown
		{
			get { return _isErrorImagesShown; }
			set
			{
				_isErrorImagesShown = value;
				RaisePropertyChanged(() => IsErrorImagesShown);
			}
		}

		private bool _isNoCommentPopupShown;

		public bool IsNoCommentPopupShown
		{
			get { return _isNoCommentPopupShown; }
			set
			{
				_isNoCommentPopupShown = value;
				RaisePropertyChanged(() => IsNoCommentPopupShown);
			}
		}

		private bool _isConnectionErrorPopupShown;

		public bool IsConnectionErrorPopupShown
		{
			get { return _isConnectionErrorPopupShown; }
			set
			{
				_isConnectionErrorPopupShown = value;
				RaisePropertyChanged(() => IsConnectionErrorPopupShown);
			}
		}

		private bool _isRatingPopupShown;

		public bool IsRatingPopupShown
		{
			get { return _isRatingPopupShown; }
			set
			{
				_isRatingPopupShown = value;
				RaisePropertyChanged(() => IsRatingPopupShown);
			}
		}

	    private bool _isAnswerFeedbackPopupShown;

	    public bool IsAnswerFeedbackPopupShown
        {
	        get { return _isAnswerFeedbackPopupShown; }
	        set
	        {
	            _isAnswerFeedbackPopupShown = value;
	            RaisePropertyChanged(() => IsAnswerFeedbackPopupShown);
	        }
	    }
        
        private bool _isResendFeedbackPopupShown;

		public bool IsResendFeedbackPopupShown
		{
			get { return _isResendFeedbackPopupShown; }
			set
			{
				_isResendFeedbackPopupShown = value;
				RaisePropertyChanged(() => IsResendFeedbackPopupShown);
			}
		}

		private bool _isToastShown;

		public bool IsToastShown
		{
			get { return _isToastShown; }
			set
			{
				_isToastShown = value;
				RaisePropertyChanged(() => IsToastShown);
			}
		}

		private bool _shouldMoveToBackground;

		public bool ShouldMoveToBackground
		{
			get { return _shouldMoveToBackground; }
			set
			{
				_shouldMoveToBackground = true;
				RaisePropertyChanged(() => ShouldMoveToBackground);
			}
		}

		private bool _isPopupShown;

		public bool IsPopupShown
		{
			get { return _isPopupShown; }
			set
			{
				_isPopupShown = value;
				MasterViewModel.Instance.IsIndicatorShown = false;
				RaisePropertyChanged(() => IsPopupShown);
			}
		}


		private bool _isPopupOfflineOptionsShown;
		public bool IsPopupOfflineOptionsShown
		{
			get { return _isPopupOfflineOptionsShown; }
			set
			{
				_isPopupOfflineOptionsShown = value;
				RaisePropertyChanged(() => IsPopupOfflineOptionsShown);
			}
		}

		private void OnSaveFeedbackCommand()
		{
			SaveFeedBackClicked?.Invoke(this, null);
		}

	    private void OnSendAnswerFeedbackCommand(AnswerFeedbackContent content)
	    {
            AnswerFeedbackContent answerFeedbackContent = new AnswerFeedbackContent()
            {
                SatisfySelected = content.SatisfySelected,
                SlowResponseSelected = content.SlowResponseSelected,
                UnSuitAnswerSelected = content.UnSuitAnswerSelected
            };

        }

        #endregion
    }
}