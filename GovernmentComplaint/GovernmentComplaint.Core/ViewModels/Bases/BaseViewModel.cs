﻿using System.Net;
using System.Threading.Tasks;
using System.Windows.Input;
using GovernmentComplaint.Core.Enums;
using GovernmentComplaint.Core.Helpers;
using GovernmentComplaint.Core.Localization;
using GovernmentComplaint.Core.Models;
using GovernmentComplaint.Core.Services;
using GovernmentComplaint.Core.ViewModels.ItemViewModels;
using MvvmCross.Core.ViewModels;

namespace GovernmentComplaint.Core.ViewModels.Bases
{
	public abstract class BaseViewModel : MvxViewModel
	{
		protected BaseViewModel(IManagementService managementService, IDataModel dataModel,
			IResourceHelper resourceHelper)
		{
			ManagementService = managementService;
			DataModel = dataModel;
			ResourceHelper = resourceHelper;
		}

		protected BaseViewModel(IManagementService managementService)
		{
			ManagementService = managementService;
		}

		protected BaseViewModel()
		{
		}

		protected void OnDataReceived(WsCommand command, HttpStatusCode code, NetworkStatus networkstatus)
		{
			switch (networkstatus)
			{
				// TODO: show error popup
				case NetworkStatus.Exception:
					MasterViewModel.Instance.IsIndicatorShown = false;
					MasterViewModel.Instance.IsConnectionErrorPopupShown = true;
					break;
				case NetworkStatus.NoWifi:
					MasterViewModel.Instance.IsIndicatorShown = false;
					break;
				case NetworkStatus.Timeout:
					MasterViewModel.Instance.IsIndicatorShown = false;
					MasterViewModel.Instance.IsConnectionErrorPopupShown = true;
					break;
				default:
					HandleDataReceived(command, code);
					break;
			}
		}

		protected virtual void HandleDataReceived(WsCommand command, HttpStatusCode code)
		{
		}

		protected abstract void UnregisteredEvent();

		public void Close(MvxViewModel viewModel)
		{
			base.Close(viewModel);
		}

		#region Properties

		protected IManagementService ManagementService { get; private set; }
		protected IDataModel DataModel { get; private set; }
		protected IResourceHelper ResourceHelper { get; private set; }
		public string Confirm => Localizer.GetText("Confirm");
		public string CancelPopup => Localizer.GetText("Cancel");
		public string LoginPopupText => Localizer.GetText("LoginText");
		public string LoginPopupInstruction => Localizer.GetText("LoginPopupInstruction");
		public string VideoInstruction => Localizer.GetText("VideoInstruction");
		public string ConfirmInstruction1 => Localizer.GetText("ConfirmInstruction1");
		public string ConfirmInstruction2 => Localizer.GetText("ConfirmInstruction2");
		public string ConfirmCancel => Localizer.GetText("ConfirmCancel");
		public string VideoTextHint => Localizer.GetText("VideoTextHint");
		public string InfoRequestText => Localizer.GetText("InfoRequestText");
		public string RatingText => Localizer.GetText("RatingCount");
		public string InformText => Localizer.GetText("InformText");
	    public string AnswerFeedback => Localizer.GetText("AnswerFeedback");
	    public string SatisfyText => Localizer.GetText("SatisfyText");
	    public string UnsatisfyText => Localizer.GetText("UnsatisfyText");
	    public string SlowResponse => Localizer.GetText("SlowResponse"); 
	    public string UnsuitAnswer => Localizer.GetText("UnsuitAnswer");
	    public string AnswerFeedbackErrorMessage => Localizer.GetText("AnswerFeedbackErrorMessage");


        public string Fail24HInstruction { get; set; } = Localizer.GetText("Fail24hInstruction");
		public string NoWifiInstruction { get; set; } = Localizer.GetText("NoWifi");
		public string ConnectionErrorInstruction { get; set; } = Localizer.GetText("TimeOut");
		public string ErrorImagesInstruction { get; set; } = Localizer.GetText("ErrorImagesInstruction");
		public string NoCommentInstruction { get; set; } = Localizer.GetText("NoCommentInstruction");
		public string ResendFeedbackInstruction { get; set; } = Localizer.GetText("ResendFeedbackInstruction");
		public string TryAgain { get; set; } = Localizer.GetText("TryAgain");
		public string ConfirmLogout { get; set; } = Localizer.GetText("ConfirmLogout");
		public string OfflineInstruction => Localizer.GetText("OfflineInstruction");
		public string GoToWifiSetting => Localizer.GetText("GoToWifiSetting");
		public string SaveFeedBack => Localizer.GetText("SaveFeedBack");

		#endregion

		private ICommand _cancelConfirmCommand;
		public ICommand AddUserInforCancelConfirmCommand => _cancelConfirmCommand ?? (_cancelConfirmCommand = new MvxCommand(OnAddUserInfoCancelConfirmed));

		private void OnAddUserInfoCancelConfirmed()
		{
			MasterViewModel.Instance.IsCancelPopupShown = false;
			DataModel.NewComplaintContent = null;
			if (DataModel.PreviousPage != null && (DataModel.PreviousPage.Equals(PreviousPage.ComplaintDetail) ||
			    DataModel.PreviousPage.Equals(PreviousPage.CreateNewComplaint)))
			{
				MasterViewModel.Instance.ExecuteBeforeShowDetailView(typeof (HomeViewModel),
					delegate
					{
						ShowViewModel<HomeViewModel>(new
						{
							lastComplaintItemId = 0,
							previousPage = DataModel.PreviousPageString
						});
					});
				MasterViewModel.Instance.SidebarItemViewModels[0].ItemStatus = ItemStatus.Inactive;
				MasterViewModel.Instance.SidebarItemViewModels[0].LabelStatus = LabelStatus.Special;
				SidebarItemViewModel.SelectedItem = MasterViewModel.Instance.SidebarItemViewModels[1];
				SidebarItemViewModel.SelectedItem.ItemStatus = ItemStatus.Active;
				SidebarItemViewModel.SelectedItem.LabelStatus = LabelStatus.Active;
			}
			else
			{
				MasterViewModel.Instance.DeleteAllSavedFeedback();
				MasterViewModel.Instance.ShowLastViewModel();
			}
		
		}

		protected void ClearHomePageData()
		{
			DataModel.CatFilter = 0;
			DataModel.SortFilter = 0;
			DataModel.Keyword = string.Empty;
			DataModel.DepartmentId = 0;
			DataModel.ComplaintContents = null;
		}

		protected async void ShowToast(string content)
		{
			MasterViewModel.Instance.ToastText = content;
			MasterViewModel.Instance.IsToastShown = true;
			await Task.Delay(ConstVariable.TimeToToastShow);
			MasterViewModel.Instance.IsToastShown = false;
		}
	}
}