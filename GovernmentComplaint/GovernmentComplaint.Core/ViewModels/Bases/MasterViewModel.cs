﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Windows.Input;
using GovernmentComplaint.Core.Enums;
using GovernmentComplaint.Core.Helpers;
using GovernmentComplaint.Core.Localization;
using GovernmentComplaint.Core.Models;
using GovernmentComplaint.Core.Services;
using GovernmentComplaint.Core.ViewModels.ItemViewModels;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform;
using MvvmCross.Plugins.PhoneCall;

namespace GovernmentComplaint.Core.ViewModels.Bases
{
	public delegate void MasterEventHandler(Type type, Action callback);

	public partial class MasterViewModel : PopupViewModel
	{
		private NewComplaintContent _lastResendFeedback;
		private List<NewComplaintContent> _savedFeedbacks;
		private bool _wasSecondLoginShown;

		#region UI Text

		public string CancelText => Localizer.GetText("Cancel");
		public string SearchTextHint => Localizer.GetText("SearchTextHint");
		public string MainAction => Localizer.GetText("MainAction");
		public string Home => Localizer.GetText("Home");
		public string Notification => Localizer.GetText("Notification");
		public string History => Localizer.GetText("History");
		public string Intro => Localizer.GetText("Intro");
		public string Logout => Localizer.GetText("Logout");
		public string SearchButtonText => Localizer.GetText("SearchButtonText");
		public string SearchCancelButtonText => Localizer.GetText("SearchCancelButtonText");
		public string Facebook => Localizer.GetText("Facebook");
		public string Google => Localizer.GetText("Google");
		public string ChooseAuth => Localizer.GetText("ChooseAuth");
		public string WelcomeUserText => Localizer.GetText("WelcomeUserText");
		public string TotalFeedbackStaticText => Localizer.GetText("TotalReplyStaticText");
		public string TotalCommentStaticText => Localizer.GetText("TotalCommentStaticText");
		public string SpinnerHeader => Localizer.GetText("SpinnerHeader");
		public string EmailLinkHint => Localizer.GetText("EmailLinkHint");
		public string PhoneLinkHint => Localizer.GetText("PhoneLinkHint");

		public string MainTitle => Localizer.GetText("MainTitle");
		#endregion

		#region Properties

		private readonly IBadgeHelper _badgeHelper;
		private readonly ISocialHelper _socialHelper;
		private readonly INetworkHelper _networkHelper;

		protected readonly IAppDataLoader _appDataLoader;

		private Enum _loginType;
		public UserInfo LoggedInUser;
		public static MasterViewModel Instance { get; private set; }
		public event EventHandler LeftButtonClicked;
		public event EventHandler RightButtonClicked;
		public event EventHandler BackButtonClicked;
		public event MasterEventHandler ShowDetailView;
		public CreateNewComplaintViewModel CreateNewComplaintViewModel { get; set; }
		public HomeViewModel HomeViewModel { get; set; }
		public ComplaintDetailViewModel ComplaintDetailViewModel { get; set; }
		public SecondLoginScreenViewModel SecondLoginScreenViewModel { get; set; }
		public HistoryViewModel HistoryViewModel { get; set; }
		public SidebarMenuType MenuType;

		private List<DepartmentItemViewModel> _departmentItemViewModels;

		public List<DepartmentItemViewModel> DepartmentItemViewModels
		{
			get { return _departmentItemViewModels; }
			set
			{
				_departmentItemViewModels = value;
				RaisePropertyChanged(() => DepartmentItemViewModels);
			}
		}

		private bool _isSidebarShown;

		public bool IsSidebarShown
		{
			get { return _isSidebarShown; }
			set
			{
				if (!value)
					ManagementService.StopTimer();
				_isSidebarShown = value;
				RaisePropertyChanged(() => IsSidebarShown);
			}
		}

		private List<SidebarItemViewModel> _sidebarItemViewModels;

		public List<SidebarItemViewModel> SidebarItemViewModels
		{
			get { return _sidebarItemViewModels; }
			set { SetProperty(ref _sidebarItemViewModels, value); }
		}

		private string _attachedImageUrl;

		public string AttachedImageUrl
		{
			get { return _attachedImageUrl; }
			set
			{
				_attachedImageUrl = value;
				RaisePropertyChanged(() => AttachedImageUrl);
			}
		}

		private SidebarItemViewModel _sidebarItem;

		public SidebarItemViewModel SidebarItem
		{
			get { return _sidebarItem; }
			set
			{
				_sidebarItem = value;
				RaisePropertyChanged(() => SidebarItem);
			}
		}

		private bool _isDepartmentShown;

		public bool IsDepartmentShown
		{
			get { return _isDepartmentShown; }
			set
			{
				_isDepartmentShown = value;
				RaisePropertyChanged(() => IsDepartmentShown);
			}
		}

		private bool _isOverlayShown;

		public bool IsOverlayShown
		{
			get { return _isOverlayShown; }
			set
			{
				_isOverlayShown = value;
				RaisePropertyChanged(() => IsOverlayShown);
			}
		}

		private string _searchText = "";

		public string SearchText
		{
			get { return _searchText; }
			set
			{
				_searchText = value;
				RaisePropertyChanged(() => SearchText);
			}
		}

		private string _titleBar;

		public string TitleBar
		{
			get { return _titleBar; }
			set
			{
				_titleBar = value;
				RaisePropertyChanged(() => TitleBar);
			}
		}

		private HeaderButtonType _topLeftButtonType;

		public HeaderButtonType TopLeftButtonType
		{
			get { return _topLeftButtonType; }
			set { SetProperty(ref _topLeftButtonType, value); }
		}

		private HeaderButtonType _topRightButtonType;

		public HeaderButtonType TopRightButtonType
		{
			get { return _topRightButtonType; }
			set { SetProperty(ref _topRightButtonType, value); }
		}

		private bool _isSearchBoxShown;

		public bool IsSearchBoxShown
		{
			get { return _isSearchBoxShown; }
			set
			{
				_isSearchBoxShown = value;
				RaisePropertyChanged(() => IsSearchBoxShown);
			}
		}

		private bool _isLeftButtonShown;

		public bool IsLeftButtonShown
		{
			get { return _isLeftButtonShown; }
			set
			{
				_isLeftButtonShown = value;
				RaisePropertyChanged(() => IsLeftButtonShown);
			}
		}

		private bool _isRightButtonShown;

		public bool IsRightButtonShown
		{
			get { return _isLeftButtonShown; }
			set
			{
				_isRightButtonShown = value;
				RaisePropertyChanged(() => IsRightButtonShown);
			}
		}

		private bool _isLoggedIn;

		public bool IsLoggedIn
		{
			get { return _isLoggedIn; }
			set
			{
				_isLoggedIn = value;
				_wasSecondLoginShown = value;
				RaisePropertyChanged(() => IsLoggedIn);
			}
		}

		private string _welcomeUserName;

		public string WelcomeUserName
		{
			get { return _welcomeUserName; }
			set
			{
				_welcomeUserName = value;
				RaisePropertyChanged(() => WelcomeUserName);
			}
		}

		private string _totalFeedbackText;

		public string TotalFeedbackText
		{
			get { return _totalFeedbackText; }
			set
			{
				_totalFeedbackText = value;
				RaisePropertyChanged(() => TotalFeedbackText);
			}
		}

		private string _totalCommentText;

		public string TotalCommentText
		{
			get { return _totalCommentText; }
			set
			{
				_totalCommentText = value;
				RaisePropertyChanged(() => TotalCommentText);
			}
		}

		private string _profilePictureUrl;

		public string ProfilePictureUrl
		{
			get { return _profilePictureUrl; }
			set
			{
				_profilePictureUrl = value;
				RaisePropertyChanged(() => ProfilePictureUrl);
			}
		}

		private string _selectedDepartment;

		public string SelectedDepartment
		{
			get { return _selectedDepartment; }
			set
			{
				_selectedDepartment = value;
				RaisePropertyChanged(() => SelectedDepartment);
			}
		}

		private int _selectedDepartmentId;

		public int SelectedDepartmentId
		{
			get { return _selectedDepartmentId; }
			set
			{
				_selectedDepartmentId = value;
				RaisePropertyChanged(() => SelectedDepartmentId);
			}
		}

		private string _userEmail;

		public string UserEmail
		{
			get { return _userEmail; }
			set
			{
				_userEmail = value;
				RaisePropertyChanged(() => UserEmail);
			}
		}

		private string _userPhoneNumber;

		public string UserPhoneNumber
		{
			get { return _userPhoneNumber; }
			set
			{
				_userPhoneNumber = value;
				RaisePropertyChanged(() => UserPhoneNumber);
			}
		}

		private string _emailVerifyMessage;

		public string VerifyMessage
		{
			get { return _emailVerifyMessage; }
			set
			{
				_emailVerifyMessage = value;
				RaisePropertyChanged(() => VerifyMessage);
			}
		}

		private bool _isEmailLinkWrong;

		public bool IsEmailLinkWrong
		{
			get { return _isEmailLinkWrong; }
			set
			{
				_isEmailLinkWrong = value;
				RaisePropertyChanged(() => IsEmailLinkWrong);
			}
		}

		private double _rating;

		public double Rating
		{
			get { return _rating; }
			set
			{
				_rating = value;
				RaisePropertyChanged(() => Rating);
			}
		}
	    
        #endregion

        #region Commands

        public MvxCommand<UserInfo> LoginCommand { get; private set; }

		public ICommand OverlaySidebarClickCommand { get; private set; }

		public ICommand OverlaySearchBoxClickCommand { get; private set; }

		public ICommand LeftButtonClickCommand { get; private set; }

		public ICommand OnDepartmentDropdownCommand { get; private set; }

		public ICommand OnCloseDepartmentCommand { get; private set; }

		public ICommand OnDepartmentCommand { get; private set; }

		public ICommand CancelSearchButtonClickCommand { get; private set; }

		public ICommand SearchButtonClickCommand { get; private set; }

		public ICommand StopTimerCommand { get; private set; }

		public ICommand EmailTouchCommand { get; private set; }

		public ICommand RightButtonClickCommand { get; private set; }

		public ICommand OverlayClickCommand { get; private set; }

		public ICommand ConfirmLogoutCommand { get; private set; }

		private ICommand _cancelLogoutCommand;

		public ICommand CancelLogoutCommand
			=> _cancelLogoutCommand ?? (_cancelLogoutCommand = new MvxCommand(OnCancelLogoutCommand));

		private ICommand _callHotlineCommand;

		public ICommand CallHotlineCommand
			=> _callHotlineCommand ?? (_callHotlineCommand = new MvxCommand(OnCallHotline));

		private ICommand _networkConnectedCommand;
		public ICommand NetworkConnectedCommand
			=> _networkConnectedCommand ?? (_networkConnectedCommand = new MvxCommand(OnNetworkConnectedCommand));

		private void OnCancelLogoutCommand()
		{
			IsLogoutPopupShown = false;
			switch (Instance.TitleBar)
			{
				case "GỬI GÓP Ý, PHẢN ÁNH":
					SidebarItemViewModels[0].ItemStatus = ItemStatus.Active;
					SidebarItemViewModels[0].LabelStatus = LabelStatus.Active;

					break;
				case "THÔNG BÁO":
					SidebarItemViewModels[2].ItemStatus = ItemStatus.Active;
					SidebarItemViewModels[2].LabelStatus = LabelStatus.Active;

					break;
				case "LỊCH SỬ GÓP Ý":
					SidebarItemViewModels[3].ItemStatus = ItemStatus.Active;
					SidebarItemViewModels[3].LabelStatus = LabelStatus.Active;

					break;
				case "GIỚI THIỆU":
					SidebarItemViewModels[4].ItemStatus = ItemStatus.Active;
					SidebarItemViewModels[4].LabelStatus = LabelStatus.Active;
					break;
				default:
					SidebarItemViewModels[1].ItemStatus = ItemStatus.Active;
					SidebarItemViewModels[1].LabelStatus = LabelStatus.Active;
					break;
			}

			SidebarItemViewModels[5].ItemStatus = ItemStatus.Inactive;
			SidebarItemViewModels[5].LabelStatus = LabelStatus.Inactive;
		}

		#endregion

		public MasterViewModel(IManagementService managementService, IDataModel dataModel,
			IResourceHelper resourceHelper, ISocialHelper socialHelper, INetworkHelper networkHelper, IAppDataLoader appDataLoader)
			: base(managementService, dataModel, resourceHelper)
		{

			_badgeHelper = Mvx.Resolve<IBadgeHelper>();
			_networkHelper = networkHelper;
			_networkHelper.NetworkConnected += OnNetworkConnected;

			_appDataLoader = appDataLoader;

			Instance = this;
			_socialHelper = socialHelper;
			ManagementService.CountNotifications();
			dataModel.DataReceived += OnDataReceived;
			ExecutionHelper.ExecuteWithOffset(
				() => { ExecuteBeforeShowDetailView(typeof(HomeViewModel), delegate { ShowViewModel<HomeViewModel>(); }); },
				500);
			InitMenuItems();
			InitCommands();
			IsSearchBoxShown = false;
			IsSidebarShown = false;
			SelectedDepartment = SpinnerHeader;
			RightButtonClicked += OnRightButtonClick;
			IsLoggedIn = false;
			dataModel.CountNotification = 0;
			dataModel.GetUserInfoNetworkStatus = NetworkStatus.Null;
		}

		#region Methods

		private void InitCommands()
		{
			RightButtonClickCommand = new MvxCommand(OnRightButtonClicked);
			OverlayClickCommand = new MvxCommand(OnOverlayClick);
			LeftButtonClickCommand = new MvxCommand(OnLeftButtonClicked);
			OverlaySidebarClickCommand = new MvxCommand(OnOverlaySidebarClicked);
			OverlaySearchBoxClickCommand = new MvxCommand(OnOverlaySearchBoxClicked);
			ConfirmLogoutCommand = new MvxCommand(OnLogoutConfirmClicked);
			LoginCommand = new MvxCommand<UserInfo>(LoginSuccess);
			OnDepartmentDropdownCommand = new MvxCommand(OnDepartmentDropdown);
			OnCloseDepartmentCommand = new MvxCommand(OnOnCloseDepartmentCommand);
			OnDepartmentCommand = new MvxCommand(OnOnDepartmentCommand);
			CancelSearchButtonClickCommand = new MvxCommand(OnCancelSearchButtonClickCommand);
			SearchButtonClickCommand = new MvxCommand(OnSearchButtonClickCommand);
			StopTimerCommand = new MvxCommand(OnStopTimerCommand);
			EmailTouchCommand = new MvxCommand(OnEmailTouchCommand);
		}

		/// <summary>
		/// Handle datareceived in case HttpStatusCode is OK 
		/// </summary>
		/// <param name="command"></param>
		/// <param name="code"></param>
		protected override void HandleDataReceived(WsCommand command, HttpStatusCode code)
		{
			base.HandleDataReceived(command, code);
			if (code == HttpStatusCode.OK)
			{
				switch (command)
				{
					case WsCommand.GetDepartments:
						DepartmentItemViewModels = DataModel.Departments
							.Select(m => new DepartmentItemViewModel(m, ManagementService, DataModel, ResourceHelper))
							.ToList();
						break;
					case WsCommand.GetUserInfo:
						ManagementService.CountNotifications();
						if (!IsSidebarShown)
						{
							if (DataModel.PreviousPage != null &&
								(DataModel.PreviousPage.Equals(PreviousPage.ComplaintDetail) ||
								 DataModel.PreviousPage.Equals(PreviousPage.CreateNewComplaint) || DataModel.PreviousPage.Equals(PreviousPage.HomeViewModel)))
							{
								//if (DataModel.UserInfo == null)
								//{
								//	ExecuteBeforeShowDetailView(typeof(AddPersonalInfoViewModel),
								//		delegate { ShowViewModel<AddPersonalInfoViewModel>(); });
								//}
								//else
								if (DataModel.UserInfo != null)
								{
									if (DataModel.PreviousPage.Equals(PreviousPage.CreateNewComplaint))
									{
										ExecuteBeforeShowDetailView(typeof(CreateNewComplaintViewModel),
											delegate { ShowViewModel<CreateNewComplaintViewModel>(); });
									}
									else
									{
										if (DataModel.PreviousPage.Equals(PreviousPage.ComplaintDetail))
											ExecuteBeforeShowDetailView(typeof(ComplaintDetailViewModel),
												delegate
												{
													ShowViewModel<ComplaintDetailViewModel>(new
													{
														id = DataModel.ComplaintContent.Id,
														pageType = DataModel.PreviousPageString,
														searchText = DataModel.Keyword
													});
												});
									}
								}
							}
						}

						if (DataModel.UserInfo != null)
						{
							TotalCommentText = DataModel.UserInfo.TotalComment;
							TotalFeedbackText = DataModel.UserInfo.TotalComplaint;
						}

						if (IsLoggedIn)
						{
							if (DataModel.UserInfo != null)
							{
								ResendFeedback();
							}
							else
							{
								PromptGetUserInfo();
							}
						}

						break;

					case WsCommand.OutOfData:
						TryGetUserInfo();
						break;
					case WsCommand.PostComplaint:
						IsIndicatorShown = false;
						if (DataModel.UserInfo.TotalComplaint != null)
						{
							var newTotalFeedback = Int32.Parse(DataModel.UserInfo.TotalComplaint) + 1;
							DataModel.UserInfo.TotalComplaint = newTotalFeedback.ToString();
						}
						else
						{
							DataModel.UserInfo.TotalComplaint = "1";
						}
						TotalFeedbackText = DataModel.UserInfo.TotalComplaint;


						// WSCommand raised by resending saved feedback
						// send the next one in saved feedback list.
						if (_appDataLoader.AppData.NumberOfOfflineFeedback > 0)
						{
							OnResendSuccessful();
						}
						else
						{
							ExecuteBeforeShowDetailView(typeof(HomeViewModel),
							delegate
							{
								ShowViewModel<HomeViewModel>(new
								{
									lastComplaintItemId = 0,
									previousPage = DataModel.PreviousPageString
								});
							});
							DataModel.NewComplaintContent = null;

							ShowToast(Localizer.GetText("SendComplaintSuccessfully"));
						}


						break;
					case WsCommand.SaveUser:
						ResendFeedback();
						break;
				}
			}
			else
			{
				IsIndicatorShown = false;
				IsConnectionErrorPopupShown = true;
			}
		}

		private void InitMenuItems()
		{
			var menuItemViewModels = new List<SidebarItemViewModel>
			{
				new SidebarItemViewModel(SidebarMenuType.NewComplaint, ManagementService, DataModel, ResourceHelper),
				new SidebarItemViewModel(SidebarMenuType.Home, ManagementService, DataModel, ResourceHelper),
				new SidebarItemViewModel(SidebarMenuType.Intro, ManagementService, DataModel, ResourceHelper),
			    new SidebarItemViewModel(SidebarMenuType.Dashboard, ManagementService, DataModel, ResourceHelper),
            };
			if (IsLoggedIn)
			{
				menuItemViewModels.Add(new SidebarItemViewModel(SidebarMenuType.Notification, ManagementService,
					DataModel, ResourceHelper));
				menuItemViewModels.Add(new SidebarItemViewModel(SidebarMenuType.History, ManagementService, DataModel, ResourceHelper));
				menuItemViewModels.Add(new SidebarItemViewModel(SidebarMenuType.Logout, ManagementService, DataModel, ResourceHelper));
			}
			menuItemViewModels.Sort(new[]
			{
				SidebarMenuType.NewComplaint,
				SidebarMenuType.Home,
				SidebarMenuType.Notification,
			    SidebarMenuType.Dashboard,
                SidebarMenuType.History,
				SidebarMenuType.Intro,
				SidebarMenuType.Logout
			});

			SidebarItemViewModels = new List<SidebarItemViewModel>(menuItemViewModels);
		}

		public void ExecuteBeforeShowDetailView(Type type, Action callback, bool isNeededToCloseMenu = true)
		{
			if (type != typeof(SecondLoginScreenViewModel) && type != typeof(AddPersonalInfoViewModel))
			{
				DataModel.LastViewModelType = type;
			}

			if (ShowDetailView != null)
			{
				ShowDetailView(type, callback);
			}
			else
			{
				callback();
			}

		}

		#endregion

		#region Command Methods

		private void OnEmailTouchCommand()
		{
			VerifyMessage = "";
		}

		private void OnStopTimerCommand()
		{
			if (!IsSidebarShown)
				ManagementService.StopTimer();
		}

		private void OnOverlayClick()
		{
			IsSidebarShown = false;
			IsSearchBoxShown = false;
			IsOverlayShown = false;
			IsDropdownPopupShown = false;

			if (TopLeftButtonType == HeaderButtonType.Logo)
				TopLeftButtonType = HeaderButtonType.Logo;
			else
				TopLeftButtonType = TopLeftButtonType == HeaderButtonType.Back
					? HeaderButtonType.Back
					: HeaderButtonType.Search;

			TopRightButtonType = HeaderButtonType.Menu;
		}

		private void OnOverlaySidebarClicked()
		{
			if (IsSidebarShown)
			{
				ManagementService.StopTimer();
				IsSidebarShown = false;
			}

			if (TopLeftButtonType == HeaderButtonType.Logo)
				TopLeftButtonType = HeaderButtonType.Logo;
			else
				TopLeftButtonType = TopLeftButtonType == HeaderButtonType.Back
					? HeaderButtonType.Back
					: HeaderButtonType.Search;

			TopRightButtonType = HeaderButtonType.Menu;
		}

		private void OnOverlaySearchBoxClicked()
		{
			if (IsSearchBoxShown)
				IsSearchBoxShown = false;

			if (TopLeftButtonType == HeaderButtonType.Logo)
				TopLeftButtonType = HeaderButtonType.Logo;
			else
				TopLeftButtonType = TopLeftButtonType == HeaderButtonType.Back
					? HeaderButtonType.Back
					: HeaderButtonType.Search;

			TopRightButtonType = HeaderButtonType.Menu;
		}

		public void OnLeftButtonClicked()
		{
			LeftButtonClicked?.Invoke(this, null);
		}

		public void OnRightButtonClicked()
		{
			RightButtonClicked?.Invoke(this, null);
		}

		private void OnSearchButtonClickCommand()
		{
			if (_networkHelper.IsConnected)
			{
				if (!string.IsNullOrEmpty(SearchText) && !string.IsNullOrWhiteSpace(SearchText) ||
					!string.Equals(SelectedDepartment, SpinnerHeader))
				{
					DataModel.PreviousPageString = Localizer.GetText("SearchTitle");
					HomeViewModel.MoveListToTop = true;
					IsSearchBoxShown = false;
					IsOverlayShown = false;
					IsIndicatorShown = true;
					TopLeftButtonType = HeaderButtonType.Search;
					DataModel.ComplaintContents = null;
					DataModel.CatFilter = 0;
					DataModel.SortFilter = 0;
					DataModel.Keyword = SearchText;
					if (SelectedDepartment == SpinnerHeader)
						SelectedDepartmentId = 0;
					DataModel.DepartmentId = SelectedDepartmentId;
					ManagementService.GetComplaints(1, DataModel.CatFilter, DataModel.SortFilter, SearchText,
						SelectedDepartmentId);
				}
			}
			else
			{
				IsNoWifiPopupShown = true;
			}
		}

		private void OnCancelSearchButtonClickCommand()
		{
			Instance.IsSearchBoxShown = false;
			Instance.IsOverlayShown = false;
			TopLeftButtonType = HeaderButtonType.Search;
			SearchText = null;
			SelectedDepartmentId = 0;
			Instance.SelectedDepartment = SpinnerHeader;
		}

		public void OnRightButtonClick(object sender, EventArgs e)
		{
			IsSidebarShown = !IsSidebarShown;
			IsOverlayShown = IsSidebarShown;
			IsSearchBoxShown = false;

			if (TopLeftButtonType == HeaderButtonType.Logo)
				TopLeftButtonType = HeaderButtonType.Logo;
			else
				TopLeftButtonType = TopLeftButtonType == HeaderButtonType.Back
					? HeaderButtonType.Back
					: HeaderButtonType.Search;

			TopRightButtonType = TopRightButtonType == HeaderButtonType.Menu
				? HeaderButtonType.MenuActive
				: HeaderButtonType.Menu;
			if (IsSidebarShown && IsLoggedIn)
				ManagementService.CountNotifications();
		}

		public void OnDepartmentDropdown()
		{
			IsDropdownPopupShown = true;
		}

		public void OnOnCloseDepartmentCommand()
		{
			IsDropdownPopupShown = false;
		}

		public void OnOnDepartmentCommand()
		{
			IsDropdownPopupShown = false;
			SelectedDepartment = SpinnerHeader;
		}

		public void InitButtonOnMainView()
		{
			IsLeftButtonShown = true;
			IsRightButtonShown = true;
			TopLeftButtonType = HeaderButtonType.Search;
			TopRightButtonType = HeaderButtonType.Menu;
		}

		public void InitButtonOnDetailView()
		{
			IsLeftButtonShown = true;
			IsRightButtonShown = true;
			TopLeftButtonType = HeaderButtonType.Back;
			TopRightButtonType = HeaderButtonType.Menu;
		}

		public void InitButtonOnProcessingDetailView()
		{
			IsLeftButtonShown = true;
			IsRightButtonShown = true;
			TopLeftButtonType = HeaderButtonType.Logo;
			TopRightButtonType = HeaderButtonType.Menu;
		}

		protected override void OnBackCommand()
		{
			BackButtonClicked?.Invoke(this, EventArgs.Empty);
		}

		public void LoginSuccess(UserInfo user)
		{
			DataModel.DidTryLogin = true;
			IsLoggedIn = true;
			InitMenuItems();

			if (TitleBar != null && TitleBar.Equals("ĐĂNG NHẬP") && IsLeftButtonShown)
			{
				OnBackCommand();
			}

			switch (Instance.TitleBar)
			{
				case "GỬI GÓP Ý, PHẢN ÁNH":
					HighLightMenuItemAtIndex(0);
					break;
				case "THÔNG BÁO":
					HighLightMenuItemAtIndex(2);

					break;
				case "LỊCH SỬ GÓP Ý":
					HighLightMenuItemAtIndex(3);

					break;
				case "GIỚI THIỆU":
					HighLightMenuItemAtIndex(4);
					break;
				default:
					// highlight home
					SidebarItemViewModel.SelectedItem = SidebarItemViewModels[1];
					SidebarItemViewModel.SelectedItem.ItemStatus = ItemStatus.Active;
					SidebarItemViewModel.SelectedItem.LabelStatus = LabelStatus.Active;
					break;
			}

			TotalCommentText = "0";
			TotalFeedbackText = "0";
			IsIndicatorShown = false;
			LoggedInUser = user;
			if (user != null)
			{
				_loginType = user.AccountType;
				WelcomeUserName = " " + user.UserName;
				ProfilePictureUrl = !string.IsNullOrEmpty(user.AvatarUrl) ? user.AvatarUrl : null;

				//await Task.Run(() =>
				//{
				ManagementService.GetUserInfo(user.UserId, user.AccountType.ToString());
				//});
			}
		}

		private void OnLogoutConfirmClicked()
		{
			IsLogoutPopupShown = false;
			if (_loginType != null && _loginType.Equals(AccountType.FACEBOOK))
			{
				_socialHelper.LogoutFacebook();
			}
			else
			{
				_socialHelper.LogoutGoogle();
			}
			_loginType = null;
			IsLoggedIn = false;
			InitMenuItems();
			_badgeHelper.ClearBadge();
			DataModel.Notifications = null;
			DataModel.ComplaintHistory = null;
			DataModel.PreviousPage = null;
			DataModel.NewComplaintContent = null;
			DataModel.CommentContent = null;
			DataModel.UserInfo = null;
			DataModel.DidTryLogin = false;
			ClearHomePageData();
			ExecuteBeforeShowDetailView(typeof(HomeViewModel), delegate { ShowViewModel<HomeViewModel>(); });
			TopRightButtonType = HeaderButtonType.MenuActive;
			DataModel.GetUserInfoNetworkStatus = NetworkStatus.Null;
		}

		protected override void UnregisteredEvent()
		{
		}

		private void OnCallHotline()
		{
			var task = Mvx.Resolve<IMvxPhoneCallTask>();
			task.MakePhoneCall("DaNangPhone", ConstVariable.ContactNumber);
		}

		private void OnNetworkConnected(object sender, EventArgs e)
		{
			OnNetworkConnectedCommand();
		}

		public void OnNetworkConnectedCommand()
		{
			IsNoWifiPopupShown = false;
			var numOfOfflineFeedback = _appDataLoader.AppData.NumberOfOfflineFeedback;
			// fail to retrieve user data from Complaint Server caused by no internet on startup
			// retry get get user info.
			if (numOfOfflineFeedback > 0 && IsLoggedIn && DataModel.UserInfo == null && DataModel.GetUserInfoNetworkStatus != NetworkStatus.Null) // && DataModel.GetUserInfoNetworkStatus == NetworkStatus.Success
			{
				TryGetUserInfo();
			}
			else
			{
				if (numOfOfflineFeedback > 0)
				{
				
					// login to send feedback
					if (!IsLoggedIn)
					{
						if (DataModel.DidTryLogin && !_wasSecondLoginShown)
						{
							DataModel.PreviousPage = PreviousPage.HomeViewModel;
							_wasSecondLoginShown = true;
							ExecutionHelper.ExecuteWithOffset(
								() =>
								{
									ExecuteBeforeShowDetailView(typeof(SecondLoginScreenViewModel), delegate
									{
										IsIndicatorShown = false;
										ShowViewModel<SecondLoginScreenViewModel>(new { numOfOfflineFeedback = numOfOfflineFeedback });
									});
								},
								1000);
						}
					}
					else
					{
						ResendFeedback();
					}
				}
			}
		}

		private void TryGetUserInfo()
		{
			//if (DataModel.GetUserInfoNetworkStatus != NetworkStatus.Null) // if did call ws to get user info
			//{
				if (DataModel.GetUserInfoNetworkStatus != NetworkStatus.Success) // but not success (time out...)
				{
					// recall ws one more time
					ManagementService.GetUserInfo(LoggedInUser.UserId, LoggedInUser.AccountType.ToString());
				}
				else // if there is no user info enterred
				{
					PromptGetUserInfo();
				}
			//}
		}

		private void PromptGetUserInfo()
		{
			if (DataModel.UserInfo == null && _appDataLoader.AppData.NumberOfOfflineFeedback > 0)
			{
				ExecutionHelper.ExecuteWithOffset(
					() =>
					{
						ExecuteBeforeShowDetailView(typeof(AddPersonalInfoViewModel), delegate
						{
							IsIndicatorShown = false;
							ShowViewModel<AddPersonalInfoViewModel>(new { isCalledByOfflineMode = true });
						});
					},
					1000);
			}
		}

		private void ResendFeedback()
		{
			if (_appDataLoader.AppData.NumberOfOfflineFeedback > 0 && DataModel.UserInfo != null)
			{
				_savedFeedbacks = _appDataLoader.LoadFeedBacks();

				_lastResendFeedback = _savedFeedbacks?.FirstOrDefault();

				if (_lastResendFeedback != null)
				{
					ManagementService.PostComplaint(_lastResendFeedback.ComplaintContentText, _lastResendFeedback.LocationText, _lastResendFeedback.Images, _lastResendFeedback.VideoLinkValue);
				}
			}

		}

		/// <summary>
		/// Send next saved feedback.
		/// </summary>
		private void OnResendSuccessful()
		{
			_appDataLoader.RemoveFeedback(_savedFeedbacks, _lastResendFeedback);
			if (_appDataLoader.AppData.NumberOfOfflineFeedback == 0)
			{

				ShowLastViewModel();

				ShowToast(Localizer.GetText("SendComplaintSuccessfully"));
			}

			ResendFeedback();
		}

		public void ShowLastViewModel()
		{
			ExecuteBeforeShowDetailView(DataModel.LastViewModelType, () =>
			{
				ShowViewModel(DataModel.LastViewModelType);
				HighLightMenuItem(DataModel.LastViewModelType.ToString());
			});
		}

		private void HighLightMenuItem(string lastViewModelType)
		{
			switch (lastViewModelType)
			{
				case "GovernmentComplaint.Core.ViewModels.CreateNewComplaintViewModel":
					HighLightMenuItemAtIndex(0);

					break;
				case "GovernmentComplaint.Core.ViewModels.NotificationViewModel":
					HighLightMenuItemAtIndex(2);

					break;
				case "GovernmentComplaint.Core.ViewModels.HistoryViewModel":
					HighLightMenuItemAtIndex(3);

					break;
				case "GovernmentComplaint.Core.ViewModels.AboutViewModel":
					HighLightMenuItemAtIndex(4);
					break;
				default:
					// highlight home
					SidebarItemViewModel.SelectedItem = SidebarItemViewModels[1];
					SidebarItemViewModel.SelectedItem.ItemStatus = ItemStatus.Active;
					SidebarItemViewModel.SelectedItem.LabelStatus = LabelStatus.Active;
					break;
			}
		}

		private void HighLightMenuItemAtIndex(int itemIndex)
		{
			SidebarItemViewModel.SelectedItem = SidebarItemViewModels[itemIndex];
			SidebarItemViewModel.SelectedItem.ItemStatus = ItemStatus.Active;
			SidebarItemViewModel.SelectedItem.LabelStatus = LabelStatus.Active;
			SidebarItemViewModels[1].ItemStatus = ItemStatus.Inactive;
			SidebarItemViewModels[1].LabelStatus = LabelStatus.Inactive;
		}

		public void DeleteAllSavedFeedback()
		{
			_appDataLoader.DeleteAllSavedFeedback();
		}
		#endregion
	}
}