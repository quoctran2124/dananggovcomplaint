﻿using GovernmentComplaint.Core.Helpers;
using GovernmentComplaint.Core.Models;
using GovernmentComplaint.Core.Services;
using System;
using System.Collections.Generic;
using GovernmentComplaint.Core.Enums;
using GovernmentComplaint.Core.ViewModels.ItemViewModels;
using MvvmCross.Platform;

namespace GovernmentComplaint.Core.ViewModels.Bases
{
	public abstract class DetailViewModel : BaseViewModel
	{
		public static List<Action> ShowedModelActions { get; private set; }

		protected DetailViewModel(IManagementService managementService, IDataModel dataModel, IResourceHelper resourceHelper)
			: base(managementService, dataModel, resourceHelper)
		{
			MasterViewModel.Instance.ShowDetailView += OnShowDetailView;
			MasterViewModel.Instance.BackButtonClicked += OnBackButtonClicked;
			ShowedModelActions = ShowedModelActions ?? new List<Action>();
		}

		#region Methods

		protected void ClearSearchResult()
		{
			DataModel.Keyword = null;
			DataModel.DepartmentId = 0;
			DataModel.ComplaintContents = null;

		}

		private void OnBackButtonClicked(object sender, EventArgs e)
		{
			OnBackCommand();
		}

		protected virtual void OnBackCommand()
		{
			MasterViewModel.Instance.IsPopupShown = false;
			MasterViewModel.Instance.IsSidebarShown = false;
			MasterViewModel.Instance.IsOverlayShown = false;

			var sidebarItem = SidebarItemViewModel.SelectedItem;
			if (sidebarItem != null)
			{
				sidebarItem.LabelStatus = sidebarItem.MenuType == SidebarMenuType.NewComplaint
						? LabelStatus.Special
						: LabelStatus.Inactive;
				sidebarItem.ItemStatus = ItemStatus.Inactive;
			}
		}

		public void OnShowDetailView(Type type, Action callback)
		{
			if (type == GetType() && type != typeof(HomeViewModel))
			{
				return;
			}
			OnClose();
			ExecutionHelper.ExecuteWithOffset(callback, 0);
		}

		public void OnShowCreateNewComplaintView(Type type, Action callback)
		{
			OnClose();
			ExecutionHelper.ExecuteWithOffset(callback, 0);
		}

		protected override void UnregisteredEvent()
		{
			DataModel.DataReceived -= OnDataReceived;
			MasterViewModel.Instance.ShowDetailView -= OnShowDetailView;
			MasterViewModel.Instance.BackButtonClicked -= OnBackButtonClicked;
		}

		protected virtual void OnClose()
		{
			UnregisteredEvent();
			Close(this);
		}

		public void GoToHome()
		{
			OnBackCommand();

			ShowedModelActions.Clear();

			ExecutionHelper.ExecuteWithOffset(delegate
			{
				ShowViewModel<HomeViewModel>();
			}, 200);
		}

		protected virtual void OnBack(object sender, EventArgs e)
		{
			if (ShowedModelActions == null || ShowedModelActions.Count == 1)
			{
				GoToHome();
			}
			else if (ShowedModelActions.Count > 1)
			{
				OnBackCommand();

				ShowedModelActions.RemoveAt(ShowedModelActions.Count - 1);

				ExecutionHelper.ExecuteWithOffset(delegate
				{
					ShowedModelActions[ShowedModelActions.Count - 1]();
				}, 200);
			}
		}

		protected void DoIfConnectionOn(Action action)
		{
			if (Mvx.Resolve<INetworkHelper>().IsConnected)
			{
				action();
			}
		}
		#endregion
	}
}
