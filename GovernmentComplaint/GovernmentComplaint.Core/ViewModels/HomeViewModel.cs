﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Windows.Input;
using GovernmentComplaint.Core.Enums;
using GovernmentComplaint.Core.Helpers;
using GovernmentComplaint.Core.Localization;
using GovernmentComplaint.Core.Models;
using GovernmentComplaint.Core.Services;
using GovernmentComplaint.Core.ViewModels.Bases;
using GovernmentComplaint.Core.ViewModels.ItemViewModels;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform;

namespace GovernmentComplaint.Core.ViewModels
{
	public class HomeViewModel : DetailViewModel
	{
		private readonly SemaphoreSlim _lock = new SemaphoreSlim(1);
		private readonly INetworkHelper _networkHelper;
		private int _indexOfItem;
		private string _previousPage;
		private int _page = 1;
		public HomeViewModel(IManagementService managementService, IDataModel dataModel, IResourceHelper resourceHelper, INetworkHelper networkHelper)
			: base(managementService, dataModel, resourceHelper)
		{
			_networkHelper = networkHelper;
			MasterViewModel.Instance.HomeViewModel = this;
			MasterViewModel.Instance.TitleBar = Localizer.GetText("MainTitle");
			MasterViewModel.Instance.InitButtonOnMainView();
			MasterViewModel.Instance.LeftButtonClicked += OnSearchButtonClick;
			if (DataModel.ComplaintContents == null)
			{
				IsNoDataTextShown = true;
			}
			DataModel.DataReceived += OnDataReceived;
			IsCategoryShown = false;
			IsSortingShown = false;
			IsCateShown = true;
			IsSearchedTextShown = false;
			InitCommands();

			if (SidebarItemViewModel.SelectedItem != null)
			{
				SidebarItemViewModel.SelectedItem.ItemStatus = ItemStatus.Inactive;
				SidebarItemViewModel.SelectedItem.LabelStatus = LabelStatus.Inactive;
			}

			SidebarItemViewModel.SelectedItem = MasterViewModel.Instance.SidebarItemViewModels[1];
			SidebarItemViewModel.SelectedItem.ItemStatus = ItemStatus.Active;
			SidebarItemViewModel.SelectedItem.LabelStatus = LabelStatus.Active;

			if (MasterViewModel.Instance.SidebarItemViewModels?.Count > 0)
			{
				MasterViewModel.Instance.SidebarItemViewModels[0].ItemStatus = ItemStatus.Inactive;
				MasterViewModel.Instance.SidebarItemViewModels[0].LabelStatus = LabelStatus.Special;

			}

			dataModel.PreviousPage = null;

			if (_networkHelper.IsConnected)
			{
				MasterViewModel.Instance.OnNetworkConnectedCommand();
			}

			DataModel.PreviousPage = PreviousPage.HomeViewModel;
		}

		public void Init(int lastComplaintItemId, string previousPage)
		{
			DataModel.PreviousPageString = previousPage;
			if (!string.IsNullOrEmpty(previousPage))
			{
				CheckIsSearched(previousPage);
			}
			if (DataModel.ComplaintContents == null || lastComplaintItemId == 0) return;
			var item = DataModel.ComplaintContents.FirstOrDefault(m => m.Id == lastComplaintItemId);
			_indexOfItem = DataModel.ComplaintContents.IndexOf(item);
			LastComplaintItemPosition = _indexOfItem;
			_previousPage = previousPage;

		}

		private void CheckIsSearched(string title)
		{
			if (string.Equals(title, Localizer.GetText("SearchTitle")))
			{
				MasterViewModel.Instance.TitleBar = Localizer.GetText("SearchTitle");
				IsCateShown = false;
				IsSearchedTextShown = true;
				if (!string.IsNullOrEmpty(DataModel.Keyword) && !string.IsNullOrWhiteSpace(DataModel.Keyword))
				{
					SearchedText = DataModel.Keyword;
				}
				else
				{
					SearchedText = MasterViewModel.Instance.SelectedDepartment;
				}
			}
		}

		protected override void HandleDataReceived(WsCommand command, HttpStatusCode code)
		{
			base.HandleDataReceived(command, code);
			if (code == HttpStatusCode.OK)
			{
				switch (command)
				{
					case WsCommand.GetCategories:
						CategoryItemViewModels = DataModel.Categories?
							.Select(
								m =>
									new DropdownCategoryItemViewModel(m, ManagementService, DataModel,
										ResourceHelper))
							.ToList();
						InitSortingDropdownItems();
						break;
					case WsCommand.GetComplaints:
						IsCategoryShown = false;
						IsSortingShown = false;
						FeedbackItemViewModels = DataModel.ComplaintContents
							.Select(m => new FeedbackItemViewModel(m, ManagementService, DataModel, ResourceHelper))
							.ToList();
						//if (DataModel.ComplaintContents != null && DataModel.ComplaintContents.Any())
						//{
						//	IsNoDataTextShown = false;
						//}

					    IsNoDataTextShown = DataModel.ComplaintContents == null || !DataModel.ComplaintContents.Any();

                        LoadingMore = false;
						if ((!string.IsNullOrEmpty(DataModel.Keyword) && !string.IsNullOrWhiteSpace(DataModel.Keyword)) ||
							DataModel.DepartmentId != 0)
						{
							MasterViewModel.Instance.TitleBar = Localizer.GetText("SearchTitle");
						}
						CheckIsSearched(MasterViewModel.Instance.TitleBar);
						MasterViewModel.Instance.IsIndicatorShown = false;
						break;
					case WsCommand.OutOfData:
						MasterViewModel.Instance.IsIndicatorShown = false;
						break;
					case WsCommand.OutOfDataComplaints:
						MasterViewModel.Instance.IsIndicatorShown = false;
						IsNoDataTextShown = DataModel.ComplaintContents == null;
						break;
				}
			}
			else
			{
				MasterViewModel.Instance.IsIndicatorShown = false;
				MasterViewModel.Instance.IsConnectionErrorPopupShown = true;
			}
		}

		private void OnSearchButtonClick(object sender, EventArgs e)
		{
			MasterViewModel.Instance.IsSearchBoxShown = !MasterViewModel.Instance.IsSearchBoxShown;
			MasterViewModel.Instance.IsOverlayShown = MasterViewModel.Instance.IsSearchBoxShown;
			MasterViewModel.Instance.IsSidebarShown = false;
			MasterViewModel.Instance.TopRightButtonType = HeaderButtonType.Menu;
			MasterViewModel.Instance.TopLeftButtonType = MasterViewModel.Instance.TopLeftButtonType ==
														 HeaderButtonType.Search
				? HeaderButtonType.SearchActive
				: HeaderButtonType.Search;
		}

		protected override void OnBackCommand()
		{
			MasterViewModel.Instance.ShouldMoveToBackground = true;
		}

		protected override void UnregisteredEvent()
		{
			base.UnregisteredEvent();
			MasterViewModel.Instance.LeftButtonClicked -= OnSearchButtonClick;
		}

		private void InitSortingDropdownItems()
		{
			//Sorting Dropdown
			var sortingList = new List<SortingCondition>(new[]
			{
				new SortingCondition
				{
					SortId = 1,
					SortCondition = Localizer.GetText("Time"),
					SortImage = ResourceHelper.IconPath("calendar_sort")
				},
				new SortingCondition
				{
					SortId = 2,
					SortCondition = Localizer.GetText("Interest"),
					SortImage = ResourceHelper.IconPath("interest")
				},
				new SortingCondition
				{
					SortId = 3,
					SortCondition = Localizer.GetText("Comment"),
					SortImage = ResourceHelper.IconPath("comment_sort")
				}
			});
			var dropDropdownSortingItemViewModels = sortingList.Select
				(sortType => new DropdownSortingItemViewModel(sortType, ManagementService, DataModel, ResourceHelper))
				.ToList();
			SortingItemViewModels = dropDropdownSortingItemViewModels;
		}

		protected void InitCommands()
		{
			CategoryCommand = new MvxCommand(OnCategoryCommand);
			SortingCommand = new MvxCommand(OnSortingCommand);
			CategoryOverlayCommand = new MvxCommand(OnCategoryOverlayCommand);
			SortingOverlayCommand = new MvxCommand(OnSortingOverlayCommand);
			GoToNewComplaintCommand = new MvxCommand(OnGoToNewComplaintCommand);
			LoadMoreData = new MvxCommand(OnLoadMoreData);
			CheckNetworkCommand = new MvxCommand(OnCheckNetworkCommand);
			RefreshCommand = new MvxCommand(OnRefreshCommand);
			ConnectionErrorConfirmCommand = new MvxCommand(OnCheckNetworkCommand);
			RemoveSearchResult = new MvxCommand(OnRemoveSearchResult);
		}

		private void OnRemoveSearchResult()
		{
			MasterViewModel.Instance.IsIndicatorShown = true;
			IsSearchedTextShown = false;
			IsCateShown = true;
			ClearHomePageData();
			MasterViewModel.Instance.SearchText = string.Empty;
			MasterViewModel.Instance.SelectedDepartment = MasterViewModel.Instance.SpinnerHeader;
			MasterViewModel.Instance.TitleBar = MasterViewModel.Instance.MainTitle;
			ManagementService.GetComplaints(1, DataModel.CatFilter, DataModel.SortFilter, DataModel.Keyword,
				DataModel.DepartmentId);
		}

		private void OnLoadMoreData()
		{
			if (DataModel.ComplaintContents == null) return;
			else
			{
				if (DataModel.ComplaintContents.Count >= 15)
				{
					MasterViewModel.Instance.IsIndicatorShown = true;
					ManagementService.GetComplaints(++_page, DataModel.CatFilter, DataModel.SortFilter,
						DataModel.Keyword,
						DataModel.DepartmentId);
				}
				else
				{
					MasterViewModel.Instance.IsIndicatorShown = false;
				}
			}

		}

		#region Commands

		public ICommand ConnectionErrorConfirmCommand { get; private set; }

		public ICommand CheckNetworkCommand { get; private set; }

		public ICommand CategoryCommand { get; private set; }

		public ICommand SortingCommand { get; private set; }

		public ICommand CategoryOverlayCommand { get; private set; }

		public ICommand SortingOverlayCommand { get; private set; }

		public ICommand GoToNewComplaintCommand { get; private set; }

		public ICommand LoadMoreData { get; set; }

		public ICommand RefreshCommand { get; private set; }

		public ICommand RemoveSearchResult { get; private set; }

		#endregion

		#region Properties

		private bool _isCateShown;
		public bool IsCateShown
		{
			get { return _isCateShown; }
			set { _isCateShown = value; RaisePropertyChanged(() => IsCateShown); }
		}

		private bool _isNoDataTextShown;
		public bool IsNoDataTextShown
		{
			get { return _isNoDataTextShown; }
			set { _isNoDataTextShown = value; RaisePropertyChanged(() => IsNoDataTextShown); }
		}

		private bool _moveListToTop;

		public bool MoveListToTop
		{
			get { return _moveListToTop; }
			set
			{
				_moveListToTop = value;
				RaisePropertyChanged(() => MoveListToTop);
			}
		}

		private bool _isRefreshing;

		public bool IsRefreshing
		{
			get { return _isRefreshing; }
			set
			{
				_isRefreshing = value;
				RaisePropertyChanged(() => IsRefreshing);
			}
		}

		private int _lastComplaintItemPosition;

		public int LastComplaintItemPosition
		{
			get { return _lastComplaintItemPosition; }
			set
			{
				_lastComplaintItemPosition = value;
				RaisePropertyChanged(() => LastComplaintItemPosition);
			}
		}

		private List<FeedbackItemViewModel> _feedbackItemViewModels;

		public List<FeedbackItemViewModel> FeedbackItemViewModels
		{
			get { return _feedbackItemViewModels; }
			set
			{
				_feedbackItemViewModels = value;
				RaisePropertyChanged(() => FeedbackItemViewModels);
			}
		}

		private List<DropdownCategoryItemViewModel> _categoryItemViewModels;

		public List<DropdownCategoryItemViewModel> CategoryItemViewModels
		{
			get { return _categoryItemViewModels; }
			set
			{
				_categoryItemViewModels = value;
				RaisePropertyChanged(() => CategoryItemViewModels);
			}
		}

		private List<DropdownSortingItemViewModel> _sortingItemViewModels;

		public List<DropdownSortingItemViewModel> SortingItemViewModels
		{
			get { return _sortingItemViewModels; }
			set
			{
				_sortingItemViewModels = value;
				RaisePropertyChanged(() => SortingItemViewModels);
			}
		}

		private bool _isCategoryShown;

		public bool IsCategoryShown
		{
			get { return _isCategoryShown; }
			set
			{
				_isCategoryShown = value;
				if (value)
					CategoryIconName = ResourceHelper.IconPath("collapse");
				else
					CategoryIconName = ResourceHelper.IconPath("dropdown");
				RaisePropertyChanged(() => IsCategoryShown);
			}
		}

		private bool _isSortingShown;

		public bool IsSortingShown
		{
			get { return _isSortingShown; }
			set
			{
				_isSortingShown = value;
				SortingIconName = ResourceHelper.IconPath(value ? "sort_active" : "sort");
				RaisePropertyChanged(() => IsSortingShown);
			}
		}

		private string _categoryIconName;

		public string CategoryIconName
		{
			get { return _categoryIconName; }
			set
			{
				_categoryIconName = value;
				RaisePropertyChanged(() => CategoryIconName);
			}
		}

		private string _sortingIconName;

		public string SortingIconName
		{
			get { return _sortingIconName; }
			set
			{
				_sortingIconName = value;
				RaisePropertyChanged(() => SortingIconName);
			}
		}

		private Category _category;

		public Category Category
		{
			get { return _category; }
			set
			{
				_category = value;
				RaisePropertyChanged(() => Category);
			}
		}

		private bool _loadingMore;

		public bool LoadingMore
		{
			get { return _loadingMore; }
			set
			{
				_loadingMore = value;
				RaisePropertyChanged(() => LoadingMore);
			}
		}

		private string _searchedText;
		public string SearchedText
		{
			get { return _searchedText; }
			set { _searchedText = value; RaisePropertyChanged(() => SearchedText); }
		}

		private bool _isSearchedTextShown;
		public bool IsSearchedTextShown
		{
			get { return _isSearchedTextShown; }
			set { _isSearchedTextShown = value; RaisePropertyChanged(() => IsSearchedTextShown); }
		}

		private bool _shouldScrollTableView;
		public bool ShouldScrollTableView
		{
			get { return _shouldScrollTableView; }
			set { _shouldScrollTableView = value; RaisePropertyChanged(() => ShouldScrollTableView); }
		}

		public string CategoryName => Localizer.GetText("Category");
		public string NoData => Localizer.GetText("NoData");
		public string Sorting => Localizer.GetText("Sorting");

		#endregion

		#region Command Methods

		private async void OnRefreshCommand()
		{
			if (_networkHelper.IsConnected)
			{
				await _lock.WaitAsync();
				try
				{
					MasterViewModel.Instance.IsIndicatorShown = true;
					IsRefreshing = true;
					DataModel.ComplaintContents = null;
					ManagementService.GetComplaints(1, DataModel.CatFilter, DataModel.SortFilter, DataModel.Keyword,
						DataModel.DepartmentId);
					IsRefreshing = false;
				}
				finally
				{
					_lock.Release();
				}
			}
			else
			{
				MasterViewModel.Instance.IsNoWifiPopupShown = true;
				IsRefreshing = false;
			}
		}

		private void OnCheckNetworkCommand()
		{
			LoadingMore = false;
			if (_networkHelper.IsConnected)
			{
				ManagementService.GetDepartments();
				ManagementService.GetCategories();
				if (DataModel.ComplaintContents == null)
				{
					MasterViewModel.Instance.IsIndicatorShown = true;
					ManagementService.GetComplaints(1, DataModel.CatFilter, DataModel.SortFilter, DataModel.Keyword,
						DataModel.DepartmentId);
				}
				else
				{
					_page = DataModel.ComplaintContents.Count / 15;
					FeedbackItemViewModels = DataModel.ComplaintContents
						.Select(m => new FeedbackItemViewModel(m, ManagementService, DataModel, ResourceHelper)).ToList();
					MasterViewModel.Instance.IsIndicatorShown = false;
				}
			}
			else
			{
				MasterViewModel.Instance.IsNoWifiPopupShown = true;
			}

			ShouldScrollTableView = _networkHelper.IsConnected;

			//if (DataModel.UserInfo == null)
			//{
			//	MasterViewModel.Instance.ShouldRetrieveUserInfo = true;
			//}
		}

		private void OnCategoryCommand()
		{
			IsCategoryShown = !IsCategoryShown;
			if (IsSortingShown)
				IsSortingShown = false;
		}

		private void OnSortingCommand()
		{
			IsSortingShown = !IsSortingShown;
			if (IsCategoryShown)
				IsCategoryShown = false;
		}

		private void OnCategoryOverlayCommand()
		{
			IsCategoryShown = false;
		}

		private void OnSortingOverlayCommand()
		{
			IsSortingShown = false;
		}

		public virtual void OnGoToNewComplaintCommand()
		{
			SidebarItemViewModel.SelectedItem = MasterViewModel.Instance.SidebarItemViewModels[0];
			SidebarItemViewModel.SelectedItem.LabelStatus = LabelStatus.Active;
			SidebarItemViewModel.SelectedItem.ItemStatus = ItemStatus.Active;
			MasterViewModel.Instance.SidebarItemViewModels[1].ItemStatus = ItemStatus.Inactive;
			MasterViewModel.Instance.SidebarItemViewModels[1].LabelStatus = LabelStatus.Inactive;
			OnShowDetailView(typeof(CreateNewComplaintViewModel),
				delegate
				{
					ShowViewModel<CreateNewComplaintViewModel>(new
					{
						previousPage = MasterViewModel.Instance.TitleBar
					});
				});
		}

		#endregion
	}
}