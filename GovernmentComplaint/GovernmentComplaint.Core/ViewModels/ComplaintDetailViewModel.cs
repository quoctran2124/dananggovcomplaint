﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Windows.Input;
using GovernmentComplaint.Core.Enums;
using GovernmentComplaint.Core.Helpers;
using GovernmentComplaint.Core.Localization;
using GovernmentComplaint.Core.Models;
using GovernmentComplaint.Core.Services;
using GovernmentComplaint.Core.ViewModels.Bases;
using GovernmentComplaint.Core.ViewModels.ItemViewModels;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform;
using MvvmCross.Plugins.WebBrowser;

namespace GovernmentComplaint.Core.ViewModels
{
	public class ComplaintDetailViewModel : DetailViewModel
	{
		private readonly INetworkHelper _networkHelper;
		private int _detailId;
		private string _previousPage, _previousPreviousPage, _searchText;
		private bool _isChildrenCommentContentEmpty;

		public ComplaintDetailViewModel(IManagementService managementService, IDataModel dataModel,
			IResourceHelper resourceHelper) : base(managementService, dataModel, resourceHelper)
		{
			_networkHelper = Mvx.Resolve<INetworkHelper>();
			ManagementService.StopTimer();
			MasterViewModel.Instance.ComplaintDetailViewModel = this;
			InitCommand();
			MasterViewModel.Instance.TitleBar = Localizer.GetText("ContentTitle");
			MasterViewModel.Instance.InitButtonOnDetailView();
			MasterViewModel.Instance.LeftButtonClicked += OnLeftButtonClicked;
			MasterViewModel.Instance.RightButtonClicked += OnRightButtonClicked;
			DataModel.PreviousPage = PreviousPage.ComplaintDetail;
			ScrollPositionValue = 0;

			CommentContent = DataModel.CommentContent;
		}

		private void InitCommand()
		{
			AddCommentCommand = new MvxCommand(OnCommentCommand);
			CommentTouchCommand = new MvxCommand(OnCommentTouchCommand);
			DetailImageClickCommand = new MvxCommand(OnDetailImageClickCommand);
			RatingCommand = new MvxCommand(OnRatingCommand);
			UserRatingCommand = new MvxCommand(OnUserRatingCommand);
			UserRatingCommandIos = new MvxCommand<int>(OnRetrieveRating);
			CheckNetworkCommand = new MvxCommand(OnCheckNetworkCommand);
			AttachedImage1ClickCommand = new MvxCommand(OnAttachedImage1ClickCommand);
			AttachedImage2ClickCommand = new MvxCommand(OnAttachedImage2ClickCommand);
		    AnswerResponseCommand = new MvxCommand(OnAnswerResponseCommand);

        }

        private void OnAttachedImage2ClickCommand()
		{
			MasterViewModel.Instance.AttachedImageUrl = AttachedImage2;
			MasterViewModel.Instance.IsAttachedImagesPopupShown = true;
		}

		private void OnAttachedImage1ClickCommand()
		{
			MasterViewModel.Instance.AttachedImageUrl = AttachedImage1;
			MasterViewModel.Instance.IsAttachedImagesPopupShown = true;
		}

		private void OnRightButtonClicked(object sender, EventArgs e)
		{
			IsKeyboardShown = false;
		}

		public void Init(int id, string pageType, string searchText)
		{
			DataModel.PreviousPageString = pageType;
			_previousPage = pageType;
			_searchText = searchText;
			_detailId = id;
			DataModel.DataReceived += OnDataReceived;
		}

		private void DisplayComplaintDetail()
		{
			var firstOrDefault = DataModel.ComplaintContents.FirstOrDefault(b => b.Id == DataModel.ComplaintContent.Id);
			Title = DataModel.ComplaintContent.Title;
			var timeCreated = DataModel.ComplaintContent.TimeCreated;
			DateCreated = timeCreated + " " +
						  Convert.ToDateTime(DataModel.ComplaintContent.DateCreated).ToString("dd/MM/yyyy");
			Color = string.IsNullOrEmpty(firstOrDefault?.Color) ? "#0464A5" : firstOrDefault.Color.Trim();
			Department = string.IsNullOrEmpty(firstOrDefault?.Department)
				? Localizer.GetText("DefaultDepartment")
				: firstOrDefault?.Department;
			Location = DataModel.ComplaintContent.Location1;
			Content = DataModel.ComplaintContent.Content;
			ImagePaths = DataModel.ComplaintContent.PictureStrings;
			if (ImagePaths.Length > 2)
			{
				ImagesQuantity = 3;
				IsImage2Shown = true;
				IsImage3Shown = true;
				IsImage1Shown = true;
				ImagePath1 = ImagePaths[0];
				ImagePath2 = ImagePaths[1];
				ImagePath3 = ImagePaths[2];
				IsIndicator1Shown = true;
			}
			else if (ImagePaths.Length > 1)
			{
				ImagesQuantity = 2;
				IsImage2Shown = true;
				IsImage1Shown = true;
				ImagePath1 = ImagePaths[0];
				ImagePath2 = ImagePaths[1];
				IsIndicator1Shown = true;
			}
			else if (ImagePaths.Length > 0)
			{
				ImagesQuantity = 1;
				IsImage1Shown = true;
				ImagePath1 = ImagePaths[0];
			}
			if (!string.IsNullOrEmpty(DataModel.CommentContent))
			{
				CommentContent = DataModel.CommentContent;
			}

			if (!string.IsNullOrEmpty(DataModel.ComplaintContent.VideoPath))
			{
				IsVideoLinkShown = true;
				VideoLink = DataModel.ComplaintContent.VideoPath;
			}


		    IsSatisfactionFeedback = false;
		    IsAnswerResponseShown = false;

            AnswerResponseResultText = IsAnswerResponseShown ? (IsSatisfactionFeedback ? Localizer.GetText("Satisfied") : Localizer.GetText("UnSatisfied")) : "";
		    UnsatisfiedReasonText = IsSatisfactionFeedback ? "" : "(" + "thời gian trả lời chậm, kết quả trả lời không hợp lý" + ")";

            AnswerResponseResultIconName = IsAnswerResponseShown ? (IsSatisfactionFeedback ? ResourceHelper.IconPath("satisfied") : ResourceHelper.IconPath("un_satified")) : "";
            IsFeedbackOfUser = true;
        }

        private void DisplayReplyDetail()
		{
			var firstOrDefault = DataModel.ComplaintContent.ReplyList.FirstOrDefault();
			ReplyDepartment = firstOrDefault?.DepartmentName;
			ReplyContent = firstOrDefault?.ReplyContent;
			var comments =
				DataModel.ComplaintContent.CommentList.Where(m => string.Equals(m.InteractionType, "BINH_LUAN"));
			CommentQuantity = comments.Count();
			DateTime replyTime;
			if (DateTime.TryParse(firstOrDefault?.DateReply, new CultureInfo("fr-FR"), DateTimeStyles.None,
				out replyTime))
				DateReplied = replyTime.ToString("HH:mm dd/MM/yyyy");
			FileNames = firstOrDefault?.FileNamesStrings;
			FileUrls = firstOrDefault?.AttachmentStrings;
			if (FileUrls != null && FileNames != null)
				if (FileUrls.Length > 2)
				{
					FileUrl1 = FileUrls[0];
					FileUrl2 = FileUrls[1];
					FileUrl3 = FileUrls[2];

					FileName1 = FileNames[0];
					FileName2 = FileNames[1];
					FileName3 = FileNames[2];

					IsAttachedFile1Shown = true;
					IsAttachedFile2Shown = true;
					IsAttachedFile3Shown = true;
				}
				else
				{
					if (FileUrls.Length > 1)
					{
						FileUrl1 = FileUrls[0];
						FileUrl2 = FileUrls[1];

						FileName1 = FileNames[0];
						FileName2 = FileNames[1];

						IsAttachedFile1Shown = true;
						IsAttachedFile2Shown = true;
					}
					else
					{
						if (FileUrls.Length > 0)
						{
							FileUrl1 = FileUrls[0];

							FileName1 = FileNames[0];

							IsAttachedFile1Shown = true;
						}
					}
				}

			ReplyImages = firstOrDefault?.ReplyImagesStrings;
			if (ReplyContent != null)
			{
				IsReplyShown = true;
			}
			else
			{
				NeedToHideReplyHolder = true;
			}

			if (FileNames != null)
				IsAttachmentShown = true;

			if (ReplyImages != null && ReplyImages.Length != 0)
			{
				IsAttachmentImagesShown = true;
				AttachedImagesQuantity = ReplyImages.Length;
				if (ReplyImages.Length > 1)
				{
					IsAttachedImage1Shown = true;
					IsAttachedImage2Shown = true;
					AttachedImage1 = ReplyImages[0];
					AttachedImage2 = ReplyImages[1];
				}
				else if (ReplyImages.Length > 0)
				{
					IsAttachedImage1Shown = true;
					AttachedImage1 = ReplyImages[0];
				}
			}
			else
			{
				IsAttachmentImagesShown = false;
			}

			if (!string.IsNullOrWhiteSpace(firstOrDefault?.RatingCount))
			{
				RatingCount = double.Parse(firstOrDefault?.RatingCount);
			}
			if (!string.IsNullOrWhiteSpace(firstOrDefault?.Rating))
			{
				Rating = Convert.ToInt32(double.Parse(firstOrDefault.Rating, CultureInfo.InvariantCulture));
			}
		}

		protected override void HandleDataReceived(WsCommand command, HttpStatusCode code)
		{
			base.HandleDataReceived(command, code);
			if (code == HttpStatusCode.OK)
			{
				var firstOrDefault = DataModel.ComplaintContent.ReplyList.FirstOrDefault();
				switch (command)
				{
					case WsCommand.GetDetailComplaint:
						// ComplaintDetailURL = DataModel.ComplaintContent.Url;
						ComplaintDetailURL = "https://egov.danang.gov.vn/web/guest/gop-y?pageid=view&ykien=" +
											 DataModel.ComplaintContent.Id;
						DisplayComment();
						DisplayComplaintDetail();
						DisplayReplyDetail();
						MasterViewModel.Instance.IsIndicatorShown = false;
						if (!string.IsNullOrEmpty(firstOrDefault?.Rating))
						{
							DataModel.ComplaintContentById(_detailId).RatingFeedback =
								Convert.ToInt32(double.Parse(firstOrDefault.Rating));
						}
						break;
					case WsCommand.PostComment:
						if (DataModel.UserInfo.TotalComment != null)
						{
							var newTotalComment = Int32.Parse(DataModel.UserInfo.TotalComment) + 1;
							DataModel.UserInfo.TotalComment = newTotalComment.ToString();
						}
						MasterViewModel.Instance.TotalCommentText = DataModel.UserInfo.TotalComment;
						CommentContent = null;
						DataModel.CommentContent = null;
						ShowToast(Localizer.GetText("CommentSuccess"));
						ManagementService.GetUserInfo(DataModel.UserInfo.AccountId, DataModel.UserInfo.AccountType);
						break;
					case WsCommand.RateReply:
						Rating = ((Rating * RatingCount + UserRating) / (RatingCount + 1));
						var content = DataModel.ComplaintContentById(_detailId);
						content.RatingFeedback = Rating;
						RatingCount++;
						ShowToast(Localizer.GetText("RateSuccess"));
						UserRating = 0;
						break;
				}
			}
		}

		private void OnLeftButtonClicked(object sender, EventArgs e)
		{
			OnBackCommand();
		}

		private void OnCheckNetworkCommand()
		{
			if (_networkHelper.IsConnected)
			{
				if (_detailId != 0)
				{
					MasterViewModel.Instance.IsIndicatorShown = true;
					ManagementService.GetDetailComplaint(_detailId);
				}
				else
				{
					if (DataModel.ComplaintContent != null)
					{
						ComplaintDetailURL = "https://egov.danang.gov.vn/web/guest/gop-y?pageid=view&ykien=" +
											 DataModel.ComplaintContent.Id;
						Department = DataModel.ComplaintContent.Department;
						Color = DataModel.ComplaintContent.Color;
						DisplayComment();
						DisplayComplaintDetail();
						DisplayReplyDetail();

						if (!string.IsNullOrEmpty(DataModel.CommentContent))
						{
							CommentContent = DataModel.CommentContent;
						}

						if (!string.IsNullOrEmpty(DataModel.ComplaintContent.ReplyList.FirstOrDefault()?.Rating))
						{
							DataModel.ComplaintContentById(DataModel.ComplaintContent.Id).RatingFeedback =
								Convert.ToInt32(double.Parse(DataModel.ComplaintContent.ReplyList.FirstOrDefault().Rating));
						}
					}
				}
			}
			else
			{
				MasterViewModel.Instance.IsNoWifiPopupShown = true;
			}
		}
        
        private void OnCommentCommand()
		{
			IsKeyboardShown = false;
			IsKeyboardHidden = true;

			if (_networkHelper.IsConnected)
			{
				DataModel.CommentContent = CommentContent;
				if (string.IsNullOrWhiteSpace(CommentContent))
				{
					MasterViewModel.Instance.IsNoCommentPopupShown = true;
				}
				else
				{
					if (!MasterViewModel.Instance.IsLoggedIn)
					{
						DataModel.CommentContent = CommentContent;
						OnShowDetailView(typeof(SecondLoginScreenViewModel),
							delegate {
								ShowViewModel<SecondLoginScreenViewModel>(new
								{
									previousPage = _previousPage
								});
							});
					}
					else
					{
						if (DataModel.UserInfo == null)
						{
							DataModel.CommentContent = CommentContent;
							OnShowDetailView(typeof(AddPersonalInfoViewModel),
								delegate {
									ShowViewModel<AddPersonalInfoViewModel>(new
									{
										previousPage = _previousPage
									});
								});
						}
						else
						{
							ManagementService.PostComment(_detailId, DataModel.UserInfo.UserId, 0, CommentContent,
								"BINH_LUAN");
							CommentContent = null;
							NeedToShowPlaceHolder = true;
						}
					}
				}
			}
			else
			{
				MasterViewModel.Instance.IsNoWifiPopupShown = true;
			}
		}

		private void OnDetailImageClickCommand()
		{
			if (IsImage1Shown || IsImage2Shown || IsImage3Shown)
			{
				_previousPreviousPage = _previousPage;
				_scrollPositionValue = ScrollPositionValue;
				MasterViewModel.Instance.ExecuteBeforeShowDetailView(typeof(ImageDetailViewModel),
					delegate
					{
						ShowViewModel<ImageDetailViewModel>(
							new
							{
								id = _detailId,
								pageType = _previousPreviousPage,
								scrollPosition = _scrollPositionValue
							});
					});
			}
		}

		public void DisplayComment()
		{
			NumsComment = DataModel.ComplaintContent.CommentList.Count(t => t.InteractionType == "BINH_LUAN");
			if (NumsComment != 0)
				CommentItemViewModels = DataModel.ComplaintContent.CommentList.
					Where(m => m.ParentId == null && string.Equals(m.InteractionType, "BINH_LUAN")).
					Select(m => new CommentItemViewModel(m, _detailId, ManagementService, DataModel, ResourceHelper))
					.ToList();
		}

		protected override void UnregisteredEvent()
		{
			base.UnregisteredEvent();
			MasterViewModel.Instance.LeftButtonClicked -= OnLeftButtonClicked;
			MasterViewModel.Instance.RightButtonClicked -= OnRightButtonClicked;
		}

		protected override void OnBackCommand()
		{
			CheckChildrenCommentIsEmpty();
			IsKeyboardHidden = true;
			IsKeyboardShown = false;
			if ((string.IsNullOrWhiteSpace(CommentContent) || string.IsNullOrEmpty(CommentContent)) &&
				_isChildrenCommentContentEmpty)
			{
				if (_previousPage == Localizer.GetText("MainTitle"))
				{
					OnShowDetailView(typeof(HomeViewModel), delegate
					{
						ShowViewModel<HomeViewModel>(new
						{
							lastComplaintItemId = _detailId
						});
					});
				}
				else
				{
					if (_previousPage == Localizer.GetText("HistoryTitle"))
					{
						OnShowDetailView(typeof(HistoryViewModel), delegate
						{
							ShowViewModel<HistoryViewModel>(new
							{
								lastComplaintItemId = _detailId,
							});
						});
					}
					else
					{
						if (_previousPage == Localizer.GetText("NotificationTitle"))
						{
							OnShowDetailView(typeof(NotificationViewModel), delegate
							{
								ShowViewModel<NotificationViewModel>(new
								{
									lastComplaintItemId = _detailId,
								});
							});
						}
						else
							OnShowDetailView(typeof(HomeViewModel), delegate
							{
								ShowViewModel<HomeViewModel>(new
								{
									lastComplaintItemId = _detailId,
									previousPage = _previousPage
								});
							});
					}
				}
			}
			else
			{
				MasterViewModel.Instance.IsCancelCommentPopupShown = true;
			}
		}

		private void CheckChildrenCommentIsEmpty()
		{
			if (CommentItemViewModels != null)
			{
				foreach (var commentItemViewModel in CommentItemViewModels)
				{
					if (commentItemViewModel.ChildrenCommentItemViewModels != null)
					{
						foreach (var childrenCommentItemViewModel in commentItemViewModel.ChildrenCommentItemViewModels)
						{
							if (!string.IsNullOrEmpty(childrenCommentItemViewModel.EnteredCommentContent) ||
								!string.IsNullOrWhiteSpace(childrenCommentItemViewModel.EnteredCommentContent))
							{
								_isChildrenCommentContentEmpty = false;
								return;
							}
						}
					}

					if (!string.IsNullOrEmpty(commentItemViewModel.EnteredCommentContent) ||
						!string.IsNullOrWhiteSpace(commentItemViewModel.EnteredCommentContent))
					{
						_isChildrenCommentContentEmpty = false;
						return;
					}
				}
			}
			_isChildrenCommentContentEmpty = true;
		}

		private void OnCommentTouchCommand()
		{
			//TODO: TURN OFF HINT
		}

		private void OnRatingCommand()
		{
			if (_networkHelper.IsConnected)
			{
				if (!MasterViewModel.Instance.IsLoggedIn)
				{
					OnShowDetailView(typeof(SecondLoginScreenViewModel),
						delegate {
							ShowViewModel<SecondLoginScreenViewModel>(new
							{
								previousPage = _previousPage
							});
						});
				}
				else
				{
					if (DataModel.UserInfo == null)
					{
						DataModel.CommentContent = CommentContent;
						OnShowDetailView(typeof(AddPersonalInfoViewModel),
							delegate {
								ShowViewModel<AddPersonalInfoViewModel>(new
								{
									previousPage = _previousPage
								});
							});
					}
					else
					{
						MasterViewModel.Instance.IsRatingPopupShown = true;
					}
				}
			}
			else
			{
				MasterViewModel.Instance.IsNoWifiPopupShown = true;
			}
		}


	    private void OnAnswerResponseCommand()
	    {
            //if (_networkHelper.IsConnected)
            //{
            //    if (!MasterViewModel.Instance.IsLoggedIn)
            //    {
            //        OnShowDetailView(typeof(SecondLoginScreenViewModel),
            //            delegate {
            //                ShowViewModel<SecondLoginScreenViewModel>(new
            //                {
            //                    previousPage = _previousPage
            //                });
            //            });
            //    }
            //    else
            //    {
            //        if (DataModel.UserInfo == null)
            //        {
            //            DataModel.CommentContent = CommentContent;
            //            OnShowDetailView(typeof(AddPersonalInfoViewModel),
            //                delegate {
            //                    ShowViewModel<AddPersonalInfoViewModel>(new
            //                    {
            //                        previousPage = _previousPage
            //                    });
            //                });
            //        }
            //        else
            //        {
            //            MasterViewModel.Instance.IsAnswerFeedbackPopupShown = true;
            //        }
            //    }
	        //}
	        //else
	        //{
	        //    MasterViewModel.Instance.IsNoWifiPopupShown = true;
            //}

            MasterViewModel.Instance.IsAnswerFeedbackPopupShown = true;

	    }
        private void OnRetrieveRating(int rating)
		{
			UserRating = rating;
		}

		private void OnUserRatingCommand()
		{
			ManagementService.RateReply(DataModel.ComplaintContent.ReplyList.FirstOrDefault().ReplyId, UserRating);
			MasterViewModel.Instance.IsRatingPopupShown = false;
		}

        #region Properties

	    
	    private bool _isFeedbackOfUser;
	    public bool IsFeedbackOfUser
        {
	        get => _isFeedbackOfUser;
	        set
	        {
	            _isFeedbackOfUser = value;
	            RaisePropertyChanged(() => IsFeedbackOfUser);
                RaisePropertyChanged(() => ResponseButtonHint);
            }
	    }

        private bool _isAnswerResponseShown;
	    public bool IsAnswerResponseShown
	    {
	        get => _isAnswerResponseShown;
	        set
	        {
	            _isAnswerResponseShown = value;
	            RaisePropertyChanged(() => IsAnswerResponseShown);
            }
        }

	    private bool _isSatisfactionFeedback;
	    public bool IsSatisfactionFeedback
        {
	        get => _isSatisfactionFeedback;
	        set
	        {
	            _isSatisfactionFeedback = value;
	            RaisePropertyChanged(() => IsSatisfactionFeedback);
	        }
	    }

        private string _answerResponseResultIconName;
	    public string AnswerResponseResultIconName
        {
	        get => _answerResponseResultIconName;
	        set
	        {
	            _answerResponseResultIconName = value;
	            RaisePropertyChanged(() => AnswerResponseResultIconName);
	        }
	    }

	    private string _answerResponseResultText;
	    public string AnswerResponseResultText
        {
	        get => _answerResponseResultText;
	        set
	        {
	            _answerResponseResultText = value;
	            RaisePropertyChanged(() => AnswerResponseResultText);
	        }
	    }

        private string _unsatisfiedReasonText;
        public string UnsatisfiedReasonText
        {
            get => _unsatisfiedReasonText;
            set
            {
                _unsatisfiedReasonText = value;
                RaisePropertyChanged(() => UnsatisfiedReasonText);
            }
        }

        private bool _needToHideReplyHolder;
		public bool NeedToHideReplyHolder
		{
			get { return _needToHideReplyHolder; }
			set { SetProperty(ref _needToHideReplyHolder, value); }
		}

		private bool _needToShowPlaceHolder;
		public bool NeedToShowPlaceHolder
		{
			get { return _needToShowPlaceHolder; }
			set
			{
				_needToShowPlaceHolder = value;
				RaisePropertyChanged(() => NeedToShowPlaceHolder);
			}
		}

		private int _attachedImagesQuantity;

		public int AttachedImagesQuantity
		{
			get { return _attachedImagesQuantity; }
			set
			{
				_attachedImagesQuantity = value;
				RaisePropertyChanged(() => AttachedImagesQuantity);
			}
		}

		private string _fileName1;

		public string FileName1
		{
			get { return _fileName1; }
			set
			{
				_fileName1 = value;
				RaisePropertyChanged(() => FileName1);
			}
		}

		private string _fileName2;

		public string FileName2
		{
			get { return _fileName2; }
			set
			{
				_fileName2 = value;
				RaisePropertyChanged(() => FileName2);
			}
		}

		private string _fileName3;

		public string FileName3
		{
			get { return _fileName3; }
			set
			{
				_fileName3 = value;
				RaisePropertyChanged(() => FileName3);
			}
		}

		private bool _isIndicator1Shown;

		public bool IsIndicator1Shown
		{
			get { return _isIndicator1Shown; }
			set
			{
				_isIndicator1Shown = value;
				RaisePropertyChanged(() => IsIndicator1Shown);
			}
		}

		private bool _isAttachedFile1Shown;

		public bool IsAttachedFile1Shown
		{
			get { return _isAttachedFile1Shown; }
			set
			{
				_isAttachedFile1Shown = value;
				RaisePropertyChanged(() => IsAttachedFile1Shown);
			}
		}

		private bool _isAttachedFile2Shown;

		public bool IsAttachedFile2Shown
		{
			get { return _isAttachedFile2Shown; }
			set
			{
				_isAttachedFile2Shown = value;
				RaisePropertyChanged(() => IsAttachedFile2Shown);
			}
		}

		private bool _isAttachedFile3Shown;

		public bool IsAttachedFile3Shown
		{
			get { return _isAttachedFile3Shown; }
			set
			{
				_isAttachedFile3Shown = value;
				RaisePropertyChanged(() => IsAttachedFile3Shown);
			}
		}

		private string _fileUrl1;

		public string FileUrl1
		{
			get { return _fileUrl1; }
			set
			{
				_fileUrl1 = value;
				RaisePropertyChanged(() => FileUrl1);
			}
		}

		private string _fileUrl2;

		public string FileUrl2
		{
			get { return _fileUrl2; }
			set
			{
				_fileUrl2 = value;
				RaisePropertyChanged(() => FileUrl2);
			}
		}

		private string _fileUrl3;

		public string FileUrl3
		{
			get { return _fileUrl3; }
			set
			{
				_fileUrl3 = value;
				RaisePropertyChanged(() => FileUrl3);
			}
		}

		private bool _isKeyboardHidden;

		public bool IsKeyboardHidden
		{
			get { return _isKeyboardHidden; }
			set
			{
				_isKeyboardHidden = value;
				RaisePropertyChanged(() => IsKeyboardHidden);
			}
		}

		private bool _isAttachmentImagesShown;

		public bool IsAttachmentImagesShown
		{
			get { return _isAttachmentImagesShown; }
			set
			{
				_isAttachmentImagesShown = value;
				RaisePropertyChanged(() => IsAttachmentImagesShown);
			}
		}

		private string _complaintDetailURL;

		public string ComplaintDetailURL
		{
			get { return _complaintDetailURL; }
			set
			{
				_complaintDetailURL = value;
				RaisePropertyChanged(() => ComplaintDetailURL);
			}
		}

		private string[] _fileUrls;

		public string[] FileUrls
		{
			get { return _fileUrls; }
			set
			{
				_fileUrls = value;
				RaisePropertyChanged(() => FileUrls);
			}
		}

		private int _scrollPositionValue;

		public int ScrollPositionValue
		{
			get { return _scrollPositionValue; }
			set
			{
				_scrollPositionValue = value;
				RaisePropertyChanged(() => ScrollPositionValue);
			}
		}

		private bool _isKeyboardShown;

		public bool IsKeyboardShown
		{
			get { return _isKeyboardShown; }
			set
			{
				_isKeyboardShown = value;
				RaisePropertyChanged(() => IsKeyboardShown);
			}
		}

		private int _commentQuantity;

		public int CommentQuantity
		{
			get { return _commentQuantity; }
			set
			{
				_commentQuantity = value;
				RaisePropertyChanged(() => CommentQuantity);
			}
		}

		private List<Comment> _totalUserComment;

		public List<Comment> TotalUserComment
		{
			get { return _totalUserComment; }
			set
			{
				_totalUserComment = value;
				RaisePropertyChanged(() => TotalUserComment);
			}
		}

		private string _commentContent;

		public string CommentContent
		{
			get { return _commentContent; }
			set
			{
				_commentContent = value;
				RaisePropertyChanged(() => CommentContent);
			}
		}

		private string[] _imagePaths;

		public string[] ImagePaths
		{
			get { return _imagePaths; }
			set
			{
				_imagePaths = value;
				RaisePropertyChanged(() => ImagePaths);
			}
		}

		private string _imagePath1;

		public string ImagePath1
		{
			get { return _imagePath1; }
			set
			{
				_imagePath1 = value;
				RaisePropertyChanged(() => ImagePath1);
			}
		}

		private string _imagePath2;

		public string ImagePath2
		{
			get { return _imagePath2; }
			set
			{
				_imagePath2 = value;
				RaisePropertyChanged(() => ImagePath2);
			}
		}

		private string _imagePath3;

		public string ImagePath3
		{
			get { return _imagePath3; }
			set
			{
				_imagePath3 = value;
				RaisePropertyChanged(() => ImagePath3);
			}
		}

		private string _attachedImage1;

		public string AttachedImage1
		{
			get { return _attachedImage1; }
			set
			{
				_attachedImage1 = value;
				RaisePropertyChanged(() => AttachedImage1);
			}
		}

		private string _attachedImage2;

		public string AttachedImage2
		{
			get { return _attachedImage2; }
			set
			{
				_attachedImage2 = value;
				RaisePropertyChanged(() => AttachedImage2);
			}
		}

		private bool _isReplyShown;

		public bool IsReplyShown
		{
			get { return _isReplyShown; }
			set
			{
				_isReplyShown = value;
				RaisePropertyChanged(() => IsReplyShown);
			}
		}

		private bool _isAttachedImage1Shown;

		public bool IsAttachedImage1Shown
		{
			get { return _isAttachedImage1Shown; }
			set
			{
				_isAttachedImage1Shown = value;
				RaisePropertyChanged(() => IsAttachedImage1Shown);
			}
		}

		private bool _isAttachedImage2Shown;

		public bool IsAttachedImage2Shown
		{
			get { return _isAttachedImage2Shown; }
			set
			{
				_isAttachedImage2Shown = value;
				RaisePropertyChanged(() => IsAttachedImage2Shown);
			}
		}

		private bool _isImage1Shown;

		public bool IsImage1Shown
		{
			get { return _isImage1Shown; }
			set
			{
				_isImage1Shown = value;
				RaisePropertyChanged(() => IsImage1Shown);
			}
		}

		private bool _isImage2Shown;

		public bool IsImage2Shown
		{
			get { return _isImage2Shown; }
			set
			{
				_isImage2Shown = value;
				RaisePropertyChanged(() => IsImage2Shown);
			}
		}

		private bool _isImage3Shown;

		public bool IsImage3Shown
		{
			get { return _isImage3Shown; }
			set
			{
				_isImage3Shown = value;
				RaisePropertyChanged(() => IsImage3Shown);
			}
		}

		private bool _isVideoLinkShown;

		public bool IsVideoLinkShown
		{
			get { return _isVideoLinkShown; }
			set
			{
				_isVideoLinkShown = value;
				RaisePropertyChanged(() => IsVideoLinkShown);
			}
		}

		private int _imagesQuantity;

		public int ImagesQuantity
		{
			get { return _imagesQuantity; }
			set
			{
				_imagesQuantity = value;
				RaisePropertyChanged(() => ImagesQuantity);
			}
		}

		private List<CommentItemViewModel> _commentItemViewModels;

		public List<CommentItemViewModel> CommentItemViewModels
		{
			get { return _commentItemViewModels; }
			set
			{
				_commentItemViewModels = value;
				RaisePropertyChanged(() => CommentItemViewModels);
			}
		}

		private string[] _replyImages;

		public string[] ReplyImages
		{
			get { return _replyImages; }
			set
			{
				_replyImages = value;
				RaisePropertyChanged(() => ReplyImages);
			}
		}

		private string _title;

		public string Title
		{
			get { return _title; }
			set
			{
				_title = value;
				RaisePropertyChanged(() => Title);
			}
		}

		private string _location;

		public string Location
		{
			get { return _location; }
			set
			{
				_location = value;
				RaisePropertyChanged(() => Location);
			}
		}

		private string _dateCreated;

		public string DateCreated
		{
			get { return _dateCreated; }
			set
			{
				_dateCreated = value;
				RaisePropertyChanged(() => DateCreated);
			}
		}

		private double _rating;

		public double Rating
		{
			get { return _rating; }
			set
			{
				_rating = value;
				RaisePropertyChanged(() => Rating);
			}
		}

		private double _ratingCount;

		public double RatingCount
		{
			get { return _ratingCount; }
			set
			{
				_ratingCount = value;
				RaisePropertyChanged(() => RatingCount);
			}
		}

		private double _userRating;

		public double UserRating
		{
			get { return _userRating; }
			set
			{
				_userRating = value;
				RaisePropertyChanged(() => UserRating);
			}
		}

		private string _department;

		public string Department
		{
			get { return _department; }
			set
			{
				_department = value;
				RaisePropertyChanged(() => Department);
			}
		}

		private string _color;

		public string Color
		{
			get { return _color; }
			set
			{
				_color = value;
				RaisePropertyChanged(() => Color);
			}
		}

		private string _userName;

		public string UserName
		{
			get { return _userName; }
			set
			{
				_userName = value;
				RaisePropertyChanged(() => UserName);
			}
		}

		private string _email;

		public string Email
		{
			get { return _email; }
			set
			{
				_email = value;
				RaisePropertyChanged(() => Email);
			}
		}

		private string _phone;

		public string Phone
		{
			get { return _phone; }
			set
			{
				_phone = value;
				RaisePropertyChanged(() => Phone);
			}
		}

		private string _address;

		public string Address
		{
			get { return _address; }
			set
			{
				_address = value;
				RaisePropertyChanged(() => Address);
			}
		}

		private string _content;

		public string Content
		{
			get { return _content; }
			set
			{
				_content = value;
				RaisePropertyChanged(() => Content);
			}
		}

		private string _videoLink;

		public string VideoLink
		{
			get { return _videoLink; }
			set
			{
				_videoLink = value;
				RaisePropertyChanged(() => VideoLink);
			}
		}

		private string _video;

		public string Video
		{
			get { return _video; }
			set
			{
				_video = value;
				RaisePropertyChanged(() => Video);
			}
		}

		private int _numsComment;

		public int NumsComment
		{
			get { return _numsComment; }
			set
			{
				_numsComment = value;
				RaisePropertyChanged(() => NumsComment);
			}
		}

		private string _replyDepartment;

		public string ReplyDepartment
		{
			get { return _replyDepartment; }
			set
			{
				_replyDepartment = value;
				RaisePropertyChanged(() => ReplyDepartment);
			}
		}

		private string[] _fileNames;

		public string[] FileNames
		{
			get { return _fileNames; }
			set
			{
				_fileNames = value;
				RaisePropertyChanged(() => FileNames);
			}
		}

		private string _dateReplied;

		public string DateReplied
		{
			get { return _dateReplied; }
			set
			{
				_dateReplied = value;
				RaisePropertyChanged(() => DateReplied);
			}
		}

		private string _filePath;

		public string FilePath
		{
			get { return _filePath; }
			set
			{
				_filePath = value;
				RaisePropertyChanged(() => FilePath);
			}
		}

		private string _replyContent;

		public string ReplyContent
		{
			get { return _replyContent; }
			set
			{
				_replyContent = value;
				RaisePropertyChanged(() => ReplyContent);
			}
		}

		private bool _isAttachmentShown;

		public bool IsAttachmentShown
		{
			get { return _isAttachmentShown; }
			set
			{
				_isAttachmentShown = value;
				RaisePropertyChanged(() => IsAttachmentShown);
			}
		}

		#endregion

		#region UI Texts

		public string StaticComplaintDate => Localizer.GetText("StaticComplaintDate");
		public string StaticComplaintPlace => Localizer.GetText("StaticComplaintPlace");
		public string StaticDate => Localizer.GetText("StaticDate");
		public string StaticReply => Localizer.GetText("StaticReply");
		public string LeftParenthesis => Localizer.GetText("LeftParenthesis");
		public string RightParenthesis => Localizer.GetText("RightParenthesis");
		public string Comment => Localizer.GetText("Comment");
		public string CommentHint => Localizer.GetText("CommentHint");
		public string CommentButtonHint => Localizer.GetText("CommentButtonHint");
	    public string ResponseButtonHint => (IsFeedbackOfUser && !IsAnswerResponseShown) ? Localizer.GetText("ResponseButtonHint") : "";

        public string RatingCountText => Localizer.GetText("RatingCount");

		#endregion

		#region Command

		public ICommand RatingCommand { get; private set; }

		public ICommand AddCommentCommand { get; private set; }

		public ICommand CommentTouchCommand { get; private set; }

		public ICommand UserRatingCommand { get; private set; }

		public ICommand CheckNetworkCommand { get; private set; }

		public ICommand AttachedImage1ClickCommand { get; set; }

		public ICommand AttachedImage2ClickCommand { get; set; }

		public MvxCommand DetailImageClickCommand { get; set; }

		public MvxCommand<int> UserRatingCommandIos { get; private set; }

		private ICommand _confirmCancelCommand;

		public ICommand ConfirmCancelCommand
			=> _confirmCancelCommand ?? (_confirmCancelCommand = new MvxCommand(OnConfirmCancelCommand));

		private ICommand _openVideoLinkCommand;
		public ICommand OpenVideoLinkCommand => _openVideoLinkCommand ?? (_openVideoLinkCommand = new MvxCommand(OnOpenVideoLinkCommand));

	    public ICommand AnswerResponseCommand { get; private set; }

        
        private void OnOpenVideoLinkCommand()
		{
			var task = Mvx.Resolve<IMvxWebBrowserTask>();
			VideoLink = VideoLink.Replace(" ", string.Empty);

			task.ShowWebPage(VideoLink);
		}

		public static string ReplaceAt(string input, int index, char newChar)
		{
			if (input == null)
			{
				throw new ArgumentNullException(nameof(input));
			}
			var chars = input.ToCharArray();
			chars[index] = newChar;
			return new string(chars);
		}

		private void OnConfirmCancelCommand()
		{
			CommentContent = null;
			OnShowDetailView(typeof(HomeViewModel),
				delegate { ShowViewModel<HomeViewModel>(); });
		}

		#endregion
	}
}