﻿using System.Linq;
using System.Windows.Input;
using GovernmentComplaint.Core.Helpers;
using GovernmentComplaint.Core.Localization;
using GovernmentComplaint.Core.Models;
using GovernmentComplaint.Core.Services;
using GovernmentComplaint.Core.ViewModels.Bases;
using GovernmentComplaint.Core.ViewModels.ItemViewModels.Base;
using MvvmCross.Core.ViewModels;

namespace GovernmentComplaint.Core.ViewModels.ItemViewModels
{
	public class FeedbackItemViewModel : BaseListItemViewModel
	{
		private readonly ComplaintContent _complaintcontent;
		public string SolvedText => Localizer.GetText("Solved");
		public string DefaultDepartment => Localizer.GetText("DefaultDepartment");
		public string NoTitle => Localizer.GetText("NoTitle");

        #region Properties

        public ICommand FeedbackClicked { get; }
        private string _title;

        public string Title
        {
            get { return _title; }
            set
            {
                _title = value;
                SetProperty(ref _title, value);
            }
        }

        private string _dateCreated;

        public string DateCreated
        {
            get { return _dateCreated; }
            set
            {
                _dateCreated = value;
                SetProperty(ref _dateCreated, value);
            }
        }

        private double _rating;

        public double Rating
        {
            get { return _rating; }
            set
            {
                _rating = value;
                SetProperty(ref _rating, value);
            }
        }

        private int _numsComment;

        public int NumsComment
        {
            get { return _numsComment; }
            set
            {
                _numsComment = value;
                SetProperty(ref _numsComment, value);
            }
        }

        private int _numsLike;

        public int NumsLike
        {
            get { return _numsLike; }
            set
            {
                _numsLike = value;
                SetProperty(ref _numsLike, value);
            }
        }

        private string _department;

        public string Department
        {
            get { return _department; }
            set
            {
                _department = value;
                SetProperty(ref _department, value);
            }
        }

        private string _color;

        public string Color
        {
            get { return _color; }
            set
            {
                _color = value;
                SetProperty(ref _color, value);
            }
        }

        private bool _solved;

        public bool Solved
        {
            get { return _solved; }
            set
            {
                _solved = value;
                SetProperty(ref _solved, value);
            }
        }

        private string _image;

        public string Image
        {
            get { return _image; }
            set
            {
                _image = value;
                RaisePropertyChanged(() => Image);
            }
        }

        private bool _isSatisfiedStatusIconShown;

        public bool IsSatisfiedStatusIconShown
        {
            get => _isSatisfiedStatusIconShown;
            set
            {
                _isSatisfiedStatusIconShown = value;
                RaisePropertyChanged(() => IsSatisfiedStatusIconShown);
            }
        }

        private string _satisfiedStatusIconName;

        public string SatisfiedStatusIconName
        {
            get { return _satisfiedStatusIconName; }
            set
            {
                _satisfiedStatusIconName = value;
                RaisePropertyChanged(() => SatisfiedStatusIconName);
            }
        }

        public string CommentText => Localizer.GetText("CommentText");
        public string LikeText => Localizer.GetText("LikeText");

        #endregion

        public FeedbackItemViewModel(ComplaintContent content,IManagementService managementService, IDataModel dataModel, IResourceHelper resourceHelper)
			: base(managementService, dataModel, resourceHelper)
		{
			_complaintcontent = DataModel.ComplaintContentById(content.Id) ?? content;
			var timeCreated = _complaintcontent.TimeCreated;
			DateCreated = timeCreated + " " + _complaintcontent.DateCreated;
			Rating = _complaintcontent.RatingFeedback;
			NumsComment = _complaintcontent.NumsComment;
			NumsLike = _complaintcontent.NumsLike;
			Department = _complaintcontent.Department ?? DefaultDepartment;
			Color = string.IsNullOrEmpty(_complaintcontent.Color) ? "#0464A5" : _complaintcontent.Color.Trim();
			Solved = string.CompareOrdinal(_complaintcontent.Solved, SolvedText) == 0;
			Image = _complaintcontent.PictureStrings.FirstOrDefault();
			FeedbackClicked = new MvxCommand(OnFeedbackClickCommand);
			Title = !string.IsNullOrEmpty(_complaintcontent.Title) ? _complaintcontent.Title : NoTitle;
		    IsSatisfiedStatusIconShown = true;
		    SatisfiedStatusIcon("");
		}

        #region Method

        private void OnFeedbackClickCommand()
		{
			DataModel.CommentContent = null;
			MasterViewModel.Instance.ExecuteBeforeShowDetailView(typeof(ComplaintDetailViewModel), delegate
			{
				ShowViewModel<ComplaintDetailViewModel>(
					new
					{
						id = _complaintcontent.Id,
						pageType = MasterViewModel.Instance.TitleBar
					});
			});
		}

	    private void SatisfiedStatusIcon(string satisfiedStatusIcon)
	    {
	        switch (satisfiedStatusIcon)
	        {
	            case "satisfied":
	                SatisfiedStatusIconName = ResourceHelper.IconPath("satisfied");
                    break;
	            case "un_satified":
	                SatisfiedStatusIconName = ResourceHelper.IconPath("un_satified");
	                break;
	            default:
	                SatisfiedStatusIconName = "";
                    break;
            }

	    }
        #endregion

        
	}
}