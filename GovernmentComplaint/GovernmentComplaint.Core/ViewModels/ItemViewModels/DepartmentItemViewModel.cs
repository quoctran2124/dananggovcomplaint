﻿using GovernmentComplaint.Core.Helpers;
using GovernmentComplaint.Core.Models;
using GovernmentComplaint.Core.Services;
using GovernmentComplaint.Core.ViewModels.Bases;
using GovernmentComplaint.Core.ViewModels.ItemViewModels.Base;

namespace GovernmentComplaint.Core.ViewModels.ItemViewModels
{
	public class DepartmentItemViewModel : BaseDropdownItemViewModel
	{
		public static DepartmentItemViewModel SelectedItem;
		private readonly Department _department;

		public DepartmentItemViewModel(Department department, IManagementService managementService, IDataModel dataModel,
		   IResourceHelper resourceHelper) : base(managementService, dataModel, resourceHelper)
		{
			_department = dataModel.DepartmentById(department.Id);
			ItemTitle = _department.Name;
		}

		protected override void UnregisteredEvent()
		{
		}

		protected override void OnItemClickCommand()
		{
			SelectedItem = this;
			MasterViewModel.Instance.SelectedDepartment = _department.Name;
			MasterViewModel.Instance.SelectedDepartmentId = _department.Id;
			MasterViewModel.Instance.IsDropdownPopupShown = false;
		}
	}
}