﻿using GovernmentComplaint.Core.Helpers;
using GovernmentComplaint.Core.Localization;
using GovernmentComplaint.Core.Models;
using GovernmentComplaint.Core.Services;
using GovernmentComplaint.Core.ViewModels.Bases;
using GovernmentComplaint.Core.ViewModels.ItemViewModels.Base;

namespace GovernmentComplaint.Core.ViewModels.ItemViewModels
{
	public class HistorySortingItemViewModel : BaseDropdownItemViewModel
	{
		public HistorySortingItemViewModel(SortingCondition sort, IManagementService managementService, IDataModel dataModel,
		   IResourceHelper resourceHelper) : base(managementService, dataModel, resourceHelper)
		{
			SortId = sort.SortId;
			SortCondition = sort.SortCondition;
			SortImage = sort.SortImage;
		}

		protected override void OnItemClickCommand()
		{
			SelectedItem = this;
			MasterViewModel.Instance.IsIndicatorShown = true;
			DataModel.HistorySortFilter = SelectedItem.SortId;
			DataModel.ComplaintHistory = null;
			MasterViewModel.Instance.HistoryViewModel.MoveListToTop = true;
			ManagementService.GetHistoryComplaints(1, DataModel.HistorySortFilter);
		}

		protected override void UnregisteredEvent()
		{
		}

		#region Dropdown text

		public string Time => Localizer.GetText("Time");
		public string Interest => Localizer.GetText("Interest");
		public string Comment => Localizer.GetText("Comment");

		#endregion

		#region Properties

		public static HistorySortingItemViewModel SelectedItem;

		private int _sortId;

		public int SortId
		{
			get { return _sortId; }
			set
			{
				_sortId = value;
				RaisePropertyChanged(() => SortId);
			}
		}

		private string _sortCondition;

		public string SortCondition
		{
			get { return _sortCondition; }
			set
			{
				_sortCondition = value;
				RaisePropertyChanged(() => SortCondition);
			}
		}

		private string _sortImage;

		public string SortImage
		{
			get { return _sortImage; }
			set
			{
				_sortImage = value;
				RaisePropertyChanged(() => SortImage);
			}
		}

		#endregion
	}
}