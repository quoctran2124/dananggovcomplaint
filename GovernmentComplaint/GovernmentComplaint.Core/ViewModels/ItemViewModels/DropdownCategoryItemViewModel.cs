using GovernmentComplaint.Core.Helpers;
using GovernmentComplaint.Core.Models;
using GovernmentComplaint.Core.Services;
using GovernmentComplaint.Core.ViewModels.Bases;
using GovernmentComplaint.Core.ViewModels.ItemViewModels.Base;
using MvvmCross.Platform;

namespace GovernmentComplaint.Core.ViewModels.ItemViewModels
{
	public class DropdownCategoryItemViewModel : BaseDropdownItemViewModel
	{
		public static DropdownCategoryItemViewModel SelectedItem;
		private readonly INetworkHelper _networkHelper;

		#region Properties

		private int _catId;

		public int CatId
		{
			get { return _catId; }
			set
			{
				_catId = value;
				RaisePropertyChanged(() => CatId);
			}
		}

		private string _catName;

		public string CatName
		{
			get { return _catName; }
			set
			{
				_catName = value;
				RaisePropertyChanged(() => CatName);
			}
		}

		private string _catImage;

		public string CatImage
		{
			get { return _catImage; }
			set
			{
				_catImage = value;
				RaisePropertyChanged(() => CatImage);
			}
		}

		#endregion

		#region Method

		public DropdownCategoryItemViewModel(Category cat, IManagementService managementService, IDataModel dataModel,
			IResourceHelper resourceHelper) : base(managementService, dataModel, resourceHelper)
		{
			_networkHelper = Mvx.Resolve<INetworkHelper>();
			var category = dataModel.CategoryById(cat.Id);
			CatId = category.Id;
			CatName = category.CatName;
			CatImage = category.Image;
		}

		#endregion

		protected override void OnItemClickCommand()
		{
			if (_networkHelper.IsConnected)
			{
				DataModel.ComplaintContents = null;
				MasterViewModel.Instance.IsIndicatorShown = true;
				MasterViewModel.Instance.HomeViewModel.MoveListToTop = true;
				SelectedItem = this;
				MasterViewModel.Instance.HomeViewModel.FeedbackItemViewModels = null;
				DataModel.CatFilter = SelectedItem.CatId;
				ManagementService.GetComplaints(1, DataModel.CatFilter, DataModel.SortFilter, DataModel.Keyword,
					DataModel.DepartmentId);
			}
			else
			{
				MasterViewModel.Instance.IsNoWifiPopupShown = true;
			}
		}

		protected override void UnregisteredEvent()
		{
		}
	}
}