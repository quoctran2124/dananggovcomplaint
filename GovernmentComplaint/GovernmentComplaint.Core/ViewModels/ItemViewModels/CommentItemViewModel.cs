﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using GovernmentComplaint.Core.Helpers;
using GovernmentComplaint.Core.Localization;
using GovernmentComplaint.Core.Models;
using GovernmentComplaint.Core.Services;
using GovernmentComplaint.Core.ViewModels.Bases;
using GovernmentComplaint.Core.ViewModels.ItemViewModels.Base;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform;

namespace GovernmentComplaint.Core.ViewModels.ItemViewModels
{
	public class CommentItemViewModel : BaseListItemViewModel
	{
		#region UI Texts

		public string StaticReplyText => Localizer.GetText("StaticReplyText");
		public string StaticLikeText => Localizer.GetText("StaticLikeText");
		public string CommentButtonHint => Localizer.GetText("CommentButtonHint");
		public string CommentHint => Localizer.GetText("CommentHint");

		#endregion

		#region Properties

		private readonly INetworkHelper _networkHelper;

		private bool _needToShowPlaceHolder;
		public bool NeedToShowPlaceHolder
		{
			get { return _needToShowPlaceHolder; }
			set { SetProperty(ref _needToShowPlaceHolder, value); }
		}

		private readonly int _detailId, _commentId;
		private readonly int _likeQuantity;

		private bool _isKeyboardHidden;

		public bool IsKeyboardHidden
		{
			get { return _isKeyboardHidden; }
			set
			{
				_isKeyboardHidden = value;
				RaisePropertyChanged(() => IsKeyboardHidden);
			}
		}

		private List<ChildrenCommentItemViewModel> _childrenCommentItemViewModels;

		public List<ChildrenCommentItemViewModel> ChildrenCommentItemViewModels
		{
			get { return _childrenCommentItemViewModels; }
			set
			{
				_childrenCommentItemViewModels = value;
				RaisePropertyChanged(() => ChildrenCommentItemViewModels);
			}
		}

		private int _childrenCommentCount;

		public int ChildrenCommentCount
		{
			get { return _childrenCommentCount; }
			set
			{
				_childrenCommentCount = value;
				RaisePropertyChanged(() => ChildrenCommentCount);
			}
		}

		private string _userName;

		public string UserName
		{
			get { return _userName; }
			set
			{
				_userName = value;
				SetProperty(ref _userName, value);
			}
		}

		private string _commentDate;

		public string CommentDate
		{
			get { return _commentDate; }
			set
			{
				_commentDate = value;
				SetProperty(ref _commentDate, value);
			}
		}

		private string _commentContent;

		public string CommentContent
		{
			get { return _commentContent; }
			set
			{
				_commentContent = value;
				SetProperty(ref _commentContent, value);
			}
		}

		private int _likeText;

		public int LikeText
		{
			get { return _likeText; }
			set
			{
				_likeText = value;
				RaisePropertyChanged(() => LikeText);
			}
		}

		private bool _isChildrenCommentShown;

		public bool IsChildrenCommentShown
		{
			get { return _isChildrenCommentShown; }
			set
			{
				_isChildrenCommentShown = value;
				RaisePropertyChanged(() => IsChildrenCommentShown);
			}
		}

		private bool _isReplyCommentLayoutShown;

		public bool IsReplyCommentLayoutShown
		{
			get { return _isReplyCommentLayoutShown; }
			set
			{
				_isReplyCommentLayoutShown = value;
				RaisePropertyChanged(() => IsReplyCommentLayoutShown);
			}
		}

		private string _enteredCommentContent;

		public string EnteredCommentContent
		{
			get { return _enteredCommentContent; }
			set
			{
				_enteredCommentContent = value;
				RaisePropertyChanged(() => EnteredCommentContent);
			}
		}

		#endregion

		#region Commands

		public ICommand ReplyCommand { get; private set; }

		public ICommand LikeCommand { get; private set; }

		public ICommand AddCommentCommand { get; private set; }

		#endregion

		#region Method

		public CommentItemViewModel(Comment comment, int detailId, IManagementService managementService, IDataModel dataModel,
			IResourceHelper resourceHelper)
			: base(managementService, dataModel, resourceHelper)
		{
			_detailId = detailId;
			DataModel.DataReceived += OnDataReceived;
			_commentId = comment.Id;
			UserName = comment.Username;
			CommentContent = comment.CommentContent;
			LikeText = comment.LikeCount;
			CommentDate = Convert.ToDateTime(comment.CommentTime).ToString("HH:mm dd/MM/yyyy");
			ChildrenCommentItemViewModels = DataModel.ComplaintContent.CommentList.
				Where(
					m =>
						(m.ParentId == comment.Id || CheckParentId(m.ParentId, comment.Id)) &&
						string.Equals(m.InteractionType, "BINH_LUAN")).
				Select(m => new ChildrenCommentItemViewModel(m, _detailId, managementService, dataModel,resourceHelper)).ToList();
			if (ChildrenCommentItemViewModels != null)
			{
				IsChildrenCommentShown = true;
				ChildrenCommentCount = ChildrenCommentItemViewModels.Count;
			}
			InitCommand();
			_likeQuantity = LikeText;
			_networkHelper = Mvx.Resolve<INetworkHelper>();
		}

		private bool CheckParentId(int? parentId, int commentId)
		{
			return (from t in DataModel.ComplaintContent.CommentList
				where parentId == t.Id
				select t.ParentId == commentId || CheckParentId(t.ParentId, commentId)).FirstOrDefault();

			#region Alternative method

			//foreach (var t in _dataModel.ComplaintContent.CommentList)
			//{
			//    if (parentId == t.CommentId)
			//    {
			//        return t.ParentId == commentId || CheckParentId(t.ParentId, commentId);
			//    }

			//}

			#endregion
		}

		private void InitCommand()
		{
			ReplyCommand = new MvxCommand(OnReplyCommand);
			LikeCommand = new MvxCommand(OnLikeCommand);
			AddCommentCommand = new MvxCommand(OnAddCommentCommand);
		}

		private void OnAddCommentCommand()
		{
			IsKeyboardHidden = true;
			if (string.IsNullOrWhiteSpace(EnteredCommentContent))
			{
				MasterViewModel.Instance.IsNoCommentPopupShown = true;
			}
			else
			{
				if (!MasterViewModel.Instance.IsLoggedIn)
				{
					MasterViewModel.Instance.ExecuteBeforeShowDetailView(typeof(SecondLoginScreenViewModel),
										delegate { ShowViewModel<SecondLoginScreenViewModel>(new
										{
											previousPage = DataModel.PreviousPageString
										}); });
				}
				else
				{
					if (DataModel.UserInfo == null)
					{
						MasterViewModel.Instance.ExecuteBeforeShowDetailView(typeof(AddPersonalInfoViewModel),
										delegate { ShowViewModel<AddPersonalInfoViewModel>(new
										{
											previousPage = DataModel.PreviousPageString
										}); });
					}
					else
					{
						if (_networkHelper.IsConnected)
						{
							ManagementService.PostComment(_detailId, DataModel.UserInfo.UserId, _commentId, EnteredCommentContent,
								"BINH_LUAN");
							DataModel.CommentContent = EnteredCommentContent;
							EnteredCommentContent = "";
							IsReplyCommentLayoutShown = false;
							NeedToShowPlaceHolder = true;
						}
						else
						{
							MasterViewModel.Instance.IsNoWifiPopupShown = true;
						}
					}
				}
				
			}
		}
		
		private async void OnLikeCommand()
		{
			if (!MasterViewModel.Instance.IsLoggedIn)
			{
				MasterViewModel.Instance.ExecuteBeforeShowDetailView(typeof(SecondLoginScreenViewModel),
					delegate { ShowViewModel<SecondLoginScreenViewModel>(new
					{
						previousPage = DataModel.PreviousPageString
					}); });
			}
			else
			{
				if (DataModel.UserInfo != null)
				{
					var isLiked =
						await ManagementService.LikeComment(_detailId, DataModel.UserInfo.UserId, _commentId, null,
							"QUAN_TAM");
					if (isLiked)
					{
						if (LikeText == _likeQuantity)
						{
							LikeText++;
						}
						else
						{
							LikeText--;
						}
					}
					else
					{
						if (_networkHelper.IsConnected)
						{
							MasterViewModel.Instance.IsFail24HPopupShown = true;
						}
					}
				}
				else
				{
					MasterViewModel.Instance.ExecuteBeforeShowDetailView(typeof(AddPersonalInfoViewModel),
					delegate { ShowViewModel<AddPersonalInfoViewModel>(new
					{
						previousPage = DataModel.PreviousPageString
					}); });
				}
			}
		}

		private void OnReplyCommand()
		{
		   if (!MasterViewModel.Instance.IsLoggedIn)
			{
				MasterViewModel.Instance.ExecuteBeforeShowDetailView(typeof(SecondLoginScreenViewModel),
					delegate { ShowViewModel<SecondLoginScreenViewModel>(new
					{
						previousPage = DataModel.PreviousPageString
					}); });
			}
			else
			{
				if (DataModel.UserInfo == null)
				{
					MasterViewModel.Instance.ExecuteBeforeShowDetailView(typeof(AddPersonalInfoViewModel),
						delegate { ShowViewModel<AddPersonalInfoViewModel>(new
						{
							previousPage = DataModel.PreviousPageString
						}); });
				}
				else
				{
					IsReplyCommentLayoutShown = !IsReplyCommentLayoutShown;
				}
			}
		}

		#endregion
	}
}