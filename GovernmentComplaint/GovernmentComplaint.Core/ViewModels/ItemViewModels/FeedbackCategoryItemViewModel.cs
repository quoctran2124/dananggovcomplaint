using GovernmentComplaint.Core.Helpers;
using GovernmentComplaint.Core.Models;
using GovernmentComplaint.Core.Services;
using GovernmentComplaint.Core.ViewModels.Bases;
using GovernmentComplaint.Core.ViewModels.ItemViewModels.Base;
using MvvmCross.Platform;

namespace GovernmentComplaint.Core.ViewModels.ItemViewModels
{
	public class FeedbackCategoryItemViewModel : BaseDropdownItemViewModel
	{
		public static FeedbackCategoryItemViewModel SelectedItem;
		private readonly INetworkHelper _networkHelper;

	    public string Slash => " /";

        #region Properties

        private int _fieldOfSuggestionId;

		public int FieldOfSuggestionId
        {
			get => _fieldOfSuggestionId;
		    set
			{
			    _fieldOfSuggestionId = value;
				RaisePropertyChanged(() => FieldOfSuggestionId);
			}
		}

		private string _fieldOfSuggestionName;

		public string FieldOfSuggestionName
        {
			get => _fieldOfSuggestionName;
		    set
			{
			    _fieldOfSuggestionName = value;
				RaisePropertyChanged(() => FieldOfSuggestionName);
			}
		}

	    private string _feedbackProcessed;

	    public string FeedbackProcessed
        {
	        get => _feedbackProcessed;
	        set
	        {
	            _feedbackProcessed = value;
	            RaisePropertyChanged(() => FeedbackProcessed);
	        }
	    }

	    private string _totalFeedback;

	    public string TotalFeedback
        {
	        get => _totalFeedback;
	        set
	        {
	            _totalFeedback = value;
	            RaisePropertyChanged(() => TotalFeedback);
	        }
	    }
        #endregion

	    public FeedbackCategoryItemViewModel(FieldOfSuggestion fieldOfSuggestion, IManagementService managementService, IDataModel dataModel,
	        IResourceHelper resourceHelper) : base(managementService, dataModel, resourceHelper)
	    {
	        _networkHelper = Mvx.Resolve<INetworkHelper>();

	        FieldOfSuggestionId = fieldOfSuggestion.Id;
	        FieldOfSuggestionName = fieldOfSuggestion.UnitName;
	        FeedbackProcessed = fieldOfSuggestion.FeedbackProcessed;
	        TotalFeedback = fieldOfSuggestion.TotalFeedback;
	    }

        #region Method
        protected override void UnregisteredEvent()
	    {
	    }

	    protected override void OnItemClickCommand()
	    {
	    }
        #endregion
    }
}