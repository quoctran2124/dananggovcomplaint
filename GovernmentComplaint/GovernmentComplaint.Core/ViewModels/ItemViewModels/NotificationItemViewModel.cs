﻿using System;
using System.Linq;
using System.Windows.Input;
using GovernmentComplaint.Core.Helpers;
using GovernmentComplaint.Core.Localization;
using GovernmentComplaint.Core.Models;
using GovernmentComplaint.Core.Services;
using GovernmentComplaint.Core.ViewModels.Bases;
using GovernmentComplaint.Core.ViewModels.ItemViewModels.Base;
using MvvmCross.Core.ViewModels;

namespace GovernmentComplaint.Core.ViewModels.ItemViewModels
{
	public class NotificationItemViewModel : BaseListItemViewModel
	{
		private readonly Notification _notification;

		public NotificationItemViewModel(Notification notification, IManagementService managementService,
			IDataModel dataModel,
			IResourceHelper resourceHelper)
			: base(managementService, dataModel, resourceHelper)
		{
			_notification = notification;

			NotificationClicked = new MvxCommand(OnNotificationClickCommand);

			_notificationContent = "<b>" + ConvertPersonReply(_notification.DepartmentReply, _notification.UserReply) +
								   " </b> " + ConvertContentNotification(_notification.Action, _notification.Content) +
								   " <b>" +
								   _notification.Location + "</b>";
			_notificationTime = ConvertTime(_notification.Time);
			_imageSource = _notification.ImageSource;
			StatusIcon = ResourceHelper.IconPath("status_" + _notification.Status);

			if (_notification.Action.Equals("BINH_LUAN"))
				StatusIcon = ResourceHelper.IconPath("status_commented");
			else if (_notification.Action.Equals("DA_XU_LY") || _notification.Action.Equals("XUAT_BAN"))
				StatusIcon = ResourceHelper.IconPath("status_solved");
			else
				StatusIcon = ResourceHelper.IconPath("status_liked");

			BackgroundColor = string.CompareOrdinal(_notification.Status, "DA_XEM") == 0 ? "#FEFEFE" : "#F0F0F0";
		}

		private static string NotiComplaint => Localizer.GetText("NotiComplaint");
		private static string NotiProcessed => Localizer.GetText("NotiProcessed");
		private static string NotiPublished => Localizer.GetText("NotiPublished");
		private static string NotiRespondent => Localizer.GetText("NotiRespondent");
		private static string NotiReply => Localizer.GetText("NotiReply");

		private string ConvertPersonReply(string department, string user)
		{
			if (department != "") return department;
			return user != "" ? user : NotiRespondent;
		}

		private string ConvertTime(string time)
		{
			try
			{
				if (time == "")
					return time;
				time = time.Trim();
				time = time.Remove(time.LastIndexOf(":", StringComparison.Ordinal));
				var temp = time.Substring(time.LastIndexOf(" ", StringComparison.Ordinal));
				time = time.Remove(time.LastIndexOf(" ", StringComparison.Ordinal));
				temp += " " + time.Substring(time.LastIndexOf("-", StringComparison.Ordinal) + 1) + "/";
				time = time.Remove(time.LastIndexOf("-", StringComparison.Ordinal));
				temp += " " + time.Substring(time.LastIndexOf("-", StringComparison.Ordinal) + 1) + "/";
				time = time.Remove(time.LastIndexOf("-", StringComparison.Ordinal));
				temp += time;
				return temp;
			}
			catch
			{
				return "";
			}
		}

		private string ConvertContentNotification(string action, string content)
		{
			string temp2 = "", temp1 = "";
			if (action == "BINH_LUAN") temp1 = NotiReply;
			if (action == "DA_XU_LY") temp1 = NotiProcessed;
			if (action == "XUAT_BAN") temp1 = NotiPublished;
			if (content == "Y_KIEN") temp2 = NotiComplaint;
			if (content == "BINH_LUAN") temp2 = NotiComplaint;
			return temp1 + " " + temp2;
		}

		private void OnNotificationClickCommand()
		{
			ManagementService.ChangeNotificationStatus(_notification.Id);
		    var notification = DataModel.Notifications?.FirstOrDefault(n => n.Id == _notification.Id);
		    if (notification != null)
		    {
		        notification.Status = "DA_XEM";
		        MasterViewModel.Instance.ExecuteBeforeShowDetailView(typeof(ComplaintDetailViewModel),
		            delegate
		            {
		                ShowViewModel<ComplaintDetailViewModel>(
		                    new { id = _notification.ComplaintId, pageType = MasterViewModel.Instance.TitleBar });
		            });
		    }
		}

		#region Properties

		public ICommand NotificationClicked { get; }

		private string _backgroundColor;

		public string BackgroundColor
		{
			get { return _backgroundColor; }
			set
			{
				_backgroundColor = value;
				RaisePropertyChanged(() => BackgroundColor);
			}
		}

		private string _notificationContent;

		public string NotificationContent
		{
			get { return _notificationContent; }
			set
			{
				_notificationContent = value;
				SetProperty(ref _notificationContent, value);
			}
		}

		private string _notificationTime;

		public string NotificationTime
		{
			get { return _notificationTime; }
			set
			{
				_notificationTime = value;
				SetProperty(ref _notificationTime, value);
			}
		}

		private string _imageSource;

		public string ImageSource
		{
			get { return _imageSource; }
			set
			{
				_imageSource = value;
				SetProperty(ref _imageSource, value);
			}
		}

		private string _statusIcon;

		public string StatusIcon
		{
			get { return _statusIcon; }
			set
			{
				_statusIcon = value;
				SetProperty(ref _statusIcon, value);
			}
		}

		private int _notificationId;

		public int NotificationId
		{
			get { return _notificationId; }
			set
			{
				_notificationId = value;
				SetProperty(ref _notificationId, value);
			}
		}

		#endregion
	}
}