﻿using GovernmentComplaint.Core.Helpers;
using GovernmentComplaint.Core.Models;
using GovernmentComplaint.Core.Services;
using GovernmentComplaint.Core.ViewModels.Bases;
using GovernmentComplaint.Core.ViewModels.ItemViewModels.Base;
using MvvmCross.Platform;

namespace GovernmentComplaint.Core.ViewModels.ItemViewModels
{
	public class DropdownSortingItemViewModel : BaseDropdownItemViewModel
	{
		public static DropdownSortingItemViewModel SelectedItem;
		private readonly INetworkHelper _networkHelper;

		#region Methods

		public DropdownSortingItemViewModel(SortingCondition sort, IManagementService managementService,
			IDataModel dataModel, IResourceHelper resourceHelper) : base(managementService, dataModel, resourceHelper)
		{
			_networkHelper = Mvx.Resolve<INetworkHelper>();
			SortId = sort.SortId;
			SortCondition = sort.SortCondition;
			SortImage = sort.SortImage;
		}

		#endregion

		protected override void OnItemClickCommand()
		{
			if (_networkHelper.IsConnected)
			{
				MasterViewModel.Instance.IsIndicatorShown = true;
				MasterViewModel.Instance.HomeViewModel.MoveListToTop = true;
				SelectedItem = this;
				DataModel.ComplaintContents = null;
				MasterViewModel.Instance.HomeViewModel.FeedbackItemViewModels = null;
				DataModel.SortFilter = SelectedItem.SortId;
				ManagementService.GetComplaints(1, DataModel.CatFilter, DataModel.SortFilter, DataModel.Keyword,
					DataModel.DepartmentId);
			}
			else
			{
				MasterViewModel.Instance.IsNoWifiPopupShown = true;
			}
		}

		protected override void UnregisteredEvent()
		{
		}

		#region Properties

		private int _sortId;

		public int SortId
		{
			get { return _sortId; }
			set
			{
				_sortId = value;
				RaisePropertyChanged(() => SortId);
			}
		}

		private string _sortCondition;

		public string SortCondition
		{
			get { return _sortCondition; }
			set
			{
				_sortCondition = value;
				RaisePropertyChanged(() => SortCondition);
			}
		}

		private string _sortImage;

		public string SortImage
		{
			get { return _sortImage; }
			set
			{
				_sortImage = value;
				RaisePropertyChanged(() => SortImage);
			}
		}

		#endregion
	}
}