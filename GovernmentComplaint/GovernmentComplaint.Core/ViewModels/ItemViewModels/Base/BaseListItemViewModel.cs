﻿using GovernmentComplaint.Core.Helpers;
using GovernmentComplaint.Core.Models;
using GovernmentComplaint.Core.Services;
using GovernmentComplaint.Core.ViewModels.Bases;

namespace GovernmentComplaint.Core.ViewModels.ItemViewModels.Base
{
    public class BaseListItemViewModel : BaseViewModel
    {
        protected BaseListItemViewModel(IManagementService managementService, IDataModel dataModel,
            IResourceHelper resourceHelper)
            : base(managementService, dataModel, resourceHelper)
        {
           
        }
        protected override void UnregisteredEvent()
        {
        }
    }
}