﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using GovernmentComplaint.Core.Helpers;
using GovernmentComplaint.Core.Models;
using GovernmentComplaint.Core.Services;
using GovernmentComplaint.Core.ViewModels.Bases;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform;

namespace GovernmentComplaint.Core.ViewModels.ItemViewModels.Base
{
    public abstract class BaseDropdownItemViewModel : BaseViewModel
    {
        protected BaseDropdownItemViewModel(IManagementService managementService, IDataModel dataModel,
           IResourceHelper resourceHelper):base(managementService,dataModel,resourceHelper)
        {
            ItemClickCommand = new MvxCommand(OnItemClickCommand);
        }

        protected BaseDropdownItemViewModel(IManagementService managementService) : base(managementService)
        {
            ItemClickCommand = new MvxCommand(OnItemClickCommand);
        }

        #region Commands

        public ICommand ItemClickCommand { get; }

        #endregion
        
        #region Method

        protected abstract void OnItemClickCommand();

        protected async void ShowViewModelOfType(Type type)
        {
            await Task.Delay(ConstVariable.TimeToFadeOut);
            MasterViewModel.Instance.ExecuteBeforeShowDetailView(type, delegate { ShowViewModel(type); });
        }

        #endregion

        #region Properties

        private string _itemTitle;

        public string ItemTitle
        {
            get { return _itemTitle; }
            set
            {
                _itemTitle = value;
                RaisePropertyChanged(() => ItemTitle);
            }
        }

        private string _iconPath;

        public string IconPath
        {
            get { return _iconPath; }
            set
            {
                _iconPath = value;
                RaisePropertyChanged(() => IconPath);
            }
        }

        #endregion
    }
}