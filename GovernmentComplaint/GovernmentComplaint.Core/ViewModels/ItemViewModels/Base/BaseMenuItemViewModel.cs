﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using GovernmentComplaint.Core.Enums;
using GovernmentComplaint.Core.Helpers;
using GovernmentComplaint.Core.Models;
using GovernmentComplaint.Core.Services;
using GovernmentComplaint.Core.ViewModels.Bases;
using MvvmCross.Core.ViewModels;

namespace GovernmentComplaint.Core.ViewModels.ItemViewModels.Base
{
    public abstract class BaseMenuItemViewModel : BaseViewModel
    {
        protected BaseMenuItemViewModel(IManagementService managementService, IDataModel dataModel,
            IResourceHelper resourceHelper) :
            base(managementService, dataModel, resourceHelper)
        {
            ItemClickCommand = new MvxCommand(OnItemClickCommand);
        }

        #region Commands

        public ICommand ItemClickCommand { get; }

        #endregion

        #region Members

        protected string ActiveIconPath;
        protected string InactiveIconPath;

        #endregion

        #region Method

        protected abstract void OnItemClickCommand();

        protected internal async void ShowViewModelOfType(Type type)
        {
            ItemStatus = ItemStatus.Active;
            LabelStatus = LabelStatus.Active;

            await Task.Delay(ConstVariable.TimeToFadeOut);

            MasterViewModel.Instance.ExecuteBeforeShowDetailView(type, delegate { ShowViewModel(type); });
        }

        protected void UpdateItemIcon()
        {
            IconPath = ResourceHelper.IconPath(ItemStatus == ItemStatus.Active ? ActiveIconPath : InactiveIconPath);
        }

        #endregion

        #region Properties

        private string _itemTitle;

        public string ItemTitle
        {
            get { return _itemTitle; }
            set
            {
                _itemTitle = value;
                RaisePropertyChanged(() => ItemTitle);
            }
        }


        private string _iconPath;

        public string IconPath
        {
            get { return _iconPath; }
            set
            {
                _iconPath = value;
                RaisePropertyChanged(() => IconPath);
            }
        }


        private ItemStatus _itemStatus = ItemStatus.Inactive;

        public ItemStatus ItemStatus
        {
            get { return _itemStatus; }
            set
            {
                _itemStatus = value;
                RaisePropertyChanged(() => ItemStatus);
                UpdateItemIcon();
            }
        }

        private LabelStatus _labelStatus = LabelStatus.Inactive;

        public LabelStatus LabelStatus
        {
            get { return _labelStatus; }
            set
            {
                _labelStatus = value;
                RaisePropertyChanged(() => LabelStatus);
            }
        }

        #endregion
    }
}