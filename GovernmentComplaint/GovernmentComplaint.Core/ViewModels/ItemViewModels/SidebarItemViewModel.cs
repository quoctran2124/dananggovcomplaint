﻿using System;
using System.Net;
using GovernmentComplaint.Core.Enums;
using GovernmentComplaint.Core.Helpers;
using GovernmentComplaint.Core.Localization;
using GovernmentComplaint.Core.Models;
using GovernmentComplaint.Core.Services;
using GovernmentComplaint.Core.ViewModels.Bases;
using GovernmentComplaint.Core.ViewModels.ItemViewModels.Base;
using MvvmCross.Platform;

namespace GovernmentComplaint.Core.ViewModels.ItemViewModels
{
	public class SidebarItemViewModel : BaseMenuItemViewModel
	{
		#region Method

		private readonly IBadgeHelper _badgeHelper;

		public SidebarItemViewModel(SidebarMenuType menuType, IManagementService managementService, IDataModel dataModel,
			IResourceHelper resourceHelper) :
			base(managementService, dataModel, resourceHelper)
		{
			_badgeHelper = Mvx.Resolve< IBadgeHelper>();
			MenuType = menuType;
			ItemTitle = Localizer.GetText(menuType.ToString());
			ActiveIconPath = EnumHelper.GetDisplayValue(menuType) + "_active";
			InactiveIconPath = EnumHelper.GetDisplayValue(menuType);
			switch (MenuType)
			{
				case SidebarMenuType.NewComplaint:
					LabelStatus = LabelStatus.Special;
					break;
				case SidebarMenuType.Home:
					LabelStatus = LabelStatus.Active;
					ItemStatus = ItemStatus.Active;
					break;
				case SidebarMenuType.Notification:
					WidthCircle = 15;
					DataModel.DataReceived += OnDataReceived;
					break;
			}
			IsCountShow = false;
			UpdateItemIcon();
		}

		protected override void HandleDataReceived(WsCommand command, HttpStatusCode code)
		{
			base.HandleDataReceived(command, code);
			if (code == HttpStatusCode.OK && command == WsCommand.CountNotification)
			{
				if (DataModel.CountNotification <= 0)
				{
					TotalNotification = 0;
					_badgeHelper.ClearBadge();
					IsCountShow = false;
				}
				else
				{
					TotalNotification = DataModel.CountNotification;
					_badgeHelper.SetBadge(TotalNotification);
					IsCountShow = true;
				}
			}
		}

		protected override void OnItemClickCommand()
		{
			foreach (var sidebarItem in MasterViewModel.Instance.SidebarItemViewModels)
				if (sidebarItem.ItemStatus == ItemStatus.Active && sidebarItem.LabelStatus == LabelStatus.Active)
				{
					sidebarItem.LabelStatus = sidebarItem.MenuType == SidebarMenuType.NewComplaint
						? LabelStatus.Special
						: LabelStatus.Inactive;

					sidebarItem.ItemStatus = ItemStatus.Inactive;
				}

			SelectedItem = this;
			ItemStatus = ItemStatus.Active;
			LabelStatus = LabelStatus.Active;

			if (SelectedItem.MenuType != SidebarMenuType.Logout)
			{
				MasterViewModel.Instance.IsSidebarShown = false;
				MasterViewModel.Instance.IsOverlayShown = false;
			}

			switch (MenuType)
			{
				case SidebarMenuType.Home:
					MasterViewModel.Instance.TopRightButtonType = HeaderButtonType.Menu;
					ClearHomePageData();
					ShowViewModelOfType(typeof(HomeViewModel));
					ClearHomePageData();
					break;
				case SidebarMenuType.History:
					DataModel.ComplaintHistory = null;
					MasterViewModel.Instance.TopRightButtonType = HeaderButtonType.Menu;
					ShowViewModelOfType(typeof(HistoryViewModel));
					break;
				case SidebarMenuType.Intro:
					MasterViewModel.Instance.TopRightButtonType = HeaderButtonType.Menu;
					ShowViewModelOfType(typeof(AboutViewModel));
					break;
			    case SidebarMenuType.Dashboard:
			        MasterViewModel.Instance.TopRightButtonType = HeaderButtonType.Menu;
			        ShowViewModelOfType(typeof(DashboardViewModel));
			        break;
                case SidebarMenuType.NewComplaint:
					MasterViewModel.Instance.TopRightButtonType = HeaderButtonType.Menu;
					MasterViewModel.Instance.ExecuteBeforeShowDetailView(typeof(CreateNewComplaintViewModel),
					delegate {
						ShowViewModel<CreateNewComplaintViewModel>(new
						{
							previousPage = DataModel.PreviousPageString
						});
					});
					break;
				case SidebarMenuType.Notification:
					DataModel.Notifications = null;
					MasterViewModel.Instance.TopRightButtonType = HeaderButtonType.Menu;
					ShowViewModelOfType(typeof(NotificationViewModel));
					break;
				case SidebarMenuType.Logout:
					MasterViewModel.Instance.IsLogoutPopupShown = true;
					break;
				default:
					throw new Exception(MenuType + "not found");
			}
		}

		protected override void UnregisteredEvent()
		{
			if (MenuType == SidebarMenuType.Notification)
				DataModel.DataReceived -= OnDataReceived;
		}

		#endregion

		#region Properties

		public static SidebarItemViewModel SelectedItem;
		private bool _isCountShow;
		private string _titleBar;

		private int _totalNotification;

		public SidebarMenuType MenuType { get; set; }

		public string TitleBar
		{
			get { return _titleBar; }
			set
			{
				_titleBar = value;
				RaisePropertyChanged(() => TitleBar);
			}
		}

		public bool IsCountShow
		{
			get { return _isCountShow; }
			set
			{
				_isCountShow = value;
				RaisePropertyChanged(() => IsCountShow);
			}
		}

		public int WidthCircle { get; set; }

		public int TotalNotification
		{
			get { return _totalNotification; }
			set
			{
				_totalNotification = value;
				RaisePropertyChanged(() => TotalNotification);
			}
		}

		#endregion
	}
}