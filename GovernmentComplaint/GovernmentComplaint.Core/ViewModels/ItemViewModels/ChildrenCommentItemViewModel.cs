﻿using System;
using System.Windows.Input;
using GovernmentComplaint.Core.Helpers;
using GovernmentComplaint.Core.Localization;
using GovernmentComplaint.Core.Models;
using GovernmentComplaint.Core.Services;
using GovernmentComplaint.Core.ViewModels.Bases;
using GovernmentComplaint.Core.ViewModels.ItemViewModels.Base;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform;

namespace GovernmentComplaint.Core.ViewModels.ItemViewModels
{
	public class ChildrenCommentItemViewModel : BaseListItemViewModel
	{
		private readonly int _detailId, _commentId;
		private readonly int _likeQuantity;
		private readonly INetworkHelper _networkHelper;

		public ChildrenCommentItemViewModel( Comment comment, int detailId, IManagementService managementService, IDataModel dataModel,
			IResourceHelper resourceHelper)
			: base(managementService, dataModel, resourceHelper)
		{
			var childrenComment = comment;
			UserName = childrenComment.Username;
			CommentContent = childrenComment.CommentContent;
			LikeText = childrenComment.LikeCount;
			CommentDate = Convert.ToDateTime(childrenComment.CommentTime).ToString("HH:mm dd/MM/yyyy");

			_commentId = childrenComment.Id;
			_detailId = detailId;
			_likeQuantity = LikeText;
			InitCommand();

			_networkHelper = Mvx.Resolve<INetworkHelper>();
		}

		#region UI Texts

		public string StaticReplyText => Localizer.GetText("StaticReplyText");
		public string StaticLikeText => Localizer.GetText("StaticLikeText");
		public string CommentButtonHint => Localizer.GetText("CommentButtonHint");
		public string CommentHint => Localizer.GetText("CommentHint");

		#endregion

		#region Properties

		private bool _needToShowPlaceHolder;
		public bool NeedToShowPlaceHolder
		{
			get { return _needToShowPlaceHolder; }
			set { SetProperty(ref _needToShowPlaceHolder, value); }
		}

		private bool _isKeyboardHidden;

		public bool IsKeyboardHidden
		{
			get { return _isKeyboardHidden; }
			set
			{
				_isKeyboardHidden = value;
				RaisePropertyChanged(() => IsKeyboardHidden);
			}
		}

		private int _childrenCommentCount;

		public int ChildrenCommentCount
		{
			get { return _childrenCommentCount; }
			set
			{
				_childrenCommentCount = value;
				RaisePropertyChanged(() => ChildrenCommentCount);
			}
		}

		private string _userName;

		public string UserName
		{
			get { return _userName; }
			set
			{
				_userName = value;
				SetProperty(ref _userName, value);
			}
		}

		private string _commentDate;

		public string CommentDate
		{
			get { return _commentDate; }
			set
			{
				_commentDate = value;
				SetProperty(ref _commentDate, value);
			}
		}

		private string _commentContent;

		public string CommentContent
		{
			get { return _commentContent; }
			set
			{
				_commentContent = value;
				SetProperty(ref _commentContent, value);
			}
		}

		private int _likeText;

		public int LikeText
		{
			get { return _likeText; }
			set
			{
				_likeText = value;
				RaisePropertyChanged(() => LikeText);
			}
		}

		private bool _isReplyCommentLayoutShown;

		public bool IsReplyCommentLayoutShown
		{
			get { return _isReplyCommentLayoutShown; }
			set
			{
				_isReplyCommentLayoutShown = value;
				RaisePropertyChanged(() => IsReplyCommentLayoutShown);
			}
		}

		private string _enteredCommentContent;

		public string EnteredCommentContent
		{
			get { return _enteredCommentContent; }
			set
			{
				_enteredCommentContent = value;
				RaisePropertyChanged(() => EnteredCommentContent);
			}
		}

		#endregion

		#region Commands

		public ICommand ReplyCommand { get; private set; }

		public ICommand LikeCommand { get; private set; }

		public ICommand AddCommentCommand { get; private set; }

		#endregion

		#region Methods

		private void InitCommand()
		{
			ReplyCommand = new MvxCommand(OnReplyCommand);
			LikeCommand = new MvxCommand(OnLikeCommand);
			AddCommentCommand = new MvxCommand(OnAddCommentCommand);
		}

		private void OnAddCommentCommand()
		{
			IsKeyboardHidden = true;
			if (string.IsNullOrWhiteSpace(EnteredCommentContent))
			{
				MasterViewModel.Instance.IsNoCommentPopupShown = true;
			}
			else
			{
				if (!MasterViewModel.Instance.IsLoggedIn)
				{
					MasterViewModel.Instance.ExecuteBeforeShowDetailView(typeof(SecondLoginScreenViewModel),
										delegate {
											ShowViewModel<SecondLoginScreenViewModel>(new
											{
												previousPage = DataModel.PreviousPageString
											});
										});
				}
				else
				{
					if (DataModel.UserInfo == null)
					{
						MasterViewModel.Instance.ExecuteBeforeShowDetailView(typeof(AddPersonalInfoViewModel),
										delegate {
											ShowViewModel<AddPersonalInfoViewModel>(new
											{
												previousPage = DataModel.PreviousPageString
											});
										});
					}
					else
					{
						if (_networkHelper.IsConnected)
						{
							ManagementService.PostComment(_detailId, DataModel.UserInfo.UserId, _commentId, EnteredCommentContent,
								"BINH_LUAN");
							DataModel.CommentContent = EnteredCommentContent;
							EnteredCommentContent = "";
							IsReplyCommentLayoutShown = false;
							NeedToShowPlaceHolder = true;
						}
						else
						{
							MasterViewModel.Instance.IsNoWifiPopupShown = true;
						}
					}
				}

			}
		}
		
		private async void OnLikeCommand()
		{
			if (!MasterViewModel.Instance.IsLoggedIn)
			{
				MasterViewModel.Instance.ExecuteBeforeShowDetailView(typeof(SecondLoginScreenViewModel),
					delegate {
						ShowViewModel<SecondLoginScreenViewModel>(new
						{
							previousPage = DataModel.PreviousPageString
						});
					});
			}
			else
			{
				if (DataModel.UserInfo != null)
				{
					var isLiked =
						await ManagementService.LikeComment(_detailId, DataModel.UserInfo.UserId, _commentId, null,
							"QUAN_TAM");
					if (isLiked)
					{
						if (LikeText == _likeQuantity)
						{
							LikeText++;
						}
						else
						{
							LikeText--;
						}
					}
					else
					{
						if (_networkHelper.IsConnected)
						{
							MasterViewModel.Instance.IsFail24HPopupShown = true;
						}
					}
				}
				else
				{
					MasterViewModel.Instance.ExecuteBeforeShowDetailView(typeof(AddPersonalInfoViewModel),
					delegate {
						ShowViewModel<AddPersonalInfoViewModel>(new
						{
							previousPage = DataModel.PreviousPageString
						});
					});
				}
			}
		}

		private void OnReplyCommand()
		{
			if (!MasterViewModel.Instance.IsLoggedIn)
			{
				MasterViewModel.Instance.ExecuteBeforeShowDetailView(typeof(SecondLoginScreenViewModel),
					delegate { ShowViewModel<SecondLoginScreenViewModel>(new
					{
						previousPage = DataModel.PreviousPageString
					}); });
			}
			else
			{
				if (DataModel.UserInfo == null)
				{
					MasterViewModel.Instance.ExecuteBeforeShowDetailView(typeof(AddPersonalInfoViewModel),
						delegate { ShowViewModel<AddPersonalInfoViewModel>(new
						{
							previousPage = DataModel.PreviousPageString
						}); });
				}
				else
				{
					IsReplyCommentLayoutShown = !IsReplyCommentLayoutShown;
				}
			}
		}

		#endregion
	}
}