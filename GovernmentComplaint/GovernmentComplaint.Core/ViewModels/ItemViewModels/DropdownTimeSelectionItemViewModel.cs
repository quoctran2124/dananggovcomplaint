using System;
using GovernmentComplaint.Core.Enums;
using GovernmentComplaint.Core.Helpers;
using GovernmentComplaint.Core.Localization;
using GovernmentComplaint.Core.Models;
using GovernmentComplaint.Core.Services;
using GovernmentComplaint.Core.ViewModels.Bases;
using GovernmentComplaint.Core.ViewModels.ItemViewModels.Base;
using MvvmCross.Platform;

namespace GovernmentComplaint.Core.ViewModels.ItemViewModels
{
	public class DropdownTimeSelectionItemViewModel : BaseDropdownItemViewModel
	{
		public static DropdownTimeSelectionItemViewModel SelectedItem;
	    public TimeSelectionEnum TimeSelectionType { get; set; }

        private readonly INetworkHelper _networkHelper;

	    private Action<TimeSelectionEnum> _updateDashboard;

		#region Properties

		#endregion

		#region Method

		public DropdownTimeSelectionItemViewModel(TimeSelectionEnum timeItemType, IManagementService managementService, 
		    IDataModel dataModel,IResourceHelper resourceHelper,Action<TimeSelectionEnum> updateDashboard) : base(managementService, dataModel, resourceHelper)
		{
			_networkHelper = Mvx.Resolve<INetworkHelper>();
		    TimeSelectionType = timeItemType;
            ItemTitle = Localizer.GetText(timeItemType.ToString());
            _updateDashboard = updateDashboard;
        }
        
        #endregion

	    protected override void OnItemClickCommand()
	    {
            SelectedItem = this;
            switch (TimeSelectionType)
            {
                case TimeSelectionEnum.Week:
                    _updateDashboard?.Invoke(TimeSelectionEnum.Week);

                    break;
                case TimeSelectionEnum.Month:
                    _updateDashboard?.Invoke(TimeSelectionEnum.Month);

                    break;
                case TimeSelectionEnum.Year:
                    _updateDashboard?.Invoke(TimeSelectionEnum.Year);

                    break;
            }
        }

	    protected override void UnregisteredEvent()
		{
		}
	}
}