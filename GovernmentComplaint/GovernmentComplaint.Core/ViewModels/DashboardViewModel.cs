﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using GovernmentComplaint.Core.Enums;
using GovernmentComplaint.Core.Helpers;
using GovernmentComplaint.Core.Localization;
using GovernmentComplaint.Core.Models;
using GovernmentComplaint.Core.Services;
using GovernmentComplaint.Core.ViewModels.Bases;
using GovernmentComplaint.Core.ViewModels.ItemViewModels;
using MvvmCross.Core.ViewModels;

namespace GovernmentComplaint.Core.ViewModels
{
	public class DashboardViewModel : DetailViewModel
	{
	    public string Hello => Localizer.GetText("Dashboard");
	    public string Summary => Localizer.GetText("Summary");
	    public string FeedbackCategories => Localizer.GetText("FeedbackCategories");
	    public string FeedbackHaveBeenProcessed => Localizer.GetText("FeedbackHaveBeenProcessed");
	    public string TotalFeedback => Localizer.GetText("TotalFeedback");
	    public string FeedbackProcessedPercent => Localizer.GetText("FeedbackProcessedPercent");
	    public string ProcessingUnitTitle => Localizer.GetText("ProcessingUnitTitle");
	    public string Number => "98";


	    public ICommand SelectTimeCommand { get; private set; }
        public ICommand SelectSeeMoreCommand { get; private set; }
	    public ICommand SelectSeeMoreProcessingUnitCommand { get; private set; }

	    //private HeaderButtonType _topLeftButtonType;

	    //public HeaderButtonType TopLeftButtonType
	    //{
	    //    get { return _topLeftButtonType; }
	    //    set { SetProperty(ref _topLeftButtonType, value); }
	    //}

        private int _numberOfCategoryItemList;

	    public int NumberOfCategoryItemList
        {
	        get => _numberOfCategoryItemList;
	        set
	        {
	            _numberOfCategoryItemList = value;
	            RaisePropertyChanged(() => NumberOfCategoryItemList);
	        }
	    }

	    private int _numberOfProcessingUnitItemList;

	    public int NumberOfProcessingUnitItemList
        {
	        get => _numberOfProcessingUnitItemList;
	        set
	        {
	            _numberOfProcessingUnitItemList = value;
	            RaisePropertyChanged(() => NumberOfProcessingUnitItemList);
	        }
	    }

	    private double _summaryPercentValue;

	    public double SummaryPercentValue
        {
	        get => _summaryPercentValue;
	        set
	        {
	            _summaryPercentValue = value;
	            RaisePropertyChanged(() => SummaryPercentValue);
	        }
	    }

        private string _seeMoreText;

	    public string SeeMoreText
        {
	        get => _seeMoreText;
	        set
	        {
	            _seeMoreText = value;
	            RaisePropertyChanged(() => SeeMoreText);
	        }
	    }

	    private string _seeMoreUnitText;

	    public string SeeMoreUnitText
        {
	        get => _seeMoreUnitText;
	        set
	        {
	            _seeMoreUnitText = value;
	            RaisePropertyChanged(() => SeeMoreUnitText);
	        }
	    }

        private string _timeSelectionIconName;

	    public string TimeSelectionIconName
        {
	        get => _timeSelectionIconName;
	        set
	        {
                _timeSelectionIconName = value;
	            RaisePropertyChanged(() => TimeSelectionIconName);
	        }
	    }

	    private string _timeSelectionText;

	    public string TimeSelectionText
        {
	        get => _timeSelectionText;
	        set
	        {
	            _timeSelectionText = value;
	            RaisePropertyChanged(() => TimeSelectionText);
	        }
	    }
        
        private bool _isTimeSelectionShown;

	    public bool IsTimeSelectionShown
        {
	        get => _isTimeSelectionShown;
	        set
	        {
	            _isTimeSelectionShown = value;
	            TimeSelectionIconName = ResourceHelper.IconPath(value ? "collapse": "dropdown");
                RaisePropertyChanged(() => IsTimeSelectionShown);
	        }
	    }

	    private bool _isSeeMoreShown;

	    public bool IsSeeMoreShown
        {
	        get => _isSeeMoreShown;
	        set
	        {
	            _isSeeMoreShown = value;
	            RaisePropertyChanged(() => IsSeeMoreShown);
	        }
	    }

	    private bool _isSeeMoreUnitShown;

	    public bool IsSeeMoreUnitShown
        {
	        get => _isSeeMoreUnitShown;
	        set
	        {
	            _isSeeMoreUnitShown = value;
	            RaisePropertyChanged(() => IsSeeMoreUnitShown);
	        }
	    }

        private List<DropdownTimeSelectionItemViewModel> _timeSelectionItemViewModels;

	    public List<DropdownTimeSelectionItemViewModel> TimeSelectionItemViewModels
        {
	        get { return _timeSelectionItemViewModels; }
	        set
	        {
	            _timeSelectionItemViewModels = value;
	            RaisePropertyChanged(() => TimeSelectionItemViewModels);
	        }
	    }


	    private List<FeedbackCategoryItemViewModel> _feedbackCategoryItemViewModels;

	    public List<FeedbackCategoryItemViewModel> FeedbackCategoryItemViewModels
        {
	        get { return _feedbackCategoryItemViewModels; }
	        set
	        {
	            _feedbackCategoryItemViewModels = value;
	            RaisePropertyChanged(() => FeedbackCategoryItemViewModels);
	        }
	    }


	    private List<FeedbackCategoryItemViewModel> _processingUnitItemViewModels;

	    public List<FeedbackCategoryItemViewModel> ProcessingUnitItemViewModels
        {
	        get => _processingUnitItemViewModels;
	        set
	        {
	            _processingUnitItemViewModels = value;
	            RaisePropertyChanged(() => ProcessingUnitItemViewModels);
	        }
	    }

	    private List<FieldOfSuggestion> _fieldOfSuggestionList;
	    private List<FieldOfSuggestion> _processingUnitList;

        public DashboardViewModel(IManagementService managementService, IDataModel dataModel, IResourceHelper resourceHelper)
			: base(managementService, dataModel, resourceHelper)
		{
			MasterViewModel.Instance.InitButtonOnProcessingDetailView();
			MasterViewModel.Instance.IsIndicatorShown = false;
			MasterViewModel.Instance.TitleBar = Localizer.GetText("Dashboard");

		    IsTimeSelectionShown = false;
            TimeSelectionIconName = ResourceHelper.IconPath("dropdown");
		    InitCommands();
		    IsSeeMoreShown = false;
		    IsSeeMoreUnitShown = false;
		    SeeMoreText = Localizer.GetText("SeeMore");
		    SeeMoreUnitText = Localizer.GetText("SeeMoreUnitText");
		    TimeSelectionText = Localizer.GetText(TimeSelectionEnum.Week.ToString());
		    SummaryPercentValue = Math.Round((double) 1 / 3 * 100, 1);
		    //SummaryPercentValue = 1 % 3 + SummaryPercentValue;
            InitFieldOfSuggestionItems();
		    InitProcessingUnitItems();

		    InitSortingDropdownItems();

		}

        protected void InitCommands()
	    {
	        SelectTimeCommand = new MvxCommand(OnSelectTimeCommand);
            SelectSeeMoreCommand = new MvxCommand(OnSelectSeeMoreCommand);
	        SelectSeeMoreProcessingUnitCommand = new MvxCommand(OnSelectSeeMoreProcessingUnitCommand);
        }

	    private void OnSelectTimeCommand()
	    {
	        IsTimeSelectionShown = !IsTimeSelectionShown;
	        InitSortingDropdownItems();
	    }

        private void InitSortingDropdownItems()
	    {
            var timeNameList = new List<DropdownTimeSelectionItemViewModel>
	        {
	            new DropdownTimeSelectionItemViewModel(TimeSelectionEnum.Week, ManagementService, DataModel, ResourceHelper, UpdateDashboardView),
	            new DropdownTimeSelectionItemViewModel(TimeSelectionEnum.Month, ManagementService, DataModel, ResourceHelper, UpdateDashboardView),
	            new DropdownTimeSelectionItemViewModel(TimeSelectionEnum.Year, ManagementService, DataModel, ResourceHelper, UpdateDashboardView),
	        };

            TimeSelectionItemViewModels = timeNameList;
        }
        
        private void UpdateDashboardView(TimeSelectionEnum timeSelected)
	    {
            //Update dashboard view when time filter change
	        IsTimeSelectionShown = false;
	        TimeSelectionText = Localizer.GetText(timeSelected.ToString());

	    }

        private void InitFieldOfSuggestionItems()
	    {
	        _fieldOfSuggestionList = new List<FieldOfSuggestion>(new[]
	        {
	            new FieldOfSuggestion
                {
	                Id = 1,
	                UnitName = "UBND",
                    FeedbackProcessed = "20",
                    TotalFeedback = "28",

                },
	            new FieldOfSuggestion
	            {
	                Id = 2,
	                UnitName = "HDND",
	                FeedbackProcessed = "10",
	                TotalFeedback = "38",

	            },
	            new FieldOfSuggestion
	            {
	                Id = 3,
	                UnitName = "TP jhlkjsfsdlkfjlksdjfkjsdklfjlksdjfkdjkfjskdfjklsdf jh lkjsfsdlkfjlksdjfkjsdklfjlksdjfkdjkfjskdfjklsdf",
	                FeedbackProcessed = "15",
	                TotalFeedback = "32",

	            },
	            new FieldOfSuggestion
	            {
	                Id = 4,
	                UnitName = "City",
	                FeedbackProcessed = "40",
	                TotalFeedback = "68",

	            },
	            new FieldOfSuggestion
	            {
	                Id = 5,
	                UnitName = "So TTTT",
	                FeedbackProcessed = "60",
	                TotalFeedback = "98",

	            },
	            new FieldOfSuggestion
	            {
	                Id = 6,
	                UnitName = "So GTVT",
	                FeedbackProcessed = "120",
	                TotalFeedback = "228",

	            },
	            new FieldOfSuggestion
	            {
	                Id = 7,
	                UnitName = "So LD",
	                FeedbackProcessed = "170",
	                TotalFeedback = "188",

	            },
	            new FieldOfSuggestion
	            {
	                Id = 8,
	                UnitName = "So VH-TT",
	                FeedbackProcessed = "111",
	                TotalFeedback = "222",

	            },
            });

	        var categoryItemViewModels = _fieldOfSuggestionList.Take(5).Select(sortType => new FeedbackCategoryItemViewModel(sortType, ManagementService, DataModel, ResourceHelper)).ToList();
	        FeedbackCategoryItemViewModels = categoryItemViewModels;
	        NumberOfCategoryItemList = FeedbackCategoryItemViewModels.Count;
        }

        private void OnSelectSeeMoreCommand()
	    {
	        IsSeeMoreShown = !IsSeeMoreShown;

            if (IsSeeMoreShown)
	        {
	            var categoryItemViewModels = _fieldOfSuggestionList.Select(sortType => new FeedbackCategoryItemViewModel(sortType, ManagementService, DataModel, ResourceHelper)).ToList();
	            FeedbackCategoryItemViewModels = categoryItemViewModels;
            }
	        else
	        {
	            var categoryItemViewModels = _fieldOfSuggestionList.Take(5).Select(sortType => new FeedbackCategoryItemViewModel(sortType, ManagementService, DataModel, ResourceHelper)).ToList();
	            FeedbackCategoryItemViewModels = categoryItemViewModels;
            }

	        NumberOfCategoryItemList = FeedbackCategoryItemViewModels.Count;
	        SeeMoreText = Localizer.GetText(IsSeeMoreShown ? "Showless" : "SeeMore");
        }

        private void InitProcessingUnitItems()
        {
            _processingUnitList = new List<FieldOfSuggestion>(new[]
            {
                new FieldOfSuggestion
                {
                    Id = 1,
                    UnitName = "CA TP DN",
                    FeedbackProcessed = "20",
                    TotalFeedback = "28",
                },
                new FieldOfSuggestion
                {
                    Id = 2,
                    UnitName = "UBND DN",
                    FeedbackProcessed = "10",
                    TotalFeedback = "38",
                },
                new FieldOfSuggestion
                {
                    Id = 3,
                    UnitName = "Ban Quản lý các khu công nghiệp và chế xuất Đà Nẵng",
                    FeedbackProcessed = "15",
                    TotalFeedback = "32",
                },
                new FieldOfSuggestion
                {
                    Id = 4,
                    UnitName = "Nhà đất - xây dựng",
                    FeedbackProcessed = "40",
                    TotalFeedback = "68",
                },
                new FieldOfSuggestion
                {
                    Id = 5,
                    UnitName = "Môi trường",
                    FeedbackProcessed = "60",
                    TotalFeedback = "98",
                },
                new FieldOfSuggestion
                {
                    Id = 6,
                    UnitName = "So GTVT",
                    FeedbackProcessed = "120",
                    TotalFeedback = "228",
                },
                new FieldOfSuggestion
                {
                    Id = 7,
                    UnitName = "So LD",
                    FeedbackProcessed = "170",
                    TotalFeedback = "188",
                },
                new FieldOfSuggestion
                {
                    Id = 8,
                    UnitName = "So VH-TT",
                    FeedbackProcessed = "111",
                    TotalFeedback = "222",
                },
            });

            var processingUnitItemViewModels = _processingUnitList.Take(5).Select(sortType => new FeedbackCategoryItemViewModel(sortType, ManagementService, DataModel, ResourceHelper)).ToList();
            ProcessingUnitItemViewModels = processingUnitItemViewModels;
            NumberOfProcessingUnitItemList = ProcessingUnitItemViewModels.Count;
        }

	    private void OnSelectSeeMoreProcessingUnitCommand()
	    {
	        IsSeeMoreUnitShown = !IsSeeMoreUnitShown;

	        if (IsSeeMoreUnitShown)
	        {
	            var processingUnitItemViewModels = _processingUnitList.Select(sortType => new FeedbackCategoryItemViewModel(sortType, ManagementService, DataModel, ResourceHelper)).ToList();
	            ProcessingUnitItemViewModels = processingUnitItemViewModels;
            }
	        else
	        {
	            var processingUnitItemViewModels = _processingUnitList.Take(5).Select(sortType => new FeedbackCategoryItemViewModel(sortType, ManagementService, DataModel, ResourceHelper)).ToList();
	            ProcessingUnitItemViewModels = processingUnitItemViewModels;
            }

	        NumberOfProcessingUnitItemList = ProcessingUnitItemViewModels.Count;
	        SeeMoreUnitText = Localizer.GetText(IsSeeMoreUnitShown ? "Showless" : "SeeMoreUnitText");
	    }
        #region UI Texts

        #endregion
    }
}