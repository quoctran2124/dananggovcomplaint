﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Input;
using GovernmentComplaint.Core.Enums;
using GovernmentComplaint.Core.Helpers;
using GovernmentComplaint.Core.Localization;
using GovernmentComplaint.Core.Models;
using GovernmentComplaint.Core.Services;
using GovernmentComplaint.Core.ViewModels.Bases;
using GovernmentComplaint.Core.ViewModels.ItemViewModels;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform;

namespace GovernmentComplaint.Core.ViewModels
{
	public class CreateNewComplaintViewModel : DetailViewModel
	{
		private static CreateNewComplaintViewModel _selectedBackgroundType;
		private string _previousPage;
		private readonly INetworkHelper _networkHelper;
		private readonly IAppDataLoader _appDataLoader;

		public CreateNewComplaintViewModel(IManagementService managementService, IDataModel dataModel,
			IResourceHelper resourceHelper, IAppDataLoader appDataLoader)
			: base(managementService, dataModel, resourceHelper)
		{
			_networkHelper = Mvx.Resolve<INetworkHelper>();
			_appDataLoader = appDataLoader;

			MasterViewModel.Instance.IsIndicatorShown = false;
			MasterViewModel.Instance.TitleBar = Localizer.GetText("NewComplaintTitle");
			MasterViewModel.Instance.CreateNewComplaintViewModel = this;
			MasterViewModel.Instance.InitButtonOnDetailView();
			MasterViewModel.Instance.LeftButtonClicked += OnSpecificBackClick;
			MasterViewModel.Instance.SaveFeedBackClicked += OnSaveFeedBackClicked;

			IsComplaintContentEmpty = false;
			IsLocationEmpty = false;
			IsVideoLinkWrong = false;

			ComplaintContentHint = NewComplaintContentHint;
			LocationHint = Location;
			VideoLinkHint = VideoTextHint;

			_selectedBackgroundType = this;
			_selectedBackgroundType.LocationBackgroundType = BackgroundType.Normal;
			_selectedBackgroundType.ContentBackgroundType = BackgroundType.Normal;
			_selectedBackgroundType.VideoLinkBackgroundType = BackgroundType.Normal;

			IsNoAttachedFileShown = true;
			InitCommands();
			AttachmentStreamsValue = new MemoryStream[3];
			if (DataModel.NewComplaintContent != null)
			{
				ComplaintContentText = DataModel.NewComplaintContent.ComplaintContentText;
				LocationText = DataModel.NewComplaintContent.LocationText;
				VideoLinkValue = DataModel.NewComplaintContent.VideoLinkValue;
				//Load images
			}
			DataModel.PreviousPage = PreviousPage.CreateNewComplaint;
		}

		#region Methods

		public void Init(string previousPage)
		{
			_previousPage = previousPage;
		}

		protected override void UnregisteredEvent()
		{
			base.UnregisteredEvent();
			MasterViewModel.Instance.LeftButtonClicked -= OnSpecificBackClick;
			MasterViewModel.Instance.SaveFeedBackClicked -= OnSaveFeedBackClicked;
		}

		private void OnSpecificBackClick(object sender, EventArgs e)
		{
			IsKeyboardHidden = true;
			OnSpecificBackClicked();
		}

		private void OnSpecificBackClicked()
		{
			if (!string.IsNullOrEmpty(ComplaintContentText) || !string.IsNullOrEmpty(LocationText) ||
			    !string.IsNullOrEmpty(VideoLinkValue) || AttachmentStreamsValue[0] != null)
			{
				MasterViewModel.Instance.IsCancelPopupShown = true;
			}
			else
			{
				OnBackCommand();
			}
		}

		#endregion

		#region Properties

		public string ComeBack => Localizer.GetText("ComeBack");
		public string Send => Localizer.GetText("Send");
		public string Cancel => Localizer.GetText("Cancel").ToUpper();
		public string Video => Localizer.GetText("Video");
		public string Picture => Localizer.GetText("Picture");
		public string Attachment => Localizer.GetText("Attachment");
		public string Location => Localizer.GetText("Location");
		public string Time => Localizer.GetText("Time");
		public string NewComplaintContentHint => Localizer.GetText("NewComplaintContentHint");
		public string NoAttachedFile => Localizer.GetText("NoAttachedFile");
		public string EmptyComplaintContent => Localizer.GetText("EmptyComplaintContent");
		public string EmptyLocation => Localizer.GetText("EmptyLocation");
		public string VideoLinkErrorHint => Localizer.GetText("NewVideoLinkHint");
		public string VideoLinkEmptyHint => Localizer.GetText("VideoLinkEmptyHint");

		private string _videoLinkHint;

		public string VideoLinkHint
		{
			get { return _videoLinkHint; }
			set
			{
				_videoLinkHint = value;
				RaisePropertyChanged(() => VideoLinkHint);
			}
		}

		private string _addFile;

		public string AddFile
		{
			get { return _addFile; }
			set
			{
				_addFile = value;
				RaisePropertyChanged(() => AddFile);
			}
		}

		private MemoryStream[] _attachmentStreams;

		public MemoryStream[] AttachmentStreamsValue
		{
			get { return _attachmentStreams; }
			set
			{
				_attachmentStreams = value;
				RaisePropertyChanged(() => AttachmentStreamsValue);
			}
		}

		private string _videoLinkValue;

		public string VideoLinkValue
		{
			get { return _videoLinkValue; }
			set
			{
				_videoLinkValue = value;
				RaisePropertyChanged(() => VideoLinkValue);
			}
		}

		private bool _isImagePickerShown;

		public bool IsImagePickerShown
		{
			get { return _isImagePickerShown; }
			set
			{
				_isImagePickerShown = value;
				RaisePropertyChanged(() => IsImagePickerShown);
			}
		}

		private bool _isKeyboardHidden;

		public bool IsKeyboardHidden
		{
			get { return _isKeyboardHidden; }
			set
			{
				_isKeyboardHidden = value;
				RaisePropertyChanged(() => IsKeyboardHidden);
			}
		}

		private bool _isVideoLayoutShown;

		public bool IsVideoLayoutShown
		{
			get { return _isVideoLayoutShown; }
			set
			{
				_isVideoLayoutShown = value;
				RaisePropertyChanged(() => IsVideoLayoutShown);
			}
		}

		private string _videoLink;

		public string VideoLink
		{
			get { return _videoLink; }
			set
			{
				_videoLink = value;
				RaisePropertyChanged(() => VideoLink);
			}
		}

		private string _enteredVideoLink;

		public string EnteredVideoLink
		{
			get { return _enteredVideoLink; }
			set
			{
				_enteredVideoLink = value;
				RaisePropertyChanged(() => EnteredVideoLink);
			}
		}

		private string _complaintContentText;

		public string ComplaintContentText
		{
			get { return _complaintContentText; }
			set
			{
				_complaintContentText = value;
				RaisePropertyChanged(() => ComplaintContentText);
			}
		}

		private string _complaintContentHint;

		public string ComplaintContentHint
		{
			get { return _complaintContentHint; }
			set
			{
				_complaintContentHint = value;
				RaisePropertyChanged(() => ComplaintContentHint);
			}
		}

		private bool _isComplaintContentEmpty;

		public bool IsComplaintContentEmpty
		{
			get { return _isComplaintContentEmpty; }
			set
			{
				_isComplaintContentEmpty = value;
				RaisePropertyChanged(() => IsComplaintContentEmpty);
			}
		}

		private bool _isVideoLinkWrong;

		public bool IsVideoLinkWrong
		{
			get { return _isVideoLinkWrong; }
			set
			{
				_isVideoLinkWrong = value;
				RaisePropertyChanged(() => IsVideoLinkWrong);
			}
		}

		private string _locationText;

		public string LocationText
		{
			get { return _locationText; }
			set
			{
				_locationText = value;
				RaisePropertyChanged(() => LocationText);
			}
		}

		private string _locationHint;

		public string LocationHint
		{
			get { return _locationHint; }
			set
			{
				_locationHint = value;
				RaisePropertyChanged(() => LocationHint);
			}
		}

		private bool _isLocationEmpty;

		public bool IsLocationEmpty
		{
			get { return _isLocationEmpty; }
			set
			{
				_isLocationEmpty = value;
				RaisePropertyChanged(() => IsLocationEmpty);
			}
		}

		private BackgroundType _locationBackgroundType;

		public BackgroundType LocationBackgroundType
		{
			get { return _locationBackgroundType; }
			set
			{
				_locationBackgroundType = value;
				RaisePropertyChanged(() => LocationBackgroundType);
			}
		}

		private BackgroundType _contentBackgroundType;

		public BackgroundType ContentBackgroundType
		{
			get { return _contentBackgroundType; }
			set
			{
				_contentBackgroundType = value;
				RaisePropertyChanged(() => ContentBackgroundType);
			}
		}

		private BackgroundType _videoLinkBackgroundType;

		public BackgroundType VideoLinkBackgroundType
		{
			get { return _videoLinkBackgroundType; }
			set
			{
				_videoLinkBackgroundType = value;
				RaisePropertyChanged(() => VideoLinkBackgroundType);
			}
		}

		private bool _isNoAttachedFileShown;

		public bool IsNoAttachedFileShown
		{
			get { return _isNoAttachedFileShown; }
			set
			{
				_isNoAttachedFileShown = value;
				RaisePropertyChanged(() => IsNoAttachedFileShown);
			}
		}

		private byte[] _attachmentData;

		public byte[] AttachmentData
		{
			get { return _attachmentData; }
			set
			{
				_attachmentData = value;
				RaisePropertyChanged(() => AttachmentData);
			}
		}

		#endregion

		#region Commands

		public ICommand CancelCommand { get; private set; }

		public ICommand CancelVideoLinkCommand { get; private set; }

		public ICommand SendCommand { get; private set; }

		public ICommand VideoCommand { get; private set; }

		public ICommand ConfirmVideoLinkCommand { get; private set; }

		public ICommand ConfirmCancelCommand { get; private set; }

		public ICommand ComplaintContentCommand { get; private set; }

		public ICommand LocationCommand { get; private set; }

		public MvxCommand AddImagesCommand { get; set; }

		public MvxCommand AttachWrongFormatOrSizeErrorCommand { get; set; }

		public ICommand VideoLinkTouchCommand { get; private set; }

		#endregion

		#region Command Methods

		private void InitCommands()
		{
			CancelCommand = new MvxCommand(OnCancelCommand);
			CancelVideoLinkCommand = new MvxCommand(OnCancelVideoLinkCommand);
			SendCommand = new MvxCommand(OnSendCommand);
			VideoCommand = new MvxCommand(OnVideoCommand);
			ConfirmVideoLinkCommand = new MvxCommand(OnConfirmVideoLinkCommand);
			ConfirmCancelCommand = new MvxCommand(OnConfirmCancelCommand);
			ComplaintContentCommand = new MvxCommand(OnComplaintContentCommand);
			LocationCommand = new MvxCommand(OnLocationCommand);
			AddImagesCommand = new MvxCommand(OnAddImagesCommand);
			AttachWrongFormatOrSizeErrorCommand = new MvxCommand(OnAttachWrongFormatOrSizeErrorCommand);
			VideoLinkTouchCommand = new MvxCommand(OnVideoLinkTouchCommand);
		}

		private void CheckVideoLink(string videoLink)
		{
			var parametersYt = "https://www.youtube.com/oembed?format=json&url=" + videoLink;
			var parametersFb = "http://graph.facebook.com/" + VideoLinkFB(videoLink);

			if (!string.IsNullOrWhiteSpace(videoLink) &&
				(ManagementService.VerifyLinkFb(parametersFb) || ManagementService.VerifyLinkYt(parametersYt)))
			{
				EnteredVideoLink = EnteredVideoLink.Replace(" ", string.Empty);
				VideoLinkValue = EnteredVideoLink;
				IsVideoLayoutShown = true;
				IsNoAttachedFileShown = false;
				MasterViewModel.Instance.IsVideoPopupShown = false;
			}
			else
			{
				_selectedBackgroundType.VideoLinkBackgroundType = BackgroundType.Error;
				EnteredVideoLink = string.Empty;
				VideoLinkHint = videoLink == "" ? VideoLinkEmptyHint : VideoLinkErrorHint;
				IsVideoLinkWrong = true;
			}
		}

		private string VideoLinkFB(string videoLink)
		{
			int temp;
			if (videoLink == null)
				return "abc";
			videoLink = videoLink.Trim();
			temp = videoLink.LastIndexOf("/");
			if (temp == -1) return "abc";
			if (videoLink[videoLink.Length - 1] >= '0' && videoLink[videoLink.Length - 1] <= '9')
			{
				videoLink = videoLink.Remove(0, temp + 1);
			}
			else
			{
				videoLink = videoLink.Remove(temp);
				temp = videoLink.LastIndexOf("/");
				if (temp > -1)
					videoLink = videoLink.Remove(0, temp + 1);
			}
			if (videoLink == "")
				videoLink = "abc";
			return videoLink;
		}

		private void OnCancelVideoLinkCommand()
		{
			_selectedBackgroundType.VideoLinkBackgroundType = BackgroundType.Normal;
			EnteredVideoLink = string.Empty;
			VideoLinkHint = VideoTextHint;
			IsVideoLinkWrong = false;
			MasterViewModel.Instance.IsVideoPopupShown = false;
		}

		private void OnVideoLinkTouchCommand()
		{
			_selectedBackgroundType.VideoLinkBackgroundType = BackgroundType.Normal;
			IsVideoLinkWrong = false;
			VideoLinkHint = VideoTextHint;
		}

		private void OnAttachWrongFormatOrSizeErrorCommand()
		{
			//TODO: SHOW POPUP ERROR
		}

		private void OnCancelCommand()
		{
			OnSpecificBackClicked();
		}

		private void OnSendCommand()
		{

			if (string.IsNullOrWhiteSpace(ComplaintContentText) || string.IsNullOrWhiteSpace(LocationText))
			{
				if (string.IsNullOrWhiteSpace(ComplaintContentText))
				{
					ComplaintContentText = null;
					ComplaintContentHint = EmptyComplaintContent;
					IsComplaintContentEmpty = true;
					_selectedBackgroundType.ContentBackgroundType = BackgroundType.Error;
				}

				if (string.IsNullOrWhiteSpace(LocationText))
				{
					LocationText = null;
					LocationHint = EmptyLocation;
					IsLocationEmpty = true;
					_selectedBackgroundType.LocationBackgroundType = BackgroundType.Error;
				}
			}
			else
			{
				var images = new List<MemoryStream>(_attachmentStreams.Where(i => i != null));
				var newComplaintContent = new NewComplaintContent(ComplaintContentText, LocationText, VideoLinkValue,
					images);
				DataModel.NewComplaintContent = newComplaintContent;

				if (_networkHelper.IsConnected)
				{
					// login to send feedback
					if (!MasterViewModel.Instance.IsLoggedIn)
					{
						OnShowDetailView(typeof(SecondLoginScreenViewModel),
							delegate
							{
								ShowViewModel<SecondLoginScreenViewModel>(new
								{
									previousPage = _previousPage
								});
							});
					}
					else
					{
						if (DataModel.UserInfo == null)
						{
							if (DataModel.GetUserInfoNetworkStatus != NetworkStatus.Success)
							{
								MasterViewModel.Instance.IsConnectionErrorPopupShown = true;
							}
							else
							{
								OnShowDetailView(typeof(AddPersonalInfoViewModel),
								delegate
								{
									ShowViewModel<AddPersonalInfoViewModel>(new
									{
										previousPage = _previousPage
									});
								});
							}
						}
						else
						{
							// happy case
							ManagementService.PostComplaint(ComplaintContentText, LocationText, images, VideoLinkValue);
							MasterViewModel.Instance.IsIndicatorShown = true;
							SidebarItemViewModel.SelectedItem.ItemStatus = ItemStatus.Inactive;
							SidebarItemViewModel.SelectedItem.LabelStatus = LabelStatus.Special;
						}
					}
				}
				else
				{
					// popup offline options
					MasterViewModel.Instance.IsPopupOfflineOptionsShown = true;
				}
			}
		}

		private void OnAddImagesCommand()
		{
			var isMaxFileReached = Array.TrueForAll(AttachmentStreamsValue, s => s != null);

			if (isMaxFileReached)
				MasterViewModel.Instance.IsErrorImagesShown = true;
			else
				IsImagePickerShown = true;
		}

		private void OnComplaintContentCommand()
		{
			IsComplaintContentEmpty = false;
			_selectedBackgroundType.ContentBackgroundType = BackgroundType.Normal;
			ComplaintContentHint = NewComplaintContentHint;
		}

		private void OnLocationCommand()
		{
			IsLocationEmpty = false;
			_selectedBackgroundType.LocationBackgroundType = BackgroundType.Normal;
			LocationHint = Location;
			if (string.Equals(LocationText, " "))
				LocationText = null;
		}

		private void OnVideoCommand()
		{
			IsKeyboardHidden = true;
			EnteredVideoLink = string.Empty;
			_selectedBackgroundType.VideoLinkBackgroundType = BackgroundType.Normal;
			IsVideoLinkWrong = false;
			VideoLinkHint = VideoTextHint;
			MasterViewModel.Instance.IsVideoPopupShown = true;
		}

		private void OnConfirmVideoLinkCommand()
		{
			CheckVideoLink(EnteredVideoLink);
		}

		private void OnConfirmCancelCommand()
		{
			if (MasterViewModel.Instance.IsCancelPopupShown)
			{
				MasterViewModel.Instance.IsCancelPopupShown = false;
				DataModel.NewComplaintContent = null;
				OnBackCommand();
			}
		}

		protected override void OnBackCommand()
		{
			base.OnBackCommand();
			OnShowDetailView(typeof(HomeViewModel), delegate
			{
				ShowViewModel<HomeViewModel>(new
				{
					lastComplaintItemId = 0,
					previousPage = _previousPage
				});
			});
		}

		private void OnSaveFeedBackClicked(object sender, EventArgs e)
		{
			_appDataLoader.SaveFeedback(DataModel.NewComplaintContent);

			DataModel.NewComplaintContent = null;

			SidebarItemViewModel.SelectedItem.ItemStatus = ItemStatus.Inactive;
			SidebarItemViewModel.SelectedItem.LabelStatus = LabelStatus.Special;

			MasterViewModel.Instance.ExecuteBeforeShowDetailView(typeof(HomeViewModel),
							delegate
							{
								ShowViewModel<HomeViewModel>(new
								{
									previousPage = _previousPage
								});
							});
		}

		#endregion
	}
}