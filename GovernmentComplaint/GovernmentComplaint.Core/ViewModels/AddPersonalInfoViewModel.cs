﻿using System;
using System.Text.RegularExpressions;
using System.Windows.Input;
using GovernmentComplaint.Core.Enums;
using GovernmentComplaint.Core.Helpers;
using GovernmentComplaint.Core.Localization;
using GovernmentComplaint.Core.Models;
using GovernmentComplaint.Core.Services;
using GovernmentComplaint.Core.ViewModels.Bases;
using MvvmCross.Core.ViewModels;

namespace GovernmentComplaint.Core.ViewModels
{
	public class AddPersonalInfoViewModel : DetailViewModel
	{
		private string _previousPage;
		private IAppDataLoader _appDataLoader;

		#region UI Texts

		public string AddInformationLabel1 => Localizer.GetText("AddInformationLabel1");
		public string AddInformationLabel2 => Localizer.GetText("AddInformationLabel2");
		public string FullName => Localizer.GetText("FullName");
		public string Email => Localizer.GetText("Email");
		public string Phone => Localizer.GetText("Phone");
		public string Cancel => Localizer.GetText("Cancel");
		public string Send => Localizer.GetText("Send");
		public string WrongEmailFormat => Localizer.GetText("WrongEmailFormat");
		public string WrongPhoneNumberFormat => Localizer.GetText("WrongPhoneNumberFormat");
		public string BlankEmailError => Localizer.GetText("BlankEmailError");
		public string BlankPhoneNumberError => Localizer.GetText("BlankPhoneNumberError");
		public string EmptyUserName => Localizer.GetText("EmptyUserName");

		#endregion

		public AddPersonalInfoViewModel(IManagementService managementService, IDataModel dataModel,
			IResourceHelper resourceHelper, IAppDataLoader appDataLoader)
			: base(managementService, dataModel, resourceHelper)
		{
			_appDataLoader = appDataLoader;
			MasterViewModel.Instance.IsIndicatorShown = false;
			MasterViewModel.Instance.TitleBar = Localizer.GetText("RequestInfo");
			MasterViewModel.Instance.LeftButtonClicked += OnLeftButtonClicked;
			InitCommands();
			InitUserInformation();
			MasterViewModel.Instance.IsLeftButtonShown = true;
			MasterViewModel.Instance.IsRightButtonShown = true;
		}

		private void InitUserInformation()
		{
			var user = MasterViewModel.Instance.LoggedInUser;
			if (user != null)
			{
				UserName = user.UserName;
				if (!string.IsNullOrEmpty(user.Email))
				{
					UserEmail = user.Email;
				}
			}
		}

		#region Methods

		public void Init(string previousPage, bool isCalledByOfflineMode = false)
		{
			_previousPage = previousPage;

			if (isCalledByOfflineMode)
			{
				MasterViewModel.Instance.IsLeftButtonShown = false;
				MasterViewModel.Instance.IsRightButtonShown = false;
			}
		}

		private void OnLeftButtonClicked(object sender, EventArgs e)
		{
			OnBackCommand();
		}

		protected virtual void InitCommands()
		{
			CancelCommand = new MvxCommand(OnCancelCommand);
			SendCommand = new MvxCommand(OnSendCommand);
		}

		private void OnSendCommand()
		{
			VerifyUserInfo();
		}

		public virtual void OnCancelCommand()
		{

			if (DataModel.PreviousPage != null && DataModel.PreviousPage.Equals(PreviousPage.ComplaintDetail))
			{
				OnShowDetailView(typeof(ComplaintDetailViewModel), delegate
				{
					ShowViewModel<ComplaintDetailViewModel>(new
					{
						id = DataModel.ComplaintContent.Id,
						pageType = _previousPage,
						searchText = DataModel.Keyword
					});
				});
			}
			else
			{
				IsKeyboardShown = false;
				MasterViewModel.Instance.IsCancelPopupShown = true;
			}
		}

		protected override void OnBackCommand()
		{
			if (!MasterViewModel.Instance.IsLeftButtonShown)
			{
				return;
			}

			if (DataModel.PreviousPage.Equals(PreviousPage.CreateNewComplaint))
			{
				OnShowDetailView(typeof(CreateNewComplaintViewModel), delegate
				{
					ShowViewModel<CreateNewComplaintViewModel>(new
					{
						previousPage = _previousPage
					});
				});
			}
			else if (DataModel.PreviousPage.Equals(PreviousPage.ComplaintDetail))
			{
				OnShowDetailView(typeof(ComplaintDetailViewModel), delegate
			   {
				   ShowViewModel<ComplaintDetailViewModel>(new
				   {
					   id = DataModel.ComplaintContent.Id,
					   pageType = _previousPage,
					   searchText = DataModel.Keyword
				   });
			   });
			}
			else
			{
				MasterViewModel.Instance.ExecuteBeforeShowDetailView(typeof(SecondLoginScreenViewModel), delegate
				{
					ShowViewModel<SecondLoginScreenViewModel>(new { numOfOfflineFeedback = _appDataLoader.AppData.NumberOfOfflineFeedback });
				});
			}
		}

		protected override void UnregisteredEvent()
		{
			base.UnregisteredEvent();
			MasterViewModel.Instance.LeftButtonClicked -= OnLeftButtonClicked;
		}

		private void VerifyUserInfo()
		{
			if (string.IsNullOrEmpty(UserName))
			{
				VerifyMessage = EmptyUserName;
			}
			else
			{
				if (!string.IsNullOrWhiteSpace(UserEmail))
				{
					VerifyMessage = "";
					var expression = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
					;
					if (Regex.IsMatch(UserEmail, expression))
						if (Regex.Replace(UserEmail, expression, string.Empty).Length == 0)
							if (string.IsNullOrWhiteSpace(UserPhoneNumber))
							{
								VerifyMessage = BlankPhoneNumberError;
							}
							else
							{
								if (Regex.Match(UserPhoneNumber, @"^[0]{1}[19]{1}[0-9]{8,9}$").Success)
								{
									if (MasterViewModel.Instance.LoggedInUser.AccountType.Equals(AccountType.FACEBOOK))
									{
										SaveFacebookUser();
									}
									else
									{
										SaveGoogleUser();
									}
								}
								else
								{
									VerifyMessage = WrongPhoneNumberFormat;
								}
							}
						else
							VerifyMessage = WrongEmailFormat;
					else
						VerifyMessage = WrongEmailFormat;
				}
				else
				{
					VerifyMessage = BlankEmailError;
				}
			}
		}

		private void SaveFacebookUser()
		{
			ManagementService.SaveUser(UserName, UserEmail, UserPhoneNumber,
				MasterViewModel.Instance.LoggedInUser.AvatarUrl, AccountType.FACEBOOK.ToString(), MasterViewModel.Instance.LoggedInUser.UserId);
			if (DataModel.PreviousPage != null)
			{
				if (DataModel.PreviousPage != null && DataModel.PreviousPage.Equals(PreviousPage.ComplaintDetail))
				{
					OnShowDetailView(typeof(ComplaintDetailViewModel), delegate
					{
						ShowViewModel<ComplaintDetailViewModel>(new
						{
							id = DataModel.ComplaintContent.Id,
							pageType = _previousPage,
							searchText = DataModel.Keyword
						});
					});
				}
				else if (DataModel.PreviousPage.Equals(PreviousPage.CreateNewComplaint))
				{
					OnShowDetailView(typeof(CreateNewComplaintViewModel), delegate
				   {
					   ShowViewModel<CreateNewComplaintViewModel>(new
					   {
						   previousPage = _previousPage
					   });
				   });
				}
			}
			else
			{
				MasterViewModel.Instance.ShowLastViewModel();
			}
		}

		private void SaveGoogleUser()
		{
			ManagementService.SaveUser(UserName, UserEmail, UserPhoneNumber,
				MasterViewModel.Instance.LoggedInUser.AvatarUrl, AccountType.GOOGLE.ToString(), MasterViewModel.Instance.LoggedInUser.UserId);
			if (DataModel.PreviousPage != null)
			{
				if (DataModel.PreviousPage.Equals(PreviousPage.ComplaintDetail))
				{
					OnShowDetailView(typeof(ComplaintDetailViewModel), delegate
					{
						ShowViewModel<ComplaintDetailViewModel>(new
						{
							id = DataModel.ComplaintContent.Id,
							pageType = _previousPage,
							searchText = DataModel.Keyword
						});
					});
				}
				else if (DataModel.PreviousPage.Equals(PreviousPage.CreateNewComplaint))
				{
					OnShowDetailView(typeof(CreateNewComplaintViewModel), delegate
					{
						ShowViewModel<CreateNewComplaintViewModel>(new
						{
							previousPage = _previousPage
						});
					});
				}
			}
			else
			{
				MasterViewModel.Instance.ShowLastViewModel();
			}
		}

		#endregion

		#region Commands

		public ICommand CancelCommand { get; private set; }
		public ICommand SendCommand { get; private set; }

		#endregion

		#region properties

		public bool IsUserNameNotAvailable => string.IsNullOrEmpty(MasterViewModel.Instance.LoggedInUser.UserName);
		public bool IsUserEmailNotAvailable => string.IsNullOrEmpty(MasterViewModel.Instance.LoggedInUser.Email);

		private string _userName;

		public string UserName
		{
			get { return _userName; }
			set
			{
				_userName = value;
				RaisePropertyChanged(() => UserName);
			}
		}

		private string _userEmail;

		public string UserEmail
		{
			get { return _userEmail; }
			set
			{
				_userEmail = value;
				RaisePropertyChanged(() => UserEmail);
			}
		}

		private string _userPhoneNumber;

		public string UserPhoneNumber
		{
			get { return _userPhoneNumber; }
			set
			{
				_userPhoneNumber = value;
				RaisePropertyChanged(() => UserPhoneNumber);
			}
		}

		private string _verifyMessage;

		public string VerifyMessage
		{
			get { return _verifyMessage; }
			set
			{
				_verifyMessage = value;
				RaisePropertyChanged(() => VerifyMessage);
			}
		}

		private bool _isKeyboardShown;

		public bool IsKeyboardShown
		{
			get { return _isKeyboardShown; }
			set
			{
				_isKeyboardShown = value;
				RaisePropertyChanged(() => IsKeyboardShown);
			}
		}

		#endregion
	}
}