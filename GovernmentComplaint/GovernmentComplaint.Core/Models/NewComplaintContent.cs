﻿using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using GovernmentComplaint.Core.Helpers;
using MvvmCross.Platform;

namespace GovernmentComplaint.Core.Models
{
	[DataContract]
	public class NewComplaintContent
	{
		[DataMember(Name = "ContentText")]
		public string ComplaintContentText { get; set; }
		[DataMember(Name = "Location")]
		public string LocationText { get; set; }
		[DataMember(Name = "VideoLink")]
		public string VideoLinkValue { get; set; }
		[IgnoreDataMember]
		public List<MemoryStream> Images { get; set; }
		[DataMember(Name = "ImagePaths")]
		public List<string> SavedImagePaths { get; set; }

		public NewComplaintContent(string contentText,string location,string videoLink,List<MemoryStream> images)
		{
			ComplaintContentText = contentText;
			LocationText = location;
			VideoLinkValue = VideoLinkValue;
			Images = images;
		}

		public void RestoreImageStreams(IAppDataLoader adAppDataLoader)
		{
			if (SavedImagePaths?.Count > 0)
			{
				Images= new List<MemoryStream>();
				
				foreach (var savedImagePath in SavedImagePaths)
				{
					Images.Add(adAppDataLoader.LoadImage(savedImagePath));
				}
			}

		}
	}
}