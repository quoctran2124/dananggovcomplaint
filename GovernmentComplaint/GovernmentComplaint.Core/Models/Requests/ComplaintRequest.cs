﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace GovernmentComplaint.Core.Models.Requests
{
    [DataContract]
    public class ComplaintRequest : BaseRequest
    {
        [DataMember(Name = "userId")]
        public int UserId { get; set; }

        [DataMember(Name = "noiDienRa")]
        public string Location { get; set; }

        [DataMember(Name = "ngayDienRa")]
        public string DateCreated { get; set; }

        [DataMember(Name = "thoiGianDienRa")]
        public string TimeCreated { get; set; }

        [DataMember(Name = "noiDungYKien")]
        public string Content { get; set; }

        //[DataMember(Name = "noiDungYKienSearch")]
        //public string ContentSearch { get; set; }

        [DataMember(Name = "hinhAnh")]
        public string ImagePath { get; set; }

        [DataMember(Name = "hinhAnh1")]
        public string ImagePath1 { get; set; }

        [DataMember(Name = "hinhAnh2")]
        public string ImagePath2 { get; set; }

        [DataMember(Name = "tenHinhAnh")]
        public string ImageName { get; set; }

        [DataMember(Name = "tenHinhAnh1")]
        public string ImageName1 { get; set; }

        [DataMember(Name = "tenHinhAnh2")]
        public string ImageName2 { get; set; }

        [DataMember(Name = "fileDinhKem")]
        public string Attachment { get; set; }

        [DataMember(Name = "tenFileDinhKem")]
        public string AttachmentName { get; set; }

        [DataMember(Name = "videos")]
        public string VideoPath { get; set; }

        //[JsonIgnore]
        //public string Title { get; set; }

        //[JsonIgnore]
        //public string TitleSearch { get; set; }

        //[JsonIgnore]
        //public int Id { get; set; }

    }
}
