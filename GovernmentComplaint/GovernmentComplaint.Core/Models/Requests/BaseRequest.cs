﻿using System.Runtime.Serialization;

namespace GovernmentComplaint.Core.Models.Requests
{
    public class BaseRequest
    {
        [DataMember(Name = "data")]
        public string Data { get; set; }
    }
}
