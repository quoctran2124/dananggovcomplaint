﻿using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace GovernmentComplaint.Core.Models.Requests
{
    [DataContract]
    public class CommentRequest : BaseRequest
    {
        [JsonIgnore]
        public int Id { get; set; }

        [DataMember(Name = "yKienId")]
        public int ComplaintDetailId { get; set; }

        [DataMember(Name = "thongTinLienHeId")]
        public int UserId { get; set; }

        [DataMember(Name = "chaId")]
        public int? ParentId { get; set; }

        [DataMember(Name = "noiDung")]
        public string CommentContent { get; set; }

        [DataMember(Name = "loaiTuongTac")]
        public string InteractionType { get; set; }
    }
}
