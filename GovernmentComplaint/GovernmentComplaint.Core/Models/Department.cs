﻿using System.Runtime.Serialization;

namespace GovernmentComplaint.Core.Models
{
    [DataContract]
    public class Department
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "ten")]
        public string Name { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
