﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net;
using GovernmentComplaint.Core.Enums;

namespace GovernmentComplaint.Core.Models
{
	public delegate void DataReceivedDelegate(WsCommand command, HttpStatusCode code, NetworkStatus networkStatus);

	public interface IDataModel
	{
		string CommentContent { get; set; }
		Enum PreviousPage { get; set; }
		Type LastViewModelType { get; set; }
		int CatFilter { get; set; }
		int SortFilter { get; set; }
		int HistorySortFilter { get; set; }
		string Keyword { get; set; }
		int DepartmentId { get; set; }
		Authentication UserInfo { get; set; }
		//ICollection<Comment> Comments { get; set; }
		List<Notification> Notifications { get; set; }
		string PreviousPageString { get; set; }

		List<ComplaintContent> ComplaintContents { get; set; }
		List<ComplaintContent> ComplaintHistory { get; set; }

		ICollection<Category> Categories { get; set; }
		ICollection<SortingCondition> SortingConditions { get; set; }
		List<Department> Departments { get; set; }
		Department DepartmentById(int id);

		Category CategoryById(int id);
		SortingCondition SortingById(int id);
		int CountNotification { get; set; }
		bool CanPostComplaint { get; set; }
		bool CanPostComment { get; set; }
		bool CanLikeComment { get; set; }
		ComplaintContent ComplaintContent { get; set; }
		ComplaintContent ComplaintContentById(int id);
		Reply Reply { get; set; }
		FacebookLink FacebookLink { get; set; }
		NewComplaintContent NewComplaintContent { get; set; }
		int NumberOfOfflineFeedback { get; set; }
		NetworkStatus GetUserInfoNetworkStatus { get; set; }
		bool DidTryLogin { get; set; }
		#region Events
		/// <summary>
		/// Everytime call web service will raise this event
		/// </summary>
		event DataReceivedDelegate DataReceived;

		/// <summary>
		/// Raise rata received event with command name and status code
		/// </summary>
		/// <param name="wsCommand"></param>
		/// <param name="statusCode"></param>
		void RaiseDataReceived(WsCommand wsCommand, HttpStatusCode statusCode, NetworkStatus networkStatus);
		
		#endregion
	}
}
