﻿using System;
using System.Runtime.Serialization;

namespace GovernmentComplaint.Core.Models
{
    [DataContract]
    public class FieldOfSuggestion : IEntity
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "tenDonVi")]
        public string UnitName { get; set; }

        [DataMember(Name = "phanHoiDaXuLy")]
        public string FeedbackProcessed { get; set; }

        [DataMember(Name = "tongSoPhanHoi")]
        public string TotalFeedback { get; set; }
    }
}
