﻿using System.Runtime.Serialization;

namespace GovernmentComplaint.Core.Models
{
    [DataContract]
    public class Authentication
    {
        [DataMember(Name = "id")]
        public int UserId { get; set; }

        [DataMember(Name = "tenDayDu")]
        public string FullName { get; set; }

        [DataMember(Name = "avatarUrl")]
        public string AvatarURL { get; set; }

        [DataMember(Name = "numsComment")]
        public int NumsComment { get; set; }

        [DataMember(Name = "numsFeedback")]
        public int NumsFeedback { get; set; }

        [DataMember(Name = "email")]
        public string Email { get; set; }

        [DataMember(Name = "ngayTao")]
        public string DateCreated { get; set; }

        [DataMember(Name = "loaiTaiKhoan")]
        public string AccountType { get; set; }

        [DataMember(Name = "maTaiKhoan")]
        public string AccountId { get; set; }

        [DataMember(Name = "soDienThoai")]
        public string Phone { get; set; }

        [DataMember(Name = "countYKien")]
        public string TotalComplaint { get; set; }

        [DataMember(Name = "countBinhLuan")]
        public string TotalComment { get; set; }
    }
}