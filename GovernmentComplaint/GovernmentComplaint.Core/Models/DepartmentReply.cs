﻿using System;
using System.Runtime.Serialization;

namespace GovernmentComplaint.Core.Models
{
    [DataContract]
    public class DepartmentReply
    {
        [DataMember(Name = "replyId")]
        public int Id { get; set; }

        [DataMember(Name = "department")]
        public Department Department { get; set; }

        [DataMember(Name = "content")]
        public string Content { get; set; }

        [DataMember(Name = "time")]
        public DateTime Time { get; set; }

        [DataMember(Name = "rate")]
        public int Rate { get; set; }
    }
}
