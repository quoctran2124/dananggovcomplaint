﻿using System.Runtime.Serialization;

namespace GovernmentComplaint.Core.Models
{
    [DataContract]
    public class Error
    {
        [DataMember(Name = "errorCode")]
        public string ErrorCode { get; set; }

        [DataMember(Name = "errorDescription")]
        public string Description { get; set; }
    }
}
