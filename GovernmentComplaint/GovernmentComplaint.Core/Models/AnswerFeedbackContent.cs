﻿using System.Runtime.Serialization;

namespace GovernmentComplaint.Core.Models
{
	[DataContract]
	public class AnswerFeedbackContent
	{
		[DataMember(Name = "SatisfySelected")]
		public bool SatisfySelected { get; set; }

	    [DataMember(Name = "SlowResponseSelected")]
	    public bool? SlowResponseSelected { get; set; }

	    [DataMember(Name = "UnSuitAnswerSelected")]
	    public bool? UnSuitAnswerSelected { get; set; }
	}
}