﻿using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace GovernmentComplaint.Core.Models
{
    [DataContract]
    public class FacebookLink : IEntity
    {
        [JsonIgnore]
        public int Id { get; set; }

        [DataMember(Name = "error")]
        public ErrorFacebook ErrorFacebook { get; set; }
    }

    [DataContract]
    public class ErrorFacebook
    {
        [DataMember(Name = "code")]
        public int Code { get; set; }
    }
}
