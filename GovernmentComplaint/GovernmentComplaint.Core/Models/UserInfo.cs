﻿using System;

namespace GovernmentComplaint.Core.Models
{
	public class UserInfo
	{
		public Enum AccountType { get; set; }
		public string UserId { get; set; }
		public string UserName { get; set; }
		public string Email { get; set; }
		public string AvatarUrl { get; set; }
	}
}