﻿using System;

namespace GovernmentComplaint.Core.Models
{
	public interface IEntity
	{
		int Id { get; set; }
	}
}
