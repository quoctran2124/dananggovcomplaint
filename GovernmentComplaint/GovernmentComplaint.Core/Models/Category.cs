﻿using System;
using System.Runtime.Serialization;

namespace GovernmentComplaint.Core.Models
{
    [DataContract]
    public class Category : IEntity
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "ten")]
        public string CatName { get; set; }

        [DataMember(Name = "hinhAnh")]
        public string Image { get; set; }
        
        [DataMember(Name="mauNen")]
        public string Color { get; set; }
    }
}
