﻿namespace GovernmentComplaint.Core.Models
{
    class Factory : IFactory
    {
        public IDataModel CreateDataModel()
        {
            return new DataModel();
        }
    }
}
