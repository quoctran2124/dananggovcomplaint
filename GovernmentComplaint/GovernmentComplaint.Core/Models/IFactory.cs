﻿namespace GovernmentComplaint.Core.Models
{
    public interface IFactory
    {
        IDataModel CreateDataModel();
    }
}
