﻿using System.Runtime.Serialization;

namespace GovernmentComplaint.Core.Models
{
	[DataContract]
	public class SortingCondition
	{
		public int SortId { get; set; }
		public string SortCondition { get; set; }
		public string SortImage { get; set; }
	}
}
