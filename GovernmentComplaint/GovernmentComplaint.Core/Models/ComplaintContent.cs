﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace GovernmentComplaint.Core.Models
{
    [DataContract]
    public class ComplaintContent : IEntity
    {
        [JsonIgnore] public List<byte[]> AllImageData;

        [DataMember(Name = "listTuongTac")] public List<Comment> CommentList;

        [DataMember(Name = "thongTinXuLy")] public List<ThongTinXuLy> ReplyList;

        [DataMember(Name = "tieuDe")]
        public string Title { get; set; }

        [DataMember(Name = "webUrl")]
        public string Url { get; set; }

        [DataMember(Name = "tenLinhVuc")]
        public string Category { get; set; }

        [DataMember(Name = "noiDienRa")]
        public string Location1 { get; set; }

        [DataMember(Name = "quanHuyenDienRa")]
        public string Location2 { get; set; }

        [DataMember(Name = "ngayDienRa")]
        public string DateCreated { get; set; }

        [DataMember(Name = "thoiGianDienRa")]
        public string TimeCreated { get; set; }

        [DataMember(Name = "rate")]
        public double RatingFeedback { get; set; }

        [DataMember(Name = "mauNen")]
        public string Color { get; set; }

        [DataMember(Name = "noiDung")]
        public string Content { get; set; }

        [DataMember(Name = "video")]
        public string VideoPath { get; set; }

        [DataMember(Name = "binhLuan")]
        public int NumsComment { get; set; }

        [DataMember(Name = "quanTam")]
        public int NumsLike { get; set; }

        [DataMember(Name = "tinhTrangXuLy")]
        public string Solved { get; set; }

        [DataMember(Name = "hinhAnh")]
        public string Pictures { get; set; }

        [DataMember(Name = "tenCoQuan")]
        public string Department { get; set; }

        public string[] PictureStrings => Pictures.Split(new[] {"@@"}, StringSplitOptions.RemoveEmptyEntries);

        [DataMember(Name = "id")]
        public int Id { get; set; }
    }

    [DataContract]
    public class ThongTinXuLy
    {
        [DataMember(Name = "ngayXuLy")]
        public string DateReply { get; set; }

        [DataMember(Name = "noiDungXuLy")]
        public string ReplyContent { get; set; }

        [DataMember(Name = "tenCoQuan")]
        public string DepartmentName { get; set; }

        [DataMember(Name = "ratingXyLy")]
        public string Rating { get; set; }

        [DataMember(Name = "soLuotDanhGia")]
        public string RatingCount { get; set; }

        [DataMember(Name = "coQuanId")]
        public int DepartmentId { get; set; }

        [DataMember(Name = "tenFile")]
        public string FileNames { get; set; }

        [DataMember(Name = "fileDinhKem")]
        public string AttachmentContent { get; set; }

        [DataMember(Name = "hinhAnh")]
        public string ReplyImages { get; set; }

        public string[] ReplyImagesStrings => ReplyImages.Split(new[] {"@@"}, StringSplitOptions.RemoveEmptyEntries);

        public string[] FileNamesStrings => FileNames.Split(new[] {"@@"}, StringSplitOptions.RemoveEmptyEntries);

        public string[] AttachmentStrings
            => AttachmentContent.Split(new[] {"@@"}, StringSplitOptions.RemoveEmptyEntries);

        [DataMember(Name = "soLuongQuanTam")]
        public int LikeCount { get; set; }

        [DataMember(Name = "soLuongBinhLuan")]
        public int CommentCount { get; set; }

        [DataMember(Name = "thoiGian")]
        public string CommentTime { get; set; }

        [DataMember(Name = "xuLyId")]
        public long ReplyId { get; set; }
    }

    [DataContract]
    public class Comment
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "mauNenTenNguoiBinhLuan")]
        public string UnusedProperty1 { get; set; }

        [DataMember(Name = "tenCoQuanNguoiBinhLuan")]
        public string UnusedProperty2 { get; set; }

        [DataMember(Name = "email")]
        public string Email { get; set; }

        [DataMember(Name = "noiDung")]
        public string CommentContent { get; set; }

        [DataMember(Name = "avatarUrl")]
        public string AvatarUrl { get; set; }

        [DataMember(Name = "tenNguoiBinhLuan")]
        public string Username { get; set; }

        [DataMember(Name = "loai")]
        public string InteractionType { get; set; }

        [DataMember(Name = "loaiTaiKhoan")]
        public string AccountType { get; set; }

        [DataMember(Name = "ChaId")]
        public int? ParentId { get; set; }

        [DataMember(Name = "maTaiKhoan")]
        public string AccountId { get; set; }

        [DataMember(Name = "soLuongQuanTam")]
        public int LikeCount { get; set; }

        [DataMember(Name = "soLuongBinhLuan")]
        public int CommentCount { get; set; }

        [DataMember(Name = "thoiGian")]
        public string CommentTime { get; set; }
    }
}