﻿using System.Net;
using System.Net.Http;
using GovernmentComplaint.Core.Enums;
using Newtonsoft.Json;
using System.Runtime.Serialization;
using GovernmentComplaint.Core.Helpers;

namespace GovernmentComplaint.Core.Models.Responses
{
    [DataContract]
    public class BaseResponse
    {
        [DataMember(Name = "isError")]
        public bool IsError { get; set; } 

        [DataMember(Name = "error")]
        public Error Error { get; set; }

        [DataMember(Name = "code")]
        public int Code { get; set; }

        [JsonIgnore]
        public bool Success => StatusCode == HttpStatusCode.OK;

        [JsonIgnore]
        public NetworkStatus NetworkStatus { get; set; }

        [JsonIgnore]
        public HttpStatusCode StatusCode { get; set; }

        [JsonIgnore]
        public string RawContent { get; set; }

        public void ConstructFromResponse(HttpResponseMessage httpResponse)
        {
            StatusCode = httpResponse.StatusCode;

            var readAsStringAsync = httpResponse.Content.ReadAsStringAsync();
            RawContent = readAsStringAsync.Result;
        }
    }
}
