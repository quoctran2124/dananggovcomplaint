﻿using System.Runtime.Serialization;

namespace GovernmentComplaint.Core.Models.Responses
{
    [DataContract]
    public class FacebookResult
    {
        [DataMember(Name = "id")]
        public string Id { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "email")]
        public string Email { get; set; }

        [DataMember(Name = "picture")]
        public Avatar Picture { get; set; }

        [DataContract]
        public class Avatar
        {
            [DataMember(Name = "data")]
            public Data Data { get; set; }
        }

        [DataContract]
        public class Data
        {
            [DataMember(Name = "is_silhouette")]
            public bool IsSilhouette { get; set; }

            [DataMember(Name = "url")]
            public string Url { get; set; }
        }
    }
}