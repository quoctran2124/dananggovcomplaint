﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using GovernmentComplaint.Core.Enums;

namespace GovernmentComplaint.Core.Models
{
	public class DataModel : IDataModel
	{
		public string CommentContent { get; set; }
		public Enum PreviousPage { get; set; }
		public Type LastViewModelType { get; set; }
		public NewComplaintContent NewComplaintContent { get; set; }
		public int CatFilter { get; set; }
		public int SortFilter { get; set; }
		public int HistorySortFilter { get; set; }
		public string Keyword { get; set; }
		public int DepartmentId { get; set; }
		public Authentication UserInfo { get; set; }
		public event DataReceivedDelegate DataReceived;
		public bool CanPostComplaint { get; set; }
		public bool CanPostComment { get; set; }
		public bool CanLikeComment { get; set; }
		public string PreviousPageString { get; set; }
		public bool DidTryLogin { get; set; }

		public int NumberOfOfflineFeedback { get; set; }
		public NetworkStatus GetUserInfoNetworkStatus { get; set; }

		public void RaiseDataReceived(WsCommand wsCommand, HttpStatusCode statusCode, NetworkStatus networkStatus)
		{
			DataReceived?.Invoke(wsCommand, statusCode, networkStatus);
		}

		public Notification Notification { get; set; }

		public ICollection<SortingCondition> SortingConditions { get; set; }
		public SortingCondition SortingById(int id)
		{
			return SortingConditions?.FirstOrDefault(b => b.SortId == id);
		}

		public int CountNotification { get; set; }
		public List<Notification> Notifications { get; set; }

		public ComplaintContent ComplaintContent { get; set; }
		public List<ComplaintContent> ComplaintContents { get; set; }
		public List<ComplaintContent> ComplaintHistory { get; set; }
		public ComplaintContent ComplaintContentById(int id)
		{
			return ComplaintContents?.FirstOrDefault(b => b.Id == id);
		}
		public ComplaintContent CreateComplaint(ComplaintContent content)
		{
			ComplaintContents.Add(content);
			return content;
		}

		public Department DepartmentById(int id)
		{
			return Departments?.FirstOrDefault(b => b.Id == id);
		}

		public ICollection<Authentication> Authentications;

		//Category
		public ICollection<Category> Categories { get; set; }


		public Category CategoryById(int id)
		{
			return Categories?.FirstOrDefault(b => b.Id == id);
		}
		public void RaiseCategoriesReceived(IList<Category> categories)
		{
			Categories = categories;
		}

		public FacebookLink FacebookLink { get; set; }
		#region Reply
		public ICollection<Reply> Replies { get; set; }

		public Reply Reply { get; set; }

		public List<Department> Departments { get; set; }


		#endregion

	}
}
