﻿using System;
using System.Runtime.Serialization;

namespace GovernmentComplaint.Core.Models
{
	[DataContract]
	public class Reply
	{
		[DataMember(Name = "id")]
		public int Id { get; set; }

		[DataMember(Name = "tenCoQuan")]
		public string ReplyDepartment { get; set; }

        [DataMember(Name = "fileName")]
        public string FileName { get; set; }

        [DataMember(Name = "filePath")]
        public string FilePath { get; set; }

        [DataMember(Name = "dateReplied")]
        public string DateReplied { get; set; }

        [DataMember(Name = "noiDungXuLy")]
		public string ReplyContent { get; set; }
	}
}
