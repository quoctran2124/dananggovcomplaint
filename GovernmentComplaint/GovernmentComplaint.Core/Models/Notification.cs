﻿using System;
using System.Linq;
using System.Runtime.Serialization;

namespace GovernmentComplaint.Core.Models
{
    [DataContract]
    public class Notification
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "YKienId")]
        public int ComplaintId { get; set; }

        [DataMember(Name = "coQuanTraLoi")]
        public string DepartmentReply { get; set; }

        [DataMember(Name = "loaiThongBao")]
        public string Content { get; set; }

        [DataMember(Name = "hoatDong")]
        public string Action { get; set; }

        [DataMember(Name = "tenNguoiTuongTac")]
        public string UserReply { get; set; }

        [DataMember(Name = "tieuDeYKien")]
        public string Location { get; set; }

        [DataMember(Name = "thoiGianTuongTac")]
        public string Time { get; set; }

        [DataMember(Name = "trangThai")]
        public string Status { get; set; }

        [DataMember(Name = "hinhAnh")]
        public string Image { get; set; }

        public string ImageSource => Image.Split(new[] { "@@" }, StringSplitOptions.RemoveEmptyEntries).FirstOrDefault();
    }
}
