﻿using System.Runtime.Serialization;

namespace GovernmentComplaint.Core.Models
{
    public class Interaction
    {
        [DataMember(Name = "complaintId")]
        public int YKienId { get; set; }

        [DataMember(Name = "userId")]
        public int ThongTinLienHeId { get; set; }

        [DataMember(Name = "commentId")]
        public int ChaId { get; set; }

        [DataMember(Name = "interactionType")]
        public string LoaiTuongTac { get; set; }
    }
}
