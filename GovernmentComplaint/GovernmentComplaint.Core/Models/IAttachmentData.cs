﻿namespace GovernmentComplaint.Core.Models
{
    public interface IAttachmentData
    {
        bool IsContain(string key);
        void AddData(string key, byte[] data);
        byte[] GetData(string key);
        void RemoveData(string key);
    }
}