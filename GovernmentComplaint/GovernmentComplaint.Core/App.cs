using GovernmentComplaint.Core.Helpers;
using GovernmentComplaint.Core.Localization;
using GovernmentComplaint.Core.Models;
using MvvmCross.Platform;
using MvvmCross.Platform.IoC;
using MvvmCross.Platform.Platform;
using MvvmCross.Plugins.File;
using GovernmentComplaint.Core.ViewModels.Bases;
namespace GovernmentComplaint.Core
{
	public class App : MvvmCross.Core.ViewModels.MvxApplication
	{
		public override void Initialize()
		{
			RegisterLocalizer();
			RegisterDataModelFactory();
			RegisterDataModel();
			RegisterAppDataLoader();

			CreatableTypes()
				.EndingWith("Service")
				.AsInterfaces()
				.RegisterAsLazySingleton();

			RegisterAppStart<MasterViewModel>();
		}

		private static void RegisterLocalizer()
		{
			var mvxFileStore = Mvx.Resolve<IMvxFileStore>();
			Mvx.RegisterSingleton(mvxFileStore);
			var mvxResourceLoader = Mvx.Resolve<IMvxResourceLoader>();
			Mvx.RegisterSingleton(mvxResourceLoader);
			Localizer.Initialize();
		}

		private void RegisterDataModelFactory()
		{
			Mvx.RegisterSingleton<IFactory>(new Factory());
		}

		private static void RegisterDataModel()
		{
			var dataModel = Mvx.Resolve<IFactory>().CreateDataModel();
			Mvx.RegisterSingleton(dataModel);
		}

		private void RegisterAppDataLoader()
		{
			var fileStore = Mvx.Resolve<IMvxFileStore>();
			Mvx.RegisterSingleton(fileStore);

			var resourceLoader = Mvx.Resolve<IMvxResourceLoader>();
			Mvx.RegisterSingleton(resourceLoader);

			var appDataLoader = new AppDataLoader(fileStore, resourceLoader);
			appDataLoader.Initialize();
			Mvx.RegisterSingleton<IAppDataLoader>(appDataLoader);
		}
	}
}
