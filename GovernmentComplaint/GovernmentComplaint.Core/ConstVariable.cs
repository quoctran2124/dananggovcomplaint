﻿namespace GovernmentComplaint.Core
{
	public static class ConstVariable
	{
		public const string AuthorizationUsername = "admin";
		public const string AuthorizationPasword = "dtt@2016123!@#";

		public const int ServiceTimeOut = 20; // seconds

		public static int MaxImageSizeBytes = 10485760; // 10 Mb
		public const string ContentType = "application/json";

		public const int TimeToCountTask = 500;
		public const int TimeToFadeOut = 150;
		public const int TimeToLoadView = 700;
		public const int TimeToToastShow = 5000;
		public const int TimeToIndicatorShow = 5000;
		public const int TimeToIndicatorLoadImageShow = 1500;
		public const int TimeToShowKeyboard = 1000;

		public const int UploadImageMaxSize = 1024;

		public const int TimeToLoadNotification = 180000;

		public const string ContactNumber = "02361022";
	}
}
