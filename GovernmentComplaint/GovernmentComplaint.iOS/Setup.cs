using GovernmentComplaint.Core;
using GovernmentComplaint.Core.Helpers;
using GovernmentComplaint.Core.Services.Interface;
using GovernmentComplaint.iOS.Helpers;
using GovernmentComplaint.iOS.Views.Bases;
using MvvmCross.Core.ViewModels;
using MvvmCross.iOS.Platform;
using MvvmCross.iOS.Views.Presenters;
using MvvmCross.Platform;
using MvvmCross.Platform.Platform;
using UIKit;

namespace GovernmentComplaint.iOS
{
	public class Setup : MvxIosSetup
	{
		public Setup(MvxApplicationDelegate applicationDelegate, UIWindow window)
			: base(applicationDelegate, window)
		{
		}

		public Setup(MvxApplicationDelegate applicationDelegate, IMvxIosViewPresenter presenter)
			: base(applicationDelegate, presenter)
		{
		}

		protected override IMvxIosViewPresenter CreatePresenter()
		{
			var presenter = new iOSPresenter(ApplicationDelegate, Window);
			Mvx.RegisterSingleton<IMvxIosViewPresenter>(presenter);
			return presenter;

		}
		protected override IMvxApplication CreateApp()
		{
			return new App();
		}

		protected override IMvxTrace CreateDebugTrace()
		{
			return new DebugTrace();
		}

		protected override void InitializeFirstChance()
		{
			Mvx.RegisterType<IResourceHelper, ResourceHelper>();
			Mvx.RegisterType<ISocialHelper, SocialHelper>();
			Mvx.RegisterType<INetworkHelper, NetworkHelper>();
			Mvx.RegisterType<ITimer, IosTimer>();
			Mvx.RegisterType<IVersionHelper, VersionHelper>();
			Mvx.RegisterType<IBadgeHelper, BadgeHelper>();
		}
	}
}
