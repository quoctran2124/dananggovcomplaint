﻿using System;
using Facebook.CoreKit;
using Foundation;
using Google.Core;
using Google.SignIn;
using GovernmentComplaint.iOS.Views.Bases;
using HockeyApp.iOS;
using MvvmCross.Core.ViewModels;
using MvvmCross.iOS.Platform;
using MvvmCross.Platform;
using UIKit;

namespace GovernmentComplaint.iOS
{
	// The UIApplicationDelegate for the application. This class is responsible for launching the
	// User Interface of the application, as well as listening (and optionally responding) to application events from iOS.
	[Register("AppDelegate")]
	public class AppDelegate : MvxApplicationDelegate
	{
		private const string ClientId = "427252326796-qsbhs39q4rc5gr1euridt40i94l4s8mn.apps.googleusercontent.com";

		public override UIWindow Window { get; set; }

		public override void OnActivated(UIApplication application)
		{
			AppEvents.ActivateApp();
		}

		public override bool FinishedLaunching(UIApplication application, NSDictionary launchOptions)
		{
			Window = new UIWindow(UIScreen.MainScreen.Bounds);
			//Hockey
			var manager = BITHockeyManager.SharedHockeyManager;
			manager.Configure("5ad02dd167264eceb6ad80ba6c5d1377");

			manager.CrashManager.CrashManagerStatus = BITCrashManagerStatus.AutoSend;
			manager.StartManager();

			//Badge register
			var settings =
				UIUserNotificationSettings.GetSettingsForTypes(UIUserNotificationType.Alert | UIUserNotificationType.Badge, null);
			UIApplication.SharedApplication.RegisterUserNotificationSettings(settings);
			//Badge register

			var setup = new Setup(this, Window);
			setup.Initialize();

			var startup = Mvx.Resolve<IMvxAppStart>();
			startup.Start();

			Window.MakeKeyAndVisible();

			NSError configureError;
			Context.SharedInstance.Configure(out configureError);
			if (configureError != null)
			{
				Console.WriteLine("Error configuring the Google context: {0}", configureError);
				SignIn.SharedInstance.ClientID = ClientId;
			}
			return ApplicationDelegate.SharedInstance.FinishedLaunching(application, launchOptions);
		}

		public override bool OpenUrl(UIApplication application, NSUrl url, string sourceApplication, NSObject annotation)
		{
			var fbDidHandle = ApplicationDelegate.SharedInstance.OpenUrl(application, url, sourceApplication, annotation);
			var ggDidHandle = SignIn.SharedInstance.HandleUrl(url, sourceApplication, annotation);
			return fbDidHandle || ggDidHandle;
		}

		public override void WillEnterForeground(UIApplication application)
		{
			var rootViewController = application.KeyWindow.RootViewController.ChildViewControllers;
			var firstViewController = rootViewController[0].ChildViewControllers;
			var currentView = firstViewController[0] as BaseView;
			currentView?.WillEnterForeground();
		}
	}
}