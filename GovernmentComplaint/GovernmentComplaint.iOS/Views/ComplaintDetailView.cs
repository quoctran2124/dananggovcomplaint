using System;
using System.Windows.Input;
using Facebook.ShareKit;
using Foundation;
using GovernmentComplaint.Core.ViewModels;
using GovernmentComplaint.iOS.Controls;
using GovernmentComplaint.iOS.Helpers;
using GovernmentComplaint.iOS.Views.Bases;
using GovernmentComplaint.iOS.Views.TableView.ItemTableViewSource;
using MvvmCross.Binding.BindingContext;
using PatridgeDev;
using SafariServices;
using UIKit;

namespace GovernmentComplaint.iOS.Views
{
	public class ComplaintDetailView : DetailView
	{
		public override void WillEnterForeground()
		{
			CheckNetworkCommand?.Execute(null);
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			CheckNetworkCommand?.Execute(null);
		}

		protected override void InitView()
		{
			CreateScrollView();
			View.Add(ScrollView);
			View.AddGestureRecognizer(new UITapGestureRecognizer(HideKeyboard));

			View.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(ScrollView, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					View, NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(ScrollView, NSLayoutAttribute.Right, NSLayoutRelation.Equal,
					View, NSLayoutAttribute.Right, 1, 0),
				NSLayoutConstraint.Create(ScrollView, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					View, NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(ScrollView, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal,
					View, NSLayoutAttribute.Bottom, 1, 0)
			});
		}

		private void CreateScrollView()
		{
			CreateImageSlider();
			CreateContentView();
			CreateDepartmentBar();
			CreateImagePoint();
			ScrollView = UIHelper.CreateScrollView(0, 0, ColorHelper.White);
			ScrollView.ShowsHorizontalScrollIndicator = true;
			ScrollView.Scrolled += OnScrollViewScrolled;
			ScrollView.Add(_contentView);
			ScrollView.Add(_imageScrollView);
			ScrollView.Add(_imagePointLayout);
			ScrollView.Add(_departmentBar);
			ScrollView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_imageScrollView, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal,
					ScrollView, NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_imageScrollView, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					ScrollView, NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_imageScrollView, NSLayoutAttribute.Width, NSLayoutRelation.Equal,
					ScrollView, NSLayoutAttribute.Width, 1, 0),
				NSLayoutConstraint.Create(_imageScrollView, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					ScrollView, NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_imagePointLayout, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal,
					ScrollView, NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_imagePointLayout, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal,
					_imageScrollView, NSLayoutAttribute.Bottom, 1, -DimensionHelper.MediumContentMargin),
				NSLayoutConstraint.Create(_departmentBar, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal,
					ScrollView, NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_departmentBar, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_imageScrollView, NSLayoutAttribute.Bottom, 1, 0),
				NSLayoutConstraint.Create(_departmentBar, NSLayoutAttribute.Right, NSLayoutRelation.Equal,
					ScrollView, NSLayoutAttribute.Right, 1, 0),
				NSLayoutConstraint.Create(_departmentBar, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					ScrollView, NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_contentView, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					ScrollView, NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_contentView, NSLayoutAttribute.Right, NSLayoutRelation.Equal,
					ScrollView, NSLayoutAttribute.Right, 1, 0),
				NSLayoutConstraint.Create(_contentView, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_departmentBar, NSLayoutAttribute.Bottom, 1, 0),
				NSLayoutConstraint.Create(_contentView, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal,
					ScrollView, NSLayoutAttribute.Bottom, 1, 0)
			});
		}

		private void OnScrollViewScrolled(object sender, EventArgs e)
		{
			HideKeyboard();
		}

		private void CreateContentView()
		{
			_contentView = UIHelper.CreateView(0, 0);
			CreateContentHolder();
			CreateReplyHolder();
			CreateSocialInteraction();
			CreateCommentSection();
			CreateDivider();

			_contentView.Add(_contentHolder);
			_contentView.Add(_replyHolder);
			_contentView.Add(_divider);
			_contentView.Add(_socialInteraction);
			_contentView.Add(_secondDivider);
			_contentView.Add(_commentSection);
			_contentView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_contentHolder, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal,
					_contentView, NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_contentHolder, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_contentView, NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_contentHolder, NSLayoutAttribute.Right, NSLayoutRelation.Equal,
					_contentView, NSLayoutAttribute.Right, 1, 0),
				NSLayoutConstraint.Create(_contentHolder, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal,
					_replyHolder, NSLayoutAttribute.Top, 1, 0),

				NSLayoutConstraint.Create(_replyHolder, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal,
					_contentView, NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_replyHolder, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_videoLinkLayout, NSLayoutAttribute.Bottom, 1, DimensionHelper.BigContentMargin),
				NSLayoutConstraint.Create(_replyHolder, NSLayoutAttribute.Right, NSLayoutRelation.Equal,
					_contentView, NSLayoutAttribute.Right, 1, -DimensionHelper.BigContentMargin),
				NSLayoutConstraint.Create(_replyHolder, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					_contentView, NSLayoutAttribute.Left, 1, DimensionHelper.BigContentMargin),

				NSLayoutConstraint.Create(_divider, NSLayoutAttribute.Width, NSLayoutRelation.Equal,
					_contentView, NSLayoutAttribute.Width, 1, 0),
				NSLayoutConstraint.Create(_divider, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					_contentView, NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_divider, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_replyHolder, NSLayoutAttribute.Bottom, 1, DimensionHelper.BigContentMargin),
				NSLayoutConstraint.Create(_divider, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal,
					_socialInteraction, NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_socialInteraction, NSLayoutAttribute.Width, NSLayoutRelation.Equal,
					_contentView, NSLayoutAttribute.Width, 1, 0),
				NSLayoutConstraint.Create(_socialInteraction, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_divider, NSLayoutAttribute.Bottom, 1, 0),
				NSLayoutConstraint.Create(_socialInteraction, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					_contentView, NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_socialInteraction, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal,
					_secondDivider, NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_secondDivider, NSLayoutAttribute.Width, NSLayoutRelation.Equal,
					_contentView, NSLayoutAttribute.Width, 1, 0),
				NSLayoutConstraint.Create(_secondDivider, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					_contentView, NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_secondDivider, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_socialInteraction, NSLayoutAttribute.Bottom, 1, DimensionHelper.MediumContentMargin),
				NSLayoutConstraint.Create(_commentSection, NSLayoutAttribute.Width, NSLayoutRelation.Equal,
					_contentView, NSLayoutAttribute.Width, 1, 0),
				NSLayoutConstraint.Create(_commentSection, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_secondDivider, NSLayoutAttribute.Bottom, 1, 0),
				NSLayoutConstraint.Create(_commentSection, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					_contentView, NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_commentSection, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal,
					_contentView, NSLayoutAttribute.Bottom, 1, 0)
			});
		}

		private void CreateDivider()
		{
			_divider = UIHelper.CreateView(0, DimensionHelper.ThickDivider, ColorHelper.DarkerGray);
			_secondDivider = UIHelper.CreateView(0, DimensionHelper.ThickDivider, ColorHelper.DarkerGray);
		}

		private void CreateContentHolder()
		{
			_contentHolder = UIHelper.CreateView(0, 0, UIColor.White);
			_lbTitle = UIHelper.CreateLabel(ColorHelper.Black, DimensionHelper.BigTextSize, true,
				UITextAlignment.Justified, 10);
			_lbDateCreated = UIHelper.CreateLabel(ColorHelper.Black, DimensionHelper.SmallTextSize);
			_lbStaticComplaintDate = UIHelper.CreateLabel(ColorHelper.DarkestGray, DimensionHelper.SmallTextSize);
			_lbLocation = UIHelper.CreateLabel(ColorHelper.Black, DimensionHelper.SmallTextSize, false,
				UITextAlignment.Justified, 10);
			_lbStaticComplaintPlace = UIHelper.CreateLabel(ColorHelper.DarkestGray, DimensionHelper.SmallTextSize);
			_lbContent = UIHelper.CreateLabel(ColorHelper.Black, DimensionHelper.SmallTextSize, false,
				UITextAlignment.Justified, 100);

			_lbLocationWidthConstraint = NSLayoutConstraint.Create(_lbLocation, NSLayoutAttribute.Width,
				NSLayoutRelation.Equal, null, NSLayoutAttribute.NoAttribute, 1,
				0);
 
			_lbVideoLink = UIHelper.CreateAlphaLabel(ColorHelper.NeutralBlue, DimensionHelper.SmallTextSize);
			_lbVideoLink.UserInteractionEnabled = false;
			_videoAttachmentImage = UIHelper.CreateImageView(DimensionHelper.AttachedSize, DimensionHelper.AttachedSize);
			_videoAttachmentImage.Image = UIImage.FromFile(ImageHelper.Link);
			_videoAttachmentImage.UserInteractionEnabled = false;
			_videoLinkLayout = UIHelper.CreateAlphaView(0, DimensionHelper.VideoLinkLayoutHeight, 0);
			_videoLinkLayout.Add(_lbVideoLink);
			_videoLinkLayout.Add(_videoAttachmentImage);
			_videoLinkLayout.AddConstraints(new []
			{
				NSLayoutConstraint.Create(_videoAttachmentImage, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					_videoLinkLayout, NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_videoAttachmentImage, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_videoLinkLayout, NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_lbVideoLink, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					_videoAttachmentImage, NSLayoutAttribute.Right, 1, DimensionHelper.MediumContentMargin),
				NSLayoutConstraint.Create(_lbVideoLink, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_videoLinkLayout, NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_lbVideoLink, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal,
					_videoAttachmentImage, NSLayoutAttribute.CenterY, 1, 0),
				NSLayoutConstraint.Create(_lbVideoLink, NSLayoutAttribute.Right, NSLayoutRelation.LessThanOrEqual,
					_videoLinkLayout, NSLayoutAttribute.Right, 1, DimensionHelper.SmallContentMargin),
				NSLayoutConstraint.Create(_lbVideoLink, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal,
					_videoLinkLayout, NSLayoutAttribute.Bottom, 1, 0),
			});

			_contentHolder.Add(_lbTitle);
			_contentHolder.Add(_lbDateCreated);
			_contentHolder.Add(_lbStaticComplaintDate);
			_contentHolder.Add(_lbLocation);
			_contentHolder.Add(_lbStaticComplaintPlace);
			_contentHolder.Add(_lbContent);
			_contentHolder.Add(_videoLinkLayout);
			_contentHolder.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_lbTitle, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					_contentHolder, NSLayoutAttribute.Left, 1, DimensionHelper.BigContentMargin),
				NSLayoutConstraint.Create(_lbTitle, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_contentHolder, NSLayoutAttribute.Top, 1, DimensionHelper.MediumContentMargin),
				NSLayoutConstraint.Create(_lbTitle, NSLayoutAttribute.Right, NSLayoutRelation.Equal,
					_contentHolder, NSLayoutAttribute.Right, 1, -DimensionHelper.BigContentMargin),

				NSLayoutConstraint.Create(_lbStaticComplaintDate, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					_lbTitle, NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_lbStaticComplaintDate, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_lbTitle, NSLayoutAttribute.Bottom, 1, DimensionHelper.MediumContentMargin),

				NSLayoutConstraint.Create(_lbDateCreated, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					_lbStaticComplaintDate, NSLayoutAttribute.Right, 1, DimensionHelper.BigContentMargin),
				NSLayoutConstraint.Create(_lbDateCreated, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_lbTitle, NSLayoutAttribute.Bottom, 1, DimensionHelper.MediumContentMargin),

				NSLayoutConstraint.Create(_lbStaticComplaintPlace, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					_lbTitle, NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_lbStaticComplaintPlace, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_lbStaticComplaintDate, NSLayoutAttribute.Bottom, 1, DimensionHelper.SmallContentMargin),

				NSLayoutConstraint.Create(_lbLocation, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					_lbDateCreated, NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_lbLocation, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_lbStaticComplaintDate, NSLayoutAttribute.Bottom, 1, DimensionHelper.SmallContentMargin),

				_lbLocationWidthConstraint,
				NSLayoutConstraint.Create(_lbContent, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					_lbTitle, NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_lbContent, NSLayoutAttribute.Right, NSLayoutRelation.Equal,
					_contentHolder, NSLayoutAttribute.Right, 1, - DimensionHelper.BigContentMargin),
				NSLayoutConstraint.Create(_lbContent, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_lbLocation, NSLayoutAttribute.Bottom, 1, DimensionHelper.BigContentMargin),

				NSLayoutConstraint.Create(_videoLinkLayout, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					_lbTitle, NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_videoLinkLayout, NSLayoutAttribute.Right, NSLayoutRelation.Equal,
					_contentHolder, NSLayoutAttribute.Right, 1, - DimensionHelper.BigContentMargin),
				NSLayoutConstraint.Create(_videoLinkLayout, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_lbContent, NSLayoutAttribute.Bottom, 1, DimensionHelper.MediumContentMargin)
			});
		}

		private NSLayoutConstraint _heightConstraint;
		private NSLayoutConstraint _heightAnswerFeedbackConstraint;
		private NSLayoutConstraint _heightButtonFeedbackConstraint;
	    
        private void CreateReplyHolder()
		{
			CreateAttachedImagesLayout();
		    CreateAnswerFeedbackContainer();

            _replyHolder = UIHelper.CreateView(0, 0, ColorHelper.BlueGray, false, true);
			_replyHolder.Layer.BorderColor = ColorHelper.BlueGrayBorder.CGColor;
			_lbStaticReply = UIHelper.CreateLabel(ColorHelper.DarkestGray, DimensionHelper.SmallTextSize);
			_lbReplyDepartment = UIHelper.CreateLabel(ColorHelper.Black, DimensionHelper.SmallTextSize, true,
				UITextAlignment.Justified, 10);
			_lbStaticDate = UIHelper.CreateLabel(ColorHelper.DarkestGray, DimensionHelper.SmallTextSize);
			_lbDateReplied = UIHelper.CreateLabel(ColorHelper.Black, DimensionHelper.SmallTextSize);
			_lbReplyContent = UIHelper.CreateLabel(ColorHelper.Black, DimensionHelper.SmallTextSize, false,
				UITextAlignment.Justified, 500);

		    _answerResponseDivider = UIHelper.CreateView(0, DimensionHelper.ThinDivider, ColorHelper.DarkestGray);

            _lbFileName1 = UIHelper.CreateAlphaLabel(ColorHelper.NeutralBlue, DimensionHelper.SmallTextSize);
			_lbFileName1.Clicked += OnAttachedFileName1Clicked;
			_lbFileName2 = UIHelper.CreateAlphaLabel(ColorHelper.NeutralBlue, DimensionHelper.SmallTextSize);
			_lbFileName2.Clicked += OnAttachedFileName2Clicked;
			_lbFileName3 = UIHelper.CreateAlphaLabel(ColorHelper.NeutralBlue, DimensionHelper.SmallTextSize);
			_lbFileName3.Clicked += OnAttachedFileName3Clicked;

			_attachmentImage1 = UIHelper.CreateImageView(DimensionHelper.AttachedSize, DimensionHelper.AttachedSize);
			_attachmentImage1.Image = UIImage.FromFile(ImageHelper.Link);
			_attachmentImage2 = UIHelper.CreateImageView(DimensionHelper.AttachedSize, DimensionHelper.AttachedSize);
			_attachmentImage2.Image = UIImage.FromFile(ImageHelper.Link);
			_attachmentImage3 = UIHelper.CreateImageView(DimensionHelper.AttachedSize, DimensionHelper.AttachedSize);
			_attachmentImage3.Image = UIImage.FromFile(ImageHelper.Link);

			var dot = UIHelper.CreateLabel(ColorHelper.DarkestGray, DimensionHelper.SmallTextSize);
			_lbRateText = UIHelper.CreateLabel(ColorHelper.DarkestGray, DimensionHelper.SmallTextSize);
			_lbTotalRate = UIHelper.CreateLabel(ColorHelper.Black, DimensionHelper.SmallTextSize);
			_ratingView = UIHelper.CreateAlphaView(100, 30, 0);
			_ratingBar = UIHelper.CreateRatingBar(0, 0);
			_ratingBar.UserInteractionEnabled = false;
			_ratingView.Add(_ratingBar);
			_ratingView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_ratingBar, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					_ratingView, NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_ratingBar, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_ratingView, NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_ratingBar, NSLayoutAttribute.Right, NSLayoutRelation.Equal,
					_ratingView, NSLayoutAttribute.Right, 1, 0),
				NSLayoutConstraint.Create(_ratingBar, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal,
					_ratingView, NSLayoutAttribute.Bottom, 1, 0)
			});

            _heightConstraint = NSLayoutConstraint.Create(_replyHolder, NSLayoutAttribute.Height, NSLayoutRelation.Equal,
					null, NSLayoutAttribute.NoAttribute, 1, 0);
		    _heightAnswerFeedbackConstraint = NSLayoutConstraint.Create(_answerFeedbackContainer, NSLayoutAttribute.Height, NSLayoutRelation.Equal,
		        null, NSLayoutAttribute.NoAttribute, 1, 0);

		    _heightButtonFeedbackConstraint = NSLayoutConstraint.Create(_feedbackButtonContainer, NSLayoutAttribute.Height, NSLayoutRelation.Equal,
		        null, NSLayoutAttribute.NoAttribute, 1, 0);
            

            _replyHolder.Add(_lbStaticReply);
			_replyHolder.Add(_lbReplyDepartment);
			_replyHolder.Add(_lbStaticDate);
			_replyHolder.Add(_lbDateReplied);
			_replyHolder.Add(_lbReplyContent);
			_replyHolder.Add(_ratingView);
			_replyHolder.Add(_lbRateText);
			_replyHolder.Add(_lbTotalRate);

		    _replyHolder.Add(_answerFeedbackContainer);
		    _replyHolder.Add(_answerResponseDivider);

            _replyHolder.Add(_lbFileName1);
			_replyHolder.Add(_attachmentImage1);
			_replyHolder.Add(_lbFileName2);
			_replyHolder.Add(_attachmentImage2);
			_replyHolder.Add(_lbFileName3);
			_replyHolder.Add(_attachmentImage3);
			_replyHolder.Add(_attachedImagesLayout);
			_replyHolder.Add(dot);
			_replyHolder.AddConstraints(new[]
			{
				//answer of
				NSLayoutConstraint.Create(_lbStaticReply, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					_replyHolder, NSLayoutAttribute.Left, 1, DimensionHelper.ComplaintDetailMargin),
				NSLayoutConstraint.Create(_lbStaticReply, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_replyHolder, NSLayoutAttribute.Top, 1, DimensionHelper.ComplaintDetailMargin),
				NSLayoutConstraint.Create(_lbReplyDepartment, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					_lbDateReplied, NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_lbReplyDepartment, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_replyHolder, NSLayoutAttribute.Top, 1, DimensionHelper.ComplaintDetailMargin),
				NSLayoutConstraint.Create(_lbReplyDepartment, NSLayoutAttribute.Width, NSLayoutRelation.Equal,
					null, NSLayoutAttribute.NoAttribute, 1,
					DimensionHelper.LocationTextWidth - DimensionHelper.MediumContentMargin),


				//date replied
				NSLayoutConstraint.Create(_lbStaticDate, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					_lbStaticReply, NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_lbStaticDate, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_lbReplyDepartment, NSLayoutAttribute.Bottom, 1, 0),
				NSLayoutConstraint.Create(_lbDateReplied, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					_lbStaticDate, NSLayoutAttribute.Right, 1, DimensionHelper.ComplaintDetailMargin),
				NSLayoutConstraint.Create(_lbDateReplied, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_lbReplyDepartment, NSLayoutAttribute.Bottom, 1, 0),

				//reply content
				NSLayoutConstraint.Create(_lbReplyContent, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					_lbStaticReply, NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_lbReplyContent, NSLayoutAttribute.Right, NSLayoutRelation.Equal,
					_replyHolder, NSLayoutAttribute.Right, 1, -DimensionHelper.ComplaintDetailMargin),
				NSLayoutConstraint.Create(_lbReplyContent, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_lbStaticDate, NSLayoutAttribute.Bottom, 1, DimensionHelper.ComplaintDetailMargin),

				//attached images & files
				NSLayoutConstraint.Create(_attachedImagesLayout, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal,
					_replyHolder, NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_attachedImagesLayout, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_lbReplyContent, NSLayoutAttribute.Bottom, 1, DimensionHelper.ComplaintDetailMargin),

				//file 1
				NSLayoutConstraint.Create(_attachmentImage1, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_attachedImagesLayout, NSLayoutAttribute.Bottom, 1, DimensionHelper.FileNameMarginRight),
				NSLayoutConstraint.Create(_attachmentImage1, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					_lbReplyContent, NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_lbFileName1, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_attachedImagesLayout, NSLayoutAttribute.Bottom, 1, 0),
				NSLayoutConstraint.Create(_lbFileName1, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					_attachmentImage1, NSLayoutAttribute.Right, 1, DimensionHelper.FileNameMarginRight),
				NSLayoutConstraint.Create(_lbFileName1, NSLayoutAttribute.Right, NSLayoutRelation.Equal,
					_replyHolder, NSLayoutAttribute.Right, 1, -DimensionHelper.FileNameMarginRight),

				//file 2
				NSLayoutConstraint.Create(_attachmentImage2, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_lbFileName1, NSLayoutAttribute.Bottom, 1, DimensionHelper.FileNameMarginRight),
				NSLayoutConstraint.Create(_attachmentImage2, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					_lbReplyContent, NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_lbFileName2, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_lbFileName1, NSLayoutAttribute.Bottom, 1, 0),
				NSLayoutConstraint.Create(_lbFileName2, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					_attachmentImage2, NSLayoutAttribute.Right, 1, DimensionHelper.FileNameMarginRight),
				NSLayoutConstraint.Create(_lbFileName2, NSLayoutAttribute.Right, NSLayoutRelation.Equal,
					_replyHolder, NSLayoutAttribute.Right, 1, -DimensionHelper.FileNameMarginRight),

				//file 3
				NSLayoutConstraint.Create(_attachmentImage3, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_lbFileName2, NSLayoutAttribute.Bottom, 1, DimensionHelper.FileNameMarginRight),
				NSLayoutConstraint.Create(_attachmentImage3, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					_lbReplyContent, NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_lbFileName3, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_lbFileName2, NSLayoutAttribute.Bottom, 1, 0),
				NSLayoutConstraint.Create(_lbFileName3, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					_attachmentImage3, NSLayoutAttribute.Right, 1, DimensionHelper.FileNameMarginRight),
				NSLayoutConstraint.Create(_lbFileName3, NSLayoutAttribute.Right, NSLayoutRelation.Equal,
					_replyHolder, NSLayoutAttribute.Right, 1, -DimensionHelper.FileNameMarginRight),
                
                //Feedback the answer
			    NSLayoutConstraint.Create(_answerFeedbackContainer, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _lbFileName3, NSLayoutAttribute.Bottom, 1, DimensionHelper.ContentMarginTop),
			    NSLayoutConstraint.Create(_answerFeedbackContainer, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _replyHolder, NSLayoutAttribute.Left, 1, DimensionHelper.ComplaintDetailMargin),
                NSLayoutConstraint.Create(_answerFeedbackContainer, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _replyHolder, NSLayoutAttribute.Right, 1, -DimensionHelper.ComplaintDetailMargin),
                
                //Divider
			    NSLayoutConstraint.Create(_answerResponseDivider, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _answerFeedbackContainer, NSLayoutAttribute.Bottom, 1, DimensionHelper.LayoutBigMargin),
			    NSLayoutConstraint.Create(_answerResponseDivider, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _replyHolder, NSLayoutAttribute.Left, 1, DimensionHelper.ComplaintDetailMargin),
			    NSLayoutConstraint.Create(_answerResponseDivider, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _replyHolder, NSLayoutAttribute.Right, 1, -DimensionHelper.ComplaintDetailMargin),
                

				//rating
				NSLayoutConstraint.Create(_ratingView, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
				    _answerResponseDivider, NSLayoutAttribute.Bottom, 1, 0),
				NSLayoutConstraint.Create(_ratingView, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					_lbReplyContent, NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_lbTotalRate, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal,
					_ratingView, NSLayoutAttribute.Bottom, 1, DimensionHelper.RatingViewMarginBottom),
				NSLayoutConstraint.Create(_lbTotalRate, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					_ratingView, NSLayoutAttribute.Right, 1, DimensionHelper.RatingViewMarginRight),
				NSLayoutConstraint.Create(_lbRateText, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal,
					_ratingView, NSLayoutAttribute.Bottom, 1, DimensionHelper.RatingViewMarginBottom),
				NSLayoutConstraint.Create(_lbRateText, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					_lbTotalRate, NSLayoutAttribute.Right, 1, DimensionHelper.RatingViewMarginRight),

				NSLayoutConstraint.Create(dot, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal,
					_ratingView, NSLayoutAttribute.Bottom, 1, DimensionHelper.RatingViewMarginBottom),
				NSLayoutConstraint.Create(dot, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					_lbTotalRate, NSLayoutAttribute.Right, 1, DimensionHelper.RatingViewMarginRight),

				NSLayoutConstraint.Create(_replyHolder, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal,
					dot, NSLayoutAttribute.Bottom, 1, DimensionHelper.ReplyHolderMarginBottom)
			});
		}

	    private void CreateAnswerFeedbackContainer()
	    {
	        _answerFeedbackContainer = UIHelper.CreateDisappearableVerticalView(0, ColorHelper.BlueGray);
	        CreateFeedbackAnswerButtonLayout();
            CreateAnswerFeedbackResultContainer();

            _answerFeedbackContainer.Add(_feedbackButtonContainer);
	        _answerFeedbackContainer.Add(_answerFeedbackResultContainer);
	        _answerFeedbackContainer.AddConstraints(new []
	        {
	            NSLayoutConstraint.Create(_feedbackButtonContainer, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _answerFeedbackContainer, NSLayoutAttribute.Top, 1, 0),
	            NSLayoutConstraint.Create(_feedbackButtonContainer, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _answerFeedbackContainer, NSLayoutAttribute.Left, 1, 0),
	            NSLayoutConstraint.Create(_feedbackButtonContainer, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _answerFeedbackContainer, NSLayoutAttribute.Right, 1, 0),

	            NSLayoutConstraint.Create(_answerFeedbackResultContainer, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _answerFeedbackContainer, NSLayoutAttribute.Top, 1, 0),
	            NSLayoutConstraint.Create(_answerFeedbackResultContainer, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _answerFeedbackContainer, NSLayoutAttribute.Left, 1, 0),
	            NSLayoutConstraint.Create(_answerFeedbackResultContainer, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _answerFeedbackContainer, NSLayoutAttribute.Right, 1, 0),
            });
        }

	    private void CreateFeedbackAnswerButtonLayout()
	    {
	        _feedbackButtonContainer = UIHelper.CreateDisappearableVerticalView(0, ColorHelper.BlueGray);
            _btnSendAnswerResponse = UIHelper.CreateButton(0, DimensionHelper.CommonHeight, UIColor.Black, DimensionHelper.MediumTextSize, ColorHelper.TrueBlue, false, false, true);
            _btnSendAnswerResponse.ContentEdgeInsets = new UIEdgeInsets(0, 10, 0, 10);

            _feedbackButtonContainer.Add(_btnSendAnswerResponse);
	        _feedbackButtonContainer.AddConstraints(new[]
	        {
	            NSLayoutConstraint.Create(_btnSendAnswerResponse, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _feedbackButtonContainer, NSLayoutAttribute.Top, 1, 0),
	            NSLayoutConstraint.Create(_btnSendAnswerResponse, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _feedbackButtonContainer, NSLayoutAttribute.Left, 1, 0),
	            NSLayoutConstraint.Create(_btnSendAnswerResponse, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _feedbackButtonContainer, NSLayoutAttribute.Bottom, 1, 0),
            });
        }
	    private void CreateAnswerFeedbackResultContainer()
	    {
	        _answerFeedbackResultContainer = UIHelper.CreateDisappearableVerticalView(0, ColorHelper.BlueGray);
	        _satisfiedStatusIcon = UIHelper.CreateImageView(DimensionHelper.MediumMenuIconHeight, DimensionHelper.MediumMenuIconHeight);
	        _satisfiedStatusIcon.UserInteractionEnabled = false;

	        _satisfiedStatusText = UIHelper.CreateLabel(ColorHelper.Black, DimensionHelper.MediumTextSize, true);

	        _unsatisfiedReasonText = UIHelper.CreateLabel(ColorHelper.Black, DimensionHelper.MediumTextSize, false, UITextAlignment.Left, 2);

	        _answerFeedbackResultContainer.Add(_satisfiedStatusIcon);
	        _answerFeedbackResultContainer.Add(_satisfiedStatusText);
	        _answerFeedbackResultContainer.Add(_unsatisfiedReasonText);
	        _answerFeedbackResultContainer.AddConstraints(new[]
	        {
	            NSLayoutConstraint.Create(_satisfiedStatusIcon, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _answerFeedbackResultContainer, NSLayoutAttribute.Top, 1, 0),
	            NSLayoutConstraint.Create(_satisfiedStatusIcon, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _answerFeedbackResultContainer, NSLayoutAttribute.Left, 1, 0),

	            NSLayoutConstraint.Create(_satisfiedStatusText, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _answerFeedbackResultContainer, NSLayoutAttribute.Top, 1, 0),
	            NSLayoutConstraint.Create(_satisfiedStatusText, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _satisfiedStatusIcon, NSLayoutAttribute.Right, 1, DimensionHelper.LayoutMargin),
	            NSLayoutConstraint.Create(_satisfiedStatusText, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _answerFeedbackResultContainer, NSLayoutAttribute.Right, 1, 0),

                NSLayoutConstraint.Create(_unsatisfiedReasonText, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _satisfiedStatusText, NSLayoutAttribute.Bottom, 1, 0),
	            NSLayoutConstraint.Create(_unsatisfiedReasonText, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _satisfiedStatusText, NSLayoutAttribute.Left, 1, 0),
	            NSLayoutConstraint.Create(_unsatisfiedReasonText, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _answerFeedbackResultContainer, NSLayoutAttribute.Right, 1, 0),
            });
	    }

        private void OnAttachedFileName3Clicked(object sender, EventArgs e)
		{
			var url = new NSUrl(FileUrl3);
			UIApplication.SharedApplication.OpenUrl(url);
		}

		private void OnAttachedFileName2Clicked(object sender, EventArgs e)
		{
			var url = new NSUrl(FileUrl2);
			UIApplication.SharedApplication.OpenUrl(url);
		}

		private void OnAttachedFileName1Clicked(object sender, EventArgs e)
		{
			var url = new NSUrl(FileUrl1);
			UIApplication.SharedApplication.OpenUrl(url);
		}

		private void CreateAttachedImagesLayout()
		{
			_attachedImagesLayout = UIHelper.CreateView(0, 0, ColorHelper.BlueGray);
			_attachLayoutConstraintHeight = NSLayoutConstraint.Create(_attachedImagesLayout, NSLayoutAttribute.Height,
				NSLayoutRelation.Equal,
				null, NSLayoutAttribute.NoAttribute, 1, DimensionHelper.AttachedImageSize);
			_attachLayoutConstraintWidth = NSLayoutConstraint.Create(_attachedImagesLayout, NSLayoutAttribute.Width,
				NSLayoutRelation.Equal,
				null, NSLayoutAttribute.NoAttribute, 1, 0);
			_attachedImage1 = UIHelper.CreateAlphaImageView(0, 0, 0, false, UIViewContentMode.ScaleToFill);
			_attachedImage2 = UIHelper.CreateAlphaImageView(0, 0, 0, false, UIViewContentMode.ScaleToFill);
			_attachedImagesLayout.Add(_attachedImage1);
			_attachedImagesLayout.Add(_attachedImage2);
			_attachedImagesLayout.AddConstraints(new[]
			{
				_attachLayoutConstraintWidth,
				_attachLayoutConstraintHeight,
				NSLayoutConstraint.Create(_attachedImage1, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_attachedImagesLayout, NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_attachedImage1, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					_attachedImagesLayout, NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_attachedImage1, NSLayoutAttribute.Width, NSLayoutRelation.Equal,
					null, NSLayoutAttribute.NoAttribute, 1, DimensionHelper.AttachedImageSize),
				NSLayoutConstraint.Create(_attachedImage1, NSLayoutAttribute.Height, NSLayoutRelation.Equal,
					_attachedImagesLayout, NSLayoutAttribute.Height, 1, 0),
				NSLayoutConstraint.Create(_attachedImage2, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_attachedImagesLayout, NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_attachedImage2, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					_attachedImage1, NSLayoutAttribute.Right, 1, DimensionHelper.LogoLayoutMargin),
				NSLayoutConstraint.Create(_attachedImage2, NSLayoutAttribute.Width, NSLayoutRelation.Equal,
					null, NSLayoutAttribute.NoAttribute, 1, DimensionHelper.AttachedImageSize),
				NSLayoutConstraint.Create(_attachedImage2, NSLayoutAttribute.Height, NSLayoutRelation.Equal,
					_attachedImagesLayout, NSLayoutAttribute.Height, 1, 0)
			});
		}

		private void CreateImageSlider()
		{
			CreateImageLayout();
			_imageScrollView = UIHelper.CreateScrollView(0, DimensionHelper.DetailImageViewHeight,
				ColorHelper.LighterGray);
			_imageScrollView.ShowsHorizontalScrollIndicator = false;
			_imageScrollView.PagingEnabled = true;
			_imageScrollView.Scrolled += OnImageScroll;
			_imageScrollView.AddSubview(_imageLayout);
			_imageScrollView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_imageLayout, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _imageScrollView,
					NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_imageLayout, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _imageScrollView,
					NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_imageLayout, NSLayoutAttribute.Right, NSLayoutRelation.Equal,
					_imageScrollView, NSLayoutAttribute.Right, 1, 0),
				NSLayoutConstraint.Create(_imageLayout, NSLayoutAttribute.Height, NSLayoutRelation.Equal,
					_imageScrollView, NSLayoutAttribute.Height, 1, 0)
			});

			_imageLayout.Clicked += OnImageLayoutClicked;
		}

		private void OnImageLayoutClicked(object sender, EventArgs e)
		{
			ScrollPositionValue = (int) (_imageScrollView.ContentOffset.X / ResolutionHelper.Width);
			DetailImageClickCommand?.Execute(null);
		}

		private void CreateImageLayout()
		{
			_imageLayout = UIHelper.CreateAlphaView(0, DimensionHelper.DetailImageViewHeight, 0, ColorHelper.White);
			_imageLayout.NeedToHideEffect = true;
			_image1 = UIHelper.CreateCustomImageView(0, DimensionHelper.DetailImageViewHeight);
			_image2 = UIHelper.CreateCustomImageView(0, DimensionHelper.DetailImageViewHeight);
			_image3 = UIHelper.CreateCustomImageView(0, DimensionHelper.DetailImageViewHeight);
			_imageScrollViewRightConstraint = NSLayoutConstraint.Create(_imageLayout, NSLayoutAttribute.Right,
				NSLayoutRelation.Equal, _image2, NSLayoutAttribute.Right, 1, 0);

			_image1.ContentMode = UIViewContentMode.ScaleAspectFill;
			_image2.ContentMode = UIViewContentMode.ScaleAspectFill;
			_image3.ContentMode = UIViewContentMode.ScaleAspectFill;

			_image1.Layer.MasksToBounds = true;
			_image2.Layer.MasksToBounds = true;
			_image3.Layer.MasksToBounds = true;

			_imageLayout.AddSubview(_image1);
			_imageLayout.AddSubview(_image2);
			_imageLayout.AddSubview(_image3);
			_imageLayout.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_image1, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _imageLayout,
					NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_image1, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _imageLayout,
					NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_image1, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _imageLayout,
					NSLayoutAttribute.Bottom, 1, 0),
				NSLayoutConstraint.Create(_image1, NSLayoutAttribute.Width, NSLayoutRelation.Equal, null,
					NSLayoutAttribute.NoAttribute, 1, ResolutionHelper.Width),
				NSLayoutConstraint.Create(_image2, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _imageLayout,
					NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_image2, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _image1,
					NSLayoutAttribute.Right, 1, 0),
				NSLayoutConstraint.Create(_image2, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _imageLayout,
					NSLayoutAttribute.Bottom, 1, 0),
				NSLayoutConstraint.Create(_image2, NSLayoutAttribute.Width, NSLayoutRelation.Equal, null,
					NSLayoutAttribute.NoAttribute, 1, ResolutionHelper.Width),
				NSLayoutConstraint.Create(_image3, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _imageLayout,
					NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_image3, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _image2,
					NSLayoutAttribute.Right, 1, 0),
				NSLayoutConstraint.Create(_image3, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _imageLayout,
					NSLayoutAttribute.Bottom, 1, 0),
				NSLayoutConstraint.Create(_image3, NSLayoutAttribute.Width, NSLayoutRelation.Equal, null,
					NSLayoutAttribute.NoAttribute, 1, ResolutionHelper.Width),
				_imageScrollViewRightConstraint
			});
		}

		private void CreateImagePoint()
		{
			_imagePointLayout = UIHelper.CreateAlphaView(0, DimensionHelper.LayoutMargin, 0, ColorHelper.Transparent);
			_imagePoint1 = UIHelper.CreateView(DimensionHelper.PointSize, DimensionHelper.PointSize, ColorHelper.Gray);
			_imagePoint1.Layer.CornerRadius = DimensionHelper.PointSize / 2;

			_imagePoint2 = UIHelper.CreateView(DimensionHelper.PointSize, DimensionHelper.PointSize,
				ColorHelper.LightGray);
			_imagePoint2.Layer.CornerRadius = DimensionHelper.PointSize / 2;

			_imagePoint3 = UIHelper.CreateView(DimensionHelper.PointSize, DimensionHelper.PointSize,
				ColorHelper.LightGray);
			_imagePoint3.Layer.CornerRadius = DimensionHelper.PointSize / 2;

			_pointScrollViewRightConstraint = NSLayoutConstraint.Create(_imagePointLayout, NSLayoutAttribute.Right,
				NSLayoutRelation.Equal, _imagePoint2, NSLayoutAttribute.Right, 1, 0);

			_imagePointLayout.AddSubview(_imagePoint1);
			_imagePointLayout.AddSubview(_imagePoint2);
			_imagePointLayout.AddSubview(_imagePoint3);
			_imagePointLayout.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_imagePoint1, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _imagePointLayout,
					NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_imagePoint1, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					_imagePointLayout, NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_imagePoint2, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _imagePointLayout,
					NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_imagePoint2, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _imagePoint1,
					NSLayoutAttribute.Right, 1, DimensionHelper.PointSize),
				NSLayoutConstraint.Create(_imagePoint3, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _imagePointLayout,
					NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_imagePoint3, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _imagePoint2,
					NSLayoutAttribute.Right, 1, DimensionHelper.PointSize),
				_pointScrollViewRightConstraint
			});
		}

		private void OnImageScroll(object sender, EventArgs e)
		{
			var scroll = (UIScrollView) sender;
			if (scroll.ContentOffset.X == 0)
			{
				_imagePoint1.BackgroundColor = ColorHelper.Gray;
				_imagePoint2.BackgroundColor = ColorHelper.LightGray;
				_imagePoint3.BackgroundColor = ColorHelper.LightGray;
			}
			else if (scroll.ContentOffset.X == ResolutionHelper.Width)
			{
				_imagePoint1.BackgroundColor = ColorHelper.LightGray;
				_imagePoint2.BackgroundColor = ColorHelper.Gray;
				_imagePoint3.BackgroundColor = ColorHelper.LightGray;
			}
			else if (scroll.ContentOffset.X == ResolutionHelper.Width * 2)
			{
				_imagePoint1.BackgroundColor = ColorHelper.LightGray;
				_imagePoint2.BackgroundColor = ColorHelper.LightGray;
				_imagePoint3.BackgroundColor = ColorHelper.Gray;
			}
		}

		private void CreateDepartmentBar()
		{
			_departmentBar = UIHelper.CreateView(0, DimensionHelper.DepartmentBar);
			_lbDepartment = UIHelper.CreateLabel(UIColor.White, DimensionHelper.MediumTextSize, true,
				UITextAlignment.Center, 3);

			_lbDepartment.LineBreakMode = UILineBreakMode.TailTruncation;

			_departmentBar.Add(_lbDepartment);
			_departmentBar.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_lbDepartment, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal,
					_departmentBar, NSLayoutAttribute.CenterY, 1, 0),
				NSLayoutConstraint.Create(_lbDepartment, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal,
					_departmentBar, NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_lbDepartment, NSLayoutAttribute.Left, NSLayoutRelation.GreaterThanOrEqual,
					_departmentBar, NSLayoutAttribute.Left, 1, DimensionHelper.SmallContentMargin),
				NSLayoutConstraint.Create(_lbDepartment, NSLayoutAttribute.Right, NSLayoutRelation.LessThanOrEqual,
					_departmentBar, NSLayoutAttribute.Right, 1, 0),
				NSLayoutConstraint.Create(_departmentBar, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal,
					_lbDepartment, NSLayoutAttribute.Bottom, 1, DimensionHelper.SmallContentMargin)
			});
		}

		private void CreateSocialInteraction()
		{
			_likeButton = new LikeControl
			{
				LikeControlStyle = LikeControlStyle.BoxCount,
				TranslatesAutoresizingMaskIntoConstraints = false
			};
			_shareButton = new ShareButton {TranslatesAutoresizingMaskIntoConstraints = false};
			_googleSharingImage = UIHelper.CreateAlphaImageView(50, 25, 0);
			_googleSharingImage.Image = UIImage.FromFile(ImageHelper.GooglePlusLogo);
			_googleSharingImage.Clicked += _googleSharingClicked;

			_socialInteraction = UIHelper.CreateView(0, DimensionHelper.SocialInterractHeight, UIColor.White);

			_facebookLike = UIHelper.CreateView(0, 0, UIColor.White);
			_facebookLike.Add(_likeButton);
			_facebookLike.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_likeButton, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal,
					_facebookLike, NSLayoutAttribute.CenterY, 1, 0),
				NSLayoutConstraint.Create(_likeButton, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal,
					_facebookLike, NSLayoutAttribute.CenterX, 1, 0)
			});

			_facebookShare = UIHelper.CreateView(0, 0, UIColor.White);
			_facebookShare.Add(_shareButton);
			_facebookShare.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_shareButton, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal,
					_facebookShare, NSLayoutAttribute.CenterY, 1, 0),
				NSLayoutConstraint.Create(_shareButton, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal,
					_facebookShare, NSLayoutAttribute.CenterX, 1, 0)
			});

			_googlePlusShare = UIHelper.CreateView(0, 0, UIColor.White);
			_googlePlusShare.Add(_googleSharingImage);
			_googlePlusShare.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_googleSharingImage, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal,
					_googlePlusShare, NSLayoutAttribute.CenterY, 1, 0),
				NSLayoutConstraint.Create(_googleSharingImage, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal,
					_googlePlusShare, NSLayoutAttribute.CenterX, 1, 0)
			});

			_socialInterationDivider1 = UIHelper.CreateView(DimensionHelper.ThinDivider, 0, ColorHelper.DarkerGray);
			_socialInterationDivider2 = UIHelper.CreateView(DimensionHelper.ThinDivider, 0, ColorHelper.DarkerGray);

			_socialInteraction.AddSubview(_facebookLike);
			_socialInteraction.AddSubview(_socialInterationDivider1);
			_socialInteraction.AddSubview(_facebookShare);
			_socialInteraction.AddSubview(_socialInterationDivider2);
			_socialInteraction.AddSubview(_googlePlusShare);
			_socialInteraction.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_facebookLike, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_socialInteraction, NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_facebookLike, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					_socialInteraction, NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_facebookLike, NSLayoutAttribute.Height, NSLayoutRelation.Equal,
					_socialInteraction, NSLayoutAttribute.Height, 1, 0),
				NSLayoutConstraint.Create(_facebookLike, NSLayoutAttribute.Width, NSLayoutRelation.Equal,
					_socialInteraction, NSLayoutAttribute.Width, OneThree, 0),
				NSLayoutConstraint.Create(_socialInterationDivider1, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_socialInteraction, NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_socialInterationDivider1, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					_facebookLike, NSLayoutAttribute.Right, 1, 0),
				NSLayoutConstraint.Create(_socialInterationDivider1, NSLayoutAttribute.Height, NSLayoutRelation.Equal,
					_socialInteraction, NSLayoutAttribute.Height, 1, 0),
				NSLayoutConstraint.Create(_facebookShare, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_socialInteraction, NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_facebookShare, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					_socialInterationDivider1, NSLayoutAttribute.Right, 1, 0),
				NSLayoutConstraint.Create(_facebookShare, NSLayoutAttribute.Height, NSLayoutRelation.Equal,
					_socialInteraction, NSLayoutAttribute.Height, 1, 0),
				NSLayoutConstraint.Create(_facebookShare, NSLayoutAttribute.Width, NSLayoutRelation.Equal,
					_socialInteraction, NSLayoutAttribute.Width, OneThree, 0),
				NSLayoutConstraint.Create(_socialInterationDivider2, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_socialInteraction, NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_socialInterationDivider2, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					_facebookShare, NSLayoutAttribute.Right, 1, 0),
				NSLayoutConstraint.Create(_socialInterationDivider2, NSLayoutAttribute.Height, NSLayoutRelation.Equal,
					_socialInteraction, NSLayoutAttribute.Height, 1, 0),
				NSLayoutConstraint.Create(_googlePlusShare, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_socialInteraction, NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_googlePlusShare, NSLayoutAttribute.Right, NSLayoutRelation.Equal,
					_socialInteraction, NSLayoutAttribute.Right, 1, 0),
				NSLayoutConstraint.Create(_googlePlusShare, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					_socialInterationDivider2, NSLayoutAttribute.Right, 1, 0),
				NSLayoutConstraint.Create(_googlePlusShare, NSLayoutAttribute.Height, NSLayoutRelation.Equal,
					_socialInteraction, NSLayoutAttribute.Height, 1, 0)
			});
		}

		private void _googleSharingClicked(object sender, EventArgs e)
		{
			var controller = new SFSafariViewController(_sharingUrl);
			PresentViewController(controller, true, null);
		    UIApplication.SharedApplication.OpenUrl(_sharingUrl);
		}

		public override void ViewDidLayoutSubviews()
		{
			base.ViewDidLayoutSubviews();
			_commentTableView.BeginUpdates();
			_commentTableView.EndUpdates();
			var constant = ResolutionHelper.Width - DimensionHelper.BigContentMargin * 2 -
						   _lbStaticComplaintDate.IntrinsicContentSize.Width - DimensionHelper.BigContentMargin;
			_lbLocationWidthConstraint.Constant = constant;

			_addCommentContainer.Layer.CornerRadius = DimensionHelper.ToastCorner;

		}

		private void CreateCommentSection()
		{
			_commentSection = UIHelper.CreateView(0, 0, UIColor.White);
			_lbCommentTitle = UIHelper.CreateLabel(UIColor.Black, DimensionHelper.BigTextSize, true);
			_lbNumberComment = UIHelper.CreateLabel(UIColor.Black, DimensionHelper.BigTextSize);
			_lbLeftParenthesis = UIHelper.CreateLabel(UIColor.Black, DimensionHelper.BigTextSize);
			_lbRightParenthesis = UIHelper.CreateLabel(UIColor.Black, DimensionHelper.BigTextSize);
			_lbTemp = UIHelper.CreateLabel(UIColor.Black, DimensionHelper.BigTextSize);

			_commentTableView = UIHelper.CreateTableView(0, 0, UIColor.White);
			_commentItemSource = new CommentItemTableViewSource(_commentTableView);
			_commentTableView.Source = _commentItemSource;

			_btnAddComment = UIHelper.CreateButton(0, 0, UIColor.White, DimensionHelper.MediumTextSize,
				ColorHelper.TrueBlue,
				false, false, true);
			_ctvAddComment = new CustomTextView();
			_addCommentContainer = UIHelper.CreateView(0, 0, ColorHelper.White, true, true);
			_addCommentContainer.AddSubview(_ctvAddComment);
			_addCommentContainer.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_ctvAddComment, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_addCommentContainer, NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_ctvAddComment, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal,
					_addCommentContainer, NSLayoutAttribute.Bottom, 1, 0),
				NSLayoutConstraint.Create(_ctvAddComment, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					_addCommentContainer, NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_ctvAddComment, NSLayoutAttribute.Right, NSLayoutRelation.Equal,
					_addCommentContainer, NSLayoutAttribute.Right, 1, 0)
			});

			_commentListHeight = NSLayoutConstraint.Create(_commentTableView, NSLayoutAttribute.Height,
				NSLayoutRelation.Equal,
				null, NSLayoutAttribute.NoAttribute, 1, 100);

			_commentSection.Add(_lbCommentTitle);
			_commentSection.Add(_lbNumberComment);
			_commentSection.Add(_lbLeftParenthesis);
			_commentSection.Add(_lbRightParenthesis);
			_commentSection.Add(_lbTemp);
			_commentSection.Add(_commentTableView);
			_commentSection.Add(_btnAddComment);
			_commentSection.Add(_addCommentContainer);

			_commentSection.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_lbCommentTitle, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_commentSection, NSLayoutAttribute.Top, 1, DimensionHelper.MediumContentMargin),
				NSLayoutConstraint.Create(_lbCommentTitle, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					_commentSection, NSLayoutAttribute.Left, 1, DimensionHelper.BigContentMargin),
				NSLayoutConstraint.Create(_lbLeftParenthesis, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_commentSection, NSLayoutAttribute.Top, 1, DimensionHelper.MediumContentMargin),
				NSLayoutConstraint.Create(_lbLeftParenthesis, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					_lbCommentTitle, NSLayoutAttribute.Right, 1, 5),
				NSLayoutConstraint.Create(_lbNumberComment, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_commentSection, NSLayoutAttribute.Top, 1, DimensionHelper.MediumContentMargin),
				NSLayoutConstraint.Create(_lbNumberComment, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					_lbLeftParenthesis, NSLayoutAttribute.Right, 1, 0),
				NSLayoutConstraint.Create(_lbRightParenthesis, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_commentSection, NSLayoutAttribute.Top, 1, DimensionHelper.MediumContentMargin),
				NSLayoutConstraint.Create(_lbRightParenthesis, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					_lbNumberComment, NSLayoutAttribute.Right, 1, 0),

				//Comment item
				NSLayoutConstraint.Create(_commentTableView, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_lbRightParenthesis, NSLayoutAttribute.Bottom, 1, DimensionHelper.LargeContentMargin),
				NSLayoutConstraint.Create(_commentTableView, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					_commentSection, NSLayoutAttribute.Left, 1, DimensionHelper.BigContentMargin),
				NSLayoutConstraint.Create(_commentTableView, NSLayoutAttribute.Right, NSLayoutRelation.Equal,
					_commentSection, NSLayoutAttribute.Right, 1, -DimensionHelper.BigContentMargin),
				NSLayoutConstraint.Create(_commentTableView, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal,
					_ctvAddComment, NSLayoutAttribute.Top, 1, -DimensionHelper.BigContentMargin),
				_commentListHeight,
				NSLayoutConstraint.Create(_btnAddComment, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal,
					_commentSection, NSLayoutAttribute.Bottom, 1, -DimensionHelper.MediumContentMargin),
				NSLayoutConstraint.Create(_btnAddComment, NSLayoutAttribute.Right, NSLayoutRelation.Equal,
					_commentSection, NSLayoutAttribute.Right, 1, -DimensionHelper.BigContentMargin),
				NSLayoutConstraint.Create(_btnAddComment, NSLayoutAttribute.Height, NSLayoutRelation.Equal,
					null, NSLayoutAttribute.NoAttribute, 1, DimensionHelper.CommonHeight),
				NSLayoutConstraint.Create(_btnAddComment, NSLayoutAttribute.Width, NSLayoutRelation.Equal,
					null, NSLayoutAttribute.NoAttribute, 1, DimensionHelper.NComplaintItemHeight),
				NSLayoutConstraint.Create(_addCommentContainer, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal,
					_commentSection, NSLayoutAttribute.Bottom, 1, -DimensionHelper.MediumContentMargin),
				NSLayoutConstraint.Create(_addCommentContainer, NSLayoutAttribute.Right, NSLayoutRelation.Equal,
					_btnAddComment, NSLayoutAttribute.Left, 1, -DimensionHelper.MediumContentMargin),
				NSLayoutConstraint.Create(_addCommentContainer, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					_commentSection, NSLayoutAttribute.Left, 1, DimensionHelper.BigContentMargin),
				NSLayoutConstraint.Create(_addCommentContainer, NSLayoutAttribute.Height, NSLayoutRelation.Equal,
					null, NSLayoutAttribute.NoAttribute, 1, DimensionHelper.CommonHeight)
			});
		}

		public event EventHandler ScrollPositionValueChanged;

		protected override void OnWillShowKeyboard(NSNotification notification)
		{
			base.OnWillShowKeyboard(notification);

			if (!IsKeyboardShown)
			{
				IsKeyboardShown = true;
				var f = _contentView.Frame;
				f.Y -= KeyboardHeight * 2;
				_contentView.Frame = f;
			}
		}

		protected override void OnWillHideKeyboard(NSNotification notification)
		{
			base.OnWillHideKeyboard(notification);
			var f = _contentView.Frame;
			f.Y += KeyboardHeight * 2;
			_contentView.Frame = f;
		}

		private UIImageView GetLastImageView(int imageQuantity)
		{
			switch (imageQuantity)
			{
				case 3:
					return _image3;
				case 2:
					return _image2;
				case 1:
				case 0:
					return _image1;
				default:
					return null;
			}
		}

		private UIView GetLastPointImageView(int imageQuantity)
		{
			switch (imageQuantity)
			{
				case 3:
					return _imagePoint3;
				case 2:
					return _imagePoint2;
				case 1:
				case 0:
					return _imagePoint1;
				default:
					return null;
			}
		}

		private bool _needToHideReplyHolder;

		public bool NeedToHideReplyHolder
		{
			get { return _needToHideReplyHolder; }
			set
			{
				_needToHideReplyHolder = value;
				if (value)
				{
					_replyHolder.Hidden = true;

					_heightConstraint.Constant = -50;
					_replyHolder.AddConstraint(_heightConstraint);
				}
			}
		}

	    private bool _isFeedbackOfUser;

	    public bool IsFeedbackOfUser
        {
	        get => _isFeedbackOfUser;
	        set
	        {
	            _isFeedbackOfUser = value;

	            if (value)
	            {
	                if (!IsAnswerResponseShown)
	                {
	                    _heightAnswerFeedbackConstraint.Constant = DimensionHelper.CommonHeight;
	                    _answerFeedbackContainer.AddConstraint(_heightAnswerFeedbackConstraint);

                        _heightButtonFeedbackConstraint.Constant = DimensionHelper.CommonHeight;
                        _feedbackButtonContainer.AddConstraint(_heightButtonFeedbackConstraint);
                    }
	                else
	                {
	                    _heightButtonFeedbackConstraint.Constant = 0;
                        _feedbackButtonContainer.AddConstraint(_heightButtonFeedbackConstraint);
	                }
                }
                else
                {
                    if (!IsAnswerResponseShown)
                    {
                        _heightAnswerFeedbackConstraint.Constant = 0;
                        _answerFeedbackContainer.AddConstraint(_heightAnswerFeedbackConstraint);

                        _heightButtonFeedbackConstraint.Constant = 0;
                        _feedbackButtonContainer.AddConstraint(_heightButtonFeedbackConstraint);

                        _answerFeedbackResultContainer.Hidden = true;
                    }
                    else
                    {
                        if (IsSatisfactionFeedback)
                        {
                            _heightAnswerFeedbackConstraint.Constant = DimensionHelper.CommonHeight;
                            _answerFeedbackContainer.AddConstraint(_heightAnswerFeedbackConstraint);

                            _answerFeedbackResultContainer.Hidden = false;
                        }
                        else
                        {
                            _answerFeedbackResultContainer.Hidden = false;

                            _heightAnswerFeedbackConstraint.Constant = DimensionHelper.FeedbackContentHeight;
                            _answerFeedbackContainer.AddConstraint(_heightAnswerFeedbackConstraint);
                        }
                    }
                }
	        }
	    }


	    public bool IsAnswerResponseShown { get; set; }
	    public bool IsSatisfactionFeedback { get; set; }

	    protected override void CreateBinding()
		{
			base.CreateBinding();

			var bindingSet = this.CreateBindingSet<ComplaintDetailView, ComplaintDetailViewModel>();

			bindingSet.Bind(_image1).For("ImageUrl").To(vm => vm.ImagePath1);
			bindingSet.Bind(_image2).For("ImageUrl").To(vm => vm.ImagePath2);
			bindingSet.Bind(_image3).For("ImageUrl").To(vm => vm.ImagePath3);

			bindingSet.Bind(_image2).For(v => v.Hidden).To(vm => vm.IsImage2Shown).WithConversion("InvertBool");
			bindingSet.Bind(_image3).For(v => v.Hidden).To(vm => vm.IsImage3Shown).WithConversion("InvertBool");

			bindingSet.Bind(_imagePoint1).For(v => v.Hidden).To(vm => vm.IsIndicator1Shown).WithConversion("InvertBool");
			bindingSet.Bind(_imagePoint2).For(v => v.Hidden).To(vm => vm.IsImage2Shown).WithConversion("InvertBool");
			bindingSet.Bind(_imagePoint3).For(v => v.Hidden).To(vm => vm.IsImage3Shown).WithConversion("InvertBool");

			bindingSet.Bind(this).For(v => v.ImagesQuantity).To(vm => vm.ImagesQuantity);
			bindingSet.Bind(this).For(v => v.CommentQuantity).To(vm => vm.CommentQuantity);

			bindingSet.Bind(_lbDepartment).To(vm => vm.Department);
			bindingSet.Bind(_departmentBar)
				.For(v => v.BackgroundColor)
				.To(vm => vm.Color)
				.WithConversion("HexaToUIColor");

			bindingSet.Bind(_lbTitle).To(vm => vm.Title);
			bindingSet.Bind(_lbStaticComplaintDate).To(vm => vm.StaticComplaintDate);
			bindingSet.Bind(_lbDateCreated).To(vm => vm.DateCreated);
			bindingSet.Bind(_lbStaticComplaintPlace).To(vm => vm.StaticComplaintPlace);
			bindingSet.Bind(_lbLocation).To(vm => vm.Location);
			bindingSet.Bind(_lbContent).To(vm => vm.Content);

			bindingSet.Bind(_replyHolder).For(v => v.Hidden).To(vm => vm.IsReplyShown).WithConversion("InvertBool");


		    bindingSet.Bind(_btnSendAnswerResponse).For("Title").To(vm => vm.ResponseButtonHint);
		    bindingSet.Bind(_btnSendAnswerResponse).For(v => v.ClickCommand).To(vm => vm.AnswerResponseCommand);


            bindingSet.Bind(_lbStaticReply).To(vm => vm.StaticReply);
			bindingSet.Bind(_lbReplyDepartment).To(vm => vm.ReplyDepartment);
			bindingSet.Bind(_lbStaticDate).To(vm => vm.StaticDate);
			bindingSet.Bind(_lbDateReplied).To(vm => vm.DateReplied);
			bindingSet.Bind(_lbReplyContent).To(vm => vm.ReplyContent);

			bindingSet.Bind(_attachedImage1)
				.For(v => v.Image)
				.To(vm => vm.AttachedImage1)
				.WithConversion("UriToUIImage");
			bindingSet.Bind(_attachedImage1).For(v => v.ClickCommand).To(vm => vm.AttachedImage1ClickCommand);
			bindingSet.Bind(_attachedImage1)
				.For(v => v.Hidden)
				.To(vm => vm.IsAttachedImage1Shown)
				.WithConversion("InvertBool");

			bindingSet.Bind(_attachedImage2)
				.For(v => v.Image)
				.To(vm => vm.AttachedImage2)
				.WithConversion("UriToUIImage");
			bindingSet.Bind(_attachedImage2).For(v => v.ClickCommand).To(vm => vm.AttachedImage2ClickCommand);
			bindingSet.Bind(_attachedImage2)
				.For(v => v.Hidden)
				.To(vm => vm.IsAttachedImage2Shown)
				.WithConversion("InvertBool");
			bindingSet.Bind(_attachedImagesLayout)
				.For(v => v.Hidden)
				.To(vm => vm.IsAttachmentImagesShown)
				.WithConversion("InvertBool");

			bindingSet.Bind(_lbFileName1).To(vm => vm.FileName1);
			bindingSet.Bind(_lbFileName1)
				.For(v => v.Hidden)
				.To(vm => vm.IsAttachedFile1Shown)
				.WithConversion("InvertBool");
			bindingSet.Bind(_attachmentImage1)
				.For(v => v.Hidden)
				.To(vm => vm.IsAttachedFile1Shown)
				.WithConversion("InvertBool");
			bindingSet.Bind(this).For(v => v.FileUrl1).To(vm => vm.FileUrl1).TwoWay();

			bindingSet.Bind(_lbFileName2).To(vm => vm.FileName2);
			bindingSet.Bind(_lbFileName2)
				.For(v => v.Hidden)
				.To(vm => vm.IsAttachedFile2Shown)
				.WithConversion("InvertBool");
			bindingSet.Bind(_attachmentImage2)
				.For(v => v.Hidden)
				.To(vm => vm.IsAttachedFile2Shown)
				.WithConversion("InvertBool");
			bindingSet.Bind(this).For(v => v.FileUrl2).To(vm => vm.FileUrl2).TwoWay();

			bindingSet.Bind(_lbFileName3).To(vm => vm.FileName3);
			bindingSet.Bind(_lbFileName3)
				.For(v => v.Hidden)
				.To(vm => vm.IsAttachedFile3Shown)
				.WithConversion("InvertBool");
			bindingSet.Bind(_attachmentImage3)
				.For(v => v.Hidden)
				.To(vm => vm.IsAttachedFile3Shown)
				.WithConversion("InvertBool");
			bindingSet.Bind(this).For(v => v.FileUrl3).To(vm => vm.FileUrl3).TwoWay();

			bindingSet.Bind(this).For(v => v.AttachedImagesQuantity).To(vm => vm.AttachedImagesQuantity);

			bindingSet.Bind(_lbVideoLink).To(vm => vm.VideoLink);
			bindingSet.Bind(_videoLinkLayout)
				.For(v => v.Hidden)
				.To(vm => vm.IsVideoLinkShown)
				.WithConversion("InvertBool");
			bindingSet.Bind(_videoLinkLayout).For(v => v.ClickCommand).To(vm => vm.OpenVideoLinkCommand);

			bindingSet.Bind(_lbLeftParenthesis).To(vm => vm.LeftParenthesis);
			bindingSet.Bind(_lbRightParenthesis).To(vm => vm.RightParenthesis);
			bindingSet.Bind(_lbCommentTitle).To(vm => vm.Comment);
			bindingSet.Bind(_lbNumberComment).To(vm => vm.NumsComment);
			bindingSet.Bind(_ctvAddComment.TextView).For("Text").To(vm => vm.CommentContent);
			bindingSet.Bind(_ctvAddComment).For(v => v.PlaceHolder).To(vm => vm.CommentHint);
			bindingSet.Bind(_btnAddComment).For("Title").To(vm => vm.CommentButtonHint);
			bindingSet.Bind(_btnAddComment).For(v => v.ClickCommand).To(vm => vm.AddCommentCommand);
			bindingSet.Bind(_commentItemSource).To(vm => vm.CommentItemViewModels);
			bindingSet.Bind(_lbRateText).For(v => v.Text).To(vm => vm.RatingText);
			bindingSet.Bind(_lbTotalRate).For(v => v.Text).To(vm => vm.RatingCount);

			bindingSet.Bind(_ratingBar).For(v => v.ChosenRating).To(vm => vm.Rating);
			bindingSet.Bind(_ratingView).For(v => v.ClickCommand).To(vm => vm.RatingCommand);

			bindingSet.Bind(this).For(v => v.CheckNetworkCommand).To(vm => vm.CheckNetworkCommand);

			bindingSet.Bind(this).For(v => v.ScrollPositionValue).To(vm => vm.ScrollPositionValue).TwoWay();
			bindingSet.Bind(this).For(v => v.DetailImageClickCommand).To(vm => vm.DetailImageClickCommand).TwoWay();

			bindingSet.Bind(this).For(v => v.IsKeyboardHidden).To(vm => vm.IsKeyboardHidden);

			bindingSet.Bind(this).For(v => v.ComplaintDetailUrl).To(vm => vm.ComplaintDetailURL);

			bindingSet.Bind(this).For(v => v.NeedToShowPlaceHolder).To(vm => vm.NeedToShowPlaceHolder);

			bindingSet.Bind(this).For(v => v.NeedToHideReplyHolder).To(vm => vm.NeedToHideReplyHolder);

		    bindingSet.Bind(_satisfiedStatusIcon).For(v => v.Image).To(vm => vm.AnswerResponseResultIconName).WithConversion("IconNameToUIImage");
		    bindingSet.Bind(_satisfiedStatusText).To(vm => vm.AnswerResponseResultText);
            bindingSet.Bind(_unsatisfiedReasonText).To(vm => vm.UnsatisfiedReasonText);

            bindingSet.Bind(this).For(v => v.IsAnswerResponseShown).To(vm => vm.IsAnswerResponseShown);
            bindingSet.Bind(this).For(v => v.IsFeedbackOfUser).To(vm => vm.IsFeedbackOfUser);
            bindingSet.Bind(this).For(v => v.IsSatisfactionFeedback).To(vm => vm.IsSatisfactionFeedback);
            bindingSet.Apply();
		}


        #region UI Elements

        //Properties
        private int _attachedImagesQuantity;
		private int _commentQuantity;
		private string _complaintDetailUrl;
		private int _imagesQuantity;
		private int _scrollPositionValue;

		public ICommand DetailImageClickCommand { get; set; }

		public int AttachedImagesQuantity
		{
			get { return _attachedImagesQuantity; }
			set
			{
				_attachedImagesQuantity = value;
				if (value == 0)
				{
					_attachLayoutConstraintHeight.Constant = 0;
				}
				else
				{
					_attachLayoutConstraintHeight.Constant = DimensionHelper.AttachedImageSize;
					if (value == 2)
						_attachLayoutConstraintWidth.Constant = DimensionHelper.AttachedImageSize * 2 + 20;
					else
						_attachLayoutConstraintWidth.Constant = DimensionHelper.AttachedImageSize;
				}
			}
		}

		public int ImagesQuantity
		{
			get { return _imagesQuantity; }
			set
			{
				_imagesQuantity = value;
				var lastImageView = GetLastImageView(value);
				var lastPointView = GetLastPointImageView(value);
				_imageLayout.RemoveConstraint(_imageScrollViewRightConstraint);
				_imagePointLayout.RemoveConstraint(_pointScrollViewRightConstraint);

				_imageScrollViewRightConstraint = NSLayoutConstraint.Create(_imageLayout, NSLayoutAttribute.Right,
					NSLayoutRelation.Equal,
					lastImageView, NSLayoutAttribute.Right, 1, 0);
				_pointScrollViewRightConstraint = NSLayoutConstraint.Create(_imagePointLayout, NSLayoutAttribute.Right,
					NSLayoutRelation.Equal,
					lastPointView, NSLayoutAttribute.Right, 1, 0);
				_imageLayout.AddConstraint(_imageScrollViewRightConstraint);
				_imagePointLayout.AddConstraint(_pointScrollViewRightConstraint);
				if (value == 0)
					_image1.Image = UIImage.FromFile(ImageHelper.DefaultImage);
			}
		}

		public int CommentQuantity
		{
			get { return _commentQuantity; }
			set
			{
				_commentQuantity = value;

				switch (_commentQuantity)
				{
					case 0:
						_commentListHeight.Constant = 20;
						break;
					case 1:
						_commentListHeight.Constant = 150;
						break;
					default:
						_commentListHeight.Constant = 200;
						break;
				}
			}
		}

		public int ScrollPositionValue
		{
			get { return _scrollPositionValue; }
			set
			{
				_scrollPositionValue = value;
				ScrollPositionValueChanged?.Invoke(this, null);
			}
		}

		public string ComplaintDetailUrl
		{
			get { return _complaintDetailUrl; }
			set
			{
				if (!string.IsNullOrEmpty(value))
				{
					//Facebook like and share
					_likeButton.SetObjectId(value);
					_content = new ShareLinkContent();
					_content.SetContentUrl(new NSUrl(value));
					_shareButton.SetShareContent(_content);
					//Google sharing
					_googleSharingImage.Image = UIImage.FromFile(ImageHelper.GooglePlusLogo);
					_googleSharingImage.UserInteractionEnabled = true;
					var url = new NSUrl(value);
					var urlComponents = new NSUrlComponents("https://plus.google.com/share");
					var query = new NSUrlQueryItem("url", url.AbsoluteString);
					urlComponents.QueryItems = new[]
					{
						query,
					};
					_sharingUrl = urlComponents.Url;
				}
				else
				{
					_googleSharingImage.Image = UIImage.FromFile(ImageHelper.GooglePlusLogoGray);
					_googleSharingImage.UserInteractionEnabled = false;
				}
				_complaintDetailUrl = value;
			}
		}

		private bool _isKeyboardHidden;

		public bool IsKeyboardHidden
		{
			get { return _isKeyboardHidden; }
			set
			{
				_isKeyboardHidden = value;
				if (value)
					View.EndEditing(true);
			}
		}

		private bool _needToShowPlaceHolder;
		public bool NeedToShowPlaceHolder
		{
			get { return _needToShowPlaceHolder; }
			set
			{
				_needToShowPlaceHolder = value;
				if (value)
				{
					_ctvAddComment.LbPlaceHolder.Hidden = false;
				}
			}
		}

		//Scroll View
		public UIScrollView ScrollView { get; private set; }
		private UIView _contentView;
		//ImageSlider
		private UIView _departmentBar;
		private UILabel _lbDepartment;
		private AlphaUIView _imageLayout;
		private UIScrollView _imageScrollView;
		private CustomImageView _image1, _image2, _image3;
		private AlphaUIView _imagePointLayout;
		private UIView _imagePoint1;
		private UIView _imagePoint2;
		private UIView _imagePoint3;
		private NSLayoutConstraint _imageScrollViewRightConstraint;
		private NSLayoutConstraint _pointScrollViewRightConstraint;

		//Content
		private UIView _contentHolder;
		private UILabel _lbTitle;
		private UILabel _lbDateCreated;
		private UILabel _lbStaticComplaintDate;
		private UILabel _lbLocation;
		private UILabel _lbStaticComplaintPlace;
		private UILabel _lbContent;
		private AlphaLabel _lbVideoLink;
		private UIImageView _videoAttachmentImage;
		private AlphaUIView _videoLinkLayout;

		//Reply
		private UIView _replyHolder;
		private UILabel _lbStaticReply;
		private UILabel _lbReplyDepartment;
		private UILabel _lbStaticDate;
		private UILabel _lbDateReplied;
		private UILabel _lbReplyContent;
		private AlphaLabel _lbFileName1, _lbFileName2, _lbFileName3;

		private NSLayoutConstraint _attachLayoutConstraintHeight,
			_attachImage1CenterXConstraint,
			_attachLayoutConstraintWidth;

		private NSLayoutConstraint _lbLocationWidthConstraint;

		public string FileUrl1 { get; set; }
		public string FileUrl2 { get; set; }
		public string FileUrl3 { get; set; }

		private UILabel _lbTotalRate;
		private UILabel _lbRateText;
		private AlphaImageView _attachedImage1, _attachedImage2;
		private UIImageView _attachmentImage1, _attachmentImage2, _attachmentImage3;
		private UIView _attachedImagesLayout;

		//Divider
		private UIView _divider;
		private UIView _socialInterationDivider1, _socialInterationDivider2;
		private UIView _secondDivider;

		//Social interaction
		private const float OneThree = 1 / 3f;
		private UIView _socialInteraction;
		private UIView _facebookLike;
		private UIView _facebookShare;
		private UIView _googlePlusShare;
		private LikeControl _likeButton;
		private ShareButton _shareButton;
		private ShareLinkContent _content;
		private AlphaImageView _googleSharingImage;
		private NSUrl _sharingUrl;
		//Comment Section
		private UIView _commentSection;
		private UILabel _lbCommentTitle;
		private UILabel _lbLeftParenthesis;
		private UILabel _lbRightParenthesis;
		private UILabel _lbTemp;
		private UILabel _lbNumberComment;
		private CommentItemTableViewSource _commentItemSource;
		private UITableView _commentTableView;
		private CustomTextView _ctvAddComment;
		private AlphaUIButton _btnAddComment;
		private PDRatingView _ratingBar;
		private NSLayoutConstraint _commentListHeight;
		private AlphaUIView _ratingView;
		private UIView _addCommentContainer;

        // Response Answer 
        private UIView _answerResponseContainer;
	    private AlphaUIButton _btnSendAnswerResponse;
	    private UIView _answerResponseDivider;

        // Feedback Container View
	    private DisappearableVerticalUiView _answerFeedbackContainer;
	    private DisappearableVerticalUiView _feedbackButtonContainer;
	    private DisappearableVerticalUiView _answerFeedbackResultContainer;

	    private UIImageView _satisfiedStatusIcon;
	    private UILabel _satisfiedStatusText;
	    private UILabel _unsatisfiedReasonText;


        public ICommand CheckNetworkCommand { get; set; }

		#endregion
	}
}