﻿using System;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Input;
using AssetsLibrary;
using CoreAnimation;
using CoreGraphics;
using Foundation;
using GovernmentComplaint.Core;
using GovernmentComplaint.Core.Helpers;
using GovernmentComplaint.iOS.Controls;
using GovernmentComplaint.iOS.Helpers;
using GovernmentComplaint.iOS.Views.Bases;
using MobileCoreServices;
using MvvmCross.Platform;
using UIKit;

namespace GovernmentComplaint.iOS.Views
{
	public partial class CreateNewComplaintView : DetailView
	{
		private nfloat _keyboardHeight;
		private bool _isKeyboardShown;

		protected override void InitView()
		{
			InitUiElements();
			InitElementContainers();
			InitMainView();
			base.InitView();
			InitEventHandler();
		}

		private void InitMainView()
		{
			View.AddGestureRecognizer(new UITapGestureRecognizer(HideKeyboard));
			View.BackgroundColor = ColorHelper.LightestGray;
			_contentView = UIHelper.CreateView(0, 0);
			_contentView.Add(_sendButtonContainer);
			_contentView.Add(_cancelButton);
			_contentView.Add(_attachRegionContainer);
			_contentView.Add(_linkVideoContainer);
			_contentView.Add(_linkPictureContainer);
			_contentView.Add(_lbAttachText);
			_contentView.Add(_locationContainer);
			_contentView.Add(_complaintContentContainer);

			#region View Constraints

			_contentView.AddConstraints(new[]
			{
				//Send Button
				NSLayoutConstraint.Create(_sendButtonContainer, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.Right, 1, -DimensionHelper.NComplaintCommonMargin),
				NSLayoutConstraint.Create(_sendButtonContainer, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.CenterX, 1, DimensionHelper.NComplaintCommonMargin / 2),
				NSLayoutConstraint.Create(_sendButtonContainer, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.Bottom, 1, -DimensionHelper.NComplaintCommonMargin),

				//Cancel Button
				NSLayoutConstraint.Create(_cancelButton, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.Left, 1, DimensionHelper.NComplaintCommonMargin),
				NSLayoutConstraint.Create(_cancelButton, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.CenterX, 1, -DimensionHelper.NComplaintCommonMargin / 2),
				NSLayoutConstraint.Create(_cancelButton, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.Bottom, 1, -DimensionHelper.NComplaintCommonMargin),

				//Attached image region
				NSLayoutConstraint.Create(_attachRegionContainer, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.Right, 1, -DimensionHelper.NComplaintCommonMargin),
				NSLayoutConstraint.Create(_attachRegionContainer, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.Left, 1, DimensionHelper.NComplaintCommonMargin),
				NSLayoutConstraint.Create(_attachRegionContainer, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal,
					_sendButtonContainer, NSLayoutAttribute.Top, 1, -DimensionHelper.AttachedSize),

				//Link Video
				NSLayoutConstraint.Create(_linkVideoContainer, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.Right, 1, -DimensionHelper.NComplaintCommonMargin),
				NSLayoutConstraint.Create(_linkVideoContainer, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal,
					_attachRegionContainer, NSLayoutAttribute.Top, 1, -DimensionHelper.NComplaintCommonMargin),

				//Link Picture
				NSLayoutConstraint.Create(_linkPictureContainer, NSLayoutAttribute.Right, NSLayoutRelation.Equal,
					_linkVideoContainer, NSLayoutAttribute.Left, 1, -DimensionHelper.NComplaintCommonMargin),
				NSLayoutConstraint.Create(_linkPictureContainer, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal,
					_attachRegionContainer, NSLayoutAttribute.Top, 1, -DimensionHelper.NComplaintCommonMargin),

				//Attach Text
				NSLayoutConstraint.Create(_lbAttachText, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.Left, 1, DimensionHelper.NComplaintAttachTextPadding),
				NSLayoutConstraint.Create(_lbAttachText, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal,
					_attachRegionContainer, NSLayoutAttribute.Top, 1, -2 * DimensionHelper.NComplaintBigIconPadding),

				//Location
				NSLayoutConstraint.Create(_locationContainer, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.Right, 1, -DimensionHelper.NComplaintCommonMargin),
				NSLayoutConstraint.Create(_locationContainer, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _contentView,
					NSLayoutAttribute.Left, 1, DimensionHelper.NComplaintCommonMargin),
				NSLayoutConstraint.Create(_locationContainer, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal,
					_linkVideoContainer, NSLayoutAttribute.Top, 1, -DimensionHelper.NComplaintCommonMargin),

				//Complaint content
				NSLayoutConstraint.Create(_complaintContentContainer, NSLayoutAttribute.Right, NSLayoutRelation.Equal,
					_contentView, NSLayoutAttribute.Right, 1, -DimensionHelper.NComplaintCommonMargin),
				NSLayoutConstraint.Create(_complaintContentContainer, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					_contentView, NSLayoutAttribute.Left, 1, DimensionHelper.NComplaintCommonMargin),
				NSLayoutConstraint.Create(_complaintContentContainer, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal,
					_locationContainer, NSLayoutAttribute.Top, 1, -DimensionHelper.NComplaintCommonMargin),
				NSLayoutConstraint.Create(_complaintContentContainer, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_contentView, NSLayoutAttribute.Top, 1, DimensionHelper.NComplaintCommonMargin),
			});

			#endregion

			View.Add(_contentView);
			View.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_contentView, NSLayoutAttribute.Right, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Right, 1, 0),
				NSLayoutConstraint.Create(_contentView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_contentView, NSLayoutAttribute.Left, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_contentView, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Bottom, 1, 0),
			});
		}

		private void InitUiElements()
		{
			//Send button
			_sendButtonContainer = UIHelper.CreateAlphaView(0, DimensionHelper.NComplaintItemHeight, 0,
				ColorHelper.Orange, true);
			_lbSendText = UIHelper.CreateAlphaLabel(ColorHelper.White, DimensionHelper.BigTextSize, true);
			_lbSendText.UserInteractionEnabled = false;
			_ivSendIcon = UIHelper.CreateAlphaImageView(DimensionHelper.NComplaintBigIconSize,
				DimensionHelper.NComplaintBigIconSize, DimensionHelper.NComplaintBigIconPadding);
			_ivSendIcon.UserInteractionEnabled = false;
			_ivSendIcon.Image = UIImage.FromFile(ImageHelper.Send);

			//Cancel button
			_cancelButton = UIHelper.CreateButton(0, DimensionHelper.NComplaintItemHeight,
				ColorHelper.LightGray, DimensionHelper.BigTextSize, ColorHelper.BorderGray, true, true, true);

			//Link video
			_linkVideoContainer = UIHelper.CreateAlphaView(DimensionHelper.NComplaintSmallButtonWidth,
				DimensionHelper.NComplaintSmallButtonHeight, 0, null, true, true);
			_lbLinkVideoText = UIHelper.CreateAlphaLabel(ColorHelper.LightGray, DimensionHelper.MediumTextSize);
			_ivLinkVideoIcon = UIHelper.CreateAlphaImageView(DimensionHelper.NComplaintSmallButtonHeight,
				DimensionHelper.NComplaintSmallButtonHeight, DimensionHelper.NComplaintSmallIconPadding);
			_ivLinkVideoIcon.Image = UIImage.FromFile(ImageHelper.Link);

			//Link picture
			_linkPictureContainer = UIHelper.CreateAlphaView(DimensionHelper.NComplaintSmallButtonWidth,
				DimensionHelper.NComplaintSmallButtonHeight, 0, null, true, true);
			_lbLinkPictureText = UIHelper.CreateAlphaLabel(ColorHelper.LightGray, DimensionHelper.MediumTextSize);
			_ivLinkPcitureIcon = UIHelper.CreateAlphaImageView(DimensionHelper.NComplaintSmallButtonHeight,
				DimensionHelper.NComplaintSmallButtonHeight, DimensionHelper.NComplaintSmallIconPadding);
			_ivLinkPcitureIcon.Image = UIImage.FromFile(ImageHelper.Images);

			//Attach text
			_lbAttachText = UIHelper.CreateAlphaLabel(ColorHelper.LightGray, DimensionHelper.MediumTextSize);

			//Attached image region
			_attachRegionContainer = UIHelper.CreateView(0, DimensionHelper.NComplaintAttachedRegionHeight);
			_lbNoAttachedText = UIHelper.CreateAlphaLabel(ColorHelper.LightGray, DimensionHelper.MediumTextSize);
			_lbNoAttachedText.Font = UIFont.FromName("Roboto-Italic", DimensionHelper.MediumTextSize);

			_videoLinkContainer = UIHelper.CreateView(0, DimensionHelper.VideoLinkLayoutHeight);
			_videoLinkContainer.Hidden = true;

			_ivSmallLinkVideoIcon = UIHelper.CreateAlphaImageView(DimensionHelper.VideoLinkLayoutHeight,
				DimensionHelper.VideoLinkLayoutHeight, DimensionHelper.VideoLinkLayoutIconPadding);
			_ivSmallLinkVideoIcon.Image = UIImage.FromFile(ImageHelper.Link);

			_ivRemoveIcon = UIHelper.CreateAlphaImageView(DimensionHelper.VideoLinkLayoutHeight,
				DimensionHelper.VideoLinkLayoutHeight, 2);
			_ivRemoveIcon.Clicked += OnRemoveVideoLinkClicked;
			_ivRemoveIcon.Image = UIImage.FromFile(ImageHelper.Remove);
			_lbVideoLink = UIHelper.CreateAlphaLabel(ColorHelper.Blue, DimensionHelper.MediumTextSize);
			_imageRegion = UIHelper.CreateView(0, DimensionHelper.NComplaintAttachedImageSize);

			_attachedImage1 = new UploadImagePreviewer(DimensionHelper.NComplaintAttachedImageSize,
				DimensionHelper.NComplaintAttachedImageSize) {Hidden = true};
			_attachedImage2 = new UploadImagePreviewer(DimensionHelper.NComplaintAttachedImageSize,
				DimensionHelper.NComplaintAttachedImageSize) {Hidden = true};
			_attachedImage3 = new UploadImagePreviewer(DimensionHelper.NComplaintAttachedImageSize,
				DimensionHelper.NComplaintAttachedImageSize) {Hidden = true};

			_lstIndicators = new UIView[3];

			_lstIndicators[0] = UIHelper.CreateView(0, 0, ColorHelper.Overlay);
			_lstIndicators[1] = UIHelper.CreateView(0, 0, ColorHelper.Overlay);
			_lstIndicators[2] = UIHelper.CreateView(0, 0, ColorHelper.Overlay);

			CreateBorderForView(_lstIndicators[0]);
			CreateBorderForView(_lstIndicators[1]);
			CreateBorderForView(_lstIndicators[2]);

			_lstIndicators[0].Hidden = true;
			_lstIndicators[1].Hidden = true;
			_lstIndicators[2].Hidden = true;

			_lstProgressBars = new UIActivityIndicatorView[3];

			_lstProgressBars[0] = UIHelper.CreateIndicator(DimensionHelper.CommonHeight, DimensionHelper.CommonHeight,
				ColorHelper.White);
			_lstProgressBars[1] = UIHelper.CreateIndicator(DimensionHelper.CommonHeight, DimensionHelper.CommonHeight,
				ColorHelper.White);
			_lstProgressBars[2] = UIHelper.CreateIndicator(DimensionHelper.CommonHeight, DimensionHelper.CommonHeight,
				ColorHelper.White);

			//Location
			_locationContainer = UIHelper.CreateAlphaView(0, DimensionHelper.LocationHeight, 0, ColorHelper.White, true,
				true);

			_tfLocation = UIHelper.CreateTextField(DimensionHelper.SmallTextSize);
			_tfLocation.TextColor = ColorHelper.Black;
			_tfLocation.ShouldBeginEditing += OnShouldTextFieldBeginEditing;
			_tfLocation.ShouldReturn += OnShouldLocationReturn;

			_lbLocationHint = UIHelper.CreateAlphaLabel(ColorHelper.Red, DimensionHelper.SmallTextSize);
			_lbLocationHint.UserInteractionEnabled = false;

			//Complaint content
			_complaintContentContainer = UIHelper.CreateAlphaView(0, 0, 0, ColorHelper.White, true, true);
			_ctvComplaintContent = new CustomTextView();

			_lbNoAttachedText.UserInteractionEnabled = false;
			_lbAttachText.UserInteractionEnabled = false;
		}

		#region Hide Keyboard

		//protected override void OnWillShowKeyboard(NSNotification notification)
		//{
		//    base.OnWillShowKeyboard(notification);

		//    if (_tfLocation.IsEditing)
		//    {
		//        var visible = notification.Name == UIKeyboard.WillShowNotification;
		//        var keyboardFrame = visible
		//            ? UIKeyboard.FrameEndFromNotification(notification)
		//            : UIKeyboard.FrameBeginFromNotification(notification);
		//        IsKeyboardShown = true;
		//        var keyboardHeight = keyboardFrame.Height;
		//        KeyboardHeight = keyboardHeight;
		//        var f = _contentView.Frame;
		//        f.Y -= keyboardHeight/2;
		//        _contentView.Frame = f;
		//    }
		//    else
		//    {
		//        if (IsKeyboardShown)
		//        {
		//            PushViewDown();
		//        }
		//    }
		//}

		//private void PushViewDown()
		//{
		//    IsKeyboardShown = false;
		//    var f = _contentView.Frame;
		//    f.Y += KeyboardHeight/2;
		//    _contentView.Frame = f;
		//}

		//protected override void OnWillHideKeyboard(NSNotification obj)
		//{
		//    base.OnWillHideKeyboard(obj);
		//    if (IsKeyboardShown)
		//    {
		//        PushViewDown();
		//    }

		//}

		#endregion

		private bool OnShouldTextFieldBeginEditing(UITextField textfield)
		{
			LocationCommand?.Execute(null);
			return true;
		}

		private bool OnShouldLocationReturn(UITextField textfield)
		{
			_tfLocation.ResignFirstResponder();
			_videoLinkContainer.UserInteractionEnabled = true;
			_linkPictureContainer.UserInteractionEnabled = true;
			return false;
		}

		private void OnRemoveVideoLinkClicked(object sender, EventArgs e)
		{
			_videoLinkContainer.Hidden = true;
			VideoLinkValue = string.Empty;
			if (AttachmentStreamsValue[0] == null &&
			    AttachmentStreamsValue[1] == null &&
			    AttachmentStreamsValue[2] == null)
				_lbNoAttachedText.Hidden = false;
		}

		private void CreateBorderForView(UIView view)
		{
			view.ClipsToBounds = true;
			view.Layer.CornerRadius = 10;
			view.Layer.BorderColor = UIColor.Gray.CGColor;
			view.Layer.BorderWidth = 1;
			view.Layer.ZPosition = 1;
		}

		private void InitEventHandler()
		{
			_imagePicker = new UIImagePickerController
			{
				SourceType = UIImagePickerControllerSourceType.PhotoLibrary,
				MediaTypes = new string[] {UTType.Image}
			};

			_imagePicker.FinishedPickingMedia += OnFinishedPickingMedia;
			_imagePicker.Canceled += OnCancelPickingImage;
			_attachedImage1.CloseButton.Clicked += OnRemoveAttachImage1;
			_attachedImage2.CloseButton.Clicked += OnRemoveAttachImage2;
			_attachedImage3.CloseButton.Clicked += OnRemoveAttachImage3;
		}

		private void OnCancelPickingImage(object sender, EventArgs e)
		{
			_imagePicker.DismissModalViewController(true);
		}

		private void OnFinishedPickingMedia(object sender, UIImagePickerMediaPickedEventArgs e)
		{
			var imageStreamList = AttachmentStreamsValue;
			Stream pickedImage = null;

			// determine what was selected, video or image
			var isImage = e.Info[UIImagePickerController.MediaType].ToString() == "public.image";

			if (!isImage)
				return;
			// get common info (shared between images and video)
			var referenceUrl = e.Info[new NSString("UIImagePickerControllerReferenceURL")] as NSUrl;

			if (referenceUrl != null)
			{
				var assetsLibrary = new ALAssetsLibrary();
				assetsLibrary.AssetForUrl(referenceUrl,
					delegate(ALAsset asset)
					{
						var representation = asset.DefaultRepresentation;

						if (representation == null)
						{
							Console.WriteLine("Can't get image information");
						}
						else
						{
							// get the original image
							var originalImage = e.Info[UIImagePickerController.OriginalImage] as UIImage;
							if (originalImage != null)
							{
								//resize image before upload
								if (Math.Max(originalImage.Size.Width, originalImage.Size.Height) >
								    ConstVariable.UploadImageMaxSize)
								{
									originalImage = ImageHelper.ResizeImageKeepRatio(originalImage,
										ConstVariable.UploadImageMaxSize);
								}
							}

							if (originalImage != null)
								pickedImage = originalImage.AsJPEG().AsStream(); // display

							var a = 0;
							for (var i = 0; i < imageStreamList.Length; i++)
								if (imageStreamList[i] == null)
								{
									var mStream = new MemoryStream();
									pickedImage?.CopyTo(mStream);
									imageStreamList[i] = mStream;
									a = i;
									break;
								}
							AttachmentStreamsValue = imageStreamList;
							ShowIndicator(_lstIndicators[a]);
							PreviewImage(originalImage);
						}
					},
					delegate(NSError error) { Console.WriteLine("User denied access to photo Library... {0}", error); });
			}
			_imagePicker.DismissModalViewController(true);
		}

		private async void ShowIndicator(UIView indicator)
		{
			indicator.Hidden = false;
			await Task.Delay(ConstVariable.TimeToIndicatorLoadImageShow);
			indicator.Hidden = true;
		}

		private void PreviewImage(UIImage originalImage)
		{
			UploadImagePreviewer previewer = null;
			previewer = _attachedImage1.ImageView.Image == null
				? _attachedImage1
				: (_attachedImage2.ImageView.Image == null
					? _attachedImage2
					: _attachedImage3);

			if (previewer != null)
			{
				previewer.ImageView.Image = originalImage;
				previewer.Hidden = false;
				_lbNoAttachedText.Hidden = true;
			}
		}

		private void OnRemoveAttachImage3(object sender, EventArgs e)
		{
			RemoveAttachedImage(2);
		}

		private void OnRemoveAttachImage2(object sender, EventArgs e)
		{
			RemoveAttachedImage(1);
		}

		private void OnRemoveAttachImage1(object sender, EventArgs e)
		{
			RemoveAttachedImage(0);
		}

		private void RemoveAttachedImage(int index)
		{
			if (index == 2)
			{
				_attachedImages[index].ImageView.Image = null;
				_attachedImages[index].Hidden = true;
				AttachmentStreamsValue[index] = null;
			}

			for (var i = index; i < 2; i++)
			{
				_attachedImages[i].ImageView.Image = _attachedImages[i + 1].ImageView.Image;
				AttachmentStreamsValue[i] = AttachmentStreamsValue[i + 1];
				_attachedImages[i + 1].ImageView.Image = null;
				AttachmentStreamsValue[i + 1] = null;
				_attachedImages[i + 1].Hidden = true;
				_attachedImages[i].Hidden = _attachedImages[i].ImageView.Image == null;
			}

			if (Array.TrueForAll(AttachmentStreamsValue, i => i == null))
				if (_videoLinkContainer.Hidden)
					_lbNoAttachedText.Hidden = false;
		}

		#region Commands

		public ICommand AttachWrongFormatOrSizeErrorCommand { get; set; }
		public ICommand LocationCommand { get; set; }

		#endregion

		#region Properties

		private UIImagePickerController _imagePicker;

		public CGColor LocationBackgroundType
		{
			get { return _locationContainer.Layer.BorderColor; }
			set { _locationContainer.Layer.BorderColor = value; }
		}

		public CGColor ContentBackgroundType
		{
			get { return _complaintContentContainer.Layer.BorderColor; }
			set { _complaintContentContainer.Layer.BorderColor = value; }
		}

		public event EventHandler AttachmentStreamsValueChanged;

		private MemoryStream[] _attachmentStreams;

		public MemoryStream[] AttachmentStreamsValue
		{
			get { return _attachmentStreams; }
			set
			{
				_attachmentStreams = value;
				AttachmentStreamsValueChanged?.Invoke(this, null);
			}
		}

		public event EventHandler VideoLinkValueChanged;

		private string _videoLink;

		public string VideoLinkValue
		{
			get { return _videoLink; }
			set
			{
				_videoLink = value;
				VideoLinkValueChanged?.Invoke(this, null);
			}
		}

		private bool _isImagePickerShown;

		public bool IsImagePickerShown
		{
			get { return _isImagePickerShown; }
			set
			{
				_isImagePickerShown = value;
				if (value)
					NavigationController.PresentModalViewController(_imagePicker, true);
				else
					_imagePicker.DismissModalViewController(true);
			}
		}

		private bool _isVideoLayoutShown;

		public bool IsVideoLayoutShown
		{
			get { return _isVideoLayoutShown; }
			set
			{
				_isVideoLayoutShown = value;
				if (value)
				{
					_videoLinkContainer.Hidden = false;
					_lbNoAttachedText.Hidden = true;
				}
			}
		}

		#endregion

		#region Gradient

		public override void ViewDidAppear(bool animated)
		{
			base.ViewDidAppear(animated);

			var linkVideoGradientLayer = GetGradientLayer(_linkVideoContainer.Bounds);
			_linkVideoContainer.Layer.InsertSublayer(linkVideoGradientLayer, 0);

			var linkPictureGradientLayer = GetGradientLayer(_linkPictureContainer.Bounds);
			_linkPictureContainer.Layer.InsertSublayer(linkPictureGradientLayer, 0);
		}

		private CAGradientLayer GetGradientLayer(CGRect cgRect)
		{
			var gradientLayer = new CAGradientLayer
			{
				Frame = cgRect,
				NeedsDisplayOnBoundsChange = true,
				MasksToBounds = true,
				Colors = new[]
					{ColorHelper.White.CGColor, ColorHelper.CenterButton.CGColor, ColorHelper.EndButton.CGColor}
			};

			return gradientLayer;
		}

		private bool _isKeyboardHidden;

		public bool IsKeyboardHidden
		{
			get { return _isKeyboardHidden; }
			set
			{
				_isKeyboardHidden = value;
				if (value)
					View.EndEditing(true);
			}
		}

		#endregion

		#region UI Elements

		//Bottom Buttons
		private AlphaUIView _sendButtonContainer;
		private AlphaLabel _lbSendText;
		private AlphaImageView _ivSendIcon;
		private AlphaUIButton _cancelButton;

		//Attached image region
		private UIView _attachRegionContainer;
		private AlphaLabel _lbNoAttachedText;
		private UIView _videoLinkContainer;
		private AlphaImageView _ivSmallLinkVideoIcon;
		private AlphaImageView _ivRemoveIcon;
		private AlphaLabel _lbVideoLink;
		private UIView _imageRegion;
		private UploadImagePreviewer _attachedImage1, _attachedImage2, _attachedImage3;
		private UploadImagePreviewer[] _attachedImages;

		private UIView[] _lstIndicators;
		private UIActivityIndicatorView[] _lstProgressBars;

		//Link Video
		private AlphaUIView _linkVideoContainer;
		private AlphaLabel _lbLinkVideoText;
		private AlphaImageView _ivLinkVideoIcon;

		//Link Picture
		private AlphaUIView _linkPictureContainer;
		private AlphaLabel _lbLinkPictureText;
		private AlphaImageView _ivLinkPcitureIcon;

		//Attach text
		private AlphaLabel _lbAttachText;

		//Location
		private AlphaUIView _locationContainer;
		private UITextField _tfLocation;
		private AlphaLabel _lbLocationHint;

		//Complaint Content
		private UIView _complaintContentContainer;
		private CustomTextView _ctvComplaintContent;

		//Content View
		private UIView _contentView;

		public override void WillEnterForeground()
		{
		}

		#endregion
	}
}