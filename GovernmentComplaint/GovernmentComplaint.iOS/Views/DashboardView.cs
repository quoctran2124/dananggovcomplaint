using System;
using GovernmentComplaint.Core.ViewModels;
using GovernmentComplaint.iOS.Controls;
using GovernmentComplaint.iOS.Helpers;
using GovernmentComplaint.iOS.Views.Bases;
using GovernmentComplaint.iOS.Views.TableView.ItemTableViewSource;
using MvvmCross.Binding.BindingContext;
using UIKit;

namespace GovernmentComplaint.iOS.Views
{
    public class DashboardView : DetailView
    {
        public UIScrollView ScrollView { get; private set; }
        private UIView _contentView;

        private UIView _summaryTitleView;
        private UILabel _summaryTitle;
        private AlphaLabel _summaryFilterText;
        private AlphaImageView _summaryFilterIcon;
        private AlphaUIView _summaryFilterView;


        private UIView _summaryContentView;
        private UIView _divider;

        private UIView _summaryLeftContentView;
        private UILabel _feedbackProcessedTitle;
        private UILabel _feedbackTotalTitle;

        private UIView _summaryRightContentView;
        private UILabel _feedbackProcessedValue;
        private UILabel _feedbackTotalValue;

        private UIView _summaryBottomContentView;
        private UILabel _percentFeedbackProcessedTitle;
        private UILabel _percentFeedbackProcessedValue;

        private UIView _categoryTitleView;
        private UILabel _categoryTitle;

        private UITableView _feedbackCategoryTableView;
        private FeedbackCategoryItemTableViewSource _feedbackCategoryItemSource;

        private AlphaUIView _seeMoreCategoryView;
        private AlphaLabel _seeMoreCategoryTitle;

        private UIView _processingUnitTitleView;
        private UILabel _processingUnitTitle;

        private UITableView _processingUnitTableView;
        private ProcessingUnitItemTableViewSource _processingUnitItemSource;

        private AlphaUIView _seeMoreProcessingUnitView;
        private AlphaLabel _seeMoreProcessingUnitTitle;

        private UITableView _timeCategoriesTableView;
        private DropdownTimeCategoriesItemTableViewSource _timeCategoriesItemSource;

        private NSLayoutConstraint _feedbackCategoryHeight;
        private NSLayoutConstraint _processingUnitHeight;

        private int _numberOfCategoryItemList;
        public int NumberOfCategoryItemList
        {
            get => _numberOfCategoryItemList;
            set
            {
                _numberOfCategoryItemList = value;
                GetHeightCategoryList();
            }
        }

        private int _numberOfProcessingUnitItemList;
        public int NumberOfProcessingUnitItemList
        {
            get => _numberOfProcessingUnitItemList;
            set
            {
                _numberOfProcessingUnitItemList = value;
                GetHeightProcessingUnitList();

            }
        }
        protected override void InitView()
        {
            base.InitView();
            CreateScrollView();

            View.AddSubview(ScrollView);
            View.AddGestureRecognizer(new UITapGestureRecognizer(HideKeyboard));

            View.AddConstraints(new[]
            {
                NSLayoutConstraint.Create(ScrollView, NSLayoutAttribute.Left, NSLayoutRelation.Equal, View, NSLayoutAttribute.Left, 1, 0),
                NSLayoutConstraint.Create(ScrollView, NSLayoutAttribute.Right, NSLayoutRelation.Equal, View, NSLayoutAttribute.Right, 1, 0),
                NSLayoutConstraint.Create(ScrollView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, View, NSLayoutAttribute.Top, 1, 0),
                NSLayoutConstraint.Create(ScrollView, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, View, NSLayoutAttribute.Bottom, 1, 0)
            });
        }

        private void CreateScrollView()
        {
            ScrollView = UIHelper.CreateScrollView(0, 0, UIColor.White);
            ScrollView.Scrolled += OnScrollViewScrolled;
            CreateContentView();

            ScrollView.AddSubview(_contentView);
            ScrollView.AddConstraints(new[]
            {
                NSLayoutConstraint.Create(_contentView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, ScrollView, NSLayoutAttribute.Top, 1, 0),
                NSLayoutConstraint.Create(_contentView, NSLayoutAttribute.Left, NSLayoutRelation.Equal, ScrollView, NSLayoutAttribute.Left, 1, 0),
                NSLayoutConstraint.Create(_contentView, NSLayoutAttribute.Width, NSLayoutRelation.Equal, ScrollView, NSLayoutAttribute.Width, 1, 0),
                NSLayoutConstraint.Create(_contentView, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, ScrollView, NSLayoutAttribute.Bottom, 1, 0),
            });
        }

        private void CreateContentView()
        {
            _contentView = UIHelper.CreateView(0, 0, UIColor.White);

            CreateSummaryTitleView();
            CreateSummaryContentView();
            CreateCategoryTitleView();
            CreateFeedbackCategoryListView();
            CreateProcessingUnitTitleView();
            CreateSeeMoreCategoryView();
            CreateProcessingUnitListView();
            CreateSeeMoreProcessingUnitView();
            CreateDropdownTimeListView();

            _feedbackCategoryHeight = NSLayoutConstraint.Create(_feedbackCategoryTableView, NSLayoutAttribute.Height, NSLayoutRelation.Equal,
                null, NSLayoutAttribute.NoAttribute, 1, NumberOfCategoryItemList * DimensionHelper.CategoryItemHeight);
            _processingUnitHeight = NSLayoutConstraint.Create(_processingUnitTableView, NSLayoutAttribute.Height, NSLayoutRelation.Equal,
                null, NSLayoutAttribute.NoAttribute, 1, NumberOfCategoryItemList * DimensionHelper.CategoryItemHeight);

            _contentView.AddSubview(_summaryTitleView);
            _contentView.AddSubview(_summaryContentView);
            _contentView.AddSubview(_categoryTitleView);
            _contentView.Add(_feedbackCategoryTableView);
            _contentView.Add(_seeMoreCategoryView);
            _contentView.Add(_processingUnitTitleView);
            _contentView.Add(_processingUnitTableView);
            _contentView.Add(_seeMoreProcessingUnitView);
            _contentView.Add(_timeCategoriesTableView);
            _contentView.AddConstraints(new[]
            {
                NSLayoutConstraint.Create(_summaryTitleView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _contentView, NSLayoutAttribute.Top, 1, 0),
                NSLayoutConstraint.Create(_summaryTitleView, NSLayoutAttribute.Width, NSLayoutRelation.Equal, _contentView, NSLayoutAttribute.Width, 1, 0),

                NSLayoutConstraint.Create(_timeCategoriesTableView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _summaryTitleView, NSLayoutAttribute.Bottom, 1, DimensionHelper.ThinDivider),
                NSLayoutConstraint.Create(_timeCategoriesTableView, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _contentView, NSLayoutAttribute.Right, 1, 0),
                NSLayoutConstraint.Create(_timeCategoriesTableView, NSLayoutAttribute.Height, NSLayoutRelation.Equal, null, NSLayoutAttribute.NoAttribute, 1, 3 * DimensionHelper.CategoryItemHeight),

                NSLayoutConstraint.Create(_summaryContentView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _summaryTitleView, NSLayoutAttribute.Bottom, 1, 0),
                NSLayoutConstraint.Create(_summaryContentView, NSLayoutAttribute.Width, NSLayoutRelation.Equal, _contentView, NSLayoutAttribute.Width, 1, 0),

                NSLayoutConstraint.Create(_categoryTitleView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _summaryContentView, NSLayoutAttribute.Bottom, 1, 0),
                NSLayoutConstraint.Create(_categoryTitleView, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _contentView, NSLayoutAttribute.Left, 1, 0),
                NSLayoutConstraint.Create(_categoryTitleView, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _contentView, NSLayoutAttribute.Right, 1, 0),

                NSLayoutConstraint.Create(_feedbackCategoryTableView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _categoryTitleView, NSLayoutAttribute.Bottom, 1, 0),
                NSLayoutConstraint.Create(_feedbackCategoryTableView, NSLayoutAttribute.Width, NSLayoutRelation.Equal, _contentView, NSLayoutAttribute.Width, 1, 0),
                _feedbackCategoryHeight,

                NSLayoutConstraint.Create(_seeMoreCategoryView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _feedbackCategoryTableView, NSLayoutAttribute.Bottom, 1, 0),
                NSLayoutConstraint.Create(_seeMoreCategoryView, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _contentView, NSLayoutAttribute.Right, 1, 0),
                NSLayoutConstraint.Create(_seeMoreCategoryView, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _contentView, NSLayoutAttribute.Left, 1, DimensionHelper.LayoutBigMargin),

                NSLayoutConstraint.Create(_processingUnitTitleView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _seeMoreCategoryView, NSLayoutAttribute.Bottom, 1, DimensionHelper.LayoutBiggerMargin),
                NSLayoutConstraint.Create(_processingUnitTitleView, NSLayoutAttribute.Width, NSLayoutRelation.Equal, _contentView, NSLayoutAttribute.Width, 1, 0),

                NSLayoutConstraint.Create(_processingUnitTableView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _processingUnitTitleView, NSLayoutAttribute.Bottom, 1, 0),
                NSLayoutConstraint.Create(_processingUnitTableView, NSLayoutAttribute.Width, NSLayoutRelation.Equal, _contentView, NSLayoutAttribute.Width, 1, 0),
                _processingUnitHeight,

                NSLayoutConstraint.Create(_seeMoreProcessingUnitView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _processingUnitTableView, NSLayoutAttribute.Bottom, 1, 0),
                NSLayoutConstraint.Create(_seeMoreProcessingUnitView, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _contentView, NSLayoutAttribute.Right, 1, 0),
                NSLayoutConstraint.Create(_seeMoreProcessingUnitView, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _contentView, NSLayoutAttribute.Left, 1, DimensionHelper.LayoutBigMargin),

                NSLayoutConstraint.Create(_contentView, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _seeMoreProcessingUnitView, NSLayoutAttribute.Bottom, 1, DimensionHelper.LayoutBigMargin),
            });
        }

        private void CreateSummaryTitleView()
        {
            _summaryTitleView = UIHelper.CreateView(0, DimensionHelper.TitleHeight, ColorHelper.Gray);
            _summaryTitle = UIHelper.CreateLabel(ColorHelper.BoldTextColor, DimensionHelper.MediumTextSize, true);
            CreateSummaryFilterView();

            _summaryTitleView.AddSubview(_summaryTitle);
            _summaryTitleView.AddSubview(_summaryFilterView);
            _summaryTitleView.AddConstraints(new[]
            {
                NSLayoutConstraint.Create(_summaryTitle, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _summaryTitleView, NSLayoutAttribute.Left, 1, DimensionHelper.LayoutMargin),
                NSLayoutConstraint.Create(_summaryTitle, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, _summaryTitleView, NSLayoutAttribute.CenterY, 1, 0),

                NSLayoutConstraint.Create(_summaryFilterView, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _summaryTitleView, NSLayoutAttribute.Right, 1, 0),
                NSLayoutConstraint.Create(_summaryFilterView, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, _summaryTitleView, NSLayoutAttribute.CenterY, 1, 0),
            });
        }

        private void CreateSummaryFilterView()
        {
            _summaryFilterView = UIHelper.CreateAlphaView(0, DimensionHelper.TitleHeight, 0, ColorHelper.Gray);
            _summaryFilterText = UIHelper.CreateAlphaLabel(ColorHelper.Black, DimensionHelper.MediumTextSize);
            _summaryFilterText.UserInteractionEnabled = false;
            _summaryFilterIcon = UIHelper.CreateAlphaImageView(DimensionHelper.DropdownHeight, DimensionHelper.DropdownIconSize, 0);
            _summaryFilterIcon.UserInteractionEnabled = false;

            _summaryFilterView.AddSubview(_summaryFilterText);
            _summaryFilterView.AddSubview(_summaryFilterIcon);
            _summaryFilterView.AddConstraints(new[]
            {
                NSLayoutConstraint.Create(_summaryFilterText, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _summaryFilterView, NSLayoutAttribute.Left, 1, DimensionHelper.LayoutMargin),
                NSLayoutConstraint.Create(_summaryFilterText, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, _summaryFilterView, NSLayoutAttribute.CenterY, 1, 0),

                NSLayoutConstraint.Create(_summaryFilterIcon, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _summaryFilterText, NSLayoutAttribute.Right, 1, DimensionHelper.LayoutMargin),
                NSLayoutConstraint.Create(_summaryFilterIcon, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, _summaryFilterView, NSLayoutAttribute.CenterY, 1, 0),

                NSLayoutConstraint.Create(_summaryFilterView, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _summaryFilterIcon, NSLayoutAttribute.Right, 1, DimensionHelper.LayoutMargin),
            });
        }

        private void CreateSummaryContentView()
        {
            CreateLeftContent();
            CreateDivider();
            CreateRightContent();
            CreateBottomContent();

            _summaryContentView = UIHelper.CreateView(0, 0, UIColor.White);

            _summaryContentView.AddSubview(_summaryLeftContentView);
            _summaryContentView.AddSubview(_summaryRightContentView);
            _summaryContentView.AddSubview(_divider);
            _summaryContentView.AddSubview(_summaryBottomContentView);

            _summaryContentView.AddConstraints(new[]
            {
                NSLayoutConstraint.Create(_summaryLeftContentView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _summaryContentView, NSLayoutAttribute.Top, 1, DimensionHelper.PopupMargin),
                NSLayoutConstraint.Create(_summaryLeftContentView, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _summaryContentView, NSLayoutAttribute.Left, 1, 0),
                NSLayoutConstraint.Create(_summaryLeftContentView, NSLayoutAttribute.Width, NSLayoutRelation.Equal, null, NSLayoutAttribute.NoAttribute, 1, (ResolutionHelper.Width/2) - 0.5f),

                NSLayoutConstraint.Create(_summaryRightContentView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _summaryContentView, NSLayoutAttribute.Top, 1, DimensionHelper.PopupMargin),
                NSLayoutConstraint.Create(_summaryRightContentView, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _summaryContentView, NSLayoutAttribute.Right, 1, 0),
                NSLayoutConstraint.Create(_summaryRightContentView, NSLayoutAttribute.Width, NSLayoutRelation.Equal, null, NSLayoutAttribute.NoAttribute, 1, (ResolutionHelper.Width/2) - 0.5f),

                NSLayoutConstraint.Create(_divider, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _summaryContentView, NSLayoutAttribute.Top, 1, DimensionHelper.PopupMargin),
                NSLayoutConstraint.Create(_divider, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _summaryContentView, NSLayoutAttribute.CenterX, 1, 0),
                NSLayoutConstraint.Create(_divider, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _summaryLeftContentView, NSLayoutAttribute.Bottom, 1, 0),

                NSLayoutConstraint.Create(_summaryBottomContentView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _summaryLeftContentView, NSLayoutAttribute.Bottom, 1, DimensionHelper.PopupMargin),
                NSLayoutConstraint.Create(_summaryBottomContentView, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _summaryContentView, NSLayoutAttribute.CenterX, 1, 0),

                NSLayoutConstraint.Create(_summaryContentView, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _summaryBottomContentView, NSLayoutAttribute.Bottom, 1, DimensionHelper.LayoutBigMargin),
            });
        }

        private void CreateDivider()
        {
            _divider = UIHelper.CreateView(DimensionHelper.ThinDivider, 0, ColorHelper.DarkestGray);
        }

        private void CreateLeftContent()
        {
            _summaryLeftContentView = UIHelper.CreateView(0, 0, UIColor.White);

            _feedbackProcessedTitle = UIHelper.CreateLabel(ColorHelper.Black, DimensionHelper.MediumTextSize);
            _feedbackProcessedValue = UIHelper.CreateLabel(ColorHelper.OrangeTextColor, DimensionHelper.LargeTextSize, true);

            _summaryLeftContentView.AddSubview(_feedbackProcessedTitle);
            _summaryLeftContentView.AddSubview(_feedbackProcessedValue);
            _summaryLeftContentView.AddConstraints(new[]
            {
                NSLayoutConstraint.Create(_feedbackProcessedTitle, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _summaryLeftContentView, NSLayoutAttribute.Top, 1, 0),
                NSLayoutConstraint.Create(_feedbackProcessedTitle, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _summaryLeftContentView, NSLayoutAttribute.CenterX, 1, 0),

                NSLayoutConstraint.Create(_feedbackProcessedValue, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _feedbackProcessedTitle, NSLayoutAttribute.Bottom, 1, DimensionHelper.LayoutBigMargin),
                NSLayoutConstraint.Create(_feedbackProcessedValue, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _summaryLeftContentView, NSLayoutAttribute.CenterX, 1, 0),

                NSLayoutConstraint.Create(_summaryLeftContentView, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _feedbackProcessedValue, NSLayoutAttribute.Bottom, 1, 0),
            });
        }

        private void CreateRightContent()
        {
            _summaryRightContentView = UIHelper.CreateView(0, 0, UIColor.White);

            _feedbackTotalTitle = UIHelper.CreateLabel(ColorHelper.Black, DimensionHelper.MediumTextSize);
            _feedbackTotalValue = UIHelper.CreateLabel(ColorHelper.BlueTextColor, DimensionHelper.LargeTextSize, true);

            _summaryRightContentView.AddSubview(_feedbackTotalTitle);
            _summaryRightContentView.AddSubview(_feedbackTotalValue);
            _summaryRightContentView.AddConstraints(new[]
            {
                NSLayoutConstraint.Create(_feedbackTotalTitle, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _summaryRightContentView, NSLayoutAttribute.Top, 1, 0),
                NSLayoutConstraint.Create(_feedbackTotalTitle, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _summaryRightContentView, NSLayoutAttribute.CenterX, 1, 0),

                NSLayoutConstraint.Create(_feedbackTotalValue, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _feedbackTotalTitle, NSLayoutAttribute.Bottom, 1, DimensionHelper.LayoutBigMargin),
                NSLayoutConstraint.Create(_feedbackTotalValue, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _summaryRightContentView, NSLayoutAttribute.CenterX, 1, 0),

                NSLayoutConstraint.Create(_summaryRightContentView, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _feedbackTotalValue, NSLayoutAttribute.Bottom, 1, 0),
            });
        }

        private void CreateBottomContent()
        {
            _summaryBottomContentView = UIHelper.CreateView(0, 0, UIColor.White);
            _percentFeedbackProcessedTitle = UIHelper.CreateLabel(ColorHelper.Black, DimensionHelper.MediumTextSize);
            _percentFeedbackProcessedValue = UIHelper.CreateLabel(ColorHelper.BolderTextColor, DimensionHelper.MediumTextSize, true);

            _summaryBottomContentView.AddSubview(_percentFeedbackProcessedValue);
            _summaryBottomContentView.AddSubview(_percentFeedbackProcessedTitle);
            _summaryBottomContentView.AddConstraints(new[]
            {
                NSLayoutConstraint.Create(_percentFeedbackProcessedValue, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _summaryBottomContentView, NSLayoutAttribute.Top, 1, 0),
                NSLayoutConstraint.Create(_percentFeedbackProcessedValue, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _summaryBottomContentView, NSLayoutAttribute.Left, 1, 0),

                NSLayoutConstraint.Create(_percentFeedbackProcessedTitle, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _summaryBottomContentView, NSLayoutAttribute.Top, 1, 0),
                NSLayoutConstraint.Create(_percentFeedbackProcessedTitle, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _percentFeedbackProcessedValue, NSLayoutAttribute.Right, 1, 0),

                NSLayoutConstraint.Create(_summaryBottomContentView, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _percentFeedbackProcessedValue, NSLayoutAttribute.Bottom, 1, 0),
                NSLayoutConstraint.Create(_summaryBottomContentView, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _percentFeedbackProcessedTitle, NSLayoutAttribute.Right, 1, 0),
            });
        }

        private void CreateCategoryTitleView()
        {
            _categoryTitleView = UIHelper.CreateView(0, DimensionHelper.TitleHeight, ColorHelper.Gray);
            _categoryTitle = UIHelper.CreateLabel(ColorHelper.BoldTextColor, DimensionHelper.MediumTextSize, true);

            _categoryTitleView.AddSubview(_categoryTitle);
            _categoryTitleView.AddConstraints(new[]
            {
                NSLayoutConstraint.Create(_categoryTitle, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _categoryTitleView, NSLayoutAttribute.Left, 1, DimensionHelper.LayoutMargin),
                NSLayoutConstraint.Create(_categoryTitle, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, _categoryTitleView, NSLayoutAttribute.CenterY, 1, 0),
            });
        }

        private void CreateFeedbackCategoryListView()
        {
            _feedbackCategoryTableView = UIHelper.CreateTableView(0, 0, ColorHelper.White);
            _feedbackCategoryItemSource = new FeedbackCategoryItemTableViewSource(_feedbackCategoryTableView);
            _feedbackCategoryTableView.Source = _feedbackCategoryItemSource;
        }

        private void CreateSeeMoreCategoryView()
        {
            _seeMoreCategoryView = UIHelper.CreateAlphaView(0, DimensionHelper.TitleHeight, 0, ColorHelper.White);
            _seeMoreCategoryTitle = UIHelper.CreateAlphaLabel(ColorHelper.BlueLighterTextColor, DimensionHelper.MediumTextSize);
            _seeMoreCategoryTitle.UserInteractionEnabled = false;

            _seeMoreCategoryView.AddSubview(_seeMoreCategoryTitle);
            _seeMoreCategoryView.AddConstraints(new[]
            {
                NSLayoutConstraint.Create(_seeMoreCategoryTitle, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _seeMoreCategoryView, NSLayoutAttribute.Left, 1, 0),
                NSLayoutConstraint.Create(_seeMoreCategoryTitle, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, _seeMoreCategoryView, NSLayoutAttribute.CenterY, 1, 0),
            });
        }

        private void GetHeightCategoryList()
        {
            _feedbackCategoryHeight.Constant = NumberOfCategoryItemList * DimensionHelper.CategoryItemHeight;
        }

        private void GetHeightProcessingUnitList()
        {
            _processingUnitHeight.Constant = NumberOfProcessingUnitItemList * DimensionHelper.CategoryItemHeight;
        }

        private void CreateProcessingUnitTitleView()
        {
            _processingUnitTitleView = UIHelper.CreateView(0, DimensionHelper.TitleHeight, ColorHelper.Gray);

            _processingUnitTitle = UIHelper.CreateLabel(ColorHelper.BoldTextColor, DimensionHelper.MediumTextSize, true);

            _processingUnitTitleView.AddSubview(_processingUnitTitle);
            _processingUnitTitleView.AddConstraints(new[]
            {
                NSLayoutConstraint.Create(_processingUnitTitle, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _processingUnitTitleView, NSLayoutAttribute.Left, 1, DimensionHelper.LayoutMargin),
                NSLayoutConstraint.Create(_processingUnitTitle, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, _processingUnitTitleView, NSLayoutAttribute.CenterY, 1, 0),
            });
        }

        private void CreateProcessingUnitListView()
        {
            _processingUnitTableView = UIHelper.CreateTableView(0, 0, ColorHelper.White);
            _processingUnitItemSource = new ProcessingUnitItemTableViewSource(_processingUnitTableView);
            _processingUnitTableView.Source = _processingUnitItemSource;
        }

        private void CreateSeeMoreProcessingUnitView()
        {
            _seeMoreProcessingUnitView = UIHelper.CreateAlphaView(0, DimensionHelper.TitleHeight, 0, ColorHelper.White);
            _seeMoreProcessingUnitTitle = UIHelper.CreateAlphaLabel(ColorHelper.BlueLighterTextColor, DimensionHelper.MediumTextSize);
            _seeMoreProcessingUnitTitle.UserInteractionEnabled = false;

            _seeMoreProcessingUnitView.AddSubview(_seeMoreProcessingUnitTitle);
            _seeMoreProcessingUnitView.AddConstraints(new[]
            {
                NSLayoutConstraint.Create(_seeMoreProcessingUnitTitle, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _seeMoreProcessingUnitView, NSLayoutAttribute.Left, 1, 0),
                NSLayoutConstraint.Create(_seeMoreProcessingUnitTitle, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, _seeMoreProcessingUnitView, NSLayoutAttribute.CenterY, 1, 0),
            });
        }

        private void CreateDropdownTimeListView()
        {
            _timeCategoriesTableView = UIHelper.CreateTableView(DimensionHelper.TimeListWidth, 0, ColorHelper.White);
            _timeCategoriesItemSource = new DropdownTimeCategoriesItemTableViewSource(_timeCategoriesTableView);
            _timeCategoriesTableView.Source = _timeCategoriesItemSource;
        }
        protected override void CreateBinding()
        {
            base.CreateBinding();

            var bindingSet = this.CreateBindingSet<DashboardView, DashboardViewModel>();

            bindingSet.Bind(_summaryTitle).To(vm => vm.Summary);
            bindingSet.Bind(_summaryFilterIcon)
                .For(v => v.Image)
                .To(vm => vm.TimeSelectionIconName)
                .WithConversion("IconNameToUIImage");

            bindingSet.Bind(_summaryFilterText).To(vm => vm.TimeSelectionText);
            bindingSet.Bind(_summaryFilterView).For(v => v.ClickCommand).To(vm => vm.SelectTimeCommand);

            bindingSet.Bind(_feedbackProcessedTitle).To(vm => vm.FeedbackHaveBeenProcessed);
            bindingSet.Bind(_feedbackProcessedValue).To(vm => vm.Number);

            bindingSet.Bind(_feedbackTotalTitle).To(vm => vm.TotalFeedback);
            bindingSet.Bind(_feedbackTotalValue).To(vm => vm.Number);
            bindingSet.Bind(_percentFeedbackProcessedValue).To(vm => vm.SummaryPercentValue);
            bindingSet.Bind(_percentFeedbackProcessedTitle).To(vm => vm.FeedbackProcessedPercent);

            bindingSet.Bind(_categoryTitle).To(vm => vm.FeedbackCategories);

            //Category List
            bindingSet.Bind(_feedbackCategoryItemSource).To(vm => vm.FeedbackCategoryItemViewModels);
            bindingSet.Bind(this).For(v => v.NumberOfCategoryItemList).To(vm => vm.NumberOfCategoryItemList);

            bindingSet.Bind(_seeMoreCategoryTitle).To(vm => vm.SeeMoreText);
            bindingSet.Bind(_seeMoreCategoryView).For(v => v.ClickCommand).To(vm => vm.SelectSeeMoreCommand);

            //Processing Unit
            bindingSet.Bind(_processingUnitTitle).To(vm => vm.ProcessingUnitTitle);

            bindingSet.Bind(_processingUnitItemSource).To(vm => vm.ProcessingUnitItemViewModels);
            bindingSet.Bind(this).For(v => v.NumberOfProcessingUnitItemList).To(vm => vm.NumberOfProcessingUnitItemList);

            bindingSet.Bind(_seeMoreProcessingUnitTitle).To(vm => vm.SeeMoreUnitText);
            bindingSet.Bind(_seeMoreProcessingUnitView).For(v => v.ClickCommand).To(vm => vm.SelectSeeMoreProcessingUnitCommand);

            bindingSet.Bind(_timeCategoriesItemSource).To(vm => vm.TimeSelectionItemViewModels);
            bindingSet.Bind(_timeCategoriesTableView).For(v => v.Hidden).To(vm => vm.IsTimeSelectionShown).WithConversion("InvertBool");

            bindingSet.Apply();
        }

        private void OnScrollViewScrolled(object sender, EventArgs e)
        {
            HideKeyboard();
        }

        public override void WillEnterForeground()
        {
        }
    }
}