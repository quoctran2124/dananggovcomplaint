using CoreAnimation;
using CoreGraphics;
using GovernmentComplaint.Core.ViewModels.Bases;
using GovernmentComplaint.iOS.Controls;
using GovernmentComplaint.iOS.Helpers;
using GovernmentComplaint.iOS.Views.Bases;
using MvvmCross.Binding.BindingContext;
using UIKit;

namespace GovernmentComplaint.iOS.Views
{
	public class SearchboxLayout : DisappearableUIView
	{
		public SearchboxLayout(MvxFluentBindingDescriptionSet<MasterView, MasterViewModel> bindingSet)
		{
			InitView();
			CreateBinding(bindingSet);
		}

		private void InitView()
		{
			TranslatesAutoresizingMaskIntoConstraints = false;
			WidthConstraint = NSLayoutConstraint.Create(this, NSLayoutAttribute.Width, NSLayoutRelation.Equal, this,
				NSLayoutAttribute.Width, 1, 0);
			BackgroundColor = ColorHelper.NeutralGray;

			_btnSearchButton = UIHelper.CreateButton(DimensionHelper.SearchButtonWidth,
				DimensionHelper.SearchButtonHeight, UIColor.White, DimensionHelper.BigTextSize,
				ColorHelper.TrueBlue, false, true, true);
			_btnCancelButton = UIHelper.CreateButton(DimensionHelper.SearchButtonWidth,
				DimensionHelper.SearchButtonHeight, UIColor.LightGray, DimensionHelper.BigTextSize,
				ColorHelper.BorderGray, false, true, true);

			_tfSearchTextField = UIHelper.CreateTextField(DimensionHelper.MediumTextSize, FontType.Regular, UIColor.White,
				true, true);

			var paddingView1 = new UIView(new CGRect(0, 0, 5, 20));
			_tfSearchTextField.TextAlignment = UITextAlignment.Left;
			_tfSearchTextField.LeftView = paddingView1;
			_tfSearchTextField.LeftViewMode = UITextFieldViewMode.Always;

			_searchTextFieldContainer = UIHelper.CreateView(0, 0, ColorHelper.White, true);
			_searchTextFieldContainer.Add(_tfSearchTextField);
			_searchTextFieldContainer.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_tfSearchTextField, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_searchTextFieldContainer, NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_tfSearchTextField, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal,
					_searchTextFieldContainer, NSLayoutAttribute.Bottom, 1, 0),
				NSLayoutConstraint.Create(_tfSearchTextField, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					_searchTextFieldContainer, NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_tfSearchTextField, NSLayoutAttribute.Right, NSLayoutRelation.Equal,
					_searchTextFieldContainer, NSLayoutAttribute.Right, 1, 0)
			});

			CreateDropdownList();
			Add(_btnSearchButton);
			Add(_searchTextFieldContainer);
			Add(_btnCancelButton);
			Add(DropdownContainer);

			AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_searchTextFieldContainer, NSLayoutAttribute.Top, NSLayoutRelation.Equal, this,
					NSLayoutAttribute.Top, 1, DimensionHelper.SearchBoxEditTextMargin),
				NSLayoutConstraint.Create(_searchTextFieldContainer, NSLayoutAttribute.Height, NSLayoutRelation.Equal,
					null,
					NSLayoutAttribute.NoAttribute, 1, DimensionHelper.SearchBoxEditTextHeight),
				NSLayoutConstraint.Create(_searchTextFieldContainer, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					this,
					NSLayoutAttribute.Left, 1, DimensionHelper.SearchBoxEditTextMargin),
				NSLayoutConstraint.Create(_searchTextFieldContainer, NSLayoutAttribute.Right, NSLayoutRelation.Equal,
					this,
					NSLayoutAttribute.Right, 1, -DimensionHelper.SearchBoxEditTextMargin),

				NSLayoutConstraint.Create(DropdownContainer, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_searchTextFieldContainer,
					NSLayoutAttribute.Bottom, 1, DimensionHelper.SearchBoxEditTextMargin),
				NSLayoutConstraint.Create(DropdownContainer, NSLayoutAttribute.Height, NSLayoutRelation.Equal, null,
					NSLayoutAttribute.NoAttribute, 1, DimensionHelper.SearchBoxEditTextHeight),
				NSLayoutConstraint.Create(DropdownContainer, NSLayoutAttribute.Left, NSLayoutRelation.Equal, this,
					NSLayoutAttribute.Left, 1, DimensionHelper.SearchBoxEditTextMargin),
				NSLayoutConstraint.Create(DropdownContainer, NSLayoutAttribute.Right, NSLayoutRelation.Equal, this,
					NSLayoutAttribute.Right, 1, -DimensionHelper.SearchBoxEditTextMargin),

				NSLayoutConstraint.Create(_btnSearchButton, NSLayoutAttribute.Right, NSLayoutRelation.Equal, this,
					NSLayoutAttribute.Right, 1, -DimensionHelper.SearchButtonMargin),
				NSLayoutConstraint.Create(_btnSearchButton, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, this,
					NSLayoutAttribute.Bottom, 1, -DimensionHelper.SearchButtonMargin),
				NSLayoutConstraint.Create(_btnCancelButton, NSLayoutAttribute.Right, NSLayoutRelation.Equal,
					_btnSearchButton,
					NSLayoutAttribute.Left, 1, -DimensionHelper.SearchBoxEditTextMargin),
				NSLayoutConstraint.Create(_btnCancelButton, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, this,
					NSLayoutAttribute.Bottom, 1, -DimensionHelper.SearchButtonMargin)
			});
		}
		
		private void CreateDropdownList()
		{
			DropdownContainer = UIHelper.CreateAlphaView(0, DimensionHelper.SearchBoxEditTextHeight, 0,
				ColorHelper.LighterGray);
			_lbSelectDepartment = UIHelper.CreateAlphaLabel(UIColor.Black, DimensionHelper.MediumTextSize);
			_lbSelectDepartment.Enabled = true;
			DropdownContainer.Add(_lbSelectDepartment);

			DropdownContainer.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_lbSelectDepartment, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					DropdownContainer,
					NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_lbSelectDepartment, NSLayoutAttribute.Height, NSLayoutRelation.Equal,
					DropdownContainer,
					NSLayoutAttribute.Height, 1, 0),
				NSLayoutConstraint.Create(_lbSelectDepartment, NSLayoutAttribute.Right, NSLayoutRelation.Equal,
					DropdownContainer,
					NSLayoutAttribute.Right, 1, 0),
				NSLayoutConstraint.Create(_lbSelectDepartment, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					DropdownContainer,
					NSLayoutAttribute.Left, 1, 5)
			});

			DropdownContainer.Layer.CornerRadius = DimensionHelper.ToastCorner;
			DropdownContainer.Clicked += (sender, args) => this.EndEditing(true);
			_lbSelectDepartment.UserInteractionEnabled = false;

		}

		private void CreateBinding(MvxFluentBindingDescriptionSet<MasterView, MasterViewModel> bindingSet)
		{
			bindingSet.Bind(_tfSearchTextField).For(v => v.Placeholder).To(vm => vm.SearchTextHint);
			bindingSet.Bind(_tfSearchTextField).For(v => v.Text).To(vm => vm.SearchText);
			bindingSet.Bind(_lbSelectDepartment).For(v => v.Text).To(vm => vm.SelectedDepartment);
			bindingSet.Bind(_btnSearchButton).For("Title").To(vm => vm.SearchButtonText);
			bindingSet.Bind(_btnSearchButton).For(v => v.ClickCommand).To(vm => vm.SearchButtonClickCommand);
			bindingSet.Bind(_btnCancelButton).For("Title").To(vm => vm.SearchCancelButtonText);
			bindingSet.Bind(_btnCancelButton).For(v => v.ClickCommand).To(vm => vm.CancelSearchButtonClickCommand);
			bindingSet.Bind(DropdownContainer).For(v => v.ClickCommand).To(vm => vm.OnDepartmentDropdownCommand);
			bindingSet.Apply();
		}
		
		#region UI Elements
		
		private UITextField _tfSearchTextField;
		private UIView _searchTextFieldContainer;
		private AlphaLabel _lbSelectDepartment;
		private AlphaUIButton _btnSearchButton;
		private AlphaUIButton _btnCancelButton;
		public AlphaUIView DropdownContainer;

		#endregion
	}
}