using System.Windows.Input;
using CoreAnimation;
using CoreGraphics;
using Foundation;
using GovernmentComplaint.Core.ViewModels;
using GovernmentComplaint.iOS.Controls;
using GovernmentComplaint.iOS.Helpers;
using GovernmentComplaint.iOS.Views.Bases;
using GovernmentComplaint.iOS.Views.TableView.ItemTableViewSource;
using GovernmentComplaint.iOS.Views.TableView.ItemTableViewSources;
using MvvmCross.Binding.BindingContext;
using UIKit;

namespace GovernmentComplaint.iOS.Views
{
	public class HistoryView : DetailView
	{
		public override void WillEnterForeground()
		{
			CheckNetworkCommand?.Execute(null);
		}

		protected override void InitView()
		{
			CreateDropdownBar();

			_sortingTableView = UIHelper.CreateTableView(0, 0, ColorHelper.Overlay);
			_sortingItemSource = new HistorySortingItemTableViewSource(_sortingTableView);
			_sortingTableView.Source = _sortingItemSource;

			//Overlay
			SortingOverlay = UIHelper.CreateAlphaView(0, 0, 0, ColorHelper.Overlay);
			//Feedback
			_feedbackHistoryTableView = UIHelper.CreateTableView(0, 0, null);
			_feedbackHistoryTableView.ClipsToBounds = true;
			_feedbackHistoryItemSource = new FeedbackItemTableViewSource(_feedbackHistoryTableView);
			_feedbackHistoryTableView.Source = _feedbackHistoryItemSource;
			_feedbackHistoryTableView.Bounces = true;
			_feedbackHistoryTableView.AlwaysBounceVertical = true;
			_lbNoData = UIHelper.CreateLabel(UIColor.Gray, DimensionHelper.MediumTextSize, false, UITextAlignment.Center,
				2);

			_refreshControl = new MvxUIRefreshControl();
			_feedbackHistoryTableView.AddSubview(_refreshControl);
			_feedbackHistoryTableView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_refreshControl, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal,
					_feedbackHistoryTableView, NSLayoutAttribute.Top, 1, -30),
				NSLayoutConstraint.Create(_refreshControl, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal,
					_feedbackHistoryTableView, NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_refreshControl, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal,
					_feedbackHistoryTableView, NSLayoutAttribute.CenterY, 1, 0)
			});
			View.Add(_lbNoData);
			View.AddSubview(DropdownBar);
			View.Add(_feedbackHistoryTableView);
			View.AddSubview(SortingOverlay);
			View.Add(_sortingTableView);
			View.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_lbNoData, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_lbNoData, NSLayoutAttribute.Top, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Top, 1, DimensionHelper.NoDataLabelMarginTop),

				//Dropdown bar
				NSLayoutConstraint.Create(DropdownBar, NSLayoutAttribute.Top, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(DropdownBar, NSLayoutAttribute.Left, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(DropdownBar, NSLayoutAttribute.Width, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Width, 1, 0),

				//Sorting table view
				NSLayoutConstraint.Create(_sortingTableView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, DropdownBar,
					NSLayoutAttribute.Bottom, 1, 0),
				NSLayoutConstraint.Create(_sortingTableView, NSLayoutAttribute.Width, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Width, 1, 0),
				NSLayoutConstraint.Create(_sortingTableView, NSLayoutAttribute.Height, NSLayoutRelation.Equal, null,
					NSLayoutAttribute.NoAttribute, 1, 3 * DimensionHelper.DropdownItemHeight),

				//Sorting overlay
				NSLayoutConstraint.Create(SortingOverlay, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_sortingTableView, NSLayoutAttribute.Bottom, 1, 0),
				NSLayoutConstraint.Create(SortingOverlay, NSLayoutAttribute.Left, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(SortingOverlay, NSLayoutAttribute.Width, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Width, 1, 0),
				NSLayoutConstraint.Create(SortingOverlay, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Bottom, 1, 0),

				//Feedback List
				NSLayoutConstraint.Create(_feedbackHistoryTableView, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					DropdownBar, NSLayoutAttribute.Bottom, 1, 0),
				NSLayoutConstraint.Create(_feedbackHistoryTableView, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					View, NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_feedbackHistoryTableView, NSLayoutAttribute.Right, NSLayoutRelation.Equal,
					View, NSLayoutAttribute.Right, 1, 0),
				NSLayoutConstraint.Create(_feedbackHistoryTableView, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal,
					View, NSLayoutAttribute.Bottom, 1, 0)
			});
			base.InitView();
		}

		protected override void CreateBinding()
		{
			base.CreateBinding();

			var set = this.CreateBindingSet<HistoryView, HistoryViewModel>();

			//Sorting
			set.Bind(_ivSortingIconName)
				.For(v => v.Image)
				.To(vm => vm.SortingIconName)
				.WithConversion("IconNameToUIImage");
			set.Bind(_ivSortingIconName).For(v => v.ClickCommand).To(vm => vm.SortingCommand);

			set.Bind(_lbSortingText).To(vm => vm.Sorting);
			set.Bind(_lbSortingText).For(v => v.ClickCommand).To(vm => vm.SortingCommand);
			set.Bind(_lbSortingText).For(v => v.TextColor).To(vm => vm.IsSortingShown).WithConversion("BoolToColor");

			//Dropdown List
			set.Bind(_sortingItemSource).To(vm => vm.HistorySortingItemViewModels);
			set.Bind(_sortingTableView).For(v => v.Hidden).To(vm => vm.IsSortingShown).WithConversion("InvertBool");

			//Overlay
			set.Bind(SortingOverlay).For(v => v.Hidden).To(vm => vm.IsSortingShown).WithConversion("InvertBool");
			set.Bind(SortingOverlay).For(v => v.ClickCommand).To(vm => vm.SortingOverlayCommand);

			//Feedback
			set.Bind(_feedbackHistoryItemSource).To(vm => vm.FeedbackHistoryItemViewModels);
			set.Bind(_feedbackHistoryItemSource).For(v => v.LoadMoreData).To(vm => vm.LoadMoreData);
			set.Bind(_lbNoData).To(vm => vm.NoData);
			set.Bind(_lbNoData).For(v => v.Hidden).To(vm => vm.IsNoDataTextShown).WithConversion("InvertBool");
			set.Bind(_refreshControl)
				.For(x => x.IsRefreshing)
				.To(vm => vm.IsRefreshing);
			set.Bind(_refreshControl)
				.For(x => x.RefreshCommand)
				.To(vm => vm.RefreshCommand);
			set.Bind(this).For(v => v.MoveListToTop).To(vm => vm.MoveListToTop);
			set.Bind(this).For(v => v.CheckNetworkCommand).To(vm => vm.CheckNetworkCommand);
			set.Bind(this).For(v => v.LastComplaintItemPosition).To(vm => vm.LastComplaintItemPosition);
			set.Apply();
		}

		private void CreateDropdownBar()
		{
			ResolutionHelper.InitStaticVariable();

			DropdownBar = UIHelper.CreateView(0, DimensionHelper.DropdownBarHeight, ColorHelper.LighterGray);

			_lbSortingText = UIHelper.CreateAlphaLabel(ColorHelper.LightGray, DimensionHelper.BigTextSize);
			_ivSortingIconName = UIHelper.CreateAlphaImageView(DimensionHelper.DropdownIconSize,
				DimensionHelper.DropdownIconSize, DimensionHelper.DropdownPadding);

			DropdownBar.Add(_ivSortingIconName);
			DropdownBar.Add(_lbSortingText);

			DropdownBar.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_ivSortingIconName, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal,
					DropdownBar, NSLayoutAttribute.CenterY, 1, 0),
				NSLayoutConstraint.Create(_ivSortingIconName, NSLayoutAttribute.Right, NSLayoutRelation.Equal,
					DropdownBar, NSLayoutAttribute.Right, 1, 0),
				NSLayoutConstraint.Create(_lbSortingText, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, DropdownBar,
					NSLayoutAttribute.CenterY, 1, 0),
				NSLayoutConstraint.Create(_lbSortingText, NSLayoutAttribute.Right, NSLayoutRelation.Equal,
					_ivSortingIconName, NSLayoutAttribute.Left, 1, 0)
			});
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			CheckNetworkCommand?.Execute(null);
			if (LastComplaintItemPosition != 0)
			{
				_feedbackHistoryTableView.ScrollToRow(NSIndexPath.FromRowSection(LastComplaintItemPosition, 0),
					UITableViewScrollPosition.Top, true);
			}
		}

		public override void ViewDidAppear(bool animated)
		{
			base.ViewDidAppear(animated);

			var linkVideoGradientLayer = GetGradientLayer(DropdownBar.Bounds);
			DropdownBar.Layer.InsertSublayer(linkVideoGradientLayer, 0);
		}

		private CAGradientLayer GetGradientLayer(CGRect cgRect)
		{
			var gradientLayer = new CAGradientLayer
			{
				Frame = cgRect,
				NeedsDisplayOnBoundsChange = true,
				MasksToBounds = true,
				Colors = new[]
				{
					ColorHelper.CenterButton.CGColor, ColorHelper.EndButton.CGColor, ColorHelper.DropdownBarEnd.CGColor
				}
			};

			return gradientLayer;
		}

		private bool _moveListToTop;

		public bool MoveListToTop
		{
			get { return _moveListToTop; }
			set
			{
				if (value)
				{
					_feedbackHistoryTableView.ContentOffset = new CGPoint(0,
						0 - _feedbackHistoryTableView.ContentInset.Top);
				}
			}
		}

		#region UI Elements

		public ICommand CheckNetworkCommand { get; set; }

		public UIView DropdownBar { get; private set; }

		//Dropdown Bar Items

		private AlphaImageView _ivSortingIconName;
		private AlphaLabel _lbSortingText;

		//List View
		private UITableView _sortingTableView;
		private HistorySortingItemTableViewSource _sortingItemSource;

		//Overlay

		public AlphaUIView SortingOverlay { get; private set; }

		//Feedback List
		private MvxUIRefreshControl _refreshControl;
		public int LastComplaintItemPosition { get; set; }
		private UITableView _feedbackHistoryTableView;
		private FeedbackItemTableViewSource _feedbackHistoryItemSource;
		private UILabel _lbNoData;

		#endregion
	}
}