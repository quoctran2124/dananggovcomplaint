using System;
using Foundation;
using GovernmentComplaint.Core.ViewModels;
using GovernmentComplaint.iOS.Controls;
using GovernmentComplaint.iOS.Helpers;
using GovernmentComplaint.iOS.Views.Bases;
using MvvmCross.Binding.BindingContext;
using UIKit;

namespace GovernmentComplaint.iOS.Views
{
	public class AboutView : DetailView
	{
		private AlphaImageView _ivHotLineBanner;
		private AlphaImageView _ivDttLogo;
		private AlphaImageView _ivSiouxLogo;
		private AlphaImageView _ivGGLogo;
		private AlphaImageView _ivDanangLogo;
		private UILabel _lbEighthIntro;
		private UILabel _lbFifthIntro;

		private UILabel _lbForthIntro;
		private UILabel _lbSeventhIntro;
		private UILabel _lbSixthIntro;
		private UILabel _lbThirdIntro;

		private UIView _logoLayout, _danangLayout;
		public UIView LowerView;
		private UIScrollView _scrollView;
		public UIView HotLineBanner;
		private UIView _divider;


		protected override void InitView()
		{
			CreateUpperView();
			CreateLowerView();

			_scrollView = UIHelper.CreateScrollView(0, 0, UIColor.White);

			_scrollView.Add(HotLineBanner);
			_scrollView.Add(LowerView);
			_scrollView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(HotLineBanner, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal,
					_scrollView, NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(HotLineBanner, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					_scrollView, NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(HotLineBanner, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_scrollView, NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(HotLineBanner, NSLayoutAttribute.Width, NSLayoutRelation.Equal,
					_scrollView, NSLayoutAttribute.Width, 1, 0),

				NSLayoutConstraint.Create(LowerView, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					_scrollView, NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(LowerView, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal,
					_scrollView, NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(LowerView, NSLayoutAttribute.Right, NSLayoutRelation.Equal,
					_scrollView, NSLayoutAttribute.Right, 1, 0),
				NSLayoutConstraint.Create(LowerView, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					HotLineBanner, NSLayoutAttribute.Bottom, 1, 0),
				NSLayoutConstraint.Create(_scrollView, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal,
					LowerView, NSLayoutAttribute.Bottom, 1, 0)
			});

			View.Add(_scrollView);
			View.AddConstraints(new []
			{
				NSLayoutConstraint.Create(_scrollView, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					View, NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_scrollView, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					View, NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_scrollView, NSLayoutAttribute.Right, NSLayoutRelation.Equal,
					View, NSLayoutAttribute.Right, 1, 0),
				NSLayoutConstraint.Create(_scrollView, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal,
					View, NSLayoutAttribute.Bottom, 1, 0),

			});
			base.InitView();
		}

		private void CreateUpperView()
		{
			ResolutionHelper.InitStaticVariable();
			HotLineBanner = UIHelper.CreateView(0, DimensionHelper.HotLineBannerHeight, ColorHelper.BannerHotLineColor);
			HotLineBanner.UserInteractionEnabled = false;

			_ivHotLineBanner = UIHelper.CreateAlphaImageView(0, 0, DimensionHelper.AboutMarginTB);
			_ivHotLineBanner.Image = UIImage.FromFile(ImageHelper.HotLineBanner);

			HotLineBanner.Add(_ivHotLineBanner);

			HotLineBanner.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_ivHotLineBanner, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal,
					HotLineBanner, NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_ivHotLineBanner, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					HotLineBanner, NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_ivHotLineBanner, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal,
					HotLineBanner, NSLayoutAttribute.Bottom, 1, 0),
				NSLayoutConstraint.Create(_ivHotLineBanner, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					HotLineBanner, NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_ivHotLineBanner, NSLayoutAttribute.Right, NSLayoutRelation.Equal,
					HotLineBanner, NSLayoutAttribute.Right, 1, 0),
				NSLayoutConstraint.Create(_ivHotLineBanner, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal,
					HotLineBanner, NSLayoutAttribute.CenterY, 1, 0)

			});
		}

		public override void ViewDidLayoutSubviews()
		{
			base.ViewDidLayoutSubviews();

			var stringAttributes = new UIStringAttributes
			{
				ParagraphStyle = new NSMutableParagraphStyle() { LineSpacing = 5f }
			};
			var attributedText = new NSMutableAttributedString(_lbSixthIntro.Text);
			attributedText.AddAttributes(stringAttributes, new NSRange(0, _lbSixthIntro.Description.Length));
			_lbSixthIntro.AttributedText = attributedText;
			_lbSixthIntro.TextAlignment = UITextAlignment.Center;
			}

		private void CreateLowerView()
		{
			ResolutionHelper.InitStaticVariable();
			LowerView = UIHelper.CreateView(0, 0, UIColor.White);
			_logoLayout = UIHelper.CreateView(0, 0, UIColor.White);

			_lbThirdIntro = UIHelper.CreateLabel(ColorHelper.TextColor, DimensionHelper.BigTextSize, true,
				UITextAlignment.Center);
			_lbForthIntro = UIHelper.CreateLabel(ColorHelper.TextColor, DimensionHelper.MediumTextSize, false,
				UITextAlignment.Center);
			_lbFifthIntro = UIHelper.CreateLabel(ColorHelper.TextColor, DimensionHelper.MediumTextSize, false,
				UITextAlignment.Center);
			_lbSixthIntro = UIHelper.CreateLabel(ColorHelper.TextColor, DimensionHelper.MediumTextSize, false,
				UITextAlignment.Center, 10);

			_lbSeventhIntro = UIHelper.CreateLabel(ColorHelper.TextColor, DimensionHelper.MediumTextSize, false,
				UITextAlignment.Center);
			_lbEighthIntro = UIHelper.CreateLabel(ColorHelper.TextColor, DimensionHelper.MediumTextSize, true,
				UITextAlignment.Left, 2);

			_ivSiouxLogo = UIHelper.CreateAlphaImageView(0, 0, DimensionHelper.ImageViewPadding/2);
			_ivSiouxLogo.Image = UIImage.FromFile(ImageHelper.SiouxLogo);
			_ivSiouxLogo.UserInteractionEnabled = false;

			_ivGGLogo = UIHelper.CreateAlphaImageView(0, 0, 3f*DimensionHelper.ImageViewPadding);
			_ivGGLogo.Image = UIImage.FromFile(ImageHelper.GGLogo);
			_ivGGLogo.UserInteractionEnabled = false;

			_ivDttLogo = UIHelper.CreateAlphaImageView(0, 0, 3f * DimensionHelper.ImageViewPadding);
			_ivDttLogo.Image = UIImage.FromFile(ImageHelper.DttLogo);
			_ivDttLogo.UserInteractionEnabled = false;

			_logoLayout.Add(_ivSiouxLogo);
			_logoLayout.Add(_ivDttLogo);
			_logoLayout.Add(_ivGGLogo);

			_logoLayout.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_ivDttLogo, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal,
					_logoLayout, NSLayoutAttribute.CenterY, 1, 0),
				NSLayoutConstraint.Create(_ivDttLogo, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					_logoLayout, NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_ivDttLogo, NSLayoutAttribute.Width, NSLayoutRelation.Equal,
					_logoLayout, NSLayoutAttribute.Width, 1/3f, 0),

				NSLayoutConstraint.Create(_ivSiouxLogo, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal,
					_logoLayout, NSLayoutAttribute.CenterY, 1, 0),
				NSLayoutConstraint.Create(_ivSiouxLogo, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal,
					_logoLayout, NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_ivSiouxLogo, NSLayoutAttribute.Width, NSLayoutRelation.Equal,
					_logoLayout, NSLayoutAttribute.Width, 1/3f, 0),

				NSLayoutConstraint.Create(_ivGGLogo, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal,
					_logoLayout, NSLayoutAttribute.CenterY, 1, 0),
				NSLayoutConstraint.Create(_ivGGLogo, NSLayoutAttribute.Right, NSLayoutRelation.Equal,
					_logoLayout, NSLayoutAttribute.Right, 1, 0),
				NSLayoutConstraint.Create(_ivGGLogo, NSLayoutAttribute.Width, NSLayoutRelation.Equal,
					_logoLayout, NSLayoutAttribute.Width, 1/3f, 0),

				NSLayoutConstraint.Create(_logoLayout, NSLayoutAttribute.Height, NSLayoutRelation.Equal,
					null, NSLayoutAttribute.NoAttribute, 1, DimensionHelper.AboutLogoSize),
			});

			_danangLayout = UIHelper.CreateView(0, DimensionHelper.AboutLogoSize);
			_ivDanangLogo = UIHelper.CreateAlphaImageView(DimensionHelper.AboutLogoSize, DimensionHelper.AboutLogoSize, 0);
			_ivDanangLogo.Image = UIImage.FromBundle(ImageHelper.DanangLogo);
			_danangLayout.Add(_ivDanangLogo);
			_danangLayout.Add(_lbEighthIntro);
			_danangLayout.AddConstraints(new []
			{
				NSLayoutConstraint.Create(_ivDanangLogo, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal,
					_danangLayout, NSLayoutAttribute.CenterY, 1, 0),
				NSLayoutConstraint.Create(_ivDanangLogo, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					_danangLayout, NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_ivDanangLogo, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_danangLayout, NSLayoutAttribute.Top, 1, 0),

				NSLayoutConstraint.Create(_lbEighthIntro, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal,
					_danangLayout, NSLayoutAttribute.CenterY, 1, 0),
				NSLayoutConstraint.Create(_lbEighthIntro, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					_ivDanangLogo, NSLayoutAttribute.Right, 1, DimensionHelper.MediumContentMargin),
				NSLayoutConstraint.Create(_lbEighthIntro, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_danangLayout, NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_lbEighthIntro, NSLayoutAttribute.Right, NSLayoutRelation.Equal,
					_danangLayout, NSLayoutAttribute.Right, 1, 0),
			});

			_divider = UIHelper.CreateView(0, DimensionHelper.ThinDivider, ColorHelper.DividerGray);

			LowerView.Add(_lbThirdIntro);
			LowerView.Add(_lbForthIntro);
			LowerView.Add(_lbFifthIntro);
			LowerView.Add(_lbSixthIntro);
			LowerView.Add(_lbSeventhIntro);
			LowerView.Add(_divider);
			LowerView.Add(_danangLayout);
			LowerView.Add(_logoLayout);

			LowerView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_lbThirdIntro, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal,
					LowerView, NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_lbThirdIntro, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					LowerView, NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_lbThirdIntro, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					LowerView, NSLayoutAttribute.Top, 1, DimensionHelper.AboutMediumMargin),
				NSLayoutConstraint.Create(_lbForthIntro, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal,
					LowerView, NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_lbForthIntro, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					LowerView, NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_lbForthIntro, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_lbThirdIntro, NSLayoutAttribute.Bottom, 1, DimensionHelper.AboutMediumMargin),
				NSLayoutConstraint.Create(_lbFifthIntro, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal,
					LowerView, NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_lbFifthIntro, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					LowerView, NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_lbFifthIntro, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_lbForthIntro, NSLayoutAttribute.Bottom, 1, DimensionHelper.AboutNarrowMargin),
				NSLayoutConstraint.Create(_lbSixthIntro, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal,
					LowerView, NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_lbSixthIntro, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					LowerView, NSLayoutAttribute.Left, 1, DimensionHelper.MediumContentMargin),
				NSLayoutConstraint.Create(_lbSixthIntro, NSLayoutAttribute.Right, NSLayoutRelation.Equal,
					LowerView, NSLayoutAttribute.Right, 1, - DimensionHelper.MediumContentMargin),
				NSLayoutConstraint.Create(_lbSixthIntro, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_lbFifthIntro, NSLayoutAttribute.Bottom, 1, DimensionHelper.AboutMediumMargin),

				NSLayoutConstraint.Create(_divider, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_lbSixthIntro, NSLayoutAttribute.Bottom, 1, DimensionHelper.AboutMarginTB),
				NSLayoutConstraint.Create(_divider, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal,
					LowerView, NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_divider, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					LowerView, NSLayoutAttribute.Left, 1, DimensionHelper.AboutMarginLR),
				NSLayoutConstraint.Create(_divider, NSLayoutAttribute.Right, NSLayoutRelation.Equal,
					LowerView, NSLayoutAttribute.Right, 1, -DimensionHelper.AboutMarginLR),

				NSLayoutConstraint.Create(_lbSeventhIntro, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal,
					LowerView, NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_lbSeventhIntro, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					LowerView, NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_lbSeventhIntro, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_divider, NSLayoutAttribute.Bottom, 1, DimensionHelper.AboutMarginTB),

				NSLayoutConstraint.Create(_danangLayout, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_lbSeventhIntro, NSLayoutAttribute.Bottom, 1, DimensionHelper.AboutWideMargin),
				NSLayoutConstraint.Create(_danangLayout, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal,
					LowerView, NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_danangLayout, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					LowerView, NSLayoutAttribute.Left, 1, DimensionHelper.AboutMarginLR),
				NSLayoutConstraint.Create(_danangLayout, NSLayoutAttribute.Right, NSLayoutRelation.Equal,
					LowerView, NSLayoutAttribute.Right, 1, -DimensionHelper.AboutMarginLR),

				NSLayoutConstraint.Create(_logoLayout, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal,
					LowerView, NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_logoLayout, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_danangLayout, NSLayoutAttribute.Bottom, 1, DimensionHelper.AboutWideMargin),
				NSLayoutConstraint.Create(_logoLayout, NSLayoutAttribute.Width, NSLayoutRelation.Equal,
					LowerView, NSLayoutAttribute.Width, 1, 0),
				NSLayoutConstraint.Create(_logoLayout, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal,
					LowerView, NSLayoutAttribute.Bottom, 1, - DimensionHelper.SmallContentMargin)
			});
		}

		protected override void CreateBinding()
		{
			base.CreateBinding();
			var set = this.CreateBindingSet<AboutView, AboutViewModel>();

			set.Bind(_lbThirdIntro).For(v => v.Text).To(vm => vm.ThirdIntro);
			set.Bind(_lbForthIntro).For(v => v.Text).To(vm => vm.FourthIntro);
			set.Bind(_lbFifthIntro).For(v => v.Text).To(vm => vm.FifthIntro);
			set.Bind(_lbSixthIntro).For(v => v.Text).To(vm => vm.SixthIntro);
			set.Bind(_lbSeventhIntro).For(v => v.Text).To(vm => vm.SeventhIntro);
			set.Bind(_lbEighthIntro).For(v => v.Text).To(vm => vm.EighthIntro);
			set.Apply();
		}

		public override void WillEnterForeground()
		{
		}
	}
}