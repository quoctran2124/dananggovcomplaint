using GovernmentComplaint.Core.ViewModels;
using MvvmCross.Binding.BindingContext;

namespace GovernmentComplaint.iOS.Views
{
    public partial class CreateNewComplaintView
    {
        protected override void CreateBinding()
        {
            base.CreateBinding();

            var set = this.CreateBindingSet<CreateNewComplaintView, CreateNewComplaintViewModel>();

            //Cancel button
            set.Bind(_cancelButton).For("Title").To(vm => vm.Cancel);
            set.Bind(_cancelButton).For(v => v.ClickCommand).To(vm => vm.CancelCommand);

            //Send button
            set.Bind(_lbSendText).To(vm => vm.Send);
            set.Bind(_sendButtonContainer).For(v => v.ClickCommand).To(vm => vm.SendCommand);

            //Attach region
            set.Bind(_lbNoAttachedText).To(vm => vm.NoAttachedFile);

            //Video button
            set.Bind(_lbLinkVideoText).To(vm => vm.Video);
            set.Bind(_lbLinkVideoText).For(v => v.ClickCommand).To(vm => vm.VideoCommand);
            set.Bind(_ivLinkVideoIcon).For(v => v.ClickCommand).To(vm => vm.VideoCommand);
            set.Bind(_linkVideoContainer).For(v => v.ClickCommand).To(vm => vm.VideoCommand);

            set.Bind(this).For(v => v.VideoLinkValue).To(vm => vm.VideoLinkValue).TwoWay();

            //Image button
            set.Bind(_lbLinkPictureText).To(vm => vm.Picture);
            set.Bind(_linkPictureContainer).For(v => v.ClickCommand).To(vm => vm.AddImagesCommand);
            set.Bind(_lbLinkPictureText).For(v => v.ClickCommand).To(vm => vm.AddImagesCommand);
            set.Bind(_ivLinkPcitureIcon).For(v => v.ClickCommand).To(vm => vm.AddImagesCommand);
            set.Bind(this).For(v => v.AttachmentStreamsValue).To(vm => vm.AttachmentStreamsValue).TwoWay();
            set.Bind(this).For(v => v.IsImagePickerShown).To(vm => vm.IsImagePickerShown);
            set.Bind(this).For(v => v.IsVideoLayoutShown).To(vm => vm.IsVideoLayoutShown).TwoWay();
            set.Bind(this)
                .For(v => v.AttachWrongFormatOrSizeErrorCommand)
                .To(vm => vm.AttachWrongFormatOrSizeErrorCommand);
            //Attach text
            set.Bind(_lbAttachText).To(vm => vm.Attachment);

            //Popup video
            set.Bind(_lbVideoLink).To(vm => vm.VideoLinkValue);

            //Location
            set.Bind(this)
                .For(v => v.LocationBackgroundType)
                .To(vm => vm.LocationBackgroundType)
                .WithConversion("BackgroundTypeToCGColor");

            set.Bind(_tfLocation).To(vm => vm.LocationText);
            _tfLocation.Placeholder = "";
            set.Bind(_tfLocation).For(v => v.Placeholder).To(vm => vm.LocationHint);
            set.Bind(this).For(v => v.LocationCommand).To(vm => vm.LocationCommand);

            set.Bind(_lbLocationHint).To(vm => vm.LocationHint);
            set.Bind(_lbLocationHint).For(v => v.Hidden).To(vm => vm.IsLocationEmpty).WithConversion("InvertBool");
            set.Bind(_locationContainer).For(v => v.ClickCommand).To(vm => vm.LocationCommand);

            //Complaint Content
            set.Bind(this)
                .For(v => v.ContentBackgroundType)
                .To(vm => vm.ContentBackgroundType)
                .WithConversion("BackgroundTypeToCGColor");

            set.Bind(_ctvComplaintContent.TextView).For("Text").To(vm => vm.ComplaintContentText);
            set.Bind(_ctvComplaintContent).For(v => v.IsError).To(vm => vm.IsComplaintContentEmpty);
            set.Bind(_ctvComplaintContent).For(v => v.PlaceHolder).To(vm => vm.ComplaintContentHint);
            set.Bind(_ctvComplaintContent).For(v => v.ClickCommand).To(vm => vm.ComplaintContentCommand);

            set.Bind(this).For(v => v.IsKeyboardHidden).To(vm => vm.IsKeyboardHidden);

            set.Apply();
        }
    }
}