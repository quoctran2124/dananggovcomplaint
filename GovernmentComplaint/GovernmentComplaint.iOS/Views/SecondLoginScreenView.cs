using System;
using System.Collections.Generic;
using System.Windows.Input;
using Facebook.LoginKit;
using Google.SignIn;
using GovernmentComplaint.Core.ViewModels;
using GovernmentComplaint.iOS.Controls;
using GovernmentComplaint.iOS.Helpers;
using GovernmentComplaint.iOS.Views.Bases;
using MvvmCross.Binding.BindingContext;
using UIKit;

namespace GovernmentComplaint.iOS.Views
{
	public class SecondLoginScreenView : DetailView
	{
		public override void WillEnterForeground()
		{
			CheckNetworkCommand?.Execute(null);
		}

		protected override void InitView()
		{
			View.BackgroundColor = ColorHelper.LightestGray;
			InitElements();
			AddFacebookLogin();
			AddGoogleLogin();
			AddCancelResending();

			View.Add(_lbSecondLoginText);
			View.Add(_facebookLoginView);
			View.Add(_googleLoginView);
			View.Add(_divider);
			View.Add(_cancelResendingView);

			View.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_lbSecondLoginText, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_lbSecondLoginText, NSLayoutAttribute.Top, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Top, 1, DimensionHelper.PersonalInfoTopMargin),
				NSLayoutConstraint.Create(_facebookLoginView, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_facebookLoginView, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_lbSecondLoginText, NSLayoutAttribute.Bottom, 1, DimensionHelper.PersonalInfoTopMargin),
				NSLayoutConstraint.Create(_googleLoginView, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_googleLoginView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _facebookLoginView,
					NSLayoutAttribute.Bottom, 1, DimensionHelper.PersonalInfoCommonMargin),

				NSLayoutConstraint.Create(_divider, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_divider, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _googleLoginView,
					NSLayoutAttribute.Bottom, 1, DimensionHelper.BigContentMargin),

				NSLayoutConstraint.Create(_cancelResendingView, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_cancelResendingView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _divider,
					NSLayoutAttribute.Bottom, 1, DimensionHelper.BigContentMargin)
			});
			base.InitView();
		}

		protected override void CreateBinding()
		{
			base.CreateBinding();
			var set = this.CreateBindingSet<SecondLoginScreenView, SecondLoginScreenViewModel>();

			set.Bind(_lbSecondLoginText).To(vm => vm.SecondLoginText);
			set.Bind(_lbFacebookText).To(vm => vm.Facebook);
			set.Bind(_lbGoogleText).To(vm => vm.Google);
			set.Bind(_lbCancelResending).To(vm => vm.CancelResendingOfflineFeedbackText);
			set.Bind(this).For(v => v.CheckNetworkCommand).To(vm => vm.CheckNetworkCommand);
			set.Bind(_cancelResendingView).For(v => v.ClickCommand).To(vm => vm.CancelResendingCommand);
			set.Bind(_cancelResendingView).For(v => v.Hidden).To(vm => vm.IsCancelResendingButtonShown).WithConversion("InvertBool");
			set.Bind(_divider).For(v => v.Hidden).To(vm => vm.IsCancelResendingButtonShown).WithConversion("InvertBool");
			set.Apply();
		}


		private void InitElements()
		{
			_lbSecondLoginText = UIHelper.CreateLabel(ColorHelper.Black, DimensionHelper.MediumTextSize);
			_lbSecondLoginText.Lines = 2;
		}

		private void AddFacebookLogin()
		{
			_facebookLoginView = UIHelper.CreateAlphaView(DimensionHelper.LoginButtonWidth,
				DimensionHelper.LoginButtonHeight, 0,
				ColorHelper.DarkBlue, true);
			_lbFacebookText = UIHelper.CreateLabel(ColorHelper.White, DimensionHelper.MediumTextSize);
			_ivFacebookLogo = UIHelper.CreateAlphaImageView(DimensionHelper.LoginButtonHeight,
				DimensionHelper.LoginButtonHeight, DimensionHelper.SearchButtonPadding);
			_ivFacebookLogo.Image = UIImage.FromFile(ImageHelper.FacebookLogo);

			_facebookLoginView.Add(_lbFacebookText);
			_facebookLoginView.Add(_ivFacebookLogo);

			_facebookLoginView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_ivFacebookLogo, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal,
					_facebookLoginView,
					NSLayoutAttribute.CenterY, 1, 0),
				NSLayoutConstraint.Create(_ivFacebookLogo, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					_facebookLoginView,
					NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_lbFacebookText, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal,
					_facebookLoginView,
					NSLayoutAttribute.CenterY, 1, 0),
				NSLayoutConstraint.Create(_lbFacebookText, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					_ivFacebookLogo,
					NSLayoutAttribute.Right, 1, 0)
			});
			_facebookLoginView.Clicked += FacebookLoginViewClicked;
		}

		private void AddCancelResending()
		{
			_divider = UIHelper.CreateView(DimensionHelper.GoogleLoginButtonWidth + DimensionHelper.BigContentMargin,
				DimensionHelper.ThinDivider, ColorHelper.Divider);

			_cancelResendingView = UIHelper.CreateAlphaView(DimensionHelper.LoginButtonWidth,
				   DimensionHelper.LoginButtonHeight, 0, ColorHelper.LightGray, true);
			_lbCancelResending = UIHelper.CreateLabel(ColorHelper.White, DimensionHelper.MediumTextSize);

			_cancelResendingView.Add(_lbCancelResending);

			_cancelResendingView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_lbCancelResending, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal,
					_cancelResendingView,
					NSLayoutAttribute.CenterY, 1, 0),
				NSLayoutConstraint.Create(_lbCancelResending, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal,
					_cancelResendingView,
					NSLayoutAttribute.CenterX, 1, 0)
			});
		}

		private void FacebookLoginViewClicked(object sender, EventArgs e)
		{
			var loginManager = new LoginManager();
			loginManager.LogInWithReadPermissions(_readPermissions.ToArray(),
				UIApplication.SharedApplication.KeyWindow.RootViewController,
				(result, error)
					=>
				{
					if (error != null)
					{
						return;
					}
					if (result.IsCancelled)
					{
						return;
					}
					// Handle your successful login
				});
		}

		private void AddGoogleLogin()
		{
			_googleLoginView = UIHelper.CreateAlphaView(DimensionHelper.LoginButtonWidth, DimensionHelper.LoginButtonHeight,
				0, ColorHelper.Red, true);
			_lbGoogleText = UIHelper.CreateLabel(ColorHelper.White, DimensionHelper.MediumTextSize);
			_ivGoogleLogo = UIHelper.CreateAlphaImageView(DimensionHelper.LoginButtonHeight,
				DimensionHelper.LoginButtonHeight, DimensionHelper.SearchButtonPadding);
			_ivGoogleLogo.Image = UIImage.FromFile(ImageHelper.GoogleLogo);

			_googleLoginView.Add(_lbGoogleText);
			_googleLoginView.Add(_ivGoogleLogo);

			_googleLoginView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_ivGoogleLogo, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, _googleLoginView,
					NSLayoutAttribute.CenterY, 1, 0),
				NSLayoutConstraint.Create(_ivGoogleLogo, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _googleLoginView,
					NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_lbGoogleText, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, _googleLoginView,
					NSLayoutAttribute.CenterY, 1, 0),
				NSLayoutConstraint.Create(_lbGoogleText, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _ivGoogleLogo,
					NSLayoutAttribute.Right, 1, 0)
			});
			_googleLoginView.Clicked += GoogleLoginViewViewClicked;
		}

		private void GoogleLoginViewViewClicked(object sender, EventArgs e)
		{
			SignIn.SharedInstance.SignInUser();
		}
		#region Properties

		private readonly List<string> _readPermissions = new List<string> {"public_profile"};
		public ICommand CheckNetworkCommand { get; set; }
		private UILabel _lbSecondLoginText;
		private AlphaUIView _facebookLoginView;
		private AlphaImageView _ivFacebookLogo;
		private UILabel _lbFacebookText;
		private AlphaUIView _googleLoginView;
		private AlphaImageView _ivGoogleLogo;
		private UILabel _lbGoogleText;

		private UIView _divider;
		private AlphaUIView _cancelResendingView;
		private UILabel _lbCancelResending;

		#endregion
	}
}