using CoreGraphics;
using GovernmentComplaint.Core.ViewModels;
using GovernmentComplaint.iOS.Controls;
using GovernmentComplaint.iOS.Helpers;
using GovernmentComplaint.iOS.Views.Bases;
using MvvmCross.Binding.BindingContext;
using UIKit;

namespace GovernmentComplaint.iOS.Views
{
	public class AddPersonalInfoView : DetailView
	{
		protected override void InitView()
		{
			View.BackgroundColor = ColorHelper.LightestGray;
			InitElements();

			View.AddGestureRecognizer(new UITapGestureRecognizer(HideKeyboard));

			View.Add(_lbAddPersonalInfo1);
			View.Add(_tfFullName);
			View.Add(_tfEmail);
			View.Add(_tfPhone);
			View.Add(_lbVerifyMessage);
			View.Add(_cancelButton);
			View.Add(_sendButton);

			View.AddConstraints(new[]
			{
				//Label
				NSLayoutConstraint.Create(_lbAddPersonalInfo1, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_lbAddPersonalInfo1, NSLayoutAttribute.Top, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Top, 1, DimensionHelper.PersonalInfoTopMargin),

				//Full Name
				NSLayoutConstraint.Create(_tfFullName, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_tfFullName, NSLayoutAttribute.Width, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Width, 1, -2 * DimensionHelper.PersonalInfoCommonMargin),
				NSLayoutConstraint.Create(_tfFullName, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_lbAddPersonalInfo1, NSLayoutAttribute.Bottom, 1, DimensionHelper.PersonalInfoTopMargin),
				NSLayoutConstraint.Create(_tfFullName, NSLayoutAttribute.Height, NSLayoutRelation.Equal, null,
					NSLayoutAttribute.NoAttribute, 1, DimensionHelper.PersonalInfoEditTextHeight),

				//Email
				NSLayoutConstraint.Create(_tfEmail, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_tfEmail, NSLayoutAttribute.Width, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Width, 1, -2 * DimensionHelper.PersonalInfoCommonMargin),
				NSLayoutConstraint.Create(_tfEmail, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _tfFullName,
					NSLayoutAttribute.Bottom, 1, DimensionHelper.PersonalInfoCommonMargin),
				NSLayoutConstraint.Create(_tfEmail, NSLayoutAttribute.Height, NSLayoutRelation.Equal, null,
					NSLayoutAttribute.NoAttribute, 1, DimensionHelper.PersonalInfoEditTextHeight),

				//Phone
				NSLayoutConstraint.Create(_tfPhone, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_tfPhone, NSLayoutAttribute.Width, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Width, 1, -2 * DimensionHelper.PersonalInfoCommonMargin),
				NSLayoutConstraint.Create(_tfPhone, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _tfEmail,
					NSLayoutAttribute.Bottom, 1, DimensionHelper.PersonalInfoCommonMargin),
				NSLayoutConstraint.Create(_tfPhone, NSLayoutAttribute.Height, NSLayoutRelation.Equal, null,
					NSLayoutAttribute.NoAttribute, 1, DimensionHelper.PersonalInfoEditTextHeight),

				//Verify Message
				NSLayoutConstraint.Create(_lbVerifyMessage, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _tfPhone,
					NSLayoutAttribute.Bottom, 1, DimensionHelper.MessageMarginTop),
				NSLayoutConstraint.Create(_lbVerifyMessage, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _tfPhone,
					NSLayoutAttribute.Left, 1, DimensionHelper.MessageMarginLeft),

				//Send Button
				NSLayoutConstraint.Create(_sendButton, NSLayoutAttribute.Right, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Right, 1, -DimensionHelper.PersonalInfoCommonMargin),
				NSLayoutConstraint.Create(_sendButton, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Bottom, 1, -DimensionHelper.PersonalInfoCommonMargin),

				//Cancel Button
				NSLayoutConstraint.Create(_cancelButton, NSLayoutAttribute.Left, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Left, 1, DimensionHelper.PersonalInfoCommonMargin),
				NSLayoutConstraint.Create(_cancelButton, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Bottom, 1, -DimensionHelper.PersonalInfoCommonMargin),
			});
			base.InitView();
		}

		protected override void CreateBinding()
		{
			base.CreateBinding();
			var set = this.CreateBindingSet<AddPersonalInfoView, AddPersonalInfoViewModel>();

			set.Bind(_lbAddPersonalInfo1).To(vm => vm.AddInformationLabel1);
			// set.Bind(_lbAddPersonalInfo2).To(vm => vm.AddInformationLabel2);
			set.Bind(_tfFullName).For(v => v.Placeholder).To(vm => vm.FullName);
			set.Bind(_tfFullName).For(v => v.Text).To(vm => vm.UserName);
			set.Bind(_tfEmail).For(v => v.Placeholder).To(vm => vm.Email);
			set.Bind(_tfEmail).For(v => v.Text).To(vm => vm.UserEmail);
			set.Bind(_tfPhone).For(v => v.Placeholder).To(vm => vm.Phone);
			set.Bind(_tfPhone).For(v => v.Text).To(vm => vm.UserPhoneNumber);
			set.Bind(_cancelButton).For("Title").To(vm => vm.Cancel);
			set.Bind(_sendButton).For("Title").To(vm => vm.Confirm);
			set.Bind(_sendButton).For(v => v.ClickCommand).To(vm => vm.SendCommand);
			set.Bind(_cancelButton).For(v => v.ClickCommand).To(vm => vm.CancelCommand);
			set.Bind(_lbVerifyMessage).For(v => v.Text).To(vm => vm.VerifyMessage);
			set.Bind(this).For(v => v._isKeyboardHidden).To(vm => vm.IsKeyboardShown).WithConversion("InvertBool");
			set.Bind(this).For(v => v.IsUserNameNotAvailable).To(vm => vm.IsUserNameNotAvailable);
			set.Bind(this).For(v => v.IsUserEmailNotAvailable).To(vm => vm.IsUserEmailNotAvailable);
			set.Apply();
		}

		private void InitElements()
		{
			//Label
			_lbAddPersonalInfo1 = UIHelper.CreateLabel(ColorHelper.Black, DimensionHelper.MediumTextSize);

			//FullName text Field
			var paddingView1 = new UIView(new CGRect(0, 0, 7, 20));
			_tfFullName = UIHelper.CreateTextField(DimensionHelper.MediumTextSize, FontType.Regular, ColorHelper.White,
				true, true);
			_tfFullName.TextColor = ColorHelper.GrayTextColor;
			_tfFullName.LeftView = paddingView1;
			_tfFullName.LeftViewMode = UITextFieldViewMode.Always;
			_tfFullName.ShouldReturn += ShouldFullNameReturn;

			//Email text feld
			var paddingView2 = new UIView(new CGRect(0, 0, 7, 20));
			_tfEmail = UIHelper.CreateTextField(DimensionHelper.MediumTextSize, FontType.Regular, ColorHelper.White,
				true, true);
			_tfEmail.TextColor = ColorHelper.GrayTextColor;
			_tfEmail.LeftView = paddingView2;
			_tfEmail.LeftViewMode = UITextFieldViewMode.Always;
			_tfEmail.ShouldReturn += ShouldEmailReturn;

			//Phone text field
			var paddingView3 = new UIView(new CGRect(0, 0, 7, 20));
			_tfPhone = UIHelper.CreateTextField(DimensionHelper.MediumTextSize, FontType.Regular, ColorHelper.White,
				true, true);
			_tfPhone.LeftView = paddingView3;
			_tfPhone.LeftViewMode = UITextFieldViewMode.Always;
			_tfPhone.ShouldReturn += ShouldPhoneReturn;

			_lbVerifyMessage = UIHelper.CreateLabel(UIColor.Red, DimensionHelper.MediumTextSize);
			//Button
			_sendButton = UIHelper.CreateButton(DimensionHelper.NComplaintButtonWidth,
				DimensionHelper.NComplaintButtonHeight,
				ColorHelper.White, DimensionHelper.BigTextSize, ColorHelper.Orange, true, true, true);
			_cancelButton = UIHelper.CreateButton(DimensionHelper.NComplaintButtonWidth,
				DimensionHelper.NComplaintButtonHeight,
				ColorHelper.LightGray, DimensionHelper.BigTextSize, ColorHelper.BorderGray, true, true, true);
		}

		private bool ShouldFullNameReturn(UITextField textfield)
		{
			_tfFullName.ResignFirstResponder();
			_tfEmail.BecomeFirstResponder();
			return false;
		}

		private bool ShouldEmailReturn(UITextField textfield)
		{
			_tfEmail.ResignFirstResponder();
			_tfPhone.BecomeFirstResponder();
			return false;
		}

		private bool ShouldPhoneReturn(UITextField textField)
		{
			_tfPhone.ResignFirstResponder();
			return false;
		}

		#region UI Elements

		//Label
		private UILabel _lbAddPersonalInfo1;
		//private UILabel _lbAddPersonalInfo2;
		private UILabel _lbVerifyMessage;
		//Text Field
		private UITextField _tfFullName;
		private UITextField _tfEmail;
		private UITextField _tfPhone;

		//Bottom Buttons
		private AlphaUIButton _sendButton;
		private AlphaUIButton _cancelButton;

		private bool _isKeyboardHidden;

		public bool IsKeyboardHidden
		{
			get { return _isKeyboardHidden; }
			set
			{
				_isKeyboardHidden = value;
				if (value)
					View.EndEditing(true);
			}
		}

		private bool _isUserNameNotAvailable;

		public bool IsUserNameNotAvailable
		{
			get { return _isUserNameNotAvailable; }

			set
			{
				_isUserNameNotAvailable = value;
				_tfFullName.BackgroundColor = value ? ColorHelper.White : ColorHelper.GrayBackground;
				_tfFullName.Enabled = value;
			}
		}

		private bool _isUserEmailNotAvailable;

		public bool IsUserEmailNotAvailable
		{
			get { return _isUserEmailNotAvailable; }

			set
			{
				_isUserEmailNotAvailable = value;
				_tfEmail.BackgroundColor = value ? ColorHelper.White : ColorHelper.GrayBackground;
				_tfEmail.Enabled = value;
			}
		}

		public override void WillEnterForeground()
		{

		}

		#endregion
	}
}