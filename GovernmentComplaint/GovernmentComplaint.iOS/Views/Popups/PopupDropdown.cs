using CoreAnimation;
using CoreGraphics;
using GovernmentComplaint.Core.ViewModels.Bases;
using GovernmentComplaint.iOS.Controls;
using GovernmentComplaint.iOS.Helpers;
using GovernmentComplaint.iOS.Views.Bases;
using GovernmentComplaint.iOS.Views.TableView.ItemTableViewSource;
using MvvmCross.Binding.BindingContext;
using UIKit;

namespace GovernmentComplaint.iOS.Views.Popups
{
	public class PopupDropdown : PopupLayout
	{
		public UIView Header;
		private AlphaLabel _lbHeader;
		public AlphaImageView IvCloseButton;
		private DepartmentItemTableViewSource _departmentItemTableViewSource;
		private UITableView _departmentTableView;

		public PopupDropdown(MvxFluentBindingDescriptionSet<MasterView, MasterViewModel> bindingSet)
		{
			CreateBinding(bindingSet);

			var gradientLayer = GetGradientLayer(Header.Bounds);
			gradientLayer.CornerRadius = DimensionHelper.ToastCorner;
			gradientLayer.BorderWidth = 1;
			gradientLayer.BorderColor = ColorHelper.BorderGray.CGColor;

			Header.Layer.InsertSublayer(gradientLayer, 0);
		}

		protected override void InitView()
		{
			base.InitView();
			Header = UIHelper.CreateView(0, DimensionHelper.MassiveContentMargin, ColorHelper.Gray);
			Header.Layer.CornerRadius = DimensionHelper.ToastCorner;
			_lbHeader = UIHelper.CreateAlphaLabel(ColorHelper.GrayTransparent, DimensionHelper.BigTextSize);
			IvCloseButton = UIHelper.CreateAlphaImageView(DimensionHelper.MassiveContentMargin,
				DimensionHelper.MassiveContentMargin, DimensionHelper.ContentPadding);
			IvCloseButton.Image = UIImage.FromFile(ImageHelper.Remove);
			_departmentTableView = UIHelper.CreateTableView(0, DimensionHelper.DepartmentTableViewHeight, UIColor.White);
			_departmentTableView.Layer.CornerRadius = DimensionHelper.ToastCorner;
			_departmentItemTableViewSource = new DepartmentItemTableViewSource(_departmentTableView);
			_departmentTableView.Source = _departmentItemTableViewSource;

			Header.Add(_lbHeader);
			Header.Add(IvCloseButton);
			Header.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_lbHeader, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal,
					Header, NSLayoutAttribute.CenterY, 1, 0),
				NSLayoutConstraint.Create(_lbHeader, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal,
					Header, NSLayoutAttribute.CenterX, 1, 0),

				NSLayoutConstraint.Create(IvCloseButton, NSLayoutAttribute.Right, NSLayoutRelation.Equal,
					Header, NSLayoutAttribute.Right, 1, 0),
				NSLayoutConstraint.Create(IvCloseButton, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal,
					Header, NSLayoutAttribute.CenterY, 1, 0),
			});

			Add(Header);
			Add(_departmentTableView);
			AddConstraints(new[]
			{
				NSLayoutConstraint.Create(Header, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					this, NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(Header, NSLayoutAttribute.Width, NSLayoutRelation.Equal,
					this, NSLayoutAttribute.Width, 1, 0),
				NSLayoutConstraint.Create(Header, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					this, NSLayoutAttribute.Left, 1, 0),

				NSLayoutConstraint.Create(_departmentTableView, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					Header, NSLayoutAttribute.Bottom, 1, 0),
				NSLayoutConstraint.Create(_departmentTableView, NSLayoutAttribute.Width, NSLayoutRelation.Equal,
					this, NSLayoutAttribute.Width, 1, 0),
				NSLayoutConstraint.Create(_departmentTableView, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					this, NSLayoutAttribute.Left, 1, 0),

				NSLayoutConstraint.Create(this, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal,
					_departmentTableView, NSLayoutAttribute.Bottom, 1, 0),
			});
		}

		private CAGradientLayer GetGradientLayer(CGRect cgRect)
		{
			var gradientLayer = new CAGradientLayer
			{
				Frame = cgRect,
				NeedsDisplayOnBoundsChange = true,
				//MasksToBounds = true,
				Colors = new[]
				{
					ColorHelper.CenterButton.CGColor, ColorHelper.EndButton.CGColor, ColorHelper.DropdownBarEnd.CGColor
				}
			};

			return gradientLayer;
		}

		private void CreateBinding(MvxFluentBindingDescriptionSet<MasterView, MasterViewModel> bindingSet)
		{
			bindingSet.Bind(_lbHeader).To(vm => vm.SpinnerHeader);
			bindingSet.Bind(_departmentItemTableViewSource).To(vm => vm.DepartmentItemViewModels);
			bindingSet.Bind(_lbHeader).For(v => v.ClickCommand).To(vm => vm.OnDepartmentCommand);
			bindingSet.Apply();
		}
	}
}