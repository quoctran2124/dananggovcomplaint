using GovernmentComplaint.iOS.Helpers;
using UIKit;

namespace GovernmentComplaint.iOS.Views.Popups
{
	public abstract class PopupLayout : UIView
	{
		public PopupLayout()
		{
			InitView();
		}

		protected virtual void InitView()
		{
			TranslatesAutoresizingMaskIntoConstraints = false;
			BackgroundColor = ColorHelper.LightestGray;
			var padding = DimensionHelper.PopupPadding;
			LayoutMargins = new UIEdgeInsets(padding, padding, padding, padding);
		}
	}
}