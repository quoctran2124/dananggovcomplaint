using GovernmentComplaint.Core.ViewModels.Bases;
using GovernmentComplaint.iOS.Controls;
using GovernmentComplaint.iOS.Helpers;
using GovernmentComplaint.iOS.Views.Bases;
using MvvmCross.Binding.BindingContext;
using UIKit;

namespace GovernmentComplaint.iOS.Views.Popups
{
	public class PopupFail24H : PopupLayout
	{
		private UILabel _lbPopupHeader;
		private UILabel _lbPopupContent;
		public AlphaUIButton ConfirmButton;

		public PopupFail24H(MvxFluentBindingDescriptionSet<MasterView, MasterViewModel> bindingSet)
		{
			CreateBinding(bindingSet);
		}

		protected override void InitView()
		{
			base.InitView();
			InitUiElements();

			AddSubview(_lbPopupHeader);
			AddSubview(_lbPopupContent);
			AddSubview(ConfirmButton);

			AddConstraints(new[]
			{
				//Confirm
				NSLayoutConstraint.Create(_lbPopupHeader, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, this,
					NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_lbPopupHeader, NSLayoutAttribute.Top, NSLayoutRelation.Equal, this,
					NSLayoutAttribute.Top, 1, 2 * DimensionHelper.PopupMargin),

				//Instruction
				NSLayoutConstraint.Create(_lbPopupContent, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal,
					this,
					NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_lbPopupContent, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_lbPopupHeader,
					NSLayoutAttribute.Bottom, 1, DimensionHelper.PopupMargin),

				//Confirm button
				NSLayoutConstraint.Create(ConfirmButton, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, this,
					NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(ConfirmButton, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _lbPopupContent,
					NSLayoutAttribute.Bottom, 1, DimensionHelper.PopupEmailMargin),
				NSLayoutConstraint.Create(ConfirmButton, NSLayoutAttribute.Width, NSLayoutRelation.Equal, null,
					NSLayoutAttribute.NoAttribute, 1, DimensionHelper.PopupButtonWidth),

				NSLayoutConstraint.Create(this, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal,
					ConfirmButton, NSLayoutAttribute.Bottom, 1, DimensionHelper.PopupButtonMargin),
			});
		}

		private void CreateBinding(MvxFluentBindingDescriptionSet<MasterView, MasterViewModel> bindingSet)
		{
			bindingSet.Bind(_lbPopupHeader).To(vm => vm.InformText);
			bindingSet.Bind(ConfirmButton).For("Title").To(vm => vm.Confirm);
			bindingSet.Bind(_lbPopupContent).To(vm => vm.Fail24HInstruction);
			bindingSet.Apply();
		}


		private void InitUiElements()
		{
			//Instruction
			_lbPopupHeader = UIHelper.CreateAlphaLabel(ColorHelper.Black, DimensionHelper.BigTextSize, true);
			_lbPopupContent = UIHelper.CreateAlphaLabel(ColorHelper.Black, DimensionHelper.MediumTextSize);

			//Confirm button
			ConfirmButton = UIHelper.CreateButton(0, DimensionHelper.PersonalInfoEditTextHeight,
				ColorHelper.White, DimensionHelper.MediumTextSize, ColorHelper.Blue, false, true, true);
		}
	}
}