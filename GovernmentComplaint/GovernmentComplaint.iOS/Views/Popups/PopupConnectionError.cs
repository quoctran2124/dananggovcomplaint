using GovernmentComplaint.Core.ViewModels.Bases;
using GovernmentComplaint.iOS.Controls;
using GovernmentComplaint.iOS.Helpers;
using GovernmentComplaint.iOS.Views.Bases;
using MvvmCross.Binding.BindingContext;
using UIKit;

namespace GovernmentComplaint.iOS.Views.Popups
{
    public class PopupConnectionError : PopupLayout
    {
        private UILabel _lbPopupContent;
        private UILabel _lbPopupHeader;
        public AlphaUIButton CancelButton;
        public AlphaUIButton ConfirmButton;

        public PopupConnectionError(MvxFluentBindingDescriptionSet<MasterView, MasterViewModel> bindingSet)
        {
            CreateBinding(bindingSet);
        }

        protected override void InitView()
        {
            base.InitView();
            InitUiElements();
            AddSubview(_lbPopupHeader);
            AddSubview(_lbPopupContent);
            AddSubview(CancelButton);
            AddSubview(ConfirmButton);
            AddConstraints(new[]
            {
                //Confirm
                NSLayoutConstraint.Create(_lbPopupHeader, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, this,
                    NSLayoutAttribute.CenterX, 1, 0),
                NSLayoutConstraint.Create(_lbPopupHeader, NSLayoutAttribute.Top, NSLayoutRelation.Equal, this,
                    NSLayoutAttribute.Top, 1, 2 * DimensionHelper.PopupMargin),

                //Instruction
                NSLayoutConstraint.Create(_lbPopupContent, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal,
                    this,
                    NSLayoutAttribute.CenterX, 1, 0),
                NSLayoutConstraint.Create(_lbPopupContent, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
                    _lbPopupHeader,
                    NSLayoutAttribute.Bottom, 1, DimensionHelper.PopupMargin),
                NSLayoutConstraint.Create(_lbPopupContent, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
                    this,
                    NSLayoutAttribute.Left, 1, DimensionHelper.FloatingButtonPadding),
                NSLayoutConstraint.Create(_lbPopupContent, NSLayoutAttribute.Right, NSLayoutRelation.Equal,
                    this,
                    NSLayoutAttribute.Right, 1, -DimensionHelper.FloatingButtonPadding),

                //Cancel popup button
                NSLayoutConstraint.Create(CancelButton, NSLayoutAttribute.Left, NSLayoutRelation.Equal, this,
                    NSLayoutAttribute.Left, 1, DimensionHelper.PopupMargin),
                NSLayoutConstraint.Create(CancelButton, NSLayoutAttribute.Right, NSLayoutRelation.Equal, this,
                    NSLayoutAttribute.CenterX, 1, -DimensionHelper.PopupMargin / 2),
                NSLayoutConstraint.Create(CancelButton, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _lbPopupContent,
                    NSLayoutAttribute.Bottom, 1, DimensionHelper.PopupLogoutMargin),

                //Confirm button
                NSLayoutConstraint.Create(ConfirmButton, NSLayoutAttribute.Right, NSLayoutRelation.Equal, this,
                    NSLayoutAttribute.Right, 1, -DimensionHelper.PopupMargin),
                NSLayoutConstraint.Create(ConfirmButton, NSLayoutAttribute.Left, NSLayoutRelation.Equal, this,
                    NSLayoutAttribute.CenterX, 1, DimensionHelper.PopupMargin / 2),
                NSLayoutConstraint.Create(ConfirmButton, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _lbPopupContent,
                    NSLayoutAttribute.Bottom, 1, DimensionHelper.PopupLogoutMargin),
                NSLayoutConstraint.Create(this, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal,
                    ConfirmButton, NSLayoutAttribute.Bottom, 1, DimensionHelper.PopupButtonMargin)
            });
        }

        private void CreateBinding(MvxFluentBindingDescriptionSet<MasterView, MasterViewModel> bindingSet)
        {
            bindingSet.Bind(_lbPopupHeader).To(vm => vm.InformText);
            bindingSet.Bind(ConfirmButton).For("Title").To(vm => vm.TryAgain);
            bindingSet.Bind(CancelButton).For("Title").To(vm => vm.CancelPopup);
            bindingSet.Bind(ConfirmButton)
                .For(v => v.ClickCommand)
                .To(vm => vm.HomeViewModel.ConnectionErrorConfirmCommand);
            bindingSet.Bind(_lbPopupContent).To(vm => vm.ConnectionErrorInstruction);
            bindingSet.Apply();
        }

        private void InitUiElements()
        {
            //Instruction
            _lbPopupHeader = UIHelper.CreateAlphaLabel(ColorHelper.Black, DimensionHelper.BigTextSize, true);
            _lbPopupContent = UIHelper.CreateAlphaLabel(ColorHelper.Black, DimensionHelper.MediumTextSize,false,UITextAlignment.Center,2);

            //Cancel popup button
            CancelButton = UIHelper.CreateButton(0, DimensionHelper.PersonalInfoEditTextHeight,
                ColorHelper.LightGray, DimensionHelper.MediumTextSize, ColorHelper.BorderGray, false, true, true);

            //Confirm button
            ConfirmButton = UIHelper.CreateButton(0, DimensionHelper.PersonalInfoEditTextHeight,
                ColorHelper.White, DimensionHelper.MediumTextSize, ColorHelper.Blue, false, true, true);
        }
    }
}