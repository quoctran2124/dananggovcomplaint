using GovernmentComplaint.Core.ViewModels.Bases;
using GovernmentComplaint.iOS.Controls;
using GovernmentComplaint.iOS.Helpers;
using GovernmentComplaint.iOS.Views.Bases;
using MvvmCross.Binding.BindingContext;
using UIKit;

namespace GovernmentComplaint.iOS.Views.Popups
{
	public class PopupCancelComment : PopupLayout
	{
		private UILabel _lbConfirm;
		private UILabel _lbConfirmInstruction1;
		private UILabel _lbConfirmCancel;
		public AlphaUIButton CancelCancelButton;
		public AlphaUIButton ConfirmCancelButton;

		public PopupCancelComment(MvxFluentBindingDescriptionSet<MasterView, MasterViewModel> bindingSet)
		{
			CreateBinding(bindingSet);
		}

		protected override void InitView()
		{
			base.InitView();
			InitUIElements();

			AddSubview(_lbConfirm);
			AddSubview(_lbConfirmInstruction1);
			AddSubview(_lbConfirmCancel);
			AddSubview(CancelCancelButton);
			AddSubview(ConfirmCancelButton);

			AddConstraints(new[]
			{
				//Confirm
				NSLayoutConstraint.Create(_lbConfirm, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, this,
					NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_lbConfirm, NSLayoutAttribute.Top, NSLayoutRelation.Equal, this,
					NSLayoutAttribute.Top, 1, 2*DimensionHelper.PopupMargin),

				//Instruction
				NSLayoutConstraint.Create(_lbConfirmInstruction1, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal,
					this,
					NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_lbConfirmInstruction1, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_lbConfirm,
					NSLayoutAttribute.Bottom, 1, DimensionHelper.PopupMargin),
				NSLayoutConstraint.Create(_lbConfirmCancel, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal,
					this,
					NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_lbConfirmCancel, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_lbConfirmInstruction1,
					NSLayoutAttribute.Bottom, 1, DimensionHelper.PopupMargin),

				//Cancel popup button
				NSLayoutConstraint.Create(CancelCancelButton, NSLayoutAttribute.Left, NSLayoutRelation.Equal, this,
					NSLayoutAttribute.Left, 1, DimensionHelper.PopupMargin),
				NSLayoutConstraint.Create(CancelCancelButton, NSLayoutAttribute.Right, NSLayoutRelation.Equal, this,
					NSLayoutAttribute.CenterX, 1, -DimensionHelper.PopupMargin/2),
				NSLayoutConstraint.Create(CancelCancelButton, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _lbConfirmCancel,
					NSLayoutAttribute.Bottom, 1, DimensionHelper.PopupCancelMargin),

				//Confirm button
				NSLayoutConstraint.Create(ConfirmCancelButton, NSLayoutAttribute.Right, NSLayoutRelation.Equal, this,
					NSLayoutAttribute.Right, 1, -DimensionHelper.PopupMargin),
				NSLayoutConstraint.Create(ConfirmCancelButton, NSLayoutAttribute.Left, NSLayoutRelation.Equal, this,
					NSLayoutAttribute.CenterX, 1, DimensionHelper.PopupMargin/2),
			   NSLayoutConstraint.Create(ConfirmCancelButton, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _lbConfirmCancel,
					NSLayoutAttribute.Bottom, 1, DimensionHelper.PopupCancelMargin),

				NSLayoutConstraint.Create(this, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal,
				ConfirmCancelButton, NSLayoutAttribute.Bottom, 1, DimensionHelper.PopupButtonMargin),
			});
		}

		private void CreateBinding(MvxFluentBindingDescriptionSet<MasterView, MasterViewModel> bindingSet)
		{
			bindingSet.Bind(_lbConfirm).To(vm => vm.Confirm);
			bindingSet.Bind(_lbConfirmInstruction1).To(vm => vm.ConfirmInstruction1);
			bindingSet.Bind(_lbConfirmCancel).To(vm => vm.ConfirmCancel);
			bindingSet.Bind(CancelCancelButton).For("Title").To(vm => vm.CancelPopup);
			bindingSet.Bind(ConfirmCancelButton).For("Title").To(vm => vm.Confirm);
			bindingSet.Bind(ConfirmCancelButton)
				.For(v => v.ClickCommand)
				.To(vm => vm.ComplaintDetailViewModel.ConfirmCancelCommand);

			bindingSet.Apply();
		}

		private void InitUIElements()
		{
			//Instruction
			_lbConfirm = UIHelper.CreateAlphaLabel(ColorHelper.Black, DimensionHelper.BigTextSize, true);
			_lbConfirmInstruction1 = UIHelper.CreateAlphaLabel(ColorHelper.Black, DimensionHelper.MediumTextSize);
			_lbConfirmCancel = UIHelper.CreateAlphaLabel(ColorHelper.Black, DimensionHelper.MediumTextSize);

			//Cancel popup button
			CancelCancelButton = UIHelper.CreateButton(0, DimensionHelper.PersonalInfoEditTextHeight,
				ColorHelper.LightGray, DimensionHelper.MediumTextSize, ColorHelper.BorderGray, false, true, true);

			//Confirm button
			ConfirmCancelButton = UIHelper.CreateButton(0, DimensionHelper.PersonalInfoEditTextHeight,
				ColorHelper.White, DimensionHelper.MediumTextSize, ColorHelper.Blue, false, true, true);
		}
	}
}