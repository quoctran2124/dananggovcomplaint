using GovernmentComplaint.Core.Localization;
using GovernmentComplaint.Core.ViewModels.Bases;
using GovernmentComplaint.iOS.Controls;
using GovernmentComplaint.iOS.Helpers;
using GovernmentComplaint.iOS.Views.Bases;
using MvvmCross.Binding.BindingContext;
using UIKit;

namespace GovernmentComplaint.iOS.Views.Popups
{
	public class PopupLogout : PopupLayout
	{
		private UILabel _lbConfirm;
		private UILabel _lbConfirmLogout;
		public AlphaUIButton CancelButton;
		public AlphaUIButton ConfirmButton;

		public PopupLogout(MvxFluentBindingDescriptionSet<MasterView, MasterViewModel> bindingSet)
		{
			CreateBinding(bindingSet);
		}

		protected override void InitView()
		{
			base.InitView();
			InitUIElements();

			AddSubview(_lbConfirm);
			AddSubview(_lbConfirmLogout);
			AddSubview(CancelButton);
			AddSubview(ConfirmButton);

			AddConstraints(new[]
			{
				//Confirm
				NSLayoutConstraint.Create(_lbConfirm, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, this,
					NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_lbConfirm, NSLayoutAttribute.Top, NSLayoutRelation.Equal, this,
					NSLayoutAttribute.Top, 1, 2 * DimensionHelper.PopupMargin),

				//Instruction
				NSLayoutConstraint.Create(_lbConfirmLogout, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal,
					this,
					NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_lbConfirmLogout, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_lbConfirm,
					NSLayoutAttribute.Bottom, 1, DimensionHelper.PopupMargin),

				//Cancel popup button
				NSLayoutConstraint.Create(CancelButton, NSLayoutAttribute.Left, NSLayoutRelation.Equal, this,
					NSLayoutAttribute.Left, 1, DimensionHelper.PopupMargin),
				NSLayoutConstraint.Create(CancelButton, NSLayoutAttribute.Right, NSLayoutRelation.Equal, this,
					NSLayoutAttribute.CenterX, 1, -DimensionHelper.PopupMargin / 2),
				NSLayoutConstraint.Create(CancelButton, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _lbConfirmLogout,
					NSLayoutAttribute.Bottom, 1, DimensionHelper.PopupLogoutMargin),

				//Confirm button
				NSLayoutConstraint.Create(ConfirmButton, NSLayoutAttribute.Right, NSLayoutRelation.Equal, this,
					NSLayoutAttribute.Right, 1, -DimensionHelper.PopupMargin),
				NSLayoutConstraint.Create(ConfirmButton, NSLayoutAttribute.Left, NSLayoutRelation.Equal, this,
					NSLayoutAttribute.CenterX, 1, DimensionHelper.PopupMargin / 2),
				NSLayoutConstraint.Create(ConfirmButton, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _lbConfirmLogout,
					NSLayoutAttribute.Bottom, 1, DimensionHelper.PopupLogoutMargin),

				NSLayoutConstraint.Create(this, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal,
					CancelButton, NSLayoutAttribute.Bottom, 1, DimensionHelper.PopupButtonMargin),
			});
		}

		private void CreateBinding(MvxFluentBindingDescriptionSet<MasterView, MasterViewModel> bindingSet)
		{
			bindingSet.Bind(_lbConfirm).To(vm => vm.Confirm);
			bindingSet.Bind(CancelButton).For("Title").To(vm => vm.CancelPopup);
			bindingSet.Bind(CancelButton)
				.For(v => v.ClickCommand)
				.To(vm => vm.CancelLogoutCommand);
			bindingSet.Bind(ConfirmButton).For("Title").To(vm => vm.Confirm);
			bindingSet.Bind(ConfirmButton)
				.For(v => v.ClickCommand)
				.To(vm => vm.ConfirmLogoutCommand);

			bindingSet.Apply();
		}


		private void InitUIElements()
		{
			//Instruction
			_lbConfirm = UIHelper.CreateAlphaLabel(ColorHelper.Black, DimensionHelper.BigTextSize, true);
			_lbConfirmLogout = UIHelper.CreateAlphaLabel(ColorHelper.Black, DimensionHelper.MediumTextSize);
			_lbConfirmLogout.Text = Localizer.GetText("ConfirmLogout");

			//Cancel popup button
			CancelButton = UIHelper.CreateButton(0, DimensionHelper.PersonalInfoEditTextHeight,
				ColorHelper.LightGray, DimensionHelper.MediumTextSize, ColorHelper.BorderGray, false, true, true);

			//Confirm button
			ConfirmButton = UIHelper.CreateButton(0, DimensionHelper.PersonalInfoEditTextHeight,
				ColorHelper.White, DimensionHelper.MediumTextSize, ColorHelper.Blue, false, true, true);
		}
	}
}