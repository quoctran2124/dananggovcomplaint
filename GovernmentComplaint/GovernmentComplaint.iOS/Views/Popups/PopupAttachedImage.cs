using GovernmentComplaint.Core.ViewModels.Bases;
using GovernmentComplaint.iOS.Controls;
using GovernmentComplaint.iOS.Helpers;
using GovernmentComplaint.iOS.Views.Bases;
using MvvmCross.Binding.BindingContext;
using UIKit;

namespace GovernmentComplaint.iOS.Views.Popups
{
    public class PopupAttachedImage : PopupLayout
    {
        private CustomImageView _image;
        public AlphaImageView IvCloseButton;

        public PopupAttachedImage(MvxFluentBindingDescriptionSet<MasterView, MasterViewModel> bindingSet)
        {
            CreateBinding(bindingSet);
        }

        protected override void InitView()
        {
            base.InitView();
            InitUiElements();

            AddSubview(IvCloseButton);
            AddSubview(_image);

            AddConstraints(new[]
            {
                NSLayoutConstraint.Create(_image, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, this,
                    NSLayoutAttribute.CenterX, 1, 0),
                NSLayoutConstraint.Create(_image, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, this,
                    NSLayoutAttribute.CenterY, 1, 0),
                NSLayoutConstraint.Create(_image, NSLayoutAttribute.Top, NSLayoutRelation.Equal, IvCloseButton,
                    NSLayoutAttribute.Bottom, 1, 0),

                NSLayoutConstraint.Create(_image, NSLayoutAttribute.Width, NSLayoutRelation.Equal, null,
                    NSLayoutAttribute.NoAttribute, 1, ResolutionHelper.Width - DimensionHelper.MainMenuHeight),
                NSLayoutConstraint.Create(_image, NSLayoutAttribute.Height, NSLayoutRelation.Equal, null,
                    NSLayoutAttribute.NoAttribute, 1, ResolutionHelper.Height - 100),

                NSLayoutConstraint.Create(IvCloseButton, NSLayoutAttribute.Top, NSLayoutRelation.Equal, this,
                    NSLayoutAttribute.Top, 1, 5),
                NSLayoutConstraint.Create(IvCloseButton, NSLayoutAttribute.Right, NSLayoutRelation.Equal, this,
                    NSLayoutAttribute.Right, 1, -5),
                NSLayoutConstraint.Create(_image, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal,
                    this, NSLayoutAttribute.Bottom, 1, -10),
                NSLayoutConstraint.Create(this, NSLayoutAttribute.Height, NSLayoutRelation.Equal,
                    null, NSLayoutAttribute.NoAttribute, 1, ResolutionHelper.Height - DimensionHelper.MainMenuHeight)
            });
        }

        private void CreateBinding(MvxFluentBindingDescriptionSet<MasterView, MasterViewModel> bindingSet)
        {
            bindingSet.Bind(_image).For("ImageUrl").To(vm => vm.AttachedImageUrl);
            bindingSet.Apply();
        }

        private void InitUiElements()
        {
            _image = UIHelper.CreateCustomImageView(0, 0);
            IvCloseButton = UIHelper.CreateAlphaImageView(DimensionHelper.DropdownIconSize,
                DimensionHelper.DropdownIconSize, 0);
            IvCloseButton.Image = UIImage.FromFile(ImageHelper.Remove);
        }
    }
}