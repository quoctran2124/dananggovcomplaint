using GovernmentComplaint.Core.ViewModels.Bases;
using GovernmentComplaint.iOS.Controls;
using GovernmentComplaint.iOS.Helpers;
using GovernmentComplaint.iOS.Views.Bases;
using MvvmCross.Binding.BindingContext;
using UIKit;

namespace GovernmentComplaint.iOS.Views.Popups
{
	public class PopupAnswerFeedback : PopupLayout
	{
		private UILabel _lbPopupContent;
		private UILabel _lbPopupHeader;
		public AlphaUIButton ConfirmButton;
		public AlphaUIButton CancelButton;

	    public AlphaUIButton SatisfiedView;
        public UIImageView SatisfiedCheckbox;
        private UILabel _satisfiedLabel;

        public AlphaUIButton UnSatisfiedView;
	    public UIImageView UnSatisfiedCheckbox;
	    private UILabel _unSatisfiedLabel;

	    public DisappearableVerticalUiView UnSatisfiedOptionLayout;

	    public AlphaUIButton SlowResponseView;
	    public UIImageView SlowResponseCheckbox;
	    private UILabel _slowResponseLabel;

	    public AlphaUIButton UnSuitAnswerView;
	    public UIImageView UnSuitAnswerCheckbox;
	    private UILabel _unSuitAnswerLabel;

	    public DisappearableVerticalUiView ErrorMessageLayout;
        private UILabel _lbErrorMessage;

        public PopupAnswerFeedback(MvxFluentBindingDescriptionSet<MasterView, MasterViewModel> bindingSet)
		{
			CreateBinding(bindingSet);
		}

		protected override void InitView()
		{
			base.InitView();
			InitUiElements();

		    SatisfiedLayout();
		    UnSatisfiedLayout();
		    UnSatisfiedContainer();

            AddSubview(_lbPopupHeader);
			AddSubview(CancelButton);
			AddSubview(ConfirmButton);

		    AddSubview(SatisfiedView);
		    AddSubview(UnSatisfiedView);
		    AddSubview(UnSatisfiedOptionLayout);

            AddConstraints(new[]
			{
				//Confirm
				NSLayoutConstraint.Create(_lbPopupHeader, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, this,
					NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_lbPopupHeader, NSLayoutAttribute.Top, NSLayoutRelation.Equal, this,
					NSLayoutAttribute.Top, 1, 2 * DimensionHelper.PopupMargin),

				//Instruction
			    NSLayoutConstraint.Create(SatisfiedView, NSLayoutAttribute.Top, NSLayoutRelation.Equal,_lbPopupHeader, NSLayoutAttribute.Bottom, 1, DimensionHelper.BiggerTextSize),
                NSLayoutConstraint.Create(SatisfiedView, NSLayoutAttribute.Left, NSLayoutRelation.Equal, this, NSLayoutAttribute.Left, 1, DimensionHelper.BiggerTextSize),
                NSLayoutConstraint.Create(SatisfiedView, NSLayoutAttribute.Right, NSLayoutRelation.Equal, this, NSLayoutAttribute.Right, 1, -DimensionHelper.BiggerTextSize),

                NSLayoutConstraint.Create(UnSatisfiedView, NSLayoutAttribute.Top, NSLayoutRelation.Equal,SatisfiedView, NSLayoutAttribute.Bottom, 1, DimensionHelper.ContentPadding),
                NSLayoutConstraint.Create(UnSatisfiedView, NSLayoutAttribute.Left, NSLayoutRelation.Equal, this, NSLayoutAttribute.Left, 1, DimensionHelper.BiggerTextSize),
			    NSLayoutConstraint.Create(UnSatisfiedView, NSLayoutAttribute.Right, NSLayoutRelation.Equal, this, NSLayoutAttribute.Right, 1, -DimensionHelper.BiggerTextSize),


                NSLayoutConstraint.Create(UnSatisfiedOptionLayout, NSLayoutAttribute.Top, NSLayoutRelation.Equal,UnSatisfiedView, NSLayoutAttribute.Bottom, 1, DimensionHelper.ContentPadding),
			    NSLayoutConstraint.Create(UnSatisfiedOptionLayout, NSLayoutAttribute.Left, NSLayoutRelation.Equal, this, NSLayoutAttribute.Left, 1, DimensionHelper.BiggerTextSize),
			    NSLayoutConstraint.Create(UnSatisfiedOptionLayout, NSLayoutAttribute.Right, NSLayoutRelation.Equal, this, NSLayoutAttribute.Right, 1, -DimensionHelper.BiggerTextSize),

				//Cancel popup button
				NSLayoutConstraint.Create(CancelButton, NSLayoutAttribute.Left, NSLayoutRelation.Equal, this,
					NSLayoutAttribute.Left, 1, DimensionHelper.PopupMargin),
				NSLayoutConstraint.Create(CancelButton, NSLayoutAttribute.Right, NSLayoutRelation.Equal, this,
					NSLayoutAttribute.CenterX, 1, -DimensionHelper.PopupMargin / 2),
				NSLayoutConstraint.Create(CancelButton, NSLayoutAttribute.Top, NSLayoutRelation.Equal, UnSatisfiedOptionLayout,
					NSLayoutAttribute.Bottom, 1, DimensionHelper.PopupLogoutMargin),

				//Confirm button
				NSLayoutConstraint.Create(ConfirmButton, NSLayoutAttribute.Right, NSLayoutRelation.Equal, this,
					NSLayoutAttribute.Right, 1, -DimensionHelper.PopupMargin),
				NSLayoutConstraint.Create(ConfirmButton, NSLayoutAttribute.Left, NSLayoutRelation.Equal, this,
					NSLayoutAttribute.CenterX, 1, DimensionHelper.PopupMargin / 2),
				NSLayoutConstraint.Create(ConfirmButton, NSLayoutAttribute.Top, NSLayoutRelation.Equal, UnSatisfiedOptionLayout,
					NSLayoutAttribute.Bottom, 1, DimensionHelper.PopupLogoutMargin),
				NSLayoutConstraint.Create(this, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal,
					ConfirmButton, NSLayoutAttribute.Bottom, 1, DimensionHelper.PopupButtonMargin)
			});
		}

	    private void SatisfiedLayout()
	    {
	        SatisfiedView = UIHelper.CreateButton(0, DimensionHelper.PersonalInfoEditTextHeight,
	            ColorHelper.Overlay, DimensionHelper.MediumTextSize, ColorHelper.LightestGray, false, true, true);

            SatisfiedCheckbox = UIHelper.CreateImageView(DimensionHelper.MediumMenuIconHeight, DimensionHelper.MediumMenuIconHeight);
	        SatisfiedCheckbox.Image = UIImage.FromFile(ImageHelper.RadioButtonChecked);
	        SatisfiedCheckbox.UserInteractionEnabled = false;

	        _satisfiedLabel = UIHelper.CreateAlphaLabel(ColorHelper.Black, DimensionHelper.MediumTextSize);
	        _satisfiedLabel.UserInteractionEnabled = false;

            SatisfiedView.Add(SatisfiedCheckbox);
	        SatisfiedView.Add(_satisfiedLabel);
	        SatisfiedView.AddConstraints(new []
	        {
	            NSLayoutConstraint.Create(SatisfiedCheckbox, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, SatisfiedView, NSLayoutAttribute.CenterY, 1, 0),
	            NSLayoutConstraint.Create(SatisfiedCheckbox, NSLayoutAttribute.Left, NSLayoutRelation.Equal, SatisfiedView, NSLayoutAttribute.Left, 1, DimensionHelper.MediumContentMargin),

	            NSLayoutConstraint.Create(_satisfiedLabel, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, SatisfiedView, NSLayoutAttribute.CenterY, 1, 0),
	            NSLayoutConstraint.Create(_satisfiedLabel, NSLayoutAttribute.Left, NSLayoutRelation.Equal, SatisfiedCheckbox, NSLayoutAttribute.Right, 1, DimensionHelper.MediumContentMargin),
            });
        }

	    private void UnSatisfiedLayout()
        { 
	        UnSatisfiedView = UIHelper.CreateButton(0, DimensionHelper.PersonalInfoEditTextHeight,
	            ColorHelper.Overlay, DimensionHelper.MediumTextSize, ColorHelper.LightestGray, false, true, true);

            UnSatisfiedCheckbox = UIHelper.CreateImageView(DimensionHelper.MediumMenuIconHeight, DimensionHelper.MediumMenuIconHeight);
	        UnSatisfiedCheckbox.Image = UIImage.FromFile(ImageHelper.RadioButtonUnChecked);
	        UnSatisfiedCheckbox.UserInteractionEnabled = false;

	        _unSatisfiedLabel = UIHelper.CreateAlphaLabel(ColorHelper.Black, DimensionHelper.MediumTextSize);
	        _unSatisfiedLabel.UserInteractionEnabled = false;

            UnSatisfiedView.Add(UnSatisfiedCheckbox);
	        UnSatisfiedView.Add(_unSatisfiedLabel);
	        UnSatisfiedView.AddConstraints(new[]
	        {
	            NSLayoutConstraint.Create(UnSatisfiedCheckbox, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, UnSatisfiedView, NSLayoutAttribute.CenterY, 1, 0),
	            NSLayoutConstraint.Create(UnSatisfiedCheckbox, NSLayoutAttribute.Left, NSLayoutRelation.Equal, UnSatisfiedView, NSLayoutAttribute.Left, 1, DimensionHelper.MediumContentMargin),

	            NSLayoutConstraint.Create(_unSatisfiedLabel, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, UnSatisfiedView, NSLayoutAttribute.CenterY, 1, 0),
	            NSLayoutConstraint.Create(_unSatisfiedLabel, NSLayoutAttribute.Left, NSLayoutRelation.Equal, UnSatisfiedCheckbox, NSLayoutAttribute.Right, 1, DimensionHelper.MediumContentMargin),
            });
	    }

	    private void UnSatisfiedContainer()
	    {
	        UnSatisfiedOptionLayout = UIHelper.CreateDisappearableVerticalView(DimensionHelper.UnSatisfiedContainerHeight, ColorHelper.LightestGray);

            SlowResponseLayout();
	        UnSuitAnswerLayout();
	        ErrorMessageContainer();

            UnSatisfiedOptionLayout.Add(SlowResponseView);
	        UnSatisfiedOptionLayout.Add(UnSuitAnswerView);
	        UnSatisfiedOptionLayout.Add(ErrorMessageLayout);
	        UnSatisfiedOptionLayout.AddConstraints(new []
            {
                NSLayoutConstraint.Create(SlowResponseView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, UnSatisfiedOptionLayout, NSLayoutAttribute.Top, 1, 0),
                NSLayoutConstraint.Create(SlowResponseView, NSLayoutAttribute.Left, NSLayoutRelation.Equal, UnSatisfiedOptionLayout, NSLayoutAttribute.Left, 1, DimensionHelper.BiggestTextSize),
                NSLayoutConstraint.Create(SlowResponseView, NSLayoutAttribute.Right, NSLayoutRelation.Equal, UnSatisfiedOptionLayout, NSLayoutAttribute.Right, 1, -DimensionHelper.BiggerTextSize),

                NSLayoutConstraint.Create(UnSuitAnswerView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, SlowResponseView, NSLayoutAttribute.Bottom, 1, 0),
                NSLayoutConstraint.Create(UnSuitAnswerView, NSLayoutAttribute.Left, NSLayoutRelation.Equal, UnSatisfiedOptionLayout, NSLayoutAttribute.Left, 1, DimensionHelper.BiggestTextSize),
                NSLayoutConstraint.Create(UnSuitAnswerView, NSLayoutAttribute.Right, NSLayoutRelation.Equal, UnSatisfiedOptionLayout, NSLayoutAttribute.Right, 1, -DimensionHelper.BiggerTextSize),

                NSLayoutConstraint.Create(ErrorMessageLayout, NSLayoutAttribute.Top, NSLayoutRelation.Equal, UnSuitAnswerView, NSLayoutAttribute.Bottom, 1, DimensionHelper.BiggerTextSize),
                NSLayoutConstraint.Create(ErrorMessageLayout, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, UnSatisfiedOptionLayout, NSLayoutAttribute.CenterX, 1, 0),
            });
        }

	    private void SlowResponseLayout()
	    {
	        SlowResponseView = UIHelper.CreateButton(0, DimensionHelper.PersonalInfoEditTextHeight,
	            ColorHelper.Overlay, DimensionHelper.MediumTextSize, ColorHelper.LightestGray, false, true, true);


	        SlowResponseCheckbox = UIHelper.CreateImageView(DimensionHelper.MediumMenuIconHeight, DimensionHelper.MediumMenuIconHeight);
	        SlowResponseCheckbox.Image = UIImage.FromFile(ImageHelper.CheckboxUnChecked);
	        SlowResponseCheckbox.UserInteractionEnabled = false;

	        _slowResponseLabel = UIHelper.CreateAlphaLabel(ColorHelper.Black, DimensionHelper.MediumTextSize);
	        _slowResponseLabel.UserInteractionEnabled = false;

            SlowResponseView.Add(SlowResponseCheckbox);
	        SlowResponseView.Add(_slowResponseLabel);
	        SlowResponseView.AddConstraints(new[]
	        {
	            NSLayoutConstraint.Create(SlowResponseCheckbox, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, SlowResponseView, NSLayoutAttribute.CenterY, 1, 0),
	            NSLayoutConstraint.Create(SlowResponseCheckbox, NSLayoutAttribute.Left, NSLayoutRelation.Equal, SlowResponseView, NSLayoutAttribute.Left, 1, DimensionHelper.MediumContentMargin),

	            NSLayoutConstraint.Create(_slowResponseLabel, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, SlowResponseView, NSLayoutAttribute.CenterY, 1, 0),
	            NSLayoutConstraint.Create(_slowResponseLabel, NSLayoutAttribute.Left, NSLayoutRelation.Equal, SlowResponseCheckbox, NSLayoutAttribute.Right, 1, DimensionHelper.MediumContentMargin),
	        });
        }

	    private void UnSuitAnswerLayout()
	    {
	        UnSuitAnswerView = UIHelper.CreateButton(0, DimensionHelper.PersonalInfoEditTextHeight,
	            ColorHelper.Overlay, DimensionHelper.MediumTextSize, ColorHelper.LightestGray, false, true, true);

	        UnSuitAnswerCheckbox = UIHelper.CreateImageView(DimensionHelper.MediumMenuIconHeight, DimensionHelper.MediumMenuIconHeight);
	        UnSuitAnswerCheckbox.Image = UIImage.FromFile(ImageHelper.CheckboxUnChecked);
	        UnSuitAnswerCheckbox.UserInteractionEnabled = false;

	        _unSuitAnswerLabel = UIHelper.CreateAlphaLabel(ColorHelper.Black, DimensionHelper.MediumTextSize);
	        _unSuitAnswerLabel.UserInteractionEnabled = false;

            UnSuitAnswerView.Add(UnSuitAnswerCheckbox);
	        UnSuitAnswerView.Add(_unSuitAnswerLabel);
	        UnSuitAnswerView.AddConstraints(new[]
	        {
	            NSLayoutConstraint.Create(UnSuitAnswerCheckbox, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, UnSuitAnswerView, NSLayoutAttribute.CenterY, 1, 0),
	            NSLayoutConstraint.Create(UnSuitAnswerCheckbox, NSLayoutAttribute.Left, NSLayoutRelation.Equal, UnSuitAnswerView, NSLayoutAttribute.Left, 1, DimensionHelper.MediumContentMargin),

	            NSLayoutConstraint.Create(_unSuitAnswerLabel, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, UnSuitAnswerView, NSLayoutAttribute.CenterY, 1, 0),
	            NSLayoutConstraint.Create(_unSuitAnswerLabel, NSLayoutAttribute.Left, NSLayoutRelation.Equal, UnSuitAnswerCheckbox, NSLayoutAttribute.Right, 1, DimensionHelper.MediumContentMargin),
	        });
	    }

	    private void ErrorMessageContainer()
	    {
	        ErrorMessageLayout = UIHelper.CreateDisappearableVerticalView(DimensionHelper.MediumMenuIconHeight, ColorHelper.LightestGray);
	        _lbErrorMessage = UIHelper.CreateAlphaLabel(ColorHelper.Red, DimensionHelper.MediumTextSize);

            ErrorMessageLayout.Add(_lbErrorMessage);
	        ErrorMessageLayout.AddConstraints(new []
	        {
	            NSLayoutConstraint.Create(_lbErrorMessage, NSLayoutAttribute.Top, NSLayoutRelation.Equal, ErrorMessageLayout, NSLayoutAttribute.Top, 1, 0),
	            NSLayoutConstraint.Create(_lbErrorMessage, NSLayoutAttribute.Left, NSLayoutRelation.Equal, ErrorMessageLayout, NSLayoutAttribute.Left, 1, 0),
	            NSLayoutConstraint.Create(_lbErrorMessage, NSLayoutAttribute.Right, NSLayoutRelation.Equal, ErrorMessageLayout, NSLayoutAttribute.Right, 1, 0),
            });
        }
        private void CreateBinding(MvxFluentBindingDescriptionSet<MasterView, MasterViewModel> bindingSet)
		{
			bindingSet.Bind(_lbPopupHeader).To(vm => vm.AnswerFeedback);
			bindingSet.Bind(ConfirmButton).For("Title").To(vm => vm.Confirm);
			bindingSet.Bind(CancelButton).For("Title").To(vm => vm.CancelPopup);
			bindingSet.Bind(_lbPopupContent).To(vm => vm.NoWifiInstruction); 

		    bindingSet.Bind(_satisfiedLabel).To(vm => vm.SatisfyText);
		    bindingSet.Bind(_unSatisfiedLabel).To(vm => vm.UnsatisfyText);
		    bindingSet.Bind(_slowResponseLabel).To(vm => vm.SlowResponse);
		    bindingSet.Bind(_unSuitAnswerLabel).To(vm => vm.UnsuitAnswer);
		    bindingSet.Bind(_lbErrorMessage).To(vm => vm.AnswerFeedbackErrorMessage);
		    
            bindingSet.Apply();
		}


		private void InitUiElements()
		{
			//Instruction
			_lbPopupHeader = UIHelper.CreateAlphaLabel(ColorHelper.Black, DimensionHelper.BigTextSize, true);
			_lbPopupContent = UIHelper.CreateAlphaLabel(ColorHelper.Black, DimensionHelper.MediumTextSize);


			//Cancel popup button
			CancelButton = UIHelper.CreateButton(0, DimensionHelper.PersonalInfoEditTextHeight,
				ColorHelper.Black, DimensionHelper.MediumTextSize, ColorHelper.BorderGray, false, true, true);

			//Confirm button
			ConfirmButton = UIHelper.CreateButton(0, DimensionHelper.PersonalInfoEditTextHeight,
				ColorHelper.White, DimensionHelper.MediumTextSize, ColorHelper.Blue, false, true, true);
        }
	}
}