using System.Windows.Input;
using GovernmentComplaint.Core.ViewModels.Bases;
using GovernmentComplaint.iOS.Controls;
using GovernmentComplaint.iOS.Helpers;
using GovernmentComplaint.iOS.Views.Bases;
using MvvmCross.Binding.BindingContext;
using UIKit;

namespace GovernmentComplaint.iOS.Views.Popups
{
	public class PopupRating : PopupLayout
	{
		private UILabel _lbRatingInstruction;
		private CustomRatingBar _ratingBar;
		public AlphaUIButton ConfirmButton;
		public AlphaUIButton CancelButton;
		public ICommand UserRatingCommand { get; set; }

		public PopupRating(MvxFluentBindingDescriptionSet<MasterView, MasterViewModel> bindingSet)
		{
			CreateBinding(bindingSet);
		}

		protected override void InitView()
		{
			base.InitView();
			InitUIElements();

			AddSubview(_lbRatingInstruction);
			AddSubview(_ratingBar);
			AddSubview(CancelButton);
			AddSubview(ConfirmButton);

			AddConstraints(new[]
			{
				//Instruction
				NSLayoutConstraint.Create(_lbRatingInstruction, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, this,
					NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_lbRatingInstruction, NSLayoutAttribute.Top, NSLayoutRelation.Equal, this,
					NSLayoutAttribute.Top, 1, 2 * DimensionHelper.PopupMargin),

				//Feedback Rating
				NSLayoutConstraint.Create(_ratingBar, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, this,
					NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_ratingBar, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_lbRatingInstruction,
					NSLayoutAttribute.Bottom, 1, 0),

				NSLayoutConstraint.Create(CancelButton, NSLayoutAttribute.Left, NSLayoutRelation.Equal, this,
					NSLayoutAttribute.Left, 1, DimensionHelper.PopupMargin),
				NSLayoutConstraint.Create(CancelButton, NSLayoutAttribute.Right, NSLayoutRelation.Equal, this,
					NSLayoutAttribute.CenterX, 1, -DimensionHelper.PopupMargin / 2),
				NSLayoutConstraint.Create(CancelButton, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _ratingBar,
					NSLayoutAttribute.Bottom, 1, DimensionHelper.PopupButtonMargin),

				NSLayoutConstraint.Create(ConfirmButton, NSLayoutAttribute.Right, NSLayoutRelation.Equal, this,
					NSLayoutAttribute.Right, 1, -DimensionHelper.PopupMargin),
				NSLayoutConstraint.Create(ConfirmButton, NSLayoutAttribute.Left, NSLayoutRelation.Equal, this,
					NSLayoutAttribute.CenterX, 1, DimensionHelper.PopupMargin / 2),
				NSLayoutConstraint.Create(ConfirmButton, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _ratingBar,
					NSLayoutAttribute.Bottom, 1, DimensionHelper.PopupButtonMargin),
				NSLayoutConstraint.Create(this, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal,
					ConfirmButton, NSLayoutAttribute.Bottom, 1, DimensionHelper.PopupButtonMargin)
			});
		}

		private void CreateBinding(MvxFluentBindingDescriptionSet<MasterView, MasterViewModel> bindingSet)
		{
			bindingSet.Bind(_lbRatingInstruction).To(vm => vm.RatingText);
			bindingSet.Bind(_ratingBar).For(v => v.Rating).To(vm => vm.ComplaintDetailViewModel.UserRating).TwoWay();
			bindingSet.Bind(this)
				.For(v => v.UserRatingCommand)
				.To(vm => vm.ComplaintDetailViewModel.UserRatingCommandIos);
			bindingSet.Bind(CancelButton).For("Title").To(vm => vm.CancelPopup);
			bindingSet.Bind(ConfirmButton).For("Title").To(vm => vm.Confirm);
			bindingSet.Bind(ConfirmButton)
				.For(v => v.ClickCommand)
				.To(vm => vm.ComplaintDetailViewModel.UserRatingCommand);
			bindingSet.Apply();
		}

		private void InitUIElements()
		{
			_lbRatingInstruction = UIHelper.CreateAlphaLabel(ColorHelper.Black, DimensionHelper.BigTextSize);
			_ratingBar = UIHelper.CreateCustomRatingBar(DimensionHelper.RatingbarWidth, DimensionHelper.RatingbarHeight);
			_ratingBar.UserInteractionEnabled = true;
			_ratingBar.RatingChosen += (sender, e) => { UserRatingCommand.Execute(e.Rating); };
			CancelButton = UIHelper.CreateButton(0, DimensionHelper.PersonalInfoEditTextHeight,
				ColorHelper.LightGray, DimensionHelper.MediumTextSize, ColorHelper.BorderGray, false, true, true);
			ConfirmButton = UIHelper.CreateButton(0, DimensionHelper.PersonalInfoEditTextHeight,
				ColorHelper.White, DimensionHelper.MediumTextSize, ColorHelper.Blue, false, true, true);
		}
	}
}