﻿using System;
using Foundation;
using GovernmentComplaint.Core.ViewModels.Bases;
using GovernmentComplaint.iOS.Controls;
using GovernmentComplaint.iOS.Helpers;
using GovernmentComplaint.iOS.Views.Bases;
using MvvmCross.Binding.BindingContext;
using UIKit;

namespace GovernmentComplaint.iOS.Views.Popups
{
	public class PopupVideo : PopupLayout
	{
		private NSObject _didBecomeActiveNotificationObserver;
		private NSObject _willResignActiveNotificationObserver;
		private bool _isKeyboardShown;
		private nfloat _keyboardHeight;
		private UILabel _lbVideoInstruction;
		private CustomTextView _tfVideoLink;
		public AlphaUIButton CancelPopupButton;
		public AlphaUIButton ConfirmButton;
		private UIView _videoLinkLayout;

		public PopupVideo(MvxFluentBindingDescriptionSet<MasterView, MasterViewModel> bindingSet)
		{
			CreateBinding(bindingSet);
		}

		protected override void InitView()
		{
			base.InitView();
			InitUIElements();

			AddSubview(_lbVideoInstruction);
			AddSubview(_videoLinkLayout);
			AddSubview(CancelPopupButton);
			AddSubview(ConfirmButton);

			AddConstraints(new[]
			{
				//Instruction
				NSLayoutConstraint.Create(_lbVideoInstruction, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, this,
					NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_lbVideoInstruction, NSLayoutAttribute.Top, NSLayoutRelation.Equal, this,
					NSLayoutAttribute.Top, 1, 2 * DimensionHelper.PopupMargin),

				//Text field
				NSLayoutConstraint.Create(_videoLinkLayout, NSLayoutAttribute.Left, NSLayoutRelation.Equal, this,
					NSLayoutAttribute.Left, 1, DimensionHelper.PopupMargin),
				NSLayoutConstraint.Create(_videoLinkLayout, NSLayoutAttribute.Right, NSLayoutRelation.Equal, this,
					NSLayoutAttribute.Right, 1, -DimensionHelper.PopupMargin),
				NSLayoutConstraint.Create(_videoLinkLayout, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_lbVideoInstruction, NSLayoutAttribute.Bottom, 1, 2 * DimensionHelper.PopupMargin),
				NSLayoutConstraint.Create(_videoLinkLayout, NSLayoutAttribute.Height, NSLayoutRelation.Equal, null,
					NSLayoutAttribute.NoAttribute, 1, DimensionHelper.PersonalInfoEditTextHeight - 10),

				//Cancel popup button
				NSLayoutConstraint.Create(CancelPopupButton, NSLayoutAttribute.Left, NSLayoutRelation.Equal, this,
					NSLayoutAttribute.Left, 1, DimensionHelper.PopupMargin),
				NSLayoutConstraint.Create(CancelPopupButton, NSLayoutAttribute.Right, NSLayoutRelation.Equal, this,
					NSLayoutAttribute.CenterX, 1, -DimensionHelper.PopupMargin / 2),
				NSLayoutConstraint.Create(CancelPopupButton, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _videoLinkLayout,
					NSLayoutAttribute.Bottom, 1, DimensionHelper.PopupLogoutMargin),

				//Confirm button
				NSLayoutConstraint.Create(ConfirmButton, NSLayoutAttribute.Right, NSLayoutRelation.Equal, this,
					NSLayoutAttribute.Right, 1, -DimensionHelper.PopupMargin),
				NSLayoutConstraint.Create(ConfirmButton, NSLayoutAttribute.Left, NSLayoutRelation.Equal, this,
					NSLayoutAttribute.CenterX, 1, DimensionHelper.PopupMargin / 2),
				NSLayoutConstraint.Create(ConfirmButton, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _videoLinkLayout,
					NSLayoutAttribute.Bottom, 1, DimensionHelper.PopupLogoutMargin),
				NSLayoutConstraint.Create(this, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal,
					ConfirmButton, NSLayoutAttribute.Bottom, 1, DimensionHelper.PopupButtonMargin)
			});

			//_willResignActiveNotificationObserver =
			//    NSNotificationCenter.DefaultCenter.AddObserver(UIKeyboard.WillHideNotification, OnWillHideKeyboard);
			//_didBecomeActiveNotificationObserver =
			//    NSNotificationCenter.DefaultCenter.AddObserver(UIKeyboard.WillShowNotification, OnWillShowKeyboard);
		}

		private void RemoveObservers()
		{
			NSNotificationCenter.DefaultCenter.RemoveObserver(_willResignActiveNotificationObserver);
			NSNotificationCenter.DefaultCenter.RemoveObserver(_didBecomeActiveNotificationObserver);
		}

		//private void OnWillShowKeyboard(NSNotification notification)
		//{
		//    var visible = notification.Name == UIKeyboard.WillShowNotification;
		//    var keyboardFrame = visible
		//        ? UIKeyboard.FrameEndFromNotification(notification)
		//        : UIKeyboard.FrameBeginFromNotification(notification);
		//    if (!IsKeyboardShown)
		//    {
		//        IsKeyboardShown = true;
		//        var keyboardHeight = keyboardFrame.Height / 2;
		//        KeyboardHeight = keyboardHeight;
		//        var f = Frame;
		//        f.Y -= keyboardHeight;
		//        Frame = f;
		//    }
		//}

		//private void OnWillHideKeyboard(NSNotification obj)
		//{
		//    IsKeyboardShown = false;
		//    var f = Frame;
		//    f.Y += KeyboardHeight;
		//    Frame = f;
		//}

		private void CreateBinding(MvxFluentBindingDescriptionSet<MasterView, MasterViewModel> bindingSet)
		{
			bindingSet.Bind(_lbVideoInstruction).To(vm => vm.VideoInstruction);
			bindingSet.Bind(_tfVideoLink)
				.For(v => v.ClickCommand)
				.To(vm => vm.CreateNewComplaintViewModel.VideoLinkTouchCommand);
			bindingSet.Bind(_tfVideoLink.TextView).For("Text").To(vm => vm.CreateNewComplaintViewModel.EnteredVideoLink);
			bindingSet.Bind(_tfVideoLink).For(v => v.IsError).To(vm => vm.CreateNewComplaintViewModel.IsVideoLinkWrong);
			bindingSet.Bind(_tfVideoLink).For(v => v.PlaceHolder).To(vm => vm.CreateNewComplaintViewModel.VideoLinkHint);
			bindingSet.Bind(ConfirmButton).For("Title").To(vm => vm.Confirm);
			bindingSet.Bind(ConfirmButton)
				.For(v => v.ClickCommand)
				.To(vm => vm.CreateNewComplaintViewModel.ConfirmVideoLinkCommand);
			bindingSet.Bind(CancelPopupButton).For("Title").To(vm => vm.CancelPopup);
			bindingSet.Apply();
		}

		private void InitUIElements()
		{
			_lbVideoInstruction = UIHelper.CreateAlphaLabel(ColorHelper.Black, DimensionHelper.BigTextSize);
			_lbVideoInstruction.UserInteractionEnabled = false;
			_tfVideoLink = new CustomTextView();
			_tfVideoLink.Layer.BorderColor = UIColor.Red.CGColor;
			_videoLinkLayout = UIHelper.CreateView(0, DimensionHelper.PersonalInfoEditTextHeight - 10, UIColor.White, true,
				true);

			_videoLinkLayout.Add(_tfVideoLink);
			_videoLinkLayout.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_tfVideoLink, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _videoLinkLayout,
					NSLayoutAttribute.Top, 1, 2),
				NSLayoutConstraint.Create(_tfVideoLink, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _videoLinkLayout,
					NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_tfVideoLink, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _videoLinkLayout,
					NSLayoutAttribute.Right, 1, 0),
				NSLayoutConstraint.Create(_tfVideoLink, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _videoLinkLayout,
					NSLayoutAttribute.Bottom, 1, 0),
			});

			_videoLinkLayout.Layer.CornerRadius = DimensionHelper.RoundCorner;

			CancelPopupButton = UIHelper.CreateButton(0, DimensionHelper.PersonalInfoEditTextHeight,
				ColorHelper.LightGray, DimensionHelper.MediumTextSize, ColorHelper.BorderGray, false, true, true);

			ConfirmButton = UIHelper.CreateButton(0, DimensionHelper.PersonalInfoEditTextHeight,
				ColorHelper.White, DimensionHelper.MediumTextSize, ColorHelper.Blue, false, true, true);

			ConfirmButton.Clicked += (sender, args) =>
			{
				this.EndEditing(true);
				//RemoveObservers();
			};

			CancelPopupButton.Clicked += (sender, args) =>
			{
				this.EndEditing(true);
				//RemoveObservers();
			};

			AddGestureRecognizer(new UITapGestureRecognizer(() => { this.EndEditing(true); }));
		}
	}
}