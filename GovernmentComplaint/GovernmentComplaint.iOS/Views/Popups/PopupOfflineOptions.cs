﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using GovernmentComplaint.Core.ViewModels.Bases;
using GovernmentComplaint.iOS.Controls;
using GovernmentComplaint.iOS.Helpers;
using GovernmentComplaint.iOS.Views.Bases;
using MvvmCross.Binding.BindingContext;
using UIKit;

namespace GovernmentComplaint.iOS.Views.Popups
{
	public class PopupOfflineOptions : PopupLayout
	{
		private UILabel _lbPopupContent;
		private UILabel _lbPopupHeader;
		public AlphaLabel GoToWifiSettingsLink;
		public AlphaUIButton SaveToLocalButton;
		public AlphaUIButton CancelButton;

		public PopupOfflineOptions(MvxFluentBindingDescriptionSet<MasterView, MasterViewModel> bindingSet)
		{
			CreateBinding(bindingSet);
		}

		protected override void InitView()
		{
			base.InitView();
			InitUiElements();

			AddSubview(_lbPopupHeader);
			AddSubview(_lbPopupContent);
			AddSubview(GoToWifiSettingsLink);
			AddSubview(CancelButton);
			AddSubview(SaveToLocalButton);

			AddConstraints(new[]
			{
				//Confirm
				NSLayoutConstraint.Create(_lbPopupHeader, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, this,
					NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_lbPopupHeader, NSLayoutAttribute.Top, NSLayoutRelation.Equal, this,
					NSLayoutAttribute.Top, 1, 2 * DimensionHelper.PopupMargin),

				//Instruction
				NSLayoutConstraint.Create(_lbPopupContent, NSLayoutAttribute.Left, NSLayoutRelation.Equal, this,
					NSLayoutAttribute.Left, 1, DimensionHelper.LayoutMargin),
				NSLayoutConstraint.Create(_lbPopupContent, NSLayoutAttribute.Right, NSLayoutRelation.Equal, this,
					NSLayoutAttribute.Right, 1, -DimensionHelper.LayoutMargin),

				NSLayoutConstraint.Create(_lbPopupContent, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_lbPopupHeader,
					NSLayoutAttribute.Bottom, 1, DimensionHelper.PopupMargin),

				//text go to setting
				NSLayoutConstraint.Create(GoToWifiSettingsLink, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal,
					this,
					NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(GoToWifiSettingsLink, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_lbPopupContent,
					NSLayoutAttribute.Bottom, 1, DimensionHelper.PopupMargin),

				//Cancel popup button
				NSLayoutConstraint.Create(CancelButton, NSLayoutAttribute.Left, NSLayoutRelation.Equal, this,
					NSLayoutAttribute.Left, 1, DimensionHelper.PopupMargin),
				NSLayoutConstraint.Create(CancelButton, NSLayoutAttribute.Right, NSLayoutRelation.Equal, this,
					NSLayoutAttribute.CenterX, 1, -DimensionHelper.PopupMargin / 2),
				NSLayoutConstraint.Create(CancelButton, NSLayoutAttribute.Top, NSLayoutRelation.Equal, GoToWifiSettingsLink,
					NSLayoutAttribute.Bottom, 1, DimensionHelper.PopupLogoutMargin),

				//Confirm button
				NSLayoutConstraint.Create(SaveToLocalButton, NSLayoutAttribute.Right, NSLayoutRelation.Equal, this,
					NSLayoutAttribute.Right, 1, -DimensionHelper.PopupMargin),
				NSLayoutConstraint.Create(SaveToLocalButton, NSLayoutAttribute.Left, NSLayoutRelation.Equal, this,
					NSLayoutAttribute.CenterX, 1, DimensionHelper.PopupMargin / 2),
				NSLayoutConstraint.Create(SaveToLocalButton, NSLayoutAttribute.Top, NSLayoutRelation.Equal, GoToWifiSettingsLink,
					NSLayoutAttribute.Bottom, 1, DimensionHelper.PopupLogoutMargin),
				NSLayoutConstraint.Create(this, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal,
					SaveToLocalButton, NSLayoutAttribute.Bottom, 1, DimensionHelper.PopupButtonMargin)
			});
		}

		private void CreateBinding(MvxFluentBindingDescriptionSet<MasterView, MasterViewModel> bindingSet)
		{
			bindingSet.Bind(_lbPopupHeader).To(vm => vm.InformText);
			bindingSet.Bind(SaveToLocalButton).For("Title").To(vm => vm.SaveFeedBack);
			bindingSet.Bind(SaveToLocalButton).For(v => v.ClickCommand).To(vm => vm.SaveFeedbackCommand);
			bindingSet.Bind(CancelButton).For("Title").To(vm => vm.CancelPopup);
			bindingSet.Bind(_lbPopupContent).To(vm => vm.OfflineInstruction);
			bindingSet.Bind(GoToWifiSettingsLink).To(vm => vm.GoToWifiSetting);

			bindingSet.Apply();
		}


		private void InitUiElements()
		{
			//Instruction
			_lbPopupHeader = UIHelper.CreateAlphaLabel(ColorHelper.Black, DimensionHelper.BigTextSize, true);
			_lbPopupContent = UIHelper.CreateAlphaLabel(ColorHelper.Black, DimensionHelper.MediumTextSize, alignment:UITextAlignment.Center);
			_lbPopupContent.Lines = 2;

			GoToWifiSettingsLink = UIHelper.CreateAlphaLabel(ColorHelper.LightBlue, DimensionHelper.BigTextSize);

			//Cancel popup button
			CancelButton = UIHelper.CreateButton(0, DimensionHelper.PersonalInfoEditTextHeight,
				ColorHelper.LightGray, DimensionHelper.MediumTextSize, ColorHelper.BorderGray, false, true, true);

			//Confirm button
			SaveToLocalButton = UIHelper.CreateButton(0, DimensionHelper.PersonalInfoEditTextHeight,
				ColorHelper.White, DimensionHelper.MediumTextSize, ColorHelper.Blue, false, true, true);
		}
	}
}