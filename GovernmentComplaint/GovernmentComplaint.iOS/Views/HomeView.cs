using System;
using System.Windows.Input;
using CoreAnimation;
using CoreGraphics;
using Foundation;
using GovernmentComplaint.Core.ViewModels;
using GovernmentComplaint.iOS.Controls;
using GovernmentComplaint.iOS.Helpers;
using GovernmentComplaint.iOS.Views.Bases;
using GovernmentComplaint.iOS.Views.TableView.ItemTableViewSource;
using GovernmentComplaint.iOS.Views.TableView.ItemTableViewSources;
using MvvmCross.Binding.BindingContext;
using UIKit;

namespace GovernmentComplaint.iOS.Views
{
	public class HomeView : DetailView
	{
		private MvxUIRefreshControl _refreshControl;

		public int LastComplaintItemPosition { get; set; }

		private bool _moveListToTop;

		public bool MoveListToTop
		{
			get { return _moveListToTop; }
			set
			{
				_moveListToTop = value;
				if (value)
				{
					_feedbackTableView.ContentOffset = new CGPoint(0, 0 - _feedbackTableView.ContentInset.Top);
				}
			}
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			CheckNetworkCommand?.Execute(null);

			if (LastComplaintItemPosition != 0)
			{
				if (ShouldScrollTableView)
				{
					_feedbackTableView.ScrollToRow(NSIndexPath.FromRowSection(LastComplaintItemPosition, 0),
						UITableViewScrollPosition.Top, true);
				}
			}
		}

		protected override void InitView()
		{
			CreateDropdownBar();
			//List Category and Sorting
			_categoryTableView = UIHelper.CreateTableView(0, 0, ColorHelper.Overlay);
			_categoryItemSource = new CategoryItemTableViewSource(_categoryTableView);
			_categoryTableView.Source = _categoryItemSource;

			_sortingTableView = UIHelper.CreateTableView(0, 0, ColorHelper.Overlay);
			_sortingItemSource = new SortingItemTableViewSource(_sortingTableView);
			_sortingTableView.Source = _sortingItemSource;

			//Overlay
			CategoryOverlay = UIHelper.CreateAlphaView(0, 0, 0, ColorHelper.Overlay);
			SortingOverlay = UIHelper.CreateAlphaView(0, 0, 0, ColorHelper.Overlay);

			//Feedback
			_refreshControl = new MvxUIRefreshControl();
			_feedbackTableView = UIHelper.CreateTableView(0, 0, null);
			_feedbackItemSource = new FeedbackItemTableViewSource(_feedbackTableView);
			_feedbackTableView.Source = _feedbackItemSource;
			_feedbackTableView.Bounces = true;
			_feedbackTableView.ClipsToBounds = true;
			_feedbackTableView.AlwaysBounceVertical = true;
			_feedbackTableView.AddSubview(_refreshControl);

			_feedbackTableView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_refreshControl, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal,
					_feedbackTableView, NSLayoutAttribute.Top, 1, -30),
				NSLayoutConstraint.Create(_refreshControl, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal,
					_feedbackTableView, NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_refreshControl, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal,
					_feedbackTableView, NSLayoutAttribute.CenterY, 1, 0)
			});
			_floatingButton = UIHelper.CreateFloatingButton(DimensionHelper.FloatingButtonSize,
				DimensionHelper.FloatingButtonSize, 0, true);
			_lbNoData = UIHelper.CreateLabel(UIColor.Gray, DimensionHelper.MediumTextSize, false, UITextAlignment.Center,
				2);
			View.AddSubview(DropdownBar);
			View.Add(_lbNoData);
			View.Add(_feedbackTableView);
			View.AddSubview(_floatingButton);
			View.AddSubview(CategoryOverlay);
			View.AddSubview(SortingOverlay);
			View.Add(_categoryTableView);
			View.Add(_sortingTableView);
			View.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_lbNoData, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_lbNoData, NSLayoutAttribute.Top, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Top, 1, DimensionHelper.NoDataLabelMarginTop),

				//Dropdown bar
				NSLayoutConstraint.Create(DropdownBar, NSLayoutAttribute.Top, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(DropdownBar, NSLayoutAttribute.Left, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(DropdownBar, NSLayoutAttribute.Width, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Width, 1, 0),

				//Category table view
				NSLayoutConstraint.Create(_categoryTableView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, DropdownBar, NSLayoutAttribute.Bottom, 1, 0),
				NSLayoutConstraint.Create(_categoryTableView, NSLayoutAttribute.Width, NSLayoutRelation.Equal, View, NSLayoutAttribute.Width, 1, 0),
				NSLayoutConstraint.Create(_categoryTableView, NSLayoutAttribute.Height, NSLayoutRelation.Equal, null, NSLayoutAttribute.NoAttribute, 1, 7 * DimensionHelper.DropdownItemHeight),

				//Sorting table view
				NSLayoutConstraint.Create(_sortingTableView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, DropdownBar,
					NSLayoutAttribute.Bottom, 1, 0),
				NSLayoutConstraint.Create(_sortingTableView, NSLayoutAttribute.Width, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Width, 1, 0),
				NSLayoutConstraint.Create(_sortingTableView, NSLayoutAttribute.Height, NSLayoutRelation.Equal, null,
					NSLayoutAttribute.NoAttribute, 1, 3 * DimensionHelper.DropdownItemHeight),

				//Category overlay
				NSLayoutConstraint.Create(CategoryOverlay, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_categoryTableView, NSLayoutAttribute.Bottom, 1, 0),
				NSLayoutConstraint.Create(CategoryOverlay, NSLayoutAttribute.Left, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(CategoryOverlay, NSLayoutAttribute.Width, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Width, 1, 0),
				NSLayoutConstraint.Create(CategoryOverlay, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Bottom, 1, 0),

				//Sorting overlay
				NSLayoutConstraint.Create(SortingOverlay, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_sortingTableView, NSLayoutAttribute.Bottom, 1, 0),
				NSLayoutConstraint.Create(SortingOverlay, NSLayoutAttribute.Left, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(SortingOverlay, NSLayoutAttribute.Width, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Width, 1, 0),
				NSLayoutConstraint.Create(SortingOverlay, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Bottom, 1, 0),

				//Floating button
				NSLayoutConstraint.Create(_floatingButton, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Bottom, 1, -DimensionHelper.FloatingButtonMargin),
				NSLayoutConstraint.Create(_floatingButton, NSLayoutAttribute.Right, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Right, 1, -DimensionHelper.FloatingButtonMargin),

				//Feedback List
				NSLayoutConstraint.Create(_feedbackTableView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, DropdownBar, NSLayoutAttribute.Bottom, 1, 0),
				NSLayoutConstraint.Create(_feedbackTableView, NSLayoutAttribute.Left, NSLayoutRelation.Equal, View, NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_feedbackTableView, NSLayoutAttribute.Right, NSLayoutRelation.Equal, View, NSLayoutAttribute.Right, 1, 0),
				NSLayoutConstraint.Create(_feedbackTableView, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, View, NSLayoutAttribute.Bottom, 1, 0)
			});
			base.InitView();
		}

		protected override void CreateBinding()
		{
			base.CreateBinding();

			var set = this.CreateBindingSet<HomeView, HomeViewModel>();

			//Category
			set.Bind(_ivCategoryIconName)
				.For(v => v.Image)
				.To(vm => vm.CategoryIconName)
				.WithConversion("IconNameToUIImage");

			set.Bind(_lbCategoryText).To(vm => vm.CategoryName);
			set.Bind(_lbCategoryText).For(v => v.TextColor).To(vm => vm.IsCategoryShown).WithConversion("BoolToColor");

			set.Bind(_cateLayout).For(v => v.Hidden).To(vm => vm.IsCateShown).WithConversion("InvertBool");
			set.Bind(_cateLayout).For(v => v.ClickCommand).To(vm => vm.CategoryCommand);

			//Sorting
			set.Bind(_ivSortingIconName)
				.For(v => v.Image)
				.To(vm => vm.SortingIconName)
				.WithConversion("IconNameToUIImage");
			set.Bind(_ivSortingIconName).For(v => v.ClickCommand).To(vm => vm.SortingCommand);

			set.Bind(_lbSortingText).To(vm => vm.Sorting);
			set.Bind(_lbSortingText).For(v => v.ClickCommand).To(vm => vm.SortingCommand);
			set.Bind(_lbSortingText).For(v => v.TextColor).To(vm => vm.IsSortingShown).WithConversion("BoolToColor");

			//Dropdown List
			set.Bind(_categoryItemSource).To(vm => vm.CategoryItemViewModels);
			set.Bind(_categoryTableView).For(v => v.Hidden).To(vm => vm.IsCategoryShown).WithConversion("InvertBool");

			set.Bind(_sortingItemSource).To(vm => vm.SortingItemViewModels);
			set.Bind(_sortingTableView).For(v => v.Hidden).To(vm => vm.IsSortingShown).WithConversion("InvertBool");

			//Overlay
			set.Bind(CategoryOverlay).For(v => v.Hidden).To(vm => vm.IsCategoryShown).WithConversion("InvertBool");
			set.Bind(CategoryOverlay).For(v => v.ClickCommand).To(vm => vm.CategoryOverlayCommand);

			set.Bind(SortingOverlay).For(v => v.Hidden).To(vm => vm.IsSortingShown).WithConversion("InvertBool");
			set.Bind(SortingOverlay).For(v => v.ClickCommand).To(vm => vm.SortingOverlayCommand);

			//Floating button
			set.Bind(_floatingButton).For(v => v.ClickCommand).To(vm => vm.GoToNewComplaintCommand);

			//Feedback
			set.Bind(_feedbackItemSource).To(vm => vm.FeedbackItemViewModels);
			set.Bind(_feedbackItemSource).For(v => v.LoadMoreData).To(vm => vm.LoadMoreData);

			set.Bind(this).For(v => v.LastComplaintItemPosition).To(vm => vm.LastComplaintItemPosition);
			set.Bind(this).For(v => v.CheckNetworkCommand).To(vm => vm.CheckNetworkCommand);
			set.Bind(this).For(v => v.MoveListToTop).To(vm => vm.MoveListToTop);

			set.Bind(_refreshControl)
				.For(x => x.IsRefreshing)
				.To(vm => vm.IsRefreshing);

			set.Bind(_refreshControl)
				.For(x => x.RefreshCommand)
				.To(vm => vm.RefreshCommand);
			set.Bind(_lbNoData).To(vm => vm.NoData);
			set.Bind(_lbNoData).For(v => v.Hidden).To(vm => vm.IsNoDataTextShown).WithConversion("InvertBool");

			set.Bind(_searchResult).To(vm => vm.SearchedText);
			set.Bind(_searchResult).For(v => v.Hidden).To(vm => vm.IsSearchedTextShown).WithConversion("InvertBool");
			set.Bind(_searchResultLayout)
				.For(v => v.Hidden)
				.To(vm => vm.IsSearchedTextShown)
				.WithConversion("InvertBool");
			set.Bind(_searchResultLayout).For(v => v.ClickCommand).To(vm => vm.RemoveSearchResult);

			set.Bind(this).For(v => v.ShouldScrollTableView).To(vm => vm.ShouldScrollTableView);
			set.Apply();
		}

		private void CreateDropdownBar()
		{
			ResolutionHelper.InitStaticVariable();

			DropdownBar = UIHelper.CreateView(0, DimensionHelper.DropdownBarHeight, ColorHelper.LighterGray);

			_lbCategoryText = UIHelper.CreateAlphaLabel(ColorHelper.LightGray, DimensionHelper.BigTextSize);
			_ivCategoryIconName = UIHelper.CreateAlphaImageView(DimensionHelper.DropdownIconSize,
				DimensionHelper.DropdownIconSize, DimensionHelper.DropdownPadding);
			_lbCategoryText.UserInteractionEnabled = false;
			_ivCategoryIconName.UserInteractionEnabled = false;

			_lbSortingText = UIHelper.CreateAlphaLabel(ColorHelper.LightGray, DimensionHelper.BigTextSize);
			_ivSortingIconName = UIHelper.CreateAlphaImageView(DimensionHelper.DropdownIconSize,
				DimensionHelper.DropdownIconSize, DimensionHelper.DropdownPadding);

			_searchResultLayout = UIHelper.CreateAlphaView(0, 0, 0);
			_searchResult = UIHelper.CreateLabel(UIColor.Black, DimensionHelper.BigTextSize);
			_searchResult = UIHelper.CreateLabel(UIColor.Black, DimensionHelper.BigTextSize);
			_searchResult.LineBreakMode = UILineBreakMode.TailTruncation;
			_removeButton = UIHelper.CreateAlphaImageView(DimensionHelper.MassiveContentMargin,
				DimensionHelper.MassiveContentMargin, DimensionHelper.ContentPadding);
			_removeButton.Image = UIImage.FromFile(ImageHelper.Remove);
			_removeButton.UserInteractionEnabled = false;

			_searchResultLayout.Add(_removeButton);
			_searchResultLayout.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_removeButton, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					_searchResultLayout, NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_removeButton, NSLayoutAttribute.Right, NSLayoutRelation.Equal,
					_searchResultLayout, NSLayoutAttribute.Right, 1, 0),
				NSLayoutConstraint.Create(_removeButton, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_searchResultLayout, NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_removeButton, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal,
					_searchResultLayout, NSLayoutAttribute.Bottom, 1, 0),
			});

			_cateLayout = UIHelper.CreateAlphaView(0, 0, 0);
			_cateLayout.Add(_ivCategoryIconName);
			_cateLayout.Add(_lbCategoryText);
			_cateLayout.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_ivCategoryIconName, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal,
					_cateLayout, NSLayoutAttribute.CenterY, 1, 0),
				NSLayoutConstraint.Create(_ivCategoryIconName, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					_cateLayout, NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_lbCategoryText, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal,
					_cateLayout, NSLayoutAttribute.CenterY, 1, 0),
				NSLayoutConstraint.Create(_lbCategoryText, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					_ivCategoryIconName, NSLayoutAttribute.Right, 1, 0),
				NSLayoutConstraint.Create(_cateLayout, NSLayoutAttribute.Right, NSLayoutRelation.Equal,
					_lbCategoryText, NSLayoutAttribute.Right, 1, 0),
			});

			DropdownBar.Add(_cateLayout);
			DropdownBar.Add(_ivSortingIconName);
			DropdownBar.Add(_lbSortingText);
			DropdownBar.Add(_searchResult);
			DropdownBar.Add(_searchResultLayout);

			DropdownBar.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_cateLayout, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal,
					DropdownBar, NSLayoutAttribute.CenterY, 1, 0),
				NSLayoutConstraint.Create(_cateLayout, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					DropdownBar, NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_cateLayout, NSLayoutAttribute.Height, NSLayoutRelation.Equal,
					DropdownBar, NSLayoutAttribute.Height, 1, 0),
				NSLayoutConstraint.Create(_ivSortingIconName, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal,
					DropdownBar, NSLayoutAttribute.CenterY, 1, 0),
				NSLayoutConstraint.Create(_ivSortingIconName, NSLayoutAttribute.Right, NSLayoutRelation.Equal,
					DropdownBar, NSLayoutAttribute.Right, 1, 0),
				NSLayoutConstraint.Create(_lbSortingText, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, DropdownBar,
					NSLayoutAttribute.CenterY, 1, 0),
				NSLayoutConstraint.Create(_lbSortingText, NSLayoutAttribute.Right, NSLayoutRelation.Equal,
					_ivSortingIconName, NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_searchResult, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, DropdownBar,
					NSLayoutAttribute.CenterY, 1, 0),
				NSLayoutConstraint.Create(_searchResult, NSLayoutAttribute.Height, NSLayoutRelation.Equal,
					DropdownBar, NSLayoutAttribute.Height, 1, 0),
				NSLayoutConstraint.Create(_searchResult, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					DropdownBar, NSLayoutAttribute.Left, 1, DimensionHelper.LayoutMargin),
				NSLayoutConstraint.Create(_searchResult, NSLayoutAttribute.Width, NSLayoutRelation.Equal,
					null, NSLayoutAttribute.NoAttribute, 1, 150),
				NSLayoutConstraint.Create(_searchResultLayout, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					_searchResult, NSLayoutAttribute.Right, 1, DimensionHelper.LayoutMargin),
				NSLayoutConstraint.Create(_searchResultLayout, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal,
					DropdownBar, NSLayoutAttribute.CenterY, 1, 0)
			});
		}

		public override void ViewDidAppear(bool animated)
		{
			base.ViewDidAppear(animated);

			var linkVideoGradientLayer = GetGradientLayer(DropdownBar.Bounds);
			DropdownBar.Layer.InsertSublayer(linkVideoGradientLayer, 0);
		}

		private CAGradientLayer GetGradientLayer(CGRect cgRect)
		{
			var gradientLayer = new CAGradientLayer
			{
				Frame = cgRect,
				NeedsDisplayOnBoundsChange = true,
				MasksToBounds = true,
				Colors = new[]
				{
					ColorHelper.CenterButton.CGColor, ColorHelper.EndButton.CGColor, ColorHelper.DropdownBarEnd.CGColor
				}
			};

			return gradientLayer;
		}

		public override void WillEnterForeground()
		{
			CheckNetworkCommand?.Execute(null);
		}


		public bool ShouldScrollTableView { get; set; }

		#region UI Elements

		public ICommand CheckNetworkCommand { get; set; }

		public UIView DropdownBar { get; private set; }

		//Dropdown Bar Items
		private AlphaImageView _ivCategoryIconName;
		private AlphaLabel _lbCategoryText;
		private AlphaUIView _cateLayout;

		private AlphaImageView _ivSortingIconName;
		private AlphaLabel _lbSortingText;

		private UILabel _searchResult;
		private AlphaImageView _removeButton;
		private AlphaUIView _searchResultLayout;

		//List View
		private UITableView _categoryTableView;
		private CategoryItemTableViewSource _categoryItemSource;

		private UITableView _sortingTableView;
		private SortingItemTableViewSource _sortingItemSource;

		//Overlay
		public AlphaUIView CategoryOverlay { get; private set; }
		public AlphaUIView SortingOverlay { get; private set; }

		//Floating button
		private FloatingButton _floatingButton;

		//Feedback List
		private UITableView _feedbackTableView;
		private FeedbackItemTableViewSource _feedbackItemSource;
		private UILabel _lbNoData;

		#endregion
	}
}