using System;
using System.Collections.Generic;
using System.Windows.Input;
using Facebook.LoginKit;
using Google.SignIn;
using GovernmentComplaint.Core.Localization;
using GovernmentComplaint.Core.ViewModels.Bases;
using GovernmentComplaint.iOS.Controls;
using GovernmentComplaint.iOS.Helpers;
using GovernmentComplaint.iOS.Views.Bases;
using GovernmentComplaint.iOS.Views.TableView.ItemTableViewSource;
using MvvmCross.Binding.BindingContext;
using UIKit;

namespace GovernmentComplaint.iOS.Views
{
	public class SidebarLayout : DisappearableUIView
	{
		public SidebarLayout(MvxFluentBindingDescriptionSet<MasterView, MasterViewModel> bindingSet)
		{
			InitView();
			CreateBinding(bindingSet);
		}

		public void InitView()
		{
			TranslatesAutoresizingMaskIntoConstraints = false;
			_sidebarTableView = UIHelper.CreateTableView(0, 0, ColorHelper.NeutralGray);
			_sidebarItemSource = new SidebarItemTableViewSource(_sidebarTableView);
			_sidebarTableView.Source = _sidebarItemSource;

			WidthConstraint = NSLayoutConstraint.Create(this, NSLayoutAttribute.Width, NSLayoutRelation.Equal, null,
				NSLayoutAttribute.NoAttribute, 1, DimensionHelper.SidebarWidth);
			BackgroundColor = ColorHelper.NeutralGray;
			AddLoginRegion();
			AddSubview(_loginRegion);
			Add(_sidebarTableView);

			AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_loginRegion, NSLayoutAttribute.Top, NSLayoutRelation.Equal, this,
					NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_loginRegion, NSLayoutAttribute.Right, NSLayoutRelation.Equal, this,
					NSLayoutAttribute.Right, 1, 0),
				NSLayoutConstraint.Create(_loginRegion, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, this,
					NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_loginRegion, NSLayoutAttribute.Height, NSLayoutRelation.Equal, null,
					NSLayoutAttribute.NoAttribute, 1, DimensionHelper.LoginRegionHeight),
				NSLayoutConstraint.Create(_sidebarTableView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _loginRegion,
					NSLayoutAttribute.Bottom, 1, 0),
				NSLayoutConstraint.Create(_sidebarTableView, NSLayoutAttribute.Right, NSLayoutRelation.Equal, this,
					NSLayoutAttribute.Right, 1, 0),
				NSLayoutConstraint.Create(_sidebarTableView, NSLayoutAttribute.Width, NSLayoutRelation.Equal, this,
					NSLayoutAttribute.Width, 1, 0),
				NSLayoutConstraint.Create(_sidebarTableView, NSLayoutAttribute.Height, NSLayoutRelation.Equal, null,
					NSLayoutAttribute.NoAttribute, 1, 3 * DimensionHelper.SidebarItemWidth)
			});
		}

		private void AddLoginRegion()
		{
			_loginRegion = UIHelper.CreateView(0, 0, ColorHelper.LightBlue);
			_lbChooseAuth = UIHelper.CreateLabel(ColorHelper.White, DimensionHelper.SmallestTextSize);
			_lbChooseAuth.Text = ChooseAuth;

			AddFacebookLogin();
			AddGoogleLogin();
			AddWelcomeRegion();
			_loginRegion.AddSubview(_lbChooseAuth);
			_loginRegion.AddSubview(_facebookLoginView);
			_loginRegion.AddSubview(_googleLoginView);
			_loginRegion.AddSubview(_avatar);
			_loginRegion.AddSubview(_welcomeRegion);
			_loginRegion.AddSubview(_feedbackImage);
			_loginRegion.AddSubview(_lbTotalFeedback);
			_loginRegion.AddSubview(_lbTotalFeedbackText);
			_loginRegion.AddSubview(_commentImage);
			_loginRegion.AddSubview(_lbTotalComment);
			_loginRegion.AddSubview(_lbTotalCommentText);
			_loginRegion.AddConstraints(new[]
			{
				//Authenticated region
				NSLayoutConstraint.Create(_avatar, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _loginRegion,
					NSLayoutAttribute.Top, 1, DimensionHelper.ChooseAuthUpperMargin),
				NSLayoutConstraint.Create(_avatar, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _loginRegion,
					NSLayoutAttribute.CenterX, 1, 0),

				NSLayoutConstraint.Create(_welcomeRegion, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _avatar,
					NSLayoutAttribute.Bottom, 1, DimensionHelper.ChooseAuthUpperMargin),
				NSLayoutConstraint.Create(_welcomeRegion, NSLayoutAttribute.Left, NSLayoutRelation.GreaterThanOrEqual,
				                          _loginRegion, NSLayoutAttribute.Left, 1, DimensionHelper.BigContentMargin),
				NSLayoutConstraint.Create(_welcomeRegion, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal,
					_loginRegion, NSLayoutAttribute.CenterX, 1, 0),

				NSLayoutConstraint.Create(_lbTotalFeedbackText, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_welcomeRegion, NSLayoutAttribute.Bottom, 1, DimensionHelper.InteractionTextTopMargin),
				NSLayoutConstraint.Create(_lbTotalFeedbackText, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _avatar,
					NSLayoutAttribute.CenterX, 1, -DimensionHelper.UserPictureMargin),
				NSLayoutConstraint.Create(_lbTotalFeedback, NSLayoutAttribute.Right, NSLayoutRelation.Equal,
					_lbTotalFeedbackText,
					NSLayoutAttribute.Left, 1, -DimensionHelper.InteractionTextSpace),
				NSLayoutConstraint.Create(_lbTotalFeedback, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_welcomeRegion,
					NSLayoutAttribute.Bottom, 1, DimensionHelper.InteractionTextTopMargin),
				NSLayoutConstraint.Create(_feedbackImage, NSLayoutAttribute.Right, NSLayoutRelation.Equal,
					_lbTotalFeedback,
					NSLayoutAttribute.Left, 1, -DimensionHelper.InteractionTextSpace),
				NSLayoutConstraint.Create(_feedbackImage, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _welcomeRegion,
					NSLayoutAttribute.Bottom, 1, DimensionHelper.InteractionImageMarginTop),
				NSLayoutConstraint.Create(_commentImage, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _avatar,
					NSLayoutAttribute.CenterX, 1, DimensionHelper.CommentImageMargin),
				NSLayoutConstraint.Create(_commentImage, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _welcomeRegion,
					NSLayoutAttribute.Bottom, 1, DimensionHelper.InteractionImageMarginTop),
				NSLayoutConstraint.Create(_lbTotalComment, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _commentImage,
					NSLayoutAttribute.Right, 1, DimensionHelper.InteractionTextSpace),
				NSLayoutConstraint.Create(_lbTotalComment, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _welcomeRegion,
					NSLayoutAttribute.Bottom, 1, DimensionHelper.InteractionTextTopMargin),
				NSLayoutConstraint.Create(_lbTotalCommentText, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_welcomeRegion,
					NSLayoutAttribute.Bottom, 1, DimensionHelper.InteractionTextTopMargin),
				NSLayoutConstraint.Create(_lbTotalCommentText, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					_lbTotalComment,
					NSLayoutAttribute.Right, 1, DimensionHelper.InteractionTextSpace),

				//Choose authentication label
				NSLayoutConstraint.Create(_lbChooseAuth, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _loginRegion,
					NSLayoutAttribute.Top, 1, DimensionHelper.ChooseAuthUpperMargin),
				NSLayoutConstraint.Create(_lbChooseAuth, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _loginRegion,
					NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_facebookLoginView, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_lbChooseAuth,
					NSLayoutAttribute.Bottom, 1, DimensionHelper.ChooseAuthUpperMargin),
				NSLayoutConstraint.Create(_facebookLoginView, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal,
					_loginRegion,
					NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_googleLoginView, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_facebookLoginView,
					NSLayoutAttribute.Bottom, 1, DimensionHelper.ChooseAuthUpperMargin),
				NSLayoutConstraint.Create(_googleLoginView, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal,
					_loginRegion,
					NSLayoutAttribute.CenterX, 1, 0),
			});
		}

		private void AddFacebookLogin()
		{
			_facebookLoginView = UIHelper.CreateAlphaView(DimensionHelper.LoginButtonWidth,
				DimensionHelper.LoginButtonHeight, 0,
				ColorHelper.DarkBlue, true);
			_lbFacebookText = UIHelper.CreateLabel(ColorHelper.White, DimensionHelper.MediumTextSize);
			_lbFacebookText.Text = "Facebook";
			_ivFacebookLogo = UIHelper.CreateAlphaImageView(DimensionHelper.LoginButtonHeight,
				DimensionHelper.LoginButtonHeight, DimensionHelper.SearchButtonPadding);
			_ivFacebookLogo.Image = UIImage.FromFile(ImageHelper.FacebookLogo);
			_facebookLoginView.Add(_lbFacebookText);
			_facebookLoginView.Add(_ivFacebookLogo);

			_facebookLoginView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_ivFacebookLogo, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal,
					_facebookLoginView,
					NSLayoutAttribute.CenterY, 1, 0),
				NSLayoutConstraint.Create(_ivFacebookLogo, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					_facebookLoginView,
					NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_lbFacebookText, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal,
					_facebookLoginView,
					NSLayoutAttribute.CenterY, 1, 0),
				NSLayoutConstraint.Create(_lbFacebookText, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					_ivFacebookLogo,
					NSLayoutAttribute.Right, 1, 0)
			});
			_facebookLoginView.Clicked += _facebookLoginView_Clicked;
		}

		private void _facebookLoginView_Clicked(object sender, EventArgs e)
		{
			var loginManager = new LoginManager();
			loginManager.LoginBehavior = LoginBehavior.Native;
			loginManager.LogInWithReadPermissions(_readPermissions.ToArray(),
				UIApplication.SharedApplication.KeyWindow.RootViewController,
				(result, error)
					=>
				{
					if (error != null)
					{
						return;
					}
					if (result.IsCancelled)
					{
						return;
					}
				});
		}

		private void AddGoogleLogin()
		{
			_googleLoginButton = new SignInButton()
			{
				TranslatesAutoresizingMaskIntoConstraints = false,
				Style = ButtonStyle.Wide
			};
			_googleLoginView = UIHelper.CreateAlphaView(DimensionHelper.LoginButtonWidth, DimensionHelper.LoginButtonHeight,
				0, ColorHelper.Red, true);
			_lbGoogleText = UIHelper.CreateLabel(ColorHelper.White, DimensionHelper.MediumTextSize);
			_lbGoogleText.Text = "Google";
			_lbGoogleText.UserInteractionEnabled = false;
			_ivGoogleLogo = UIHelper.CreateAlphaImageView(DimensionHelper.LoginButtonHeight,
				DimensionHelper.LoginButtonHeight, DimensionHelper.SearchButtonPadding);
			_ivGoogleLogo.Image = UIImage.FromFile(ImageHelper.GoogleLogo);
			_ivGoogleLogo.UserInteractionEnabled = false;
			_googleLoginView.Add(_ivGoogleLogo);
			_googleLoginView.Add(_lbGoogleText);
			_googleLoginView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_ivGoogleLogo, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal,
					_googleLoginView,
					NSLayoutAttribute.CenterY, 1, 0),
				NSLayoutConstraint.Create(_ivGoogleLogo, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					_googleLoginView,
					NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_lbGoogleText, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal,
					_googleLoginView,
					NSLayoutAttribute.CenterY, 1, 0),
				NSLayoutConstraint.Create(_lbGoogleText, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					_ivGoogleLogo,
					NSLayoutAttribute.Right, 1, 0)
			});
			_googleLoginView.Clicked += _googleLoginView_Clicked;
		}

		private void _googleLoginView_Clicked(object sender, EventArgs e)
		{
			SignIn.SharedInstance.SignInUser();
		}

		private void AddWelcomeRegion()
		{
			_avatar = UIHelper.CreateCustomImageView(DimensionHelper.UserPictureSize, DimensionHelper.UserPictureSize);
			_avatar.Layer.CornerRadius = DimensionHelper.UserPictureSize / 2;
			_avatar.Layer.MasksToBounds = true;
			_avatar.Image = UIImage.FromFile("Images/default_complaint_image.png");
			_welcomeRegion = UIHelper.CreateView(0,0, UIColor.White);
			_welcomeRegion.BackgroundColor = UIColor.Brown;
			_lbWelcomeUserText = UIHelper.CreateLabel(UIColor.White, DimensionHelper.MediumTextSize);
			_lbWelcomeUserName = UIHelper.CreateLabel(UIColor.White, DimensionHelper.MediumTextSize, true);
			_lbWelcomeUserName.LineBreakMode = UILineBreakMode.TailTruncation;
			_feedbackImage = UIHelper.CreateImageView(DimensionHelper.InteractionImageSize,
				DimensionHelper.InteractionImageSize);
			_feedbackImage.Image = UIImage.FromFile("Images/feedback_sidebar.png");
			_commentImage = UIHelper.CreateImageView(DimensionHelper.InteractionImageSize,
				DimensionHelper.InteractionImageSize);
			_commentImage.Image = UIImage.FromFile("Images/comment_sidebar.png");
			_lbTotalFeedback = UIHelper.CreateLabel(UIColor.White, DimensionHelper.MediumTextSize);
			_lbTotalComment = UIHelper.CreateLabel(UIColor.White, DimensionHelper.MediumTextSize);
			_lbTotalFeedbackText = UIHelper.CreateLabel(UIColor.White, DimensionHelper.MediumTextSize);
			_lbTotalFeedbackText.Text = TotalFeedbackText;
			_lbTotalCommentText = UIHelper.CreateLabel(UIColor.White, DimensionHelper.MediumTextSize);
			_lbTotalCommentText.Text = TotalCommentText;
			_welcomeRegion.AddSubview(_lbWelcomeUserText);
			_welcomeRegion.AddSubview(_lbWelcomeUserName);
			_welcomeRegion.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_lbWelcomeUserText, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_welcomeRegion, NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_lbWelcomeUserText, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					_welcomeRegion, NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_lbWelcomeUserName, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					_lbWelcomeUserText, NSLayoutAttribute.Right, 1, 0),
				NSLayoutConstraint.Create(_lbWelcomeUserName, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_welcomeRegion, NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_lbWelcomeUserName, NSLayoutAttribute.Right, NSLayoutRelation.Equal,
					_welcomeRegion, NSLayoutAttribute.Right, 1, 0)
			});
		}

		private void CreateBinding(MvxFluentBindingDescriptionSet<MasterView, MasterViewModel> bindingSet)
		{
			//Is not logged in
			bindingSet.Bind(_sidebarItemSource).To(vm => vm.SidebarItemViewModels);
			bindingSet.Bind(_avatar).For(v => v.Image).To(vm => vm.ProfilePictureUrl).WithConversion("UriToUIImage");
			bindingSet.Bind(_avatar).For(v => v.Hidden).To(vm => vm.IsLoggedIn).WithConversion("InvertBool");
			bindingSet.Bind(_lbWelcomeUserName).To(vm => vm.WelcomeUserName);
			bindingSet.Bind(_lbWelcomeUserName).For(v => v.Hidden).To(vm => vm.IsLoggedIn).WithConversion("InvertBool");
			bindingSet.Bind(_lbTotalFeedback).To(vm => vm.TotalFeedbackText);
			bindingSet.Bind(_lbTotalFeedback).For(v => v.Hidden).To(vm => vm.IsLoggedIn).WithConversion("InvertBool");
			bindingSet.Bind(_lbTotalComment).To(vm => vm.TotalCommentText);
			bindingSet.Bind(_lbTotalComment).For(v => v.Hidden).To(vm => vm.IsLoggedIn).WithConversion("InvertBool");
			bindingSet.Bind(_lbWelcomeUserText).To(vm => vm.WelcomeUserText);
			bindingSet.Bind(_lbWelcomeUserText).For(v => v.Hidden).To(vm => vm.IsLoggedIn).WithConversion("InvertBool");
			bindingSet.Bind(_lbTotalFeedbackText).To(vm => vm.TotalFeedbackStaticText);
			bindingSet.Bind(_lbTotalFeedbackText).For(v => v.Hidden).To(vm => vm.IsLoggedIn).WithConversion("InvertBool");
			bindingSet.Bind(_lbTotalCommentText).To(vm => vm.TotalCommentStaticText);
			bindingSet.Bind(_lbTotalCommentText).For(v => v.Hidden).To(vm => vm.IsLoggedIn).WithConversion("InvertBool");
			bindingSet.Bind(_feedbackImage).For(v => v.Hidden).To(vm => vm.IsLoggedIn).WithConversion("InvertBool");
			bindingSet.Bind(_commentImage).For(v => v.Hidden).To(vm => vm.IsLoggedIn).WithConversion("InvertBool");

			//Logged in
			bindingSet.Bind(_lbChooseAuth).For(v => v.Hidden).To(vm => vm.IsLoggedIn);
			bindingSet.Bind(_googleLoginView).For(v => v.Hidden).To(vm => vm.IsLoggedIn);
			bindingSet.Bind(_facebookLoginView).For(v => v.Hidden).To(vm => vm.IsLoggedIn);

			bindingSet.Apply();
		}

		#region Properties

		private AlphaUIView _facebookLoginView;
		private UILabel _lbFacebookText;
		private AlphaImageView _ivFacebookLogo;
		private AlphaUIView _googleLoginView;
		private UILabel _lbGoogleText;
		private AlphaImageView _ivGoogleLogo;
		private UIView _loginRegion;
		private UILabel _lbChooseAuth;
		private UITableView _sidebarTableView;
		private SidebarItemTableViewSource _sidebarItemSource;
		public string ChooseAuth => Localizer.GetText("ChooseAuth");
		private readonly List<string> _readPermissions = new List<string> {"public_profile"};
		private SignInButton _googleLoginButton;
		private CustomImageView _avatar;
		private UIView _welcomeRegion;
		private UILabel _lbWelcomeUserText;
		private UILabel _lbWelcomeUserName;
		private UIImageView _feedbackImage;
		private UILabel _lbTotalFeedback;
		public string TotalFeedbackText => Localizer.GetText("TotalReplyStaticText");
		private UIImageView _commentImage;
		private UILabel _lbTotalFeedbackText;
		private UILabel _lbTotalComment;
		public string TotalCommentText => Localizer.GetText("TotalCommentStaticText");
		private UILabel _lbTotalCommentText;

		#endregion
	}
}