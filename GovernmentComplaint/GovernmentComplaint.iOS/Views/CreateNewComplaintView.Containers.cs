using CoreGraphics;
using GovernmentComplaint.iOS.Controls;
using GovernmentComplaint.iOS.Helpers;
using UIKit;

namespace GovernmentComplaint.iOS.Views
{
    public partial class CreateNewComplaintView
    {
        protected void InitElementContainers()
        {
            #region Send button

            _sendButtonContainer.Add(_lbSendText);

            _sendButtonContainer.AddConstraints(new[]
            {
                NSLayoutConstraint.Create(_lbSendText, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _sendButtonContainer,
                    NSLayoutAttribute.CenterX, 1, 0),
                NSLayoutConstraint.Create(_lbSendText, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal,
                    _sendButtonContainer, NSLayoutAttribute.CenterY, 1, 0)
            });

            #endregion

            #region Attached image region

            _videoLinkContainer.Add(_ivSmallLinkVideoIcon);
            _videoLinkContainer.Add(_ivRemoveIcon);
            _videoLinkContainer.Add(_lbVideoLink);
            _videoLinkContainer.AddConstraints(new[]
            {
                NSLayoutConstraint.Create(_ivSmallLinkVideoIcon, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
                    _videoLinkContainer, NSLayoutAttribute.Left, 1, 0),
                NSLayoutConstraint.Create(_ivSmallLinkVideoIcon, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
                    _videoLinkContainer, NSLayoutAttribute.Top, 1, 0),
                NSLayoutConstraint.Create(_ivRemoveIcon, NSLayoutAttribute.Right, NSLayoutRelation.Equal,
                    _videoLinkContainer, NSLayoutAttribute.Right, 1, 0),
                NSLayoutConstraint.Create(_ivRemoveIcon, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
                    _videoLinkContainer, NSLayoutAttribute.Top, 1, 0),
                NSLayoutConstraint.Create(_lbVideoLink, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
                    _ivSmallLinkVideoIcon, NSLayoutAttribute.Right, 1, 0),
                NSLayoutConstraint.Create(_lbVideoLink, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _ivRemoveIcon,
                    NSLayoutAttribute.Left, 1, 0),
                NSLayoutConstraint.Create(_lbVideoLink, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal,
                    _videoLinkContainer, NSLayoutAttribute.CenterY, 1, 0)
            });


            _lstProgressBars[0].StartAnimating();
            _lstProgressBars[0].Transform = CGAffineTransform.MakeScale(1.5f, 1.5f);

            _lstProgressBars[1].StartAnimating();
            _lstProgressBars[1].Transform = CGAffineTransform.MakeScale(1.5f, 1.5f);

            _lstProgressBars[2].StartAnimating();
            _lstProgressBars[2].Transform = CGAffineTransform.MakeScale(1.5f, 1.5f);

            _lstIndicators[0].Add(_lstProgressBars[0]);
            _lstIndicators[0].AddConstraints(new []
            {
                NSLayoutConstraint.Create(_lstProgressBars[0], NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _lstIndicators[0],
                    NSLayoutAttribute.CenterX, 1, 0),
                NSLayoutConstraint.Create(_lstProgressBars[0], NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, _lstIndicators[0], 
                    NSLayoutAttribute.CenterY, 1, 0)
            });

            _lstIndicators[1].Add(_lstProgressBars[1]);
            _lstIndicators[1].AddConstraints(new[]
            {
                NSLayoutConstraint.Create(_lstProgressBars[1], NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _lstIndicators[1],
                    NSLayoutAttribute.CenterX, 1, 0),
                NSLayoutConstraint.Create(_lstProgressBars[1], NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, _lstIndicators[1],
                    NSLayoutAttribute.CenterY, 1, 0)
            });

            _lstIndicators[2].Add(_lstProgressBars[2]);
            _lstIndicators[2].AddConstraints(new[]
            {
                NSLayoutConstraint.Create(_lstProgressBars[2], NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _lstIndicators[2],
                    NSLayoutAttribute.CenterX, 1, 0),
                NSLayoutConstraint.Create(_lstProgressBars[2], NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, _lstIndicators[2],
                    NSLayoutAttribute.CenterY, 1, 0)
            });

            _attachedImages = new[] { _attachedImage1, _attachedImage2, _attachedImage3 };

            _attachedImage1.Add(_lstIndicators[0]);
            _attachedImage1.AddConstraints(new []
            {
                NSLayoutConstraint.Create(_lstIndicators[0], NSLayoutAttribute.Top, NSLayoutRelation.Equal, _attachedImage1,
                    NSLayoutAttribute.Top, 1, 0),
                NSLayoutConstraint.Create(_lstIndicators[0], NSLayoutAttribute.Left, NSLayoutRelation.Equal, _attachedImage1,
                    NSLayoutAttribute.Left, 1, 0),
                NSLayoutConstraint.Create(_lstIndicators[0], NSLayoutAttribute.Right, NSLayoutRelation.Equal, _attachedImage1,
                    NSLayoutAttribute.Right, 1, 0),
                NSLayoutConstraint.Create(_lstIndicators[0], NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _attachedImage1,
                    NSLayoutAttribute.Bottom, 1, 0),

            });

            _attachedImage2.Add(_lstIndicators[1]);
            _attachedImage2.AddConstraints(new[]
            {
                NSLayoutConstraint.Create(_lstIndicators[1], NSLayoutAttribute.Top, NSLayoutRelation.Equal, _attachedImage2,
                    NSLayoutAttribute.Top, 1, 0),
                NSLayoutConstraint.Create(_lstIndicators[1], NSLayoutAttribute.Left, NSLayoutRelation.Equal, _attachedImage2,
                    NSLayoutAttribute.Left, 1, 0),
                NSLayoutConstraint.Create(_lstIndicators[1], NSLayoutAttribute.Right, NSLayoutRelation.Equal, _attachedImage2,
                    NSLayoutAttribute.Right, 1, 0),
                NSLayoutConstraint.Create(_lstIndicators[1], NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _attachedImage2,
                    NSLayoutAttribute.Bottom, 1, 0),

            });

            _attachedImage3.Add(_lstIndicators[2]);
            _attachedImage3.AddConstraints(new[]
            {
                NSLayoutConstraint.Create(_lstIndicators[2], NSLayoutAttribute.Top, NSLayoutRelation.Equal, _attachedImage3,
                    NSLayoutAttribute.Top, 1, 0),
                NSLayoutConstraint.Create(_lstIndicators[2], NSLayoutAttribute.Left, NSLayoutRelation.Equal, _attachedImage3,
                    NSLayoutAttribute.Left, 1, 0),
                NSLayoutConstraint.Create(_lstIndicators[2], NSLayoutAttribute.Right, NSLayoutRelation.Equal, _attachedImage3,
                    NSLayoutAttribute.Right, 1, 0),
                NSLayoutConstraint.Create(_lstIndicators[2], NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _attachedImage3,
                    NSLayoutAttribute.Bottom, 1, 0),

            });
            
            _imageRegion.Add(_attachedImage1);
            _imageRegion.Add(_attachedImage2);
            _imageRegion.Add(_attachedImage3);

            _imageRegion.AddConstraints(new[]
            {
                NSLayoutConstraint.Create(_attachedImage1, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _imageRegion,
                    NSLayoutAttribute.Left, 1, 0),
                NSLayoutConstraint.Create(_attachedImage1, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _imageRegion,
                    NSLayoutAttribute.Top, 1, 0),
                NSLayoutConstraint.Create(_attachedImage2, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _attachedImage1,
                    NSLayoutAttribute.Right, 1, DimensionHelper.CommentImageMargin),
                NSLayoutConstraint.Create(_attachedImage2, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _imageRegion,
                    NSLayoutAttribute.Top, 1, 0),
                NSLayoutConstraint.Create(_attachedImage3, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _attachedImage2,
                    NSLayoutAttribute.Right, 1, DimensionHelper.CommentImageMargin),
                NSLayoutConstraint.Create(_attachedImage3, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _imageRegion,
                    NSLayoutAttribute.Top, 1, 0)
            });

            _attachRegionContainer.AddSubview(_imageRegion);
            _attachRegionContainer.AddSubview(_videoLinkContainer);
            _attachRegionContainer.Add(_lbNoAttachedText);
            _attachRegionContainer.AddConstraints(new[]
            {
                  NSLayoutConstraint.Create(_lbNoAttachedText, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
                    _attachRegionContainer, NSLayoutAttribute.Left, 1, DimensionHelper.AboutNarrowMargin),
                NSLayoutConstraint.Create(_lbNoAttachedText, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
                    _attachRegionContainer, NSLayoutAttribute.Top, 1, 0),

                 NSLayoutConstraint.Create(_imageRegion, NSLayoutAttribute.Right, NSLayoutRelation.Equal,
                    _attachRegionContainer, NSLayoutAttribute.Right, 1, 0),
                NSLayoutConstraint.Create(_imageRegion, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
                    _attachRegionContainer, NSLayoutAttribute.Left, 1, 0),
                NSLayoutConstraint.Create(_imageRegion, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
                    _lbNoAttachedText, NSLayoutAttribute.Bottom, 1, -10),

                NSLayoutConstraint.Create(_videoLinkContainer, NSLayoutAttribute.Right, NSLayoutRelation.Equal,
                    _attachRegionContainer, NSLayoutAttribute.Right, 1, 0),
                NSLayoutConstraint.Create(_videoLinkContainer, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
                    _attachRegionContainer, NSLayoutAttribute.Left, 1, 0),
                NSLayoutConstraint.Create(_videoLinkContainer, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
                    _imageRegion, NSLayoutAttribute.Bottom, 1, 3),
                NSLayoutConstraint.Create(_videoLinkContainer, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal,
                    _attachRegionContainer, NSLayoutAttribute.Bottom, 1, 2),
            });

            #endregion

            #region Link video

            _linkVideoContainer.Add(_lbLinkVideoText);
            _linkVideoContainer.Add(_ivLinkVideoIcon);

            _linkVideoContainer.AddConstraints(new[]
            {
                NSLayoutConstraint.Create(_ivLinkVideoIcon, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
                    _linkVideoContainer, NSLayoutAttribute.Left, 1, 0),
                NSLayoutConstraint.Create(_ivLinkVideoIcon, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal,
                    _linkVideoContainer, NSLayoutAttribute.CenterY, 1, 0),
                NSLayoutConstraint.Create(_lbLinkVideoText, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
                    _ivLinkVideoIcon, NSLayoutAttribute.Right, 1, 0),
                NSLayoutConstraint.Create(_lbLinkVideoText, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal,
                    _linkVideoContainer, NSLayoutAttribute.CenterY, 1, 0)
            });

            #endregion

            #region Link picture

            _linkPictureContainer.Add(_lbLinkPictureText);
            _linkPictureContainer.Add(_ivLinkPcitureIcon);

            _linkPictureContainer.AddConstraints(new[]
            {
                NSLayoutConstraint.Create(_ivLinkPcitureIcon, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
                    _linkPictureContainer, NSLayoutAttribute.Left, 1, 0),
                NSLayoutConstraint.Create(_ivLinkPcitureIcon, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal,
                    _linkPictureContainer, NSLayoutAttribute.CenterY, 1, 0),
                NSLayoutConstraint.Create(_lbLinkPictureText, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
                    _ivLinkPcitureIcon, NSLayoutAttribute.Right, 1, 0),
                NSLayoutConstraint.Create(_lbLinkPictureText, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal,
                    _linkPictureContainer, NSLayoutAttribute.CenterY, 1, 0)
            });

            #endregion

            #region Location
            
            _locationContainer.Add(_tfLocation);
            _locationContainer.Add(_lbLocationHint);

            _locationContainer.AddConstraints(new[]
            {
                NSLayoutConstraint.Create(_tfLocation, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
                    _locationContainer, NSLayoutAttribute.Left, 1, DimensionHelper.NComplaintTextPadding),
                NSLayoutConstraint.Create(_tfLocation, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _locationContainer,
                    NSLayoutAttribute.Right, 1, 0),
                NSLayoutConstraint.Create(_tfLocation, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal,
                    _locationContainer, NSLayoutAttribute.CenterY, 1, 0),

                NSLayoutConstraint.Create(_lbLocationHint, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
                    _locationContainer, NSLayoutAttribute.Left, 1, DimensionHelper.NComplaintTextPadding),
                NSLayoutConstraint.Create(_lbLocationHint, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal,
                    _locationContainer, NSLayoutAttribute.CenterY, 1, 0),
            });
            
            #endregion

            #region Complaint content

            _complaintContentContainer.Add(_ctvComplaintContent);

            _complaintContentContainer.AddConstraints(new[]
            {
                NSLayoutConstraint.Create(_ctvComplaintContent, NSLayoutAttribute.Right, NSLayoutRelation.Equal,
                    _complaintContentContainer, NSLayoutAttribute.Right, 1, 0),
                NSLayoutConstraint.Create(_ctvComplaintContent, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
                    _complaintContentContainer, NSLayoutAttribute.Left, 1, 0),
                NSLayoutConstraint.Create(_ctvComplaintContent, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
                    _complaintContentContainer, NSLayoutAttribute.Top, 1, 0),
                NSLayoutConstraint.Create(_ctvComplaintContent, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal,
                    _complaintContentContainer, NSLayoutAttribute.Bottom, 1, 0)
            });

            #endregion
        }
    }
}