namespace GovernmentComplaint.iOS.Views.Bases
{
    public abstract class DetailView : BaseView
    {
        public PopupView PopupView => ParentViewController as PopupView;


        protected override void InitView()
        {

        }

        protected override void CreateBinding()
        {
        }
    }
}