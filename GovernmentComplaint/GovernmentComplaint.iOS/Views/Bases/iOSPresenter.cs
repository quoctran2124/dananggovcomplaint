using GovernmentComplaint.Core.ViewModels.Bases;
using MvvmCross.Core.ViewModels;
using MvvmCross.iOS.Views;
using MvvmCross.iOS.Views.Presenters;
using MvvmCross.Platform;
using System.Linq;
using UIKit;

namespace GovernmentComplaint.iOS.Views.Bases
{
    public class iOSPresenter : MvxIosViewPresenter
    {
        private MasterView _masterView;

        public iOSPresenter(IUIApplicationDelegate applicationDelegate, UIWindow window)
            : base(applicationDelegate, window)
        {
        }

        public void RegisterMasterController(MasterView masterView)
        {
            _masterView = masterView;
        }

        public override void Show(MvxViewModelRequest request)
        {
            if (request.ViewModelType.IsSubclassOf(typeof(PopupViewModel)))
            {
                base.Show(request);
            }
            else
            {
                var viewController = (UIViewController)Mvx.Resolve<IMvxIosViewCreator>().CreateView(request);

                _masterView.AddChildViewController(viewController);

                //_masterView.View.Add(viewController.View);
				_masterView.View.InsertSubviewBelow(viewController.View, _masterView.MainMenu);
				_masterView.View.AddConstraints(new[]
                {
                    NSLayoutConstraint.Create(viewController.View, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _masterView.MainMenu, NSLayoutAttribute.Bottom, 1, 0),
                    NSLayoutConstraint.Create(viewController.View, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _masterView.View, NSLayoutAttribute.Left, 1, 0),
                    NSLayoutConstraint.Create(viewController.View, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _masterView.View, NSLayoutAttribute.Right, 1, 0),
                    NSLayoutConstraint.Create(viewController.View, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _masterView.View, NSLayoutAttribute.Bottom, 1, 0)
                });

                viewController.View.TranslatesAutoresizingMaskIntoConstraints = false;
            }
        }

        public override void Close(IMvxViewModel toClose)
        {
            if (toClose is PopupViewModel)
            {
                base.Close(toClose);
            }
            else
            {
                var viewController = (MvxViewController)_masterView.ChildViewControllers
                    .FirstOrDefault(x => (x as MvxViewController).ViewModel == toClose);

				viewController.BindingContext.ClearAllBindings();
                viewController.ViewModel = null;
                viewController.View.RemoveFromSuperview();
                viewController.ViewWillUnload();
                viewController.RemoveFromParentViewController();
                viewController.Dispose();
            }
        }
    }
}