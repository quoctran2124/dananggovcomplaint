using System.Windows.Input;
using Foundation;
using GovernmentComplaint.Core.Models;
using GovernmentComplaint.Core.ViewModels.Bases;
using GovernmentComplaint.iOS.Helpers;
using GovernmentComplaint.iOS.Views.Popups;
using MvvmCross.Binding.BindingContext;
using UIKit;

namespace GovernmentComplaint.iOS.Views.Bases
{
	public partial class MasterView
	{

		private NSObject _willResignActiveNotificationObserver;
		private NSObject _didBecomeActiveNotificationObserver;

		private void ShowResendFeedbackPopup()
		{
			var bindingSet = this.CreateBindingSet<MasterView, MasterViewModel>();
			PopupLayout = new PopupResendFeedback(bindingSet);

			(PopupLayout as PopupResendFeedback).CancelButton.Clicked += delegate { IsPopupShown = false; };

			(PopupLayout as PopupResendFeedback).ConfirmButton.Clicked += delegate
			{
				IsPopupShown = false;
			};
			IsPopupShown = true;
		}

		private void ShowNoWifiPopup()
		{
			var bindingSet = this.CreateBindingSet<MasterView, MasterViewModel>();
			PopupLayout = new PopupNoWifi(bindingSet);

			(PopupLayout as PopupNoWifi).CancelButton.Clicked += delegate { IsPopupShown = false; };

			(PopupLayout as PopupNoWifi).ConfirmButton.Clicked += delegate
			{
				UIApplication.SharedApplication.OpenUrl(new NSUrl("App-Prefs:root=Settings"));
				IsPopupShown = false;
			};
			IsPopupShown = true;
		}

	    private void ShowAnswerFeedbackPopup()
	    {
	        var bindingSet = this.CreateBindingSet<MasterView, MasterViewModel>();
	        PopupLayout = new PopupAnswerFeedback(bindingSet);
	        var satisfiedCheckbox = (PopupLayout as PopupAnswerFeedback).SatisfiedCheckbox;
	        var unSatisfiedCheckbox = (PopupLayout as PopupAnswerFeedback).UnSatisfiedCheckbox;
	        var slowResponseCheckbox = (PopupLayout as PopupAnswerFeedback).SlowResponseCheckbox;
	        var unSuitAnswerCheckbox = (PopupLayout as PopupAnswerFeedback).UnSuitAnswerCheckbox;
	        var errorMessageLayout = (PopupLayout as PopupAnswerFeedback).ErrorMessageLayout;

            var unSatisfiedLayout = (PopupLayout as PopupAnswerFeedback).UnSatisfiedOptionLayout;
	        unSatisfiedLayout.Hidden = true;
	        errorMessageLayout.Hidden = true;

            IsSatisfySelected = true;
	        IsSlowResponseSelected = false;
	        IsUnSuitAnswerSelected = false;


            (PopupLayout as PopupAnswerFeedback).SatisfiedView.Clicked += delegate
	        {
	            IsSatisfySelected = true;

                satisfiedCheckbox.Image = UIImage.FromFile(ImageHelper.RadioButtonChecked);
	            unSatisfiedCheckbox.Image = UIImage.FromFile(ImageHelper.RadioButtonUnChecked);
	            unSatisfiedLayout.Hidden = true;
            };

	        (PopupLayout as PopupAnswerFeedback).UnSatisfiedView.Clicked += delegate
	        {
	            IsSatisfySelected = false;

                satisfiedCheckbox.Image = UIImage.FromFile(ImageHelper.RadioButtonUnChecked);
	            unSatisfiedCheckbox.Image = UIImage.FromFile(ImageHelper.RadioButtonChecked);
	            unSatisfiedLayout.Hidden = false;
	        };

	        (PopupLayout as PopupAnswerFeedback).SlowResponseView.Clicked += delegate
	        {
	            IsSlowResponseSelected = !IsSlowResponseSelected;
                
                slowResponseCheckbox.Image = UIImage.FromFile(IsSlowResponseSelected ? ImageHelper.CheckboxChecked : ImageHelper.CheckboxUnChecked);
	        };

	        (PopupLayout as PopupAnswerFeedback).UnSuitAnswerView.Clicked += delegate
	        {
	            IsUnSuitAnswerSelected = !IsUnSuitAnswerSelected;

	            unSuitAnswerCheckbox.Image = UIImage.FromFile(IsUnSuitAnswerSelected ? ImageHelper.CheckboxChecked : ImageHelper.CheckboxUnChecked);
            };

            (PopupLayout as PopupAnswerFeedback).CancelButton.Clicked += delegate { IsPopupShown = false; };

	        (PopupLayout as PopupAnswerFeedback).ConfirmButton.Clicked += delegate
	        {
	            if (IsSatisfySelected == false && IsSlowResponseSelected == false && IsUnSuitAnswerSelected == false)
	            {
	                errorMessageLayout.Hidden = false;
	                return;
	            }

                AnswerFeedbackContent answerFeedbackContent = new AnswerFeedbackContent()
	            {
	                SatisfySelected = IsSatisfySelected,
	                SlowResponseSelected = IsSlowResponseSelected,
	                UnSuitAnswerSelected = IsUnSuitAnswerSelected
	            };
	            
                SendAnswerFeedback?.Execute(answerFeedbackContent);
                IsPopupShown = false;
	        };
	        IsPopupShown = true;
	    }

        private void ShowConnectionErrorPopup()
		{
			var bindingSet = this.CreateBindingSet<MasterView, MasterViewModel>();
			PopupLayout = new PopupConnectionError(bindingSet);

			(PopupLayout as PopupConnectionError).CancelButton.Clicked += delegate { IsPopupShown = false; };

			(PopupLayout as PopupConnectionError).ConfirmButton.Clicked += delegate
			{
				IsPopupShown = false;
			};
			IsPopupShown = true;
		}

		private void OnOverlayLayoutClicked()
		{
			_popupLayout.Hidden = true;
			_popupContainer.Hidden = true;
		}

		private void ShowRatingPopup()
		{
			var bindingSet = this.CreateBindingSet<MasterView, MasterViewModel>();
			PopupLayout = new PopupRating(bindingSet);
			(PopupLayout as PopupRating).CancelButton.Clicked += delegate { IsPopupShown = false; };
			IsPopupShown = true;
		}

		private void ShowPopupOfflineOptions()
		{
			var bindingSet = this.CreateBindingSet<MasterView, MasterViewModel>();
			PopupLayout = new PopupOfflineOptions(bindingSet);

			(PopupLayout as PopupOfflineOptions).CancelButton.Clicked += delegate { IsPopupShown = false; };

			(PopupLayout as PopupOfflineOptions).GoToWifiSettingsLink.Clicked += delegate
			{
				UIApplication.SharedApplication.OpenUrl(new NSUrl("App-Prefs:root=Settings"));
				IsPopupShown = false;
			};
			IsPopupShown = true;
		}

	 protected override void OnWillHideKeyboard(NSNotification notification)
		{
			base.OnWillHideKeyboard(notification);

			if (PopupLayout is PopupVideo && !IsKeyboardShown)
			{
				IsKeyboardShown = true;
				var f = PopupLayout.Frame;
				f.Y += KeyboardHeight;
				PopupLayout.Frame = f;
			}
		}

		protected override void OnWillShowKeyboard(NSNotification notification)
		{
			base.OnWillShowKeyboard(notification);

			if (PopupLayout is PopupVideo)
			{
				var f = PopupLayout.Frame;
				f.Y -= KeyboardHeight;
				PopupLayout.Frame = f;
			}
		}

		private void ShowVideoPopup()
		{
			var bindingSet = this.CreateBindingSet<MasterView, MasterViewModel>();
			PopupLayout = new PopupVideo(bindingSet);

			(PopupLayout as PopupVideo).CancelPopupButton.Clicked += delegate { IsPopupShown = false; };

			IsPopupShown = true;
		}

		private void ShowCancelPopup()
		{
			var bindingSet = this.CreateBindingSet<MasterView, MasterViewModel>();
			PopupLayout = new PopupCancel(bindingSet);

			(PopupLayout as PopupCancel).CancelCancelButton.Clicked += delegate { IsPopupShown = false; };


			IsPopupShown = true;
		}

		private void ShowLogoutPopup()
		{
			var bindingSet = this.CreateBindingSet<MasterView, MasterViewModel>();
			PopupLayout = new PopupLogout(bindingSet);

			(PopupLayout as PopupLogout).CancelButton.Clicked += delegate { IsPopupShown = false; };


			IsPopupShown = true;
		}

		//private void ShowEmailPopup()
		//{
		//    var bindingSet = this.CreateBindingSet<MasterView, MasterViewModel>();
		//    PopupLayout = new PopupEmail(bindingSet);
		//    IsPopupShown = true;
		//}

		//private void ShowLoginPopup()
		//{
		//    var bindingSet = this.CreateBindingSet<MasterView, MasterViewModel>();
		//    PopupLayout = new PopupLogin(bindingSet);

		//    (PopupLayout as PopupLogin).CancelButton.Clicked += delegate { IsPopupShown = false; };


		//    IsPopupShown = true;
		//}

		private void ShowAttachedImagePopup()
		{
			var bindingSet = this.CreateBindingSet<MasterView, MasterViewModel>();
			PopupLayout = new PopupAttachedImage(bindingSet);
			(PopupLayout as PopupAttachedImage).IvCloseButton.Clicked += delegate { IsPopupShown = false; };

			IsPopupShown = true;
		}

		private void ShowDropdownPopup()
		{
			var bindingSet = this.CreateBindingSet<MasterView, MasterViewModel>();
			PopupLayout = new PopupDropdown(bindingSet);

			(PopupLayout as PopupDropdown).IvCloseButton.Clicked += delegate { IsPopupShown = false; };

			IsPopupShown = true;
		}

		private void ShowFail24HPopup()
		{
			var bindingSet = this.CreateBindingSet<MasterView, MasterViewModel>();
			PopupLayout = new PopupFail24H(bindingSet);

			(PopupLayout as PopupFail24H).ConfirmButton.Clicked += delegate { IsPopupShown = false; };

			IsPopupShown = true;
		}

		private void ShowNoCommentPopup()
		{
			var bindingSet = this.CreateBindingSet<MasterView, MasterViewModel>();
			PopupLayout = new PopupNoComment(bindingSet);

			(PopupLayout as PopupNoComment).ConfirmButton.Clicked += delegate { IsPopupShown = false; };

			IsPopupShown = true;
		}

		private void ShowCancelCommentPopup()
		{
			var bindingSet = this.CreateBindingSet<MasterView, MasterViewModel>();
			PopupLayout = new PopupCancelComment(bindingSet);

			(PopupLayout as PopupCancelComment).ConfirmCancelButton.Clicked += delegate { IsPopupShown = false; };

			(PopupLayout as PopupCancelComment).CancelCancelButton.Clicked += delegate { IsPopupShown = false; };
			IsPopupShown = true;
		}

		private void ShowErrorImages()
		{
			var bindingSet = this.CreateBindingSet<MasterView, MasterViewModel>();
			PopupLayout = new PopupErrorImages(bindingSet);

			(PopupLayout as PopupErrorImages).ConfirmButton.Clicked += delegate { IsPopupShown = false; };

			IsPopupShown = true;
		}

        #region Properties

	    public ICommand SendAnswerFeedback { get; set; }

        public bool IsSatisfySelected { get; set; }
	    public bool IsSlowResponseSelected { get; set; }
	    public bool IsUnSuitAnswerSelected { get; set; }

        private bool _isNoWifiPopupShown;

		public bool IsNoWifiPopupShown
		{
			get { return _isNoWifiPopupShown; }
			set
			{
				_isNoWifiPopupShown = value;
				if (value)
					ShowNoWifiPopup();
				else
					IsPopupShown = false;
			}
		}



	    private bool _isAnswerFeedbackPopupShown;

	    public bool IsAnswerFeedbackPopupShown
        {
	        get { return _isAnswerFeedbackPopupShown; }
	        set
	        {
	            _isAnswerFeedbackPopupShown = value;
	            if (value)
	                ShowAnswerFeedbackPopup();
	            else
	                IsPopupShown = false;
	        }
	    }


        private bool _isConnectionErrorPopupShown;

		public bool IsConnectionErrorPopupShown
		{
			get { return _isConnectionErrorPopupShown; }
			set
			{
				_isConnectionErrorPopupShown = value;
				if (value)
					ShowConnectionErrorPopup();
				else
					IsPopupShown = false;
			}
		}

		private bool _isErrorImagesShown;

		public bool IsErrorImagesShown
		{
			get { return _isErrorImagesShown; }
			set
			{
				_isErrorImagesShown = value;
				if (value)
					ShowErrorImages();
				else
					IsPopupShown = false;
			}
		}

		private bool _isFail24HPopupShown;

		public bool IsFail24HPopupShown
		{
			get { return _isFail24HPopupShown; }
			set
			{
				_isFail24HPopupShown = value;
				if (value)
					ShowFail24HPopup();
				else
					IsPopupShown = false;
			}
		}

		private bool _isNoCommentPopupShown;

		public bool IsNoCommentPopupShown
		{
			get { return _isNoCommentPopupShown; }
			set
			{
				_isNoCommentPopupShown = value;
				if (value)
					ShowNoCommentPopup();
				else
					IsPopupShown = false;
			}
		}

		private bool _isCancelCommentPopupShown;
		public bool IsCancelCommentPopupShown
		{
			get { return _isCancelCommentPopupShown; }
			set
			{
				_isCancelCommentPopupShown = value;
				if (value)
					ShowCancelCommentPopup();
				else
					IsPopupShown = false;
			}
		}

		private bool _isRatingPopupShown;

		public bool IsRatingPopupShown
		{
			get { return _isRatingPopupShown; }
			set
			{
				_isRatingPopupShown = value;
				if (value)
					ShowRatingPopup();
				else
				{
					IsPopupShown = false;
				}
			}
		}

		private bool _isVideoPopupShown;

		public bool IsVideoPopupShown
		{
			get { return _isVideoPopupShown; }
			set
			{
				_isVideoPopupShown = value;
				if (value)
					ShowVideoPopup();
				else
					IsPopupShown = false;
			}
		}

		private bool _isCancelPopupShown;

		public bool IsCancelPopupShown
		{
			get { return _isCancelPopupShown; }
			set
			{
				_isCancelPopupShown = value;
				if (value)
					ShowCancelPopup();
				else
					IsPopupShown = false;
			}
		}

		private bool _isLogoutPopupShown;

		public bool IsLogoutPopupShown
		{
			get { return _isLogoutPopupShown; }
			set
			{
				_isLogoutPopupShown = value;
				if (value)
					ShowLogoutPopup();
				else
					IsPopupShown = false;
			}
		}

		private bool _isResendFeedbackPopupShown;

		public bool IsResendFeedbackPopupShown
		{
			get { return _isResendFeedbackPopupShown; }
			set
			{
				_isResendFeedbackPopupShown = value;
				if (value)
					ShowResendFeedbackPopup();
				else
					IsPopupShown = false;
			}
		}


		private bool _isPopupOfflineOptionsShown;
		public bool IsPopupOfflineOptionsShown
		{
			get { return _isPopupOfflineOptionsShown; }
			set
			{
				_isPopupOfflineOptionsShown = value;
				if (value)
				{
					ShowPopupOfflineOptions();
				}
				else
				{
					IsPopupShown = false;
				}
			}
		}


		//private bool _isRequestInfoPopupShown;

		//public bool IsRequestInfoPopupShown
		//{
		//    get { return _isRequestInfoPopupShown; }
		//    set
		//    {
		//        _isRequestInfoPopupShown = value;
		//        if (value)
		//            ShowEmailPopup();
		//        else
		//            IsPopupShown = false;
		//    }
		//}

		//private bool _isLoginPopupShown;

		//public bool IsLoginPopupShown
		//{
		//    get { return _isLoginPopupShown; }
		//    set
		//    {
		//        _isLoginPopupShown = value;
		//        if (value)
		//            ShowLoginPopup();
		//        else
		//            IsPopupShown = false;
		//    }
		//}

		private bool _isDropdownPopupShown;
		public bool IsDropdownPopupShown
		{
			get { return _isDropdownPopupShown; }
			set
			{
				_isDropdownPopupShown = value;
				if (value)
					ShowDropdownPopup();
				else
					IsPopupShown = false;
			}
		}

		private bool _isAttachedImagesPopupShown;
		public bool IsAttachedImagesPopupShown
		{
			get { return _isAttachedImagesPopupShown; }
			set
			{
				_isAttachedImagesPopupShown = value;
				if (value)
					ShowAttachedImagePopup();
				else
					IsPopupShown = false;
			}
		}

		#endregion

		private void AppWillResignActive(NSNotification obj)
		{
			View.EndEditing(true);
		}

		private void AppWillTerminate(NSNotification obj)
		{
			if (_willResignActiveNotificationObserver != null)
			{
				NSNotificationCenter.DefaultCenter.RemoveObserver(_willResignActiveNotificationObserver);
			}

			if (_didBecomeActiveNotificationObserver != null)
			{
				NSNotificationCenter.DefaultCenter.RemoveObserver(_didBecomeActiveNotificationObserver);
			}
		}
	}
}