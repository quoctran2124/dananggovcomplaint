using System;
using Foundation;
using GovernmentComplaint.iOS.Helpers;
using MvvmCross.iOS.Views;
using UIKit;

namespace GovernmentComplaint.iOS.Views.Bases
{
	public abstract class BaseView : MvxViewController
	{
		public bool IsVisible { get; private set; }

		public override void ViewDidLoad()
		{
			View = new UIView { BackgroundColor = UIColor.White };
			View.MultipleTouchEnabled = false;

			NavigationController.NavigationBarHidden = true;

			// ios7 layout
			if (RespondsToSelector(new ObjCRuntime.Selector("edgesForExtendedLayout")))
			{
				EdgesForExtendedLayout = UIRectEdge.None;
			}

			base.ViewDidLoad();

			InitView();
			CreateBinding();
		}

		public override void DidRotate(UIInterfaceOrientation fromInterfaceOrientation)
		{
			ResolutionHelper.RefreshStaticVariable();

			base.DidRotate(fromInterfaceOrientation);
		}

		private NSObject _willResignActiveNotificationObserver;
		private NSObject _didBecomeActiveNotificationObserver;

		public override void ViewWillAppear(bool animated)
		{
			IsVisible = true;

			base.ViewWillAppear(animated);
			_willResignActiveNotificationObserver = NSNotificationCenter.DefaultCenter.AddObserver(UIKeyboard.WillHideNotification, OnWillHideKeyboard);
			_didBecomeActiveNotificationObserver = NSNotificationCenter.DefaultCenter.AddObserver(UIKeyboard.WillShowNotification, OnWillShowKeyboard);
		}

		public override void ViewWillDisappear(bool animated)
		{
			base.ViewWillDisappear(animated);

			if (_willResignActiveNotificationObserver != null)
			{
				NSNotificationCenter.DefaultCenter.RemoveObserver(_willResignActiveNotificationObserver);
			}

			if (_didBecomeActiveNotificationObserver != null)
			{
				NSNotificationCenter.DefaultCenter.RemoveObserver(_didBecomeActiveNotificationObserver);
			}

			IsVisible = false;
		}

		protected abstract void InitView();

		protected abstract void CreateBinding();

		protected bool IsKeyboardShown;
		protected nfloat KeyboardHeight;

		protected virtual void OnWillShowKeyboard(NSNotification notification)
		{
			var visible = notification.Name == UIKeyboard.WillShowNotification;
			var keyboardFrame = visible
				? UIKeyboard.FrameEndFromNotification(notification)
				: UIKeyboard.FrameBeginFromNotification(notification);

			if (!IsKeyboardShown)
			{
				KeyboardHeight = keyboardFrame.Height / 2;
			}
		}

		protected virtual void OnWillHideKeyboard(NSNotification notification)
		{
			IsKeyboardShown = false;
		}

		protected void HideKeyboard()
		{
			View.EndEditing(true);
		}

		public abstract void WillEnterForeground();
	}
}
