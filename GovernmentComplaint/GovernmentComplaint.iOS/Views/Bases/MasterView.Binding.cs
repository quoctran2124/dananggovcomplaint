
using GovernmentComplaint.Core.ViewModels.Bases;
using MvvmCross.Binding.BindingContext;

namespace GovernmentComplaint.iOS.Views.Bases
{
	public partial class MasterView
	{
		private bool _isSearchBoxShown;

		public bool IsSearchBoxShown
		{
			get { return _isSearchBoxShown; }
			set
			{
				_isSearchBoxShown = value;
				SearchboxLayout.Hidden = value;

				if (value)
				{
					HideKeyboard();
				}
			}
		}

		protected override void CreateBinding()
		{
			base.CreateBinding();

             var bindingSet = this.CreateBindingSet<MasterView, MasterViewModel>();

			//MainMenu
			bindingSet.Bind(_ivLeftButton).For("Icon").To(vm => vm.TopLeftButtonType).WithConversion("HeaderButtonTypeToImage");
			bindingSet.Bind(_ivLeftButton).For(v => v.Hidden).To(vm => vm.IsLeftButtonShown).WithConversion("InvertBool");
			bindingSet.Bind(_leftButtonContainer).For(v => v.Hidden).To(vm => vm.IsLeftButtonShown).WithConversion("InvertBool");

			bindingSet.Bind(_topLeftButtonLayout).For(v => v.ClickCommand).To(vm => vm.LeftButtonClickCommand);

			bindingSet.Bind(_ivRightButton).For("Icon").To(vm => vm.TopRightButtonType).WithConversion("HeaderButtonTypeToImage");

			bindingSet.Bind(_rightButtonContainer).For(v => v.Hidden).To(vm => vm.IsRightButtonShown).WithConversion("InvertBool");

			bindingSet.Bind(_topRightButtonLayout).For(v => v.ClickCommand).To(vm => vm.RightButtonClickCommand);
			bindingSet.Bind(_ivDialButton).For(v => v.ClickCommand).To(vm => vm.CallHotlineCommand);

			//Sidebar
			bindingSet.Bind(this).For(x => x.IsSidebarLayoutShown).To(vm => vm.IsSidebarShown).TwoWay();

			//Overlay
			bindingSet.Bind(_overlaySidebarLayout).For(v => v.Hidden).To(vm => vm.IsSidebarShown).WithConversion("InvertBool");
			bindingSet.Bind(_overlaySidebarLayout).For(v => v.ClickCommand).To(vm => vm.OverlaySidebarClickCommand);

			bindingSet.Bind(_overlaySearchBox).For(v => v.Hidden).To(vm => vm.IsSearchBoxShown).WithConversion("InvertBool");
			bindingSet.Bind(_overlaySearchBox).For(v => v.ClickCommand).To(vm => vm.OverlaySearchBoxClickCommand);

			//SearchBox
			bindingSet.Bind(this).For(v => v.IsSearchBoxShown).To(vm => vm.IsSearchBoxShown).WithConversion("InvertBool");

			// Popups
			bindingSet.Bind(this).For(x => x.IsVideoPopupShown).To(vm => vm.IsVideoPopupShown);
			bindingSet.Bind(this).For(x => x.IsCancelPopupShown).To(vm => vm.IsCancelPopupShown);
			bindingSet.Bind(this).For(x => x.IsLogoutPopupShown).To(vm => vm.IsLogoutPopupShown);
			bindingSet.Bind(this).For(x => x.IsDropdownPopupShown).To(vm => vm.IsDropdownPopupShown);
			bindingSet.Bind(this).For(x => x.IsRatingPopupShown).To(vm => vm.IsRatingPopupShown);
			bindingSet.Bind(this).For(x => x.IsFail24HPopupShown).To(vm => vm.IsFail24HPopupShown);
			bindingSet.Bind(this).For(x => x.IsNoWifiPopupShown).To(vm => vm.IsNoWifiPopupShown);

            bindingSet.Bind(this).For(x => x.IsAnswerFeedbackPopupShown).To(vm => vm.IsAnswerFeedbackPopupShown);


            bindingSet.Bind(this).For(x => x.IsConnectionErrorPopupShown).To(vm => vm.IsConnectionErrorPopupShown);
			bindingSet.Bind(this).For(x => x.IsErrorImagesShown).To(vm => vm.IsErrorImagesShown);
			bindingSet.Bind(this).For(x => x.IsNoCommentPopupShown).To(vm => vm.IsNoCommentPopupShown);
			bindingSet.Bind(this).For(x => x.IsAttachedImagesPopupShown).To(vm => vm.IsAttachedImagesPopupShown);
			bindingSet.Bind(this).For(x => x.IsCancelCommentPopupShown).To(vm => vm.IsCancelCommentPopupShown);
			bindingSet.Bind(this).For(x => x.IsResendFeedbackPopupShown).To(vm => vm.IsResendFeedbackPopupShown);

			bindingSet.Bind(this).For(x => x.IsPopupOfflineOptionsShown).To(vm => vm.IsPopupOfflineOptionsShown);

			//Title bar
			bindingSet.Bind(_titleBar).For(v => v.Text).To(vm => vm.TitleBar);
			bindingSet.Bind(this).For(v => v.LoginCommand).To(vm => vm.LoginCommand);

		    bindingSet.Bind(this).For(x => x.SendAnswerFeedback).To(vm => vm.SendAnswerFeedback);

            bindingSet.Apply();
		}

		public override void WillEnterForeground()
		{
		}
	}
}