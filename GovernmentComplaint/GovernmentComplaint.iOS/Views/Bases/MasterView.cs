﻿using System;
using System.Drawing;
using System.Windows.Input;
using CoreAnimation;
using CoreGraphics;
using Facebook.CoreKit;
using Facebook.LoginKit;
using Foundation;
using Google.SignIn;
using GovernmentComplaint.Core.Enums;
using GovernmentComplaint.Core.Models;
using GovernmentComplaint.Core.ViewModels.Bases;
using GovernmentComplaint.iOS.Controls;
using GovernmentComplaint.iOS.Helpers;
using MvvmCross.Binding.BindingContext;
using MvvmCross.iOS.Views.Presenters;
using MvvmCross.Platform;
using UIKit;

namespace GovernmentComplaint.iOS.Views.Bases
{
	public partial class MasterView : PopupView, ISignInDelegate, ISignInUIDelegate
	{
		#region Properties

		private readonly string appId = "551193541690039";
		private readonly string appName = "Ứng dụng góp ý";

		private bool _isSidebarLayoutShown;
		public ICommand LoginCommand { get; set; }

		public bool IsSidebarLayoutShown
		{
			get { return _isSidebarLayoutShown; }
			set
			{
				_isSidebarLayoutShown = value;
				{
					if (value)
						ToggleMenuItem();
					else
						HideMenuItem();
				}
			}
		}
		#endregion

		private CAGradientLayer GetGradientLayer(CGRect cgRect)
		{
			var gradientLayer = new CAGradientLayer
			{
				Frame = cgRect,
				NeedsDisplayOnBoundsChange = true,
				Colors = new[]
				{
					ColorHelper.CenterButton.CGColor, ColorHelper.EndButton.CGColor, ColorHelper.DropdownBarEnd.CGColor
				}
			};

			return gradientLayer;
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			FacebookSingleSignOn();
		}

		public override void ViewDidLayoutSubviews()
		{
			base.ViewDidLayoutSubviews();

			var gradientLayer = GetGradientLayer(SearchboxLayout.DropdownContainer.Bounds);
			gradientLayer.CornerRadius = DimensionHelper.ToastCorner;
			gradientLayer.BorderWidth = 1;
			gradientLayer.BorderColor = ColorHelper.BorderGray.CGColor;

			SearchboxLayout.DropdownContainer.Layer.InsertSublayer(gradientLayer, 0);
		}

		protected override void InitView()
		{
			Settings.AppID = appId;
			Settings.DisplayName = appName;
			var login = new LoginManager()
			{
				LoginBehavior = LoginBehavior.Native
			};
			Profile.EnableUpdatesOnAccessTokenChange(true);
			Profile.Notifications.ObserveDidChange((sender, e) => {
				RetrieveFacebookUser();
			});

			SignIn.SharedInstance.Delegate = this;
			SignIn.SharedInstance.UIDelegate = this;
			SignIn.SharedInstance.SignInUserSilently();

			CreateMainMenu();

			_horizontalDivider = UIHelper.CreateView(0, DimensionHelper.DividerWeight, ColorHelper.NeutralBlue);
			_overlaySidebarLayout = UIHelper.CreateAlphaView(0, 0, 0, ColorHelper.Overlay);
			_overlaySearchBox = UIHelper.CreateAlphaView(0, 0, 0, ColorHelper.Overlay);

			SidebarLayout = new SidebarLayout(this.CreateBindingSet<MasterView, MasterViewModel>())
			{
				Hidden = true
			};

			SearchboxLayout = new SearchboxLayout(this.CreateBindingSet<MasterView, MasterViewModel>())
			{
				Hidden = true
			};

			View.Add(MainMenu);
			View.Add(_horizontalDivider);
			View.Add(_overlaySidebarLayout);
			View.Add(SidebarLayout);
			View.Add(SearchboxLayout);
			View.Add(_overlaySearchBox);

			View.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(MainMenu, NSLayoutAttribute.Top, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Top, 1, ResolutionHelper.StatusHeight),
				NSLayoutConstraint.Create(MainMenu, NSLayoutAttribute.Left, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(MainMenu, NSLayoutAttribute.Width, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Width, 1, 0),
				NSLayoutConstraint.Create(_horizontalDivider, NSLayoutAttribute.Top, NSLayoutRelation.Equal, MainMenu,
					NSLayoutAttribute.Bottom, 1, 0),
				NSLayoutConstraint.Create(_horizontalDivider, NSLayoutAttribute.Width, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Width, 1, 0),
				NSLayoutConstraint.Create(SidebarLayout, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_horizontalDivider,
					NSLayoutAttribute.Bottom, 1, 0),
				NSLayoutConstraint.Create(SidebarLayout, NSLayoutAttribute.Right, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Right, 1, 0),
				NSLayoutConstraint.Create(SidebarLayout, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Bottom, 1, 0),
				NSLayoutConstraint.Create(_overlaySidebarLayout, NSLayoutAttribute.Top, NSLayoutRelation.Equal, MainMenu,
					NSLayoutAttribute.Bottom, 1, 0),
				NSLayoutConstraint.Create(_overlaySidebarLayout, NSLayoutAttribute.Left, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_overlaySidebarLayout, NSLayoutAttribute.Right, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Right, 1, 0),
				NSLayoutConstraint.Create(_overlaySidebarLayout, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Bottom, 1, 0),
				NSLayoutConstraint.Create(SearchboxLayout, NSLayoutAttribute.Top, NSLayoutRelation.Equal, MainMenu,
					NSLayoutAttribute.Bottom, 1, 0),
				NSLayoutConstraint.Create(SearchboxLayout, NSLayoutAttribute.Width, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Width, 1, 0),
				NSLayoutConstraint.Create(SearchboxLayout, NSLayoutAttribute.Left, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(SearchboxLayout, NSLayoutAttribute.Height, NSLayoutRelation.Equal, null,
					NSLayoutAttribute.NoAttribute, 1, DimensionHelper.SearchBoxHeight),
				NSLayoutConstraint.Create(_overlaySearchBox, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					SearchboxLayout,
					NSLayoutAttribute.Bottom, 1, 0),
				NSLayoutConstraint.Create(_overlaySearchBox, NSLayoutAttribute.Left, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_overlaySearchBox, NSLayoutAttribute.Right, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Right, 1, 0),
				NSLayoutConstraint.Create(_overlaySearchBox, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Bottom, 1, 0)
			});

			var presenter = (iOSPresenter) Mvx.Resolve<IMvxIosViewPresenter>();
			presenter.RegisterMasterController(this);
			SidebarLayout.Hidden = true;
			_overlaySidebarLayout.Hidden = true;
			_willResignActiveNotificationObserver =
				NSNotificationCenter.DefaultCenter.AddObserver(UIApplication.WillResignActiveNotification, AppWillResignActive);
			_didBecomeActiveNotificationObserver =
				NSNotificationCenter.DefaultCenter.AddObserver(UIApplication.WillTerminateNotification, AppWillTerminate);
			base.InitView();
		}

		private void FacebookSingleSignOn()
		{
			if(AccessToken.CurrentAccessToken != null)
			{
				RetrieveFacebookUser();
			}
		}

		public void RetrieveFacebookUser()
		{
			var profile = Profile.CurrentProfile;
			if (profile != null)
			{
				var user = new UserInfo
				{
					AccountType = AccountType.FACEBOOK,
					UserId = profile.UserID,
					AvatarUrl = profile.ImageUrl(ProfilePictureMode.Normal, new CGSize(500, 500)).ToString(),
					Email = "",
					UserName = profile.Name,
				};
				LoginCommand?.Execute(user);
			}
		}

		private void HideMenuItem()
		{
			AnimationHelper.Fade(SidebarLayout, false, 0.1f, delegate
			{
				SidebarLayout.Hidden = true;
				_overlaySidebarLayout.Hidden = true;
			});

			AnimationHelper.Fade(_overlaySidebarLayout, false, 0.15f);
		}

		private void ToggleMenuItem()
		{
			if (SidebarLayout.Hidden)
			{
				SidebarLayout.Hidden = false;
				_overlaySidebarLayout.Hidden = false;

				AnimationHelper.Fade(SidebarLayout, true, 0.1f);

				AnimationHelper.SlideMenu(SidebarLayout, true, 0.25f, delegate
				{
					SidebarLayout.Transform = CGAffineTransform.MakeIdentity();
					_overlaySidebarLayout.UserInteractionEnabled = true;
				});

				AnimationHelper.Fade(_overlaySidebarLayout, true, 0.15f);
			}
			else
			{
				HideMenuItem();
			}
		}

		public static UIImage MaxResizeImage(UIImage sourceImage, nfloat maxWidth, nfloat maxHeight)
		{
			if (sourceImage != null)
			{
				var sourceSize = sourceImage.Size;
				var maxResizeFactor = Math.Max(maxWidth / sourceSize.Width, maxHeight / sourceSize.Height);
				if (maxResizeFactor > 1) return sourceImage;
				var width = maxResizeFactor * sourceSize.Width;
				var height = maxResizeFactor * sourceSize.Height;
				UIGraphics.BeginImageContext(new SizeF((float) width, (float) height));
				sourceImage.Draw(new RectangleF(0, 0, (float) width, (float) height));
				var resultImage = UIGraphics.GetImageFromCurrentImageContext();
				UIGraphics.EndImageContext();
				return resultImage;
			}
			return null;
		}

		private void CreateMainMenu()
		{
			ResolutionHelper.InitStaticVariable();

			MainMenu = UIHelper.CreateView(0, DimensionHelper.MainMenuHeight, ColorHelper.ThemeBlue);
			MainMenu.BackgroundColor =
				UIColor.FromPatternImage(MaxResizeImage(UIImage.FromFile(ImageHelper.MainMenuImage),
					UIScreen.MainScreen.Bounds.Width, DimensionHelper.MainMenuHeight));

			_titleBar = UIHelper.CreateLabel(ColorHelper.White, DimensionHelper.BigTextSize);
			_verticalDivider = UIHelper.CreateView(DimensionHelper.DividerWeight, 0, ColorHelper.NeutralBlue);
			_ivDialButton = UIHelper.CreateAlphaImageView(0, 0, DimensionHelper.ImageViewPadding);
			_ivDialButton.Image = UIImage.FromBundle(ImageHelper.HotLineIcon);

			_ivLeftButton = UIHelper.CreateButton(0, DimensionHelper.MainMenuButtonSize,
				UIColor.Black, DimensionHelper.BigTextSize, ColorHelper.Transparent);
			_ivLeftButton.UserInteractionEnabled = false;
			_leftButtonContainer = UIHelper.CreateDisappearableLayout(DimensionHelper.MainMenuButtonSize, ColorHelper.Transparent);
			_leftButtonContainer.WidthWhenHidden = DimensionHelper.LayoutMargin;

			_topLeftButtonLayout = UIHelper.CreateAlphaView(DimensionHelper.MainMenuButtonSize,
				DimensionHelper.MainMenuButtonSize, 0);
			_topLeftButtonLayout.Add(_ivLeftButton);
			_topLeftButtonLayout.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_ivLeftButton, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					_topLeftButtonLayout, NSLayoutAttribute.Left, 1, DimensionHelper.ImageViewPadding),
				NSLayoutConstraint.Create(_ivLeftButton, NSLayoutAttribute.Right, NSLayoutRelation.Equal,
					_topLeftButtonLayout, NSLayoutAttribute.Right, 1, -DimensionHelper.ImageViewPadding),
				NSLayoutConstraint.Create(_ivLeftButton, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_topLeftButtonLayout, NSLayoutAttribute.Top, 1, DimensionHelper.ImageViewPadding),
				NSLayoutConstraint.Create(_ivLeftButton, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal,
					_topLeftButtonLayout, NSLayoutAttribute.Bottom, 1, -DimensionHelper.ImageViewPadding)
			});

			_leftButtonContainer.Add(_topLeftButtonLayout);
			_leftButtonContainer.AddConstraints(new []
			{
				NSLayoutConstraint.Create(_topLeftButtonLayout, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _leftButtonContainer, NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_topLeftButtonLayout, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, _leftButtonContainer, NSLayoutAttribute.CenterY, 1, 0),
			});

			_ivRightButton = UIHelper.CreateButton(0, DimensionHelper.MainMenuButtonSize,
				UIColor.Black, DimensionHelper.BigTextSize, ColorHelper.Transparent);
			_ivRightButton.UserInteractionEnabled = false;

			_rightButtonContainer = UIHelper.CreateDisappearableLayout(DimensionHelper.MainMenuButtonSize,
				ColorHelper.Transparent);

			_topRightButtonLayout = UIHelper.CreateAlphaView(DimensionHelper.MainMenuButtonSize,
				DimensionHelper.MainMenuButtonSize, 0);
			
			_topRightButtonLayout.Add(_ivRightButton);
			_topRightButtonLayout.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_ivRightButton, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					_topRightButtonLayout, NSLayoutAttribute.Left, 1, DimensionHelper.ImageViewPadding),
				NSLayoutConstraint.Create(_ivRightButton, NSLayoutAttribute.Right, NSLayoutRelation.Equal,
					_topRightButtonLayout, NSLayoutAttribute.Right, 1, -DimensionHelper.ImageViewPadding),
				NSLayoutConstraint.Create(_ivRightButton, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					_topRightButtonLayout, NSLayoutAttribute.Top, 1, DimensionHelper.ImageViewPadding),
				NSLayoutConstraint.Create(_ivRightButton, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal,
					_topRightButtonLayout, NSLayoutAttribute.Bottom, 1, -DimensionHelper.ImageViewPadding)
			});

			_rightButtonContainer.Add(_topRightButtonLayout);
			_rightButtonContainer.AddConstraints(new []
			{
				NSLayoutConstraint.Create(_topRightButtonLayout, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _rightButtonContainer, NSLayoutAttribute.CenterX, 1, 0), 
				NSLayoutConstraint.Create(_topRightButtonLayout, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, _rightButtonContainer, NSLayoutAttribute.CenterY, 1, 0), 
			});

			MainMenu.AddSubview(_leftButtonContainer);
			MainMenu.AddSubview(_titleBar);
			MainMenu.AddSubview(_rightButtonContainer);
			MainMenu.AddSubview(_verticalDivider);
			MainMenu.AddSubview(_ivDialButton);

			MainMenu.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_leftButtonContainer, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					MainMenu, NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_leftButtonContainer, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					MainMenu, NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_leftButtonContainer, NSLayoutAttribute.Height, NSLayoutRelation.Equal,
					MainMenu, NSLayoutAttribute.Height, 1, 0),
				NSLayoutConstraint.Create(_titleBar, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal,
					MainMenu, NSLayoutAttribute.CenterY, 1, 0),
				NSLayoutConstraint.Create(_titleBar, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					_leftButtonContainer, NSLayoutAttribute.Right, 1, 0),
				NSLayoutConstraint.Create(_titleBar, NSLayoutAttribute.Height, NSLayoutRelation.Equal,
					MainMenu, NSLayoutAttribute.Height, 1, 0),
				NSLayoutConstraint.Create(_rightButtonContainer, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					MainMenu, NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_rightButtonContainer, NSLayoutAttribute.Right, NSLayoutRelation.Equal,
					MainMenu, NSLayoutAttribute.Right, 1, 0),
				NSLayoutConstraint.Create(_rightButtonContainer, NSLayoutAttribute.Height, NSLayoutRelation.Equal,
					MainMenu, NSLayoutAttribute.Height, 1, 0),
				NSLayoutConstraint.Create(_ivDialButton, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					MainMenu, NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_ivDialButton, NSLayoutAttribute.Right, NSLayoutRelation.Equal,
					_rightButtonContainer, NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_ivDialButton, NSLayoutAttribute.Height, NSLayoutRelation.Equal,
					MainMenu, NSLayoutAttribute.Height, 1, 0),
				NSLayoutConstraint.Create(_ivDialButton, NSLayoutAttribute.Width, NSLayoutRelation.Equal,
					MainMenu, NSLayoutAttribute.Height, 1, 0)
			});
		}

		public void DidSignIn(SignIn signIn, GoogleUser user, NSError error)
		{
			if(error == null)
			{
				var userInfo = new UserInfo
				{
					AccountType = AccountType.GOOGLE,
					UserId = user.UserID,
					AvatarUrl = user.Profile.GetImageUrl(500)?.ToString(),
					Email = user.Profile.Email,
					UserName = user.Profile.Name
				};
				LoginCommand?.Execute(userInfo);
			}
		}

		#region UI Elements

		public UIView MainMenu { get; private set; }
		private AlphaUIView _overlaySidebarLayout;
		private AlphaUIView _overlaySearchBox;
		private AlphaUIView _topLeftButtonLayout;
		private AlphaUIView _topRightButtonLayout;
		private UIView _verticalDivider;
		private UIView _horizontalDivider;
		private AlphaImageView _ivDialButton;

		private UILabel _titleBar;

		private DisappearableUIView _leftButtonContainer;
		private AlphaUIButton _ivLeftButton;
		private DisappearableUIView _rightButtonContainer;
		private AlphaUIButton _ivRightButton;

		public SidebarLayout SidebarLayout { get; private set; }
		public SearchboxLayout SearchboxLayout { get; private set; }

		#endregion
	}
}