using System.Linq;
using CoreGraphics;
using Foundation;
using GovernmentComplaint.Core.ViewModels.Bases;
using GovernmentComplaint.iOS.Helpers;
using MvvmCross.Binding.BindingContext;
using UIKit;

namespace GovernmentComplaint.iOS.Views.Bases
{
	[Register("PopupView")]
	public abstract class PopupView : BaseView
	{
		protected override void InitView()
		{
			_popupContainer = UIHelper.CreateView(0, 0, ColorHelper.Overlay);
			_toastContainer = UIHelper.CreateView(0, 0, ColorHelper.Blue, true);
			_indicatorLayout = UIHelper.CreateLayout(0, 0, ColorHelper.Overlay);
			View.AddSubview(_popupContainer);
			View.AddSubview(_toastContainer);
			View.AddSubview(_indicatorLayout);
			View.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_popupContainer, NSLayoutAttribute.Top, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_popupContainer, NSLayoutAttribute.Left, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_popupContainer, NSLayoutAttribute.Right, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Right, 1, 0),
				NSLayoutConstraint.Create(_popupContainer, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Bottom, 1, 0),
				NSLayoutConstraint.Create(_toastContainer, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Bottom, 1, -DimensionHelper.MainMenuHeight),
				NSLayoutConstraint.Create(_toastContainer, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_toastContainer, NSLayoutAttribute.Left, NSLayoutRelation.GreaterThanOrEqual,
					View, NSLayoutAttribute.LeftMargin, 1, 0),
				NSLayoutConstraint.Create(_toastContainer, NSLayoutAttribute.Right, NSLayoutRelation.LessThanOrEqual,
					View, NSLayoutAttribute.RightMargin, 1, 0),
				NSLayoutConstraint.Create(_indicatorLayout, NSLayoutAttribute.Top, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_indicatorLayout, NSLayoutAttribute.Left, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_indicatorLayout, NSLayoutAttribute.Right, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Right, 1, 0),
				NSLayoutConstraint.Create(_indicatorLayout, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Bottom, 1, 0)
			});

			_popupContainer.Hidden = true;
			_toastContainer.Hidden = true;
			_indicatorLayout.Hidden = true;
			AddIndicator();
			AddToast();
		}

		private void OnOverlayLayoutClicked()
		{
			_popupLayout.Hidden = true;
			_popupContainer.Hidden = true;
			// View.EndEditing(true);
			HideKeyboard();
		}

		private void AddToast()
		{
			_toastMessage = UIHelper.CreateLabel(ColorHelper.White, DimensionHelper.MediumTextSize,
				alignment: UITextAlignment.Center, lines: 3);
			_toastContainer.AddSubview(_toastMessage);
			_toastContainer.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_toastContainer, NSLayoutAttribute.TopMargin, NSLayoutRelation.Equal,
					_toastMessage,
					NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_toastContainer, NSLayoutAttribute.LeftMargin, NSLayoutRelation.Equal,
					_toastMessage,
					NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_toastContainer, NSLayoutAttribute.BottomMargin, NSLayoutRelation.Equal,
					_toastMessage, NSLayoutAttribute.Bottom, 1, 0),
				NSLayoutConstraint.Create(_toastContainer, NSLayoutAttribute.RightMargin, NSLayoutRelation.Equal,
					_toastMessage,
					NSLayoutAttribute.Right, 1, 0)
			});
		}

		private void AddIndicator()
		{
			var indicator = UIHelper.CreateIndicator(DimensionHelper.PopupProgressSize,
				DimensionHelper.PopupProgressSize,
				ColorHelper.White);

			indicator.StartAnimating();
			indicator.Transform = CGAffineTransform.MakeScale(1.5f, 1.5f);

			_indicatorLayout.AddSubview(indicator);
			_indicatorLayout.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(indicator, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, _indicatorLayout,
					NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(indicator, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, _indicatorLayout,
					NSLayoutAttribute.CenterY, 1, 0)
			});
		}

		protected override void CreateBinding()
		{
			var bindingSet = this.CreateBindingSet<PopupView, PopupViewModel>();
			bindingSet.Bind(_toastMessage).For(v => v.Text).To(vm => vm.ToastText);
			bindingSet.Bind(_toastContainer).For(v => v.Hidden).To(vm => vm.IsToastShown).WithConversion("InvertBool");
			bindingSet.Bind(this).For(v => v.IsIndicatorShown).To(vm => vm.IsIndicatorShown);
			bindingSet.Apply();
		}

		public override void ViewDidLayoutSubviews()
		{
			base.ViewDidLayoutSubviews();
			_toastContainer.Layer.CornerRadius = DimensionHelper.ToastCorner;
		}

		#region Properties

		protected UIView _popupContainer;
		protected UIView _popupLayout;
		private UIView _toastContainer;
		private UILabel _toastMessage;
		private bool _isPopupShown;
		private UIView _indicatorLayout;

		public bool IsPopupShown
		{
			get { return _isPopupShown; }
			set
			{
				if (value)
				{
					if (IsVisible)
					{
						_popupContainer.Hidden = false;
						_isPopupShown = true;
					}
				}
				else
				{
					_popupContainer.Hidden = true;
					_isPopupShown = false;
				}
			}
		}

		private bool _isIndicatorShown;

		public bool IsIndicatorShown
		{
			get { return _isIndicatorShown; }
			set
			{
				if (value)
				{
					if (IsVisible)
					{
						HideKeyboard();
						_indicatorLayout.Hidden = false;
						_isIndicatorShown = true;
					}
				}
				else
				{
					_indicatorLayout.Hidden = true;
					_isIndicatorShown = false;
				}
			}
		}

		public UIView PopupLayout
		{
			get { return _popupLayout; }
			set
			{
				if (_popupLayout != null && _popupContainer.Subviews.Contains(_popupLayout))
				{
					_popupLayout.RemoveFromSuperview();
					_popupLayout.Dispose();
				}

				_popupLayout = value;
				value.Layer.CornerRadius = DimensionHelper.RoundCorner;

				_popupContainer.Add(value);
				_popupContainer.AddConstraints(new[]
				{
					NSLayoutConstraint.Create(value, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _popupContainer,
						NSLayoutAttribute.Left, 1, DimensionHelper.PopupMargin),
					NSLayoutConstraint.Create(value, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _popupContainer,
						NSLayoutAttribute.Right, 1, -DimensionHelper.PopupMargin),
					NSLayoutConstraint.Create(value, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, _popupContainer,
						NSLayoutAttribute.CenterY, 1, 0)
				});
			}
		}

		private bool _isLogoutPopupShown;

		public bool IsLogoutPopupShown
		{
			get { return _isLogoutPopupShown; }
			set
			{
				if (value)
				{
					if (!IsVisible) return;
					_popupContainer.Hidden = false;
					_isLogoutPopupShown = true;
				}
				else
				{
					_popupContainer.Hidden = true;
					_isPopupShown = false;
				}
			}
		}

		#endregion
	}
}