using System;
using GovernmentComplaint.Core.ViewModels.ItemViewModels;
using GovernmentComplaint.iOS.Controls;
using GovernmentComplaint.iOS.Helpers;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Binding.iOS.Views;
using UIKit;

namespace GovernmentComplaint.iOS.Views.TableView.ItemCells
{
    public class DropdownTimeCategoryItemCell : MvxTableViewCell
    {
        #region UI Elements
        private AlphaUIView _timeContainerItemView;
        private UILabel _lbTimeName;
        #endregion

        public DropdownTimeCategoryItemCell(IntPtr ptr) : base(ptr)
        {
            InitView();
            CreateBindings();
        }

        private void InitView()
        {
            UserInteractionEnabled = true;
            ExclusiveTouch = true;
            ContentView.ExclusiveTouch = true;
            _timeContainerItemView = UIHelper.CreateAlphaView(0, 0, 0, ColorHelper.Gray);
            //_timeContainerItemView.UserInteractionEnabled = true;

            _lbTimeName = UIHelper.CreateLabel(ColorHelper.Black, DimensionHelper.MediumTextSize);
            var divider = UIHelper.CreateView(0, DimensionHelper.DividerWeight, ColorHelper.Divider);

            _timeContainerItemView.Add(_lbTimeName);
            _timeContainerItemView.Add(divider);
            _timeContainerItemView.AddConstraints(new[]
            {
                NSLayoutConstraint.Create(_lbTimeName, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, _timeContainerItemView, NSLayoutAttribute.CenterY, 1, 0),
                NSLayoutConstraint.Create(_lbTimeName, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _timeContainerItemView, NSLayoutAttribute.Left, 1, DimensionHelper.LayoutBigMargin),

                NSLayoutConstraint.Create(divider, NSLayoutAttribute.Width, NSLayoutRelation.Equal, _timeContainerItemView, NSLayoutAttribute.Width, 1, 0),
                NSLayoutConstraint.Create(divider, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _timeContainerItemView, NSLayoutAttribute.Bottom, 1, 0),
            });

            Add(_timeContainerItemView);
            AddConstraints(new[]
            {
                NSLayoutConstraint.Create(_timeContainerItemView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, this, NSLayoutAttribute.Top, 1, 0),
                NSLayoutConstraint.Create(_timeContainerItemView, NSLayoutAttribute.Width, NSLayoutRelation.Equal, this, NSLayoutAttribute.Width, 1, 0),
                NSLayoutConstraint.Create(_timeContainerItemView, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, this, NSLayoutAttribute.Bottom, 1, 0)
            });
        }

        private void CreateBindings()
        {
            var set = this.CreateBindingSet<DropdownTimeCategoryItemCell, DropdownTimeSelectionItemViewModel>();
            set.Bind(_lbTimeName).To(vm => vm.ItemTitle);
            set.Bind(_timeContainerItemView).For(v => v.ClickCommand).To(vm => vm.ItemClickCommand);
            set.Apply();
        }
    }
}