﻿using System;
using System.Runtime.CompilerServices;
using CoreAnimation;
using CoreGraphics;
using GovernmentComplaint.Core.ViewModels.ItemViewModels;
using GovernmentComplaint.iOS.Controls;
using GovernmentComplaint.iOS.Helpers;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Binding.iOS.Views;
using PatridgeDev;
using UIKit;

namespace GovernmentComplaint.iOS.Views.TableView.ItemCells
{
    public class FeedbackItemCell : MvxTableViewCell
    {
        public FeedbackItemCell(IntPtr handle) : base(handle)
        {
            InitView();
            CreateBindings();
        }

        private void InitView()
        {
            UserInteractionEnabled = true;
            InitUIElements();
            var divider = UIHelper.CreateView(0, DimensionHelper.DividerWeight, ColorHelper.Divider);
           _container.Add(_feedbackImage);
            _container.Add(_feedbackStatus);
            _container.Add(_department);
            _container.Add(_title);
            _container.Add(_time);
            _container.Add(_ratingBar);
            _container.Add(_satisfiedStatusIcon);
            _container.Add(divider);
            _container.Add(_commentImage);
            _container.Add(_numsComment);
            _container.Add(_comment);
            _container.Add(_likeImage);
            _container.Add(_numsLike);
            _container.Add(_like);
            _container.AddConstraints(new[]
            {
                //Feedback Image
                NSLayoutConstraint.Create(_feedbackImage, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _container,
                    NSLayoutAttribute.Top, 1, DimensionHelper.FeedbackImageMarginTop),
                NSLayoutConstraint.Create(_feedbackImage, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _container,
                    NSLayoutAttribute.Left, 1, DimensionHelper.FeedbackImageMarginLeft),
                NSLayoutConstraint.Create(_feedbackImage, NSLayoutAttribute.Width, NSLayoutRelation.Equal, null,
                    NSLayoutAttribute.NoAttribute, 1, DimensionHelper.FeedbackImageWidth),
                NSLayoutConstraint.Create(_feedbackImage, NSLayoutAttribute.Height, NSLayoutRelation.Equal, null,
                    NSLayoutAttribute.NoAttribute, 1, DimensionHelper.FeedbackImageHeight),

                //Feedback status
                NSLayoutConstraint.Create(_feedbackStatus, NSLayoutAttribute.Right, NSLayoutRelation.Equal,
                    _feedbackImage, NSLayoutAttribute.Right, 1, -DimensionHelper.FeedbackSolvedImageMarginRight),
                NSLayoutConstraint.Create(_feedbackStatus, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _container,
                    NSLayoutAttribute.Top, 1, DimensionHelper.FeedbackSolvedImageMarginTop),

                //Feedback Department
                NSLayoutConstraint.Create(_department, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _feedbackImage,
                    NSLayoutAttribute.Bottom, 1, 0),
                NSLayoutConstraint.Create(_department, NSLayoutAttribute.Height, NSLayoutRelation.Equal, null,
                    NSLayoutAttribute.NoAttribute, 1, DimensionHelper.FeedbackDepartmentTextHeight),
                NSLayoutConstraint.Create(_department, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _feedbackImage,
                    NSLayoutAttribute.Left, 1, 0),
                NSLayoutConstraint.Create(_department, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _feedbackImage,
                    NSLayoutAttribute.Right, 1, 0),

                //Feedback Title
                NSLayoutConstraint.Create(_title, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _feedbackImage,
                    NSLayoutAttribute.Right, 1, DimensionHelper.FeedbackTitleMarginLeft),
                NSLayoutConstraint.Create(_title, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _container,
                    NSLayoutAttribute.Right, 1, -DimensionHelper.FeedbackTitleMarginRight),
                NSLayoutConstraint.Create(_title, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _container,
                    NSLayoutAttribute.Top, 1, DimensionHelper.FeedbackTitleMarginTop),

                //Feedback Time
                NSLayoutConstraint.Create(_time, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _feedbackImage,
                    NSLayoutAttribute.Right, 1, DimensionHelper.FeedbackTitleMarginLeft),
                NSLayoutConstraint.Create(_time, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _title,
                    NSLayoutAttribute.Bottom, 1, DimensionHelper.FeedbackTimeMarginTop),

                //Feedback Rating
                NSLayoutConstraint.Create(_ratingBar, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _time,
                    NSLayoutAttribute.Left, 1, 0),
                NSLayoutConstraint.Create(_ratingBar, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _commentImage,
                    NSLayoutAttribute.Top, 1, -14),

                //Satisfy status
                NSLayoutConstraint.Create(_satisfiedStatusIcon, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _ratingBar, NSLayoutAttribute.Top, 1, 0),
                NSLayoutConstraint.Create(_satisfiedStatusIcon, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _container, NSLayoutAttribute.Right, 1, -DimensionHelper.LayoutBigMargin),
                
                //Feedback Comment
                NSLayoutConstraint.Create(_commentImage, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _time,
                    NSLayoutAttribute.Left, 1, 0),
                NSLayoutConstraint.Create(_commentImage, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, divider,
                    NSLayoutAttribute.Top, 1, -10),
                NSLayoutConstraint.Create(_numsComment, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _commentImage,
                    NSLayoutAttribute.Right, 1, DimensionHelper.FeedbackCommentMarginLeft),
                NSLayoutConstraint.Create(_numsComment, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, divider,
                    NSLayoutAttribute.Top, 1, -10),
                NSLayoutConstraint.Create(_comment, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _numsComment,
                    NSLayoutAttribute.Right, 1, DimensionHelper.FeedbackCommentMarginLeft),
                NSLayoutConstraint.Create(_comment, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, divider,
                    NSLayoutAttribute.Top, 1, -10),

                //Feedback Like
                NSLayoutConstraint.Create(_likeImage, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _comment,
                    NSLayoutAttribute.Right, 1, DimensionHelper.FeedbackLikeImageMarginLeft),
                NSLayoutConstraint.Create(_likeImage, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, divider,
                    NSLayoutAttribute.Top, 1, -10),
                NSLayoutConstraint.Create(_numsLike, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _likeImage,
                    NSLayoutAttribute.Right, 1, DimensionHelper.FeedbackLikeMarginLeft),
                NSLayoutConstraint.Create(_numsLike, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, divider,
                    NSLayoutAttribute.Top, 1, -10),
                NSLayoutConstraint.Create(_like, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _numsLike,
                    NSLayoutAttribute.Right, 1, DimensionHelper.FeedbackLikeMarginLeft),
                NSLayoutConstraint.Create(_like, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, divider,
                    NSLayoutAttribute.Top, 1, -10),

                //Divider
                NSLayoutConstraint.Create(divider, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _container,
                    NSLayoutAttribute.Bottom, 1, 0),
                NSLayoutConstraint.Create(divider, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _container,
                    NSLayoutAttribute.Left, 1, 0),
                NSLayoutConstraint.Create(divider, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _container,
                    NSLayoutAttribute.Right, 1, 0),
                NSLayoutConstraint.Create(divider, NSLayoutAttribute.Height, NSLayoutRelation.Equal, null,
                    NSLayoutAttribute.NoAttribute, 1, DimensionHelper.SeparateLine)
            });
            Add(_container);
            AddConstraints(new[]
            {
                NSLayoutConstraint.Create(_container, NSLayoutAttribute.Top, NSLayoutRelation.Equal, this,
                    NSLayoutAttribute.Top, 1, 0),
                NSLayoutConstraint.Create(_container, NSLayoutAttribute.Left, NSLayoutRelation.Equal, this,
                    NSLayoutAttribute.Left, 1, 0),
                NSLayoutConstraint.Create(_container, NSLayoutAttribute.Right, NSLayoutRelation.Equal, this,
                    NSLayoutAttribute.Right, 1, 0),
                NSLayoutConstraint.Create(_container, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, this,
                    NSLayoutAttribute.Bottom, 1, 0)
            });
        }

        private void InitUIElements()
        {
            _container = UIHelper.CreateAlphaView(0, 0, 0);
            var rect = new CGRect(_container.Bounds.X, _container.Bounds.Y, UIScreen.MainScreen.Bounds.Width,
                DimensionHelper.FeedbackHeight);
            var gradient = new CAGradientLayer
            {
                Colors = new[] {UIColor.White.CGColor, UIColor.FromWhiteAlpha((nfloat) 0.9, (nfloat) 1.0).CGColor},
                Frame = rect
            };
            _container.Layer.InsertSublayer(gradient, 0);
            _feedbackImage = new CustomImageView
            {
                ContentMode = UIViewContentMode.ScaleAspectFill,
                ClipsToBounds = true
            };
            //_feedbackImage.Image = UIImage.FromFile("Images/default_complaint_image");
            _commentImage = UIHelper.CreateImageView(DimensionHelper.FeedbackCommentImageSize,
                DimensionHelper.FeedbackCommentImageSize, UIViewContentMode.ScaleToFill);
            _commentImage.Image = UIImage.FromBundle("Images/comment");
            _numsComment = UIHelper.CreateLabel(ColorHelper.FeedbackTimeColor, DimensionHelper.SmallestTextSize);
            _comment = UIHelper.CreateLabel(ColorHelper.FeedbackReactColor, DimensionHelper.SmallestTextSize);
            _likeImage = UIHelper.CreateImageView(DimensionHelper.FeedbackLikeImageSize,
                DimensionHelper.FeedbackLikeImageSize,UIViewContentMode.ScaleToFill);
            _likeImage.Image = UIImage.FromBundle("Images/like");
            _numsLike = UIHelper.CreateLabel(ColorHelper.FeedbackReactColor, DimensionHelper.SmallestTextSize);
            _like = UIHelper.CreateLabel(ColorHelper.FeedbackReactColor, DimensionHelper.SmallestTextSize);
            _feedbackStatus = UIHelper.CreateImageView(DimensionHelper.FeedbackSolvedImageSize,
                DimensionHelper.FeedbackSolvedImageSize);
            _feedbackStatus.Image = UIImage.FromBundle("Images/status_solved");
            _department = UIHelper.CreateLabel(ColorHelper.White, DimensionHelper.NanoTextSize, true,
                UITextAlignment.Center, 2);
            _title = UIHelper.CreateLabel(ColorHelper.Black, DimensionHelper.FeedbackTitleTextSize, true, UITextAlignment.Left,
                2);
			_title.LineBreakMode = UILineBreakMode.TailTruncation;
			_time = UIHelper.CreateLabel(ColorHelper.FeedbackTimeColor, DimensionHelper.SmallTextSize);
            _ratingBar = UIHelper.CreateRatingBar(100, 20);
            _ratingBar.UserInteractionEnabled = false;

            _satisfiedStatusIcon = UIHelper.CreateImageView(DimensionHelper.MediumMenuIconHeight, DimensionHelper.MediumMenuIconHeight);
            _satisfiedStatusIcon.UserInteractionEnabled = false;
        }

        private void CreateBindings()
        {
            var set = this.CreateBindingSet<FeedbackItemCell, FeedbackItemViewModel>();
            set.Bind(_feedbackImage).For("ImageUrl").To(vm => vm.Image);
            set.Bind(_feedbackStatus).For(v => v.Hidden).To(vm => vm.Solved).WithConversion("InvertBool");
            set.Bind(_department).To(vm => vm.Department);
            set.Bind(_department).For(v => v.BackgroundColor).To(vm => vm.Color).WithConversion("HexaToUIColor");
            set.Bind(_title).To(vm => vm.Title);
            set.Bind(_time).To(vm => vm.DateCreated);
            set.Bind(_ratingBar).For(v => v.ChosenRating).To(vm => vm.Rating);
            set.Bind(_numsComment).To(vm => vm.NumsComment);
            set.Bind(_numsLike).To(vm => vm.NumsLike);
            set.Bind(_comment).To(vm => vm.CommentText);
            set.Bind(_like).To(vm => vm.LikeText);
            set.Bind(_container).For(v => v.ClickCommand).To(vm => vm.FeedbackClicked);
            set.Bind(_satisfiedStatusIcon).For(v => v.Image).To(vm => vm.SatisfiedStatusIconName).WithConversion("IconNameToUIImage");
            set.Bind(_satisfiedStatusIcon).For(v => v.Hidden).To(vm => vm.IsSatisfiedStatusIconShown).WithConversion("InvertBool");
            set.Apply();
        }

        #region UI Elements

        private AlphaUIView _container;
        private CustomImageView _feedbackImage;
        private UIImageView _feedbackStatus;
        private UILabel _department;
        private UILabel _title;
        private UILabel _time;
        private UIImageView _commentImage;
        private UILabel _numsComment;
        private UILabel _comment;
        private UIImageView _likeImage;
        private UILabel _numsLike;
        private UILabel _like;
        private PDRatingView _ratingBar;

        private UIImageView _satisfiedStatusIcon;

        #endregion
    }
}