using System;
using CoreAnimation;
using GovernmentComplaint.Core.ViewModels.ItemViewModels;
using GovernmentComplaint.iOS.Controls;
using GovernmentComplaint.iOS.Helpers;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Binding.iOS.Views;
using UIKit;

namespace GovernmentComplaint.iOS.Views.TableView.ItemCells
{
	public class NotificationItemCell : MvxTableViewCell
	{
		private CAGradientLayer _gradient;

		public NotificationItemCell(IntPtr handle) : base(handle)
		{
			InitView();
			CreateBindings();
		}

		private void InitView()
		{
			UserInteractionEnabled = true;
			InitUIElements();
			var divider = UIHelper.CreateView(0, DimensionHelper.DividerWeight, ColorHelper.Divider);
			_container.Add(_notificationImage);
			_container.Add(_content);
			_container.Add(_time);
			_container.Add(_statusIcon);
			_container.Add(divider);
			_container.AddConstraints(new[]
			{
				//Notification image
				NSLayoutConstraint.Create(_notificationImage, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _container,
					NSLayoutAttribute.Top, 1, DimensionHelper.NotificationMagrinTop),
				NSLayoutConstraint.Create(_notificationImage, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _container,
					NSLayoutAttribute.Left, 1, DimensionHelper.NotificationMagrinLeft),

				//Notification content
				NSLayoutConstraint.Create(_content, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _container,
					NSLayoutAttribute.Top, 1, DimensionHelper.NotificationMagrinTop),
				NSLayoutConstraint.Create(_content, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _notificationImage,
					NSLayoutAttribute.Right, 1, DimensionHelper.SmallContentMargin),
				NSLayoutConstraint.Create(_content, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _container,
					NSLayoutAttribute.Right, 1, 0),

				//Notification status icon
				NSLayoutConstraint.Create(_statusIcon, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _container,
					NSLayoutAttribute.Right, 1, -DimensionHelper.NotificationStatusMagrinRight),
				NSLayoutConstraint.Create(_statusIcon, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _content,
					NSLayoutAttribute.Bottom, 1, 0),

				//Notification time
				NSLayoutConstraint.Create(_time, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _statusIcon,
					NSLayoutAttribute.Left, 1, -DimensionHelper.NotificationTimeMagrinRight),
				NSLayoutConstraint.Create(_time, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _content,
					NSLayoutAttribute.Bottom, 1, DimensionHelper.NotificationTimeMagrinTop),

				//Divider
				NSLayoutConstraint.Create(divider, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _container,
					NSLayoutAttribute.Bottom, 1, 0),
				NSLayoutConstraint.Create(divider, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _container,
					NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(divider, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _container,
					NSLayoutAttribute.Right, 1, 0),
				NSLayoutConstraint.Create(divider, NSLayoutAttribute.Height, NSLayoutRelation.Equal, null,
					NSLayoutAttribute.NoAttribute, 1, DimensionHelper.SeparateLine)
			});
			Add(_container);
			AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_container, NSLayoutAttribute.Top, NSLayoutRelation.Equal, this,
					NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_container, NSLayoutAttribute.Left, NSLayoutRelation.Equal, this,
					NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_container, NSLayoutAttribute.Right, NSLayoutRelation.Equal, this,
					NSLayoutAttribute.Right, 1, 0),
				NSLayoutConstraint.Create(_container, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, this,
					NSLayoutAttribute.Bottom, 1, 0)
			});
		}

		private void InitUIElements()
		{
			_container = UIHelper.CreateAlphaView(0, 0, 0);
			_notificationImage = UIHelper.CreateImageView(DimensionHelper.NotificationImageSize,
				DimensionHelper.NotificationImageSize);
			_statusIcon = UIHelper.CreateImageView(DimensionHelper.NotificationStatusSize,
				DimensionHelper.NotificationStatusSize);
			_content = UIHelper.CreateCustomUILabel(ColorHelper.Black, DimensionHelper.SmallestTextSize,
				UITextAlignment.Left, 2);
			_time = UIHelper.CreateLabel(ColorHelper.Black, DimensionHelper.TinyTextSize);
		}

		private void CreateBindings()
		{
			var set = this.CreateBindingSet<NotificationItemCell, NotificationItemViewModel>();

			set.Bind(_container)
				.For(v => v.BackgroundColor)
				.To(vm => vm.BackgroundColor)
				.WithConversion("HexaToUIColor");
			set.Bind(_content).For(v => v.HtmlText).To(vm => vm.NotificationContent);
			set.Bind(_time).To(vm => vm.NotificationTime);
			set.Bind(_container).For(v => v.ClickCommand).To(vm => vm.NotificationClicked);
			set.Bind(_notificationImage).For(v => v.Image).To(vm => vm.ImageSource).WithConversion("UriToUIImage");
			set.Bind(_statusIcon).For(v => v.Image).To(vm => vm.StatusIcon).WithConversion("IconNameToUIImage");

			set.Apply();
		}

		#region UI Elements

		private AlphaUIView _container;
		private UIImageView _notificationImage;
		private UIImageView _statusIcon;
		private CustomUILabel _content;
		private UILabel _time;

		#endregion
	}
}