using System;
using GovernmentComplaint.Core.ViewModels.ItemViewModels;
using GovernmentComplaint.iOS.Helpers;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Binding.iOS.Views;
using UIKit;

namespace GovernmentComplaint.iOS.Views.TableView.ItemCells
{
    public class FeedbackCategoryItemCell : MvxTableViewCell
    {
        #region UI Elements

        private UIView _containerItemView;
        private UILabel _lbFieldOfSuggestionName;

        private UIView _rightContentView;
        private UILabel _lbFeedbackProcessed;
        private UILabel _lbTotalFeedback;
        private UILabel _lbSlash;

        #endregion

        public FeedbackCategoryItemCell(IntPtr ptr) : base(ptr)
        {
            InitView();
            CreateBindings();
        }

        private void InitView()
        {
            UserInteractionEnabled = false;
            ExclusiveTouch = true;
            ContentView.ExclusiveTouch = true;

            _containerItemView = UIHelper.CreateView(0, 0, ColorHelper.White);

            _lbFieldOfSuggestionName = UIHelper.CreateLabel(ColorHelper.DarkGray, DimensionHelper.MediumTextSize, lines: 2);

            RightContent();

            _containerItemView.Add(_lbFieldOfSuggestionName);
            _containerItemView.Add(_rightContentView);
            _containerItemView.AddConstraints(new[]
            {
                NSLayoutConstraint.Create(_rightContentView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _containerItemView, NSLayoutAttribute.Top, 1, 0),
                NSLayoutConstraint.Create(_rightContentView, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, _containerItemView, NSLayoutAttribute.CenterY, 1, 0),
                NSLayoutConstraint.Create(_rightContentView, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _containerItemView, NSLayoutAttribute.Right, 1, -DimensionHelper.LayoutBigMargin),


                NSLayoutConstraint.Create(_lbFieldOfSuggestionName, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _containerItemView, NSLayoutAttribute.Left, 1, DimensionHelper.LayoutBigMargin),
                NSLayoutConstraint.Create(_lbFieldOfSuggestionName, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, _containerItemView, NSLayoutAttribute.CenterY, 1, 0),
                NSLayoutConstraint.Create(_lbFieldOfSuggestionName, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _rightContentView, NSLayoutAttribute.Left, 1, 0),
            });

            Add(_containerItemView);
            AddConstraints(new[]
            {
                NSLayoutConstraint.Create(_containerItemView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, this, NSLayoutAttribute.Top, 1, 0),
                NSLayoutConstraint.Create(_containerItemView, NSLayoutAttribute.Left, NSLayoutRelation.Equal, this, NSLayoutAttribute.Left, 1, 0),
                NSLayoutConstraint.Create(_containerItemView, NSLayoutAttribute.Right, NSLayoutRelation.Equal, this, NSLayoutAttribute.Right, 1, 0),
                NSLayoutConstraint.Create(_containerItemView, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, this, NSLayoutAttribute.Bottom, 1, 0)
            });
        }

        private void RightContent()
        {
            _rightContentView = UIHelper.CreateView(DimensionHelper.RightContentWidth, DimensionHelper.CategoryItemHeight, ColorHelper.White);
            _lbFeedbackProcessed = UIHelper.CreateLabel(ColorHelper.DarkGray, DimensionHelper.MediumTextSize);
            _lbTotalFeedback = UIHelper.CreateLabel(ColorHelper.DarkGray, DimensionHelper.MediumTextSize);
            _lbSlash = UIHelper.CreateLabel(ColorHelper.DarkGray, DimensionHelper.MediumTextSize);

            _rightContentView.Add(_lbFeedbackProcessed);
            _rightContentView.Add(_lbSlash);
            _rightContentView.Add(_lbTotalFeedback);
            _rightContentView.AddConstraints(new[]
            {
                NSLayoutConstraint.Create(_lbTotalFeedback, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, _rightContentView, NSLayoutAttribute.CenterY, 1, 0),
                NSLayoutConstraint.Create(_lbTotalFeedback, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _rightContentView, NSLayoutAttribute.Right, 1, 0),

                NSLayoutConstraint.Create(_lbSlash, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, _rightContentView, NSLayoutAttribute.CenterY, 1, 0),
                NSLayoutConstraint.Create(_lbSlash, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _lbTotalFeedback, NSLayoutAttribute.Left, 1, 0),

                NSLayoutConstraint.Create(_lbFeedbackProcessed, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, _rightContentView, NSLayoutAttribute.CenterY, 1, 0),
                NSLayoutConstraint.Create(_lbFeedbackProcessed, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _lbSlash, NSLayoutAttribute.Left, 1, 0),

                NSLayoutConstraint.Create(_rightContentView, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _lbTotalFeedback, NSLayoutAttribute.Right, 1, 0),
            });
        }

        private void CreateBindings()
        {
            var set = this.CreateBindingSet<FeedbackCategoryItemCell, FeedbackCategoryItemViewModel>();
            set.Bind(_lbFieldOfSuggestionName).To(vm => vm.FieldOfSuggestionName);
            set.Bind(_lbTotalFeedback).To(vm => vm.TotalFeedback);
            set.Bind(_lbSlash).To(vm => vm.Slash);
            set.Bind(_lbFeedbackProcessed).To(vm => vm.FeedbackProcessed);
            set.Apply();
        }
    }
}