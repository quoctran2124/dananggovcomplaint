using System;
using GovernmentComplaint.Core.ViewModels.ItemViewModels;
using MvvmCross.Binding.iOS.Views;
using UIKit;
using GovernmentComplaint.iOS.Helpers;
using MvvmCross.Binding.BindingContext;
using GovernmentComplaint.iOS.Controls;
using GovernmentComplaint.Core.ViewModels.ItemViewModels.Base;

namespace GovernmentComplaint.iOS.Views.TableView.ItemCells
{
	public class HistorySortingItemCell : MvxTableViewCell
	{

		public HistorySortingItemCell(IntPtr ptr) : base(ptr)
		{
			InitView();
			CreateBindings();
		}

		private void InitView()
		{
			UserInteractionEnabled = true;
			InitUIElements();
			var divider = UIHelper.CreateView(0, DimensionHelper.DividerWeight, ColorHelper.Divider);
			_lbSortingItem.UserInteractionEnabled = false;
			_ivSortingItemIconName.UserInteractionEnabled = false;
			_container.Add(_lbSortingItem);
			_container.Add(_ivSortingItemIconName);
			_container.Add(divider);

			_container.AddConstraints(new[]
			{
				// Category Icon
				NSLayoutConstraint.Create(_ivSortingItemIconName, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, _container, NSLayoutAttribute.CenterY, 1, 0),
				NSLayoutConstraint.Create(_ivSortingItemIconName, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _container, NSLayoutAttribute.Left, 1, 0),

				  // Category Item name
				NSLayoutConstraint.Create(_lbSortingItem, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, _container, NSLayoutAttribute.CenterY, 1, 0),
				NSLayoutConstraint.Create(_lbSortingItem, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _ivSortingItemIconName, NSLayoutAttribute.Right, 1, 0),

				//Divider
				NSLayoutConstraint.Create(divider, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _container,
					NSLayoutAttribute.Bottom, 1, 0),
				NSLayoutConstraint.Create(divider, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _container,
					NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(divider, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _container,
					NSLayoutAttribute.Right, 1, 0),
				NSLayoutConstraint.Create(divider, NSLayoutAttribute.Height, NSLayoutRelation.Equal, null,
					NSLayoutAttribute.NoAttribute, 1, DimensionHelper.SeparateLine)


			});
			Add(_container);
			AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_container, NSLayoutAttribute.Top, NSLayoutRelation.Equal, this, NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_container, NSLayoutAttribute.Left, NSLayoutRelation.Equal, this, NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_container, NSLayoutAttribute.Right, NSLayoutRelation.Equal, this, NSLayoutAttribute.Right, 1, 0),
				NSLayoutConstraint.Create(_container, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, this, NSLayoutAttribute.Bottom, 1, 0),
			});
		}

		private void InitUIElements()
		{
			_container = UIHelper.CreateAlphaView(0, 0, 0, ColorHelper.LightestGray);
			_lbSortingItem = UIHelper.CreateLabel(ColorHelper.DarkGray, DimensionHelper.BigTextSize);
			_ivSortingItemIconName = UIHelper.CreateAlphaImageView(DimensionHelper.DropdownIconSize, DimensionHelper.DropdownIconSize, DimensionHelper.DropdownPadding);
		}

		private void CreateBindings()
		{
			var set = this.CreateBindingSet<HistorySortingItemCell, HistorySortingItemViewModel>();
			set.Bind(_container).For(v => v.ClickCommand).To(vm => vm.ItemClickCommand);
			set.Bind(_lbSortingItem).To(vm => vm.SortCondition);
			set.Bind(_ivSortingItemIconName).For(v => v.Image).To(vm => vm.SortImage).WithConversion("IconNameToUIImage");
			set.Apply();
		}

		#region UI Elements
		private AlphaUIView _container;
		private UILabel _lbSortingItem;
		private AlphaImageView _ivSortingItemIconName;
		#endregion

	}
}