using System;
using System.Windows.Input;
using GovernmentComplaint.Core.ViewModels.ItemViewModels;
using GovernmentComplaint.iOS.Controls;
using GovernmentComplaint.iOS.Helpers;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Binding.iOS.Views;
using UIKit;

namespace GovernmentComplaint.iOS.Views.TableView.ItemCells
{
    public class DepartmentItemCell : MvxTableViewCell
    {
        public DepartmentItemCell(IntPtr ptr) : base(ptr)
        {
            InitView();
            CreateBindings();
        }

        private void InitView()
        {
            UserInteractionEnabled = true;
            _lbDepartmentName = UIHelper.CreateAlphaLabel(ColorHelper.Black, DimensionHelper.MediumTextSize, false, UITextAlignment.Left, 2);
            _lbDepartmentName.UserInteractionEnabled = false;
            _container = UIHelper.CreateAlphaView(0, 0, 0, ColorHelper.LightestGray);
            var divider = UIHelper.CreateView(0, DimensionHelper.DividerWeight, ColorHelper.Divider);

            _container.Add(_lbDepartmentName);
            _container.Add(divider);

            _container.AddConstraints(new []
            {
                NSLayoutConstraint.Create(_lbDepartmentName, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal,
                    _container, NSLayoutAttribute.CenterY, 1, 0),
                NSLayoutConstraint.Create(_lbDepartmentName, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
                    _container, NSLayoutAttribute.Left, 1, 5),
                NSLayoutConstraint.Create(_lbDepartmentName, NSLayoutAttribute.Right, NSLayoutRelation.Equal,
                    _container, NSLayoutAttribute.Right, 1, - 5),

                NSLayoutConstraint.Create(divider, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _container,
                    NSLayoutAttribute.Bottom, 1, 0),
                NSLayoutConstraint.Create(divider, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _container,
                    NSLayoutAttribute.Left, 1, 3),
                NSLayoutConstraint.Create(divider, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _container,
                    NSLayoutAttribute.Right, 1, 0),
                NSLayoutConstraint.Create(divider, NSLayoutAttribute.Height, NSLayoutRelation.Equal, null,
                    NSLayoutAttribute.NoAttribute, 1, DimensionHelper.SeparateLine)
            });

            Add(_container);
            AddConstraints(new[]
            {
                NSLayoutConstraint.Create(_container, NSLayoutAttribute.Top, NSLayoutRelation.Equal, this,
                    NSLayoutAttribute.Top, 1, 0),
                NSLayoutConstraint.Create(_container, NSLayoutAttribute.Left, NSLayoutRelation.Equal, this,
                    NSLayoutAttribute.Left, 1, 0),
                NSLayoutConstraint.Create(_container, NSLayoutAttribute.Right, NSLayoutRelation.Equal, this,
                    NSLayoutAttribute.Right, 1, 0),
                NSLayoutConstraint.Create(_container, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, this,
                    NSLayoutAttribute.Bottom, 1, 0)
            });
        }
        
        private void CreateBindings()
        {
            var set = this.CreateBindingSet<DepartmentItemCell, DepartmentItemViewModel>();
            set.Bind(_lbDepartmentName).To(vm => vm.ItemTitle);
            set.Bind(_container).For(v=>v.ClickCommand).To(vm => vm.ItemClickCommand);
            set.Apply();
        }

        #region UI Elements

        private AlphaLabel _lbDepartmentName;
        private AlphaUIView _container;
        
        #endregion
    }
}