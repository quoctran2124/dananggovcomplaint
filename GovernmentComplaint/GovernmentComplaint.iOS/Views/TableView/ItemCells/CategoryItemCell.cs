using System;
using GovernmentComplaint.Core.ViewModels.ItemViewModels;
using GovernmentComplaint.iOS.Controls;
using GovernmentComplaint.iOS.Helpers;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Binding.iOS.Views;
using UIKit;

namespace GovernmentComplaint.iOS.Views.TableView.ItemCells
{
    public class CategoryItemCell : MvxTableViewCell
    {
        public CategoryItemCell(IntPtr ptr) : base(ptr)
        {
            InitView();
            CreateBindings();
        }

        private void InitView()
        {
            UserInteractionEnabled = true;
            InitUIElements();
            var divider = UIHelper.CreateView(0, DimensionHelper.DividerWeight, ColorHelper.Divider);
            ExclusiveTouch = true;
            ContentView.ExclusiveTouch = true;
            _container.Add(_lbCategoryItem);
            _container.Add(_ivCategoryItemIconName);
            _container.Add(divider);

            _container.AddConstraints(new[]
            {
                // Category Icon
                NSLayoutConstraint.Create(_ivCategoryItemIconName, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal,
                    _container, NSLayoutAttribute.CenterY, 1, 0),
                NSLayoutConstraint.Create(_ivCategoryItemIconName, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
                    _container, NSLayoutAttribute.Left, 1, 0),

                // Category Item name             
                NSLayoutConstraint.Create(_lbCategoryItem, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, _container,
                    NSLayoutAttribute.CenterY, 1, 0),
                NSLayoutConstraint.Create(_lbCategoryItem, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
                    _ivCategoryItemIconName, NSLayoutAttribute.Right, 1, 0),

                //Divider
                NSLayoutConstraint.Create(divider, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _container,
                    NSLayoutAttribute.Bottom, 1, 0),
                NSLayoutConstraint.Create(divider, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _container,
                    NSLayoutAttribute.Left, 1, 0),
                NSLayoutConstraint.Create(divider, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _container,
                    NSLayoutAttribute.Right, 1, 0),
                NSLayoutConstraint.Create(divider, NSLayoutAttribute.Height, NSLayoutRelation.Equal, null,
                    NSLayoutAttribute.NoAttribute, 1, DimensionHelper.SeparateLine)
            });

            Add(_container);
            AddConstraints(new[]
            {
                NSLayoutConstraint.Create(_container, NSLayoutAttribute.Top, NSLayoutRelation.Equal, this,
                    NSLayoutAttribute.Top, 1, 0),
                NSLayoutConstraint.Create(_container, NSLayoutAttribute.Left, NSLayoutRelation.Equal, this,
                    NSLayoutAttribute.Left, 1, 0),
                NSLayoutConstraint.Create(_container, NSLayoutAttribute.Right, NSLayoutRelation.Equal, this,
                    NSLayoutAttribute.Right, 1, 0),
                NSLayoutConstraint.Create(_container, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, this,
                    NSLayoutAttribute.Bottom, 1, 0)
            });
        }

        private void InitUIElements()
        {
            _container = UIHelper.CreateAlphaView(0, 0, 0, ColorHelper.LightestGray);
            _lbCategoryItem = UIHelper.CreateAlphaLabel(ColorHelper.DarkGray, DimensionHelper.BigTextSize);
            _lbCategoryItem.UserInteractionEnabled = false;
            _ivCategoryItemIconName = UIHelper.CreateAlphaImageView(DimensionHelper.DropdownIconSize,
                DimensionHelper.DropdownIconSize, DimensionHelper.ImageViewPadding);
            _ivCategoryItemIconName.UserInteractionEnabled = false;
        }

        private void CreateBindings()
        {
            var set = this.CreateBindingSet<CategoryItemCell, DropdownCategoryItemViewModel>();
            set.Bind(_lbCategoryItem).To(vm => vm.CatName);
            set.Bind(_ivCategoryItemIconName)
                .For(v => v.Image)
                .To(vm => vm.CatImage)
                .WithConversion("UriToUIImage");
            set.Bind(_container).For(v => v.ClickCommand).To(vm => vm.ItemClickCommand);
            set.Apply();
        }

        #region UI Elements

        private AlphaUIView _container;
        private AlphaLabel _lbCategoryItem;
        private AlphaImageView _ivCategoryItemIconName;
        
        #endregion
    }
}