using System;
using System.Diagnostics;
using System.Drawing;
using CoreGraphics;
using Foundation;
using GovernmentComplaint.Core.ViewModels.ItemViewModels;
using GovernmentComplaint.iOS.Controls;
using GovernmentComplaint.iOS.Helpers;
using GovernmentComplaint.iOS.Views.TableView.ItemTableViewSource;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Binding.iOS.Views;
using UIKit;

namespace GovernmentComplaint.iOS.Views.TableView.ItemCells
{
    public class CommentItemCell : MvxTableViewCell
    {
        #region UI Elements
        private UILabel _lbUserName;
        private UILabel _lbCommentDate;
        private UILabel _lbCommentContent;
        private AlphaLabel _lbStaticReplyText;
        private AlphaLabel _lbStaticLikeText;
        private UILabel _lbLikeText;

        private ChildrenCommentTableViewSource _childrenCommentItemSource;
        private UITableView _childrenCommentTableView;
        private NSLayoutConstraint _childrenCommentTableHeight;

        private UIView _replyCommentLayout;
        private CustomTextView _ctvReplyComment;
        private AlphaUIButton _btnReplyComment;

        private bool _isKeyboardHidden;
        public bool IsKeyboardHidden
        {
            get { return _isKeyboardHidden; }
            set
            {
                _isKeyboardHidden = value;
                if (value)
                {
                    this.EndEditing(true);
                }
            }
		}
		private bool _needToShowPlaceHolder;
		public bool NeedToShowPlaceHolder
		{
			get { return _needToShowPlaceHolder; }
			set
			{
				_needToShowPlaceHolder = value;
				if (value)
				{
					_ctvReplyComment.LbPlaceHolder.Hidden = false;
				}
			}
		}

		#endregion

		public CommentItemCell(IntPtr ptr) : base(ptr)
        {
            InitView();
            CreateBinding();
        }

        private void AddViews()
        {
            var header = UIHelper.CreateView(30, 0, UIColor.White);

            _lbCommentContent = UIHelper.CreateLabel(ColorHelper.Black, DimensionHelper.MediumTextSize, lines: 0);
            _lbCommentContent.SizeToFit();
            _lbCommentContent.LineBreakMode = UILineBreakMode.WordWrap;
            
            _lbUserName = UIHelper.CreateLabel(ColorHelper.ThemeBlue, DimensionHelper.MediumTextSize, true);
            _lbCommentDate = UIHelper.CreateLabel(ColorHelper.DarkestGray, DimensionHelper.MediumTextSize);
            _lbStaticReplyText = UIHelper.CreateAlphaLabel(ColorHelper.DarkestGray, DimensionHelper.MediumTextSize);
            _lbStaticLikeText = UIHelper.CreateAlphaLabel(ColorHelper.DarkestGray, DimensionHelper.MediumTextSize);
            _lbLikeText = UIHelper.CreateLabel(ColorHelper.Black, DimensionHelper.MediumTextSize);
            _childrenCommentTableView = UIHelper.CreateTableView(0, 80, UIColor.White);
            _childrenCommentItemSource = new ChildrenCommentTableViewSource(_childrenCommentTableView);
            _childrenCommentTableView.Source = _childrenCommentItemSource;
            _childrenCommentTableView.BackgroundColor = UIColor.White;
            _childrenCommentTableView.SeparatorStyle = UITableViewCellSeparatorStyle.SingleLine;

            _childrenCommentTableHeight = NSLayoutConstraint.Create(_childrenCommentTableView, NSLayoutAttribute.Height,
                NSLayoutRelation.Equal, null, NSLayoutAttribute.NoAttribute, 1, 0);

			_btnReplyComment = UIHelper.CreateButton(DimensionHelper.NComplaintItemHeight, DimensionHelper.CommonHeight, UIColor.White, DimensionHelper.MediumTextSize, ColorHelper.TrueBlue,
				false, false, true);
			_replyCommentLayout = UIHelper.CreateView(0, DimensionHelper.CommonHeight, UIColor.White, true, true);
            _ctvReplyComment = new CustomTextView();

            _replyCommentLayout.Add(_ctvReplyComment);
            _replyCommentLayout.AddConstraints(new[]
            {
                NSLayoutConstraint.Create(_ctvReplyComment, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _replyCommentLayout, NSLayoutAttribute.Top, 1, 0),
                NSLayoutConstraint.Create(_ctvReplyComment, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _replyCommentLayout, NSLayoutAttribute.Left, 1, 0),
                NSLayoutConstraint.Create(_ctvReplyComment, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _replyCommentLayout, NSLayoutAttribute.Right, 1, 0),
                NSLayoutConstraint.Create(_ctvReplyComment, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _replyCommentLayout, NSLayoutAttribute.Bottom, 1, 0),

            });

            AddSubviews(new[]
            {
                header, _lbCommentContent, _lbUserName, _lbCommentDate, _lbStaticReplyText,
                _lbStaticLikeText, _lbLikeText, _childrenCommentTableView, _replyCommentLayout, _btnReplyComment
            });
            AddConstraints(new[]
            {
                //header
                NSLayoutConstraint.Create(header, NSLayoutAttribute.Top, NSLayoutRelation.Equal, this, NSLayoutAttribute.Top, 1, 0),
                NSLayoutConstraint.Create(header, NSLayoutAttribute.Left, NSLayoutRelation.Equal, this, NSLayoutAttribute.Left, 1, 0),
                NSLayoutConstraint.Create(header, NSLayoutAttribute.Right, NSLayoutRelation.Equal, this, NSLayoutAttribute.Right, 1, 0),

                NSLayoutConstraint.Create(_lbUserName, NSLayoutAttribute.Top, NSLayoutRelation.Equal, this, NSLayoutAttribute.Top, 1, 0),
                NSLayoutConstraint.Create(_lbUserName, NSLayoutAttribute.Left, NSLayoutRelation.Equal, this, NSLayoutAttribute.Left, 1, 0),
                
                NSLayoutConstraint.Create(_lbCommentDate, NSLayoutAttribute.Top, NSLayoutRelation.Equal, this, NSLayoutAttribute.Top, 1, 0),
                NSLayoutConstraint.Create(_lbCommentDate, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _lbUserName, NSLayoutAttribute.Right, 1, DimensionHelper.CommentSmallPadding),

                //content
                NSLayoutConstraint.Create(_lbCommentContent, NSLayoutAttribute.Top, NSLayoutRelation.Equal, header, NSLayoutAttribute.Bottom, 1, DimensionHelper.CommentBigPadding),
                NSLayoutConstraint.Create(_lbCommentContent, NSLayoutAttribute.Left, NSLayoutRelation.Equal, this, NSLayoutAttribute.Left, 1, 0),
                NSLayoutConstraint.Create(_lbCommentContent, NSLayoutAttribute.Right, NSLayoutRelation.Equal, this, NSLayoutAttribute.Right, 1, 0),
                
                //reply
                NSLayoutConstraint.Create(_lbStaticReplyText, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _lbCommentContent, NSLayoutAttribute.Bottom, 1, DimensionHelper.CommentSmallPadding),
                NSLayoutConstraint.Create(_lbStaticReplyText, NSLayoutAttribute.Right, NSLayoutRelation.Equal, this, NSLayoutAttribute.Right, 1, - DimensionHelper.CommentSmallPadding),

                NSLayoutConstraint.Create(_lbStaticLikeText, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _lbCommentContent, NSLayoutAttribute.Bottom, 1, DimensionHelper.CommentSmallPadding),
                NSLayoutConstraint.Create(_lbStaticLikeText, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _lbStaticReplyText, NSLayoutAttribute.Left, 1, - DimensionHelper.CommentSmallPadding),

                NSLayoutConstraint.Create(_lbLikeText, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _lbCommentContent, NSLayoutAttribute.Bottom, 1, DimensionHelper.CommentSmallPadding),
                NSLayoutConstraint.Create(_lbLikeText, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _lbStaticLikeText, NSLayoutAttribute.Left, 1, - DimensionHelper.CommentSmallPadding),
                
                //children comments
                NSLayoutConstraint.Create(_childrenCommentTableView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _lbLikeText, NSLayoutAttribute.Bottom, 1, DimensionHelper.CommentSmallPadding),
                NSLayoutConstraint.Create(_childrenCommentTableView, NSLayoutAttribute.Right, NSLayoutRelation.Equal, this, NSLayoutAttribute.Right, 1, 0),
                NSLayoutConstraint.Create(_childrenCommentTableView, NSLayoutAttribute.Left, NSLayoutRelation.Equal, this, NSLayoutAttribute.Left, 1, DimensionHelper.LargeContentMargin),
                _childrenCommentTableHeight,

                NSLayoutConstraint.Create(_btnReplyComment, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _childrenCommentTableView, NSLayoutAttribute.Bottom, 1, DimensionHelper.CommentSmallPadding),
                NSLayoutConstraint.Create(_btnReplyComment, NSLayoutAttribute.Right, NSLayoutRelation.Equal, this, NSLayoutAttribute.Right, 1, - DimensionHelper.CommentSmallPadding),

                NSLayoutConstraint.Create(_replyCommentLayout, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _childrenCommentTableView, NSLayoutAttribute.Bottom, 1, DimensionHelper.CommentSmallPadding),
                NSLayoutConstraint.Create(_replyCommentLayout, NSLayoutAttribute.Left, NSLayoutRelation.Equal, this, NSLayoutAttribute.Left, 1, 0),
                NSLayoutConstraint.Create(_replyCommentLayout, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _btnReplyComment, NSLayoutAttribute.Left, 1, - DimensionHelper.MediumContentMargin),

                //NSLayoutConstraint.Create(this, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _childrenCommentTableView, NSLayoutAttribute.Bottom, 1, 5),
            });

            _replyCommentLayout.Layer.CornerRadius = DimensionHelper.ToastCorner;
        }

        private int _childrenCommentCount;
        public int ChildrenCommentCount
        {
            get { return _childrenCommentCount; }
            set
            {
                _childrenCommentCount = value;

                _childrenCommentTableHeight.Constant = value > 0 ? 100 : 0;

                //if (value > 0)
                //{
                //    UITableView parent;

                //    var view = Superview as UITableView;

                //    if (view != null)
                //    {
                //        parent = view;
                //    }
                //    else
                //    {
                //        parent = Superview.Superview as UITableView;
                //    }

                //    if (parent != null)
                //    {
                //        parent.BeginUpdates();
                //        parent.EndUpdates();
                //    }
                //}
               

            }
        }

        public float CellHeight(string text, int childrenCommentCount)
        {
            ChildrenCommentCount = childrenCommentCount;
	        var height = 2 * (float) DimensionHelper.MediumTextSize + 2 * (float) DimensionHelper.CommentSmallPadding +
	                     (float) _childrenCommentTableHeight.Constant + (float) DimensionHelper.CommonHeight;

			if (text != null)
            {
                _lbCommentContent.Text = text;
                var s = _lbCommentContent.Text.StringSize(_lbCommentContent.Font,
                    new SizeF((float)(Frame.Width - DimensionHelper.BigContentMargin * 2), float.MaxValue), UILineBreakMode.WordWrap);

                height += (float)(s.Height + _lbCommentContent.Font.LineHeight);

            }

            return height;
        }

        private void InitView()
        {
            UserInteractionEnabled = true;
            AddViews();
        }

        private void CreateBinding()
        {
            var set = this.CreateBindingSet<CommentItemCell, CommentItemViewModel>();

            set.Bind(this).For(v => v.ChildrenCommentCount).To(vm => vm.ChildrenCommentCount);

            set.Bind(_lbStaticReplyText).To(vm => vm.StaticReplyText);

            set.Bind(_lbStaticLikeText).To(vm => vm.StaticLikeText);
            set.Bind(_lbStaticLikeText).For(v => v.ClickCommand).To(vm => vm.LikeCommand);

            set.Bind(_lbLikeText).For(v => v.Text).To(vm => vm.LikeText);

            set.Bind(_lbCommentContent).For(v => v.Text).To(vm => vm.CommentContent);
            set.Bind(_lbCommentDate).For(v => v.Text).To(vm => vm.CommentDate);
            set.Bind(_lbUserName).For(v => v.Text).To(vm => vm.UserName);

            set.Bind(_childrenCommentItemSource).To(vm => vm.ChildrenCommentItemViewModels);
            set.Bind(_childrenCommentTableView).For(v => v.Hidden).To(vm => vm.IsChildrenCommentShown).WithConversion("InvertBool");

            set.Bind(_lbStaticReplyText).For(v => v.ClickCommand).To(vm => vm.ReplyCommand);
            set.Bind(_replyCommentLayout).For(v => v.Hidden).To(vm => vm.IsReplyCommentLayoutShown).WithConversion("InvertBool");
            set.Bind(_btnReplyComment).For(v => v.Hidden).To(vm => vm.IsReplyCommentLayoutShown).WithConversion("InvertBool");

            set.Bind(_ctvReplyComment.TextView).For("Text").To(vm => vm.EnteredCommentContent);
            set.Bind(_ctvReplyComment).For(v => v.PlaceHolder).To(vm => vm.CommentHint);
            set.Bind(_btnReplyComment).For("Title").To(vm => vm.CommentButtonHint);
            set.Bind(_btnReplyComment).For(v => v.ClickCommand).To(vm => vm.AddCommentCommand);

            set.Bind(this).For(v => v.IsKeyboardHidden).To(vm => vm.IsKeyboardHidden);
			set.Bind(this).For(v => v.NeedToShowPlaceHolder).To(vm => vm.NeedToShowPlaceHolder);
			set.Apply();
        }
    }
}