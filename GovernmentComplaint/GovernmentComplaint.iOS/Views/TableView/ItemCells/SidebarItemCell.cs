using System;
using GovernmentComplaint.Core.ViewModels;
using GovernmentComplaint.Core.ViewModels.ItemViewModels;
using GovernmentComplaint.Core.ViewModels.ItemViewModels.Base;
using GovernmentComplaint.iOS.Controls;
using GovernmentComplaint.iOS.Helpers;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Binding.iOS.Views;
using UIKit;

namespace GovernmentComplaint.iOS.Views.TableView.ItemCells
{
	public class SidebarItemCell : MvxTableViewCell
	{
		private AlphaUIView _container;
		private UILabel _lbSidebarItem;
	    private UILabel _lbCountNotification;
		private AlphaImageView _ivSidebarItem;

		public SidebarItemCell(IntPtr ptr) : base(ptr)
		{
			InitView();
			CreateBinding();
		}

		private void InitView()
		{
			UserInteractionEnabled = true;
			InitUIElement();
			var verticalDivider = UIHelper.CreateView(0, 0, ColorHelper.Divider);
			var horizontalDivider = UIHelper.CreateView(0, 0, ColorHelper.Divider);
			_ivSidebarItem = UIHelper.CreateAlphaImageView(DimensionHelper.SidebarIconSize, DimensionHelper.SidebarIconSize, 
				DimensionHelper.SidebarItemSidePadding);

		    _lbSidebarItem.UserInteractionEnabled = false;
            _ivSidebarItem.UserInteractionEnabled = false;
           
            verticalDivider.UserInteractionEnabled = false;
            horizontalDivider.UserInteractionEnabled = false;

            _container.Add(_lbSidebarItem);
			_container.Add(_ivSidebarItem);
            _container.Add(_lbCountNotification);
			_container.Add(verticalDivider);
			_container.Add(horizontalDivider);

			_container.AddConstraints(new []
            {
                NSLayoutConstraint.Create(_ivSidebarItem, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, _container,
                NSLayoutAttribute.CenterY, 1, 0),
                NSLayoutConstraint.Create(_ivSidebarItem, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _container,
                NSLayoutAttribute.Right, 1, 0),

                NSLayoutConstraint.Create(verticalDivider, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, _container,
                NSLayoutAttribute.CenterY, 1, 0),
                NSLayoutConstraint.Create(verticalDivider, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _ivSidebarItem,
                NSLayoutAttribute.Left, 1, 0),
                NSLayoutConstraint.Create(verticalDivider, NSLayoutAttribute.Width, NSLayoutRelation.Equal, null,
                NSLayoutAttribute.NoAttribute, 1, DimensionHelper.BorderWidth),
                NSLayoutConstraint.Create(verticalDivider, NSLayoutAttribute.Height, NSLayoutRelation.Equal, null,
                NSLayoutAttribute.NoAttribute, 1, DimensionHelper.SidebarItemHeight),

                NSLayoutConstraint.Create(_lbSidebarItem, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, _container,
				NSLayoutAttribute.CenterY, 1, 0),
				NSLayoutConstraint.Create(_lbSidebarItem, NSLayoutAttribute.Right, NSLayoutRelation.Equal, verticalDivider,
				NSLayoutAttribute.Left, 1, - DimensionHelper.SidebarItemSidePadding),

                NSLayoutConstraint.Create(_lbCountNotification, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _container,
                NSLayoutAttribute.Top, 1, DimensionHelper.NotificationCountMarginTop),
                NSLayoutConstraint.Create(_lbCountNotification, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _container,
                NSLayoutAttribute.Right, 1, -DimensionHelper.NotificationCountMarginRight),

                NSLayoutConstraint.Create(horizontalDivider, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _container,
                NSLayoutAttribute.Bottom, 1, 0),
                NSLayoutConstraint.Create(horizontalDivider, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _container,
                NSLayoutAttribute.Left, 1, 0),
                NSLayoutConstraint.Create(horizontalDivider, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _container,
                NSLayoutAttribute.Right, 1, 0),
                NSLayoutConstraint.Create(horizontalDivider, NSLayoutAttribute.Height, NSLayoutRelation.Equal, null,
                NSLayoutAttribute.NoAttribute, 1, DimensionHelper.BorderWidth),

            });

			Add(_container);
			AddConstraints(new []
			{
				NSLayoutConstraint.Create(_container, NSLayoutAttribute.Top, NSLayoutRelation.Equal, this, NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_container, NSLayoutAttribute.Left, NSLayoutRelation.Equal, this, NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_container, NSLayoutAttribute.Right, NSLayoutRelation.Equal, this, NSLayoutAttribute.Right, 1, 0),
				NSLayoutConstraint.Create(_container, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, this, NSLayoutAttribute.Bottom, 1, 0),
			});
		}

		private void InitUIElement()
		{
            _container = UIHelper.CreateAlphaView(0, 0, 0, ColorHelper.NeutralGray);
			_lbSidebarItem = UIHelper.CreateCustomColorLabel(DimensionHelper.MediumTextSize);
            _lbCountNotification = UIHelper.CreateLabel(UIColor.White, DimensionHelper.SmallTextSize,false,UITextAlignment.Center);
            _lbCountNotification.UserInteractionEnabled = false;
            _lbCountNotification.AddConstraints(new[]
		    {
                NSLayoutConstraint.Create(_lbCountNotification,NSLayoutAttribute.Height, NSLayoutRelation.Equal,null,
                NSLayoutAttribute.NoAttribute, 1,DimensionHelper.NotificationCountSize),
                NSLayoutConstraint.Create(_lbCountNotification,NSLayoutAttribute.Width, NSLayoutRelation.Equal,null,
                NSLayoutAttribute.NoAttribute, 1,DimensionHelper.NotificationCountSize),
            });
            _lbCountNotification.LayoutMargins = new UIEdgeInsets(1,1,1,1);
		    _lbCountNotification.Layer.CornerRadius = 8;
		    _lbCountNotification.Layer.MasksToBounds = true;
            _lbCountNotification.BackgroundColor = UIColor.Red;
		    _lbCountNotification.Hidden = true;
            _ivSidebarItem = UIHelper.CreateAlphaImageView(0, 0, DimensionHelper.SidebarItemSidePadding);
		}

		private void CreateBinding()
		{
			var set = this.CreateBindingSet<SidebarItemCell, SidebarItemViewModel>();
			set.Bind(_container).For(v => v.ClickCommand).To(vm => vm.ItemClickCommand);
			set.Bind(_container).For(v => v.BackgroundColor).To(vm => vm.ItemStatus).WithConversion("SidebarMenuStatusToColor");

			set.Bind(_lbSidebarItem).To(vm => vm.ItemTitle);
			set.Bind(_lbSidebarItem).For(v => v.TextColor).To(vm => vm.LabelStatus).WithConversion("SidebarMenuStatusLabelToColor");

            set.Bind(_lbCountNotification).For(v => v.Text).To(vm => vm.TotalNotification);
            set.Bind(_lbCountNotification).For(v => v.Hidden).To(vm => vm.IsCountShow).WithConversion("InvertBool");

            set.Bind(_ivSidebarItem).For(v => v.Image).To(vm => vm.IconPath).WithConversion("IconNameToUIImage");
			set.Apply();
		}
	}
}