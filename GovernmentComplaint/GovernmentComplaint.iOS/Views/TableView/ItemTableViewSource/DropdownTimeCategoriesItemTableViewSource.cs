using System;
using Foundation;
using GovernmentComplaint.iOS.Helpers;
using GovernmentComplaint.iOS.Views.TableView.ItemCells;
using MvvmCross.Binding.iOS.Views;
using UIKit;

namespace GovernmentComplaint.iOS.Views.TableView.ItemTableViewSource
{
    public class DropdownTimeCategoriesItemTableViewSource : MvxStandardTableViewSource
    {
        public DropdownTimeCategoriesItemTableViewSource(UITableView tableView) : base(tableView)
        {
            tableView.RegisterClassForCellReuse(typeof(DropdownTimeCategoryItemCell), new NSString("DropdownTimeCategoryItemCell"));
        }

        protected override UITableViewCell GetOrCreateCellFor(UITableView tableView, NSIndexPath indexPath, object item)
        {
            return tableView.DequeueReusableCell("DropdownTimeCategoryItemCell") as DropdownTimeCategoryItemCell;
        }

        public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
        {
            return DimensionHelper.CategoryItemHeight;
        }
    }
}