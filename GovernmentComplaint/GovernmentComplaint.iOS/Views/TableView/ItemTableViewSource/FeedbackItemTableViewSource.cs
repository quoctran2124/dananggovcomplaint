using System;
using System.Diagnostics;
using System.Windows.Input;
using Foundation;
using GovernmentComplaint.iOS.Helpers;
using GovernmentComplaint.iOS.Views.TableView.ItemCells;
using MvvmCross.Binding.ExtensionMethods;
using MvvmCross.Binding.iOS.Views;
using UIKit;

namespace GovernmentComplaint.iOS.Views.TableView.ItemTableViewSource
{
    public class FeedbackItemTableViewSource : MvxStandardTableViewSource
    {
        public ICommand LoadMoreData { get; set; }

        public FeedbackItemTableViewSource(UITableView tableView) : base(tableView)
        {
            tableView.RegisterClassForCellReuse(typeof (FeedbackItemCell), new NSString("FeedbackItemCell"));
        }

        public override nint NumberOfSections(UITableView tableView)
        {
            return 1;
        }

        protected override UITableViewCell GetOrCreateCellFor(UITableView tableView, NSIndexPath indexPath, object item)
        {
            return tableView.DequeueReusableCell("FeedbackItemCell") as FeedbackItemCell;
        }

        public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
        {
            return DimensionHelper.FeedbackHeight;
        }

        public override void WillDisplay(UITableView tableView, UITableViewCell cell, NSIndexPath indexPath)
        {
            if (indexPath.Row == RowsInSection(tableView, 0)  - 1)
            {
                LoadMoreData.Execute(null);
            }
        }
    }
}