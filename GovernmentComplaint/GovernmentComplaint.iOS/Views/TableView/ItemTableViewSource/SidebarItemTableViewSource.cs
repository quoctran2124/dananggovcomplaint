using System;
using Foundation;
using GovernmentComplaint.iOS.Helpers;
using GovernmentComplaint.iOS.Views.TableView.ItemCells;
using MvvmCross.Binding.iOS.Views;
using UIKit;

namespace GovernmentComplaint.iOS.Views.TableView.ItemTableViewSource
{
	public class SidebarItemTableViewSource : MvxTableViewSource
	{
		public SidebarItemTableViewSource(UITableView tableView) : base(tableView)
        {
			tableView.RegisterClassForCellReuse(typeof(SidebarItemCell), new NSString("SidebarItemCell"));
		}

		protected override UITableViewCell GetOrCreateCellFor(UITableView tableView, NSIndexPath indexPath, object item)
		{
			return tableView.DequeueReusableCell("SidebarItemCell") as SidebarItemCell;
		}

		public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
		{
			return DimensionHelper.SidebarItemHeight;
		}
	}
}