using Foundation;
using GovernmentComplaint.iOS.Helpers;
using GovernmentComplaint.iOS.Views.TableView.ItemCells;
using MvvmCross.Binding.iOS.Views;
using System;
using UIKit;

namespace GovernmentComplaint.iOS.Views.TableView.ItemTableViewSource
{
    public class SortingItemTableViewSource : MvxStandardTableViewSource
    {
        public SortingItemTableViewSource(UITableView tableView) : base(tableView)
        {
            tableView.RegisterClassForCellReuse(typeof(SortingItemCell), new NSString("SortingItemCell"));
        }

        protected override UITableViewCell GetOrCreateCellFor(UITableView tableView, NSIndexPath indexPath, object item)
        {
            return tableView.DequeueReusableCell("SortingItemCell") as SortingItemCell;
        }

        public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
        {
            return DimensionHelper.DropdownItemHeight;
        }
    }
}