using Foundation;
using GovernmentComplaint.iOS.Helpers;
using GovernmentComplaint.iOS.Views.TableView.ItemCells;
using MvvmCross.Binding.iOS.Views;
using System;
using UIKit;

namespace GovernmentComplaint.iOS.Views.TableView.ItemTableViewSources
{
    public class HistorySortingItemTableViewSource : MvxStandardTableViewSource
    {
        public HistorySortingItemTableViewSource(UITableView tableView) : base(tableView)
        {
            tableView.RegisterClassForCellReuse(typeof(HistorySortingItemCell), new NSString("HistorySortingItemCell"));
        }

        protected override UITableViewCell GetOrCreateCellFor(UITableView tableView, NSIndexPath indexPath, object item)
        {
            return tableView.DequeueReusableCell("HistorySortingItemCell") as HistorySortingItemCell;
        }

        public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
        {
            return DimensionHelper.DropdownItemHeight;
        }
    }
}