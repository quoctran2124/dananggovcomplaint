using Foundation;
using GovernmentComplaint.iOS.Helpers;
using GovernmentComplaint.iOS.Views.TableView.ItemCells;
using MvvmCross.Binding.iOS.Views;
using System;
using System.Windows.Input;
using UIKit;

namespace GovernmentComplaint.iOS.Views.TableView.ItemTableViewSource
{
    public class NotificationItemTableViewSource : MvxStandardTableViewSource
    {
        public ICommand LoadMoreData { get; set; }
        public override nint NumberOfSections(UITableView tableView)
        {
            return 1;
        }

        public override void WillDisplay(UITableView tableView, UITableViewCell cell, NSIndexPath indexPath)
        {
            if (indexPath.Row == RowsInSection(tableView, 0) - 1)
            {
                LoadMoreData.Execute(null);
            }
        }
        public NotificationItemTableViewSource(UITableView tableView) : base(tableView)
        {
            tableView.RegisterClassForCellReuse(typeof(NotificationItemCell), new NSString("NotificationItemCell"));
        }

        protected override UITableViewCell GetOrCreateCellFor(UITableView tableView, NSIndexPath indexPath, object item)
        {
            return tableView.DequeueReusableCell("NotificationItemCell") as NotificationItemCell;
        }

        public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
        {
            return DimensionHelper.NotificationHeight;
        }
       
    }
}