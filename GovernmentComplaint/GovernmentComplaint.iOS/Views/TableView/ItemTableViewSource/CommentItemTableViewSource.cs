using System;
using System.Collections.Generic;
using Foundation;
using GovernmentComplaint.Core.ViewModels.ItemViewModels;
using GovernmentComplaint.iOS.Helpers;
using GovernmentComplaint.iOS.Views.TableView.ItemCells;
using MvvmCross.Binding.iOS.Views;
using UIKit;

namespace GovernmentComplaint.iOS.Views.TableView.ItemTableViewSource
{
    public class CommentItemTableViewSource : MvxTableViewSource
    {
        private List<CommentItemViewModel> _commentItemViewModels;

        public CommentItemTableViewSource(UITableView tableView) : base(tableView)
        {
            tableView.RegisterClassForCellReuse(typeof(CommentItemCell), new NSString("CommentItemCell"));
        }

        protected override UITableViewCell GetOrCreateCellFor(UITableView tableView, NSIndexPath indexPath, object item)
        {
            return tableView.DequeueReusableCell("CommentItemCell") as CommentItemCell;
        }

        public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
        {
            var t = (CommentItemCell)GetCell(tableView, indexPath);
            
            _commentItemViewModels = _commentItemViewModels ?? ItemsSource as List<CommentItemViewModel>;

            var commentItemViewModel = _commentItemViewModels?[indexPath.Row];

            return t.CellHeight(commentItemViewModel?.CommentContent, commentItemViewModel.ChildrenCommentCount);
        }
    }
}