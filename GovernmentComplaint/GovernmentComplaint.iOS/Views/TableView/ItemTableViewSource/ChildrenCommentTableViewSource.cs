using System;
using System.Collections.Generic;
using Foundation;
using GovernmentComplaint.Core.ViewModels.ItemViewModels;
using GovernmentComplaint.iOS.Helpers;
using GovernmentComplaint.iOS.Views.TableView.ItemCells;
using MvvmCross.Binding.iOS.Views;
using UIKit;

namespace GovernmentComplaint.iOS.Views.TableView.ItemTableViewSource
{
    public class ChildrenCommentTableViewSource : MvxTableViewSource
    {

        private List<ChildrenCommentItemViewModel> _childrenCommentItemViewModels;

        public ChildrenCommentTableViewSource(UITableView tableView) : base(tableView)
        {
            tableView.RegisterClassForCellReuse(typeof(ChildrenCommentItemCell), new NSString("ChildrenCommentItemCell"));
        }

        protected override UITableViewCell GetOrCreateCellFor(UITableView tableView, NSIndexPath indexPath, object item)
        {
            return tableView.DequeueReusableCell("ChildrenCommentItemCell") as ChildrenCommentItemCell;
        }

        public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
        {
            var t = (ChildrenCommentItemCell)GetCell(tableView, indexPath);

            _childrenCommentItemViewModels = _childrenCommentItemViewModels ?? ItemsSource as List<ChildrenCommentItemViewModel>;

            return t.CellHeight(_childrenCommentItemViewModels?[indexPath.Row]?.CommentContent);
        }
    }
}