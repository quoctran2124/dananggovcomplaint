using Foundation;
using GovernmentComplaint.iOS.Helpers;
using GovernmentComplaint.iOS.Views.TableView.ItemCells;
using MvvmCross.Binding.iOS.Views;
using System;
using UIKit;

namespace GovernmentComplaint.iOS.Views.TableView.ItemTableViewSources
{
    public class CategoryItemTableViewSource : MvxStandardTableViewSource
    {
        public CategoryItemTableViewSource(UITableView tableView) : base(tableView)
        {
            tableView.RegisterClassForCellReuse(typeof(CategoryItemCell), new NSString("CategoryItemCell"));
        }

        protected override UITableViewCell GetOrCreateCellFor(UITableView tableView, NSIndexPath indexPath, object item)
        {
            return tableView.DequeueReusableCell("CategoryItemCell") as CategoryItemCell;
        }

        public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
        {
            return DimensionHelper.DropdownItemHeight;
        }
    }
}