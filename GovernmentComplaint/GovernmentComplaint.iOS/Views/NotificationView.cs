using System.Windows.Input;
using CoreGraphics;
using Foundation;
using GovernmentComplaint.Core.ViewModels;
using GovernmentComplaint.iOS.Controls;
using GovernmentComplaint.iOS.Helpers;
using GovernmentComplaint.iOS.Views.Bases;
using GovernmentComplaint.iOS.Views.TableView.ItemTableViewSource;
using MvvmCross.Binding.BindingContext;
using UIKit;

namespace GovernmentComplaint.iOS.Views
{
	public class NotificationView : DetailView
	{
		public override void WillEnterForeground()
		{
			CheckNetworkCommand?.Execute(null);
		}

		protected override void InitView()
		{
			_refreshControl = new MvxUIRefreshControl();
			_notificationTableView = UIHelper.CreateTableView(0, 0, null);
			_notificationTableView.ClipsToBounds = true;
			_notificationTableView.Bounces = true;
			_notificationTableView.AlwaysBounceVertical = true;
            _notificationItemSource = new NotificationItemTableViewSource(_notificationTableView);
			_notificationTableView.Source = _notificationItemSource;
			_lbNoData = UIHelper.CreateLabel(UIColor.Gray, DimensionHelper.MediumTextSize, false, UITextAlignment.Center,
				2);
			_notificationTableView.AddSubview(_refreshControl);
			_notificationTableView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_refreshControl, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal,
					_notificationTableView, NSLayoutAttribute.Top, 1, -30),
				NSLayoutConstraint.Create(_refreshControl, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal,
					_notificationTableView, NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_refreshControl, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal,
					_notificationTableView, NSLayoutAttribute.CenterY, 1, 0)
			});

			View.Add(_lbNoData);
			View.Add(_notificationTableView);
			View.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_lbNoData, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_lbNoData, NSLayoutAttribute.Top, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Top, 1, DimensionHelper.NotificationNoDataLabelMarginTop),
				NSLayoutConstraint.Create(_notificationTableView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_notificationTableView, NSLayoutAttribute.Left, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.Left, 1, 0),
			    NSLayoutConstraint.Create(_notificationTableView, NSLayoutAttribute.Right, NSLayoutRelation.Equal, View,
			        NSLayoutAttribute.Right, 1, 0),
			    NSLayoutConstraint.Create(_notificationTableView, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, View,
			        NSLayoutAttribute.Bottom, 1, 0),
            });
			base.InitView();
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			CheckNetworkCommand?.Execute(null);
			if (LastComplaintItemPosition > 0)
			{
				_notificationTableView.ScrollToRow(NSIndexPath.FromRowSection(LastComplaintItemPosition, 0),
					UITableViewScrollPosition.Top, true);
			}
		}

		protected override void CreateBinding()
		{
			base.CreateBinding();
			var set = this.CreateBindingSet<NotificationView, NotificationViewModel>();
			//Notification List
			set.Bind(_notificationItemSource).To(vm => vm.NotificationItemViewModels);
			set.Bind(_notificationItemSource).For(v => v.LoadMoreData).To(vm => vm.LoadMoreData);
			set.Bind(_lbNoData).To(vm => vm.NoData);
			set.Bind(_lbNoData).For(v => v.Hidden).To(vm => vm.IsNoDataTextShown).WithConversion("InvertBool");
			set.Bind(this).For(v => v.CheckNetworkCommand).To(vm => vm.CheckNetworkCommand);
			set.Bind(_refreshControl)
				.For(x => x.IsRefreshing)
				.To(vm => vm.IsRefreshing);
			set.Bind(_refreshControl)
				.For(x => x.RefreshCommand)
				.To(vm => vm.RefreshCommand);
			set.Bind(this).For(v => v.MoveListToTop).To(vm => vm.MoveListToTop);
			set.Bind(this).For(v => v.LastComplaintItemPosition).To(vm => vm.LastComplaintItemPosition);
			set.Apply();
		}

		private bool _moveListToTop;

		public bool MoveListToTop
		{
			get { return _moveListToTop; }
			set
			{
				if (value)
				{
					_notificationTableView.ContentOffset = new CGPoint(0, 0 - _notificationTableView.ContentInset.Top);
				}
			}
		}

		#region UI Elements

		//List notification

		public int LastComplaintItemPosition { get; set; }
		public ICommand CheckNetworkCommand { get; set; }
		private MvxUIRefreshControl _refreshControl;
		private UITableView _notificationTableView;
		private NotificationItemTableViewSource _notificationItemSource;
		private UILabel _lbNoData;

		#endregion
	}
}