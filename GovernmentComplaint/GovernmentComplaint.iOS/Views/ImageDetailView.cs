using System;
using CoreGraphics;
using GovernmentComplaint.Core.ViewModels;
using GovernmentComplaint.iOS.Controls;
using GovernmentComplaint.iOS.Helpers;
using GovernmentComplaint.iOS.Views.Bases;
using MvvmCross.Binding.BindingContext;
using UIKit;

namespace GovernmentComplaint.iOS.Views
{
	public class ImageDetailView : DetailView
	{
		private int _imagesQuantity;

		public int ImagesQuantity
		{
			get { return _imagesQuantity; }
			set
			{
				_imagesQuantity = value;
				var lastImageView = GetLastImageView(value);
				var lastPointView = GetLastPointImageView(value);
				_imageLayout.RemoveConstraint(_imageScrollViewRightConstraint);
				_imagePointLayout.RemoveConstraint(_pointScrollViewRightConstraint);

				_imageScrollViewRightConstraint = NSLayoutConstraint.Create(_imageLayout, NSLayoutAttribute.Right,
					NSLayoutRelation.Equal,
					lastImageView, NSLayoutAttribute.Right, 1, 0);
				_pointScrollViewRightConstraint = NSLayoutConstraint.Create(_imagePointLayout, NSLayoutAttribute.Right,
					NSLayoutRelation.Equal,
					lastPointView, NSLayoutAttribute.Right, 1, 0);

				_imageLayout.AddConstraint(_imageScrollViewRightConstraint);
				_imagePointLayout.AddConstraint(_pointScrollViewRightConstraint);
			}
		}

		public int ScrollPosition { get; set; }
		
		protected override void InitView()
		{
			CreateImageSlider();
			CreateImagePoint();
			View.BackgroundColor = ColorHelper.Black;
			View.Add(_imageScrollView);
			View.Add(_imagePointLayout);
			View.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_imageScrollView, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					View, NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_imageScrollView, NSLayoutAttribute.Right, NSLayoutRelation.Equal,
					View, NSLayoutAttribute.Right, 1, 0),
				NSLayoutConstraint.Create(_imageScrollView, NSLayoutAttribute.Top, NSLayoutRelation.Equal,
					View, NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_imageScrollView, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal,
					View, NSLayoutAttribute.Bottom, 1, 0),
				NSLayoutConstraint.Create(_imagePointLayout, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, View,
					NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(_imagePointLayout, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal,
					_imageScrollView, NSLayoutAttribute.Bottom, 1, -DimensionHelper.MediumContentMargin)
			});
		}

		private void CreateImageSlider()
		{
			CreateImageLayout();
			_imageScrollView = UIHelper.CreateScrollView(0, DimensionHelper.DetailImageViewHeight, ColorHelper.Black);
			_imageScrollView.ShowsHorizontalScrollIndicator = false;
			_imageScrollView.PagingEnabled = true;
			_imageScrollView.Scrolled += OnImageScroll;
			_imageScrollView.AddSubview(_imageLayout);
			_imageScrollView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_imageLayout, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _imageScrollView,
					NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_imageLayout, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _imageScrollView,
					NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_imageLayout, NSLayoutAttribute.Right, NSLayoutRelation.Equal,
					_imageScrollView, NSLayoutAttribute.Right, 1, 0),
				NSLayoutConstraint.Create(_imageLayout, NSLayoutAttribute.Height, NSLayoutRelation.Equal,
					_imageScrollView, NSLayoutAttribute.Height, 1, 0)
			});
		}

		private void CreateImageLayout()
		{
			_imageLayout = UIHelper.CreateView(0, 0, ColorHelper.Black);
			_image1 = UIHelper.CreateCustomImageView(0, 0);
			_image2 = UIHelper.CreateCustomImageView(0, 0);
			_image3 = UIHelper.CreateCustomImageView(0, 0);

			_imageZoom1 = UIHelper.CreateScrollView(0, 0, UIColor.Black);
			_imageZoom2 = UIHelper.CreateScrollView(0, 0, UIColor.Black);
			_imageZoom3 = UIHelper.CreateScrollView(0, 0, UIColor.Black);

			_imageScrollViewRightConstraint = NSLayoutConstraint.Create(_imageLayout, NSLayoutAttribute.Right,
				NSLayoutRelation.Equal, _imageZoom1, NSLayoutAttribute.Right, 1, 0);

			SetImageInScroll(_image1, _imageZoom1);
			SetImageInScroll(_image2, _imageZoom2);
			SetImageInScroll(_image3, _imageZoom3);

			_imageLayout.AddSubview(_imageZoom1);
			_imageLayout.AddSubview(_imageZoom2);
			_imageLayout.AddSubview(_imageZoom3);
			_imageLayout.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_imageZoom1, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _imageLayout,
					NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_imageZoom1, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _imageLayout,
					NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_imageZoom1, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _imageLayout,
					NSLayoutAttribute.Bottom, 1, 0),
				NSLayoutConstraint.Create(_imageZoom1, NSLayoutAttribute.Width, NSLayoutRelation.Equal, null,
					NSLayoutAttribute.NoAttribute, 1, ResolutionHelper.Width),
				NSLayoutConstraint.Create(_imageZoom2, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _imageLayout,
					NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_imageZoom2, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _imageZoom1,
					NSLayoutAttribute.Right, 1, 0),
				NSLayoutConstraint.Create(_imageZoom2, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _imageLayout,
					NSLayoutAttribute.Bottom, 1, 0),
				NSLayoutConstraint.Create(_imageZoom2, NSLayoutAttribute.Width, NSLayoutRelation.Equal, null,
					NSLayoutAttribute.NoAttribute, 1, ResolutionHelper.Width),
				NSLayoutConstraint.Create(_imageZoom3, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _imageLayout,
					NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_imageZoom3, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _imageZoom2,
					NSLayoutAttribute.Right, 1, 0),
				NSLayoutConstraint.Create(_imageZoom3, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _imageLayout,
					NSLayoutAttribute.Bottom, 1, 0),
				NSLayoutConstraint.Create(_imageZoom3, NSLayoutAttribute.Width, NSLayoutRelation.Equal, null,
					NSLayoutAttribute.NoAttribute, 1, ResolutionHelper.Width),
				_imageScrollViewRightConstraint
			});
		}

		public override void ViewDidLayoutSubviews()
		{
			base.ViewDidLayoutSubviews();
			SetZoomScale(_image1, _imageZoom1);
			SetZoomScale(_image2, _imageZoom2);
			SetZoomScale(_image3, _imageZoom3);
			_imageScrollView.SetContentOffset(new CGPoint(ScrollPosition * View.Frame.Width, 0), false);
		}

		private void SetZoomScale(UIImageView imageView, UIScrollView scrollView)
		{
			if (imageView.Image != null)
			{
				_widthScale = ResolutionHelper.Width / imageView.Image.Size.Width;
				scrollView.MinimumZoomScale = _widthScale;
				scrollView.ZoomScale = _widthScale;
			}

		}

		private void OnDoubleTapImage1(UIGestureRecognizer gesture)
		{
			nfloat zoomScale = 1;
			if (_imageZoom1.ZoomScale >= _imageZoom1.MinimumZoomScale)
			{
				zoomScale = _imageZoom1.MaximumZoomScale;

				if (_imageZoom1.ZoomScale == _imageZoom1.MaximumZoomScale)
					zoomScale = _imageZoom1.MinimumZoomScale;
			}
			_imageZoom1.SetZoomScale(zoomScale, true);
		}

		private void OnDoubleTapImage2(UIGestureRecognizer gesture)
		{
			nfloat zoomScale = 1;
			if (_imageZoom2.ZoomScale >= _imageZoom2.MinimumZoomScale)
			{
				zoomScale = _imageZoom2.MaximumZoomScale;

				if (_imageZoom2.ZoomScale == _imageZoom2.MaximumZoomScale)
					zoomScale = _imageZoom2.MinimumZoomScale;
			}
			_imageZoom2.SetZoomScale(zoomScale, true);
		}

		private void OnDoubleTapImage3(UIGestureRecognizer gesture)
		{
			nfloat zoomScale = 1;
			if (_imageZoom3.ZoomScale >= _imageZoom3.MinimumZoomScale)
			{
				zoomScale = _imageZoom3.MaximumZoomScale;

				if (_imageZoom3.ZoomScale == _imageZoom3.MaximumZoomScale)
					zoomScale = _imageZoom3.MinimumZoomScale;
			}
			_imageZoom3.SetZoomScale(zoomScale, true);
		}

		private void SetImageInScroll(UIView imageView, UIScrollView scrollView)
		{
			scrollView.AddSubview(imageView);
			scrollView.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(imageView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, scrollView,
					NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(imageView, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, scrollView,
					NSLayoutAttribute.Bottom, 1, 0),
				NSLayoutConstraint.Create(imageView, NSLayoutAttribute.Left, NSLayoutRelation.Equal, scrollView,
					NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(imageView, NSLayoutAttribute.Right, NSLayoutRelation.Equal, scrollView,
					NSLayoutAttribute.Right, 1, 0)
			});

			scrollView.MaximumZoomScale = 2f;
			scrollView.ViewForZoomingInScrollView += sv => imageView;

			UITapGestureRecognizer doubletap;

			if (Equals(scrollView, _imageZoom1))
				doubletap = new UITapGestureRecognizer(OnDoubleTapImage1);
			else
				doubletap = Equals(scrollView, _imageZoom2)
					? new UITapGestureRecognizer(OnDoubleTapImage2)
					: new UITapGestureRecognizer(OnDoubleTapImage3);

			doubletap.NumberOfTapsRequired = 2;

			scrollView.AddGestureRecognizer(doubletap);
			scrollView.ShowsHorizontalScrollIndicator = false;

			imageView.Center = scrollView.Center;
		}

		private void CreateImagePoint()
		{
			_imagePointLayout = UIHelper.CreateView(0, DimensionHelper.LayoutMargin, ColorHelper.Transparent);
			_imagePoint1 = UIHelper.CreateView(DimensionHelper.PointSize, DimensionHelper.PointSize, ColorHelper.Gray);
			_imagePoint1.Layer.CornerRadius = DimensionHelper.PointSize / 2;

			_imagePoint2 = UIHelper.CreateView(DimensionHelper.PointSize, DimensionHelper.PointSize,
				ColorHelper.LightGray);
			_imagePoint2.Layer.CornerRadius = DimensionHelper.PointSize / 2;

			_imagePoint3 = UIHelper.CreateView(DimensionHelper.PointSize, DimensionHelper.PointSize,
				ColorHelper.LightGray);
			_imagePoint3.Layer.CornerRadius = DimensionHelper.PointSize / 2;

			_pointScrollViewRightConstraint = NSLayoutConstraint.Create(_imagePointLayout, NSLayoutAttribute.Right,
				NSLayoutRelation.Equal, _imagePoint2, NSLayoutAttribute.Right, 1, 0);

			_imagePointLayout.AddSubview(_imagePoint1);
			_imagePointLayout.AddSubview(_imagePoint2);
			_imagePointLayout.AddSubview(_imagePoint3);
			_imagePointLayout.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_imagePoint1, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _imagePointLayout,
					NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_imagePoint1, NSLayoutAttribute.Left, NSLayoutRelation.Equal,
					_imagePointLayout, NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(_imagePoint2, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _imagePointLayout,
					NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_imagePoint2, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _imagePoint1,
					NSLayoutAttribute.Right, 1, DimensionHelper.PointSize),
				NSLayoutConstraint.Create(_imagePoint3, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _imagePointLayout,
					NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(_imagePoint3, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _imagePoint2,
					NSLayoutAttribute.Right, 1, DimensionHelper.PointSize),
				_pointScrollViewRightConstraint
				//NSLayoutConstraint.Create(_imagePointLayout, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _imagePoint3, NSLayoutAttribute.Right, 1, 0),
			});
		}

		private void OnImageScroll(object sender, EventArgs e)
		{
			var scroll = (UIScrollView) sender;
			if (scroll.ContentOffset.X == 0)
			{
				_imagePoint1.BackgroundColor = ColorHelper.Gray;
				_imagePoint2.BackgroundColor = ColorHelper.LightGray;
				_imagePoint3.BackgroundColor = ColorHelper.LightGray;
				_imageZoom2.ZoomScale = _imageZoom2.MinimumZoomScale;
				_imageZoom3.ZoomScale = _imageZoom3.MinimumZoomScale;
			}
			else if (scroll.ContentOffset.X == ResolutionHelper.Width)
			{
				_imagePoint1.BackgroundColor = ColorHelper.LightGray;
				_imagePoint2.BackgroundColor = ColorHelper.Gray;
				_imagePoint3.BackgroundColor = ColorHelper.LightGray;
				_imageZoom1.ZoomScale = _imageZoom1.MinimumZoomScale;
				_imageZoom3.ZoomScale = _imageZoom3.MinimumZoomScale;
			}
			else if (scroll.ContentOffset.X == ResolutionHelper.Width * 2)
			{
				_imagePoint1.BackgroundColor = ColorHelper.LightGray;
				_imagePoint2.BackgroundColor = ColorHelper.LightGray;
				_imagePoint3.BackgroundColor = ColorHelper.Gray;
				_imageZoom2.ZoomScale = _imageZoom2.MinimumZoomScale;
				_imageZoom1.ZoomScale = _imageZoom1.MinimumZoomScale;
			}
		}

		private UIScrollView GetLastImageView(int imageQuantity)
		{
			switch (imageQuantity)
			{
				case 3:
					return _imageZoom3;
				case 2:
					return _imageZoom2;
				case 1:
				case 0:
					return _imageZoom1;
				default:
					return null;
			}
		}

		private UIView GetLastPointImageView(int imageQuantity)
		{
			switch (imageQuantity)
			{
				case 3:
					return _imagePoint3;
				case 2:
					return _imagePoint2;
				case 1:
				case 0:
					return _imagePoint1;
				default:
					return null;
			}
		}

		protected override void CreateBinding()
		{
			base.CreateBinding();

			var bindingSet = this.CreateBindingSet<ImageDetailView, ImageDetailViewModel>();

			bindingSet.Bind(_image1).For("ZoomedImageUrl").To(vm => vm.ImagePath1);
			bindingSet.Bind(_image2).For("ZoomedImageUrl").To(vm => vm.ImagePath2);
			bindingSet.Bind(_image3).For("ZoomedImageUrl").To(vm => vm.ImagePath3);

			bindingSet.Bind(_image2).For(v => v.Hidden).To(vm => vm.IsImage2Shown).WithConversion("InvertBool");
			bindingSet.Bind(_image3).For(v => v.Hidden).To(vm => vm.IsImage3Shown).WithConversion("InvertBool");

			bindingSet.Bind(_imagePoint1).For(v => v.Hidden).To(vm => vm.IsIndicator1Shown).WithConversion("InvertBool");
			bindingSet.Bind(_imagePoint2).For(v => v.Hidden).To(vm => vm.IsImage2Shown).WithConversion("InvertBool");
			bindingSet.Bind(_imagePoint3).For(v => v.Hidden).To(vm => vm.IsImage3Shown).WithConversion("InvertBool");

			bindingSet.Bind(this).For(v => v.ImagesQuantity).To(vm => vm.ImagesQuantity);

			bindingSet.Bind(this).For(v => v.ScrollPosition).To(vm => vm.ScrollPosition);

			bindingSet.Apply();
		}

		#region UI Elements

		private UIView _imageLayout;
		private UIScrollView _imageScrollView;
		private UIScrollView _imageZoom1, _imageZoom2, _imageZoom3;
		private CustomImageView _image1;
		private CustomImageView _image2;
		private CustomImageView _image3;
		private UIView _imagePointLayout;
		private UIView _imagePoint1;
		private UIView _imagePoint2;
		private UIView _imagePoint3;
		private NSLayoutConstraint _imageScrollViewRightConstraint;
		private NSLayoutConstraint _pointScrollViewRightConstraint;
		private nfloat _widthScale;

		public override void WillEnterForeground()
		{
		}

		#endregion
	}
}