using System;
using UIKit;

namespace GovernmentComplaint.iOS.Controls
{
    public class CollapsableView : UIView
    {
        private readonly NSLayoutConstraint _heightConstrainst;
        private readonly NSLayoutConstraint _widthConstrainst;


        public CollapsableView(nfloat width, nfloat height)
        {
            TranslatesAutoresizingMaskIntoConstraints = false;

            _widthConstrainst = NSLayoutConstraint.Create(this, NSLayoutAttribute.Width, NSLayoutRelation.Equal, null,
                NSLayoutAttribute.NoAttribute, 1, width);

            _heightConstrainst = NSLayoutConstraint.Create(this, NSLayoutAttribute.Height, NSLayoutRelation.Equal, null,
                NSLayoutAttribute.NoAttribute, 1, height);

            Height = height;
            Width = width;
            AddConstraints(new[]
            {
                _widthConstrainst,
                _heightConstrainst
            });
        }
        private nfloat _height;
        public nfloat Height
        {
            get { return _height; }
            set
            {
                _height = value;
                _heightConstrainst.Constant = value;
            }
        }
        private nfloat _width;
        public nfloat Width
        {
            get { return _width; }
            set
            {
                _width = value;
                _widthConstrainst.Constant = value;
            }
        }

        public override bool Hidden
        {
            get { return base.Hidden; }
            set
            {
                base.Hidden = value;
                _heightConstrainst.Constant = Hidden ? 0 : _height;
                _widthConstrainst.Constant = Hidden ? 0 : _width;
            }
        }
    }
}