using System;
using System.Collections.Generic;
using UIKit;

namespace GovernmentComplaint.iOS.Controls
{
    public class PickerViewModel : UIPickerViewModel
    {
        public IList<object> values;

        public PickerViewModel(IList<object> values)
        {
            this.values = values;
        }

        public event EventHandler<PickerChangedEventArgs> PickerChanged;

        public override nint GetComponentCount(UIPickerView picker)
        {
            return 1;
        }

        public override nint GetRowsInComponent(UIPickerView picker, nint component)
        {
            return values.Count;
        }

        public override string GetTitle(UIPickerView picker, nint row, nint component)
        {
            return values[(int)row].ToString();
        }

        public override nfloat GetRowHeight(UIPickerView picker, nint component)
        {
            return 40f;
        }

        public override void Selected(UIPickerView picker, nint row, nint component)
        {
            if (PickerChanged != null)
            {
                PickerChanged(this, new PickerChangedEventArgs { SelectedValue = values[(int)row] });
            }
        }
    }

    public class PickerChangedEventArgs : EventArgs
    {
        public object SelectedValue { get; set; }
    }
}
