using System;
using System.Windows.Input;
using Foundation;
using UIKit;

namespace GovernmentComplaint.iOS.Controls
{
    public class AlphaUIButton : UIButton
    {
        public event EventHandler Clicked;

        public ICommand ClickCommand { get; set; }
        public UIColor TextColor
        {
            set
            {
                SetTitleColor(value, UIControlState.Normal);
            }
        }

        private nfloat _width;
        private NSLayoutConstraint _widthConstrainst;

        public NSLayoutConstraint WidthConstraint
        {
            get { return _widthConstrainst; }
            set
            {
                _widthConstrainst = value;
                _width = _widthConstrainst.Constant;
                AddConstraint(value);
            }
        }

        public UIImage Icon
        {
            set
            {
                SetBackgroundImage(value, UIControlState.Normal);

                if (value != null)
                {
                    _widthConstrainst.Constant = _width;
                }
                else
                {
                    if (Frame != null)
                    {
                        SizeToFit();
                        _widthConstrainst.Constant = Frame.Size.Width;
                    }
                }
            }
        }

        public override void TouchesBegan(NSSet touches, UIEvent evt)
        {
            if (Enabled)
            {
                Alpha = 0.5f;
            }

            base.TouchesBegan(touches, evt);
        }

        public override void TouchesCancelled(NSSet touches, UIEvent evt)
        {
            base.TouchesCancelled(touches, evt);

            Alpha = 1f;
        }

        public override void TouchesEnded(NSSet touches, UIEvent evt)
        {
            base.TouchesEnded(touches, evt);

            Alpha = 1f;
            ClickCommand?.Execute(null);
            Clicked?.Invoke(this, EventArgs.Empty);
        }

    }
}