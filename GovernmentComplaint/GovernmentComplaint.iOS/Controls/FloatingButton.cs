using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Foundation;
using GovernmentComplaint.iOS.Helpers;
using UIKit;

namespace GovernmentComplaint.iOS.Controls
{
    public class FloatingButton : ExpandedImageView
    {
        private bool _enabled;
        private UIView _floatingButton;
        private AlphaImageView _floatingImage;

        public FloatingButton(UIEdgeInsets padding, UIViewContentMode contentMode)
            : base(padding, contentMode)
        {
            UserInteractionEnabled = true;
            Enabled = true;
            AddGestureRecognizer(new UITapGestureRecognizer(OnClicked));
            CreateFloatingButton();
            Add(_floatingButton);
            AddConstraints(new[]
            {
                NSLayoutConstraint.Create(_floatingButton, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal,
                    this, NSLayoutAttribute.CenterY, 1, 0),
                NSLayoutConstraint.Create(_floatingButton, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal,
                    this, NSLayoutAttribute.CenterX, 1, 0)
            });
        }

        public nfloat Width { get; set; }
        public ICommand ClickCommand { get; set; }

        public bool Enabled
        {
            get { return _enabled; }
            set
            {
                _enabled = value;

                if (value)
                    Alpha = 1f;
                else
                    Alpha = 0.98f;
            }
        }

        public override bool Hidden
        {
            get { return base.Hidden; }
            set
            {
                base.Hidden = value;
                WidthConstraint.Constant = value ? 0 : Width;
            }
        }

        public event EventHandler Clicked;

        private void CreateFloatingButton()
        {
            _floatingButton = UIHelper.CreateView(DimensionHelper.FloatingButtonSize,
                DimensionHelper.FloatingButtonSize, ColorHelper.DarkBlue);
            _floatingButton.Layer.CornerRadius = DimensionHelper.FloatingButtonSize / 2;
            _floatingButton.BackgroundColor = ColorHelper.DarkBlue;

            _floatingImage = UIHelper.CreateAlphaImageView(DimensionHelper.FloatingButtonSize,
                DimensionHelper.FloatingButtonSize, DimensionHelper.FloatingButtonPadding);
            _floatingImage.Image = UIImage.FromFile(ImageHelper.Send);
            _floatingImage.UserInteractionEnabled = false;

            _floatingButton.AddSubview(_floatingImage);
            _floatingButton.AddConstraints(new[]
            {
                NSLayoutConstraint.Create(_floatingImage, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal,
                    _floatingButton, NSLayoutAttribute.CenterX, 1, 0),
                NSLayoutConstraint.Create(_floatingImage, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal,
                    _floatingButton, NSLayoutAttribute.CenterY, 1, 0)
            });
        }

        private void OnClicked()
        {
            if (Enabled)
            {
                EffectAlpha();

                if (ClickCommand != null)
                    ClickCommand.Execute(null);

                Clicked?.Invoke(this, null);
            }
        }

        public override void TouchesBegan(NSSet touches, UIEvent evt)
        {
            if (Enabled)
                Alpha = 0.98f;
            base.TouchesBegan(touches, evt);
        }

        public override void TouchesCancelled(NSSet touches, UIEvent evt)
        {
            base.TouchesCancelled(touches, evt);

            Alpha = 1f;
        }

        public override void TouchesEnded(NSSet touches, UIEvent evt)
        {
            base.TouchesEnded(touches, evt);

            Alpha = 1f;
        }

        private async void EffectAlpha()
        {
            Alpha = 0.98f;
            await Task.Delay(100);

            if (Enabled)
                Alpha = 1f;
        }
    }
}