using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Foundation;
using UIKit;

namespace GovernmentComplaint.iOS.Controls
{
    public class AlphaLabel : UILabel
    {
        public event EventHandler Clicked;

        public ICommand ClickCommand { get; set; }

        public bool ParameterCommand { get; set; }

        public AlphaLabel()
        {
            Initialize();
        }

        private void Initialize()
        {
            UserInteractionEnabled = true;
            AddGestureRecognizer(new UITapGestureRecognizer(OnClicked));
        }

        public override void TouchesBegan(NSSet touches, UIEvent evt)
        {
            Alpha = 0.5f;

            base.TouchesBegan(touches, evt);
        }

        public override void TouchesCancelled(NSSet touches, UIEvent evt)
        {
            base.TouchesCancelled(touches, evt);

            Alpha = 1f;
        }

        public override void TouchesEnded(NSSet touches, UIEvent evt)
        {
            base.TouchesEnded(touches, evt);

            Alpha = 1f;
        }

        private void OnClicked()
        {
            EffectAlpha();

            if (ParameterCommand)
            {
                ClickCommand?.Execute(true);
            }
            else
            {
                ClickCommand?.Execute(null);
            }

            Clicked?.Invoke(this, EventArgs.Empty);
        }

        private async void EffectAlpha()
        {
            Alpha = 0.5f;
            await Task.Delay(200);
            Alpha = 1f;
        }
    }
}
