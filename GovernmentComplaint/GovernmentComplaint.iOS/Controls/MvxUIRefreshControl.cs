using Foundation;
using System;
using System.Windows.Input;
using MvvmCross.iOS.Views;
using UIKit;

namespace GovernmentComplaint.iOS.Controls
{
    public class MvxUIRefreshControl : UIRefreshControl
    {
        private string _message;
        /// <summary>
        /// Gets or sets the message to display
        /// </summary>
        /// <value>The message.</value>
        public string Message
        {
            get { return _message; }
            set
            {
                _message = value ?? string.Empty;
                AttributedTitle = new NSAttributedString(_message);
            }
        }

        private bool _isRefreshing;
        /// <summary>
        /// Gets or sets a value indicating whether this instance is refreshing.
        /// </summary>
        /// <value><c>true</c> if this instance is refreshing; otherwise, <c>false</c>.</value>
        public bool IsRefreshing
        {
            get { return _isRefreshing; }
            set
            {
                _isRefreshing = value;

                if (_isRefreshing)
                {
                    BeginRefreshing();
                }
                else
                {
                    EndRefreshing();
                }
            }
        }

        /// <summary>
        /// Gets or sets the refresh command.
        /// </summary>
        /// <value>The refresh command.</value>
        public ICommand RefreshCommand { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="MvvmCross.iOS.Views.MvxUIRefreshControl"/> class.
        /// </summary>
        public MvxUIRefreshControl()
        {
            TranslatesAutoresizingMaskIntoConstraints = false;
            ValueChanged += OnValueChanged;
        }

        public void UnsubscribeEvent()
        {
            ValueChanged -= OnValueChanged;
        }

        private void OnValueChanged(object sender, EventArgs e)
        {
            RefreshCommand?.Execute(null);
        }
    }
}