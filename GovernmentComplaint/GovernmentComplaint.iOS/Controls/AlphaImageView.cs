﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Foundation;
using GovernmentComplaint.iOS.Helpers;
using UIKit;
using SDWebImage;
namespace GovernmentComplaint.iOS.Controls
{
    public class AlphaImageView : ExpandedImageView
    {
        private bool _enabled;

        public AlphaImageView(UIEdgeInsets padding, UIViewContentMode contentMode)
            : base(padding, contentMode)
        {
            UserInteractionEnabled = true;
            Enabled = true;
            AddGestureRecognizer(new UITapGestureRecognizer(OnClicked));
        }

        public ICommand ClickCommand { get; set; }

        public bool Enabled
        {
            get { return _enabled; }
            set
            {
                _enabled = value;

                if (value)
                {
                    Alpha = 1;
                }
                else
                {
                    Alpha = 0.5f;
                }
            }
        }

        public nfloat Width { get; set; }

        public override bool Hidden
        {
            get { return base.Hidden; }
            set
            {
                base.Hidden = value;
                WidthConstraint.Constant = value ? 0 : Width;
            }
        }

        public event EventHandler Clicked;

        private void OnClicked()
        {
            if (Enabled)
            {
                EffectAlpha();

                if (ClickCommand != null)
                {
                    ClickCommand.Execute(null);
                }

                Clicked?.Invoke(this, null);
            }
        }

        public override void TouchesBegan(NSSet touches, UIEvent evt)
        {
            if (Enabled)
            {
                Alpha = 0.5f;
            }

            base.TouchesBegan(touches, evt);
        }

        public override void TouchesCancelled(NSSet touches, UIEvent evt)
        {
            base.TouchesCancelled(touches, evt);

            Alpha = 1f;
        }

        public override void TouchesEnded(NSSet touches, UIEvent evt)
        {
            base.TouchesEnded(touches, evt);

            Alpha = 1f;
        }

        private async void EffectAlpha()
        {
            Alpha = 0.5f;
            await Task.Delay(100);

            if (Enabled)
            {
                Alpha = 1f;
            }
        }


        public string ImageUrl
        {
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    _imageView.SetImage(new NSUrl(value));
                }
                else
                {
                    Image = UIImage.FromBundle("Images/default_complaint_image");
                }
            }
        }


        

    }
}