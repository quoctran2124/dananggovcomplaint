using System;
using System.Windows.Input;
using GovernmentComplaint.iOS.Helpers;
using UIKit;

namespace GovernmentComplaint.iOS.Controls
{
    public class UploadImagePreviewer : CollapsableView
    {
        public UIImageView ImageView;
        public AlphaImageView CloseButton;

        public UploadImagePreviewer(nfloat width, nfloat height) : base(width, height)
        {
            AddConstraints(new[]
            {
                NSLayoutConstraint.Create(this, NSLayoutAttribute.Width, NSLayoutRelation.Equal, null, NSLayoutAttribute.NoAttribute, 1, width),
                NSLayoutConstraint.Create(this, NSLayoutAttribute.Height, NSLayoutRelation.Equal, null, NSLayoutAttribute.NoAttribute, 1, height),
            });
            InitView();
        }

        private void InitView()
        {
            
            ImageView = UIHelper.CreateImageView(0, 0, UIViewContentMode.ScaleAspectFill);
            ImageView.ClipsToBounds = true;
            ImageView.Layer.CornerRadius = 10;
            ImageView.Layer.BorderColor = UIColor.Gray.CGColor;
            ImageView.Layer.BorderWidth = 1;
            ImageView.Layer.ZPosition = 1;

            CloseButton = UIHelper.CreateAlphaImageView(20, 20, 0);
            CloseButton.Image = UIImage.FromFile(ImageHelper.Remove);
            CloseButton.Layer.ZPosition = 10;

            Add(ImageView);
            Add(CloseButton);

            AddConstraints(new[]
            {
                NSLayoutConstraint.Create(CloseButton, NSLayoutAttribute.Top, NSLayoutRelation.Equal, this, NSLayoutAttribute.Top, 1, 8),
                NSLayoutConstraint.Create(CloseButton, NSLayoutAttribute.Right, NSLayoutRelation.Equal, this, NSLayoutAttribute.Right, 1, -8),

                NSLayoutConstraint.Create(ImageView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, this, NSLayoutAttribute.Top, 1, 0),
                NSLayoutConstraint.Create(ImageView, NSLayoutAttribute.Right, NSLayoutRelation.Equal, this, NSLayoutAttribute.Right, 1, 0),
                NSLayoutConstraint.Create(ImageView, NSLayoutAttribute.Left, NSLayoutRelation.Equal, this, NSLayoutAttribute.Left, 1, 0),
                NSLayoutConstraint.Create(ImageView, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, this, NSLayoutAttribute.Bottom, 1, 0),


            });

            TranslatesAutoresizingMaskIntoConstraints = false;
        }
    }
}
