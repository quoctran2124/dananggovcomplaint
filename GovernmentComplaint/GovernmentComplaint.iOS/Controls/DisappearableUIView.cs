﻿using System;
using UIKit;

namespace GovernmentComplaint.iOS.Controls
{
	public class DisappearableUIView : UIView
	{
		private nfloat _width;
		public nfloat WidthWhenHidden { get; set; }
		private NSLayoutConstraint _widthConstrainst;

		public NSLayoutConstraint WidthConstraint
		{
			get { return _widthConstrainst; }
			set
			{
				_widthConstrainst = value;
				_width = _widthConstrainst.Constant;
				AddConstraint(value);
			}
		}

		public override bool Hidden
		{
			get { return base.Hidden; }
			set
			{
				base.Hidden = value;
				if (_widthConstrainst != null)
					_widthConstrainst.Constant = Hidden ? WidthWhenHidden : _width;
			}
		}
	}
}