using System;
using UIKit;

namespace GovernmentComplaint.iOS.Controls
{
    public class ExpandedImageView : UIView
    {
        public UIImageView _imageView;
        private nfloat _width;
        private NSLayoutConstraint _widthConstraint;

        public ExpandedImageView(UIEdgeInsets padding, UIViewContentMode contentMode)
        {
            TranslatesAutoresizingMaskIntoConstraints = false;
            LayoutMargins = padding;

            _imageView = new UIImageView();
            _imageView.TranslatesAutoresizingMaskIntoConstraints = false;
            _imageView.ContentMode = contentMode;

            AddSubview(_imageView);
            AddConstraints(new[]
            {
                NSLayoutConstraint.Create(_imageView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, this,
                    NSLayoutAttribute.TopMargin, 1, 0),
                NSLayoutConstraint.Create(_imageView, NSLayoutAttribute.Left, NSLayoutRelation.Equal, this,
                    NSLayoutAttribute.LeftMargin, 1, 0),
                NSLayoutConstraint.Create(_imageView, NSLayoutAttribute.Right, NSLayoutRelation.Equal, this,
                    NSLayoutAttribute.RightMargin, 1, 0),
                NSLayoutConstraint.Create(_imageView, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, this,
                    NSLayoutAttribute.BottomMargin, 1, 0)
            });
        }

        public NSLayoutConstraint WidthConstraint
        {
            get { return _widthConstraint; }
            set
            {
                _widthConstraint = value;
                _width = _widthConstraint.Constant;
                AddConstraint(value);
            }
        }

        public UIImage Image
        {
            get { return _imageView.Image; }
            set { _imageView.Image = value; }
        }
    }
}