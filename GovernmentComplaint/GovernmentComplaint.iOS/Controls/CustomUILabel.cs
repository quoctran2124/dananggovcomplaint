using Foundation;
using GovernmentComplaint.iOS.Helpers;
using UIKit;

namespace GovernmentComplaint.iOS.Controls
{
	public class CustomUILabel : UILabel
	{
		public string HtmlText
		{
			get { return this.Text; }
			set
			{
				var attr = new NSAttributedStringDocumentAttributes();
				var nsError = new NSError();
				attr.DocumentType = NSDocumentType.HTML;
				var dict = new NSDictionary();
				dict = new NSMutableDictionary()
				{
					{
						UIStringAttributeKey.Font,
						UIFont.FromName("Helvetica", DimensionHelper.SmallTextSize)
					}
				};
				var data = NSData.FromString(value, NSStringEncoding.Unicode);
				AttributedText = new NSAttributedString(data, attr, ref nsError);
				//var attString = new NSAttributedString(data,attr,out dict,ref nsError);
				//var att = new NSMutableAttributedString(attString);
				//att.AddAttribute(UIStringAttributeKey.Font, UIFont.FromName("Helvetica",DimensionHelper.SmallTextSize), new NSRange(0,att.Length));
				//AttributedText = att;
			}
		}
	}
}