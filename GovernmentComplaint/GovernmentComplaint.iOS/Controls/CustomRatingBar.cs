using PatridgeDev;

namespace GovernmentComplaint.iOS.Controls
{
    public class CustomRatingBar : PDRatingView
    {
        public CustomRatingBar(RatingConfig config) : base(config)
        {
            RatingChosen += CustomRatingBar_RatingChosen;
        }

        public double Rating { get; set; }

        private void CustomRatingBar_RatingChosen(object sender, RatingChosenEventArgs e)
        {
            Rating = e.Rating;
        }
    }
}