using System.Windows.Input;
using Foundation;
using GovernmentComplaint.iOS.Helpers;
using UIKit;

namespace GovernmentComplaint.iOS.Controls
{
	public class CustomTextView : UIView
	{
		private bool _isError;
		public UILabel LbPlaceHolder;
		public UITextView TextView;

		public CustomTextView()
		{
			InitElements();
			InitView();
			TranslatesAutoresizingMaskIntoConstraints = false;
		}

		public bool IsEditing => TextView.Focused;

		public ICommand ClickCommand { get; set; }

		public string PlaceHolder
		{
			get { return LbPlaceHolder.Text; }
			set
			{
				LbPlaceHolder.Hidden = TextView.Text.Length > 0;
				LbPlaceHolder.Text = value;
			}
		}

		public bool IsError
		{
			get { return _isError; }
			set
			{
				_isError = value;
				if (value)
				{
					LbPlaceHolder.TextColor = ColorHelper.Red;
					TextView.ResignFirstResponder();
				}
				else
				{
					LbPlaceHolder.TextColor = ColorHelper.PlaceHolderGray;
				}
			}
		}

		private void InitView()
		{
			Add(TextView);
			Add(LbPlaceHolder);
			AddConstraints(new[]
			{
				NSLayoutConstraint.Create(LbPlaceHolder, NSLayoutAttribute.Top, NSLayoutRelation.Equal, this,
					NSLayoutAttribute.Top, 1, 5),
				NSLayoutConstraint.Create(LbPlaceHolder, NSLayoutAttribute.Left, NSLayoutRelation.Equal, this,
					NSLayoutAttribute.Left, 1, 5),
				NSLayoutConstraint.Create(TextView, NSLayoutAttribute.Left, NSLayoutRelation.Equal, this,
					NSLayoutAttribute.Left, 1, 0),
				NSLayoutConstraint.Create(TextView, NSLayoutAttribute.Right, NSLayoutRelation.Equal, this,
					NSLayoutAttribute.Right, 1, 0),
				NSLayoutConstraint.Create(TextView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, this,
					NSLayoutAttribute.Top, 1, 0),
				NSLayoutConstraint.Create(TextView, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, this,
					NSLayoutAttribute.Bottom, 1, 0)
			});
		}

		private void InitElements()
		{
			TextView = new UITextView
			{
				TranslatesAutoresizingMaskIntoConstraints = false,
				Editable = true,
				AutocorrectionType = UITextAutocorrectionType.No,
				Font = UIFont.FromName("Roboto-Regular", DimensionHelper.SmallTextSize),
				ShowsHorizontalScrollIndicator = false
			};

			TextView.ShouldChangeText += OnTextViewShouldChangeText;
			TextView.ShouldBeginEditing += OnTextViewShouldBeginEditing;

			LbPlaceHolder = UIHelper.CreateLabel(ColorHelper.LightGray, DimensionHelper.SmallTextSize);
			LbPlaceHolder.BackgroundColor = UIColor.Clear;
			LbPlaceHolder.UserInteractionEnabled = false;
		}

		private bool OnTextViewShouldBeginEditing(UITextView textview)
		{
			ClickCommand?.Execute(null);
			return true;
		}

		private bool OnTextViewShouldChangeText(UITextView textview, NSRange range, string text)
		{
			var adder = 1;
			if (text.Length == 0)
				adder = -1;
			LbPlaceHolder.Hidden = TextView.Text.Length + adder > 0;
			return true;
		}
	}
}