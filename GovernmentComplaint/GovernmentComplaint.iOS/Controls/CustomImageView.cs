using System;
using AddressBookUI;
using Foundation;
using UIKit;
using SDWebImage;
namespace GovernmentComplaint.iOS.Controls
{
    public class CustomImageView : UIImageView
    {
        public EventHandler ImageLoaded;

        public CustomImageView()
        {
            TranslatesAutoresizingMaskIntoConstraints = false;
        }

        public string ImageUrl
        {
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                   this.SetImage(new NSUrl(value), UIImage.FromBundle("Images/default_complaint_image"));
                }
                else
                {
                    Image = UIImage.FromBundle("Images/default_complaint_image");
                }
            }
        }

        public string ZoomedImageUrl
        {
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    this.SetImage(new NSUrl(value), CompletedBlock());
                }
                else
                {
                    Image = UIImage.FromBundle("");
                }
            }
        }

        private SDWebImageCompletionHandler CompletedBlock()
        {
            return (image, error, cacheType, imageUrl) =>
            {
                ImageLoaded?.Invoke(this, null);
            };
        }
    }
}