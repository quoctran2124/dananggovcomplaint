﻿using System;
using UIKit;

namespace GovernmentComplaint.iOS.Controls
{
	public class DisappearableVerticalUiView : UIView
	{
		private nfloat _height;
		public nfloat HeightWhenHidden { get; set; }
		private NSLayoutConstraint _heightConstrainst;

		public NSLayoutConstraint HeightConstrainst
        {
			get { return _heightConstrainst; }
			set
			{
			    _heightConstrainst = value;
				_height = _heightConstrainst.Constant;
				AddConstraint(value);
			}
		}

		public override bool Hidden
		{
			get { return base.Hidden; }
			set
			{
				base.Hidden = value;
				if (_heightConstrainst != null)
				    _heightConstrainst.Constant = Hidden ? HeightWhenHidden : _height;
			}
		}
	}
}