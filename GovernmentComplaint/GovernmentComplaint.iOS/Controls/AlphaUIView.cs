using System;
using Foundation;
using System.Threading.Tasks;
using System.Windows.Input;
using UIKit;

namespace GovernmentComplaint.iOS.Controls
{
    public class AlphaUIView : UIView
    {
        public ICommand ClickCommand { get; set; }
		public bool NeedToHideEffect { get; set; }

        public event EventHandler Clicked;


        private float _clickedAlpha;
        public AlphaUIView(float clickedAlpha = 0.5f)
        {
            _clickedAlpha = clickedAlpha;
            Initialize();
        }

        private void Initialize()
        {
            UserInteractionEnabled = true;
            
            AddGestureRecognizer(new UITapGestureRecognizer(OnClicked));
        }

        public override void TouchesBegan(NSSet touches, UIEvent evt)
        {
			if (!NeedToHideEffect)
			{
				Alpha = _clickedAlpha;
			}

            base.TouchesBegan(touches, evt);
        }

        public override void TouchesCancelled(NSSet touches, UIEvent evt)
        {
            base.TouchesCancelled(touches, evt);

			if (!NeedToHideEffect)
			{
				Alpha = 1f;
			}
        }

        public override void TouchesEnded(NSSet touches, UIEvent evt)
        {
            base.TouchesEnded(touches, evt);

			if (!NeedToHideEffect)
			{
				Alpha = 1f;
			}
		}

        private void OnClicked(UITapGestureRecognizer obj)
        {
			if (!NeedToHideEffect)
	        {
				EffectAlpha();
			}

            ClickCommand?.Execute(null);

            //Clicked(this, EventArgs.Empty);

            Clicked?.Invoke(this, EventArgs.Empty);
        }

        private async void EffectAlpha()
        {
            Alpha = _clickedAlpha;
            await Task.Delay(200);
            Alpha = 1f;
        }
    }
}