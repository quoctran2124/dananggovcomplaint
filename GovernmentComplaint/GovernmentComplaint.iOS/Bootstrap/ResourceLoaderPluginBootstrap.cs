using MvvmCross.Platform.Plugins;

namespace GovernmentComplaint.iOS.Bootstrap
{
    public class ResourceLoaderPluginBootstrap
        : MvxLoaderPluginBootstrapAction<MvvmCross.Plugins.ResourceLoader.PluginLoader, MvvmCross.Plugins.ResourceLoader.iOS.Plugin>
    {
    }
}