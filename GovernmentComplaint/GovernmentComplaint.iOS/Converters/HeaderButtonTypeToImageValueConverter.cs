using System;
using System.Globalization;
using GovernmentComplaint.Core.Enums;
using GovernmentComplaint.iOS.Helpers;
using MvvmCross.Platform.Converters;
using UIKit;

namespace GovernmentComplaint.iOS.Converters
{
	public class HeaderButtonTypeToImageValueConverter : MvxValueConverter<HeaderButtonType, UIImage>
	{
		protected override UIImage Convert(HeaderButtonType value, Type targetType, object parameter, CultureInfo culture)
		{
			switch (value)
			{
				case HeaderButtonType.None:
					return null;
				case HeaderButtonType.Back:
					return UIImage.FromFile(ImageHelper.Back);
				case HeaderButtonType.Search:
					return UIImage.FromFile(ImageHelper.Search);
				case HeaderButtonType.SearchActive:
					return UIImage.FromFile(ImageHelper.SearchActive);
				case HeaderButtonType.Menu:
					return UIImage.FromFile(ImageHelper.Menu);
				case HeaderButtonType.MenuActive:
					return UIImage.FromFile(ImageHelper.MenuActive);
                case HeaderButtonType.Logo:
                    return UIImage.FromFile(ImageHelper.LogoTitle);
                default:
					throw new Exception("Header button type is not supported");
			}
		}
	}
}