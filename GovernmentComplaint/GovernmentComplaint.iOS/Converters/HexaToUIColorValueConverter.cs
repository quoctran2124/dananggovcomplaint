﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using GovernmentComplaint.iOS.Helpers;
using MvvmCross.Platform.Converters;
using UIKit;

namespace GovernmentComplaint.iOS.Converters
{
    public class HexaToUIColorValueConverter : MvxValueConverter<string, UIColor>
    {
        protected override UIColor Convert(string value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!String.IsNullOrEmpty(value))
            {
                var color = value.Replace("#", string.Empty);
                return ColorHelper.ToUIColor(color);
            }
            else
            {
                return ColorHelper.Theme3rdBlue;
            }
        }
    }
}
