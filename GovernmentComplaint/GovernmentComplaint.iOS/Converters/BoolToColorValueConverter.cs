using System;
using MvvmCross.Platform.Converters;
using UIKit;
using System.Globalization;
using GovernmentComplaint.iOS.Helpers;

namespace GovernmentComplaint.iOS.Converters
{
    public class BoolToColorValueConverter : MvxValueConverter<bool, UIColor>
    {
        protected override UIColor Convert(bool value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value)
            {
                return ColorHelper.DarkBlue;
            }
            else { return ColorHelper.LightGray; }
        }
    }
}