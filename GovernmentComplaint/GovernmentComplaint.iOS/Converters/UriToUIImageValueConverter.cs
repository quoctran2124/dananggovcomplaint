using System;
using System.Globalization;
using Foundation;
using MvvmCross.Platform.Converters;
using UIKit;

namespace GovernmentComplaint.iOS.Converters
{
    public class UriToUIImageValueConverter : MvxValueConverter<string, UIImage>
    {
        protected override UIImage Convert(string value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                using (var url = new NSUrl(value))
                using (var data = NSData.FromUrl(url))
                {
                        return UIImage.LoadFromData(data);
                }
            }
            catch (Exception)
            {
                return UIImage.FromFile("Images/default_complaint_image");
            }
            
        }
    }
}