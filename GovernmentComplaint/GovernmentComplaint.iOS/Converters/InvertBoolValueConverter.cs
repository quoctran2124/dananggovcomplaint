﻿using System;
using System.Globalization;
using MvvmCross.Platform.Converters;

namespace GovernmentComplaint.iOS.Converters
{
    public class InvertBoolValueConverter : MvxValueConverter<bool, bool>
    {
        protected override bool Convert(bool value, Type targetType, object parameter, CultureInfo culture)
        {
            return !value;
        }
    }
}