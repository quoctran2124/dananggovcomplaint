using System;
using System.Globalization;
using CoreGraphics;
using GovernmentComplaint.Core.Enums;
using GovernmentComplaint.iOS.Helpers;
using MvvmCross.Platform.Converters;

namespace GovernmentComplaint.iOS.Converters
{
    public class BackgroundTypeToCGColorValueConverter : MvxValueConverter<BackgroundType, CGColor>
    {
        protected override CGColor Convert(BackgroundType value, Type targetType, object parameter, CultureInfo culture)
        {
            switch (value)
            {
                case BackgroundType.Error:
                    return ColorHelper.Red.CGColor;
                case BackgroundType.Normal:
                    return ColorHelper.BorderGray.CGColor;
                default:
                    return null;
            }
        }
    }
}