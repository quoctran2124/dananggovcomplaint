using System;
using System.Globalization;
using GovernmentComplaint.Core.Enums;
using GovernmentComplaint.iOS.Helpers;
using MvvmCross.Platform.Converters;
using UIKit;

namespace GovernmentComplaint.iOS.Converters
{
	public class SidebarMenuStatusLabelToColorValueConverter : MvxValueConverter<LabelStatus, UIColor>
	{
		protected override UIColor Convert(LabelStatus value, Type targetType, object parameter, CultureInfo culture)
		{
			switch (value)
			{
				case LabelStatus.Special:
					return ColorHelper.Orange;
				case LabelStatus.Active:
					return ColorHelper.White;
				case LabelStatus.Inactive:
					return ColorHelper.Black;
                case LabelStatus.Count:
                    return ColorHelper.Red;
                default:
					return ColorHelper.Black;
			}
		}
	}
}