using System;
using System.Globalization;
using GovernmentComplaint.Core.Enums;
using GovernmentComplaint.iOS.Helpers;
using MvvmCross.Platform.Converters;
using UIKit;

namespace GovernmentComplaint.iOS.Converters
{
	public class SidebarMenuStatusToColorValueConverter : MvxValueConverter<ItemStatus, UIColor>
	{
		protected override UIColor Convert(ItemStatus value, Type targetType, object parameter, CultureInfo culture)
		{
			switch (value)
			{
				case ItemStatus.Active:
					return ColorHelper.Orange;
				case ItemStatus.Inactive:
					return ColorHelper.NeutralGray;
				default:
					return ColorHelper.NeutralGray;
			}
		}
	}
}