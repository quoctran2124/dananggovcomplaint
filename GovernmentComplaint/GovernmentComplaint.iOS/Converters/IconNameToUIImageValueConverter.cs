using System;
using MvvmCross.Platform.Converters;
using UIKit;
using System.Globalization;

namespace GovernmentComplaint.iOS.Converters
{
    public class IconNameToUIImageValueConverter : MvxValueConverter<string, UIImage>
    {
        protected override UIImage Convert(string value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
            {
                return null;
            }

            return UIImage.FromFile(value);
        }
    }
}