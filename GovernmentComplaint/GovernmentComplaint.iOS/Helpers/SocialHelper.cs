using Facebook.LoginKit;
using Google.SignIn;
using GovernmentComplaint.Core.Helpers;

namespace GovernmentComplaint.iOS.Helpers
{
    public class SocialHelper : ISocialHelper
    {
        public void LogoutFacebook()
        {
            new LoginManager().LogOut();
        }

        public void LogoutGoogle()
        {
            SignIn.SharedInstance.SignOutUser();
        }
    }
}