using System;
using System.Security.Cryptography.X509Certificates;

namespace GovernmentComplaint.iOS.Helpers
{
	public static class DimensionHelper
	{
		// Text size
		public static nfloat NanoTextSize { get; private set; }
		public static nfloat TinyTextSize { get; private set; }
		public static nfloat SmallestTextSize { get; private set; }
		public static nfloat SmallTextSize { get; private set; }
		public static nfloat MediumTextSize { get; private set; }
		public static nfloat BigTextSize { get; private set; }
		public static nfloat BiggerTextSize { get; private set; }
		public static nfloat BiggestTextSize { get; private set; }
		public static nfloat LargeTextSize { get; private set; }
		public static nfloat MenuLineWidth { get; private set; }
		public static nfloat RoundCorner { get; private set; }
		public static nfloat CategoryLineHeight { get; private set; }
		public static nfloat DividerWeight { get; private set; }

	    public static nfloat LayoutMargin { get; set; }

        public static nfloat LayoutBigMargin { get; private set; }
		public static nfloat LayoutBiggerMargin { get; private set; }

		public static nfloat CategoryElementMargin { get; private set; }
		public static nfloat BottomBorderredContainerHeight { get; private set; }
		public static nfloat DropdownBarHeight { get; private set; }
		public static nfloat SeparateLine { get; private set; }
		public static nfloat SimpleListItemHeight { get; private set; }
		public static nfloat DropdownItemHeight { get; private set; }
		public static nfloat DropdownIconSize { get; private set; }
		public static nfloat DropdownPadding { get; private set; }
		//Notification
		public static nfloat NotificationTextWidth { get; private set; }
		public static nfloat NotificationTextHeight { get; private set; }
		public static nfloat NotificationHeight { get; private set; }
		public static nfloat NotificationImageSize { get; private set; }
		public static nfloat NotificationStatusSize { get; private set; }
		public static nfloat NotificationMagrinLeft { get; private set; }
		public static nfloat NotificationMagrinTop { get; private set; }
		public static nfloat NotificationContentMarginTop { get; private set; }
		public static nfloat NotificationStatusMagrinTop { get; private set; }
		public static nfloat NotificationStatusMagrinRight { get; private set; }
		public static nfloat NotificationTimeMagrinTop { get; private set; }
		public static nfloat NotificationTimeMagrinRight { get; private set; }
		public static nfloat NotificationNoDataLabelMarginTop { get; private set; }
		public static nfloat NotificationCountSize { get; private set; }
		public static nfloat NotificationCountMarginTop { get; private set; }
		public static nfloat NotificationCountMarginRight { get; private set; }

		//MainMenu
		public static nfloat MainMenuHeight { get; private set; }
		public static nfloat MainMenuButtonSize { get; private set; }
		public static nfloat TitleBarWidth { get; private set; }
		public static nfloat ImageViewPadding { get; private set; }
		public static nfloat NoDataLabelMarginTop { get; private set; }

		//Sidebar
		public static nfloat SidebarWidth { get; private set; }
		public static nfloat LoginButtonSideMargin { get; private set; }
		public static nfloat LoginButtonHeight { get; private set; }
		public static nfloat LoginButtonWidth { get; private set; }
		public static nfloat ChooseAuthUpperMargin { get; private set; }
		public static nfloat LoginRegionHeight { get; private set; }
		public static nfloat LoginButtonRightPadding { get; private set; }
		public static nfloat LoginButtonLeftPadding { get; private set; }
		public static nfloat SidebarItemWidth { get; private set; }
		public static nfloat SidebarItemHeight { get; private set; }
		public static nfloat SidebarItemUpperPadding { get; private set; }
		public static nfloat SidebarItemSidePadding { get; private set; }
		public static nfloat SidebarIconSize { get; private set; }
		public static nfloat BorderWidth { get; private set; }
		public static nfloat SearchBoxHeight { get; private set; }
		public static nfloat SearchBoxEditTextWidth { get; private set; }
		public static nfloat SearchBoxEditTextHeight { get; private set; }
		public static nfloat SearchBoxEditTextMargin { get; private set; }
		public static nfloat SearchButtonMargin { get; private set; }
		public static nfloat SearchButtonHeight { get; private set; }
		public static nfloat SearchButtonWidth { get; private set; }
		public static nfloat SearchButtonPadding { get; private set; }
		public static nfloat WelcomeTextMarginLeft { get; private set; }

		public static nfloat NComplaintItemHeight { get; private set; }
		public static nfloat NComplaintButtonWidth { get; private set; }
		public static nfloat NComplaintButtonHeight { get; private set; }
		public static nfloat NComplaintBigIconPadding { get; private set; }
		public static nfloat NComplaintBigIconSize { get; private set; }
		public static nfloat NComplaintBigIconMarginLeft { get; private set; }
		public static nfloat NComplaintSmallButtonWidth { get; private set; }
		public static nfloat NComplaintSmallButtonHeight { get; private set; }
		public static nfloat NComplaintSmallIconWidth { get; private set; }
		public static nfloat NComplaintCommonMargin { get; private set; }
		public static nfloat NComplaintSmallIconPadding { get; private set; }
		public static nfloat NComplaintTextPadding { get; private set; }
		public static nfloat NComplaintAttachTextPadding { get; private set; }
		public static nfloat NComplaintAttachedRegionHeight { get; private set; }
		public static nfloat NComplaintAttachedImageSize { get; private set; }
		public static nfloat VideoLinkLayoutHeight { get; private set; }
		public static nfloat VideoLinkLayoutIconPadding { get; private set; }
		public static nfloat LocationHeight { get; private set; }



		//Add personal information
		public static nfloat PersonalInfoCommonMargin { get; private set; }
		public static nfloat PersonalInfoTopMargin { get; private set; }
		public static nfloat PersonalInfoEditTextHeight { get; private set; }
		public static nfloat MessageMarginLeft { get; private set; }
		public static nfloat MessageMarginTop { get; private set; }
		//Floating button
		public static nfloat FloatingButtonSize { get; private set; }
		public static nfloat FloatingButtonPadding { get; private set; }
		public static nfloat FloatingButtonMargin { get; private set; }

		//Feedback
		public static nfloat FeedbackHeight { get; private set; }
		public static nfloat FeedbackImageHeight { get; private set; }
		public static nfloat FeedbackImageWidth { get; private set; }
		public static nfloat FeedbackImageMarginLeft { get; private set; }
		public static nfloat FeedbackImageMarginTop { get; private set; }
		public static nfloat FeedbackCommentImageSize { get; private set; }
		public static nfloat FeedbackLikeImageSize { get; private set; }
		public static nfloat FeedbackSolvedImageSize { get; private set; }
		public static nfloat FeedbackDepartmentTextHeight { get; private set; }
		public static nfloat FeedbackDepartmentTextMarginLeft { get; private set; }
		public static nfloat FeedbackDepartmentTextMarginBottom { get; private set; }

		//AboutView

		public static nfloat AboutNarrowMargin { get; private set; }
		public static nfloat FeedbackTitleMarginRight { get; private set; }
		public static nfloat AboutWideMargin { get; private set; }
		public static nfloat AboutWidestMargin { get; private set; }
		public static nfloat LogoWidePadding { get; private set; }
		public static nfloat LogoNarrowPadding { get; private set; }
		public static nfloat UpperViewHeight { get; private set; }
		public static nfloat AboutLargeMargin { get; private set; }
		public static nfloat LogoSubPadding { get; private set; }
		public static nfloat AboutMediumMargin { get; private set; }
		public static nfloat LogoWidth { get; private set; }
		public static nfloat LogoLayoutWidth { get; private set; }
		public static nfloat LogoLayoutHeight { get; private set; }
		public static nfloat LogoLayoutMargin { get; private set; }
		public static nfloat SecondIntroMarginTop { get; private set; }
		public static nfloat FirstIntroMarginTop { get; private set; }

		public static nfloat AboutMarginLR { get; private set; }
		public static nfloat AboutMarginTB { get; private set; }
		public static nfloat HotLineBannerHeight { get; private set; }
		public static nfloat AboutLogoSize { get; private set; }


		//Popup
		public static nfloat PopupPadding { get; private set; }
		public static nfloat PopupMargin { get; private set; }
		public static nfloat PopupHeight { get; private set; }
		public static nfloat BottomBarHeight { get; set; }
		public static nfloat PopupProgressSize { get; set; }
		public static nfloat PopupLogoutMargin { get; set; }
		public static nfloat PopupButtonMargin { get; set; }
		public static nfloat PopupCancelMargin { get; set; }
		public static nfloat PopupCancelButtonMargin { get; set; }
		public static nfloat PopupEmailMargin { get; set; }
		public static nfloat PopupButtonHeight { get; set; }
		public static nfloat PopupButtonWidth { get; set; }
		public static nfloat DepartmentTableViewHeight { get; set; }
		public static nfloat ToastCorner { get; set; }

		public static nfloat FeedbackTitleMarginLeft { get; private set; }
		public static nfloat FeedbackTitleMarginTop { get; private set; }
		public static nfloat FeedbackTimeMarginTop { get; private set; }
		public static nfloat FeedbackRatingMarginTop { get; private set; }
		public static nfloat FeedbackRatingMarginLeft { get; private set; }
		public static nfloat FeedbackCommentImageMarginLeft { get; private set; }
		public static nfloat FeedbackCommentImageMarginTop { get; private set; }
		public static nfloat FeedbackCommentMarginLeft { get; private set; }
		public static nfloat FeedbackCommentMarginTop { get; private set; }
		public static nfloat FeedbackLikeImageMarginLeft { get; private set; }
		public static nfloat FeedbackLikeImageMarginTop { get; private set; }
		public static nfloat FeedbackLikeMarginLeft { get; private set; }
		public static nfloat FeedbackLikeMarginTop { get; private set; }
		public static nfloat FeedbackSolvedImageMarginTop { get; private set; }
		public static nfloat FeedbackSolvedImageMarginRight { get; private set; }
		public static nfloat FeedbackTitleTextSize { get; private set; }

		//Complaint Detail
		public static nfloat ComplaintDetailLocationWidth { get; private set; }
		public static nfloat OutterCommentSectionHeight { get; private set; }
		public static nfloat DetailImageViewHeight { get; private set; }
		public static nfloat ComplaintDetailMargin { get; private set; }
		public static nfloat ComplaintDetailMarginTop { get; private set; }
		public static nfloat MediumContentMargin { get; private set; }
		public static nfloat SmallContentMargin { get; private set; }
		public static nfloat BigContentMargin { get; private set; }
		public static nfloat DepartmentBar { get; private set; }
		public static nfloat ContentPadding { get; private set; }
		public static nfloat ContentMarginTop { get; private set; }
		public static nfloat AttachedSize { get; private set; }
		public static nfloat SocialInterractHeight { get; private set; }
		public static nfloat ThickDivider { get; private set; }
		public static nfloat ThinDivider { get; private set; }
		public static nfloat CommentFrameWidth { get; private set; }
		public static nfloat CommonHeight { get; private set; }
		public static nfloat CommonWidth { get; private set; }
		public static nfloat MassiveContentMargin { get; private set; }
		public static nfloat CommentItemHeight { get; private set; }
		public static nfloat LargeContentMargin { get; private set; }
		public static nfloat LocationTextWidth { get; private set; }
		public static nfloat AttachedImageSize { get; private set; }
		public static nfloat RatingViewMarginBottom { get; private set; }
		public static nfloat RatingViewMarginRight { get; private set; }
		public static nfloat ReplyHolderMarginBottom { get; private set; }
		public static nfloat FileNameMarginRight { get; private set; }
		public static nfloat RatingbarWidth { get; private set; }
		public static nfloat RatingbarHeight { get; private set; }
		public static nfloat CommentSmallPadding { get; private set; }
		public static nfloat CommentBigPadding { get; private set; }

		//Authenticated region
		public static nfloat GoogleLoginButtonHeight { get; private set; }
		public static nfloat GoogleLoginButtonWidth { get; private set; }
		public static nfloat UserPictureSize { get; private set; }
		public static nfloat InteractionTextTopMargin { get; private set; }
		public static nfloat InteractionTextSpace { get; private set; }
		public static nfloat UserPictureMargin { get; private set; }
		public static nfloat InteractionImageMarginTop { get; private set; }
		public static nfloat CommentImageMargin { get; private set; }
		public static nfloat InteractionImageSize { get; private set; }
		public static nfloat PointSize { get; private set; }

	    public static nfloat MediumMenuIconHeight { get; private set; }
	    public static nfloat UnSatisfiedContainerHeight { get; private set; }
	    public static nfloat DropdownHeight { get; private set; }
	    public static nfloat TitleHeight { get; private set; }
	    public static nfloat CategoryItemHeight { get; private set; }
	    public static nfloat RightContentWidth { get; private set; }
	    public static nfloat FeedbackContentHeight { get; private set; }
	    public static nfloat TimeListWidth { get; private set; }

        public static float RATE;

		static DimensionHelper()
		{
			ResolutionHelper.InitStaticVariable();
			InitStaticVariable();
		}

		public static void InitStaticVariable()
		{
			TinyTextSize = 9 * RATE;
			SmallestTextSize = 11 * RATE;
			SmallTextSize = 13 * RATE;
			MediumTextSize = 15 * RATE;
			BigTextSize = 17 * RATE;
			BiggerTextSize = 25 * RATE;
			BiggestTextSize = 30 * RATE;
			LargeTextSize = 32 * RATE;

            // General
		    LayoutMargin = 10 * RATE;
		    LayoutBigMargin = 15 * RATE;
		    LayoutBiggerMargin = 10 * RATE;


            //layout dimension
            CategoryElementMargin = 15 * RATE;

			//element dimensions
			MenuLineWidth = 3 * RATE;
			RoundCorner = 4 * RATE;
			CategoryLineHeight = 104 * RATE;
			MenuLineWidth = 3 * RATE;
			BottomBorderredContainerHeight = 60 * RATE;
			DropdownBarHeight = 45 * RATE;
			SeparateLine = 0.8f * RATE;
			SimpleListItemHeight = 45 * RATE;
			DropdownItemHeight = 60 * RATE;
			DropdownIconSize = 45 * RATE;
			DropdownPadding = 10 * RATE;

			//New Complaint
			NComplaintItemHeight = 50 * RATE;
			NComplaintBigIconPadding = 10 * RATE;
			NComplaintBigIconSize = 45 * RATE;
			NComplaintBigIconMarginLeft = 50 * RATE;
			NComplaintSmallButtonWidth = 125 * RATE;
			NComplaintSmallButtonHeight = 40 * RATE;
			NComplaintCommonMargin = 12 * RATE;
			NComplaintSmallIconPadding = 12 * RATE;
			NComplaintSmallIconWidth = 60 * RATE;
			NComplaintTextPadding = 5 * RATE;
			NComplaintAttachTextPadding = 17 * RATE;
			NComplaintAttachedRegionHeight = 120 * RATE;
			NComplaintAttachedImageSize = 80*RATE;
			VideoLinkLayoutHeight = 30 * RATE;
			VideoLinkLayoutIconPadding = 5 * RATE;
			LocationHeight = 45 * RATE;


			//notification dimensions
			NotificationTextWidth = 9 * RATE;
			NotificationTextHeight = 25 * RATE;
			NotificationHeight = 80 * RATE;
			NotificationImageSize = 35 * RATE;
			NotificationStatusSize = 13 * RATE;
			NotificationMagrinLeft = 8 * RATE;
			NotificationMagrinTop = 25 * RATE; ;
			NotificationContentMarginTop = 30 * RATE;
			NotificationStatusMagrinTop = 6 * RATE;
			NotificationStatusMagrinRight = 15 * RATE;
			NotificationTimeMagrinTop = 5 * RATE;
			NotificationTimeMagrinRight = 10 * RATE;
			NotificationNoDataLabelMarginTop = 10 * RATE;
			NotificationCountSize = 17 * RATE;
			NotificationCountMarginTop = 5 * RATE;
			NotificationCountMarginRight = 8 *RATE;
			//MainMenu dimensions
			MainMenuHeight = 50 * RATE;
			MainMenuButtonSize = 50 * RATE;
			TitleBarWidth = 275 * RATE;
			ImageViewPadding = 12 * RATE;
			DividerWeight = 1*RATE;
			NoDataLabelMarginTop = 70 * RATE;

			//Sidebar dimensions
			SidebarWidth = 277 * RATE;
			LoginButtonSideMargin = 35 * RATE;
			LoginButtonHeight = 42 * RATE;
			LoginButtonWidth = 227 * RATE;
			GoogleLoginButtonHeight = 49*RATE;
			GoogleLoginButtonWidth = 236 * RATE;
			ChooseAuthUpperMargin = 10 * RATE;
			LoginRegionHeight = 160 * RATE;
			LoginButtonRightPadding = 40 * RATE;
			LoginButtonLeftPadding = 10 * RATE;
			SidebarItemWidth = 226 * RATE;
			SidebarItemHeight = 50 * RATE;
			SidebarItemUpperPadding = 10 * RATE;
			SidebarItemSidePadding = 15 * RATE;
			SidebarIconSize = 50 * RATE;
			BorderWidth = 1 * RATE;
			WelcomeTextMarginLeft = 45 * RATE;
			//Searchbox dimensions
			SearchBoxHeight = 155 * RATE;
			SearchBoxEditTextWidth = 300 * RATE;
			SearchBoxEditTextHeight = 35 * RATE;
			SearchBoxEditTextMargin = 10 * RATE;
			SearchButtonMargin = 13 * RATE;
			SearchButtonHeight = 40 * RATE;
			SearchButtonWidth = 80 * RATE;
			SearchButtonPadding = 5 * RATE;

			//Personal Information
			PersonalInfoCommonMargin = 12 * RATE;
			PersonalInfoTopMargin = 23 * RATE;
			PersonalInfoEditTextHeight = 45 * RATE;
			MessageMarginLeft = 7 * RATE;
			MessageMarginTop = 5 * RATE;
			NComplaintButtonWidth = 160 * RATE;
			NComplaintButtonHeight = 45 * RATE;

			//Floating button
			FloatingButtonSize = 60 * RATE;
			FloatingButtonPadding = 15 * RATE;
			FloatingButtonMargin = 10 * RATE;

			//Feedback
			NanoTextSize = 8 * RATE;
			FeedbackHeight = 140 * RATE;
			FeedbackImageHeight = 110 * RATE;
			FeedbackImageWidth = 145 * RATE;
			FeedbackImageMarginLeft = 10 * RATE;
			FeedbackImageMarginTop = 8 * RATE;
			FeedbackDepartmentTextHeight = 18 * RATE;
			FeedbackDepartmentTextMarginLeft = 10 * RATE;
			FeedbackDepartmentTextMarginBottom = 8 * RATE;
			FeedbackCommentImageSize = 14 * RATE;
			FeedbackLikeImageSize = 15 * RATE;
			FeedbackSolvedImageSize = 15 * RATE;
			FeedbackTitleMarginRight = 6 * RATE;
			FeedbackTitleMarginLeft = 10 * RATE;
			FeedbackTitleMarginTop = 6 * RATE;
			FeedbackTimeMarginTop = 4 * RATE;
			FeedbackCommentImageMarginLeft = 12 * RATE;
			FeedbackCommentImageMarginTop = 4 * RATE;
			FeedbackCommentMarginLeft = 3 * RATE;
			FeedbackCommentMarginTop = 2 * RATE;
			FeedbackRatingMarginTop = 4 * RATE;
			FeedbackRatingMarginLeft = 10 * RATE;
			FeedbackLikeImageMarginLeft = 25 * RATE;
			FeedbackLikeImageMarginTop = 3 * RATE;
			FeedbackLikeMarginLeft = 3 * RATE;
			FeedbackLikeMarginTop = 2 * RATE;
			FeedbackSolvedImageMarginTop = 12 * RATE;
			FeedbackSolvedImageMarginRight = 5 * RATE;
			FeedbackTitleTextSize = 15 * RATE;

			//AboutView
			AboutNarrowMargin = 6 * RATE;
			AboutWideMargin = 12 * RATE;
			AboutWidestMargin = 60 * RATE;
			LogoWidePadding = 50 * RATE;
			LogoNarrowPadding = 10 * RATE;
			UpperViewHeight = 300 * RATE;
			AboutLargeMargin = 55 * RATE;
			AboutMediumMargin = 20 * RATE;
			LogoSubPadding = 40 * RATE;
			LogoWidth = 100 * RATE;
			LogoLayoutWidth = 220 * RATE;
			LogoLayoutHeight = 50 * RATE;
			LogoLayoutMargin = 20 * RATE;
			SecondIntroMarginTop = 5*RATE;
			FirstIntroMarginTop = -25 * RATE;

			AboutMarginLR = 35 * RATE;
			AboutMarginTB = 25 * RATE;
			HotLineBannerHeight = 100 * RATE;
			AboutLogoSize = 55 * RATE;
			//Popup
			PopupPadding = 12 * RATE;
			PopupMargin = 12 * RATE;
			PopupHeight = 190 * RATE;
			BottomBarHeight =55*RATE;
			PopupProgressSize = 32 * RATE;
			PopupLogoutMargin = 30*RATE;
			PopupEmailMargin = 20 * RATE;
			PopupButtonMargin = 10*RATE;
			PopupCancelMargin = 40*RATE;
			PopupButtonHeight = 50*RATE;
			PopupButtonWidth = 120*RATE;
			DepartmentTableViewHeight = 270*RATE;
			ToastCorner = 5* RATE;
			//Complaint Detail
			OutterCommentSectionHeight = 280*RATE;
			ComplaintDetailLocationWidth = 230 * RATE;
			DetailImageViewHeight = 200 * RATE;
			ComplaintDetailMargin = 10 * RATE;
			ComplaintDetailMarginTop = 10 * RATE;
			MediumContentMargin = 10 * RATE;
			SmallContentMargin = 2*RATE;
			BigContentMargin = 25*RATE;
			LargeContentMargin = 30*RATE;
			MassiveContentMargin = 40*RATE;
			DepartmentBar = 40 * RATE;
			ContentPadding = 10 * RATE;
			ContentMarginTop = 20 * RATE;
			AttachedSize = 8 * RATE;
			SocialInterractHeight = 50 * RATE;
			ThickDivider = 10 * RATE;
			ThinDivider = 1 * RATE;
			CommentFrameWidth = 250 * RATE;
			CommonHeight = 30 * RATE;
			CommonWidth = 300 * RATE;
			CommentItemHeight = 120*RATE;
			PointSize = 8 * RATE;
			LocationTextWidth = 223*RATE;
			AttachedImageSize = 80*RATE;
			RatingViewMarginBottom = -6*RATE;
			RatingViewMarginRight = 5 * RATE;
			ReplyHolderMarginBottom = 10 * RATE;
			FileNameMarginRight = 5 * RATE;
			RatingbarHeight = 60*RATE;
			RatingbarWidth = 180*RATE;
			CommentSmallPadding = 5*RATE;
			CommentBigPadding = 20*RATE;
			//Authenticated region
			UserPictureSize = 80*RATE;
			InteractionImageSize = 15*RATE;
			InteractionTextTopMargin = 30*RATE;
			InteractionTextSpace = 4*RATE;
			UserPictureMargin = 18*RATE;
			InteractionImageMarginTop = 32*RATE;
			CommentImageMargin = 10*RATE;

		    MediumMenuIconHeight = 20 * RATE;
		    UnSatisfiedContainerHeight = 120 * RATE;
		    DropdownHeight = 20 * RATE;
		    TitleHeight = 40 * RATE;

		    CategoryItemHeight = 60 * RATE;
		    RightContentWidth = 80 * RATE;
		    FeedbackContentHeight = 80 * RATE;
		    TimeListWidth = 140 * RATE;
		}
	}
}