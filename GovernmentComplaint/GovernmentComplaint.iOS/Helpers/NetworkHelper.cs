using GovernmentComplaint.Core.Helpers;
using System.Net;
using SystemConfiguration;
using System;
using CoreFoundation;

namespace GovernmentComplaint.iOS.Helpers
{
	public class NetworkHelper : INetworkHelper
	{
		private NetworkReachability _defaultRouteReachability;

		public EventHandler NetworkConnected { get; set; }

		public NetworkHelper()
		{
			_defaultRouteReachability = new NetworkReachability(new IPAddress(0));
			_defaultRouteReachability.SetNotification(OnNetworkStatusChanged);
			_defaultRouteReachability.Schedule(CFRunLoop.Current, CFRunLoop.ModeDefault);
		}

		private void OnNetworkStatusChanged(NetworkReachabilityFlags flags)
		{
			if (IsConnected)
			{
				NetworkConnected?.Invoke(this, EventArgs.Empty);
			}
		}

		public bool IsConnected => IsNetworkAvailable();

		private bool IsNetworkAvailable()
		{
			NetworkReachabilityFlags flags;

			if (!_defaultRouteReachability.TryGetFlags(out flags))
			{
				return false;
			}

			return IsReachableWithoutRequiringConnection(flags);
		}

		private bool IsReachableWithoutRequiringConnection(NetworkReachabilityFlags flags)
		{
			// Is it reachable with the current network configuration?
			bool isReachable = (flags & NetworkReachabilityFlags.Reachable) != 0;

			// Do we need a connection to reach it?
			bool noConnectionRequired = (flags & NetworkReachabilityFlags.ConnectionRequired) == 0;

			// Since the network stack will automatically try to get the WAN up,
			// probe that
			if ((flags & NetworkReachabilityFlags.IsWWAN) != 0)
			{
				noConnectionRequired = true;
			}

			return isReachable && noConnectionRequired;
		}
	}
}