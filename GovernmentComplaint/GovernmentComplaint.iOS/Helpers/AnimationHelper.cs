using System;
using CoreGraphics;
using GovernmentComplaint.iOS.Controls;
using UIKit;

namespace GovernmentComplaint.iOS.Helpers
{
    public static class AnimationHelper
    {
        public static void Fade(UIView view, bool isIn, double duration = 0.15, Action onFinished = null)
        {
            var minAlpha = (nfloat)0.0f;
            var maxAlpha = (nfloat)1.0f;

            view.Alpha = isIn ? minAlpha : maxAlpha;
            view.Transform = CGAffineTransform.MakeIdentity();
            UIView.Animate(duration, 0, UIViewAnimationOptions.CurveEaseInOut,
                () =>
                {
                    view.Alpha = isIn ? maxAlpha : minAlpha;
                },
                onFinished);
        }

        public static void SlideMenu(UIView view, bool isIn, float duration, Action onFinished = null)
        {
            nfloat offset;

            if (view is DisappearableUIView)
            {
                offset = (view as DisappearableUIView).WidthConstraint.Constant / 2;
            }
            else
            {
                offset = view.Bounds.Width / 2;
            }

            var center = view.Center;

            center.X = isIn ? offset : -offset;
            view.Center = center;

            UIView.Animate(duration, 0, UIViewAnimationOptions.CurveEaseInOut,
                () =>
                {
                    center.X = isIn ? -offset : offset;
                    view.Center = center;
                },
                onFinished);
        }

        public static void Scale(this UIView view, bool isIn, nfloat minScale, nfloat maxScale, double duration = 0.3f, Action onFinished = null)
        {
            var minTransform = CGAffineTransform.MakeScale(minScale, minScale);
            var maxTransform = CGAffineTransform.MakeScale(maxScale, maxScale);

            view.Transform = isIn ? minTransform : maxTransform;

            UIView.Animate(duration, 0, UIViewAnimationOptions.CurveEaseInOut,
                () =>
                {
                    view.Transform = isIn ? maxTransform : minTransform;
                },
                onFinished
            );
        }

        public static void SlideVerticaly(this UIView view, nfloat toX, nfloat toY, double duration = 0.3, Action onFinished = null)
        {
            UIView.Animate(duration, 0.05f, UIViewAnimationOptions.CurveEaseInOut,
                () =>
                {
                    var tempFrame = view.Frame;
                    tempFrame.X += toX;
                    tempFrame.Y += toY;
                    view.Frame = tempFrame;
                    view.Alpha = 0;
                },
                onFinished
            );
        }
    }
}