using Foundation;
using GovernmentComplaint.Core.Helpers;

namespace GovernmentComplaint.iOS.Helpers
{
	public class VersionHelper : IVersionHelper
	{
		public string GetVersion()
		{
			return NSBundle.MainBundle.InfoDictionary["CFBundleShortVersionString"].ToString();
		}
	}
}