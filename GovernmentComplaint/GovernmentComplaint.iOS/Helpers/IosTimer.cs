﻿using System;
using System.Timers;
using GovernmentComplaint.Core.Helpers;

namespace GovernmentComplaint.iOS.Helpers
{
    public class IosTimer : ITimer
    {
        private Timer _timer;
        private Action _action;

        public void Init(Action action, int interval)
        {
            _action = action;
            _timer = new Timer(interval);
            _timer.Elapsed += OnTimerElapsed;
        }

        public void Start()
        {
            _timer.Enabled = true;
        }

        public void Stop()
        {
            _timer.Enabled = false;
        }

        private void OnTimerElapsed(object sender, ElapsedEventArgs e)
        {
            _action();
        }
    }
}