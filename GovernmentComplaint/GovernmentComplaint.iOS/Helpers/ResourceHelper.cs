﻿using GovernmentComplaint.Core.Helpers;
using System.IO;

namespace GovernmentComplaint.iOS.Helpers
{
	public class ResourceHelper : IResourceHelper
	{
		public string IconPath(string icon)
		{
			return Path.Combine(ImageHelper.IMAGE_PATH, icon + ".png");
		}

	}
}