using GovernmentComplaint.Core.Helpers;
using UIKit;

namespace GovernmentComplaint.iOS.Helpers
{
    public class BadgeHelper : IBadgeHelper
    {
        public void SetBadge(int badgeNumber)
        {
            UIApplication.SharedApplication.BeginInvokeOnMainThread(() => UIApplication.SharedApplication.ApplicationIconBadgeNumber = badgeNumber);
        }

        public void ClearBadge()
        {
            UIApplication.SharedApplication.BeginInvokeOnMainThread(() => UIApplication.SharedApplication.ApplicationIconBadgeNumber = 0);
        }
    }
}