﻿using System;
using UIKit;

namespace GovernmentComplaint.iOS.Helpers
{
    public static class ResolutionHelper
    {
        private const int BaseDeviceSize = 320;
        public static float BaseRate = 7f / 8;

        public static nfloat Width { get; private set; }
        public static nfloat Height { get; private set; }
        public static nfloat StatusHeight { get; private set; }
        public static bool IsTablet { get; private set; }
        public static bool IsPortrait { get; private set; }

        public static void InitStaticVariable()
        {
            IsTablet = UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad;
            StatusHeight = UIApplication.SharedApplication.StatusBarHidden ? 0 : UIApplication.SharedApplication.StatusBarFrame.Height;

            RefreshStaticVariable();
        }

        public static void RefreshStaticVariable()
        {
            if (UIDevice.CurrentDevice.Orientation == UIDeviceOrientation.FaceUp || UIDevice.CurrentDevice.Orientation == UIDeviceOrientation.FaceDown)
            {
                return;
            }

            IsPortrait = UIDevice.CurrentDevice.Orientation == UIDeviceOrientation.Portrait
                || UIDevice.CurrentDevice.Orientation == UIDeviceOrientation.PortraitUpsideDown;

            Width = UIScreen.MainScreen.Bounds.Width;
            Height = UIScreen.MainScreen.Bounds.Height - StatusHeight;

            DimensionHelper.RATE = (float)(Math.Min(Width, Height) / BaseDeviceSize) * BaseRate;
        }
    }
}