using System;
using UIKit;

namespace GovernmentComplaint.iOS.Helpers
{
	public static class ColorHelper
	{
		public static UIColor Overlay => ToUIColor("88000000");
		public static UIColor GrayTransparent => ToUIColor("515151");

		public static UIColor Transparent => ToUIColor("00000000");
		public static UIColor ThemeTransparentBlue => ToUIColor("005083");

		public static UIColor Divider => ToUIColor("dadada");
		public static UIColor White => ToUIColor("ffffff");
		public static UIColor Black => ToUIColor("000000");
		public static UIColor LighterBlack => ToUIColor("3B3C3C");
		public static UIColor GrayBackground => ToUIColor("E6E6E6");
		public static UIColor GrayTextColor => ToUIColor("373737");

		public static UIColor LightestGray => ToUIColor("f5f5f5");
		public static UIColor LighterGray => ToUIColor("EEEEEE");
		public static UIColor LightGray => ToUIColor("9E9E9E");
		public static UIColor NeutralGray => ToUIColor("f1f2f1");
		public static UIColor DarkGray => ToUIColor("212121");
		public static UIColor DarkerGray => ToUIColor("E3E1E0");
		public static UIColor DarkestGray => ToUIColor("AEB0B0");
		public static UIColor Gray => ToUIColor("f3f3f3");
		public static UIColor PlaceHolderGray => ToUIColor("C7C7CD");
		public static UIColor DividerGray => ToUIColor("EBEBEB");
		public static UIColor TextColor => ToUIColor("4C4C4C");
		public static UIColor BannerHotLineColor => ToUIColor("263C76");

		public static UIColor LighterBlue => ToUIColor("BBDEFB");
		public static UIColor LightBlue => ToUIColor("1F92D1");
		public static UIColor DarkBlue => ToUIColor("3b5998");
		public static UIColor ThemeBlue => ToUIColor("073D6F");
		public static UIColor Theme2ndBlue => ToUIColor("0F57A5");
		public static UIColor Theme3rdBlue => ToUIColor("0464A5");
		public static UIColor TrueBlue => ToUIColor("2487C8");

		public static UIColor Yellow => ToUIColor("f7941d");
		public static UIColor Pink => ToUIColor("ffa3c1");
		public static UIColor Red => ToUIColor("db3236");
		public static UIColor Orange => ToUIColor("F58221");

		//public static UIColor BackgeoundGray => ToUIColor("E0E0E0");
		public static UIColor BorderGray => ToUIColor("E0E0E0");

		public static UIColor  CenterButton => ToUIColor("f6f6f6");
		public static UIColor EndButton => ToUIColor("ededed");

		//Dropdown bar
		public static UIColor DropdownBarEnd => ToUIColor("d5d5d5");

		//Feedback
		public static UIColor FeedbackTimeColor => ToUIColor("6D6E6D");
		public static UIColor FeedbackReactColor => ToUIColor("636463");

		//Popup
		public static UIColor Blue => ToUIColor("2196F3");


		//Complaint Detail
		public static UIColor BlueGray => ToUIColor("F0F7FB");
		public static UIColor BlueGrayBorder => ToUIColor("E2EDF3");
		public static UIColor NeutralBlue => ToUIColor("4e69a2");

        // Dashboard View
	    public static UIColor BoldTextColor => ToUIColor("585858");
	    public static UIColor BolderTextColor => ToUIColor("303030");
	    public static UIColor OrangeTextColor => ToUIColor("F47D06");
	    public static UIColor BlueTextColor => ToUIColor("1D5082");
	    public static UIColor OrangeLighterTextColor => ToUIColor("F78717");
	    public static UIColor BlueLighterTextColor => ToUIColor("006BA6");
	    public static UIColor DarkTextColor => ToUIColor("969696");



        public static UIColor ToUIColor(string hexString)
		{
			if (hexString.Length != 6 && hexString.Length != 8)
			{
				throw new Exception("Invalid hex string");
			}

			int index = -2;
			int alpha;
			if (hexString.Length == 6)
			{
				alpha = 255;
			}
			else
			{
				index += 2;
				alpha = int.Parse(hexString.Substring(index, 2), System.Globalization.NumberStyles.AllowHexSpecifier);
			}

			var red = int.Parse(hexString.Substring(index + 2, 2), System.Globalization.NumberStyles.AllowHexSpecifier);
			var green = int.Parse(hexString.Substring(index + 4, 2), System.Globalization.NumberStyles.AllowHexSpecifier);
			var blue = int.Parse(hexString.Substring(index + 6, 2), System.Globalization.NumberStyles.AllowHexSpecifier);

			return UIColor.FromRGBA(red, green, blue, alpha);
		}
	}
}