using System;
using System.Drawing;
using System.IO;
using UIKit;

namespace GovernmentComplaint.iOS.Helpers
{
    public static class ImageHelper
    {
        public const string IMAGE_PATH = "Images";

        public static string Back { get; private set; }

        //ToDo: define image variable here
        public static string Location { get; private set; }
        public static string Send { get; private set; }
		public static string Logo { get; private set; }
		public static string LogoTitle { get; private set; }
		public static string FacebookLogo { get; private set; }
		public static string GoogleLogo { get; private set; }
        public static string CalendarSort { get; private set; }
        public static string Time { get; private set; }
        public static string Images { get; private set; }
        public static string Link { get; private set; }
		public static string DanangLogo { get; private set; }
		public static string DanangLogoAbout { get; private set; }
		public static string SiouxLogo { get; private set; }
        public static string Remove { get; private set; }
		public static string Search { get; private set; }
		public static string SearchActive { get; private set; }
		public static string Menu { get; private set; }
		public static string MenuActive { get; private set; }
        public static string Test1 { get; private set; }
        public static string Test2 { get; private set; }
        public static string SearchDropdown { get; private set; }
        public static string DefaultImage { get; private set; }
        public static string CommentSort { get; private set; }
        public static string MainMenuImage { get; private set; }
        public static string DttLogo { get; private set; }
        public static string AboutBackground { get; private set; }
        public static string GooglePlusLogo { get; private set; }
        public static string GooglePlusLogoGray { get; private set; }
		public static string HotLineIcon { get; private set; }
		public static string HotLineBanner { get; private set; }
		public static string GGLogo { get; private set; }

        public static string CheckboxChecked { get; private set; }
        public static string CheckboxUnChecked { get; private set; }
        public static string RadioButtonChecked { get; private set; }
        public static string RadioButtonUnChecked { get; private set; }
        static ImageHelper()
        {
            Back = Path.Combine(IMAGE_PATH, "back_icon.png");
            Location = Path.Combine(IMAGE_PATH, "define_location.png");
            Send = Path.Combine(IMAGE_PATH, "send.png");
			Logo = Path.Combine(IMAGE_PATH, "logo.png");
			FacebookLogo = Path.Combine(IMAGE_PATH, "fb.png");
			GoogleLogo = Path.Combine(IMAGE_PATH, "googleplus.png");
            CalendarSort = Path.Combine(IMAGE_PATH, "calendar_sort.png");
            Time = Path.Combine(IMAGE_PATH, "time.png");
            Images = Path.Combine(IMAGE_PATH, "images.png");
            Link = Path.Combine(IMAGE_PATH, "link.png");
			DanangLogo = Path.Combine(IMAGE_PATH, "DN_logo_title_bar.png");
			DanangLogoAbout = Path.Combine(IMAGE_PATH, "DN_logo_about.png");
			SiouxLogo = Path.Combine(IMAGE_PATH, "Sioux_logo_about");
            Remove = Path.Combine(IMAGE_PATH, "remove.png");
			SearchActive = Path.Combine(IMAGE_PATH, "active_search_icon.png");
			Search = Path.Combine(IMAGE_PATH, "search_icon.png");
			Menu = Path.Combine(IMAGE_PATH, "menu_icon.png");
			MenuActive = Path.Combine(IMAGE_PATH, "active_menu_icon.png");
            Test1 = Path.Combine(IMAGE_PATH, "lunarsurface.jpg");
            Test2 = Path.Combine(IMAGE_PATH, "windows_10_build_10565_hidden_wallpapers_by_wandersons13-d9d0kjo.jpg");
            SearchDropdown = Path.Combine(IMAGE_PATH, "search_dropdown.png");
            DefaultImage = Path.Combine(IMAGE_PATH, "default_complaint_image.png");
            CommentSort = Path.Combine(IMAGE_PATH, "comment_sort.png");
            MainMenuImage = Path.Combine(IMAGE_PATH, "title_bg");
            DttLogo = Path.Combine(IMAGE_PATH, "DTT_logo_about");
            AboutBackground = Path.Combine(IMAGE_PATH, "about_bg_image");
            GooglePlusLogo = Path.Combine(IMAGE_PATH, "GoogleplusButton");
            GooglePlusLogoGray = Path.Combine(IMAGE_PATH, "googleplusgray");
			LogoTitle = Path.Combine(IMAGE_PATH, "DN_logo_title_bar");
			HotLineIcon = Path.Combine(IMAGE_PATH, "hotline_icon_title_bar");
			HotLineBanner = Path.Combine(IMAGE_PATH, "hotline_image.png");
			GGLogo = Path.Combine(IMAGE_PATH, "green_global_logo_about.png");

            CheckboxChecked = Path.Combine(IMAGE_PATH, "checkbox_checked.png");
            CheckboxUnChecked = Path.Combine(IMAGE_PATH, "checkbox_unchecked.png");
            RadioButtonChecked = Path.Combine(IMAGE_PATH, "radio_button_checked.png");
            RadioButtonUnChecked = Path.Combine(IMAGE_PATH, "radio_button_unchecked.png");
        }

        public static UIImage GetImageFromView(UIView view)
        {
            UIGraphics.BeginImageContext(view.Frame.Size);
            view.Layer.RenderInContext(UIGraphics.GetCurrentContext());

            var image = UIGraphics.GetImageFromCurrentImageContext();
            UIGraphics.EndImageContext();

            return image;
        }

        public static UIImage ResizeImageKeepRatio(UIImage sourceImage, nfloat maxSize)
        {
            if (sourceImage == null)
            {
                return null;
            }

            SizeF newSize;

            nfloat shorterSide;
            if (sourceImage.Size.Width > sourceImage.Size.Height)
            {
                shorterSide = maxSize * (sourceImage.Size.Height / sourceImage.Size.Width);

                newSize = new SizeF((float)maxSize, (float)shorterSide);
            }
            else
            {
                shorterSide = maxSize * (sourceImage.Size.Width / sourceImage.Size.Height);

                newSize = new SizeF((float)shorterSide, (float)maxSize);
            }


            UIGraphics.BeginImageContext(newSize);
            sourceImage.Draw(new RectangleF(new PointF(0, 0), newSize));
            var resultImage = UIGraphics.GetImageFromCurrentImageContext();
            UIGraphics.EndImageContext();

            return resultImage;
        }
    }
}