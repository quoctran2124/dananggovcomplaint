using System;
using CoreGraphics;
using GovernmentComplaint.iOS.Controls;
using GovernmentComplaint.iOS.Views.Popups;
using PatridgeDev;
using UIKit;
using PlaceholderTextView = PlaceholderEnabledUITextView.PlaceholderEnabledUITextView;

namespace GovernmentComplaint.iOS.Helpers
{
    public enum FontType
    {
        Thin,
        Light,
        Regular,
        Medium,
        Bold,
        Italic
    }

    public static class UIHelper
    {
        public static UIView CreateLayout(nfloat height, nfloat margin, UIColor backgroundColor)
        {
            var view = CreateView(0, height, backgroundColor);
            view.LayoutMargins = new UIEdgeInsets(margin, margin, margin, margin);
            return view;
        }

        public static UIView CreateLayout(nfloat height, UIEdgeInsets marginInsets, UIColor backgroundColor)
        {
            var view = CreateView(0, height, backgroundColor);
            view.LayoutMargins = marginInsets;
            return view;
        }

        public static UILabel CreateLabel(UIColor textColor, nfloat fontSize, bool isBold = false,
            UITextAlignment alignment = UITextAlignment.Left, int lines = 1)
        {
            var label = new UILabel();
            label.TranslatesAutoresizingMaskIntoConstraints = false;
            label.LineBreakMode = UILineBreakMode.WordWrap;
            label.TextAlignment = alignment;
            label.Lines = lines;
            label.TextColor = textColor;
            label.Font = isBold ? UIFont.FromName("Roboto-Bold", fontSize) : UIFont.FromName("Roboto-Regular", fontSize);
            return label;
        }

        public static UILabel CreateCustomColorLabel(nfloat fontSize, bool isBold = false,
            UITextAlignment alignment = UITextAlignment.Left, int lines = 1)
        {
            var label = new UILabel();
            label.TranslatesAutoresizingMaskIntoConstraints = false;
            label.LineBreakMode = UILineBreakMode.WordWrap;
            label.TextAlignment = alignment;
            label.Lines = lines;
            label.Font = isBold ? UIFont.FromName("Roboto-Bold", fontSize) : UIFont.FromName("Roboto-Regular", fontSize);
            return label;
        }

        public static CustomUILabel CreateCustomUILabel(UIColor textColor, nfloat fontSize,
            UITextAlignment alignment = UITextAlignment.Left, int lines = 1)
        {
            var label = new CustomUILabel();
            label.TranslatesAutoresizingMaskIntoConstraints = false;
            label.LineBreakMode = UILineBreakMode.WordWrap;
            label.TextAlignment = alignment;
            label.Lines = lines;
            label.TextColor = textColor;
            label.Font = UIFont.FromName("Helvetica", fontSize);
            return label;
        }


        public static AlphaUIButton CreateButton(nfloat Width, nfloat Height,
            UIColor textColor, nfloat fontSize,
            UIColor backgroundColor,
            bool isBold = false,
            bool isBorderEnabled = false,
            bool isRoundCorner = false)
        {
            var button = new AlphaUIButton();
            button.TranslatesAutoresizingMaskIntoConstraints = false;

            if (Width > 0)
                button.AddConstraint(NSLayoutConstraint.Create(button, NSLayoutAttribute.Width,
                    NSLayoutRelation.Equal, null, NSLayoutAttribute.NoAttribute, 1, Width));

            if (Height > 0)
                button.AddConstraint(NSLayoutConstraint.Create(button, NSLayoutAttribute.Height,
                    NSLayoutRelation.Equal, null, NSLayoutAttribute.NoAttribute, 1, Height));

            if (isRoundCorner)
                button.Layer.CornerRadius = DimensionHelper.RoundCorner;

            button.BackgroundColor = backgroundColor;
            button.SetTitleColor(textColor, UIControlState.Normal);
            button.Font = isBold
                ? UIFont.FromName("Roboto-Bold", fontSize)
                : UIFont.FromName("Roboto-Regular", fontSize);

            return button;
        }

        public static UIView CreateView(nfloat width, nfloat height, UIColor backgroundColor = null,
            bool isRoundCorner = false, bool isBorderEnabled = false)
        {
            var view = new UIView();
            view.TranslatesAutoresizingMaskIntoConstraints = false;

            if (width > 0)
                view.AddConstraint(NSLayoutConstraint.Create(view, NSLayoutAttribute.Width,
                    NSLayoutRelation.Equal, null, NSLayoutAttribute.NoAttribute, 1, width));

            if (height > 0)
                view.AddConstraint(NSLayoutConstraint.Create(view, NSLayoutAttribute.Height,
                    NSLayoutRelation.Equal, null, NSLayoutAttribute.NoAttribute, 1, height));


            view.BackgroundColor = backgroundColor;

            if (isRoundCorner)
                view.Layer.CornerRadius = width / 2;

            if (isBorderEnabled)
            {
                view.Layer.BorderWidth = 1;
                view.Layer.BorderColor = ColorHelper.BorderGray.CGColor;
            }

            return view;
        }

        public static UIView CreateWebView(nfloat width, nfloat height, UIColor backgroundColor = null,
            bool isRoundCorner = false, bool isBorderEnabled = false)
        {
            var view = new UIWebView();
            view.TranslatesAutoresizingMaskIntoConstraints = false;

            if (width > 0)
                view.AddConstraint(NSLayoutConstraint.Create(view, NSLayoutAttribute.Width,
                    NSLayoutRelation.Equal, null, NSLayoutAttribute.NoAttribute, 1, width));

            if (height > 0)
                view.AddConstraint(NSLayoutConstraint.Create(view, NSLayoutAttribute.Height,
                    NSLayoutRelation.Equal, null, NSLayoutAttribute.NoAttribute, 1, height));


            view.BackgroundColor = backgroundColor;

            if (isRoundCorner)
                view.Layer.CornerRadius = width / 2;

            if (isBorderEnabled)
            {
                view.Layer.BorderWidth = 1;
                view.Layer.BorderColor = ColorHelper.BorderGray.CGColor;
            }

            return view;
        }

        public static UIView CreateCircle(nfloat width, nfloat height, UIColor backgroundColor = null,
            bool isRoundCorner = false, bool isBorderEnabled = false)
        {
            var view = new UIView();
            view.TranslatesAutoresizingMaskIntoConstraints = false;

            if (width > 0)
                view.AddConstraint(NSLayoutConstraint.Create(view, NSLayoutAttribute.Width,
                    NSLayoutRelation.Equal, null, NSLayoutAttribute.NoAttribute, 1, width));

            if (height > 0)
                view.AddConstraint(NSLayoutConstraint.Create(view, NSLayoutAttribute.Height,
                    NSLayoutRelation.Equal, null, NSLayoutAttribute.NoAttribute, 1, height));


            view.BackgroundColor = backgroundColor;

            if (isRoundCorner)
                view.Layer.CornerRadius = height / 2;

            if (isBorderEnabled)
            {
                view.Layer.BorderWidth = 1;
                view.Layer.BorderColor = ColorHelper.BorderGray.CGColor;
            }

            return view;
        }

        public static AlphaUIView CreateAlphaView(nfloat width, nfloat height, nfloat margin,
            UIColor backgroundColor = null, bool isRoundCorner = false, bool isBorderEnabled = false)
        {
            var view = new AlphaUIView();
            view.TranslatesAutoresizingMaskIntoConstraints = false;

            if (width > 0)
                view.AddConstraint(NSLayoutConstraint.Create(view, NSLayoutAttribute.Width,
                    NSLayoutRelation.Equal, null, NSLayoutAttribute.NoAttribute, 1, width));

            if (height > 0)
                view.AddConstraint(NSLayoutConstraint.Create(view, NSLayoutAttribute.Height,
                    NSLayoutRelation.Equal, null, NSLayoutAttribute.NoAttribute, 1, height));

            view.LayoutMargins = new UIEdgeInsets(margin, margin, margin, margin);
            view.BackgroundColor = backgroundColor ?? UIColor.Clear;

            if (isRoundCorner)
                view.Layer.CornerRadius = DimensionHelper.RoundCorner;

            if (isBorderEnabled)
            {
                view.Layer.BorderWidth = 1;
                view.Layer.BorderColor = ColorHelper.BorderGray.CGColor;
            }

            return view;
        }

        public static AlphaLabel CreateAlphaLabel(UIColor textColor, nfloat fontSize, bool isBold = false,
            UITextAlignment alignment = UITextAlignment.Left, int lines = 1)
        {
            var label = new AlphaLabel();
            label.TranslatesAutoresizingMaskIntoConstraints = false;
            label.UserInteractionEnabled = true;

            label.TextAlignment = alignment;
            label.Lines = lines;
            label.TextColor = textColor;
            label.Font = isBold ? UIFont.FromName("Roboto-Bold", fontSize) : UIFont.FromName("Roboto-Regular", fontSize);

            return label;
        }

        //public static TableUIView CreateAlphaTableView(nfloat width, nfloat height, UIColor backgroundColor = null)
        //{
        //	var view = new TableUIView();
        //	view.TranslatesAutoresizingMaskIntoConstraints = false;

        //	if (width > 0)
        //	{
        //		view.AddConstraint(NSLayoutConstraint.Create(view, NSLayoutAttribute.Width,
        //			NSLayoutRelation.Equal, null, NSLayoutAttribute.NoAttribute, 1, width));
        //	}

        //	if (height > 0)
        //	{
        //		view.AddConstraint(NSLayoutConstraint.Create(view, NSLayoutAttribute.Height,
        //			NSLayoutRelation.Equal, null, NSLayoutAttribute.NoAttribute, 1, height));
        //	}

        //	view.BackgroundColor = backgroundColor ?? UIColor.Clear;

        //	return view;
        //}

        public static UIImageView CreateImageView(nfloat width, nfloat height,
            UIViewContentMode contentMode = UIViewContentMode.ScaleAspectFit)
        {
            var imageView = new UIImageView();
            imageView.TranslatesAutoresizingMaskIntoConstraints = false;

            if (width > 0)
                imageView.AddConstraint(NSLayoutConstraint.Create(imageView, NSLayoutAttribute.Width,
                    NSLayoutRelation.Equal, null, NSLayoutAttribute.NoAttribute, 1, width));

            if (height > 0)
                imageView.AddConstraint(NSLayoutConstraint.Create(imageView, NSLayoutAttribute.Height,
                    NSLayoutRelation.Equal, null, NSLayoutAttribute.NoAttribute, 1, height));

            imageView.ContentMode = contentMode;

            return imageView;
        }

        public static CustomImageView CreateCustomImageView(nfloat width, nfloat height,
            UIViewContentMode contentMode = UIViewContentMode.ScaleAspectFit)
        {
            var imageView = new CustomImageView();
            imageView.TranslatesAutoresizingMaskIntoConstraints = false;

            if (width > 0)
                imageView.AddConstraint(NSLayoutConstraint.Create(imageView, NSLayoutAttribute.Width,
                    NSLayoutRelation.Equal, null, NSLayoutAttribute.NoAttribute, 1, width));

            if (height > 0)
                imageView.AddConstraint(NSLayoutConstraint.Create(imageView, NSLayoutAttribute.Height,
                    NSLayoutRelation.Equal, null, NSLayoutAttribute.NoAttribute, 1, height));

            imageView.ContentMode = contentMode;

            return imageView;
        }

        //public static RadioButton CreateRadioButton(nfloat width, nfloat height)
        //{
        //    var radioButton = new RadioButton();

        //    if (width > 0)
        //    {
        //        radioButton.AddConstraint(NSLayoutConstraint.Create(radioButton, NSLayoutAttribute.Width,
        //            NSLayoutRelation.Equal, null, NSLayoutAttribute.NoAttribute, 1, width));
        //    }

        //    if (height > 0)
        //    {
        //        radioButton.AddConstraint(NSLayoutConstraint.Create(radioButton, NSLayoutAttribute.Height,
        //            NSLayoutRelation.Equal, null, NSLayoutAttribute.NoAttribute, 1, height));
        //    }

        //    return radioButton;
        //}

        public static UITableView CreateTableView(nfloat width, nfloat height, UIColor backgroundColor)
        {
            var tableView = new UITableView();
            tableView.TranslatesAutoresizingMaskIntoConstraints = false;
            tableView.DelaysContentTouches = false;
            tableView.RowHeight = UITableView.AutomaticDimension;
            tableView.AllowsMultipleSelection = false;
            tableView.ExclusiveTouch = false;
            tableView.EstimatedRowHeight = 3 * DimensionHelper.NotificationHeight;
            if (width > 0)
            {
                tableView.AddConstraint(NSLayoutConstraint.Create(tableView, NSLayoutAttribute.Width,
                   NSLayoutRelation.Equal, null, NSLayoutAttribute.NoAttribute, 1, width));
            }
            if (height > 0)
            {
                tableView.AddConstraint(NSLayoutConstraint.Create(tableView, NSLayoutAttribute.Height,
                    NSLayoutRelation.Equal, null, NSLayoutAttribute.NoAttribute, 1, height));
            }
            tableView.AllowsSelection = false;
            tableView.Bounces = false;
            tableView.BackgroundColor = backgroundColor;
            tableView.SeparatorStyle = UITableViewCellSeparatorStyle.None;
            tableView.ReloadData();
            return tableView;
        }

        public static FloatingButton CreateFloatingButton(nfloat width, nfloat height, nfloat padding,
            bool isRoundCorner = false, UIViewContentMode contentMode = UIViewContentMode.ScaleAspectFit)
        {
            var button = new FloatingButton(new UIEdgeInsets(padding, padding, padding, padding), contentMode);
            button.TranslatesAutoresizingMaskIntoConstraints = false;
            button.ExclusiveTouch = true;

            if (width > 0)
                button.WidthConstraint = NSLayoutConstraint.Create(button, NSLayoutAttribute.Width,
                    NSLayoutRelation.Equal, null, NSLayoutAttribute.NoAttribute, 1, width);

            if (height > 0)
                button.AddConstraint(NSLayoutConstraint.Create(button, NSLayoutAttribute.Height,
                    NSLayoutRelation.Equal, null, NSLayoutAttribute.NoAttribute, 1, height));

            if (isRoundCorner)
                button.Layer.CornerRadius = DimensionHelper.RoundCorner;

            return button;
        }

        public static AlphaImageView CreateAlphaImageView(nfloat width, nfloat height, nfloat padding,
            bool isRoundCorner = false, UIViewContentMode contentMode = UIViewContentMode.ScaleAspectFit)
        {
            var imageView = new AlphaImageView(new UIEdgeInsets(padding, padding, padding, padding), contentMode);
            imageView.TranslatesAutoresizingMaskIntoConstraints = false;
            imageView.ExclusiveTouch = true;

            if (width > 0)
                imageView.WidthConstraint = NSLayoutConstraint.Create(imageView, NSLayoutAttribute.Width,
                    NSLayoutRelation.Equal, null, NSLayoutAttribute.NoAttribute, 1, width);

            if (height > 0)
                imageView.AddConstraint(NSLayoutConstraint.Create(imageView, NSLayoutAttribute.Height,
                    NSLayoutRelation.Equal, null, NSLayoutAttribute.NoAttribute, 1, height));

            if (isRoundCorner)
                imageView.Layer.CornerRadius = DimensionHelper.RoundCorner;

            return imageView;
        }

        public static AlphaImageView CreateImageButton(nfloat width, nfloat height,
            UIViewContentMode contentMode = UIViewContentMode.ScaleAspectFit)
        {
            var imageView = new AlphaImageView(UIEdgeInsets.Zero, contentMode);
            imageView.TranslatesAutoresizingMaskIntoConstraints = false;

            imageView.UserInteractionEnabled = true;
            imageView.TranslatesAutoresizingMaskIntoConstraints = false;
            imageView.BackgroundColor = ColorHelper.Transparent;

            if (width > 0)
                imageView.AddConstraint(NSLayoutConstraint.Create(imageView, NSLayoutAttribute.Width,
                    NSLayoutRelation.Equal, null, NSLayoutAttribute.NoAttribute, 1, width));

            if (height > 0)
                imageView.AddConstraint(NSLayoutConstraint.Create(imageView, NSLayoutAttribute.Height,
                    NSLayoutRelation.Equal, null, NSLayoutAttribute.NoAttribute, 1, height));

            imageView.ContentMode = contentMode;

            return imageView;
        }

        public static CustomRatingBar CreateCustomRatingBar(nfloat width, nfloat height, bool interaction = false)
        {
            var ratingConfig = new RatingConfig(UIImage.FromBundle("Images/star_empty"),
                UIImage.FromBundle("Images/star_full"),
                UIImage.FromBundle("Images/star_full"));
            // var ratingFrame = new CGRect(CGPoint.Empty, new CGSize(width, height));
            var ratingView = new CustomRatingBar(ratingConfig);
            ratingView.TranslatesAutoresizingMaskIntoConstraints = false;
            ratingView.AddConstraint(NSLayoutConstraint.Create(ratingView, NSLayoutAttribute.Height,
                NSLayoutRelation.Equal, null, NSLayoutAttribute.NoAttribute, 1, height));
            ratingView.AddConstraint(NSLayoutConstraint.Create(ratingView, NSLayoutAttribute.Width,
                NSLayoutRelation.Equal, null, NSLayoutAttribute.NoAttribute, 1, width));
            ratingView.UserInteractionEnabled = interaction;
            ratingView.AverageRating = 0m;
            ratingView.LayoutSubviews();
            return ratingView;
        }

        public static PDRatingView CreateRatingBar(nfloat width, nfloat height, bool interaction = false)
        {
            var ratingConfig = new RatingConfig(UIImage.FromBundle("Images/star_empty"),
                UIImage.FromBundle("Images/star_full"),
                UIImage.FromBundle("Images/star_full"));
            // var ratingFrame = new CGRect(CGPoint.Empty, new CGSize(width, height));
            var ratingView = new PDRatingView(ratingConfig);
            ratingView.TranslatesAutoresizingMaskIntoConstraints = false;
            ratingView.AddConstraint(NSLayoutConstraint.Create(ratingView, NSLayoutAttribute.Height,
                NSLayoutRelation.Equal, null, NSLayoutAttribute.NoAttribute, 1, height));
            ratingView.AddConstraint(NSLayoutConstraint.Create(ratingView, NSLayoutAttribute.Width,
                NSLayoutRelation.Equal, null, NSLayoutAttribute.NoAttribute, 1, width));
            ratingView.UserInteractionEnabled = interaction;
            ratingView.AverageRating = 0m;
            ratingView.LayoutSubviews();
            return ratingView;
        }
		
		public static DisappearableUIView CreateDisappearableLayout(nfloat width, UIColor backgroundColor)
		{
			var view = new DisappearableUIView
			{
				TranslatesAutoresizingMaskIntoConstraints = false,
				BackgroundColor = backgroundColor
			};

			if (width > 0)
			{
				view.WidthConstraint = NSLayoutConstraint.Create(view, NSLayoutAttribute.Width,
					NSLayoutRelation.Equal, null, NSLayoutAttribute.NoAttribute, 1, width);
			}

			return view;
		}

        public static DisappearableVerticalUiView CreateDisappearableVerticalView(nfloat height, UIColor backgroundColor)
        {
            var view = new DisappearableVerticalUiView
            {
                TranslatesAutoresizingMaskIntoConstraints = false,
                BackgroundColor = backgroundColor
            };

            if (height > 0)
            {
                view.HeightConstrainst = NSLayoutConstraint.Create(view, NSLayoutAttribute.Height,
                    NSLayoutRelation.Equal, null, NSLayoutAttribute.NoAttribute, 1, height);
            }

            return view;
        }

        public static UICollectionView CreateCollectionView(nfloat width, nfloat height, CGRect rectangle,
            UICollectionViewLayout collectionViewLayout)
        {
            var collectionView = new UICollectionView(rectangle, collectionViewLayout);
            collectionView.TranslatesAutoresizingMaskIntoConstraints = false;
            collectionView.DelaysContentTouches = false;

            if (width > 0)
                collectionView.AddConstraint(NSLayoutConstraint.Create(collectionView, NSLayoutAttribute.Width,
                    NSLayoutRelation.Equal, null, NSLayoutAttribute.NoAttribute, 1, width));

            if (height > 0)
                collectionView.AddConstraint(NSLayoutConstraint.Create(collectionView, NSLayoutAttribute.Height,
                    NSLayoutRelation.Equal, null, NSLayoutAttribute.NoAttribute, 1, height));

            collectionView.AllowsSelection = false;
            collectionView.Bounces = false;
            collectionView.ShowsVerticalScrollIndicator = false;
            collectionView.ShowsHorizontalScrollIndicator = false;

            collectionView.BackgroundColor = UIColor.Clear;

            return collectionView;
        }

        public static UIScrollView CreateScrollView(nfloat width, nfloat height, UIColor backgroundColor,
            bool isRoundCorner = false)
        {
            var view = new UIScrollView();
            view.TranslatesAutoresizingMaskIntoConstraints = false;
            view.DelaysContentTouches = false;
            view.ShowsVerticalScrollIndicator = false;
            if (width > 0)
                view.AddConstraint(NSLayoutConstraint.Create(view, NSLayoutAttribute.Width,
                    NSLayoutRelation.Equal, null, NSLayoutAttribute.NoAttribute, 1, width));

            if (height > 0)
                view.AddConstraint(NSLayoutConstraint.Create(view, NSLayoutAttribute.Height,
                    NSLayoutRelation.Equal, null, NSLayoutAttribute.NoAttribute, 1, height));

            view.BackgroundColor = backgroundColor;

            if (isRoundCorner)
                view.Layer.CornerRadius = DimensionHelper.RoundCorner;

            return view;
        }

        public static UIActivityIndicatorView CreateIndicator(nfloat width, nfloat height, UIColor backgroundColor)
        {
            var indicator = new UIActivityIndicatorView();
            indicator.TranslatesAutoresizingMaskIntoConstraints = false;

            if (width > 0)
                indicator.AddConstraint(NSLayoutConstraint.Create(indicator, NSLayoutAttribute.Width,
                    NSLayoutRelation.Equal, null, NSLayoutAttribute.NoAttribute, 1, width));

            if (height > 0)
                indicator.AddConstraint(NSLayoutConstraint.Create(indicator, NSLayoutAttribute.Height,
                    NSLayoutRelation.Equal, null, NSLayoutAttribute.NoAttribute, 1, height));

            indicator.Color = backgroundColor;

            return indicator;
        }

        public static UIStackView CreateStackView(nfloat width, nfloat height,
            UILayoutConstraintAxis axis = UILayoutConstraintAxis.Vertical, nfloat spacing = default(nfloat))
        {
            var stackView = new UIStackView();
            stackView.TranslatesAutoresizingMaskIntoConstraints = false;

            if (width > 0)
                stackView.AddConstraint(NSLayoutConstraint.Create(stackView, NSLayoutAttribute.Width,
                    NSLayoutRelation.Equal, null, NSLayoutAttribute.NoAttribute, 1, width));

            if (height > 0)
                stackView.AddConstraint(NSLayoutConstraint.Create(stackView, NSLayoutAttribute.Height,
                    NSLayoutRelation.Equal, null, NSLayoutAttribute.NoAttribute, 1, height));

            stackView.Axis = axis;
            stackView.Alignment = UIStackViewAlignment.Fill;
            stackView.Distribution = UIStackViewDistribution.FillEqually;
            stackView.Spacing = spacing;
            return stackView;
        }

        public static PlaceholderTextView CreateTextView(nfloat width, nfloat height, nfloat fontSize)
        {
            var textView = new PlaceholderTextView
            {
                Editable = true,
                TranslatesAutoresizingMaskIntoConstraints = false
            };

            if (width > 0)
                textView.AddConstraint(NSLayoutConstraint.Create(textView, NSLayoutAttribute.Width,
                    NSLayoutRelation.Equal, null, NSLayoutAttribute.NoAttribute, 1, width));

            if (height > 0)
                textView.AddConstraint(NSLayoutConstraint.Create(textView, NSLayoutAttribute.Height,
                    NSLayoutRelation.Equal, null, NSLayoutAttribute.NoAttribute, 1, height));

            textView.Font = UIFont.FromName("Roboto-Regular", fontSize);

            return textView;
        }

        public static UITextField CreateTextField(nfloat fontSize, FontType fontType = FontType.Bold,
            UIColor backgroundColor = null, bool isRoundCorner = false, bool isBorderEnabled = false)
        {
            var textField = new UITextField();
            textField.TranslatesAutoresizingMaskIntoConstraints = false;

            textField.TextAlignment = UITextAlignment.Left;
            textField.AutocorrectionType = UITextAutocorrectionType.No;
            textField.AutocapitalizationType = UITextAutocapitalizationType.None;
            textField.ReturnKeyType = UIReturnKeyType.Done;
            // textField.EditingDidBegin += TextFieldOnEditingDidBegin;
            //textField.BorderStyle = UITextBorderStyle.RoundedRect;

            textField.TextColor = ColorHelper.DarkGray;
            textField.AccessibilityHint = "ABC";
            // textField.BorderStyle = borderStyle;
            textField.Font = UIFont.FromName("Roboto-Regular", fontSize);

            textField.BackgroundColor = backgroundColor ?? UIColor.Clear;
            if (isRoundCorner)
                textField.Layer.CornerRadius = DimensionHelper.RoundCorner;

            if (isBorderEnabled)
            {
                textField.Layer.BorderWidth = 1;
                textField.Layer.BorderColor = ColorHelper.BorderGray.CGColor;
            }
            return textField;
        }

        public static UIPickerView CreatePickerView(UIColor backgroundColor = null, bool isRoundCorner = false,
            bool isBorderEnabled = false)
        {
            var pickerView = new UIPickerView
            {
                TranslatesAutoresizingMaskIntoConstraints = false,
                BackgroundColor = backgroundColor ?? UIColor.Clear
            };

            if (isRoundCorner)
                pickerView.Layer.CornerRadius = DimensionHelper.RoundCorner;

            if (!isBorderEnabled) return pickerView;
            pickerView.Layer.BorderWidth = 1;
            pickerView.Layer.BorderColor = ColorHelper.BorderGray.CGColor;

            return pickerView;
        }

        private static UIFont GetFont(FontType fontType, nfloat fontSize)
        {
            switch (fontType)
            {
                case FontType.Thin:
                    return UIFont.FromName("Roboto-Thin", fontSize);
                case FontType.Light:
                    return UIFont.FromName("Roboto-Light", fontSize);
                case FontType.Medium:
                    return UIFont.FromName("Roboto-Medium", fontSize);
                case FontType.Bold:
                    return UIFont.FromName("Roboto-Bold", fontSize);
                case FontType.Italic:
                    return UIFont.FromName("Roboto-Italic", fontSize);
                default:
                    return UIFont.FromName("Roboto-Regular", fontSize);
            }
        }
    }
}