using Android.Support.V4.Content.Res;
using Android.Views;

namespace GovernmentComplaint.Droid.Listener
{
    public class MyTouchListener : Java.Lang.Object, View.IOnTouchListener
    {
        public bool OnTouch(View v, MotionEvent e)
        {
            if (v.Id == Resource.Id.CommentFrame || v.Id == Resource.Id.ComplaintContent)
            {
                v.RequestFocus();
                v.Parent.RequestDisallowInterceptTouchEvent(true);
                switch (e.Action & MotionEventActions.Mask)
                {
                    case MotionEventActions.Up:
                        v.Parent.RequestDisallowInterceptTouchEvent(false);
                        break;
                }
            }
            return false;
        }
    }
}