﻿using System;
using Android.Content;
using GovernmentComplaint.Core.Helpers;
using GovernmentComplaint.Droid.Helpers;
using MvvmCross.Platform;

namespace GovernmentComplaint.Droid.Receivers
{
	[BroadcastReceiver(Enabled = true, Exported = false)]
	public class NetworkChangeReceiver : BroadcastReceiver
	{
		public bool IsFirstReceive = true;
		public event EventHandler NetworkConnected;
		private NetworkHelper _networkHelper;
		public override void OnReceive(Context context, Intent intent)
		{
			if (!IsFirstReceive)
			{
				if (_networkHelper.IsConnected)
				{
					NetworkConnected?.Invoke(this, EventArgs.Empty);
				}
			}
			else
			{
				_networkHelper = new NetworkHelper();
				IsFirstReceive = false;
			}
		}
	}
}