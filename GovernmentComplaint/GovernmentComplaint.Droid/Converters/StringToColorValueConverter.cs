using System;
using System.Globalization;
using Android.Graphics;
using MvvmCross.Platform.Converters;

namespace GovernmentComplaint.Droid.Converters
{
    public class StringToColorValueConverter : MvxValueConverter<string, Color>
    {
        protected override Color Convert(string value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!string.IsNullOrEmpty(value))
            {
                return Color.ParseColor(value.Trim());
            }
            else
            {
                return Color.ParseColor("#0464A5");
            }
        }
    }
}