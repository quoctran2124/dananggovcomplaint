using System;
using System.Globalization;
using MvvmCross.Platform.Converters;
using Android.Graphics;
using GovernmentComplaint.Droid.Helpers;

namespace GovernmentComplaint.Droid.Converters
{
    public class BoolToHintColorValueConverter : MvxValueConverter<bool, Color>
    {
        protected override Color Convert(bool value, Type targetType, object parameter, CultureInfo culture)
        {
            return value ? ColorHelper.Red : ColorHelper.LightGray;
        }
    }
}