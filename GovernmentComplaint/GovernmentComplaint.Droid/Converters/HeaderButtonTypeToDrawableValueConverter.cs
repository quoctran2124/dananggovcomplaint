
using GovernmentComplaint.Core.Enums;
using System;
using System.Globalization;
using Android.App;
using Android.Graphics.Drawables;
using Android.Support.V4.Content;
using MvvmCross.Platform.Converters;

namespace GovernmentComplaint.Droid.Converters
{
	public class HeaderButtonTypeToDrawableValueConverter : MvxValueConverter<HeaderButtonType, Drawable>
	{
		protected override Drawable Convert(HeaderButtonType value, Type targetType, object parameter,
			CultureInfo culture)
		{
			switch (value)
			{
				case HeaderButtonType.None:
					return null;
				case HeaderButtonType.Back:
					return ContextCompat.GetDrawable(Application.Context, Resource.Drawable.back_icon);
				case HeaderButtonType.Search:
					return ContextCompat.GetDrawable(Application.Context, Resource.Drawable.search_icon);
				case HeaderButtonType.SearchActive:
					return ContextCompat.GetDrawable(Application.Context, Resource.Drawable.active_search_icon);
				case HeaderButtonType.Menu:
					return ContextCompat.GetDrawable(Application.Context, Resource.Drawable.menu_icon);
				case HeaderButtonType.MenuActive:
					return ContextCompat.GetDrawable(Application.Context, Resource.Drawable.active_menu_icon);
                case HeaderButtonType.Logo:
                    return ContextCompat.GetDrawable(Application.Context, Resource.Drawable.DN_logo_title_bar);
                default:
					throw new Exception("Header button type is not supported");
			}
		}
	}
}