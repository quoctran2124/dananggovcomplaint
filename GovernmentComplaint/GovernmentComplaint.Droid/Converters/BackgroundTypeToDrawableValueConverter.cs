using System;
using System.Globalization;
using Android.App;
using Android.Graphics.Drawables;
using Android.Support.V4.Content;
using GovernmentComplaint.Core.Enums;
using MvvmCross.Platform.Converters;

namespace GovernmentComplaint.Droid.Converters
{
    public class BackgroundTypeToDrawableValueConverter : MvxValueConverter<BackgroundType, Drawable>
    {
        protected override Drawable Convert(BackgroundType value, Type targetType, object parameter, CultureInfo culture)
        {
            switch (value)
            {
                case BackgroundType.Error:
                    return GetDrawable(Resource.Drawable.new_complaint_background_error);
                case BackgroundType.Normal:
                    return GetDrawable(Resource.Drawable.new_complaint_background);
                default:
                    return null;
            }
        }

        private Drawable GetDrawable(int id)
        {
            return ContextCompat.GetDrawable(Application.Context, id);
        }
    }
}