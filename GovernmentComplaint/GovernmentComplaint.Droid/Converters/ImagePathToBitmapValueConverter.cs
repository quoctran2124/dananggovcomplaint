using MvvmCross.Platform.Converters;
using System;
using System.Globalization;
using System.Net;
using Android.Graphics;
using Com.Bumptech.Glide;

namespace GovernmentComplaint.Droid.Converters
{
    public class ImagePathToBitmapValueConverter : MvxValueConverter<string, Bitmap>
    {
        protected override Bitmap Convert(string value, Type targetType, object parameter, CultureInfo culture)
        {
            Bitmap imageBitmap = null;
            if (value != null)
            {
                using (var webClient = new WebClient())
                {
                    //TODO: Scale too big images, try catch exception
                    var imageBytes = webClient.DownloadData(value);
                    if (imageBytes != null && imageBytes.Length > 0)
                    {
                        imageBitmap = BitmapFactory.DecodeByteArray(imageBytes, 0, imageBytes.Length);
                    }
                }
            }
            return imageBitmap;
        }
        
    }
}