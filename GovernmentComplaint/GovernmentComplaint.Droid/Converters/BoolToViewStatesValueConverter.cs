using System;
using System.Globalization;
using MvvmCross.Platform.Converters;

namespace GovernmentComplaint.Droid.Converters
{
	public class BoolToViewStatesValueConverter : MvxValueConverter<bool, string>
	{
		protected override string Convert(bool value, Type targetType, object parameter, CultureInfo culture)
		{
			if (value)
			{
				return "visible";
			}
			return "gone";
		}
	}
}