using System;
using GovernmentComplaint.Core.Enums;
using System.Globalization;
using Android.Graphics;
using GovernmentComplaint.Droid.Helpers;
using MvvmCross.Platform.Converters;

namespace GovernmentComplaint.Droid.Converters
{
	public class SidebarMenuStatusToColorValueConverter : MvxValueConverter<ItemStatus, Color>
	{
		protected override Color Convert(ItemStatus value, Type targetType, object parameter, CultureInfo culture)
		{
			switch (value)
			{
				case ItemStatus.Active:
					return ColorHelper.Orange;
				default:
					return ColorHelper.NeutralGray;
			}
		}
	}
}