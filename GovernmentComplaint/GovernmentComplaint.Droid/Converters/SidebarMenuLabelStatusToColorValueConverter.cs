using System;
using GovernmentComplaint.Core.Enums;
using System.Globalization;
using Android.Graphics;
using GovernmentComplaint.Droid.Helpers;
using MvvmCross.Platform.Converters;

namespace GovernmentComplaint.Droid.Converters
{
	public class SidebarMenuLabelStatusToColorValueConverter : MvxValueConverter<LabelStatus, Color>
	{
		protected override Color Convert(LabelStatus value, Type targetType, object parameter, CultureInfo culture)
		{
			switch (value)
			{
				case LabelStatus.Special:
					return ColorHelper.Orange;
				case LabelStatus.Active:
					return ColorHelper.White;
				case LabelStatus.Inactive:
					return ColorHelper.Black;
				default:
					return ColorHelper.Black;
			}
		}
	}
}