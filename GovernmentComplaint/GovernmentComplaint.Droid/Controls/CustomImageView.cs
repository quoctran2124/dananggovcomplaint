using Android.App;
using Android.Content;
using Android.OS;
using Android.Support.V4.Content;
using Android.Util;
using Android.Widget;
using Com.Bumptech.Glide;

namespace GovernmentComplaint.Droid.Controls
{
	public class CustomImageView : ImageView
	{
		public CustomImageView(Context context) : base(context)
		{
		}

		public CustomImageView(Context context, IAttributeSet attrs) : base(context, attrs)
		{
		}

		public string ImageUrl
		{
			set
			{
				if (!string.IsNullOrEmpty(value))
				{
					Glide.With(Application.Context).Load(value).DontAnimate().Into(this);
				}
				else
				{
					if (Build.VERSION.SdkInt >= BuildVersionCodes.LollipopMr1)
					{
						SetImageDrawable(Resources.GetDrawable(Resource.Drawable.default_complaint_image, null));
					}
					else
					{
						SetImageDrawable(ContextCompat.GetDrawable(Application.Context, Resource.Drawable.default_complaint_image));
					}
				}
			}
		}
	}
}