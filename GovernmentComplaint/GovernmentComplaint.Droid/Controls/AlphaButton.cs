using Android.App;
using Android.Content;
using Android.Graphics.Drawables;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace GovernmentComplaint.Droid.Controls
{
    public class AlphaButton : Button
    {
        public AlphaButton(Context context)
            : base(context)
        {
        }

        public AlphaButton(Context context, IAttributeSet attrs)
            : base(context, attrs)
        {
        }

        public Drawable Icon
        {
            set
            {
                Background = value;

                if (value != null)
                {
                    var width = (int) Application.Context.Resources.GetDimension(Resource.Dimension.TitleBarButtonWidth);
                    LayoutParameters.Width = width;
                }
                else if (LayoutParameters != null)
                {
                    LayoutParameters.Width = ViewGroup.LayoutParams.WrapContent;
                }
            }
        }

        public override bool OnTouchEvent(MotionEvent e)
        {
            if (Clickable && Enabled)
            {
                switch (e.Action)
                {
                    case MotionEventActions.Down:
                        Alpha = 0.5f;
                        break;
                    case MotionEventActions.Up:
                    case MotionEventActions.Cancel:
                        Alpha = 1f;
                        break;
                    default:
                        break;
                }
            }

            return base.OnTouchEvent(e);
        }
    }
}