using System;
using System.Windows.Input;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Views.InputMethods;
using Android.Widget;
using GovernmentComplaint.Droid.Helpers;

namespace GovernmentComplaint.Droid.Controls
{
    public class CustomEditText : EditText
    {
        private bool _needHideKeyboard;

        private Color _hintColor;
        public Color HintColor
        {
            get { return _hintColor; }
            set
            {
                _hintColor = value;
                SetHintTextColor(value);
            }
        }

        public CustomEditText(Context context) : base(context)
        {
        }

        public CustomEditText(Context context, IAttributeSet attrs) : base(context, attrs)
        {
        }

        public ICommand TouchCommand { get; set; }

        public bool NeedHideKeyboard
        {
            get { return _needHideKeyboard; }
            set
            {
                _needHideKeyboard = value;
                if (value)
                {
                    ClearFocus();
                }
            }
        }

        public override bool OnKeyPreIme(Keycode keyCode, KeyEvent e)
        {
            if (e.KeyCode == Keycode.Back)
            {
                ClearFocus();
                return true;
            }
            return base.OnKeyPreIme(keyCode, e);
        }

        public override void OnEditorAction(ImeAction actionCode)
        {
            if (actionCode == ImeAction.Done)
            {
                ClearFocus();
            }

            base.OnEditorAction(actionCode);
        }

        private bool _isKeyboardShown;

        protected override void OnFocusChanged(bool gainFocus, [GeneratedEnum] FocusSearchDirection direction,
            Rect previouslyFocusedRect)
        {
            //try
            {
                base.OnFocusChanged(gainFocus, direction, previouslyFocusedRect);

                if (!gainFocus)
                {
                    KeyboardHelper.HideKeyboard(this);
                }
            }
        }
        
        public override bool OnTouchEvent(MotionEvent e)
        {
                if ((e.Action == MotionEventActions.Down) && (KeyboardHelper.isKeyBoardShow == false))
                {
                    TouchCommand?.Execute(null);
                }
                base.OnTouchEvent(e);

            return true;
        }
    }
}