using System.Windows.Input;
using Android.Content;
using Android.Util;
using Android.Widget;
using MvvmCross.Binding.Droid.Views;

namespace GovernmentComplaint.Droid.Controls
{
    public class AutoloadingListView : MvxListView, AbsListView.IOnScrollListener
    {
        public bool LoadingMore { get; set; }
        public ICommand LoadMoreData { get; set; }
        public AutoloadingListView(Context context, IAttributeSet attrs) : base(context, attrs)
        {
            SetOnScrollListener(this);
        }

        public AutoloadingListView(Context context, IAttributeSet attrs, IMvxAdapter adapter)
            : base(context, attrs, adapter)
        {
            SetOnScrollListener(this);
        }

        public void OnScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount)
        {
            var lastInScreen = firstVisibleItem + visibleItemCount;
            if (totalItemCount > 0 && (lastInScreen == totalItemCount) && !LoadingMore)
            {
                LoadMoreData.Execute(null);
                LoadingMore = true;
            }
           
        }

        public void OnScrollStateChanged(AbsListView view, ScrollState scrollState)
        {
        }
    }
}