using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Support.V4.Content;
using Android.Util;
using Com.Bumptech.Glide;
using Refractored.Controls;

namespace GovernmentComplaint.Droid.Controls
{
	public class CustomCircleImageView : CircleImageView
	{
		public CustomCircleImageView(Context context) : base(context)
		{
		}

		public CustomCircleImageView(Context context, IAttributeSet attrs) : base(context, attrs)
		{
		}

		public CustomCircleImageView(Context context, IAttributeSet attrs, int defStyle)
			: base(context, attrs, defStyle)
		{
		}

		public string ImageUrl
		{
			set
			{
				if (!string.IsNullOrEmpty(value))
				{
					Glide.With(Application.Context).Load(value).DontAnimate().Into(this);
				}
				else
				{
					if (Build.VERSION.SdkInt >= BuildVersionCodes.LollipopMr1)
					{
						SetImageDrawable(Resources.GetDrawable(Resource.Drawable.default_complaint_image, null));
					}
					else
					{
						SetImageDrawable(ContextCompat.GetDrawable(Application.Context, Resource.Drawable.default_complaint_image));
					}
				}
			}
		}
	}
}