using Android.App;
using Android.Content;
using Android.OS;
using Android.Util;
using Com.Bumptech.Glide;
using ImageViews.Photo;

namespace GovernmentComplaint.Droid.Controls
{
    public class CustomPhotoView : PhotoView
    {
        public CustomPhotoView(Context context) : base(context)
        {
        }

        public CustomPhotoView(Context context, IAttributeSet attrs) : base(context, attrs)
        {
        }


        public string ImageUrl
        {
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    Glide.With(Application.Context).Load(value).DontAnimate().Into(this);
                }
            }
        }
    }
}