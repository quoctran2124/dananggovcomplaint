using Android.Content;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace GovernmentComplaint.Droid.Controls
{
	public class AlphaRelativeLayout : RelativeLayout
	{
		public AlphaRelativeLayout(Context context)
		: base(context)
		{
		}

		public AlphaRelativeLayout(Context context, IAttributeSet attrs)
		: base(context, attrs)
		{
		}

		public override bool OnTouchEvent(MotionEvent e)
		{
			if (Clickable && Enabled)
			{
				switch (e.Action)
				{
					case MotionEventActions.Down:
						Alpha = 0.5f;
						break;
					case MotionEventActions.Up:
					case MotionEventActions.Cancel:
						Alpha = 1f;
						break;
				}
			}

			return base.OnTouchEvent(e);
		}
	}
}