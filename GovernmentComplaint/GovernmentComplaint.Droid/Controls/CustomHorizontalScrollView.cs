using System;
using Android.Content;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using GovernmentComplaint.Droid.Helpers;

namespace GovernmentComplaint.Droid.Controls
{
    public class CustomHorizontalScrollView : HorizontalScrollView
    {
        public delegate void ScrollState(bool isClicked);
        public event ScrollState ScrollStateChanged;
        private bool _isClicked;
        private float _initialX, _finalX, _initialY;

        public CustomHorizontalScrollView(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
        {
        }

        public CustomHorizontalScrollView(Context context) : base(context)
        {
        }

        public CustomHorizontalScrollView(Context context, IAttributeSet attrs) : base(context, attrs)
        {
        }

        public override bool OnTouchEvent(MotionEvent e)
        {
            switch (e.Action)
            {
                case MotionEventActions.Down:
                    Parent?.RequestDisallowInterceptTouchEvent(true);
                    _isClicked = true;
                    _initialX = e.GetX();
                    _initialY = e.GetY();
                    break;
                case MotionEventActions.Move:
                    var x = e.GetX();
                    var y = e.GetY();

                    var deviation = ResolutionHelper.ToPixel(5);
                    _isClicked = (Math.Abs(_initialX - x) < deviation) && (Math.Abs(_initialY - y) < deviation);

                    break;
                case MotionEventActions.Up:
                    Parent?.RequestDisallowInterceptTouchEvent(false);
                    if (!_isClicked)
                    {
                        _finalX = e.GetX();
                        if (_initialX < _finalX)
                        {
                            SmoothScrollBy(-ResolutionHelper.Width, 0);
                        }
                        if (_initialX > _finalX)
                        {
                            SmoothScrollBy(ResolutionHelper.Width, 0);
                        }
                    }
                   
                    ScrollStateChanged?.Invoke(_isClicked);

                    break;
                case MotionEventActions.Cancel:
                    Parent?.RequestDisallowInterceptTouchEvent(false);
                    break;
            }
            return true;
        }

        public new void SmoothScrollBy(int dx, int dy)
        {
            base.SmoothScrollBy(dx, dy);
            this.ScrollX += dx;
        }

        public override bool OnKeyDown(Keycode keyCode, KeyEvent e)
        {
            return false;
        }

        public override bool OnKeyLongPress(Keycode keyCode, KeyEvent e)
        {
            return false;
        }

        public override bool OnKeyUp(Keycode keyCode, KeyEvent e)
        {
            return false;
        }
    }
}