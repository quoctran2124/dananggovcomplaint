using System;
using MvvmCross.Binding.Droid.Target;
using Android.Views;
using Android.Graphics.Drawables;

namespace GovernmentComplaint.Droid.Controls
{
    public class ViewBackgroundTargetBinding : MvxAndroidTargetBinding
    {
        public ViewBackgroundTargetBinding(object target)
            : base(target)
        {
        }

        public override Type TargetType
        {
            get { return typeof(string); }
        }

        protected override void SetValueImpl(object target, object value)
        {
        }

        public override void SetValue(object value)
        {
            var view = Target as View;

            if (view == null)
            {
                return;
            }

            view.Background = (Drawable)value;
        }
    }
}