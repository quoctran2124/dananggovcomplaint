using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Text;
using Android.Util;
using Android.Widget;

namespace GovernmentComplaint.Droid.Controls
{
    public class CustomTextView : TextView
    {
        public CustomTextView(Context context) : base(context)
        {
        }

        public CustomTextView(Context context, IAttributeSet attrs) : base(context, attrs)
        {
        }

        public string HtmlText
        {
            set
            {
                if (Build.VERSION.SdkInt >= BuildVersionCodes.N)
                {
                    SetText(Html.FromHtml(value, FromHtmlOptions.ModeLegacy), BufferType.Normal);
                }
                else
                {
                    SetText(Html.FromHtml(value), BufferType.Normal);
                }
                    
                SetTypeface(Typeface.SansSerif, TypefaceStyle.Normal);
            }
        }
    }
}