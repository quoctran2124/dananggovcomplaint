using Android.Content;
using Android.Graphics;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace GovernmentComplaint.Droid.Controls
{
	public class AlphaImageView : ImageView
	{
		public AlphaImageView(Context context)
			: base(context)
		{
		}

		public AlphaImageView(Context context, IAttributeSet attrs)
			: base(context, attrs)
		{
		}

		public override bool OnTouchEvent(MotionEvent e)
		{
			if (Clickable && Enabled)
			{
				switch (e.Action)
				{
					case MotionEventActions.Down:
						Alpha = 0.5f;
						break;
					case MotionEventActions.Up:
					case MotionEventActions.Cancel:
						Alpha = 1f;
						break;
					default:
						break;
				}
			}

			return base.OnTouchEvent(e);
		}

		public override bool Enabled
		{
			get
			{
				return base.Enabled;
			}
			set
			{
				base.Enabled = value;

				if (value)
				{
					Alpha = 1;
				}
				else
				{
					Alpha = 0.5f;
				}
			}
		}
    }
}