using Android.Content;
using Android.Util;
using Android.Views;
using MvvmCross.Binding.Droid.Views;

namespace GovernmentComplaint.Droid.Controls
{
	public sealed class CustomListView : MvxListView
	{
		private readonly int _maximumVisibleItem;
		private int _itemHeight;
		private int _listViewHeight;

		public CustomListView(Context context, IAttributeSet attrs) : base(context, attrs)
		{
			var typeArray = context.ObtainStyledAttributes(attrs, Resource.Styleable.custom_list_view);
			_maximumVisibleItem = typeArray.GetInteger(Resource.Styleable.custom_list_view_maxVisibleItems, 0);
			ViewTreeObserver.AddOnGlobalLayoutListener(this);
		}

		public override void OnGlobalLayout()
		{
			base.OnGlobalLayout();
			UpdateLayout();
		}

		public void UpdateHeight(bool isListCollapsed)
		{
			var layoutParams = LayoutParameters;

			if (isListCollapsed)
			{
				layoutParams.Height = 0;
			}
			else
			{
				UpdateItemHeight();
				layoutParams.Height = GetTotalListViewHeight(Adapter.Count);
			}

			LayoutParameters = layoutParams;
		}

		private void UpdateLayout()
		{
			if (LayoutParameters == null)
			{
				return;
			}

			var layoutParams = LayoutParameters;

			if (Adapter != null && Adapter.Count > 0)
			{
				var numOfItem = Adapter.Count;

				if (_itemHeight == 0)
				{
					UpdateItemHeight();
				}

				if ((_maximumVisibleItem > 0) && (numOfItem > _maximumVisibleItem))
				{
					numOfItem = _maximumVisibleItem;
				}

				var totalListViewHeight = GetTotalListViewHeight(numOfItem);

				if (totalListViewHeight == _listViewHeight)
				{
					return;
				}

				layoutParams.Height = totalListViewHeight;
			}
			else
			{
				layoutParams.Height = 0;
			}

			_listViewHeight = layoutParams.Height;
			LayoutParameters = layoutParams;
		}

		private void UpdateItemHeight()
		{
			if (Adapter.Count > 0)
			{
				var itemView = Adapter.GetView(0, null, this);

				if (itemView != null)
				{
					itemView.Measure(MeasureSpec.MakeMeasureSpec(0, MeasureSpecMode.Unspecified),
						MeasureSpec.MakeMeasureSpec(0, MeasureSpecMode.Unspecified));

					_itemHeight = itemView.MeasuredHeight;
				}
			}
		}

		private int GetTotalListViewHeight(int visibleItemNumber)
		{
			return visibleItemNumber * _itemHeight + (_maximumVisibleItem - 2) * DividerHeight;
		}
	}
}