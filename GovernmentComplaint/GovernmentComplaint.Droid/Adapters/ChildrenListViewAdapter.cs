using Android.Content;
using Android.Views;
using Android.Widget;
using GovernmentComplaint.Droid.Helpers;
using GovernmentComplaint.Droid.Listener;
using MvvmCross.Binding.Droid.BindingContext;
using MvvmCross.Binding.Droid.Views;

namespace GovernmentComplaint.Droid.Adapters
{
	public class ChildrenListViewAdapter : MvxAdapter
	{
		public ChildrenListViewAdapter(Context context, IMvxAndroidBindingContext bindingContext) : base(context, bindingContext)
		{

		}

		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			var view = base.GetView(position, convertView, parent);
			var commentEditText = view.FindViewById<EditText>(Resource.Id.CommentFrame);
			commentEditText.SetOnTouchListener(new MyTouchListener());

			var commentButton = view.FindViewById<Button>(Resource.Id.replyCommentButton);
			commentButton.Click += (sender, args) => KeyboardHelper.HideKeyboard(view);
			return view;
		}
	}
}