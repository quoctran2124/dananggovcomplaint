using Android.App;
using Android.Content;
using Android.Views;
using Android.Widget;
using GovernmentComplaint.Core.ViewModels.ItemViewModels;
using GovernmentComplaint.Droid.Controls;
using MvvmCross.Binding.Droid.BindingContext;
using MvvmCross.Binding.Droid.Views;

namespace GovernmentComplaint.Droid.Adapters
{
    public class FeedbackListAdapter : MvxAdapter
    {
        public FeedbackListAdapter(Context context) : base(context)
        {
        }

        public FeedbackListAdapter(Context context, IMvxAndroidBindingContext bindingContext)
            : base(context, bindingContext)
        {
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            if (convertView != null) return convertView;
            var inflater = Application.Context.GetSystemService(Context.LayoutInflaterService) as LayoutInflater;
            convertView = inflater?.Inflate(Resource.Layout.FeedbackItemView, null);
            var t = GetRawItem(position) as FeedbackItemViewModel;

            var imageView = convertView?.FindViewById<CustomImageView>(Resource.Id.image);
            if (imageView != null) imageView.ImageUrl = t?.Image;

            var title = convertView?.FindViewById<TextView>(Resource.Id.title);
            if (title != null) title.Text = t?.Title;
            return convertView;
        }

        protected override View GetBindableView(View convertView, object source)
        {
            if (convertView == null)
                convertView = base.GetBindableView(null, source);
            else
                BindBindableView(source, convertView as IMvxListItemView);
            return convertView;
        }
    }
}