using Android.App;
using GovernmentComplaint.Core.Helpers;
using ME.Leolin.Shortcutbadger;

namespace GovernmentComplaint.Droid.Helpers
{
    public class BadgeHelper : IBadgeHelper
    {
        public void SetBadge(int badgeNumber)
        {
            ShortcutBadger.ApplyCount(Application.Context, badgeNumber);
        }

        public void ClearBadge()
        {
            ShortcutBadger.RemoveCount(Application.Context);
        }
    }
}