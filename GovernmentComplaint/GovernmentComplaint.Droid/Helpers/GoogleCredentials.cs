using Android.Gms.Common.Apis;

namespace GovernmentComplaint.Droid.Helpers
{
    public class GoogleCredentials
    {
        private static GoogleCredentials _mInstance;

        private GoogleCredentials()
        {
        }

        public GoogleApiClient GoogleApiClient { get; set; }

        public static GoogleCredentials Instance()
        {
            if (_mInstance == null)
                _mInstance = new GoogleCredentials();
            return _mInstance;
        }
    }
}