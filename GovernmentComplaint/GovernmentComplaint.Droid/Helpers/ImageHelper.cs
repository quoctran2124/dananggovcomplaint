using System;
using System.IO;
using Android.Graphics;
using GovernmentComplaint.Core;
using Uri = Android.Net.Uri;

namespace GovernmentComplaint.Droid.Helpers
{
    public class ImageHelper
    {
        public static int CalculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight)
        {
            // Raw height and width of image
            int height = options.OutHeight;
            int width = options.OutWidth;
            int inSampleSize = 1;

            if (height > reqHeight || width > reqWidth)
            {
                int resizedHeight = height / 2;
                int resizedWidth = width / 2;
                inSampleSize *= 2;

                // Calculate the largest inSampleSize value that is a power of 2 and keeps both
                // height and width larger than the requested height and width.
                while (resizedHeight >= reqHeight || resizedWidth >= reqWidth)
                {
                    inSampleSize *= 2;
                    resizedHeight = height / inSampleSize;
                    resizedWidth = width / inSampleSize;
                }
            }

            return inSampleSize;
        }

        public static Bitmap DecodeSampledBitmapFromFile(Uri uri, int reqWidth, int reqHeight)
        {
			Bitmap bitmap = null;
			Stream imgStream = FileHelper.GetImageStream(uri);

			var options = new BitmapFactory.Options
			{
				InJustDecodeBounds = true
			};

			// just decode bounds
			BitmapFactory.DecodeStream(imgStream, null, options);

			var originWidth = options.OutWidth;
			var originHeight = options.OutHeight;

			// have to reset stream, otherwise bitmap will be null after decoding
			imgStream.Close();

			imgStream = FileHelper.GetImageStream(uri);

			if (Math.Max(originWidth, originHeight) > ConstVariable.UploadImageMaxSize)
			{
				options.InSampleSize = CalculateInSampleSize(options, reqWidth, reqHeight);

				// Decode bitmap with inSampleSize set
				options.InJustDecodeBounds = false;

				bitmap = BitmapFactory.DecodeStream(imgStream, null, options);
			}
			else
			{
				bitmap = BitmapFactory.DecodeStream(imgStream);
			}

			return bitmap;
		}

    }
}