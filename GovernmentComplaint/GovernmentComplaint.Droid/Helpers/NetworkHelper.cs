using System;
using Android.App;
using Android.Content;
using Android.Net;
using GovernmentComplaint.Core.Helpers;

namespace GovernmentComplaint.Droid.Helpers
{
	public class NetworkHelper : INetworkHelper
	{
		public bool IsConnected
		{
			get
			{
				var connectivityManager =
					(ConnectivityManager) Application.Context.GetSystemService(Context.ConnectivityService);
				var networkInfo = connectivityManager.ActiveNetworkInfo;
				return networkInfo != null && networkInfo.IsConnectedOrConnecting;
			}
		}

		public EventHandler NetworkConnected { get; set; }
	}
}