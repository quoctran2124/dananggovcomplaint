using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using Android.App;
using Android.OS;
using Android.Provider;

namespace GovernmentComplaint.Droid.Helpers
{
    public class FileHelper
    {
		private const string ContentUriPattern = @"content://";
		private const string FileUriPattern = @"file://";

		public static string GetImagePath(Android.Net.Uri contentUri)
		{
			if (contentUri == null)
			{
				return string.Empty;
			}

			var returnPath = string.Empty;
			var url = contentUri.ToString();

			if (Regex.IsMatch(url, ContentUriPattern))
			{
				returnPath = GetPathFromImageLibrary(contentUri);
			}
			else if (Regex.IsMatch(url, FileUriPattern))
			{
				returnPath = url.Remove(0, FileUriPattern.Length);
			}

			return returnPath;
		}

		public static Stream GetImageStream(Android.Net.Uri contentUri)
		{
			try
			{
				return Application.Context.ContentResolver.OpenInputStream(contentUri);
			}
			catch (Exception exception)
			{
				Console.WriteLine(exception);
			}
			return Stream.Null;
		}

		public static ParcelFileDescriptor GetFileDescription(Android.Net.Uri contentUri)
		{
			try
			{
				return Application.Context.ContentResolver.OpenFileDescriptor(contentUri, "r");
			}
			catch
			{
				return null;
			}
		}

		private static string GetPathFromImageLibrary(Android.Net.Uri contentUri)
		{
			try
			{
				var cursor = Application.Context.ContentResolver.Query(contentUri, null, null, null, null);
				cursor.MoveToFirst();
				var documentId = cursor.GetString(0);
				documentId = documentId.Contains(':') ? documentId.Split(':')[1] : documentId;
				cursor.Close();

				cursor = Application.Context.ContentResolver.Query(
					MediaStore.Images.Media.ExternalContentUri,
					null,
					MediaStore.Images.Media.InterfaceConsts.Id + " = ? ",
					new[] { documentId }, null);

				cursor.MoveToFirst();
				var path = cursor.GetString(cursor.GetColumnIndex(MediaStore.Images.Media.InterfaceConsts.Data));
				cursor.Close();
				return path;
			}
			catch (Exception exception)
			{
				Console.WriteLine(exception);
			}

			return string.Empty;
		}
	}
}