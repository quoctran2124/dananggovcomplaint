using Android.App;
using GovernmentComplaint.Core.Helpers;

namespace GovernmentComplaint.Droid.Helpers
{
	public class VersionHelper : IVersionHelper
	{
		public string GetVersion()
		{
			return Application.Context.PackageManager.GetPackageInfo(Application.Context.PackageName, 0).VersionName;
		}
	}
}