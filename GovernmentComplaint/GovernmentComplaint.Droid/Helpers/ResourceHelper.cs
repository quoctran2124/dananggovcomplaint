using System;
using System.IO;
using GovernmentComplaint.Core.Helpers;

namespace GovernmentComplaint.Droid.Helpers
{
	public class ResourceHelper : IResourceHelper
	{
		public string IconPath(string icon)
		{
            return icon.ToLower();
		}
	}
}