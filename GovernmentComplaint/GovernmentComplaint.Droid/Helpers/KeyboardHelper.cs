using Android.Content;
using Android.Views;
using Android.Views.InputMethods;

namespace GovernmentComplaint.Droid.Helpers
{
	public static class KeyboardHelper
	{
        public static bool isKeyBoardShow;
		public static void HideKeyboard(View view)
		{
			if (view == null)
			{
                isKeyBoardShow = true;
				return;
			}

			var inputMethodManager = (InputMethodManager)Android.App.Application.Context.GetSystemService(Context.InputMethodService);
			inputMethodManager.HideSoftInputFromWindow(view.WindowToken, 0);
		}

		public static void ShowKeyboard(View view)
		{
			if (view == null)
			{
                isKeyBoardShow = true;
				return;
			}

			var inputMethodManager = (InputMethodManager)Android.App.Application.Context.GetSystemService(Context.InputMethodService);
			inputMethodManager.ShowSoftInput(view, ShowFlags.Implicit);
		}
	}
}