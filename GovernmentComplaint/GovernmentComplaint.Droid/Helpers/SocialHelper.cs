using Android.Gms.Auth.Api;
using Android.Gms.Common.Apis;
using GovernmentComplaint.Core.Helpers;
using GovernmentComplaint.Droid.Views.Bases;
using Xamarin.Facebook.Login;

namespace GovernmentComplaint.Droid.Helpers
{
	public class SocialHelper : ISocialHelper
	{
		public void LogoutFacebook()
		{
			LoginManager.Instance.LogOut();
		}

		public void LogoutGoogle()
		{
			var apiClient = MasterView.GoogleApiClient;
			if (apiClient != null && apiClient.IsConnected)
			{
				Auth.GoogleSignInApi.SignOut(apiClient);
			}
		}
	}
}