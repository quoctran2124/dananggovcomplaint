using Android.Content;
using Android.Graphics;
using Android.Support.V4.Content;

namespace GovernmentComplaint.Droid.Helpers
{
	public static class ColorHelper
	{
		public static Color Transparent { get; private set; }
		public static Color DarkBlue { get; private set; }
		public static Color LightGray { get; private set; }
		public static Color Orange { get; private set; }
		public static Color Black { get; private set; }
		public static Color NeutralGray { get; private set; }
		public static Color White { get; private set; }
		public static Color Red { get; private set; }

		public static void InitColors(Context context)
		{
			DarkBlue = GetColorFromResource(context, Resource.Color.DarkBlue);
			LightGray = GetColorFromResource(context, Resource.Color.LightGray);
			Transparent = new Color(0, 0, 0, 0);
			Black = GetColorFromResource(context, Resource.Color.Black);
			Orange = GetColorFromResource(context, Resource.Color.Orange);
			NeutralGray = GetColorFromResource(context, Resource.Color.NeutralGray);
			White = GetColorFromResource(context, Resource.Color.White);
			Red = GetColorFromResource(context, Resource.Color.Red);
		}

		private static Color GetColorFromResource(Context context, int id)
		{
			var color = ContextCompat.GetColor(context, id);
			return new Color(color);
		}
	}
}