using Android.App;
using Android.Content.Res;
using Android.Graphics;

namespace GovernmentComplaint.Droid.Helpers
{
	public static class ResolutionHelper
	{
		private const int DeviceSizeThreshold = 600;
		public static int Width { get; private set; }
		public static int Height { get; private set; }
		public static int NavigationHeight { get; private set; }
		public static int StatusHeight { get; private set; }
		public static bool IsTablet { get; private set; }
		public static float Density { get; private set; }
		public static bool IsPortrait { get; private set; }

		public static void InitStaticVariable(Activity activity)
		{
			Density = activity.Resources.DisplayMetrics.Density;

			var statusBarId = activity.Resources.GetIdentifier("status_bar_height", "dimen", "android");
			var navigationBarId = activity.Resources.GetIdentifier("navigation_bar_height", "dimen", "android");
			NavigationHeight = navigationBarId > 0 ? activity.Resources.GetDimensionPixelSize(navigationBarId) : 0;
			StatusHeight = statusBarId > 0 ? activity.Resources.GetDimensionPixelSize(statusBarId) : 0;

			RefreshStaticVariable(activity);

			var minValue = Width < Height ? Width : Height;

			if (activity.Resources.Configuration.Orientation == Orientation.Landscape)
			{
				minValue += NavigationHeight;
			}

			IsTablet = minValue >= ToPixel(DeviceSizeThreshold);
		}

		public static void RefreshStaticVariable(Activity activity)
		{
			var display = activity.WindowManager.DefaultDisplay;
			var size = new Point();
			display.GetSize(size);
			Width = size.X;
			Height = size.Y;

			IsPortrait = Height > Width;
		}

		public static int ToPixel(float dp)
		{
			return (int)(dp * Density + 0.5);
		}
	}
}