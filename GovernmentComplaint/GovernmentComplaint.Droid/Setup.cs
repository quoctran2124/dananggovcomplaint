using Android.Content;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform.Platform;
using MvvmCross.Platform;
using GovernmentComplaint.Core.Helpers;
using GovernmentComplaint.Droid.Helpers;
using MvvmCross.Droid.Views;
using GovernmentComplaint.Droid.Views.Bases;
using System.Collections.Generic;
using MvvmCross.Binding.Bindings.Target.Construction;
using Android.Views;
using GovernmentComplaint.Droid.Controls;
using MvvmCross.Droid.Platform;

namespace GovernmentComplaint.Droid
{
	public class Setup : MvxAndroidSetup
	{

		public Setup(Context applicationContext) : base(applicationContext)
		{
		}

		protected override IMvxApplication CreateApp()
		{
			return new Core.App();
		}

		protected override IMvxTrace CreateDebugTrace()
		{
			return new DebugTrace();
		}

		protected override void InitializeFirstChance()
		{
			Mvx.RegisterType<IResourceHelper, ResourceHelper>();
			Mvx.RegisterType<ISocialHelper, SocialHelper>();
			Mvx.RegisterType<INetworkHelper, NetworkHelper>();
			Mvx.RegisterType<ITimer, AndroidTimer>();
			Mvx.RegisterType<IBadgeHelper, BadgeHelper>();
			Mvx.RegisterType<IVersionHelper, VersionHelper>();
		}

		protected override IDictionary<string, string> ViewNamespaceAbbreviations
		{
			get
			{
				var abbreviations = base.ViewNamespaceAbbreviations;
				abbreviations["dc"] = "GovernmentComplaint.Droid.Controls";
				return abbreviations;
			}
		}

		protected override IMvxAndroidViewPresenter CreateViewPresenter()
		{
			var presenter = Mvx.IocConstruct<DroidPresenter>();
			Mvx.RegisterSingleton<IMvxAndroidViewPresenter>(presenter);
			return presenter;
		}

		protected override void InitializeIoC()
		{
			base.InitializeIoC();

			Mvx.ConstructAndRegisterSingleton<IFragmentTypeLookup, FragmentTypeLookup>();
		}

		protected override void FillTargetFactories(IMvxTargetBindingFactoryRegistry registry)
		{
			registry.RegisterCustomBindingFactory<View>("Background", binary => new ViewBackgroundTargetBinding(binary));
			base.FillTargetFactories(registry);
		}
	}
}
