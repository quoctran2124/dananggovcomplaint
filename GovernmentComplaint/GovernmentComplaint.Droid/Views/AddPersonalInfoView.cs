using System;
using Android.Graphics;
using Android.Views;
using Android.Widget;
using GovernmentComplaint.Core.ViewModels;
using GovernmentComplaint.Droid.Helpers;
using GovernmentComplaint.Droid.Views.Bases;
using MvvmCross.Binding.BindingContext;

namespace GovernmentComplaint.Droid.Views
{
	public class AddPersonalInfoView : DetailView
	{
		protected override int LayoutId => Resource.Layout.AddPersonalInfoView;
		private EditText _fullname, _email, _phone;

		protected override void CreateBinding()
		{
			base.CreateBinding();

			var bindingSet = this.CreateBindingSet<AddPersonalInfoView, AddPersonalInfoViewModel>();

			bindingSet.Bind(this).For(v => v.IsKeyboardShown).To(vm => vm.IsKeyboardShown);

			bindingSet.Bind(this).For(v => v.IsUserNameNotAvailable).To(vm => vm.IsUserNameNotAvailable);

			bindingSet.Bind(this).For(v => v.IsUserEmailNotAvailable).To(vm => vm.IsUserEmailNotAvailable);

			bindingSet.Apply();
		}

		protected override void InitView(View view)
		{
			_view = view;
			_fullname = view.FindViewById<EditText>(Resource.Id.FullName);
			_email = view.FindViewById<EditText>(Resource.Id.Email);
			_phone = view.FindViewById<EditText>(Resource.Id.Phone);
			view.Click += (sender, args) => KeyboardHelper.HideKeyboard(view);
		}

		private void PhoneOnKeyPress(object sender, View.KeyEventArgs keyEventArgs)
		{
			keyEventArgs.Handled = false;
			if (keyEventArgs.Event.Action != KeyEventActions.Down || keyEventArgs.KeyCode != Keycode.Enter) return;
			KeyboardHelper.HideKeyboard(_view);
			keyEventArgs.Handled = true;
		}

		private void EmailOnKeyPress(object sender, View.KeyEventArgs keyEventArgs)
		{
			keyEventArgs.Handled = false;
			if (keyEventArgs.Event.Action != KeyEventActions.Down || keyEventArgs.KeyCode != Keycode.Enter) return;

			_phone.RequestFocus();
			keyEventArgs.Handled = true;
		}

		private void FullnameOnKeyPress(object sender, View.KeyEventArgs keyEventArgs)
		{
			keyEventArgs.Handled = false;
			if (keyEventArgs.Event.Action != KeyEventActions.Down || keyEventArgs.KeyCode != Keycode.Enter) return;

			_email.RequestFocus();
			keyEventArgs.Handled = true;
		}

		#region Properties
		private View _view;
		private bool _isUserNameNotAvailable;

		public bool IsUserNameNotAvailable
		{
			get { return _isUserNameNotAvailable; }
			set
			{
				_isUserNameNotAvailable = value;
				_fullname.Enabled = value;
				_fullname.SetBackgroundResource(value ? Resource.Drawable.new_complaint_background : Resource.Drawable.new_complaint_dark_background);
			}
		}

		private bool _isUserEmailNotAvailable;

		public bool IsUserEmailNotAvailable
		{
			get { return _isUserEmailNotAvailable; }
			set
			{
				_isUserEmailNotAvailable = value;
				_email.Enabled = value;
				_email.SetBackgroundResource(value ? Resource.Drawable.new_complaint_background : Resource.Drawable.new_complaint_dark_background);
			}
		}

		private bool _isKeyboardShown;

		public bool IsKeyboardShown
		{
			get { return _isKeyboardShown; }
			set
			{
				_isKeyboardShown = value;
				if (!value)
				{
					KeyboardHelper.HideKeyboard(_view);
				}
			}
		}

		public override void OnDestroy()
		{
			base.OnDestroy();
			_fullname.KeyPress -= FullnameOnKeyPress;
			_email.KeyPress -= EmailOnKeyPress;
			_phone.KeyPress -= PhoneOnKeyPress;
		}

		#endregion

	}
}