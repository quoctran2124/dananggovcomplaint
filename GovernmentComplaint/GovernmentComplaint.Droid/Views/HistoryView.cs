using System.Windows.Input;
using Android.Views;
using GovernmentComplaint.Core.ViewModels;
using GovernmentComplaint.Droid.Controls;
using GovernmentComplaint.Droid.Views.Bases;
using MvvmCross.Binding.BindingContext;

namespace GovernmentComplaint.Droid.Views
{
    public class HistoryView : DetailView
    {
        private AutoloadingListView _autoLoadingList;
        public ICommand CheckNetworkCommand { get; set; }
        protected override int LayoutId => Resource.Layout.HistoryView;
        private int _lastComplaintItemPosition;

        public int LastComplaintItemPosition
        {
            get { return _lastComplaintItemPosition; }
            set
            {
                _lastComplaintItemPosition = value;

                if (_lastComplaintItemPosition != 0)
                    _autoLoadingList.SetSelection(_lastComplaintItemPosition);
            }
        }

        private bool _moveListToTop;

        public bool MoveListToTop
        {
            get { return _moveListToTop; }
            set
            {
                if (value)
                {
                    _autoLoadingList.SetSelectionAfterHeaderView();
                }
                _moveListToTop = value;
            }
        }

        protected override void CreateBinding()
        {
            base.CreateBinding();
            var bindingSet = this.CreateBindingSet<HistoryView, HistoryViewModel>();
            bindingSet.Bind(_autoLoadingList)
                .For(v => v.LoadMoreData)
                .To(vm => vm.LoadMoreData);
            bindingSet.Bind(_autoLoadingList)
                .For(v => v.LoadingMore)
                .To(vm => vm.LoadingMore);
            bindingSet.Bind(this)
                .For(v => v.CheckNetworkCommand)
                .To(vm => vm.CheckNetworkCommand);
            bindingSet.Bind(this)
                .For(v => v.LastComplaintItemPosition)
                .To(vm => vm.LastComplaintItemPosition);
            bindingSet.Bind(this)
                .For(v => v.MoveListToTop)
                .To(vm => vm.MoveListToTop);
            bindingSet.Apply();
        }

        protected override void InitView(View view)
        {
            _autoLoadingList = view.FindViewById<AutoloadingListView>(Resource.Id.HistoryList);
        }

        public override void OnResume()
        {
            base.OnResume();
            CheckNetworkCommand?.Execute(null);
        }
    }
}