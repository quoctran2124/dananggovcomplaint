using System;
using System.IO;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Views;
using Android.Widget;
using GovernmentComplaint.Core;
using GovernmentComplaint.Core.ViewModels;
using GovernmentComplaint.Droid.Controls;
using GovernmentComplaint.Droid.Helpers;
using GovernmentComplaint.Droid.Listener;
using GovernmentComplaint.Droid.Views.Bases;
using MvvmCross.Binding.BindingContext;

namespace GovernmentComplaint.Droid.Views
{
	public class CreateNewComplaintView : DetailView, ViewTreeObserver.IOnGlobalLayoutListener
	{
		public const int FileSelectCode = 69;
		private LinearLayout _attachLayout;
		private CustomEditText _locationText, _contentText;
		private ImageView[] _lstAttachs, _lstRemoveAttachs;
		private Bitmap[] _lstBitmaps;
		private RelativeLayout[] _lstIndicators;

		private int _mExtraScreenHeight = -1, _mKeyboardHeight = -1, _mKeypadHeight;
		private TextView _noAttachTextView;
		private AlphaImageView _removeVideoLink;
		private RelativeLayout _videoLinkLayout;
		private View _view;
		protected override int LayoutId => Resource.Layout.CreateNewComplaintView;

		public void OnGlobalLayout()
		{
			var rect = new Rect();
			View.GetWindowVisibleDisplayFrame(rect);
			var rootViewHeight = View.RootView.Height;
			var visibleDisplayFrameHeight = rect.Height();
			var fakeHeight = rootViewHeight - visibleDisplayFrameHeight;

			if (_mExtraScreenHeight == -1)
			{
				_mExtraScreenHeight = fakeHeight;
			}
			else if (fakeHeight <= _mExtraScreenHeight)
			{
				_mExtraScreenHeight = fakeHeight;
			}
			else if (fakeHeight > _mExtraScreenHeight)
			{
				_mKeypadHeight = fakeHeight - _mExtraScreenHeight;
				View.ViewTreeObserver.RemoveOnGlobalLayoutListener(this);
			}
		}

		protected override void Dispose(bool disposing)
		{
			base.Dispose(disposing);
			_locationText.KeyPress -= _locationText_KeyPress;
		}

		#region Properties

		public event EventHandler AttachmentStreamsValueChanged;

		private MemoryStream[] _attachmentStreams;

		public MemoryStream[] AttachmentStreamsValue
		{
			get { return _attachmentStreams; }
			set
			{
				_attachmentStreams = value;
				AttachmentStreamsValueChanged?.Invoke(this, null);
			}
		}

		public event EventHandler VideoLinkValueChanged;

		private string _videoLink;

		public string VideoLinkValue
		{
			get { return _videoLink; }
			set
			{
				_videoLink = value;
				VideoLinkValueChanged?.Invoke(this, null);
			}
		}

		private int _attachAmount;

		private int AttachAmountValue
		{
			get { return _attachAmount; }
			set
			{
				_attachAmount = value;
				if (value != 0)
				{
					_noAttachTextView.Visibility = ViewStates.Gone;
					_attachLayout.Visibility = ViewStates.Visible;
					_lstRemoveAttachs[AttachAmountValue - 1].Visibility = ViewStates.Visible;
				}
				else
				{
					_attachLayout.Visibility = ViewStates.Gone;
					if (_videoLinkLayout.Visibility == ViewStates.Gone ||
						_videoLinkLayout.Visibility == ViewStates.Invisible)
						_noAttachTextView.Visibility = ViewStates.Visible;
				}
			}
		}

		private bool _isVideoLayoutShown;

		public bool IsVideoLayoutShown
		{
			get { return _isVideoLayoutShown; }
			set
			{
				_isVideoLayoutShown = value;
				if (value)
					_videoLinkLayout.Visibility = ViewStates.Visible;
			}
		}

		private bool _isImagePickerShown;

		public bool IsImagePickerShown
		{
			get { return _isImagePickerShown; }
			set
			{
				_isImagePickerShown = value;
				if (value)
					OpenFileExplorer(this, null);
			}
		}

		#endregion

		#region Methods

		protected override void InitView(View view)
		{
			_view = view;
			base.InitView(view);
			_videoLinkLayout = (RelativeLayout) view.FindViewById(Resource.Id.videoLinkLayout);
			_videoLinkLayout.Visibility = ViewStates.Invisible;

			_removeVideoLink = (AlphaImageView) view.FindViewById(Resource.Id.removeIcon);
			_removeVideoLink.Click += OnRemoveVideoClick;
			_noAttachTextView = (TextView) view.FindViewById(Resource.Id.noAttachedImage);
			_attachLayout = (LinearLayout) view.FindViewById(Resource.Id.imagesChose);

			_lstAttachs = new[]
			{
				(ImageView) view.FindViewById(Resource.Id.AttachedImage1),
				(ImageView) view.FindViewById(Resource.Id.AttachedImage2),
				(ImageView) view.FindViewById(Resource.Id.AttachedImage3)
			};

			_lstRemoveAttachs = new[]
			{
				(ImageView) view.FindViewById(Resource.Id.removeAttachedImage1),
				(ImageView) view.FindViewById(Resource.Id.removeAttachedImage2),
				(ImageView) view.FindViewById(Resource.Id.removeAttachedImage3)
			};

			_lstRemoveAttachs[0].Visibility =
				_lstRemoveAttachs[1].Visibility = _lstRemoveAttachs[2].Visibility = ViewStates.Invisible;

			_lstRemoveAttachs[0].Click += Remove1OnClick;
			_lstRemoveAttachs[1].Click += Remove2OnClick;
			_lstRemoveAttachs[2].Click += Remove3OnClick;

			_lstBitmaps = new Bitmap[3];

			_lstIndicators = new[]
			{
				view.FindViewById<RelativeLayout>(Resource.Id.indicator1),
				view.FindViewById<RelativeLayout>(Resource.Id.indicator2),
				view.FindViewById<RelativeLayout>(Resource.Id.indicator3)
			};

			_locationText = (CustomEditText) view.FindViewById(Resource.Id.LocationText);
			_locationText.KeyPress += _locationText_KeyPress;

			_contentText = (CustomEditText) view.FindViewById(Resource.Id.ComplaintContent);
			_contentText.SetOnTouchListener(new MyTouchListener());
			_contentText.SetSelectAllOnFocus(false);
		}
		

		private void _locationText_KeyPress(object sender, View.KeyEventArgs args)
		{
			args.Handled = false;
			if (args.Event.Action != KeyEventActions.Down || args.KeyCode != Keycode.Enter) return;
			KeyboardHelper.HideKeyboard(_view);
			args.Handled = true;
		}

		private void OnRemoveVideoClick(object sender, EventArgs e)
		{
			_videoLinkLayout.Visibility = ViewStates.Gone;
			VideoLinkValue = string.Empty;
			if (AttachAmountValue == 0)
				_noAttachTextView.Visibility = ViewStates.Visible;
		}

		private void RearrangeImage(int index)
		{
			AttachAmountValue--;

			var length = _lstBitmaps.Length;
			for (var i = index; i < length - 1; i++)
				if (_lstBitmaps[i + 1] != null)
				{
					AttachmentStreamsValue[i] = AttachmentStreamsValue[i + 1];
					_lstBitmaps[i] = _lstBitmaps[i + 1];
					_lstAttachs[i].SetImageBitmap(null);
					_lstAttachs[i].SetImageBitmap(_lstBitmaps[i]);
				}
				else
				{
					DestroyImage(i);
					return;
				}
			DestroyImage(length - 1);
		}

		private void DestroyImage(int index)
		{
			AttachmentStreamsValue[index] = null;
			_lstBitmaps[index] = null;
			_lstAttachs[index].SetImageBitmap(null);
			_lstRemoveAttachs[index].Visibility = ViewStates.Invisible;
		}

		private void OpenFileExplorer(object sender, EventArgs e)
		{
			var intent = new Intent(Intent.ActionGetContent);
			intent.SetType("image/*");
			try
			{
				StartActivityForResult(Intent.CreateChooser(intent, "Select attachment"),
					FileSelectCode);
			}
			catch (ActivityNotFoundException)
			{
				Toast.MakeText(Application.Context, "Install file manager", ToastLength.Long).Show();
			}
		}

		private bool _isKeyboardHidden;

		public bool IsKeyboardHidden
		{
			get { return _isKeyboardHidden; }
			set
			{
				if (value)
				{
					KeyboardHelper.HideKeyboard(View);
				}
			}
		}

		protected override void CreateBinding()
		{
			base.CreateBinding();
			var bindingSet = this.CreateBindingSet<CreateNewComplaintView, CreateNewComplaintViewModel>();
			bindingSet.Bind(this).For(v => v.AttachmentStreamsValue).To(vm => vm.AttachmentStreamsValue).TwoWay();
			bindingSet.Bind(this).For(v => v.IsVideoLayoutShown).To(vm => vm.IsVideoLayoutShown).TwoWay();
			bindingSet.Bind(this).For(v => v.IsImagePickerShown).To(vm => vm.IsImagePickerShown);
			bindingSet.Bind(this).For(v => v.VideoLinkValue).To(vm => vm.VideoLinkValue).TwoWay();
			bindingSet.Bind(this).For(v => v.IsKeyboardHidden).To(vm => vm.IsKeyboardHidden);
			bindingSet.Apply();
		}

		#endregion

		#region Event handlers

		public override void OnActivityResult(int requestCode, Result resultCode, Intent data)
		{
			base.OnActivityResult(requestCode, resultCode, data);

			if (requestCode != FileSelectCode) return;
			if (resultCode != Result.Ok) return;
			var uri = data.Data;
			//var imageFileDescription = FileHelper.GetFileDescription(uri);
			//if (imageFileDescription != null)
			//{
			//	if (imageFileDescription.StatSize > ConstVariable.MaxImageSizeBytes)
			//	{
			//		return;
			//	}
			//}
			//var path = FileHelper.GetImagePath(uri);
			_lstBitmaps[AttachAmountValue] = ImageHelper.DecodeSampledBitmapFromFile(uri,
				ConstVariable.UploadImageMaxSize,
				ConstVariable.UploadImageMaxSize);

			AttachmentStreamsValue[AttachAmountValue] = null;
			AttachmentStreamsValue[AttachAmountValue] = new MemoryStream();
			_lstBitmaps[AttachAmountValue].Compress(Bitmap.CompressFormat.Jpeg, 50,
				AttachmentStreamsValue[AttachAmountValue]);
			_lstAttachs[AttachAmountValue].SetImageBitmap(_lstBitmaps[AttachAmountValue]);
			ShowIndicator(_lstIndicators[AttachAmountValue]);
			AttachAmountValue++;
		}

		private void Remove3OnClick(object sender, EventArgs eventArgs)
		{
			RearrangeImage(2);
		}

		private void Remove2OnClick(object sender, EventArgs eventArgs)
		{
			RearrangeImage(1);
		}

		private void Remove1OnClick(object sender, EventArgs eventArgs)
		{
			RearrangeImage(0);
		}

		private async void ShowIndicator(View indicator)
		{
			indicator.Visibility = ViewStates.Visible;
			await Task.Delay(ConstVariable.TimeToIndicatorLoadImageShow);
			indicator.Visibility = ViewStates.Gone;
		}

		#endregion
	}
}