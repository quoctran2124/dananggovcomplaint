using System.Windows.Input;
using Android.App;
using Android.Views;
using Android.Widget;
using GovernmentComplaint.Core.ViewModels;
using GovernmentComplaint.Droid.Controls;
using GovernmentComplaint.Droid.Views.Bases;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Binding.Droid.Views;
namespace GovernmentComplaint.Droid.Views
{
    public class HomeView : DetailView
    {
        private AutoloadingListView _autoLoadingList;

        private int _lastComplaintItemPosition;
        protected override int LayoutId => Resource.Layout.HomeView;

        public ICommand CheckNetworkCommand { get; set; }

        public int LastComplaintItemPosition
        {
            get { return _lastComplaintItemPosition; }
            set
            {
                _lastComplaintItemPosition = value;

                if (_lastComplaintItemPosition != 0)
                    _autoLoadingList.SetSelection(_lastComplaintItemPosition);
            }
        }

        private bool _moveListToTop;

        public bool MoveListToTop
        {
            get { return _moveListToTop; }
            set
            {
                if (value)
                {
                    _autoLoadingList.SetSelectionAfterHeaderView();
                }
            }
        }

        protected override void InitView(View view)
        {
            base.InitView(view);
            _autoLoadingList = view.FindViewById<AutoloadingListView>(Resource.Id.feedback);
        }

        public override void OnResume()
        {
            base.OnResume();
            CheckNetworkCommand?.Execute(null);
        }

	    protected override void CreateBinding()
        {
            base.CreateBinding();
            var bindingSet = this.CreateBindingSet<HomeView, HomeViewModel>();
            bindingSet.Bind(this)
                .For(v => v.LastComplaintItemPosition)
                .To(vm => vm.LastComplaintItemPosition);
            bindingSet.Bind(this)
                .For(v => v.CheckNetworkCommand)
                .To(vm => vm.CheckNetworkCommand);
            bindingSet.Bind(_autoLoadingList)
                .For(v => v.LoadMoreData)
                .To(vm => vm.LoadMoreData);
            bindingSet.Bind(_autoLoadingList)
                .For(v => v.LoadingMore)
                .To(vm => vm.LoadingMore);
            bindingSet.Bind(this)
                .For(v => v.MoveListToTop)
                .To(vm => vm.MoveListToTop);
            bindingSet.Apply();
        }
    }
}