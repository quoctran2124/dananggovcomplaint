using System.Windows.Input;
using Android.App;
using Android.Content;
using Android.Gms.Auth.Api;
using Android.Gms.Auth.Api.SignIn;
using Android.Gms.Common.Apis;
using Android.OS;
using Android.Views;
using GovernmentComplaint.Core.Enums;
using GovernmentComplaint.Core.Models;
using GovernmentComplaint.Core.ViewModels;
using GovernmentComplaint.Droid.Controls;
using GovernmentComplaint.Droid.Views.Bases;
using MvvmCross.Binding.BindingContext;
using Org.Json;
using Xamarin.Facebook;
using Xamarin.Facebook.Login.Widget;

namespace GovernmentComplaint.Droid.Views
{
	public class SecondLoginScreenView : DetailView, IFacebookCallback, GraphRequest.IGraphJSONObjectCallback

	{
		protected override int LayoutId => Resource.Layout.SecondLoginScreenView;
		private static int RC_SIGN_IN = 9002;


		public override void OnResume()
		{
			base.OnResume();
			CheckNetworkCommand?.Execute(null);
		}

		protected override void InitView(View view)
		{
			var facebookLoginButton = view.FindViewById<LoginButton>(Resource.Id.fbLoginButton);
			_facebookSignInView = view.FindViewById<AlphaRelativeLayout>(Resource.Id.fbSignInLayout);
			facebookLoginButton.RegisterCallback(MasterView.CallbackManager, this);
			_facebookSignInView.Click += (s, e) => { facebookLoginButton.PerformClick(); };

			if (MasterView.GoogleApiClient != null)
			{
				_mGoogleApiClient = MasterView.GoogleApiClient;
			}
			var googleSignInView = view.FindViewById<AlphaRelativeLayout>(Resource.Id.GoogleLogin);
			googleSignInView.Click += (s, e) =>
			{
				var signInIntent = Auth.GoogleSignInApi.GetSignInIntent(_mGoogleApiClient);
				StartActivityForResult(signInIntent, RC_SIGN_IN);
			};
		}

		public override void OnCreate(Bundle bundle)
		{
			base.OnCreate(bundle);
		}

		public override void OnActivityResult(int requestCode, Result resultCode, Intent data)
		{
			base.OnActivityResult(requestCode, resultCode, data);
			MasterView.CallbackManager.OnActivityResult(requestCode, (int) resultCode, data);
			if (requestCode == RC_SIGN_IN)
			{
				var result = Auth.GoogleSignInApi.GetSignInResultFromIntent(data);
				HandleSignInResult(result);
			}
		}

		private void HandleSignInResult(GoogleSignInResult result)
		{
			if (result.IsSuccess)
			{
				// Signed in successfully, show authenticated UI.
				var account = result.SignInAccount;
				var user = new UserInfo
				{
					AccountType = AccountType.GOOGLE,
					UserId = account.Id,
					AvatarUrl = account.PhotoUrl?.ToString(),
					Email = account.Email,
					UserName = account.DisplayName,
				};
				GoogleLoginCommand?.Execute(user);
			}
		}

		#region Facebook Setup

		public void OnSuccess(Java.Lang.Object p0)
		{
		}

		public void OnCancel()
		{
		}

		public void OnError(FacebookException p0)
		{
		}

		public void OnCompleted(JSONObject json, GraphResponse response)
		{
		}

		#endregion

		#region GooglePlus Setup

		#endregion

		protected override void CreateBinding()
		{
			base.CreateBinding();
			var bindingSet = this.CreateBindingSet<SecondLoginScreenView, SecondLoginScreenViewModel>();
			bindingSet.Bind(this)
				.For(v => v.GoogleLoginCommand)
				.To(vm => vm.GoogleLoginCommand);
			bindingSet.Bind(this)
				.For(v => v.CheckNetworkCommand)
				.To(vm => vm.CheckNetworkCommand);
			bindingSet.Apply();
		}

		#region Properties

		private GoogleApiClient _mGoogleApiClient;
		private AlphaRelativeLayout _facebookSignInView;
		public string TokenValue { get; set; }
		public ICommand CheckNetworkCommand { get; set; }
		public ICommand GoogleLoginCommand { get; set; }

		#endregion
	}
}