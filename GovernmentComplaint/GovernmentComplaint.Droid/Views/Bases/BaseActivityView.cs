using Android.App;
using Android.Content.PM;
using Android.Content.Res;
using Android.OS;
using Android.Views;
using GovernmentComplaint.Droid.Helpers;
using MvvmCross.Droid.Views;

namespace GovernmentComplaint.Droid.Views.Bases
{
	[Activity]
	public abstract class BaseActivityView : MvxActivity
	{
		protected abstract int LayoutId { get; }

		protected abstract void InitView();
		protected abstract void CreateBinding();

		public bool IsVisible { get; private set; }

		private View _rootViewgroup;

		public override void OnConfigurationChanged(Configuration newConfig)
		{
			ResolutionHelper.RefreshStaticVariable(this);

			base.OnConfigurationChanged(newConfig);
		}

		protected override void OnCreate(Bundle bundle)
		{
			RequestedOrientation = ScreenOrientation.Portrait;
			base.OnCreate(bundle);
		}

		protected override void OnResume()
		{
			base.OnResume();
			IsVisible = true;
		}

		protected override void OnPause()
		{
			base.OnPause();
			IsVisible = false;
		}

		protected override void OnViewModelSet()
		{
			IsVisible = true;
			SetContentView(LayoutId);

			_rootViewgroup = FindViewById(Android.Resource.Id.Content);
			_rootViewgroup.FocusableInTouchMode = true;
			_rootViewgroup.Clickable = true;
			_rootViewgroup.FocusChange += OnViewFocusChange;

			InitView();
			CreateBinding();
		}

		protected void HideKeyboard()
		{
			KeyboardHelper.HideKeyboard(_rootViewgroup);
		}

		protected void OnViewFocusChange(object sender, View.FocusChangeEventArgs e)
		{
			if (e.HasFocus)
			{
				KeyboardHelper.HideKeyboard(sender as View);
			}
		}

		//public override View OnCreateView(View parent, string name, Context context, IAttributeSet attrs)
		//{
		//	var view = MvxAppCompatActivityHelper.OnCreateView(parent, name, context, attrs);
		//	return view ?? base.OnCreateView(parent, name, context, attrs);
		//}

		//protected override void AttachBaseContext(Context @base)
		//{
		//	base.AttachBaseContext(MvxContextWrapper.Wrap(@base, this));
		//}
	}
}