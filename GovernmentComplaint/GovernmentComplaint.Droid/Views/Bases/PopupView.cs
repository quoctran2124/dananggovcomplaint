using System;
using System.Windows.Input;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.Provider;
using Android.Support.V4.Content;
using Android.Views;
using Android.Widget;
using GovernmentComplaint.Core.Models;
using GovernmentComplaint.Core.ViewModels.Bases;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Binding.Droid.BindingContext;

namespace GovernmentComplaint.Droid.Views.Bases
{
	[Activity]
	public abstract class PopupView : BaseActivityView
	{
		private bool _isCancelPopupShown;
		private bool _isDropdownPopupShown;
		private bool _isFail24HPopupShown;
		//private bool _isLoginPopupShown;
		private bool _isLogoutPopupShown;
		private bool _isNoWifiPopupShown;
		private bool _isConnectionErrorShown;
		private bool _isPopup2Shown;
		private bool _isPopupShown;
		//private bool _isRequestInfoPopupShown;
		private bool _isErrorImagesShown;
		private bool _isVideoPopupShown;
		private bool _isNoCommentPopupShown;
		private bool _isAttachedImagesPopupShown;
		private bool _isCancelCommentPopupShown;
		private bool _isResendFeedbackPopupShown;
		private View _popupContainer, _indicator;
		public ViewGroup PopupLayout { get; private set; }
		public ViewGroup DropdownPopupLayout { get; private set; }
		public ICommand BackCommand { get; set; }
		public ICommand SaveFeedbackCommand { get; set; }
	    public ICommand SendAnswerFeedback { get; set; }

        public bool ShouldMoveToBackground
		{
			get { return false; }
			set
			{
				if (value)
					MoveTaskToBack(true);
			}
		}

	    public bool IsSatisfySelected { get; set; }
	    public bool IsSlowResponseSelected { get; set; }
	    public bool IsUnSuitAnswerSelected { get; set; }


        public bool IsPopupShown
		{
			get { return _isPopupShown; }
			set
			{
				if (value)
				{
					if (IsVisible)
					{
						_popupContainer.Visibility = ViewStates.Visible;
						_isPopupShown = true;
					}
				}
				else
				{
					_popupContainer.Visibility = ViewStates.Gone;
					_isPopupShown = false;
				}
			}
		}

		public bool IsPopup2Shown
		{
			get { return _isPopup2Shown; }
			set
			{
				if (value)
				{
					if (IsVisible)
						_isPopup2Shown = true;
				}
				else
				{
					_isPopup2Shown = false;
				}
			}
		}

		public bool IsLogoutPopupShown
		{
			get { return _isLogoutPopupShown; }
			set
			{
				_isLogoutPopupShown = value;
				if (value)
					ShowLogoutPopup();
				else
					IsPopupShown = false;
			}
		}

		public bool IsCancelCommentPopupShown
		{
			get { return _isCancelCommentPopupShown; }
			set
			{
				_isCancelCommentPopupShown = value;
				if (value)
					ShowCancelCommentPopup();
				else
					IsPopupShown = false;
			}
		}

		public bool IsErrorImagesShown
		{
			get { return _isErrorImagesShown; }
			set
			{
				_isErrorImagesShown = value;
				if (value)
					ShowErrorImagesPopup();
				else
					IsPopupShown = false;
			}
		}

		public bool IsFail24HPopupShown
		{
			get { return _isFail24HPopupShown; }
			set
			{
				_isFail24HPopupShown = value;
				if (value)
					ShowFail24HPopup();
				else
					IsPopupShown = false;
			}
		}

		public bool IsNoCommentPopupShown
		{
			get { return _isNoCommentPopupShown; }
			set
			{
				_isNoCommentPopupShown = value;
				if (value)
					ShowNoCommentPopup();
				else
					IsPopupShown = false;
			}
		}

		public bool IsNoWifiPopupShown
		{
			get { return _isNoWifiPopupShown; }
			set
			{
				_isNoWifiPopupShown = value;
				if (value)
					ShowNoWifiPopup();
				else
					IsPopupShown = false;
			}
		}

		public bool IsConnectionErrorPopupShown
		{
			get { return _isConnectionErrorShown; }
			set
			{
				_isConnectionErrorShown = value;
				if (value)
					ShowConnectionErrorPopup();
				else
					IsPopupShown = false;
			}
		}

		public bool IsVideoPopupShown
		{
			get { return _isVideoPopupShown; }
			set
			{
				_isVideoPopupShown = value;
				if (value)
					ShowVideoPopup();
				else
					IsPopupShown = false;
			}
		}

		//public bool IsRequestInfoPopupShown
		//{
		//    get { return _isRequestInfoPopupShown; }
		//    set
		//    {
		//        _isRequestInfoPopupShown = value;
		//        if (value)
		//            ShowRequestInfoPopup();
		//        else
		//            IsPopupShown = false;
		//    }
		//}

		//public bool IsLoginPopupShown
		//{
		//    get { return _isLoginPopupShown; }
		//    set
		//    {
		//        _isLoginPopupShown = value;
		//        if (value)
		//            ShowLoginPopup();
		//        else
		//            IsPopupShown = false;
		//    }
		//}

		public bool IsCancelPopupShown
		{
			get { return _isCancelPopupShown; }
			set
			{
				_isCancelPopupShown = value;
				if (value)
					ShowCancelPopup();
				else
					IsPopupShown = false;
			}
		}

		

		public bool IsDropdownPopupShown
		{
			get { return _isDropdownPopupShown; }
			set
			{
				_isDropdownPopupShown = value;
				if (value)
					ShowDropdownDepartment();
				else
					IsPopupShown = false;
			}
		}

		public bool IsAttachedImagesPopupShown
		{
			get { return _isAttachedImagesPopupShown; }
			set
			{
				_isAttachedImagesPopupShown = value;
				if (value)
					ShowAttachedImage();
				else
					IsPopupShown = false;
			}
		}
		public bool IsResendFeedbackPopupShown
		{
			get { return _isResendFeedbackPopupShown; }
			set
			{
				_isResendFeedbackPopupShown = value;
				if (value)
					ShowResendFeedbackPopup();
				else
					IsPopupShown = false;
			}
		}


		private bool _isPopupOfflineOptionsShown;
		public bool IsPopupOfflineOptionsShown
		{
			get { return _isPopupOfflineOptionsShown; }
			set
			{
				_isPopupOfflineOptionsShown = value;
				if (value)
				{
					ShowPopupOfflineOptions();
				}
				else
				{
					IsPopupShown = false;
				}
			}
		}

		public View SetPopupLayout(int layout)
		{
			PopupLayout.RemoveAllViews();
			return this.BindingInflate(layout, PopupLayout);
		}

		public View SetDropdownPopupLayout(int layout)
		{
			DropdownPopupLayout.RemoveAllViews();
			return this.BindingInflate(layout, DropdownPopupLayout);
		}

		protected override void InitView()
		{
			var backgroundView = FindViewById<ViewGroup>(Android.Resource.Id.Content).GetChildAt(0) as ViewGroup;
			this.BindingInflate(Resource.Layout.PopupView, backgroundView);
			PopupLayout = FindViewById<ViewGroup>(Resource.Id.popupLayout);
			_popupContainer = FindViewById<ViewGroup>(Resource.Id.popupContainer);
			_indicator = FindViewById(Resource.Id.indicator);
			DropdownPopupLayout = FindViewById<ViewGroup>(Resource.Id.dropdownPopupLayout);
		}


		protected override void CreateBinding()
		{
			var bindingSet = this.CreateBindingSet<PopupView, PopupViewModel>();

			bindingSet.Bind(this).For(x => x.IsVideoPopupShown).To(vm => vm.IsVideoPopupShown);

			bindingSet.Bind(this).For(x => x.IsCancelPopupShown).To(vm => vm.IsCancelPopupShown);

			bindingSet.Bind(this).For(x => x.IsCancelCommentPopupShown).To(vm => vm.IsCancelCommentPopupShown);

			bindingSet.Bind(this).For(x => x.ShouldMoveToBackground).To(vm => vm.ShouldMoveToBackground);

			bindingSet.Bind(this).For(x => x.BackCommand).To(vm => vm.BackCommand);

			bindingSet.Bind(this).For(x => x.IsLogoutPopupShown).To(vm => vm.IsLogoutPopupShown);

			bindingSet.Bind(this).For(x => x.IsDropdownPopupShown).To(vm => vm.IsDropdownPopupShown);

			bindingSet.Bind(this).For(x => x.IsIndicatorShown).To(vm => vm.IsIndicatorShown);
			
			bindingSet.Bind(this).For(x => x.IsFail24HPopupShown).To(vm => vm.IsFail24HPopupShown);

			bindingSet.Bind(this).For(x => x.IsNoWifiPopupShown).To(vm => vm.IsNoWifiPopupShown);

			// feature v1.1
			bindingSet.Bind(this).For(x => x.IsPopupOfflineOptionsShown).To(vm => vm.IsPopupOfflineOptionsShown);

			bindingSet.Bind(this).For(x => x.SaveFeedbackCommand).To(vm => vm.SaveFeedbackCommand);
			
			bindingSet.Bind(this).For(x => x.IsConnectionErrorPopupShown).To(vm => vm.IsConnectionErrorPopupShown);

			bindingSet.Bind(this).For(x => x.IsRatingPopupShown).To(vm => vm.IsRatingPopupShown);

		    bindingSet.Bind(this).For(x => x.IsAnswerFeedbackPopupShown).To(vm => vm.IsAnswerFeedbackPopupShown);

            bindingSet.Bind(this).For(x => x.IsErrorImagesShown).To(vm => vm.IsErrorImagesShown);

			bindingSet.Bind(this).For(x => x.IsNoCommentPopupShown).To(vm => vm.IsNoCommentPopupShown);

			bindingSet.Bind(this).For(x => x.IsAttachedImagesPopupShown).To(vm => vm.IsAttachedImagesPopupShown);

			bindingSet.Bind(this).For(x => x.IsResendFeedbackPopupShown).To(vm => vm.IsResendFeedbackPopupShown);

			bindingSet.Bind(this).For(x => x.IsPopupShown).To(vm => vm.IsPopupShown);

		    bindingSet.Bind(this).For(x => x.SendAnswerFeedback).To(vm => vm.SendAnswerFeedback);


            bindingSet.Apply();
		}

		private void ShowResendFeedbackPopup()
		{
			var popupLayout = SetPopupLayout(Resource.Layout.PopupResendFeedback);
			popupLayout.FindViewById(Resource.Id.cancelButton).Click += delegate { IsPopupShown = false; };

			popupLayout.FindViewById(Resource.Id.confirmButton).Click += delegate { IsPopupShown = false; };

			IsPopupShown = true;
		}

		private void ShowVideoPopup()
		{
			var popupLayout = SetPopupLayout(Resource.Layout.PopupVideo);
			popupLayout.FindViewById(Resource.Id.VideoCancelButton).Click += delegate { IsPopupShown = false; };
			IsPopupShown = true;
		}

		private void ShowCancelCommentPopup()
		{
			var popupLayout = SetPopupLayout(Resource.Layout.PopupCancelComment);
			popupLayout.FindViewById(Resource.Id.cancelCancelButton).Click += delegate { IsPopupShown = false; };

			popupLayout.FindViewById(Resource.Id.confirmCancelButton).Click += delegate { IsPopupShown = false; };

			IsPopupShown = true;
		}


		private void ShowErrorImagesPopup()
		{
			var popupLayout = SetPopupLayout(Resource.Layout.PopupErrorImages);
			popupLayout.FindViewById(Resource.Id.confirmButton).Click += delegate { IsPopupShown = false; };
			IsPopupShown = true;
		}

		private void ShowNoWifiPopup()
		{
			var popupLayout = SetPopupLayout(Resource.Layout.PopupNoWifi);
			popupLayout.FindViewById(Resource.Id.confirmButton).Click += delegate
			{
				IsPopupShown = false;
				StartActivity(new Intent(Settings.ActionWifiSettings));
			};
			popupLayout.FindViewById(Resource.Id.CancelButton).Click += delegate { IsPopupShown = false; };
			IsPopupShown = true;
		}

		private void ShowPopupOfflineOptions()
		{
			var popupLayout = SetPopupLayout(Resource.Layout.PopupOfflineOptions);
			popupLayout.FindViewById(Resource.Id.confirmButton).Click += delegate
			{
				IsPopupShown = false;
				SaveFeedbackCommand?.Execute(null);
			};

			popupLayout.FindViewById(Resource.Id.tvGoToWifiSetting).Click += delegate
			{
				IsPopupShown = false;
				StartActivity(new Intent(Settings.ActionWifiSettings));
			};
			
			popupLayout.FindViewById(Resource.Id.CancelButton).Click += delegate { IsPopupShown = false; };
			IsPopupShown = true;
		}

		private void ShowConnectionErrorPopup()
		{
			var popupLayout = SetPopupLayout(Resource.Layout.PopupConnectionError);
			popupLayout.FindViewById(Resource.Id.confirmButton).Click += delegate
			{
				IsPopupShown = false;
			};
			popupLayout.FindViewById(Resource.Id.CancelButton).Click += delegate { IsPopupShown = false; };
			IsPopupShown = true;
		}

		private void ShowCancelPopup()
		{
			var popupLayout = SetPopupLayout(Resource.Layout.PopupCancel);
			popupLayout.FindViewById(Resource.Id.cancelCancelButton).Click += delegate { IsPopupShown = false; };

			IsPopupShown = true;
		}

		private void ShowLogoutPopup()
		{
			var popupLayout = SetPopupLayout(Resource.Layout.PopupLogout);
			popupLayout.FindViewById(Resource.Id.CancelButton).Click += delegate { IsPopupShown = false; };
			popupLayout.FindViewById(Resource.Id.ConfirmButton).Click += delegate { IsPopupShown = false; };
			IsPopupShown = true;
		}
		
		private void ShowDropdownDepartment()
		{
			var popupLayout = SetPopupLayout(Resource.Layout.PopupDepartmentListView);
			IsPopupShown = true;
		}

		private void ShowAttachedImage()
		{
			var popupLayout = SetPopupLayout(Resource.Layout.PopupAttachedImage);
			popupLayout.FindViewById(Resource.Id.cancelButton).Click += delegate { IsPopupShown = false; };
			IsPopupShown = true;
		}

		private void ShowFail24HPopup()
		{
			var popupLayout = SetPopupLayout(Resource.Id.popupFail24h);
			popupLayout.FindViewById(Resource.Id.confirmButton).Click += delegate { IsPopupShown = false; };
			IsPopupShown = true;
		}

		private void ShowNoCommentPopup()
		{
			var popupLayout = SetPopupLayout(Resource.Layout.PopupNoComment);
			popupLayout.FindViewById(Resource.Id.confirmButton).Click += delegate { IsPopupShown = false; };
			IsPopupShown = true;
		}

		private void ShowRatingPopup()
		{
			var popupLayout = SetPopupLayout(Resource.Layout.PopupRating);
			var ratingBar = popupLayout.FindViewById<RatingBar>(Resource.Id.ratingBar);
			popupLayout.FindViewById(Resource.Id.CancelButton).Click += delegate
			{
				IsPopupShown = false;
				ratingBar.Rating = 0;
			};
			ratingBar.Click += delegate { IsPopupShown = false; };
			IsPopupShown = true;
			var stars = (LayerDrawable) ratingBar.ProgressDrawable;
			stars.GetDrawable(2)
				.SetColorFilter(new Color(ContextCompat.GetColor(Application.Context, Resource.Color.StarColor)),
					PorterDuff.Mode.SrcAtop);
			stars.GetDrawable(0)
				.SetColorFilter(new Color(ContextCompat.GetColor(Application.Context, Resource.Color.DarkestGray)),
					PorterDuff.Mode.SrcAtop);
			stars.GetDrawable(1)
				.SetColorFilter(new Color(ContextCompat.GetColor(Application.Context, Resource.Color.StarColor)),
					PorterDuff.Mode.SrcAtop);
		}

	    private void ShowAnswerFeedbackPopup()
	    {
	        var popupLayout = SetPopupLayout(Resource.Layout.PopupAnswerFeedback);
	        IsSatisfySelected = true;
	        IsSlowResponseSelected = false;
	        IsUnSuitAnswerSelected = false;

            var satisfiedCheckbox = popupLayout.FindViewById<ImageView>(Resource.Id.satisfiedCheckbox);
	        var unSatisfiedImage = popupLayout.FindViewById<ImageView>(Resource.Id.unsatisfiedCheckbox);
	        var unSatisfiedLayout = popupLayout.FindViewById(Resource.Id.unSatisfiedLayout);
	        unSatisfiedLayout.Visibility = ViewStates.Gone;
	        var slowResponseCheckbox = popupLayout.FindViewById<ImageView>(Resource.Id.slowResponseCheckbox);
	        var unSuitAnswerCheckbox = popupLayout.FindViewById<ImageView>(Resource.Id.unSuitAnswerCheckbox);

	        var answerFeedbackError = popupLayout.FindViewById<TextView>(Resource.Id.answerFeedbackError);
	        answerFeedbackError.Visibility = ViewStates.Gone;



            popupLayout.FindViewById(Resource.Id.satisfied).Click += delegate
            {
                IsSatisfySelected = true;
                satisfiedCheckbox.SetImageDrawable(GetDrawable(Resource.Drawable.radio_checked));
                unSatisfiedImage.SetImageDrawable(GetDrawable(Resource.Drawable.radio_unchecked));
                unSatisfiedLayout.Visibility = ViewStates.Gone;
            };

	        popupLayout.FindViewById(Resource.Id.unsatisfied).Click += delegate
	        {
                IsSatisfySelected = false;
	            satisfiedCheckbox.SetImageDrawable(GetDrawable(Resource.Drawable.radio_unchecked));
                unSatisfiedImage.SetImageDrawable(GetDrawable(Resource.Drawable.radio_checked));
	            unSatisfiedLayout.Visibility = ViewStates.Visible;
	            slowResponseCheckbox.SetImageDrawable(GetDrawable(Resource.Drawable.checkbox_uncheck));
	            unSuitAnswerCheckbox.SetImageDrawable(GetDrawable(Resource.Drawable.checkbox_uncheck));
            };

	        popupLayout.FindViewById(Resource.Id.slowResponse).Click += delegate
	        {
	            IsSlowResponseSelected = !IsSlowResponseSelected;
	            slowResponseCheckbox.SetImageDrawable(GetDrawable(IsSlowResponseSelected ? Resource.Drawable.checkbox_checked : Resource.Drawable.checkbox_uncheck));
	        };

	        popupLayout.FindViewById(Resource.Id.unSuitAnswer).Click += delegate
	        {
	            IsUnSuitAnswerSelected = !IsUnSuitAnswerSelected;
	            unSuitAnswerCheckbox.SetImageDrawable(GetDrawable(IsUnSuitAnswerSelected ? Resource.Drawable.checkbox_checked : Resource.Drawable.checkbox_uncheck));
	        };

            popupLayout.FindViewById(Resource.Id.confirmButton).Click += delegate
            {
                if (IsSatisfySelected == false && IsSlowResponseSelected == false && IsUnSuitAnswerSelected == false)
                {
                    answerFeedbackError.Visibility = ViewStates.Visible;
                    return;
                }

                AnswerFeedbackContent answerFeedbackContent = new AnswerFeedbackContent()
                {
                    SatisfySelected = IsSatisfySelected,
                    SlowResponseSelected = IsSlowResponseSelected,
                    UnSuitAnswerSelected = IsUnSuitAnswerSelected
                };

                SendAnswerFeedback?.Execute(answerFeedbackContent);
                IsPopupShown = false;

            };
            popupLayout.FindViewById(Resource.Id.cancelButton).Click += delegate { IsPopupShown = false; };
            IsPopupShown = true;
	    }
        
        public override void OnBackPressed()
		{
			BackCommand.Execute(null);
		}

		private bool _isAnswerFeedbackPopupShown;
		public bool IsAnswerFeedbackPopupShown
        {
			get { return _isAnswerFeedbackPopupShown; }
			set
			{
			    _isAnswerFeedbackPopupShown = value;
				if (value)
				{
				    ShowAnswerFeedbackPopup();
				}
				else
				{
					IsPopupShown = false;
				}
			}
		}


	    private bool _isRatingPopupShown;
	    public bool IsRatingPopupShown
	    {
	        get { return _isRatingPopupShown; }
	        set
	        {
	            _isRatingPopupShown = value;
	            if (value)
	            {
	                ShowRatingPopup();
	            }
	            else
	            {
	                IsPopupShown = false;
	            }
	        }
	    }
        #region Indicator Shown

        public event EventHandler IsIndicatorShownChanged;
		private bool _isIndicatorShown;

		public bool IsIndicatorShown
		{
			get { return _isIndicatorShown; }
			set
			{
				if (value)
				{
					if (IsVisible)
					{
						HideKeyboard();

						_indicator.Visibility = ViewStates.Visible;
						_isIndicatorShown = value;
					}
					else
					{
						IsIndicatorShownChanged(this, null);
					}
				}
				else
				{
					_indicator.Visibility = ViewStates.Gone;
					_isIndicatorShown = value;
				}
			}
		}

		#endregion
	}
}