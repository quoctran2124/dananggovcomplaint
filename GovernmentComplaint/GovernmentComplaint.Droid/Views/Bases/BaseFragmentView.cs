using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using MvvmCross.Droid.FullFragging.Fragments;
using MvvmCross.Binding.Droid.BindingContext;
using Xamarin.Facebook;

namespace GovernmentComplaint.Droid.Views.Bases
{
    public abstract class BaseFragmentView : MvxFragment
    {
        protected abstract int LayoutId { get; }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            base.OnCreateView(inflater, container, savedInstanceState);

            var view = this.BindingInflate(LayoutId, null);
            var layoutParameters = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MatchParent,
                ViewGroup.LayoutParams.MatchParent);

            view.LayoutParameters = layoutParameters;
        
            InitView(view);
            CreateBinding();

            return view;
        }

        protected abstract void InitView(View view);
        protected abstract void CreateBinding();
    }
}