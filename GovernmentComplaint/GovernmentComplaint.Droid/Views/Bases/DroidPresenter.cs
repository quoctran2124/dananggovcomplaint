using System;
using Android.App;
using MvvmCross.Droid.Views;
using MvvmCross.Droid.FullFragging.Fragments;
using MvvmCross.Core.ViewModels;
using GovernmentComplaint.Droid;

namespace GovernmentComplaint.Droid.Views.Bases
{
    public class DroidPresenter : MvxAndroidViewPresenter
    {
        private readonly IFragmentTypeLookup _fragmentTypeLookup;
        private readonly IMvxViewModelLoader _viewModelLoader;
        private FragmentManager _fragmentManager;

        public DroidPresenter(IMvxViewModelLoader viewModelLoader, IFragmentTypeLookup fragmentTypeLookup)
        {
            _fragmentTypeLookup = fragmentTypeLookup;
            _viewModelLoader = viewModelLoader;
        }

        public void RegisterFragmentManager(FragmentManager fragmentManager)
        {
            _fragmentManager = fragmentManager;
        }

        public override void Show(MvxViewModelRequest request)
        {
            Type fragmentType;
            if (_fragmentManager == null ||
                !_fragmentTypeLookup.TryGetFragmentType(request.ViewModelType, out fragmentType))
            {
                base.Show(request);
                return;
            }

            var fragment = (MvxFragment)Activator.CreateInstance(fragmentType);
            fragment.ViewModel = _viewModelLoader.LoadViewModel(request, null);

            ShowFragment(fragment);
        }

        private void ShowFragment(MvxFragment fragment)
        {
            var transaction = _fragmentManager.BeginTransaction();

            transaction.Add(Resource.Id.container, fragment, fragment.GetType().Name).Commit();
        }

        public override void Close(IMvxViewModel viewModel)
        {
            if (_fragmentManager != null)
            {
                var transaction = _fragmentManager.BeginTransaction();

                var name = viewModel.GetType().Name;
                var tag = name.Substring(0, name.LastIndexOf("Model"));
                var fragmentToClose = (MvxFragment)_fragmentManager.FindFragmentByTag(tag);

                while (fragmentToClose != null && fragmentToClose.ViewModel != viewModel)
                {
                    fragmentToClose = (MvxFragment)_fragmentManager.FindFragmentByTag(tag);
                }

                if (fragmentToClose != null)
                {
                    transaction.Remove(fragmentToClose).Commit();
                }
            }

            base.Close(viewModel);
        }
    }
}