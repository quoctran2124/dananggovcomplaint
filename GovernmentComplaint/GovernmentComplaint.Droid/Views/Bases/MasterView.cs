using System;
using System.Windows.Input;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Gms.Auth.Api;
using Android.Gms.Auth.Api.SignIn;
using Android.Gms.Common;
using Android.Gms.Common.Apis;
using Android.Net;
using Android.OS;
using Android.Widget;
using GovernmentComplaint.Core.Enums;
using GovernmentComplaint.Core.Models;
using GovernmentComplaint.Core.ViewModels.Bases;
using GovernmentComplaint.Droid.Controls;
using GovernmentComplaint.Droid.Helpers;
using GovernmentComplaint.Droid.Receivers;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Droid.Views;
using MvvmCross.Platform;
using Org.Json;
using Xamarin.Facebook;
using Xamarin.Facebook.Login.Widget;
using static Android.Gms.Common.Apis.GoogleApiClient;
using Object = Java.Lang.Object;

namespace GovernmentComplaint.Droid.Views.Bases
{
	[Activity(ConfigurationChanges = ConfigChanges.Orientation | ConfigChanges.ScreenSize)]
	public class MasterView : PopupView, IFacebookCallback, GraphRequest.IGraphJSONObjectCallback,
		IOnConnectionFailedListener, IConnectionCallbacks
	{
		private NetworkHelper _networkHelper;
		private AlphaRelativeLayout _fbButton;
		private static int RC_SIGN_IN = 9001;
		private ProfileTracker _profileTracker;
		public ICommand NetworkConnectedCommand { get; set; }

		private bool _wasConnectedBeforeStop;

		private NetworkChangeReceiver _networkChangeReceiver;

		protected override void OnCreate(Bundle bundle)
		{
			FacebookSdk.SdkInitialize(Application.Context);
			FacebookSdk.AddLoggingBehavior(LoggingBehavior.IncludeAccessTokens);
			RequestedOrientation = ScreenOrientation.Portrait;
			base.OnCreate(bundle);
			var facebookLoginButton = FindViewById<LoginButton>(Resource.Id.fbLoginButton);
			CallbackManager = CallbackManagerFactory.Create();
			facebookLoginButton.RegisterCallback(CallbackManager, this);
			_fbButton = FindViewById<AlphaRelativeLayout>(Resource.Id.FacebookLogin);
			_fbButton.Click += (s, e) => { facebookLoginButton.PerformClick(); };
			_profileTracker = new CustomProfileTracker
			{
				HandleCurrentProfileChanged = (oldProfile, currentProfile) =>
				{
					RetrieveFacebookUser();
				}
			};

			var gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DefaultSignIn)
				.RequestProfile()
				.RequestEmail()
				.Build();

			GoogleApiClient = new GoogleApiClient.Builder(this)
				.AddConnectionCallbacks(this)
				.AddOnConnectionFailedListener(this)
				.AddApi(Auth.GOOGLE_SIGN_IN_API, gso)
				.Build();
			var googleSignInView = FindViewById<AlphaRelativeLayout>(Resource.Id.GoogleLogin);
			googleSignInView.Click += (s, e) =>
			{
				var signInIntent = Auth.GoogleSignInApi.GetSignInIntent(GoogleApiClient);
				StartActivityForResult(signInIntent, RC_SIGN_IN);
			};
			GoogleSingleSignOn();

			_menuLayout = FindViewById<RelativeLayout>(Resource.Id.SidebarMenu);
			FacebookSingleSignOn();

			_spinnerSearchBox = FindViewById<RelativeLayout>(Resource.Id.SpinnerSearchBox);
			_spinnerSearchBox.Click += (sender, args) => HideKeyboard();

			_networkHelper = new NetworkHelper();
		}

		protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
		{
			base.OnActivityResult(requestCode, resultCode, data);
			CallbackManager.OnActivityResult(requestCode, (int)resultCode, data);

			if (requestCode == RC_SIGN_IN)
			{
				var result = Auth.GoogleSignInApi.GetSignInResultFromIntent(data);
				HandleSignInResult(result);
			}
		}

		private void HandleSignInResult(GoogleSignInResult result)
		{
			if (result.IsSuccess)
			{
				// Signed in successfully, show authenticated UI.
				var account = result.SignInAccount;
				var user = new UserInfo
				{
					AccountType = AccountType.GOOGLE,
					UserId = account.Id,
					AvatarUrl = account.PhotoUrl?.ToString(),
					Email = account.Email,
					UserName = account.DisplayName,
				};
				LoginCommand?.Execute(user);
			}
		}

		protected override void InitView()
		{
			base.InitView();
			ResolutionHelper.RefreshStaticVariable(this);
			var presenter = (DroidPresenter)Mvx.Resolve<IMvxAndroidViewPresenter>();
			presenter.RegisterFragmentManager(FragmentManager);

			_networkChangeReceiver = new NetworkChangeReceiver();
			_networkChangeReceiver.NetworkConnected += OnNetworkConnected;
			RegisterReceiver(_networkChangeReceiver, new IntentFilter(ConnectivityManager.ConnectivityAction));
		}

		protected override void CreateBinding()
		{
			base.CreateBinding();
			var bindingSet = this.CreateBindingSet<MasterView, MasterViewModel>();
			bindingSet.Bind(this)
				.For(v => v.LoginCommand)
				.To(vm => vm.LoginCommand);

			bindingSet.Bind(this)
				.For(v => v.NetworkConnectedCommand)
				.To(vm => vm.NetworkConnectedCommand);

			bindingSet.Bind()
				.For(v => v.IsSidebarShown)
				.To(vm => vm.IsSidebarShown);
			bindingSet.Apply();
		}

		private void HideMenu()
		{
			_menuLayout.Animate().TranslationX(_menuLayout.Width).SetDuration(100);
		}

		public void ExecuteToggleMenu()
		{
			_menuLayout.TranslationX = _menuLayout.Width;
			_menuLayout.Animate().TranslationX(0).SetDuration(100);
		}

		#region Facebook Setup

		public void RetrieveFacebookUser()
		{
			var profile = Profile.CurrentProfile;
			if (profile != null)
			{
				var user = new UserInfo
				{
					AccountType = AccountType.FACEBOOK,
					UserId = profile.Id,
					AvatarUrl = profile.GetProfilePictureUri(500, 500).ToString(),
					Email = "",
					UserName = profile.Name,
				};
				LoginCommand?.Execute(user);
			}
		}

		public void FacebookSingleSignOn()
		{
			RetrieveFacebookUser();
		}

		public void OnCancel()
		{
		}

		public void OnError(FacebookException p0)
		{
		}

		public void OnSuccess(Object result)
		{
		}

		public void OnCompleted(JSONObject json, GraphResponse response)
		{
		}

		protected override void OnDestroy()
		{
			base.OnDestroy();
			_profileTracker.StopTracking();
		}

		#endregion

		#region GooglePlus Setup

		private void GoogleSingleSignOn()
		{
			var pendingResult = Auth.GoogleSignInApi.SilentSignIn(GoogleApiClient);
			if (pendingResult.IsDone)
			{
				var result = pendingResult.Get();
				HandleSignInResult((GoogleSignInResult)result);
			}
		}

		protected override void OnStart()
		{
			base.OnStart();
			GoogleApiClient.Connect();
		}

		private bool _isStoping;
		protected override void OnStop()
		{
			base.OnStop();
			GoogleApiClient.Disconnect();
			_isStoping = true;
			_wasConnectedBeforeStop = _networkHelper.IsConnected;
		}

		protected override void OnResume()
		{
			base.OnResume();
			_isStoping = false;
			if (!_networkChangeReceiver.IsFirstReceive)
			{
				if (!_wasConnectedBeforeStop && _networkHelper.IsConnected)
				{
					NetworkConnectedCommand?.Execute(null);
				}
			}
		}

		public void OnConnectionFailed(ConnectionResult result)
		{
		}

		public void OnConnected(Bundle connectionHint)
		{
		}

		public void OnConnectionSuspended(int cause)
		{
			GoogleApiClient.Connect();
		}

		#endregion

		#region Properties

		public static ICallbackManager CallbackManager;
		public static GoogleApiClient GoogleApiClient;
		private bool _isSidebarShown;
		protected override int LayoutId => Resource.Layout.MasterView;
		public ICommand LoginCommand { get; set; }
		private RelativeLayout _menuLayout, _spinnerSearchBox;

		public bool IsShowMenu { get; set; }

		public bool IsSidebarShown
		{
			get { return _isSidebarShown; }
			set
			{
				_isSidebarShown = value;
				if (value)
				{
					ExecuteToggleMenu();
					HideKeyboard();
				}
				else
				{
					HideMenu();
				}
			}
		}

		#endregion


		private void OnNetworkConnected(object sender, EventArgs e)
		{
			if (!_isStoping)
			{
				NetworkConnectedCommand?.Execute(null);
			}
		}

	}

	class CustomProfileTracker : ProfileTracker
	{
		public delegate void CurrentProfileChangedDelegate(Profile oldProfile, Profile currentProfile);

		public CurrentProfileChangedDelegate HandleCurrentProfileChanged { get; set; }

		protected override void OnCurrentProfileChanged(Profile oldProfile, Profile currentProfile)
		{
			var p = HandleCurrentProfileChanged;
			if (p != null)
				p(oldProfile, currentProfile);
		}
	}
}