using System;

namespace GovernmentComplaint.Droid.Views.Bases
{
    public interface IFragmentTypeLookup
    {
        bool TryGetFragmentType(Type viewModelType, out Type fragmentType);
    }
}