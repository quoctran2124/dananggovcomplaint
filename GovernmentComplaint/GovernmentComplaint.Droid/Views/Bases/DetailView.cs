using System.IO;
using Android.Content;
using Android.Graphics;
using Android.Views;
using Java.Lang;
using MvvmCross.Binding.Droid.BindingContext;
using File = Java.IO.File;
using Path = System.IO.Path;
using Environment = Android.OS.Environment;
using Uri = Android.Net.Uri;

namespace GovernmentComplaint.Droid.Views.Bases
{
	public abstract class DetailView : BaseFragmentView
    {
        private byte[] _attachmentData;


		public PopupView PopupView => Activity as PopupView;
        public string AttachmentUrls { get; set; }

        public byte[] AttachmentData
        {
            get { return _attachmentData; }
            set
            {
                _attachmentData = value;
                if (value != null)
                    OpenAttachment();
            }
        }

        protected View SetPopupLayout(int layout)
        {
            if (PopupView == null) return null;
            PopupView.PopupLayout.RemoveAllViews();
            return this.BindingInflate(layout, PopupView.PopupLayout);
        }

        private void OpenAttachment()
        {
            if (_attachmentData == null) return;
            var bitmap = CreateBitmap();
            PassBitmapToSuitableApp(bitmap);
        }

        private void PassBitmapToSuitableApp(Bitmap bitmap)
        {
            if (bitmap == null)
                return;
            var documentsPath = Environment.ExternalStorageDirectory.Path;
            var savedPath = Path.Combine(documentsPath, AttachmentUrls);

            using (var stream = new FileStream(savedPath, FileMode.Create))
            {
                bitmap.Compress(Bitmap.CompressFormat.Jpeg, 95, stream);
                var file = new File(savedPath);
                var uri = Uri.FromFile(file);
                var intent = new Intent(Intent.ActionView);
                intent.SetDataAndType(uri, "image/jpeg");
                intent.SetFlags(ActivityFlags.ClearWhenTaskReset | ActivityFlags.NewTask);
                StartActivity(intent);
            }
        }

        private Bitmap CreateBitmap()
        {
            var imageOption = new BitmapFactory.Options();
            Bitmap imageBitmap = null;
            try
            {
                imageBitmap = BitmapFactory.DecodeByteArray(_attachmentData, 0, _attachmentData.Length, imageOption);
            }
            catch (OutOfMemoryError)
            {
                if (imageBitmap != null)
                {
                    imageBitmap.Dispose();
                    imageBitmap = null;
                }

                for (var i = 2; i <= 4; i++)
                    try
                    {
                        var sampleSize = i;
                        imageOption.InSampleSize = sampleSize;
                        imageBitmap = BitmapFactory.DecodeByteArray(_attachmentData, 0, _attachmentData.Length,
                            imageOption);
                        break;
                    }
                    catch (OutOfMemoryError)
                    {
                        if (imageBitmap == null) continue;
                        imageBitmap.Dispose();
                        imageBitmap = null;
                    }
            }
            return imageBitmap;
        }

        protected override void InitView(View view)
        {

		}

        protected override void CreateBinding()
        {
		}

	}
}