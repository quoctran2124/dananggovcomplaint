using System;
using System.Text.RegularExpressions;
using Android.Util;
using Android.Views;
using GovernmentComplaint.Core.ViewModels;
using GovernmentComplaint.Droid.Views.Bases;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Binding.Droid.Views;

namespace GovernmentComplaint.Droid.Views
{
    public class DashboardView : DetailView
    {
        protected override int LayoutId => Resource.Layout.DashboardView;
        private MvxListView _categoryListView;
        private MvxListView _processingUnitListView;

        private View _view;

        private int _numberOfCategoryItemList;
        public int NumberOfCategoryItemList
        {
            get => _numberOfCategoryItemList;
            set
            {
                _numberOfCategoryItemList = value;
                GetHeightCategoryList();
            }
        }

        private int _numberOfProcessingUnitItemList;

        public int NumberOfProcessingUnitItemList
        {
            get => _numberOfProcessingUnitItemList;
            set
            {
                _numberOfProcessingUnitItemList = value;
                GetHeightProcessingUnitList();
            }
        }

        protected override void InitView(View view)
        {
            base.InitView(view);
            _view = view;
            _categoryListView = (MvxListView) view.FindViewById(Resource.Id.categoryList);
            _processingUnitListView = (MvxListView) view.FindViewById(Resource.Id.processingUnitList);
        }

        private void GetHeightCategoryList()
        {
            var paramListView = _categoryListView.LayoutParameters;
            var itemHeight = Resources.GetDimension(Resource.Dimension.CategoryItemHeight);

            paramListView.Height = (int)Math.Round(NumberOfCategoryItemList * itemHeight, 0);
        }

        private void GetHeightProcessingUnitList()
        {
            var paramListView = _processingUnitListView.LayoutParameters;
            var itemHeight = Resources.GetDimension(Resource.Dimension.CategoryItemHeight);

            paramListView.Height = (int)Math.Round(NumberOfProcessingUnitItemList * itemHeight, 0);
        }

        private int PixelsToDp(int pixels)
        {
            return (int) TypedValue.ApplyDimension(ComplexUnitType.Dip, pixels, Resources.DisplayMetrics);
        }

        protected override void CreateBinding()
        {
            base.CreateBinding();
            var bindingSet = this.CreateBindingSet<DashboardView, DashboardViewModel>();
            bindingSet.Bind(this).For(v => v.NumberOfCategoryItemList).To(vm => vm.NumberOfCategoryItemList);
            bindingSet.Bind(this).For(v => v.NumberOfProcessingUnitItemList).To(vm => vm.NumberOfProcessingUnitItemList);
            bindingSet.Apply();
        }
    }
}