using System;
using System.Windows.Input;
using Android.App;
using Android.Content;
using Android.Gms.Plus;
using Android.Runtime;
using Android.Views;
using Android.Webkit;
using Android.Widget;
using GovernmentComplaint.Core.ViewModels;
using GovernmentComplaint.Droid.Adapters;
using GovernmentComplaint.Droid.Controls;
using GovernmentComplaint.Droid.Helpers;
using GovernmentComplaint.Droid.Listener;
using GovernmentComplaint.Droid.Views.Bases;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Binding.Droid.BindingContext;
using MvvmCross.Binding.Droid.Views;
using Xamarin.Facebook.Share.Model;
using Xamarin.Facebook.Share.Widget;
using Uri = Android.Net.Uri;

namespace GovernmentComplaint.Droid.Views
{
	public class ComplaintDetailView : DetailView
	{
		public override void OnActivityResult(int requestCode, [GeneratedEnum] Result resultCode, Intent data)
		{
			base.OnActivityResult(requestCode, resultCode, data);
			MasterView.CallbackManager.OnActivityResult(requestCode, (int)resultCode, data);
		}
		protected override void InitView(View view)
		{
			_view = view;
			var screenWidth = ResolutionHelper.Width;
			var imageLayout1 = (LinearLayout) view.FindViewById(Resource.Id.productImageLayout1);
			imageLayout1.LayoutParameters.Width = screenWidth;

			var imageLayout2 = (LinearLayout) view.FindViewById(Resource.Id.productImageLayout2);
			imageLayout2.LayoutParameters.Width = screenWidth;

			var imageLayout3 = (LinearLayout) view.FindViewById(Resource.Id.productImageLayout3);
			imageLayout3.LayoutParameters.Width = screenWidth;
			_indicator1 = (ImageView) view.FindViewById(Resource.Id.indicator1);
			_indicator1.SetImageResource(Resource.Drawable.indicator_circle_gray);

			_indicator2 = (ImageView) view.FindViewById(Resource.Id.indicator2);
			_indicator2.SetImageResource(Resource.Drawable.indicator_circle_light_gray);

			_indicator3 = (ImageView) view.FindViewById(Resource.Id.indicator3);
			_indicator3.SetImageResource(Resource.Drawable.indicator_circle_light_gray);

			_imageScrollView = (CustomHorizontalScrollView) view.FindViewById(Resource.Id.scrollView);
			_imageScrollView.ScrollStateChanged += OnScrollStateChanged;

			//Facebook Like Button
			_likeView = (LikeView) view.FindViewById(Resource.Id.likeView);
			_likeView.SetAuxiliaryViewPosition(LikeView.AuxiliaryViewPosition.Inline);
			_likeView.SetLikeViewStyle(LikeView.Style.Button);

			//Facebook Share Button
			_fbShareButton = (ShareButton) view.FindViewById(Resource.Id.btnFacebookShare);

			//Google+ sharing
			_plusOneButton = (PlusOneButton) view.FindViewById(Resource.Id.plus_one_button);

			_attachedFileName1 = (TextView) view.FindViewById(Resource.Id.FileName1);
			_attachedFileName2 = (TextView) view.FindViewById(Resource.Id.FileName2);
			_attachedFileName3 = (TextView) view.FindViewById(Resource.Id.FileName3);

			_attachedFileName1.Click += OnAttachFileName1Clicked;
			_attachedFileName2.Click += OnAttachFileName2Clicked;
			_attachedFileName3.Click += OnAttachFileName3Clicked;

			_contentView = (WebView) view.FindViewById(Resource.Id.ComplaintDetailContent);
			_titleView = (WebView) view.FindViewById(Resource.Id.ComplaintDetailTitle);
			_replyContentView = (WebView) view.FindViewById(Resource.Id.ReplyContent);
			_commentFrame = (CustomEditText) view.FindViewById(Resource.Id.CommentFrame);
			_commentFrame.SetOnTouchListener(new MyTouchListener());

			_addCommentButton = view.FindViewById<Button>(Resource.Id.postCommentButton);
			_addCommentButton.Click += (sender, args) => KeyboardHelper.HideKeyboard(view);

			_commentList = (MvxListView)view.FindViewById(Resource.Id.CommentList);
			var adapter = new ListViewAdapter(Application.Context, (IMvxAndroidBindingContext)BindingContext);
			_commentList.Adapter = adapter;

            // Answer feedback view
		    _answerResponseView = view.FindViewById<RelativeLayout>(Resource.Id.answerResponseResultContainer);
		    _answerResponseResultContainerView = view.FindViewById<RelativeLayout>(Resource.Id.answerResponseResultContainer);
		    _answerResponseButton = view.FindViewById<Button>(Resource.Id.answerResponseButton);

		}

		private void OnAttachFileName3Clicked(object sender, EventArgs e)
		{
			var browserIntent = new Intent(Intent.ActionView, Uri.Parse(FileUrl3));
			StartActivity(browserIntent);
		}

		private void OnAttachFileName2Clicked(object sender, EventArgs e)
		{
			var browserIntent = new Intent(Intent.ActionView, Uri.Parse(FileUrl2));
			StartActivity(browserIntent);
		}

		private void OnAttachFileName1Clicked(object sender, EventArgs e)
		{
			var browserIntent = new Intent(Intent.ActionView, Uri.Parse(FileUrl1));
			StartActivity(browserIntent);
		}

		public override void OnResume()
		{
			base.OnResume();
			CheckNetworkCommand?.Execute(null);
			if (!string.IsNullOrEmpty(ComplaintDetailURL))
			{
				_plusOneButton.Initialize(ComplaintDetailURL, PlusOneRequestCode);
			}
		}

		protected override void CreateBinding()
		{
			var bindingSet = this.CreateBindingSet<ComplaintDetailView, ComplaintDetailViewModel>();
			bindingSet.Bind(this).For(v => v.DetailImageClickCommand).To(vm => vm.DetailImageClickCommand);
			bindingSet.Bind(this).For(v => v.ImagesQuantity).To(vm => vm.ImagesQuantity);
			bindingSet.Bind(this).For(v => v.CommentQuantity).To(vm => vm.CommentQuantity);
			bindingSet.Bind(this).For(v => v.IsKeyboardShown).To(vm => vm.IsKeyboardShown);
			bindingSet.Bind(this).For(v => v.ScrollPositionValue).To(vm => vm.ScrollPositionValue).TwoWay();
			bindingSet.Bind(this).For(v => v.ComplaintDetailURL).To(vm => vm.ComplaintDetailURL);
			bindingSet.Bind(this).For(v => v.FileUrl1).To(vm => vm.FileUrl1).TwoWay();
			bindingSet.Bind(this).For(v => v.FileUrl2).To(vm => vm.FileUrl2).TwoWay();
			bindingSet.Bind(this).For(v => v.FileUrl3).To(vm => vm.FileUrl3).TwoWay();
			bindingSet.Bind(this)
				.For(v => v.CheckNetworkCommand)
				.To(vm => vm.CheckNetworkCommand);
			bindingSet.Bind(this)
				.For(v => v.WebViewContent)
				.To(vm => vm.Content);
			bindingSet.Bind(this)
				.For(v => v.WebViewReplyContent)
				.To(vm => vm.ReplyContent);
			bindingSet.Bind(this)
				.For(v => v.WebViewTitle)
				.To(vm => vm.Title);
			base.CreateBinding();

		    bindingSet.Bind(this).For(v => v.IsAnswerResponseShown).To(vm => vm.IsAnswerResponseShown);
            bindingSet.Bind(this).For(v => v.IsFeedbackOfUser).To(vm => vm.IsFeedbackOfUser);
		    bindingSet.Bind(this).For(v => v.IsSatisfactionFeedback).To(vm => vm.IsSatisfactionFeedback);

            bindingSet.Apply();
		}

		private void ApplyNewHeightToList(int index)
		{
			#region Github ingredient

			//var commentListHeight = _commentList.LayoutParameters as RelativeLayout.LayoutParams;
			//_adapter = _commentList.Adapter;
			//if (commentListHeight != null)
			//{
			//    var total = 10;
			//    if (_adapter == null) return;
			//    for (var i = 0; i < _adapter.Count; i++)
			//    {

			//        var item = _adapter.GetView(i, null, _commentList);
			//        item.Measure(View.MeasureSpec.MakeMeasureSpec(0, MeasureSpecMode.Unspecified),
			//            View.MeasureSpec.MakeMeasureSpec(0, MeasureSpecMode.Unspecified));
			//        total += item.MeasuredHeight;
			//        Debug.Write("DMMMMMM:" + item.MeasuredHeight);
			//    }
			//    commentListHeight.Height = total;
			//}
			//_commentList.LayoutParameters = commentListHeight;

			#endregion

			var commentListHeight = _commentList.LayoutParameters as RelativeLayout.LayoutParams;

			switch (index)
			{
				case 0:
					if (commentListHeight != null)
						commentListHeight.Height = 20;
					break;
				default:
					if (commentListHeight != null)
						commentListHeight.Height = 500;
					break;
			}

			_commentList.LayoutParameters = commentListHeight;
		}

		private void OnScrollStateChanged(bool isClicked)
		{
			if (isClicked)
			{
				// Click
				ScrollPositionValue = _imageScrollView.ScrollX / ResolutionHelper.Width;
				DetailImageClickCommand?.Execute(null);
			}
			else
			{
				// Scroll
				if (_imageScrollView.ScrollX == 0)
				{
					_indicator1.SetImageResource(Resource.Drawable.indicator_circle_gray);
					_indicator2.SetImageResource(Resource.Drawable.indicator_circle_light_gray);
					_indicator3.SetImageResource(Resource.Drawable.indicator_circle_light_gray);
				}
				else if (_imageScrollView.ScrollX == ResolutionHelper.Width)
				{
					_indicator1.SetImageResource(Resource.Drawable.indicator_circle_light_gray);
					_indicator2.SetImageResource(Resource.Drawable.indicator_circle_gray);
					_indicator3.SetImageResource(Resource.Drawable.indicator_circle_light_gray);
				}
				else if (_imageScrollView.ScrollX == ResolutionHelper.Width * 2)
				{
					_indicator1.SetImageResource(Resource.Drawable.indicator_circle_light_gray);
					_indicator2.SetImageResource(Resource.Drawable.indicator_circle_light_gray);
					_indicator3.SetImageResource(Resource.Drawable.indicator_circle_gray);
				}
			}
		}

		#region Properties

		private const int PlusOneRequestCode = 0;
		private TextView _attachedFileName1, _attachedFileName2, _attachedFileName3;
		private CustomEditText _commentFrame;
		private MvxListView _commentList;
		private string _complaintDetailURL;
		private WebView _contentView, _titleView, _replyContentView;
		private ShareButton _fbShareButton;
		private CustomHorizontalScrollView _imageScrollView;
		private ImageView _indicator1, _indicator2, _indicator3;
		private LikeView _likeView;
		private PlusOneButton _plusOneButton;
		private View _view;
		private Button _addCommentButton;
	    private RelativeLayout _answerResponseView, _answerResponseResultContainerView;
	    private Button _answerResponseButton;

        protected override int LayoutId => Resource.Layout.ComplaintDetailView;
		public string FileUrl1 { get; set; }
		public string FileUrl2 { get; set; }
		public string FileUrl3 { get; set; }
		public ICommand CheckNetworkCommand { get; set; }
		public ICommand DetailImageClickCommand { get; set; }

	    private bool _isFeedbackOfUser;

	    public bool IsFeedbackOfUser
        {
	        get => _isFeedbackOfUser;
	        set
	        {
	            _isFeedbackOfUser = value;

                if (value)
                {
                    if (!IsAnswerResponseShown)
                    {
                        _answerResponseButton.Visibility = ViewStates.Visible;
                        _answerResponseResultContainerView.Visibility = ViewStates.Gone;
                    }
                    else
                    {
                        _answerResponseButton.Visibility = ViewStates.Gone;
                        _answerResponseResultContainerView.Visibility = ViewStates.Visible;
                    }
                }
                else
                {
                    _answerResponseButton.Visibility = ViewStates.Gone;
                    _answerResponseResultContainerView.Visibility = ViewStates.Visible;

                    //if (!IsAnswerResponseShown)
                    //{
                    //    _answerResponseButton.Visibility = ViewStates.Gone;
                    //    _answerResponseResultContainerView.Visibility = ViewStates.Gone;
                    //}
                    //else
                    //{
                    //    if (IsSatisfactionFeedback)
                    //    {
                    //        //_heightAnswerFeedbackConstraint.Constant = DimensionHelper.CommonHeight;
                    //        //_answerFeedbackContainer.AddConstraint(_heightAnswerFeedbackConstraint);

                    //        //_answerFeedbackResultContainer.Hidden = false;
                    //    }
                    //    else
                    //    {
                    //        //_answerFeedbackResultContainer.Hidden = false;

                    //        //_heightAnswerFeedbackConstraint.Constant = DimensionHelper.FeedbackContentHeight;
                    //        //_answerFeedbackContainer.AddConstraint(_heightAnswerFeedbackConstraint);
                    //    }
                    //}
                }
            }
	    }

        public string ComplaintDetailURL
		{
			get { return _complaintDetailURL; }
			set
			{
				if (!string.IsNullOrEmpty(value))
				{
					_likeView.SetObjectIdAndType(value, LikeView.ObjectType.OpenGraph);
					_likeView.SetEnabled(true);
					var content = new ShareLinkContent.Builder()
						.SetContentUrl(Uri.Parse(value))
						.JavaCast<ShareLinkContent.Builder>()
						.Build();
					_fbShareButton.ShareContent = content;
					_plusOneButton.Initialize(value, PlusOneRequestCode);
				}
				else
				{
					_likeView.SetEnabled(false);
				}
				_complaintDetailURL = value;
			}
		}

	    public bool IsAnswerResponseShown { get; set; }
	    public bool IsSatisfactionFeedback { get; set; }

        private int _scrollPositionValue;

		public int ScrollPositionValue
		{
			get { return _scrollPositionValue; }
			set
			{
				_scrollPositionValue = value;
				ScrollPositionValueChanged?.Invoke(this, null);
			}
		}

		private string _webViewContent;

		public string WebViewContent
		{
			get { return _webViewContent; }
			set
			{
				_webViewContent = value.Replace("\n", "<br/>");
				var text = "<html><head>"
						   + "<style type=\"text/css\">body{color: #000000;font-size:13px;}"
						   + "</style></head>"
						   + "<body>"
						   + "<p align=\"justify\">"
						   + _webViewContent
						   + "</p> "
						   + "</body></html>";
				text = text.Replace(".com", ". com");
				_contentView.LoadData(text, "text/html; charset=UTF-8", null);
			}
		}

		private string _webViewTitle;

		public string WebViewTitle
		{
			get { return _webViewTitle; }
			set
			{
				_webViewTitle = value.Replace("\n", "<br/>");
				var text = "<html><head>"
						   + "<style type=\"text/css\">body{color: #000000;font-size:16px;}"
						   + "</style></head>"
						   + "<body>"
						   + "<p align=\"justify\">"
						   + "<b>"
						   + _webViewTitle
						   + "</b>"
						   + "</p> "
						   + "</body></html>";
				_titleView.LoadData(text, "text/html; charset=UTF-8", null);
			}
		}

		private string _webViewReplyContent;

		public string WebViewReplyContent
		{
			get { return _webViewReplyContent; }
			set
			{
				_webViewReplyContent = value.Replace("\n", "<br/>");
				var text = "<html><head>"
						   + "<style type=\"text/css\">body{color: #000000;font-size:13px;}"
						   + "</style></head>"
						   + "<body bgcolor=\"#F0F7FB\">"
						   + "<p align=\"justify\">"
						   + _webViewReplyContent
						   + "</p> "
						   + "</body></html>";
				_replyContentView.LoadData(text, "text/html; charset=UTF-8", null);
			}
		}


		public event EventHandler ScrollPositionValueChanged;

		private bool _isKeyboardShown;

		public bool IsKeyboardShown
		{
			get { return _isKeyboardShown; }
			set
			{
				_isKeyboardShown = value;
				if (!value)
					KeyboardHelper.HideKeyboard(_view);
			}
		}

		private int _commentQuantity;

		public int CommentQuantity
		{
			get { return _commentQuantity; }
			set
			{
				_commentQuantity = value;
				ApplyNewHeightToList(_commentQuantity);
			}
		}

		private int _imagesQuantity;

		public int ImagesQuantity
		{
			get { return _imagesQuantity; }
			set
			{
				_imagesQuantity = value;
				ChangeIndicatorMargin(_imagesQuantity);
			}
		}

		private void ChangeIndicatorMargin(int index)
		{
			switch (index)
			{
				case 1:
					var l1 = _indicator1.LayoutParameters as LinearLayout.LayoutParams;
					if (l1 != null)
						l1.RightMargin = 0;
					_indicator1.LayoutParameters = l1;
					break;
				case 2:
					var l2 = _indicator2.LayoutParameters as LinearLayout.LayoutParams;
					if (l2 != null)
						l2.RightMargin = 0;
					_indicator2.LayoutParameters = l2;
					break;
				default:
					var l3 = _indicator3.LayoutParameters as LinearLayout.LayoutParams;
					if (l3 != null)
						l3.RightMargin = 0;
					_indicator3.LayoutParameters = l3;
					break;
			}
		}

		#endregion
	}
}