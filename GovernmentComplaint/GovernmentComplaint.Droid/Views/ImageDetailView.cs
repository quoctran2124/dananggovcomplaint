using System;
using Android.Content;
using Android.Net;
using Android.Views;
using Android.Widget;
using GovernmentComplaint.Core.ViewModels;
using GovernmentComplaint.Droid.Controls;
using GovernmentComplaint.Droid.Helpers;
using GovernmentComplaint.Droid.Views.Bases;
using MvvmCross.Binding.BindingContext;

namespace GovernmentComplaint.Droid.Views
{
    public class ImageDetailView : DetailView, ViewTreeObserver.IOnGlobalLayoutListener
    {
        private CustomHorizontalScrollView _imageScrollView;
        private ImageView _indicator1, _indicator2, _indicator3;

        protected override int LayoutId => Resource.Layout.ImageDetailView;

        protected override void InitView(View view)
        {
            var screenWidth = ResolutionHelper.Width;
            var productImageLayout1 = (LinearLayout)view.FindViewById(Resource.Id.productImageLayout1);
            productImageLayout1.LayoutParameters.Width = screenWidth;
            var productImage1 = (CustomPhotoView)view.FindViewById(Resource.Id.productImage1);
            productImage1.LayoutParameters.Width = screenWidth;

            var productImageLayout2 = (LinearLayout)view.FindViewById(Resource.Id.productImageLayout2);
            productImageLayout2.LayoutParameters.Width = screenWidth;

            var productImageLayout3 = (LinearLayout)view.FindViewById(Resource.Id.productImageLayout3);
            productImageLayout3.LayoutParameters.Width = screenWidth;

            _indicator1 = (ImageView)view.FindViewById(Resource.Id.indicator1);
            _indicator1.SetImageResource(Resource.Drawable.indicator_circle_gray);

            _indicator2 = (ImageView)view.FindViewById(Resource.Id.indicator2);
            _indicator2.SetImageResource(Resource.Drawable.indicator_circle_light_gray);

            _indicator3 = (ImageView)view.FindViewById(Resource.Id.indicator3);
            _indicator3.SetImageResource(Resource.Drawable.indicator_circle_light_gray);

            _imageScrollView = (CustomHorizontalScrollView)view.FindViewById(Resource.Id.scrollView);
            _imageScrollView.ScrollStateChanged += OnScrollStateChanged;
            _imageScrollView.ViewTreeObserver.AddOnGlobalLayoutListener(this);
        }

        private void OnScrollStateChanged(bool isClicked)
        {
            if (!isClicked)
            {
                RearrangeIndicators();
            }
        }

        private void RearrangeIndicators()
        {
            if (_imageScrollView.ScrollX == 0)
            {
                _indicator1.SetImageResource(Resource.Drawable.indicator_circle_gray);
                _indicator2.SetImageResource(Resource.Drawable.indicator_circle_light_gray);
                _indicator3.SetImageResource(Resource.Drawable.indicator_circle_light_gray);
            }
            else if (_imageScrollView.ScrollX == ResolutionHelper.Width)
            {
                _indicator1.SetImageResource(Resource.Drawable.indicator_circle_light_gray);
                _indicator2.SetImageResource(Resource.Drawable.indicator_circle_gray);
                _indicator3.SetImageResource(Resource.Drawable.indicator_circle_light_gray);
            }
            else if (_imageScrollView.ScrollX == ResolutionHelper.Width * 2)
            {
                _indicator1.SetImageResource(Resource.Drawable.indicator_circle_light_gray);
                _indicator2.SetImageResource(Resource.Drawable.indicator_circle_light_gray);
                _indicator3.SetImageResource(Resource.Drawable.indicator_circle_gray);
            }
        }

        private int _scrollPosition;
        public int ScrollPosition
        {
            get { return _scrollPosition; }
            set
            {
                _scrollPosition = value;
            }
        }

        private int _imagesQuantity;
        public int ImagesQuantity
        {
            get { return _imagesQuantity; }
            set
            {
                _imagesQuantity = value;
                switch (_imagesQuantity)
                {
                    case 1:
                        var l1 = _indicator1.LayoutParameters as LinearLayout.LayoutParams;
                        if (l1 != null)
                        {
                            l1.RightMargin = 0;
                        }
                        _indicator1.LayoutParameters = l1;
                        break;
                    case 2:
                        var l2 = _indicator2.LayoutParameters as LinearLayout.LayoutParams;
                        if (l2 != null)
                        {
                            l2.RightMargin = 0;
                        }
                        _indicator2.LayoutParameters = l2;
                        break;
                    case 3:
                        var l3 = _indicator3.LayoutParameters as LinearLayout.LayoutParams;
                        if (l3 != null)
                        {
                            l3.RightMargin = 0;
                        }
                        _indicator3.LayoutParameters = l3;
                        break;
                    default:
                        break;
                }

            }
        }

        protected override void CreateBinding()
        {
            var bindingSet = this.CreateBindingSet<ImageDetailView, ImageDetailViewModel>();
            bindingSet.Bind(this).For(v => v.ImagesQuantity).To(vm => vm.ImagesQuantity);
            bindingSet.Bind(this).For(v => v.ScrollPosition).To(vm => vm.ScrollPosition);
            base.CreateBinding();
            bindingSet.Apply();

        }

        public void OnGlobalLayout()
        {
	        if (IsConnected)
	        {
				Activity.RunOnUiThread(() => _imageScrollView.ScrollTo(_scrollPosition * ResolutionHelper.Width, 0));
				var isAtScrollPosition = _imageScrollView.ScrollX == _scrollPosition * ResolutionHelper.Width;
				RearrangeIndicators();

				if (isAtScrollPosition)
				{
					_imageScrollView.ViewTreeObserver.RemoveOnGlobalLayoutListener(this);
				}
			}
		}

		public bool IsConnected
		{
			get
			{
				try
				{
					var activeConnection = ConnectivityManager.ActiveNetworkInfo;
					return activeConnection != null && activeConnection.IsConnected;
				}
				catch (Exception)
				{
					return false;
				}
			}
		}

		private ConnectivityManager _connectivityManager;

		public ConnectivityManager ConnectivityManager
		{
			get
			{
				_connectivityManager = _connectivityManager ?? (ConnectivityManager)Activity.GetSystemService(Context.ConnectivityService);
				return _connectivityManager;
			}
		}
	}
}