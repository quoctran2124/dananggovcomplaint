using Android.Views;
using GovernmentComplaint.Droid.Views.Bases;

namespace GovernmentComplaint.Droid.Views
{
    public class AboutView : DetailView
    {
        protected override int LayoutId => Resource.Layout.AboutView;

        protected override void InitView(View view)
        {
        }

        protected override void CreateBinding()
        {
        }
    }
}