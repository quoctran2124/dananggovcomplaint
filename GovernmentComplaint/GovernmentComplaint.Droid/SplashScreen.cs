﻿using Android.App;
using Android.Content.PM;
using GovernmentComplaint.Droid.Helpers;
using HockeyApp.Android;
using MvvmCross.Droid.Views;

namespace GovernmentComplaint.Droid
{
    [Activity(
        Label = "Góp Ý Đà Nẵng"
        , MainLauncher = true
        , Icon = "@drawable/icon"
        , Theme = "@style/MyTheme.Splash"
        , NoHistory = true
        , ScreenOrientation = ScreenOrientation.Portrait)]
    public class SplashScreen : MvxSplashScreenActivity
    {
        public const string HOCKEYAPP_APPID = "d585bc3cebc94060b1680f989230b9ce";

        protected override void OnCreate(Android.OS.Bundle bundle)
        {
            base.OnCreate(bundle);
            CrashManager.Register(this, HOCKEYAPP_APPID, new HockeyCrashManagerSettings());
            ResolutionHelper.InitStaticVariable(this);
            ColorHelper.InitColors(this);
        }
    }
    public class HockeyCrashManagerSettings : CrashManagerListener { public override bool ShouldAutoUploadCrashes() { return true; } }
}
