using MvvmCross.Platform.Plugins;

namespace GovernmentComplaint.Droid.Bootstrap
{
    public class FilePluginBootstrap
        : MvxPluginBootstrapAction<MvvmCross.Plugins.File.PluginLoader>
    {
    }
}