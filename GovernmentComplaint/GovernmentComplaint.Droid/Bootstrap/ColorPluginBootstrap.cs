using MvvmCross.Platform.Plugins;

namespace GovernmentComplaint.Droid.Bootstrap
{
    public class ColorPluginBootstrap
        : MvxPluginBootstrapAction<MvvmCross.Plugins.Color.PluginLoader>
    {
    }
}