using MvvmCross.Platform.Plugins;

namespace GovernmentComplaint.Droid.Bootstrap
{
    public class ResourceLoaderPluginBootstrap
        : MvxPluginBootstrapAction<MvvmCross.Plugins.ResourceLoader.PluginLoader>
    {
    }
}