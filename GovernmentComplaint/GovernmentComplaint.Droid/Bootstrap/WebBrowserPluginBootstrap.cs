using MvvmCross.Platform.Plugins;

namespace GovernmentComplaint.Droid.Bootstrap
{
    public class WebBrowserPluginBootstrap
        : MvxPluginBootstrapAction<MvvmCross.Plugins.WebBrowser.PluginLoader>
    {
    }
}