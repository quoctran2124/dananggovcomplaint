using MvvmCross.Platform.Plugins;

namespace GovernmentComplaint.Droid.Bootstrap
{
    public class PhoneCallPluginBootstrap
        : MvxPluginBootstrapAction<MvvmCross.Plugins.PhoneCall.PluginLoader>
    {
    }
}